/**
 * 
 */
package com.sgcc.devops.core.util;

import org.apache.log4j.Logger;

import com.sgcc.devops.common.util.SSH;

/**
 * @author luogan 2015年5月19日 下午5:41:20
 */
public abstract class HostInfo {

	final static String DEFAULT_USER = "root";

	final static Logger logger = Logger.getLogger(HostInfo.class);

	/**
	 * 默认账户获得ssh连接
	 * 
	 * @param hostIp
	 * @param hostPwd
	 * @return
	 */
	public SSH getConnect(String hostIp, String hostPwd) {
		SSH ssh = new SSH(hostIp, DEFAULT_USER, hostPwd);
		return ssh.connect() ? ssh : null;
	}

	/**
	 * 获取SSH连接
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户名
	 * @param password
	 *            用户密码
	 * @return
	 */
	public SSH getConnection(String ip, String username, String password,String hostport) {
		SSH ssh = new SSH(ip, username, password, Integer.parseInt(hostport));
		return ssh.connect() ? ssh : null;
	}

	/**
	 * 获取docker主机的UUID
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户账号
	 * @param password
	 *            用户密码
	 * @return
	 */
	public String host(String ip, String username, String password, String hostPort) {
		return execCommandReq(ip, username, password, hostPort, queryCommand());
	}
	

	/**
	 * 获取docker主机的UUID
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户账号
	 * @param password
	 *            用户密码
	 * @return
	 */
	public String UUID(String ip, String username, String password, String hostPort) {
		return execCommand(ip, username, password, hostPort, command());
	}

	/**
	 * 获取Docker主机的UUID
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户账号
	 * @param password
	 *            用户密码
	 * @return
	 */
	public String hostUUID(String ip, String username, String password, String hostport) {
		return execCommand(ip, username, password, hostport, command());
	}

	/**
	 * 检测swarm进程
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户账号
	 * @param password
	 *            用户密码
	 * @return
	 */
	public boolean checkSwarm(String ip, String username, String password, String hostport) {
		SSH ssh = getConnection(ip, username, password, hostport);
		if (null == ssh) {
			return false;
		}
		boolean fileExist = false;
		try {
			fileExist = ssh.GetFile("/usr/bin/swarm", this.getClass().getResource("/").getPath());
		} catch (Exception e) {
			logger.error("Check host swarm exception:", e);
		} finally {
			ssh.close();
		}
		return fileExist;
	}

	/**
	 * 启动Swarm进程
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户名称
	 * @param password
	 *            用户密码
	 * @param port
	 *            swarm端口
	 * @param path
	 *            集群配置文件路径
	 * @return
	 */
	public boolean startSwarm(String ip, String username, String password, String hostport,String port, String path) {
		String directory = path.substring(0, path.lastIndexOf("/"));
		boolean startResult = false;

		String command = "mkdir -p " + directory + ";rm -rf "+directory+"/*;touch " + path + ";chmod 775 " + path + ";";
		startResult = execBooleanCommand(ip, username, password,hostport,command,port);
		if (startResult) {
			logger.info("Create config file success");
			startResult = execCommandWithoutResult(ip, username, password, hostport,commandStartSwarm(ip, port, path));
		} else {
			logger.error("Create config file fail");
		}
		return startResult;
	}

	/**
	 * 停止Swarm进程
	 * 
	 * @param ip
	 *            主机地址
	 * @param username
	 *            用户名称
	 * @param password
	 *            用户密码
	 * @param port
	 *            swarm端口
	 * @param path
	 *            集群配置文件路径
	 * @return
	 */
	public boolean stopSwarm(String ip, String username, String password, String hostPort,String port) {
		return execCommandWithoutResult(ip, username, password, hostPort,commandStopSwarm(port));
	}

	public String checkDocker(String ip, String username, String password, String hostport) {
		return execCommand(ip, username, password, hostport, dockerInfoCommand());
	}

	public String queryDockerPort(String ip, String username, String password, String hostport) {
		return execCommand(ip, username, password, hostport, queryDockerPortCommand());
	}

	public String clusterHealthCheck(String ip, String username, String password, String hostport,String clusterPort) {
		return execCheckCommand(ip, username, password,hostport, clusterHealthCheckCommand(ip, clusterPort));
	}

	public String execCheckCommand(String ip, String username, String password, String hostport, String clusterHealthCheckCommand) {
		SSH ssh = this.getConnection(ip, username, password, hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return null;
		}

		try {
			return ssh.executeWithResult(clusterHealthCheckCommand);
		} catch (Exception e) {
			logger.error("Execute command :[" + clusterHealthCheckCommand + "] exception:", e);
			return null;
		} finally {
			ssh.close();
		}
	}

	/**
	 * 指定指定的命令
	 * 
	 * @param command
	 *            linux命令字符串
	 * @return 执行的结果
	 */
	public String execCommandReq(String ip, String username, String password,String hostport, String queryCommand) {
		SSH ssh = this.getConnection(ip, username, password,hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return null;
		}
		try {
			// return ssh.toString();
			return ssh.executeWithResult(queryCommand);
		} catch (Exception e) {
			logger.error("Execute command exception:", e);
			return null;
		} finally {
			ssh.close();
		}
	}

	/**
	 * 指定指定的命令
	 * 
	 * @param command
	 *            linux命令字符串
	 * @return 执行的结果
	 */
	public String execCommand(String ip, String username, String password, String hostport, String command) {
		SSH ssh = this.getConnection(ip, username, password, hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return null;
		}
		try {
			return ssh.executeWithResult(command);
		} catch (Exception e) {
			logger.error("Execute command :[" + command + "] exception:", e);
			return null;
		} finally {
			ssh.close();
		}
	}
	/**
	 * 指定指定的命令
	 * 
	 * @param command
	 *            linux命令字符串
	 * @return 执行的结果
	 */
	public String execCommandWithTimeOut(String ip, String username, String password, String hostport, String command,Long timeout) {
		SSH ssh = this.getConnection(ip, username, password, hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return null;
		}
		try {
			return ssh.executeWithResult(command, timeout);
		} catch (Exception e) {
			logger.error("Execute command :[" + command + "] exception:", e);
			return null;
		} finally {
			ssh.close();
		}
	}
	public boolean execBooleanCommand(String ip, String username, String password, String hostport, String command,String port) {
		SSH ssh = this.getConnection(ip, username, password, hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return false;
		}
		try {
			return ssh.execute(command, 60 * 1000);
		} catch (Exception e) {
			logger.error("Execute command :[" + command + "] exception:", e);
			return false;
		} finally {
			ssh.close();
		}
	}

	public boolean execCommandWithoutResult(String ip, String username, String password, String hostport, String command) {
		SSH ssh = this.getConnection(ip, username, password,hostport);
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return false;
		}
		try {
			String result = ssh.executeWithResult(command);
			if(result.isEmpty()){
				return true;
			}
			return false;
		} catch (Exception e) {
			logger.error("Execute command :[" + command + "] exception:", e);
			return false;
		} finally {
			ssh.close();
		}
	}

	protected abstract String command();

	protected abstract String queryCommand();

	protected abstract String commandStartSwarm(String ip, String port, String path);

	protected abstract String commandStopSwarm(String port);

	/**
	 * 检查docker环境的命令行
	 * 
	 * @return
	 */
	protected abstract String dockerInfoCommand();

	protected abstract String queryDockerPortCommand();

	protected abstract String clusterHealthCheckCommand(String ip, String port);

	protected abstract String valid();

	protected abstract String clusterCommand();
	
	public boolean dockerInstalled(String ip, String username, String password,String hostport) {
		String result = execCommand(ip, username, password,hostport, dockerCommand());
		if(null==result||result.contains("command not found")){
			return false;
		}else{
			return true;
		}
	}

	/**
	* TODO
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年3月8日
	*/
	protected abstract String dockerCommand();

	/**
	* TODO停止docker进程
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年3月9日
	*/
	protected abstract String commandStopDocker();
	public boolean stopDocker(String ip, String username, String password,String hostport) {
		String result = execCommand(ip, username, password,hostport, commandStopDocker());
		if(null==result||result.contains("cannot find process")){
			return false;
		}else{
			return true;
		}
	}
	/**
	* TODO启动docker进程
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年3月9日
	*/
	protected abstract String commandStartDocker();
	public String startDocker(String ip, String username, String password, String hostport) {
		String result = execCommand(ip, username, password,hostport,  commandStartDocker());
		return result;
	}
	
	public boolean uploadFile(String ip, String name, String password, String hostport,
			String file, String hostFileLocation, String hostFileName) {
		SSH ssh = this.getConnection(ip, name, password,hostport);
		boolean result;
		if (null == ssh) {
			logger.warn("Can not connect to host 【" + ip + "】");
			return false;
		}
		try {
			 result = ssh.CopyDirectory(file, hostFileLocation, hostFileName);
		} catch (Exception e) {
			logger.error("上传文件失败, exception:", e);
			result = false;
		} finally {
			ssh.close();
		}
		return result;
	}
	protected abstract String commandInstallAllFile(String hostFileLocation, String hostFileName);
	public String installAllFile(String ip, String username, String password,String hostport, String hostFileLocation, String hostFileName ) {
		String result = execCommand(ip, username, password,hostport, commandInstallAllFile(hostFileLocation,hostFileName));
		return result;
	}
	protected abstract String commandCreateFileLocation(String hostFileLocation);
	public String createFileLocation(String ip, String username, String password, String hostport,String hostFileLocation ) {
		String result = execCommand(ip, username, password,hostport, commandCreateFileLocation(hostFileLocation));
		return result;
	}
	public String getVersion(String ip, String username, String password,String hostport,String command) {
		String result = execCommand(ip, username, password, hostport, command);
		return result;
	}
}
