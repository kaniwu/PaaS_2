package com.sgcc.devops.core.detector;

import com.sgcc.devops.common.util.SSH;

/**
 * 主机健康探测器
 * 
 * @author dmw
 *
 */
public class HostDetector implements Detector {

	private String hostname;
	private String username;
	private String hostSecure;
	private String hostPort;

	public HostDetector() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HostDetector(String hostname, String username, String hostSecure,String hostPort) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.hostSecure = hostSecure;
		this.hostPort = hostPort;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String gethostSecure() {
		return hostSecure;
	}

	public void sethostSecure(String hostSecure) {
		this.hostSecure = hostSecure;
	}
	
	public String gethostPort() {
		return hostPort;
	}

	public void sethostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	@Override
	public boolean normal() {
		SSH ssh = new SSH(hostname, username, hostSecure,Integer.parseInt(hostPort));
		if (ssh.connect()) {
			ssh.close();
			return true;
		} else {
			return false;
		}
	}

}
