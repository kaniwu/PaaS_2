package com.sgcc.devops.core.monitor;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.Monitor;

public class MonitorResult extends Result {

	private Monitor data;

	public Monitor getData() {
		return data;
	}

	public void setData(Monitor data) {
		this.data = data;
	}

	public MonitorResult(boolean success, String message, Monitor data) {
		super(success, message);
		this.data = data;
	}

	public MonitorResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MonitorResult(boolean success, String message) {
		super(success, message);
		// TODO Auto-generated constructor stub
	}

}
