/**
 * 
 */
package com.sgcc.devops.core.impl;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.SystemCore;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * date：2015年9月25日 上午9:03:46 project name：sgcc.devops.core
 * 
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：SystemCoreImpl.java description：
 */
@Component
public class SystemCoreImpl implements SystemCore {
	private static Logger logger = Logger.getLogger(SystemCore.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.core.SystemCore#baseDeploy(com.sgcc.devops.common.model.
	 * DeployModel, net.sf.json.JSONObject, net.sf.json.JSONObject,
	 * net.sf.json.JSONObject)
	 */
	public JSONObject baseDeploy(DeployModel deployModel, JSONObject databaseJO, JSONObject registryhostJO,
			JSONObject clusterhostJO, JSONObject imageJO) {
		String containerId = "";
		JSONObject deployObject = new JSONObject();
		// 集群信息
		String clusterHostIp = clusterhostJO.getString("clusterhostIp");
		clusterhostJO.getString("clusterhostUser");
		clusterhostJO.getString("clusterhostPwd");
		String clusterPort = clusterhostJO.getString("clusterPort");
		// 镜像仓库信息
		String registryhostIp = registryhostJO.getString("registryhostIp");
		String registryhostUser = registryhostJO.getString("registryhostUser");
		String registryhostPwd = registryhostJO.getString("registryhostPwd");
		registryhostJO.getString("registryPort");
		// 基础镜像信息
		String imageName = imageJO.getString("imageName");
		String imageTag = imageJO.getString("imageTag");

		SSH ssh = new SSH(registryhostIp, registryhostUser, registryhostPwd);
		try {
			// 通过dockerfile在仓库主机创建新镜像
			StringBuffer commandStr = new StringBuffer();
			// 在默认目录创建dockerfile文件
//			commandStr.append("cd " + deployModel.getAppRoute() + ";");
//			if (".zip"
//					.equals(deployModel.getConfigFilename().substring(deployModel.getConfigFilename().length() - 4))) {
//				commandStr.append("unzip  " + deployModel.getConfigFilename() + ";");
//				deployModel.getConfigFilename().substring(0, deployModel.getConfigFilename().length() - 4);
//			} else if (".tar.gz"
//					.equals(deployModel.getConfigFilename().substring(deployModel.getConfigFilename().length() - 7))) {
//				commandStr.append("tar –xzf " + deployModel.getConfigFilename() + ";");
//				deployModel.getConfigFilename().substring(0, deployModel.getConfigFilename().length() - 7);
//			}
//			commandStr.append("touch Dockerfile" + ";");
			/* 创建的新的数据源xml文件 */
			String new_dbsource_filename = databaseJO.getString("componentName") + "-jdbc.xml";
			commandStr.append("touch " + new_dbsource_filename + ";");

			// commandStr.append("vi Dockerfile"+";");
			// 编辑生成写入到
			StringBuffer dockerfile_buffer = new StringBuffer();
			dockerfile_buffer.append("# Version: " + deployModel.getDeployVersion() + "\n");
			dockerfile_buffer.append("FROM " + imageName + ":" + imageTag + "\n");
			dockerfile_buffer.append("MAINTAINER beyondcent \"test weblogic version\"" + "\n");
			dockerfile_buffer.append("WORKDIR ~/" + "\n");
			dockerfile_buffer.append("ADD " + new_dbsource_filename + " ~/" + "\n");
			/* 设置环境变量，所有操作都是非交互式的 */
			dockerfile_buffer.append("ENV DEBIAN_FRONTEND noninteractive" + "\n");
			/* 变更镜像的时区设置，保证应用运行的时间正确性 */
			dockerfile_buffer
					.append("RUN echo \"Asia/Shanghai\"> /etc/timezone && dpkg-reconfigure -f noninteractive tzdata");
			/*
			 * 替换weblogic下的config.xml文件中的部分内容，将production-mode-enabled字段设置为false
			 */
			String domain_name = "basic_domain";
			dockerfile_buffer
					.append("RUN sed -i \"s/<production-mode-enabled>true<\\/production-mode-enabled>/<production-mode-enabled>true<\\/production-mode-enabled>/g\" $WLS_HOME/user_projects/domains/"
							+ domain_name + "/cponfig/config.xml");

			/* 分别生成向config.xml文件增加的内容 */
			String AppendConfigXmlStr = geneConfigXmlInfo(databaseJO.getString("componentName"));
			logger.info("Generate Append to Config.xml String is : " + AppendConfigXmlStr);
			/* 向dockerfile中添加替换config */
			dockerfile_buffer.append("RUN sed -i \"s/<\\/domain>/" + AppendConfigXmlStr
					+ "<\\/domain>/g\" $WLS_HOME/user_projects/domains/" + domain_name + "/cponfig/config.xml");

			/* 写入到Dockerfile字符串 */
			String DockerfileStr = dockerfile_buffer.toString();
			commandStr.append("echo -e " + DockerfileStr + ">Dockerfile" + ";");

			// 添加修改weblogic配置，域首先固定，basicdatasource

			String NewDatabaseXmlStr = geneDatasourceXml(databaseJO.getString("componentName"),
					databaseJO.getString("componentIp"), databaseJO.getString("componentPort"),
					databaseJO.getString("componentUserName"), databaseJO.getString("componentPwd"));
			/* 将创建的新增数据源内容添加到数据源xml文件中 */
			commandStr.append("echo -e " + NewDatabaseXmlStr + ">" + new_dbsource_filename + ";");
			logger.info("Generate New Database Source Xml String is :" + NewDatabaseXmlStr);

			/// *修改ngnix配置内容，暂时不处理，因为没有创建container，无法配置nginx*/

			// dockerfile完成后，使用docker build构建新镜像,以应用名称和版本号作为新镜像的名称--不确定
//			String newImageName = registryhostIp + "/" + deployModel.getAppFilename().toLowerCase() + ":"
//					+ deployModel.getDeployVersion();
			String newImageName=null;
			ssh.execute(commandStr.toString());
			logger.info("Node(" + registryhostIp + " Run Cmd:" + commandStr.toString());
			/* 重新清空命令执行缓存字符串 */
			commandStr = new StringBuffer();

			/* 执行打包命令，并获取ID信息 */
			// commandStr.append("docker build -t=\"" + newImageName + "\"" +
			// ";");
			String build_cmd = "sudo docker build -t=\"" + newImageName + "\"" + ";";
			String ImageId = ssh.executeWithResult(build_cmd);
			logger.info("Node(" + registryhostIp + " Run Cmd:" + build_cmd);

			/* 为docker镜像打标 */
			String tag_cmd = "sudo docker tag " + ImageId + " " + newImageName + ";";
			ssh.execute(tag_cmd);
			logger.info("Node(" + registryhostIp + " Run Cmd:" + tag_cmd);

			/* push到本地仓库里面 */
			String push_cmd = "sudo docker push " + newImageName;
			ssh.execute(push_cmd);
			logger.info("Node(" + registryhostIp + " Run Cmd:" + push_cmd);

			/* 在镜像表里添加纪录 */
//			ImageModel biz_image = new ImageModel();
//			biz_image.setImageStatus((byte) Status.IMAGE.NORMAL.ordinal());
//			biz_image.setHostId(registryhostId);
//			biz_image.setImageName(newImageName);
//			biz_image.setImageUuid(ImageId);

			JSONObject imageObject = new JSONObject();
//			imageObject.put("imageName", deployModel.getAppFilename());
			imageObject.put("imageTag", deployModel.getDeployVersion());
			deployObject.put("imageObject", imageObject);

			// 集群查找镜像，不存在pull一个
			String repoUrl = "http://" + clusterHostIp + ":" + clusterPort;
			DockerClient client = DockerClientBuilder.getInstance(repoUrl).build();
			if (!imageIsExist(client, newImageName)) {
				client.pullImageCmd(newImageName);
			}
			try {
				client.close();
			} catch (IOException e1) {
				logger.error("Get connection to docker host error!", e1);
			}
			// 创建多个容器
			String masterUrl = clusterHostIp + ":" + clusterPort;
			JSONArray containerArray = new JSONArray();
			for (int i = 0; i < deployModel.getInstanceCount(); i++) {
				commandStr.append("docker -H tcp://");
				commandStr.append(masterUrl);
				commandStr.append(" run ");
				commandStr.append(" -d ");
				commandStr.append(" -P");
				commandStr.append(" " + newImageName + ";");
				containerId = ssh.executeWithResult(commandStr.toString());

				new JSONObject();
				containerArray.add(containerInfo(getDockerClient(clusterHostIp, clusterPort),
						containerId.substring(0, containerId.length() - 1)));
			}
		} catch (Exception e) {
			logger.error("Make image error!", e);
		} finally {
			if (ssh.connect()) {
				ssh.close();
			}
		}
		return deployObject;
	}

	private String geneConfigXmlInfo(String databaseName) {
		StringBuffer str_buffer = new StringBuffer();
		str_buffer.append("<jdbc-system-resource>");
		str_buffer.append("<name>" + databaseName + "<\\/name>");
		str_buffer.append("<target>basicdatasource<\\/target>");
		str_buffer.append("<descriptor-file-name>jdbc\\/" + databaseName + "-jdbc.xml<\\/descriptor-file-name>");
		str_buffer.append("<\\/jdbc-system-resource>");
		return str_buffer.toString();
	}

	private static String geneDatasourceXml(String databaseName, String ip_addr, String port, String username,
			String password) {
		StringBuffer str_buffer = new StringBuffer();
		str_buffer.append("<?xml version='1.0' encoding='UTF-8'?>");
		str_buffer.append("<jdbc-data-source xmlns=\"http:\\/\\/www.bea.com\\/ns\\/weblogic\\/jdbc-data-source\" "
				+ "xmlns:sec=\"http:\\/\\/www.bea.com\\/ns\\/weblogic\\/90\\/security\" "
				+ "xmlns:wls=\"http:\\/\\/www.bea.com\\/ns\\/weblogic\\/90\\/security\\/wls\" "
				+ "xmlns:xsi=\"http:\\/\\/www.w3.org\\/2001\\/XMLSchema-instance\" "
				+ "xsi:schemaLocation=\"http:\\/\\/www.bea.com\\/ns\\/weblogic\\/jdbc-data-source http:\\/\\/www.bea.com\\/ns\\/weblogic\\/jdbc-data-source\\/1.0\\/jdbc-data-source.xsd\">");
		str_buffer.append("<name>" + databaseName + "</name>");
		str_buffer.append("<jdbc-driver-params>");
		str_buffer.append("<url>jdbc:sqlserver://" + ip_addr + ":" + password + "</url>");
		str_buffer.append("<driver-name>com.microsoft.sqlserver.jdbc.SQLServerDriver</driver-name>");
		str_buffer.append("<properties><property><name>user</name><value>" + username
				+ "</value></property><property><name>databaseName</name><value>" + databaseName
				+ "</value></property></properties>");
		str_buffer.append("<password>" + password + "</password>");
		str_buffer.append("</jdbc-driver-params>");
		str_buffer
				.append("<jdbc-connection-pool-params><initial-capacity>1</initial-capacity><max-capacity>15</max-capacity><capacity-increment>1</capacity-increment>"
						+ "<test-table-name>SQL SELECT 1</test-table-name><statement-cache-size>10</statement-cache-size><statement-cache-type>LRU</statement-cache-type></jdbc-connection-pool-params><jdbc-data-source-params><jndi-name>jdbc/"
						+ databaseName
						+ "</jndi-name><global-transactions-protocol>OnePhaseCommit</global-transactions-protocol></jdbc-data-source-params>");
		str_buffer.append("</jdbc-data-source>");

		return str_buffer.toString();
	}

	/**
	 * @author langzi
	 * @param clusterIp
	 * @param clusterPort
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	public DockerClient getDockerClient(String clusterIp, String clusterPort) {
		String url = "http://" + clusterIp + ":" + clusterPort;
		return DockerClientBuilder.getInstance(url).build();
	}

	public boolean imageIsExist(DockerClient client, String imageName) {
		List<Image> imageList = client.listImagesCmd().exec();
		for (Image image : imageList) {
			for (int i = 0; i < image.getRepoTags().length; i++) {
				if (imageName.equals(image.getRepoTags()[i])) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author langzi
	 * @param containerId
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	public JSONObject containerInfo(DockerClient client, String containerId) {
		JSONObject jo = new JSONObject();
		if (!"".equals(containerId)) {
			boolean start = false;
			while (!start) {
				List<Container> containers = client.listContainersCmd().withShowAll(true).exec();
				int flag = 0;
				for (Container container : containers) {
					if (container.getId().equals(containerId)) {
						jo = JSONUtil.parseObjectToJsonObject(container);
						start = true;
						break;
					}
					flag++;
				}
				if (flag >= containers.size()) {
					break;
				}
			}
		}
		return jo;
	}
}
