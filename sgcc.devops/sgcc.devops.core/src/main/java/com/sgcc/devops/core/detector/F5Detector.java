package com.sgcc.devops.core.detector;

import com.sgcc.devops.common.util.SSH;

public class F5Detector implements Detector {
	private String hostname;
	private String username;
	private String hostSecure;

	public F5Detector(String hostname, String username, String hostSecure) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.hostSecure = hostSecure;
	}

	@Override
	public boolean normal() {
		SSH ssh = new SSH(hostname, username, hostSecure);
		if (ssh.connect()) {
			return true;
		} else {
			return false;
		}
	}

}
