package com.sgcc.devops.core.task;

import java.util.concurrent.Callable;

import com.github.dockerjava.api.DockerClient;

/**
 * date：2015年8月26日 下午5:02:34 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContanierTrashTask.java description：
 */
public class RemoveImageTask implements Callable<Boolean> {

	private DockerClient client;
	private String imageId;

	/**
	 * 
	 * @param client
	 * @param imageId
	 */
	public RemoveImageTask(DockerClient client, String imageId) {
		super();
		this.client = client;
		this.imageId = imageId;
	}

	@Override
	public Boolean call() throws Exception {
		try {
			client.removeImageCmd(imageId).exec();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
