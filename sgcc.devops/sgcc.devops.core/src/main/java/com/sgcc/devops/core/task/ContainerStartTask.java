package com.sgcc.devops.core.task;

import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.sgcc.devops.common.util.JSONUtil;

import net.sf.json.JSONObject;

/**
 * date：2015年8月26日 下午5:01:26 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerStartTask.java description：
 */
public class ContainerStartTask implements Callable<JSONObject> {

	private DockerClient client;
	private String containerId;
	private Logger logger = Logger.getLogger(ContainerStartTask.class);

	public ContainerStartTask(DockerClient client, String containerId) {
		super();
		this.client = client;
		this.containerId = containerId;
	}

	@Override
	public JSONObject call() throws Exception {
		try {
			client.startContainerCmd(containerId).exec();
			return containerInfo(client, containerId);
			// return true;
		} catch (Exception e) {
			logger.error("Start container error"+e.getMessage(), e);
			return null;
			// return false;
		}
	}

	/**
	 * @author langzi
	 * @param containerId
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	private JSONObject containerInfo(DockerClient client, String containerId) {
		JSONObject jo = new JSONObject();
		if (!"".equals(containerId)) {
			boolean start = false;
			while (!start) {
				List<Container> containers = client.listContainersCmd().withShowAll(true).exec();
				int flag = 0;
				for (Container container : containers) {
					if (container.getId().equals(containerId)) {
						jo = JSONUtil.parseObjectToJsonObject(container);
						start = true;
						break;
					}
					flag++;
				}
				if (flag >= containers.size()) {
					break;
				}
			}
		}
		return jo;
	}

}
