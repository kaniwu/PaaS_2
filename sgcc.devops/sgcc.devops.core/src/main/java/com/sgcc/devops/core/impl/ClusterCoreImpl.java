package com.sgcc.devops.core.impl;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.core.intf.ClusterCore;
import com.sgcc.devops.core.util.DockerHostInfo;
import com.sgcc.devops.core.util.HostInfo;
import com.sgcc.devops.core.util.TypeUtils;

/**
 * @author luogan 2015年8月17日 下午4:34:37
 */
@Component
public class ClusterCoreImpl implements ClusterCore {
	private static Logger logger = Logger.getLogger(ClusterCore.class);

	@Override
	public String connectClusterHost(String hostIp, String hostName, String hostPwd,String hostPort) {
		String classname = TypeUtils.getClassname("1");
		try {
			logger.info("Connect cluster host success");
			return ((HostInfo) Class.forName(classname).newInstance()).hostUUID(hostIp, hostName, hostPwd,hostPort);
		} catch (Exception e) {
			logger.error("Connect cluster host fail");
			return null;
		}
	}

	@Override
	public boolean checkAndStartSwarm(String ip, String username, String password, String hostPort,String port, String file) {
		DockerHostInfo host = new DockerHostInfo();
		boolean swarmExist = host.checkSwarm(ip, username, password,hostPort);
		if (swarmExist) {
			logger.info("Connect cluster Swarm success");
			String path = "/home/" + username + "/" + file;
			return host.startSwarm(ip, username, password,hostPort, port, path);
		} else {
			logger.warn("Swarm do not exist!!!!");
			return false;
		}
	}

	@Override
	public boolean startSwarm(String ip, String username, String password, String hostPort,String port, String file) {
		DockerHostInfo host = new DockerHostInfo();
		String path = "/home/" + username + "/" + file;
		logger.info("Connect start Swarm success");
		return host.startSwarm(ip, username, password,hostPort, port, path);
	}

	@Override
	public boolean stopSwarm(String ip, String username, String password,String hostPort, String port) {
		DockerHostInfo host = new DockerHostInfo();
		boolean swarmExist = host.checkSwarm(ip, username, password,hostPort);
		if (swarmExist) {
			logger.info("Swarm exist");
			return host.stopSwarm(ip, username, password,hostPort, port);
		} else {
			logger.error("Can not connect to swarm host");
			return false;
		}
	}
	@Override
	public boolean checkSwarm(String ip, String username, String password,String hostPort) {
		DockerHostInfo host = new DockerHostInfo();
		boolean swarmExist = host.checkSwarm(ip, username, password,hostPort);
		return swarmExist ;
	}

}
