/**
 * 
 */
package com.sgcc.devops.core.intf;

import com.github.dockerjava.api.DockerClient;
import com.sgcc.devops.common.model.DeployModel;

import net.sf.json.JSONObject;

/**
 * date：2015年9月25日 上午9:03:26 project name：sgcc.devops.core
 * （目前不用）
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：SystemCore.java description：
 */
public interface SystemCore {

	/**
	 * 应用部署（目前不用）
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年9月25日
	 */
//	public JSONObject baseDeploy(DeployModel deployModel, JSONObject databaseJO, JSONObject registryhostJO,
//			JSONObject clusterhostJO, JSONObject imageJO);
	/**
	* 
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年4月21日
	 */
	public boolean imageIsExist(DockerClient client, String newImageName);
	
	public DockerClient getDockerClient(String clusterHostIp, String clusterPort);
	/**
	* 获取容器信息
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年4月21日
	 */
	public JSONObject containerInfo(DockerClient dockerClient, String substring);

}
