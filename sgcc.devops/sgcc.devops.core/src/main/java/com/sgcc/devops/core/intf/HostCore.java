package com.sgcc.devops.core.intf;

import java.util.Map;

import com.sgcc.devops.common.util.SSH;

/**
 * @author luogan 2015年8月17日 下午2:44:04
 */
public interface HostCore {

	/**
	 * 连接服务器
	 * 
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public String connect(String hostIp, String hostName, String hostPwd,String hostPort, Integer hostType);
	
	/**
	 * 检查docker的信息
	 * docker info运行状态信息
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public String dockerInfo(String ip, String account, String password,String hostport);

	/**
	 * 检查docker端口
	 * 
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public String queryPort(String ip, String account, String password,String hostport);

	/**
	 * 检查swarm的信息
	 * 
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public String clusterHealthCheck(String ip, String account, String password,String hostport, String port);

	/**
	 * 连接服务器
	 * 
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public SSH getSSHConnection(String hostIp, String hostPwd, String hostport,String type);

	/**
	 * 连接服务器
	 * 
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public SSH sshConnection(String hostIp, String name, String hostPwd,String hostport, String fileName, String imageName);

	/**主机放置到集群中
	 * @param hostIp 集群管理节点主机IP
	 * @param name 集群管理节点主机用户名
	 * @param hostPwd 集群管理节点主机密码
	 * @param ips 需要增加的Docker宿主机ID
	 * @param path 集群管理文件名称
	 * @return
	 */
	public boolean putToCluster(String hostIp, String name, String hostPwd,String hostport, String ips, String path);

	/**集群配置文件的更新，
	 * @param hostIp 集群管理节点主机IP
	 * @param name 集群管理节点主机用户名
	 * @param hostPwd 集群管理节点主机密码
	 * @param ips 集群配置文件中需要增加的主机内容
	 * @param path 集群管理文件的路径
	 * @return
	 */
	public boolean updateCluster(String hostIp, String name, String hostPwd,String hostport, String ips, String path);

	/**
	 * 获取硬盘信息
	 * 
	 * @return
	 */
	public Map<String, Long> getDiskInfo(String poolUuid);

	/**
	* 停止主机上的docker 容器（不建议直接到主机上停止容器）
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年1月20日
	*/
	public String stopFromHost(String hostIp, String hostUser, String decrypt,String hostport,
			String conIds);

	/**
	* 删除主机上的容器（不建议直接到主机上删除容器）
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年1月20日
	*/
	public String removeFromHost(String hostIp, String hostUser,
			String decrypt,String hostport, String string);
	/**
	* 主机是否安装docker接口
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年1月20日
	*/
	public boolean dockerInstalled(String hostIp, String hostUser,String pwd,String hostport);

	/**
	* 停止主机docker接口
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年3月9日
	*/
	public boolean stopDocker(String hostIp, String hostUser, String decrypt,String hostPort);

	/**
	* 启动主机docker
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年3月9日
	*/
	public String startDocker(String hostIp, String hostUser, String decrypt, String hostPort);

	/**
	 * 启动swarm
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年3月23日
	*/
	public String startSwarm(String hostIp, String hostUser, String decrypt, String hostPort,
			String clusterPort,String config);

	/**
	* 停止swarm
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年3月23日
	*/
	public boolean stopSwarm(String hostIp, String hostUser, String decrypt,String hostPort,String port);
	
	/**
	 * @author chengyong
	 * 上传安装包
	 * 并安装安装包里面的所有文件
	 * @param ip
	 * @param name
	 * @param password
	 * @param file  本地的文件(地址）
	 * @param hostFileLocation 复制的文件存放地址
	 * @param hostFileName  复制文件名
	 * @return
	 */
	public boolean installAllFile(String ip, String name, String password, String hostPort,String file, String hostFileLocation,
			String hostFileName) ;
	/**
	 * 查询主机已安装软件版本
	 * command：查询安装版本脚本命令
	 * 返回：空值未安装；不为空，则返回结果。
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月5日
	 */
	public String installedInfo(String ip,String name,String pwd,String hostPort,String command,Long timeout);
	/**
	 * 主机安装软件/升级软件
	 * 参数：主机ip,主机用户，登录密码，安装包本地存放路径，安装包上传到主机上的路径，安装执行脚本命令/升级执行脚本命令
	 * 返回：执行的结果
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月5日
	 */
	public String hostInstallSoftware(SSH ssh,String hostIp, String hostUser, String decrypt,String hostport,String localHostPath,String fileName,String localFilePath,String remoteFilePath,String command,Long timeout);
	
	public String getDockerVersion(String hostIp, String hostUser, String decrypt,String hostPort);
	
	public String getSwarmVersion(String hostIp, String hostUser, String decrypt,String hostPort);
	
	public String getNginxVersion(String hostIp, String hostUser, String decrypt,String hostPort);
	
	public String getRegistryVersion(String hostIp, String hostUser, String decrypt,String hostPort);
	
	public String updateAndStartDocker(String ip,String name,String pwd,String hostPort,String registry,String labels,String dockerParam);
}
