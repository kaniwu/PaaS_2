package com.sgcc.devops.core.detector;

import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.xbill.DNS.ARecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TSIG;
import org.xbill.DNS.Update;
import org.xbill.DNS.ZoneTransferIn;

public class DNSDetector implements Detector {
	
	private static Logger logger = Logger.getLogger(DNSDetector.class);
	
	private String ip;
	private int port = 53;
	private String key;
	private String secret;
	private String zone;
	
	public DNSDetector(String ip,int port,String key,String secret,String zone){
		this.ip = ip;
		this.port = port;
		this.key = key;
		this.secret = secret;
		this.zone = zone;
	}
	
	
	@SuppressWarnings("finally")
	@Override
	public boolean normal() {
	   boolean canConnect = true;
	   try {
		    String code = "testpaas";
		    String hostIp = this.ip;
		    Name name = Name.fromString(zone);  
	        Name host = Name.fromString(code, name);  
	        Update update = new Update(name, DClass.IN);  
	        Record record = new ARecord(host, DClass.IN, 3600, InetAddress.getByName(hostIp));  
	        update.add(record);  
	        Resolver resolver = new SimpleResolver(this.ip);  
	        resolver.setPort(this.port);  
	        TSIG tsig = new TSIG(this.key, this.secret);  
	        resolver.setTSIGKey(tsig);  
	        resolver.setTCP(true);  
	        Message response = resolver.send(update);  
	  
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("DNS connection test exception:", e);
			e.printStackTrace();
			canConnect = false;
		}finally {
			return canConnect;
		}  
	}


	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public int getPort() {
		return port;
	}


	public void setPort(int port) {
		this.port = port;
	}


	public String getKey() {
		return key;
	}


	public void setKey(String key) {
		this.key = key;
	}


	public String getSecret() {
		return secret;
	}


	public void setSecret(String secret) {
		this.secret = secret;
	}


	public String getZone() {
		return zone;
	}


	public void setZone(String zone) {
		this.zone = zone;
	}

}
