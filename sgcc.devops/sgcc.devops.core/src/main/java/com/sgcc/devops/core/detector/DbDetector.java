package com.sgcc.devops.core.detector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 * 数据库联通性测试组件
 * 
 * @author dmw
 *
 */
public class DbDetector implements Detector {

	private static Logger logger = Logger.getLogger(DbDetector.class);
	private String url;
	private String username;
	private String hostSecure;
	private String driver;

	public DbDetector(String url, String username, String hostSecure, String driver) {
		super();
		this.url = url;
		this.username = username;
		this.hostSecure = hostSecure;
		this.driver = driver;
	}

	@Override
	public boolean normal() {
		Connection connection = null;
		try {
			Class.forName(driver);
			connection = DriverManager.getConnection(url, username, hostSecure);
			return true;
		} catch (ClassNotFoundException | SQLException e) {
			logger.error("DB connection test exception:", e);
			return false;
		}finally{
			if(null!=connection){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
