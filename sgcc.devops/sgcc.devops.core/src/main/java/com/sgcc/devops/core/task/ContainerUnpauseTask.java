package com.sgcc.devops.core.task;

import java.util.concurrent.Callable;

import com.github.dockerjava.api.DockerClient;

/**
 * date：2015年8月26日 下午10:12:27 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerUnpauseTask.java description：
 */
public class ContainerUnpauseTask implements Callable<Boolean> {

	private DockerClient client;
	private String containerId;

	/**
	 * Constructor
	 * 
	 * @author liangzi
	 * @param client
	 * @param containerId
	 * @version 1.0 2015年8月26日
	 */

	public ContainerUnpauseTask(DockerClient client, String containerId) {
		this.client = client;
		this.containerId = containerId;
	}

	@Override
	public Boolean call() throws Exception {
		try {
			client.unpauseContainerCmd(containerId).exec();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
