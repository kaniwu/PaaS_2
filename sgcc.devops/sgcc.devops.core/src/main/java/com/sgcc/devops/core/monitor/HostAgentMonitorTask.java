package com.sgcc.devops.core.monitor;

import java.util.concurrent.Callable;

import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Monitor;

public class HostAgentMonitorTask implements Callable<MonitorResult> {

	private HostMonitorClient client;
	private HostMonitorParam param;

	public HostAgentMonitorTask() {
		super();
		client = new HostMonitorClient();
		client.setHttpClient(new HttpClient());
	}

	public HostAgentMonitorTask(HostMonitorParam param) {
		super();
		client = new HostMonitorClient();
		client.setHttpClient(new HttpClient());
		this.param = param;
	}

	@Override
	public MonitorResult call() throws Exception {
		Monitor data = client.monitor(param);
		if (null == data) {
			return new MonitorResult(false, "get monitor info error", null);
		}
		return new MonitorResult(true, "success", data);
	}

}
