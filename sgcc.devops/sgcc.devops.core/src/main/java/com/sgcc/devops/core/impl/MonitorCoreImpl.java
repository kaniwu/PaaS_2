package com.sgcc.devops.core.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sgcc.devops.core.intf.MonitorCore;
import com.sgcc.devops.core.monitor.ContainerMonitorClient;
import com.sgcc.devops.core.monitor.ContainerMonitorParam;
import com.sgcc.devops.core.monitor.HostMonitorClient;
import com.sgcc.devops.core.monitor.HostMonitorParam;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Monitor;

/**
 * @author dmw
 *
 */
@Service("monitorCore")
public class MonitorCoreImpl implements MonitorCore {

	@Autowired
	private HttpClient httpClient;

	@Override
	public Monitor containerState(String conId, String clusterIp, String clusterPort, String container) {
		ContainerMonitorClient client = new ContainerMonitorClient();
		client.setHttpClient(httpClient);
		ContainerMonitorParam param = new ContainerMonitorParam(conId, clusterIp, clusterPort, container);
		return client.monitor(param);
	}

	@Override
	public Monitor hostState(String ip, String port) throws Exception {
		HostMonitorClient client = new HostMonitorClient();
		client.setHttpClient(httpClient);
		HostMonitorParam param = new HostMonitorParam(null, ip, port);
		return client.monitor(param);
	}
}
