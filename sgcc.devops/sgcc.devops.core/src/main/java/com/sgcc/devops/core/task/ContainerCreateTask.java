package com.sgcc.devops.core.task;

import java.util.concurrent.Callable;

import com.sgcc.devops.common.model.ContainerModel;

/**
 * date：2015年8月26日 下午5:03:31 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerCreateTask.java description：
 */
public class ContainerCreateTask implements Callable<Boolean> {

	private String hostIp;
	private String hostName;
	private String hostPassword;
	private String clusterPort;
	private ContainerModel model;

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostPassword() {
		return hostPassword;
	}

	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}

	public String getClusterPort() {
		return clusterPort;
	}

	public void setClusterPort(String clusterPort) {
		this.clusterPort = clusterPort;
	}

	public ContainerModel getModel() {
		return model;
	}

	public void setModel(ContainerModel model) {
		this.model = model;
	}

	/**
	 * Constructor
	 * 
	 * @author liangzi
	 * @param client
	 * @param containerId
	 * @version 1.0 2015年8月26日
	 */
	public ContainerCreateTask(String hostIp, String hostName, String hostPassword, String clusterPort,
			ContainerModel model) {
		this.hostIp = hostIp;
		this.hostName = hostName;
		this.hostPassword = hostPassword;
		this.clusterPort = clusterPort;
		this.model = model;
	}

	@Override
	public Boolean call() throws Exception {
		return false;
	}

}
