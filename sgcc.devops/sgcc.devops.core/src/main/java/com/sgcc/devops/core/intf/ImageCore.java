package com.sgcc.devops.core.intf;

import java.util.List;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.SimpleModel;
import com.sgcc.devops.core.util.OperationResult;

/**
 * 镜像制作核心接口，用户登录到仓库主机进行镜像的相关操作，主要包括镜像的制作，打标和发布
 * 
 * @author dmw
 * @version 1.0
 * @since 1.0
 */
public interface ImageCore {
	/**
	 * 制作镜像，SSH登录仓库主机并执行镜像制作脚本
	 * 
	 * @param ip
	 *            仓库主机IP
	 * @param name
	 *            仓库主机用户名
	 * @param password
	 *            登录密码
	 * @param fileName
	 *            需要解压的镜像文件名
	 * @param imageName
	 *            镜像名称
	 * @param tag
	 *            镜像版本
	 * @return 镜像是否制作成功，成功后返回的message值代表镜像的UUID且success = true；否则succes =
	 *         false，且message值为失败的原因
	 */
	public abstract Result makeImage(String ip, String name, String password, String hostport,String fileName, String imageName,String tag);

	/**
	 * 镜像打标 ：SSH登录仓库主机并执行镜像打标脚本
	 * 
	 * @param ip
	 *            主机IP
	 * @param name
	 *            用户名
	 * @param password
	 *            登录密码
	 * @param source
	 *            源镜像
	 * @param target
	 *            目标镜像
	 * @return 镜像是否打标成功，成功后返回success = true；否则succes = false，且message值为失败的原因
	 */
	public abstract Result tagImage(String masterIp, String name, String password,String hostport, String source, String target);

	/**
	 * 镜像发布：SSH登录仓库主机，并执行镜像发布脚本
	 * 
	 * @param ip
	 *            主机IP
	 * @param name
	 *            用户名
	 * @param password
	 *            登录密码
	 * @param image
	 *            镜像全称
	 * @return 镜像发布是否成功，成功后返回success = true；否则succes = false，且message值为失败的原因
	 */
	public abstract Result pushImage(String masterIp, String name, String password,String hostport, String image);

	public OperationResult removeImages(List<SimpleModel> simpleModels) ;
}
