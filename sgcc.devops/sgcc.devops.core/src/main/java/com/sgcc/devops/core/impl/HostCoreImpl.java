package com.sgcc.devops.core.impl;

import java.io.File;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.core.util.DockerHostInfo;

/**
// * @author luogan 2015年8月17日 下午4:34:49
 */
@Component
public class HostCoreImpl implements HostCore {
	private static Logger logger = Logger.getLogger(HostCore.class);
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Override
	public String connect(String hostIp, String hostName, String hostPwd,String hostPort, Integer hostType) {
		try {
			String response = null;
			DockerHostInfo dockerHost = new DockerHostInfo();
			if (hostType == 3) {
			response = dockerHost.host(hostIp, hostName, hostPwd,hostPort);
		} else {
			response = dockerHost.UUID(hostIp, hostName, hostPwd,hostPort);
		}
			if (response != null) {
				logger.info("Connect host success");
				return response;
			} else {
				logger.warn("Connect host fail");
				return null;
			}
		} catch (Exception e) {
			logger.error("Connect host fail", e);
			return null;
		}
	}

	

	@Override
	public SSH getSSHConnection(String hostIp, String hostPwd, String hostPort,String type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Long> getDiskInfo(String poolUuid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SSH sshConnection(String hostIp, String name, String hostPwd,String hostPort, String fileName, String imageName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String dockerInfo(String ip, String account, String password, String hostport) {
		DockerHostInfo host = new DockerHostInfo();
		logger.info("Query docker success");
		return host.checkDocker(ip, account, password,hostport);
	}

	@Override
	public String queryPort(String ip, String account, String password, String hostport) {
		DockerHostInfo host = new DockerHostInfo();
		logger.info("Query docker port success");
		return host.queryDockerPort(ip, account, password,hostport);
	}

	@Override
	public boolean putToCluster(String hostIp, String name, String hostPwd,String hostPort, String ips, String path) {
		String swarmCfgPath = "/home" + "/" + name + "/" + path;
		try {
			DockerHostInfo dockerHost = new DockerHostInfo();
			StringBuffer command = new StringBuffer();
			command.append("sudo echo -e").append(" '").append(ips).append("'").append(">>").append(swarmCfgPath);
			dockerHost.execCommandWithoutResult(hostIp, name, hostPwd, hostPort,command.toString());
			logger.info("Connect host and add cluster config success");
			return true;
		} catch (Exception e) {
			logger.error("Connect host and add cluster config fail", e);
			return false;
		}
	}

	@Override
	public boolean updateCluster(String hostIp, String name, String hostPwd, String hostPort,String ips, String path) {
		String swarmCfgPath = "/home" + "/" + name + "/" + path;
		try {
			DockerHostInfo dockerHost = new DockerHostInfo();
			StringBuffer command = new StringBuffer();
			if(StringUtils.hasText(ips)){
				command.append("sudo echo -e").append(" '").append(ips).append("'").append(">").append(swarmCfgPath);
			}else{
				command.append("sudo echo -n").append(" '").append(ips).append("'").append(">").append(swarmCfgPath);
			}
			boolean b = dockerHost.execCommandWithoutResult(hostIp, name, hostPwd,hostPort, command.toString());
			logger.info("Connect host and remove cluster config success");
			return b;
		} catch (Exception e) {
			logger.info("Connect host and remove cluster config fail");
			return false;
		}
	}

	@Override
	public String clusterHealthCheck(String ip, String account, String password,String hostPort, String clusterPort) {
		DockerHostInfo host = new DockerHostInfo();
		logger.info("Query cluster list success");
		return host.clusterHealthCheck(ip, account, password, hostPort,clusterPort);
	}
	
	public String stopFromHost(String ip,String username,String password,String hostPort,String conIds){
		DockerHostInfo host = new DockerHostInfo();
		StringBuffer queryCommand = new StringBuffer("docker stop ");
		queryCommand.append(conIds);
		String result = host.execCommandReq(ip, username, password,hostPort, queryCommand.toString());
		return result;
	}
	public String removeFromHost(String ip,String username,String password,String hostPort, String conIds){
		DockerHostInfo host = new DockerHostInfo();
		StringBuffer queryCommand = new StringBuffer("docker rm -f ");
		queryCommand.append(conIds);
		String result = host.execCommandReq(ip, username, password, hostPort,queryCommand.toString());
		return result;
	}
	public static void main(String args[]){
		HostCoreImpl coreImpl = new HostCoreImpl();
		String dockerinfo = coreImpl.dockerInfo("192.168.49.136", "root", "123456","22");
		System.out.println(dockerinfo);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#dockerInstalled(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean dockerInstalled(String ip, String username, String password,String hostPort) {
		DockerHostInfo host = new DockerHostInfo();
		return host.dockerInstalled(ip, username, password,hostPort);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#stopDocker(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean stopDocker(String hostIp, String hostUser, String decrypt,String hostPort) {
		DockerHostInfo host = new DockerHostInfo();
		return host.stopDocker(hostIp, hostUser, decrypt,hostPort);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#startDocker(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String startDocker(String hostIp, String hostUser, String decrypt,String hostPort) {
		DockerHostInfo host = new DockerHostInfo();
		return host.startDocker(hostIp, hostUser, decrypt,hostPort);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#startSwarm(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String startSwarm(String hostIp, String hostUser, String decrypt,String hostPort,
			String clusterPort,String config) {
		String command = "nohup swarm manage -H tcp://" + hostIp + ":" + clusterPort + " file://" + config + ">>" + config + ".log 2>&1&";
		try {
			DockerHostInfo dockerHost = new DockerHostInfo();
			String result = dockerHost.execCommandWithTimeOut(hostIp, hostUser, decrypt,hostPort, command,10000L);
			return result;
		} catch (Exception e) {
			return e.toString();
		}
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#stopSwarm(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public boolean stopSwarm(String hostIp, String hostUser, String decrypt,String hostPort,String port) {
		DockerHostInfo host = new DockerHostInfo();
		return host.stopSwarm(hostIp, hostUser, decrypt,hostPort, port);
	}

	@Override
	public boolean installAllFile(String ip, String name, String password,String hostport,
			String file, String hostFileLocation, String hostFileName) {
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		//在host上创建文件地址
		String result =dockerHostInfo.createFileLocation(ip, name, password,hostport,hostFileLocation);
        if(null==result){
        	return false;
        }
		//上传文件,不做判断，因为上传失败也可能安装成功（安装包已经存在）
		dockerHostInfo.uploadFile(ip, name, password,hostport,file,hostFileLocation,hostFileName);
		//安装文件
		String installResult =dockerHostInfo.installAllFile(ip, name, password,hostport,hostFileLocation,hostFileName);
		if(null==installResult){
			return false;
		}
		return true;
	}
	/**
	 * 主机安装软件/升级软件
	 * 参数：主机ip,主机用户，登录密码，安装包本地存放路径，安装包上传到主机上的路径，安装执行脚本命令/升级执行脚本命令
	 * 返回：执行的结果
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月5日
	 */
	public String hostInstallSoftware(SSH ssh,String hostIp, String hostUser, String decrypt,String hostport,
			String localHostPath,String fileName,String localFilePath,String remoteFilePath,String command,Long timeout){
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		//在host上创建文件地址
		String result =dockerHostInfo.createFileLocation(hostIp, hostUser, decrypt,hostport,remoteFilePath);
        if(!StringUtils.isEmpty(result)){
        	return "主机【"+hostIp+"】创建远程文件目录出错："+result;
        }
        String FilePath =(localHostPath+"/"+fileName).replace("\\", "/").replace("//", "/");
        File to = new File(FilePath);
        //如果本地有，不需要重复下载
        if(to.exists()&&to.isFile()){
        }else{
        	if(tmpHostConfig.isTransferNeeded()){
        		try {
        			//拷贝到本地
        			File temFile = new File(localHostPath);
        			if(!temFile.exists()){
        				temFile.mkdirs();
        			}
        			if(ssh.connect()){
        				boolean b = ssh.GetFile(localFilePath, localHostPath);
        				ssh.close();
            			if(!b){
            				return "文件远程拷贝到本地失败";
            			}
    				}else{
    	        		return "中转主机连接失败！";
    	        	}
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
            }
        }
		//上传文件,不做判断，因为上传失败也可能安装成功（安装包已经存在）
		boolean b = dockerHostInfo.uploadFile(hostIp, hostUser, decrypt,hostport,localHostPath,remoteFilePath,"");
		if(!b){
			return "向主机【"+hostIp+"】拷贝安装文件出错";
		}
		//安装文件
		String commandNew = "cd " + remoteFilePath + "\n" +command+"\n echo $?";
		result = dockerHostInfo.execCommandWithTimeOut(hostIp, hostUser, decrypt,hostport, commandNew,timeout);
		result = result.replaceAll("\n", "").trim();
		if(result.substring(result.length()-1).equals("0")){
			return "执行成功";
		}else{
			return result;
		}
	}

	/**
	 * 查询主机已安装软件版本、运行状态
	 * command：查询安装版本脚本命令
	 * 返回：空值未安装；不为空，则返回结果。
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月5日
	 */
	@Override
	public String installedInfo(String ip, String name, String pwd,String hostPort,
			String command,Long timeout) {
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		return dockerHostInfo.execCommandWithTimeOut(ip, name, pwd,hostPort, command,timeout);
	}

	@Override
	public String getDockerVersion(String hostIp, String hostUser,
			String decrypt,String hostPort) {
		// TODO 自动生成的方法存根
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		String result = dockerHostInfo.getVersion(hostIp, hostUser, decrypt, hostPort,"docker -v");
		if(org.apache.commons.lang.StringUtils.isEmpty(result)){
			return "取得版本失败";
		}else if(result.contains("command not found")){
			return "未安装";
		}
		
		return result;
	}

	@Override
	public String getSwarmVersion(String hostIp, String hostUser, String decrypt,String hostPort) {
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		String result = dockerHostInfo.getVersion(hostIp, hostUser, decrypt,hostPort, "swarm -v");
		if(org.apache.commons.lang.StringUtils.isEmpty(result)){
			return "取得版本失败";
		}else if(result.contains("command not found")){
			return "未安装";
		}
		return result;
	}

	@Override
	public String getNginxVersion(String hostIp, String hostUser, String decrypt,String hostPort) {
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		String result = dockerHostInfo.getVersion(hostIp, hostUser, decrypt,hostPort, "nginx -v");
		if(org.apache.commons.lang.StringUtils.isEmpty(result)){
			return "取得版本失败";
		}else if(result.contains("command not found")){
			return "未安装";
		}
		return result;
	}

	@Override
	public String getRegistryVersion(String hostIp, String hostUser,
			String decrypt,String hostPort) {
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		String result = dockerHostInfo.getVersion(hostIp, hostUser, decrypt, hostPort,"rpm -qa|grep registry");
		if(org.apache.commons.lang.StringUtils.isEmpty(result)){
			return "取得版本失败";
		}else if(result.contains("command not found")){
			return "未安装";
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.HostCore#updateAndStartDocker(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String updateAndStartDocker(String ip, String name, String pwd,String hostPort,String registry,String labels,String dockerParam) {
		//更新docker启动配置
		DockerHostInfo dockerHostInfo = new DockerHostInfo();
		//删除原来的执行命令
		StringBuffer ExecStartCommand = new StringBuffer("sudo sed -i '/^ExecStart/'d /usr/lib/systemd/system/docker.service ;");
		//添加一条执行命令
		ExecStartCommand.append("sudo sed -i '9i ExecStart=/usr/bin/docker daemon -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock ");
		if(!StringUtils.isEmpty(labels)){
			ExecStartCommand.append(" "+labels+" ");
		}
		if(!StringUtils.isEmpty(dockerParam)){
			ExecStartCommand.append(" "+dockerParam+" ");
		}
		if(!StringUtils.isEmpty(registry)){
			ExecStartCommand.append("--insecure-registry "+registry+"' /usr/lib/systemd/system/docker.service;");
		}else{
			ExecStartCommand.append(" ' /usr/lib/systemd/system/docker.service;");
		}
		ExecStartCommand.append("sudo systemctl daemon-reload");
		boolean b = dockerHostInfo.execCommandWithoutResult(ip, name, pwd,hostPort,ExecStartCommand.toString());
		if(b){
			//重启docker
			String result = this.startDocker(ip, name, pwd,hostPort);
			//0,成功；1，失败
			return result;
		}else{
			return "执行失败";
		}
	}
}
