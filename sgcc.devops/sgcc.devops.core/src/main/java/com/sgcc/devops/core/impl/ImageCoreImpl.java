package com.sgcc.devops.core.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DockerClientBuilder;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.model.SimpleModel;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.core.intf.ImageCore;
import com.sgcc.devops.core.task.RemoveImageTask;
import com.sgcc.devops.core.util.DockerHostInfo;
import com.sgcc.devops.core.util.OperationResult;

/**
 * 镜像制作核心接口实现类，用户登录到仓库主机进行镜像的相关操作，主要包括镜像的制作，打标和发布
 * 
 * @author dmw
 * @version 1.0
 */
@Component
public class ImageCoreImpl implements ImageCore {
	private static Logger logger = Logger.getLogger(HostCore.class);
	@Resource
	private HostCore hostCore;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private DockerHostConfig dockerHostConfig;
	@Resource
	private LocalConfig localConfig;
	@Override
	public Result makeImage(String ip, String name, String password, String hostport,String fileName, String imageName,
			String imageTag) {
		String baseImagePath = tmpHostConfig.getBaseImagePath();
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		if(ssh.connect()){
			try {
				ssh.execute("mkdir -p "+baseImagePath, 5000);
				ssh.close();
			} catch (Exception e) {
				ssh.close();
				e.printStackTrace();
			}
		}
		if(tmpHostConfig.isTransferNeeded()){
			String localpath = localConfig.getLocalBaseImagePath()+"/"+fileName;
			localpath = localpath.replace("\\", "/").replace("//", "/");
			if (ssh.connect()) {
				boolean pushSuccess = ssh.SCPFile(localpath, tmpHostConfig.getBaseImagePath());
				if (pushSuccess) {
					File file = null;
					try {
						file = new File(localpath);
						if (file.exists())
							file.delete();
					} catch (Exception e) {
						logger.error("Delete file exception:", e);
					}
				}
				ssh.close();
			}else{
				return new Result(false, "中转主机连接失败！");
			}
		}else{
			baseImagePath = localConfig.getLocalBaseImagePath();
		}
		StringBuffer commandStr = new StringBuffer();
		
		commandStr.append("cd ").append(baseImagePath).append(";");
		if (fileName.contains(".tar")) {
			commandStr.append("sudo docker load --input " + baseImagePath + fileName + ";");
			commandStr.append(
					"rm -rf " + baseImagePath + fileName.substring(0, fileName.length() - 4) + "*;");
		} else {
			logger.error("制作镜像异常：不支持此类文件类型！" + fileName);
			return new Result(false, "不支持此类文件类型！");
		}
		commandStr.append(" sudo docker images|grep ").append(imageName).append("|grep ").append(imageTag)
				.append("|awk '{print \"IMAGE_ID:\"$3}';");
		logger.debug(commandStr.toString());
		try {
			ssh.connect();
			String result = ssh.executeWithResult(commandStr.toString());
			logger.info("Make image result:" + result);
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
			String uuid = null;
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (line.contains("IMAGE_ID")) {
					uuid = line.split(":")[1];
					break;
				} else {
					continue;
				}
			}
			if (null == uuid) {
				return new Result(false, "镜像制作脚本执行异常:</br>执行命令："+commandStr+";</br>返回结果："+result);
			} else {
				return new Result(true, uuid);
			}
		} catch (Exception e) {
			logger.info("Make image fail");
			return new Result(false, "制作镜像失败：脚本执行异常！"+e);
		} finally {
			ssh.close();
		}
	}

	@Override
	public Result pushImage(String ip, String name, String password, String hostport,String image) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		if (ssh.connect()) {
			StringBuffer commandStr = new StringBuffer();
			commandStr.append("sudo docker push " + image);
			try {
				String result = ssh.executeWithResult(commandStr.toString());
				logger.info("Push image result:" + result);
				if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
					return new Result(false, "镜像发布脚本执行失败！");
				} else {
					return new Result(true, "镜像发布成功！");
				}
			} catch (Exception e) {
				logger.error("Push image fail", e);
				return new Result(false, "发布镜像失败：发布镜像脚本执行异常！");
			} finally {
				ssh.close();
			}
		} else {
			logger.warn("连接主机异常！可能是用户名或密码错误！");
			return new Result(false, "镜像发布失败：连接仓库主机异常！");
		}
	}

	@Override
	public Result tagImage(String ip, String name, String password, String hostport,String source, String target) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		if (ssh.connect()) {
			StringBuffer command = new StringBuffer();
			command.append("sudo docker tag ").append(source).append(" ").append(target);
			try {
				String result = ssh.executeWithResult(command.toString());
				logger.info("tag image result:" + result);
				return new Result(true, "镜像打标成功！");
			} catch (Exception e) {
				logger.error("Tag image fail", e);
				return new Result(false, "镜像打标失败：脚本执行异常！");
			} finally {
				ssh.close();
			}
		} else {
			logger.warn("镜像打标失败：仓库主机无法连接！");
			return new Result(false, "镜像打标失败：仓库主机无法连接！");
		}
	}
	public OperationResult removeImages(List<SimpleModel> simpleModels) {
		OperationResult operationResult = new OperationResult();
		try {
			if (simpleModels != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<Boolean> comp = new ExecutorCompletionService<>(executor);
				for (SimpleModel simpleModel : simpleModels) {
					DockerClient client = getDockerClient(simpleModel.getClusterIp(), simpleModel.getClusterPort());
					comp.submit(new RemoveImageTask(client, simpleModel.getUuid()));
				}
				executor.shutdown();
				int index = 0;
				List<SimpleModel> sucessModels = new ArrayList<SimpleModel>();
				long begingTime = System.currentTimeMillis();
				while (index < simpleModels.size()) {
					Future<Boolean> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get()) {
							sucessModels.add(simpleModels.get(index));
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						operationResult.setTimeOutMessage("容器删除超时！");
						continue;
					}
				}
				if (sucessModels.size() == simpleModels.size()) {
					operationResult.setFlag(true);
				} else {
					operationResult.setFlag(false);
				}
				operationResult.setSuccessModels(sucessModels);
			}
			logger.info("Remove container success:operate server success");
		} catch (Exception e) {
			logger.error("Remove container error:operate server error", e);
		}
		return operationResult;
	}
	public DockerClient getDockerClient(String clusterIp, String clusterPort) {
		String url = "http://" + clusterIp + ":" + clusterPort;
		return DockerClientBuilder.getInstance(url).build();
	}
}
