package com.sgcc.devops.core.util;

/**
 * @author luogan 2015年5月19日 下午5:52:57
 */
public class DockerHostInfo extends HostInfo {
	//查主机内核、cpu、内存、ID
	@Override
	protected String command() {
		return "sudo docker info | grep -E \"CPUs|ID|Total Memory|Kernel Version\"";
	}
	//查docker进程
	@Override
	protected String valid() {
		return "ps -aux | grep \"docker\"";
	}
	//启动swarm，参数：集群IP，集群端口，配置文件
	@Override
	protected String commandStartSwarm(String ip, String port, String path) {
		return "nohup swarm manage -H tcp://" + ip + ":" + port + " file://" + path + ">>" + path + ".log 2>&1&";
	}
	@Override
	protected String clusterCommand() {
		return "sudo docker info | grep -E \"CPUs|ID|Total Memory\"";
	}

	public DockerHostInfo() {
		super();
	}

	@Override
	protected String dockerInfoCommand() {
		return "sudo export LANG=en_US;docker info";
	}

	@Override
	protected String queryCommand() {
		return "uname -a|awk '{print \"Kernel:\"$3}';cat /proc/cpuinfo |grep 'processor'|uniq|wc -l|awk '{print \"CPU:\"$1}';free|grep Mem|awk '{print \"MEM:\"$2/1024\"MiB\"}';cat /etc/redhat-release|awk '{print \"opration:\"$0}';";
	}

	@Override
	protected String queryDockerPortCommand() {
		return "ps -ef | grep docker| grep sock|awk '{print $10}'|awk -F ':' '{print $3}'";
	}
	@Override
	protected String commandStopDocker() {
		return "sudo systemctl stop docker;sudo ps -ef | grep docker| grep sock|awk '{print $2}'|xargs kill -9";
	}
	@Override
	protected String commandStartDocker() {
		return "sudo systemctl restart docker.service;echo $?";
	}
	@Override
	protected String commandStopSwarm(String port) {
		return "ps -ef |grep swarm|grep " + port + "|awk '{print $2}'|xargs kill -9";
	}
	@Override
	protected String clusterHealthCheckCommand(String ip, String port) {
		return "sudo docker -H tcp://" + ip + ":" + port
				+ " info|grep [0-9].[0-9].[0-9].[0-9]|awk '{print $2}'|awk -F ':' '{print $1}'";
	}
	@Override
	protected String dockerCommand(){
		return "sudo docker";
	}

	@Override
	protected String commandInstallAllFile(String hostFileLocation,
			String hostFileName) {
		return "cd "+hostFileLocation+hostFileName+"; rpm -ivh --replacepkgs ./* --nodeps --force";
	}
	@Override
	protected String commandCreateFileLocation(String hostFileLocation) {
		return "mkdir -p "+hostFileLocation;
	}

}
