package com.sgcc.devops.core.monitor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sgcc.devops.common.constant.Type.TARGET_TYPE;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Monitor;

@Repository("hostMonitorClient")
public class HostMonitorClient {

	@Autowired
	private HttpClient httpClient;

	private static String[] ignoreNetDevice = { "docker", "lo", "veth" };

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	public Monitor monitor(HostMonitorParam param) {
		String response = httpClient.get(null, assembleUrl(param));
		long start = System.currentTimeMillis();
		if (StringUtils.isEmpty(response)) {
			return null;
		}
		JSONObject firstResult = JSON.parseObject(response);
		response = httpClient.get(null, assembleUrl(param));
		if (StringUtils.isEmpty(response)) {
			return null;
		}
		long end = System.currentTimeMillis();
		BigDecimal gap = new BigDecimal(end - start).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP);
		JSONObject secondResult = JSON.parseObject(response);
		Double cpu = cpuUsage(secondResult.getJSONObject("CPU"));
		JSONObject memObject = secondResult.getJSONObject("MEM");
		BigDecimal useDecimal = memObject.getBigDecimal("Total").subtract(memObject.getBigDecimal("Available"));
		BigDecimal allDecimal = memObject.getBigDecimal("Total");
		Double memUsage = useDecimal.multiply(new BigDecimal(100)).divide(allDecimal, 2, RoundingMode.HALF_UP)
				.doubleValue();
		NetAmount first = netUsage(firstResult.getJSONArray("NET"));
		NetAmount second = netUsage(secondResult.getJSONArray("NET"));
		Long netIn = new BigDecimal(second.getIn() - first.getIn()).divide(gap, 2, RoundingMode.HALF_UP)
				.divide(new BigDecimal(1024), 2, RoundingMode.HALF_UP).longValue();
		Long netOut = new BigDecimal(second.getOut() - first.getOut()).divide(gap, 2, RoundingMode.HALF_UP)
				.divide(new BigDecimal(1024), 2, RoundingMode.HALF_UP).longValue();
		DiskSpace disk = diskUsage(secondResult.getJSONArray("DISK"));
		BigDecimal diskUsed = new BigDecimal(disk.getUsed() + "");
		BigDecimal diskTotal = new BigDecimal(disk.getTotal() + "");
		Double diskUsage = diskUsed.divide(diskTotal, 2, RoundingMode.HALF_UP).doubleValue();
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		Monitor monitor = new Monitor((byte) TARGET_TYPE.HOST.ordinal(), new Timestamp(new Date().getTime()),
				param.getId(), cpu.toString(), memUsage.toString(), netIn.toString(), netOut.toString(),
				diskUsage.toString());
		return monitor;
	}

	private boolean isIgnoreNetDevice(String deviceName) {
		if (ignoreNetDevice.length == 0) {
			return false;
		}
		for (String device : ignoreNetDevice) {
			if (deviceName.contains(device)) {
				return true;
			}
		}
		return false;
	}

	class DiskSpace {
		private Long used;
		private Long total;

		public Long getUsed() {
			return used;
		}

		public void setUsed(Long used) {
			this.used = used;
		}

		public Long getTotal() {
			return total;
		}

		public void setTotal(Long total) {
			this.total = total;
		}

		public DiskSpace(Long used, Long total) {
			super();
			this.used = used;
			this.total = total;
		}

	}

	/**
	 * 网络流量
	 * 
	 * @author dmw
	 *
	 */
	class NetAmount {
		private Long in;
		private Long out;

		public Long getIn() {
			return in;
		}

		public void setIn(Long in) {
			this.in = in;
		}

		public Long getOut() {
			return out;
		}

		public void setOut(Long out) {
			this.out = out;
		}

		public NetAmount(Long in, Long out) {
			super();
			this.in = in;
			this.out = out;
		}
	}

	private NetAmount netUsage(JSONArray jsonArray) {
		long in = 0l;
		long out = 0l;
		for (int i = 0; i < jsonArray.size(); i++) {
			String device = jsonArray.getJSONObject(i).getString("name");
			if (isIgnoreNetDevice(device)) {
				continue;
			}
			in += jsonArray.getJSONObject(i).getLongValue("read");
			out += jsonArray.getJSONObject(i).getLongValue("write");
		}
		return new NetAmount(in, out);
	}

	private DiskSpace diskUsage(JSONArray jsonArray) {
		long used = 0l;
		long total = 0l;
		for (int i = 0; i < jsonArray.size(); i++) {
			String use = jsonArray.getJSONObject(i).getString("Used");
			String all = jsonArray.getJSONObject(i).getString("Size");
			if (use.contains("G")) {
				used += Float.valueOf(use.substring(0, use.length() - 2)) * 1024;
			} else if (use.contains("M")) {
				used += Float.valueOf(use.substring(0, use.length() - 2));
			}
			if (all.contains("G")) {
				total += Float.valueOf(all.substring(0, all.length() - 2)) * 1024;
			} else if (all.contains("M")) {
				total += Float.valueOf(all.substring(0, all.length() - 2));
			}
		}
		return new DiskSpace(used, total);
	}

	private double cpuUsage(JSONObject json) {
		Long user = json.getLong("User");
		Long nice = json.getLong("Nice");
		Long system = json.getLong("System");
		Long idle = json.getLong("Idle");
		Long iowait = json.getLong("Iowait");
		Long irq = json.getLong("Irq");
		Long softirq = json.getLong("Softirq");
		Long stealstolen = json.getLong("Stealstolen");
		Long guest = json.getLong("Guest");
		Long total = user + nice + system+iowait+irq+softirq+stealstolen+guest+idle;
		BigDecimal useTime = new BigDecimal(total-idle+0);
		BigDecimal totalTime = new BigDecimal(total+0);
		return useTime.divide(totalTime, 4, RoundingMode.HALF_UP).multiply(new BigDecimal(100)).doubleValue();
	}

	private String assembleUrl(HostMonitorParam param) {
		return "http://" + param.getIp() + ":" + param.getPort() + "/all";
	}
}
