package com.sgcc.devops.core.monitor;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.stereotype.Repository;

import com.sgcc.devops.dao.entity.Monitor;

/**
 * 批量监控容器客户端
 * 
 * @author dmw
 *
 */
@Repository("containerMonitorCluster")
public class ContainerMonitorCluster {
	private static final Long TIME_OUT = 1000 * 60L;

	public List<Monitor> batchMoniter(List<ContainerMonitorParam> containers) throws Exception {
		ExecutorService executor = Executors.newCachedThreadPool();
		CompletionService<MonitorResult> completionService = new ExecutorCompletionService<>(executor);
		if (null == containers || containers.isEmpty()) {
			return null;
		}
		for (ContainerMonitorParam container : containers) {
			ContainerMonitorTask task = new ContainerMonitorTask(container);
			completionService.submit(task);
		}
		List<Monitor> monitors = new ArrayList<Monitor>();
		int completeTask = 0;
		long begingTime = System.currentTimeMillis();
		while (completeTask < containers.size()) {
			Future<MonitorResult> future = completionService.take();
			MonitorResult result = future.get();
			if (result.isSuccess()) {
				monitors.add(result.getData());
			}
			completeTask++;
			if (System.currentTimeMillis() - begingTime > TIME_OUT) {
				break;
			}
		}
		executor.shutdown();
		return monitors;
	}
}
