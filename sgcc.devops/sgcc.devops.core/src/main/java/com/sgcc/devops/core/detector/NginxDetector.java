package com.sgcc.devops.core.detector;

import org.apache.commons.lang.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.SSH;

public class NginxDetector {
	private String hostname;
	private String username;
	private String hostSecure;

	public NginxDetector(String hostname, String username, String hostSecure) {
		super();
		this.hostname = hostname;
		this.username = username;
		this.hostSecure = hostSecure;
	}

	public Result normal() {
		SSH ssh = new SSH(hostname, username, hostSecure);
		if (ssh.connect()) {
			try {
				String result = ssh.executeWithResult("ps -A | grep nginx", 3000);
				if(StringUtils.isEmpty(result)){
					ssh.close();
					ssh.connect();
					String res = ssh.executeWithResult("nginx", 3000);
					if(res.contains("command not found")){
						return new Result(false,res);
					}else if(StringUtils.isEmpty(res)){
						return new Result(true, "测试成功");
					}
					return new Result(false, "测试失败: "+res);
				}else{
					return new Result(true, "测试成功");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return new Result(false, "测试失败");
		} else {
			return new Result(false, "主机连接失败");
		}
	}

}
