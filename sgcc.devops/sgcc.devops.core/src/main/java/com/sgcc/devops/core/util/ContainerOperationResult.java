package com.sgcc.devops.core.util;

import java.util.List;

import com.sgcc.devops.common.model.SimpleContainer;

import net.sf.json.JSONArray;

/**
 * date：2015年8月27日 上午9:33:07 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerResult.java description：
 */
public class ContainerOperationResult {

	private Boolean flag = false;
	private List<SimpleContainer> successContainers;
	private JSONArray jsonArray;
	private String timeOutMessage;

	public ContainerOperationResult() {
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	public List<SimpleContainer> getsuccessContainers() {
		return successContainers;
	}

	public void setsuccessContainers(List<SimpleContainer> successContainers) {
		this.successContainers = successContainers;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public String getTimeOutMessage() {
		return timeOutMessage;
	}

	public void setTimeOutMessage(String timeOutMessage) {
		this.timeOutMessage = timeOutMessage;
	}

}
