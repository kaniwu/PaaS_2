package com.sgcc.devops.core.detector;

/**
 * 探测器接口
 * 
 * @author dmw
 *
 */
public interface Detector {

	boolean normal();
}
