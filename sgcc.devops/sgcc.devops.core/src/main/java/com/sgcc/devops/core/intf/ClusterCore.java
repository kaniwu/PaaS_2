package com.sgcc.devops.core.intf;

/**
 * @author luogan 2015年8月17日 下午2:43:54
 */
public interface ClusterCore {

	/**
	 * @author luogan
	 * @param model
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public String connectClusterHost(String hostIp, String hostName, String hostPwd,String hostPort);

	/**
	 * 检查swarm是否已装，如果已安装，启动进程
	 * 参数：主机IP，登录用户，密码，swarm启动端口，swarm配置文件名称
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年4月21日
	 */
	public boolean checkAndStartSwarm(String hostIp, String hostName, String hostPwd,String hostPort, String port, String file);

	/**
	 * 启动swarm进程
	 * 参数：主机IP，登录用户，密码，swarm启动端口，swarm配置文件名称
	 * @author luogan
	 * @param model
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public boolean startSwarm(String hostIp, String hostName, String hostPwd, String hostPort,String port, String file);

	/**
	 * 停止swarm
	 * 参数：主机IP，登录用户，密码，swarm启动端口
	 * @author lining
	 * @param hostIp,
	 *            hostpwd
	 * @return hostUuid
	 */
	public boolean stopSwarm(String ip, String account, String password, String hostPort,String port);
    
	/**
	 * 检查swarm的状态
	 * 参数 主机IP，登录用户，密码
	 * @param ip
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean checkSwarm(String ip, String username, String password,String hostPort);
}
