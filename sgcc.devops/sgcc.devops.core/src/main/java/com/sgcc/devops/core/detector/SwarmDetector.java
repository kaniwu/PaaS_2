package com.sgcc.devops.core.detector;

import java.util.Map;

import com.sgcc.devops.core.util.HttpClient;

/**
 * 集群探测器，确保swarm进程存活
 * @author dmw
 *
 */
public class SwarmDetector implements Detector{

	private String ip;
	private String port;
	private HttpClient client;
	
	public SwarmDetector() {
		super();
	}

	public SwarmDetector(String ip, String port,HttpClient client) {
		super();
		this.ip = ip;
		this.port = port;
		this.client = client;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	private String detectUrl(){
		return "http://"+ip+":"+port+"/info";
	}
	@Override
	public boolean normal() {
		Map<String,Object> response = client.doGet(null, detectUrl());
		if(response.containsKey("success")){
			return (boolean) response.get("success");
		}else{
			return true;
		}
	}
}
