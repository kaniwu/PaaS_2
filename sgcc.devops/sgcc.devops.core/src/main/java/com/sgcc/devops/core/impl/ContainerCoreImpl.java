package com.sgcc.devops.core.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Image;
import com.github.dockerjava.core.DockerClientBuilder;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.SimpleContainer;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.ContainerCore;
import com.sgcc.devops.core.task.ContainerPauseTask;
import com.sgcc.devops.core.task.ContainerStartTask;
import com.sgcc.devops.core.task.ContainerStopTask;
import com.sgcc.devops.core.task.ContainerUnpauseTask;
import com.sgcc.devops.core.task.ContanierTrashTask;
import com.sgcc.devops.core.util.ContainerOperationResult;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * date：2015年8月25日 下午2:08:32 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerCoreImpl.java description：
 */
@Component
public class ContainerCoreImpl implements ContainerCore {

	private static Logger logger = Logger.getLogger(ContainerCore.class);
	@Resource
	private DockerHostConfig dockerHostConfig;
	@Override
	public JSONObject createContainer(String hostIp, String hostName, String hostSecure, String clusterPort,
			ContainerModel model) {
		String containerId = "";
		SSH ssh = new SSH(hostIp, hostName, hostSecure);
		JSONObject jo = new JSONObject();
		jo.put("issuccess", false);
		try {
			StringBuffer commandStr = new StringBuffer();
			String masterUrl = hostIp + ":" + clusterPort;
			if (ssh.connect()) {
				String conId = model.getConId();
				commandStr.append("docker -H tcp://");
				commandStr.append(masterUrl);
				if ("0".equals(model.getCreateModel())) {
					commandStr.append(" create ");
				} else {
					commandStr.append(" run ");
					commandStr.append(" -d ");
				}
				
				if(model.getConCommand() == null || model.getConCommand().length() == 0){
				}else{
					commandStr.append(" -t ");
				}
				
				if(model.getConPorts()==null || model.getConPorts().length() ==0){
					commandStr.append(" -P");
				}else{
					for(String port:model.getConPorts().split(",")){
						commandStr.append(" -p "+port);
					}
				}
				if(model.getCpu()!=null&&model.getCpu()>0){
					commandStr.append(" -c 1024 ");
				}
				if(model.getMemery()!=null&&model.getMemery()>0){
					commandStr.append(" -m "+model.getMemery()+"G ");
				}
				
				if(StringUtils.isNotEmpty(model.getLogPath())){
					commandStr.append(" -v "+dockerHostConfig.getHostAppLogPath()+"/"+conId+"_"+model.getSystemCode()+":"+model.getLogPath());
				}
				//使用那个主机的驱动
				commandStr.append(" -e DOCKER_HOST=\"" + masterUrl + "\"");
				if (StringUtils.isNotEmpty(model.getCreateParams())) {
					commandStr.append(" -e " + model.getCreateParams());
				}
				if(null!=model.getParameter()){
					commandStr.append(" " +model.getParameter());
				}
				commandStr.append(" " + model.getImageName());
				if(model.getConCommand() == null || model.getConCommand().length() == 0){
					commandStr.append(";");
				}else{
					commandStr.append(" "+model.getConCommand() +";");
				}
//				logger.info(commandStr.toString());
				containerId = ssh.executeWithResult(commandStr.toString());
				if (containerId.toLowerCase().contains("error") || containerId.toLowerCase().contains("failed") || containerId.contains("EOF")) {
					jo.put("issuccess", false);
					jo.put("result", "系统部署容器启动失败:"+containerId);
					return jo;
				}
				SimpleContainer simpleContainer = new SimpleContainer(containerId.substring(0, 64).trim(), hostIp, clusterPort);
				jo = containerInfo(simpleContainer);
				if (!jo.isEmpty()) {
					if(!"0".equals(model.getCreateModel())
							&&(jo.getJSONArray("ports").size()==0||jo.getString("status")!=null&&!jo.getString("status").contains("Up"))){
						jo.put("issuccess", false);
						jo.put("result", "应用容器无法正常启动，可能的原因：（1）部署配置问题导致容器进程无法正常启动；（2）服务器资源不足");
						//删除启动失败的容器
						List<SimpleContainer> simpleContainers = new ArrayList<>();
						simpleContainers.add(simpleContainer);
						this.removeContainer(simpleContainers);
					}else{
						jo.put("command", commandStr);
						jo.put("conId", conId);
						jo.put("issuccess", true);
					}
				}
				commandStr.delete(0, commandStr.length());
			}
		} catch (Exception e) {
			logger.error("Create container though docker error!", e);
		} finally {
			ssh.close();
		}
		return jo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.core.ContainerCore#startContainer(java.lang.String[])
	 */
	@Override
	public ContainerOperationResult startContainer(List<SimpleContainer> simpleContainers) {
		ContainerOperationResult containerResult = new ContainerOperationResult();
		try {
			if (simpleContainers != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<JSONObject> comp = new ExecutorCompletionService<>(executor);
				for (SimpleContainer simCon : simpleContainers) {
					DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
					comp.submit(new ContainerStartTask(client, simCon.getContainerUuid()));
				}
				executor.shutdown();
				int index = 0;
				long begingTime = System.currentTimeMillis();
				List<SimpleContainer> successContainers = new ArrayList<SimpleContainer>();
				JSONArray containerArray = new JSONArray();
				while (index < simpleContainers.size()) {
					Future<JSONObject> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get() != null) {
							successContainers.add(simpleContainers.get(index));
							containerArray.add(future.get());
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						containerResult.setTimeOutMessage("容器启动超时！");
						break;
					}
				}
				if (successContainers.size() == simpleContainers.size()) {
					containerResult.setFlag(true);
				} else {
					containerResult.setFlag(false);
				}
				containerResult.setJsonArray(containerArray);
				containerResult.setsuccessContainers(successContainers);
			}
			logger.info("Start container success:operate server success");
		} catch (Exception e) {
			logger.error("Start container error:operate server error", e);
		}
		return containerResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.core.ContainerCore#stopContainer(java.lang.String[])
	 */
	@Override
	public ContainerOperationResult stopContainer(List<SimpleContainer> simpleContainers) {
		ContainerOperationResult containerResult = new ContainerOperationResult();
		try {
			if (simpleContainers != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<Boolean> comp = new ExecutorCompletionService<>(executor);
				for (SimpleContainer simCon : simpleContainers) {
					DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
					comp.submit(new ContainerStopTask(client, simCon.getContainerUuid()));
				}
				executor.shutdown();
				int index = 0;
				List<SimpleContainer> successContainers = new ArrayList<SimpleContainer>();
				long begingTime = System.currentTimeMillis();
				while (index < simpleContainers.size()) {
					Future<Boolean> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get()) {
							successContainers.add(simpleContainers.get(index));
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						containerResult.setTimeOutMessage("容器停止超时！");
						continue;
					}
				}
				if (successContainers.size() == simpleContainers.size()) {
					containerResult.setFlag(true);
				} else {
					containerResult.setFlag(false);
				}
				containerResult.setsuccessContainers(successContainers);
			}
			logger.info("Stop container success:operate server success");
		} catch (Exception e) {
			logger.error("Stop container error:operate server error", e);
		}
		return containerResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.core.ContainerCore#pauseContainer(java.lang.String[])
	 */
	@Override
	public ContainerOperationResult pauseContainer(List<SimpleContainer> simpleContainers) {
		ContainerOperationResult containerResult = new ContainerOperationResult();
		try {
			if (simpleContainers != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<Boolean> comp = new ExecutorCompletionService<>(executor);
				for (SimpleContainer simCon : simpleContainers) {
					DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
					comp.submit(new ContainerPauseTask(client, simCon.getContainerUuid()));
				}
				executor.shutdown();
				int index = 0;
				List<SimpleContainer> successContainers = new ArrayList<SimpleContainer>();
				long begingTime = System.currentTimeMillis();
				while (index < simpleContainers.size()) {
					Future<Boolean> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get()) {
							successContainers.add(simpleContainers.get(index));
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						containerResult.setTimeOutMessage("容器挂起超时！");
						continue;
					}
				}
				if (successContainers.size() == simpleContainers.size()) {
					containerResult.setFlag(true);
				} else {
					containerResult.setFlag(false);
				}
				containerResult.setsuccessContainers(successContainers);
			}
			logger.info("Pause container success:operate server success");
		} catch (Exception e) {
			logger.error("Pause container error:operate server error", e);
		}
		return containerResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.core.ContainerCore#unpauseContainer(java.lang.String[])
	 */
	@Override
	public ContainerOperationResult unpauseContainer(List<SimpleContainer> simpleContainers) {
		ContainerOperationResult containerResult = new ContainerOperationResult();
		try {
			if (simpleContainers != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<Boolean> comp = new ExecutorCompletionService<>(executor);
				for (SimpleContainer simCon : simpleContainers) {
					DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
					comp.submit(new ContainerUnpauseTask(client, simCon.getContainerUuid()));
				}
				executor.shutdown();
				int index = 0;
				List<SimpleContainer> successContainers = new ArrayList<SimpleContainer>();
				long begingTime = System.currentTimeMillis();
				while (index < simpleContainers.size()) {
					Future<Boolean> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get()) {
							successContainers.add(simpleContainers.get(index));
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						containerResult.setTimeOutMessage("容器恢复超时！");
						continue;
					}
				}
				if (successContainers.size() == simpleContainers.size()) {
					containerResult.setFlag(true);
				} else {
					containerResult.setFlag(false);
				}
				containerResult.setsuccessContainers(successContainers);
			}
			logger.info("Unpause container success:operate server success");
		} catch (Exception e) {
			logger.error("Unpause container error:operate server error", e);
		}
		return containerResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.core.ContainerCore#deleteContainer(java.lang.String[])
	 */
	@Override
	public ContainerOperationResult removeContainer(List<SimpleContainer> simpleContainers) {
		ContainerOperationResult containerResult = new ContainerOperationResult();
		try {
			if (simpleContainers != null) {
				ExecutorService executor = Executors.newCachedThreadPool();
				CompletionService<Boolean> comp = new ExecutorCompletionService<>(executor);
				for (SimpleContainer simCon : simpleContainers) {
					DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
					comp.submit(new ContanierTrashTask(client, simCon.getContainerUuid()));
				}
				executor.shutdown();
				int index = 0;
				List<SimpleContainer> successContainers = new ArrayList<SimpleContainer>();
				long begingTime = System.currentTimeMillis();
				while (index < simpleContainers.size()) {
					Future<Boolean> future = comp.poll();
					if (future == null) {
					} else {
						if (future.get()) {
							successContainers.add(simpleContainers.get(index));
						}
						index++;
					}
					TimeUnit.MILLISECONDS.sleep(500);
					if (System.currentTimeMillis() - begingTime > 5 * 60 * 1000) {
						containerResult.setTimeOutMessage("容器删除超时！");
						continue;
					}
				}
				if (successContainers.size() == simpleContainers.size()) {
					containerResult.setFlag(true);
				} else {
					containerResult.setFlag(false);
				}
				containerResult.setsuccessContainers(successContainers);
			}
			logger.info("Remove container success:operate server success");
		} catch (Exception e) {
			logger.error("Remove container error:operate server error", e);
		}
		return containerResult;
	}

	/**
	 * @author langzi
	 * @param client
	 * @param imageName
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	@SuppressWarnings("unused")
	private boolean imageIsExist(DockerClient client, String imageName) {
		List<Image> imageList = client.listImagesCmd().exec();
		for (Image image : imageList) {
			for (int i = 0; i < image.getRepoTags().length; i++) {
				if (imageName.equals(image.getRepoTags()[i])) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * @author langzi
	 * @param containerId
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	public JSONObject containerInfo(SimpleContainer simCon) {
		DockerClient client = getDockerClient(simCon.getClusterIp(), simCon.getClusterPort());
		JSONObject jo = new JSONObject();
		if (!"".equals(simCon.getContainerUuid())) {
			boolean start = false;
			while (!start) {
				List<Container> containers = client.listContainersCmd().withShowAll(true).exec();
				int flag = 0;
				for (Container container : containers) {
					if (container.getId().equals(simCon.getContainerUuid())) {
						jo = JSONUtil.parseObjectToJsonObject(container);
						start = true;
						break;
					}
					flag++;
				}
				if (flag >= containers.size()) {
					break;
				}
			}
		}
		return jo;
	}

	/**
	 * @author langzi
	 * @param clusterIp
	 * @param clusterPort
	 * @return
	 * @version 1.0 2015年9月10日
	 */
	public DockerClient getDockerClient(String clusterIp, String clusterPort) {
		String url = "http://" + clusterIp + ":" + clusterPort;
		return DockerClientBuilder.getInstance(url).build();
	}

	public static void main(String[] args) {
		ContainerCoreImpl core = new ContainerCoreImpl();
		SimpleContainer simpleContainer = new SimpleContainer("a7f0143c0b088b12e972d0d9edda3f3507a5ce5107c1c090c9013f65ade4f9d3", "192.168.49.135", "5010");
		core.containerInfo(simpleContainer);
	}
}
