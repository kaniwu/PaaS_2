package com.sgcc.devops.core.intf;

import java.util.List;

import net.sf.json.JSONObject;

import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.SimpleContainer;
import com.sgcc.devops.core.util.ContainerOperationResult;

/**
 * date：2015年8月25日 上午11:29:47 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerCore.java description：
 */
public interface ContainerCore {

	/**
	 *创建容器
	 *参数 ：集群主机IP，登录用户，密码，集群端口，容器启动参数
	 * @author langzi
	 * @param model
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public JSONObject createContainer(String hostIp, String hostName, String hostPassword, String clusterPort,
			ContainerModel model);

	/**
	 *	批量容器启动
	 *参数对象：容器uuid
	 * @author langzi
	 * @param containerUuid
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public ContainerOperationResult startContainer(List<SimpleContainer> simpleContainers);

	/**
	 * 批量停止容器
	 * 参数对象：容器uuid
	 * @author langzi
	 * @param containerUuid
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public ContainerOperationResult stopContainer(List<SimpleContainer> simpleContainers);

	/**
	 * 批量暂停容器
	 * 参数对象：容器uuid
	 * @author langzi
	 * @param containerUuid
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public ContainerOperationResult pauseContainer(List<SimpleContainer> simpleContainers);

	/**
	 * 批量从暂停状态恢复
	 * 参数对象：容器uuid
	 * @author langzi
	 * @param containerUuid
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public ContainerOperationResult unpauseContainer(List<SimpleContainer> simpleContainers);

	/**
	 * 批量删除
	 * 参数对象：容器uuid
	 * @author langzi
	 * @param containerUuid
	 * @return
	 * @version 1.0 2015年8月25日
	 */
	public ContainerOperationResult removeContainer(List<SimpleContainer> simpleContainers);

	/**
	* 获取容器信息
	* 参数：容器uuid
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月14日
	*/
	public JSONObject containerInfo(SimpleContainer simpleContainer);
}
