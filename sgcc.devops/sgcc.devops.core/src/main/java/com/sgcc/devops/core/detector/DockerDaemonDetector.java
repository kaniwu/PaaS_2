package com.sgcc.devops.core.detector;

import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sgcc.devops.core.util.HttpClient;

public class DockerDaemonDetector implements Detector {

	private String ip;
	private String port;

	public DockerDaemonDetector(String ip, String port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	private String assembleUrl() {
		return "http://" + ip + ":" + port + "/info";
	}

	@Override
	public boolean normal() {
		HttpClient client = new HttpClient(7000);
		String url = assembleUrl();
		String response = client.get(null, url);
		if (StringUtils.isEmpty(response)) {
			return false;
		}
		if (response.contains("command not found") || response.contains("Cannot connect to the Docker daemon")) {
			return false;
		}
		JSONObject jsonObject = JSON.parseObject(response);
		try {
			String id = jsonObject.getString("ID");
			return !(null == id);
		} catch (Exception e) {
			return false;
		}
	}

}
