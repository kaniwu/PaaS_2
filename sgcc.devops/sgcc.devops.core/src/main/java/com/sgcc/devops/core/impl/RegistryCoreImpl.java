package com.sgcc.devops.core.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.RegistryCore;
import com.sgcc.devops.core.util.DockerConstants;
import com.sgcc.devops.core.util.DockerHostInfo;
@Component
public class RegistryCoreImpl implements RegistryCore {

	private static Logger logger = Logger.getLogger(RegistryCoreImpl.class);

	public Result createRegistry(String ip, String name, String password, String hostPort,String fileName, String imageName,
			String imageTag) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostPort);
		if (ssh.connect()) {
			String tag = imageName + ":" + imageTag;
			StringBuffer commandStr = new StringBuffer();
			commandStr.append("cd ").append(DockerConstants.DOCKERFILE_PATH).append(";");
			if (fileName.contains(".tar.gz")) {
				commandStr.append("tar zxvf " + fileName + ";");
				commandStr.append("sudo docker build -t " + tag + " " + DockerConstants.DOCKERFILE_PATH
						+ fileName.substring(0, fileName.length() - 7) + "/;");
				commandStr.append("rm -rf " + DockerConstants.DOCKERFILE_PATH
						+ fileName.substring(0, fileName.length() - 7) + "*;");
			} else if (fileName.contains(".zip")) {
				commandStr.append("unzip " + fileName + ";");
				commandStr.append("sudo docker build -t " + tag + " " + DockerConstants.DOCKERFILE_PATH
						+ fileName.substring(0, fileName.length() - 4) + "/;");
				commandStr.append("rm -rf " + DockerConstants.DOCKERFILE_PATH
						+ fileName.substring(0, fileName.length() - 4) + "*;");
			} else {
				return new Result(false, "不支持此类文件类型！");
			}
			try {
				String result = ssh.executeWithResult(commandStr.toString());
				logger.info("Make image result:" + result);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
				String uuid = null;
				int stepNull = 0;
				while (stepNull < 2) {
					String line = reader.readLine();
					if (null == line) {
						stepNull++;
					} else if (line.contains("Successfully") && line.contains("built")) {
						uuid = line.split(" ")[2];
					} else {
						continue;
					}
				}
				if (null == uuid) {
					return new Result(false, "镜像制作脚本执行异常！");
				} else {
					return new Result(true, uuid);
				}
			} catch (Exception e) {
				logger.info("Make image fail");
				return new Result(false, "制作镜像失败：脚本执行异常！");
			} finally {
				ssh.close();
			}
		} else {
			return new Result(false, "登录仓库主机异常");
		}
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.RegistryCore#startRegistry(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Result startRegistry(String ip, String name, String password,String hostport) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		if (ssh.connect()) {
			String commandStr ="systemctl start docker-distribution";
			try {
				String result = ssh.executeWithResult(commandStr);
				logger.info("Start registry result:" + result);
				if (result.contains("Failed")) {
					return new Result(false, "启动仓库【"+ip+"】失败："+result);
				}else{
					return new Result(true, "启动仓库【"+ip+"】成功！");
				}
			} catch (Exception e) {
				logger.error("Make image fail:"+e);
				return new Result(false, "启动仓库【"+ip+"】失败："+e);
			} finally {
				ssh.close();
			}
		} else {
			return new Result(false, "登录仓库主机【"+ip+"】异常！");
		}
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.core.intf.RegistryCore#stopRegistry(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Result stopRegistry(String ip, String name, String password, String hostport) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		if (ssh.connect()) {
			String commandStr ="systemctl stop docker-distribution";
			try {
				String result = ssh.executeWithResult(commandStr);
				logger.error("Start registry result:" + result);
				if (result.contains("Failed")) {
					return new Result(false, "停止仓库【"+ip+"】失败："+result);
				}else{
					return new Result(true, "停止仓库【"+ip+"】成功！");
				}
			} catch (Exception e) {
				logger.error("Start registry result:"+e);
				return new Result(false, "停止仓库【"+ip+"】失败："+e);
			} finally {
				ssh.close();
			}
		} else {
			return new Result(false, "登录仓库主机【"+ip+"】异常！");
		}
	}

	@Override
	public String registryInfo(String ip, String name, String password,String hostport) {
		SSH ssh = new DockerHostInfo().getConnection(ip, name, password,hostport);
		String result = null;
		if (ssh.connect()) {
			String commandStr ="export LANG=en_US;systemctl status docker-distribution";
			try {
				 result = ssh.executeWithResult(commandStr);
//				 logger.error("status registry result:" + result);
				 return result;
			} catch (Exception e) {
				logger.error("status registry result:"+e);
				return null;
			} finally {
				ssh.close();
			}
		} else {
			return null;
		}
	}

}
