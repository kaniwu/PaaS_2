/**
 * 
 */
package com.sgcc.devops.core.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @author luogan 2015年5月19日 下午5:36:28
 */
public class TypeUtils {

	public static final Map<String, String> tMapping = new HashMap<String, String>();

	static {
		tMapping.put("1", DockerHostInfo.class.getName());
	}

	public static String getClassname(String key) {
		return (key == null) ? null : tMapping.get(key);
	}

}
