package com.sgcc.devops.core.util;

import java.util.List;

import net.sf.json.JSONArray;

import com.sgcc.devops.common.model.SimpleModel;

/**
 * date：2015年8月27日 上午9:33:07 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerResult.java description：
 */
public class OperationResult {

	private Boolean flag = false;
	private List<SimpleModel> successModels;
	private JSONArray jsonArray;
	private String timeOutMessage;

	public OperationResult() {
	}

	public Boolean getFlag() {
		return flag;
	}

	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	

	public List<SimpleModel> getSuccessModels() {
		return successModels;
	}

	public void setSuccessModels(List<SimpleModel> successModels) {
		this.successModels = successModels;
	}

	public JSONArray getJsonArray() {
		return jsonArray;
	}

	public void setJsonArray(JSONArray jsonArray) {
		this.jsonArray = jsonArray;
	}

	public String getTimeOutMessage() {
		return timeOutMessage;
	}

	public void setTimeOutMessage(String timeOutMessage) {
		this.timeOutMessage = timeOutMessage;
	}

}
