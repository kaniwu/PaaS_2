package com.sgcc.devops.core.task;

import java.util.concurrent.Callable;

import com.github.dockerjava.api.DockerClient;

/**
 * date：2015年8月26日 下午10:11:04 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerPauseTask.java description：
 */
public class ContainerPauseTask implements Callable<Boolean> {

	private DockerClient client;
	private String containerId;

	/**
	 * Constructor
	 * 
	 * @author liangzi
	 * @param client
	 * @param containerId
	 * @version 1.0 2015年8月26日
	 */
	public ContainerPauseTask(DockerClient client, String containerId) {
		this.client = client;
		this.containerId = containerId;
	}

	@Override
	public Boolean call() throws Exception {
		try {
			client.pauseContainerCmd(containerId).exec();
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
