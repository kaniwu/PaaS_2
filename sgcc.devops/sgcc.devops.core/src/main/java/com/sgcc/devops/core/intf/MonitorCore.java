package com.sgcc.devops.core.intf;

import com.sgcc.devops.dao.entity.Monitor;

/**
 * 监控核心服务接口
 * 
 * @author dmw
 *
 */
public interface MonitorCore {

	/**
	 * 获取容器监控数据
	 * 
	 * @param conId
	 *            容器记录主键
	 * @param clusterIp
	 *            容器所在集群IP地址
	 * @param clusterPort
	 *            集群服务端口
	 * @param container
	 *            容器UUID
	 * @return 性能数据
	 * @throws Exception
	 */
	public Monitor containerState(String conId, String clusterIp, String clusterPort, String container)
			throws Exception;

	/**
	 * 获取主机监控数据
	 * 
	 * @param ip
	 *            物理主机IP地址
	 * @param port
	 *            物理主机代理暴漏端口
	 * @return 性能数据
	 * @throws Exception
	 */
	public Monitor hostState(String ip, String port) throws Exception;
}
