package com.sgcc.devops.core.intf;

import com.sgcc.devops.common.bean.Result;

/**
 * date：2015年8月25日 上午11:29:47 project name：sgcc-devops-core
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerCore.java description：
 */
public interface RegistryCore {

	/**
	 * @author yangqinglin
	 * @description 在目标主机上创建仓库（）
	 * @return
	 * @version 1.0 2015年9月18日
	 */
//	Result createRegistry(String ip, String name, String password, String fileName, String imageName, String imageTag);
	/**
	 * 启动
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月14日
	 */
	Result startRegistry(String ip, String name, String password,String hostport);
	/**
	 * 停止
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月14日
	 */
	Result stopRegistry(String ip, String name, String password,String hostport);
	
	/**
	 * 检查registry的状态
	 * @param ip
	 * @param name
	 * @param password
	 * @return
	 */
	String registryInfo(String ip, String name, String password,String hostport);
}
