package com.sgcc.devops.core.monitor;

public class HostMonitorParam {

	private String id;// 物理主机主键ID
	private String ip;// 物理主机IP
	private String port;// 物理主机代理端口

	public HostMonitorParam(String id, String ip, String port) {
		super();
		this.id = id;
		this.ip = ip;
		this.port = port;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public HostMonitorParam() {
		super();
	}
}
