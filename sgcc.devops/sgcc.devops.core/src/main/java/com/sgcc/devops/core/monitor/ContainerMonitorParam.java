package com.sgcc.devops.core.monitor;

public class ContainerMonitorParam {

	private String id;// 容器表主键ID
	private String cluter; // 集群IP地址
	private String port;// 集群对外服务端口
	private String container;// 容器UUID

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCluter() {
		return cluter;
	}

	public void setCluter(String cluter) {
		this.cluter = cluter;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getContainer() {
		return container;
	}

	public void setContainer(String container) {
		this.container = container;
	}

	public ContainerMonitorParam(String id, String cluter, String port, String container) {
		super();
		this.id = id;
		this.cluter = cluter;
		this.port = port;
		this.container = container;
	}

	public ContainerMonitorParam() {
		super();
	}
}
