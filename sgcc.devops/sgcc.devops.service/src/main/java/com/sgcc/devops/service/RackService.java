/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Rack;

/**  
 * date：2016年3月16日 上午9:24:09
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RackService.java
 * description：  
 */
public interface RackService {
    int deleteByPrimaryKey(String id) throws Exception;

    int insert(Rack record) throws Exception;

    Rack selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(Rack record) throws Exception;

    int updateByPrimaryKey(Rack record) throws Exception;
    
    List<Rack> rackListByServerRoom(String serverRoomId); 
    
    boolean ifRackNameRepeat(Rack rack);

	Rack getRack(Rack rack);

	GridBean rackList(PagerModel pagerModel, String serverRoomId);
}
