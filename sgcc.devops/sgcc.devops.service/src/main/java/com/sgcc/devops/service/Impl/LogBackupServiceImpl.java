package com.sgcc.devops.service.Impl;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sgcc.devops.dao.intf.LogBackupMapper;
import com.sgcc.devops.service.LogBackupService;

/**  
 * date：2016年2月4日 下午3:20:40
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogBackupService.java
 * description：日志备份数据维护接口实现类
 */
@Component
public class LogBackupServiceImpl implements LogBackupService {

	@Resource
	private LogBackupMapper logBackupMapper;

	@Override
	public boolean trsOvertimedLog(Date savetime) {
		// TODO Auto-generated method stub
		return false;
	}

}
