package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.AppPrelib;
import com.sgcc.devops.dao.entity.SystemApp;

import net.sf.json.JSONArray;

public interface SystemAppService {

	public JSONArray getSystemApps(String systemId);
	
	public SystemApp load(String id);

	/**
	* TODO
	* @author mayh
	* @return SystemApp
	* @version 1.0
	* 2016年5月11日
	*/
	public SystemApp getSystemAppById(String systemAppId);

	/**
	* 通过应用包Id查询预加载包
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月12日
	*/
	public List<AppPrelib> getPreLibByAppId(String id);

}
