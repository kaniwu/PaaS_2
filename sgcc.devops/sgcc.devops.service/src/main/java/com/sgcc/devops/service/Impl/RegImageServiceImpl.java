/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.dao.intf.RegistryMapper;
import com.sgcc.devops.service.RegImageService;

/**  
 * date：2016年2月4日 下午3:33:06
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RegImageService.java
 * description：  仓库镜像关联数据维护接口实现类
 */
@Component
public class RegImageServiceImpl implements RegImageService {
	@Resource
	private RegImageMapper regImageMapper;
	@Resource
	private RegistryMapper registryMapper;

	public List<RegImage> selectAll(String imageId) {
		RegImage record = new RegImage();
		record.setImageId(imageId);
		List<RegImage> regImages = regImageMapper.selectAll(record);
		return regImages;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.RegImageService#getRegistry(java.lang.Integer)
	 */
	@Override
	public Registry getRegistry(String imageId) {
		RegImage record = new RegImage();
		record.setImageId(imageId);
		List<RegImage> regImages = regImageMapper.selectAll(record);
		if (null == regImages || regImages.isEmpty()) {
			return null;
		}
		Registry registry = new Registry();
		registry.setRegistryId(regImages.get(0).getRegistryId());
		registry = registryMapper.selectRegistry(registry);
		return registry;
	}
}
