package com.sgcc.devops.service;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.ElasticStrategy;

/**  
 * date：2016年2月4日 下午3:17:35
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticStrategyService.java
 * description：  弹性伸缩策略数据维护接口类
 */
public interface ElasticStrategyService {

	/** 插入弹性伸缩策略表 */
	public Result insert(ElasticStrategy record);

	/** 删除弹性伸缩策略表 */
	public Result delete(String id);

	/** 更新弹性伸缩策略表 */
	public Result update(ElasticStrategy record);

	/** 获取弹性伸缩策略表 */
	public ElasticStrategy load(String id);

	/** 根据系统Id获取弹性伸缩策略表 */
	public ElasticStrategy loadBySystem(String systemId);
}
