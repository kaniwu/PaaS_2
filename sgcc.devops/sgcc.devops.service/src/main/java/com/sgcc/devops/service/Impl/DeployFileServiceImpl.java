/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.sgcc.devops.dao.entity.DeployFile;
import com.sgcc.devops.dao.intf.DeployFileMapper;
import com.sgcc.devops.service.DeployFileService;

/**  
 * date：2016年1月26日 上午9:49:52
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployFileService.java
 * description：  部署配置文件数据维护接口实现类
 */
@Component
public class DeployFileServiceImpl implements DeployFileService{
	@Resource
	private DeployFileMapper deployFileMapper;
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployFileService#selectByDeployId(java.lang.String)
	 */
	@Override
	public List<DeployFile> selectByDeployId(String deployId) {
		List<DeployFile> deployFiles = deployFileMapper.selectByDeployId(deployId);
		return deployFiles;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployFileService#insert(com.sgcc.devops.dao.entity.DeployFile)
	 */
	@Override
	public int insert(DeployFile deployFile) {
		
		return deployFileMapper.insert(deployFile);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployFileService#deleteByDeployId(java.lang.String)
	 */
	@Override
	public void deleteByDeployId(String deployId) {
		deployFileMapper.deleteByDeployId(deployId);
		
	}

}
