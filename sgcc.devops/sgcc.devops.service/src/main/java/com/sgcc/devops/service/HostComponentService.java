/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.HostComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.HostInstall;

public interface HostComponentService {

	GridBean hostComponentListAll(PagerModel pagerModel);

	GridBean hostComponentList(PagerModel pagerModel,HostComponentModel hostComponentModel);
	
	HostComponent getHostComponent(HostComponent hostComponent);
	
	int deleteByPrimaryKey(String id) throws Exception;
	
	HostComponent selectById(String id);
	
	boolean ifRepeat(HostComponentModel hostComponentModel);
	
	 int insert(HostComponentModel hostComponentModel) throws Exception;
	 
	 int updateByPrimaryKeySelective(HostComponent hostComponent);

	/**
	* TODO
	* @author mayh
	* @return List<HostInstall>
	* @version 1.0
	* 2016年4月14日
	*/
	List<HostInstall> selectHostInstallByHostId(String hostId);

	Integer countHostComp();
	
}
