package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.ConPort;

/**  
 * date：2016年2月4日 下午2:58:24
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ConportService.java
 * description：  容器端口数据维护接口类
 */
public interface ConportService {

	/**
	 * @author langzi
	 * @param containerId
	 * @return
	 * @version 1.0 2015年8月31日
	 */
	public List<ConPort> listConPorts(String containerId) throws Exception;

	/**
	 * @author langzi
	 * @param appId
	 * @return
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<ConPort> listConPortsByAppId(String appId) throws Exception;

	/**
	 * @author langzi
	 * @param port
	 * @return
	 * @version 1.0 2015年8月31日
	 */
	public int addConports(ConPort port) throws Exception;

	/**
	 * @author langzi
	 * @param port
	 * @return
	 * @throws Exception
	 * @version 1.0 2015年9月16日
	 */
	public int updateConports(ConPort port) throws Exception;

	/**
	 * @author langzi
	 * @param containerId
	 * @return
	 * @version 1.0 2015年8月31日
	 */
	public int removeConports(String[] containerIds) throws Exception;
}
