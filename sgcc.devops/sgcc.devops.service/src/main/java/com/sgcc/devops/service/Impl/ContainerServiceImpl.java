package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.SimpleContainer;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.entity.expand.ContainerExpand;
import com.sgcc.devops.dao.intf.ContainerMapper;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.dao.intf.ImageMapper;
import com.sgcc.devops.dao.intf.SystemMapper;
import com.sgcc.devops.service.ContainerService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:01:44
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ContainerService.java
 * description：容器业务数据维护接口实现类
 */
@Component
public class ContainerServiceImpl implements ContainerService {

	Logger logger = Logger.getLogger(ContainerService.class);

	@Resource
	private ContainerMapper containerMapper;
	@Resource
	private SystemMapper systemMapper;
	@Resource
	private ImageMapper imageMapper;
	@Resource
	private HostMapper hostMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.ContainerService#listAllContainer()
	 */
	@Override
	public List<Container> listAllContainer() {
		try {
			return containerMapper.selectAll();
		} catch (Exception e) {
			logger.error("Get all containers error: "+e.getMessage());
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#listContainersByAppId(java.lang.
	 * Integer)
	 */
	@Override
	public List<Container> listContainersByAppId(String appId) throws Exception {
		return containerMapper.selectContainerByAppId(appId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#listAllContainersJsonArray()
	 */
	@Override
	public JSONArray listAllContainersJsonArray() {
		return JSONUtil.parseObjectToJsonArray(listAllContainer());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#selectContainerUuid(net.sf.json.
	 * JSONArray)
	 */
	@Override
	public List<SimpleContainer> selectContainerUuid(String[] containerIds) throws Exception {
		if(StringUtils.isEmpty(containerIds)||containerIds.length==0){
			return null;
		}
		List<Container> containers = containerMapper.selectContainerUuid(containerIds);
		List<SimpleContainer> simpleContainers = new ArrayList<SimpleContainer>();
		if (containers.size() > 0) {
			for (Container con : containers) {
				SimpleContainer simCon = new SimpleContainer(con.getConUuid(), con.getClusterIp(),
						con.getClusterPort());
				simCon.setContainerId(con.getConId());
				simpleContainers.add(simCon);
			}
		}
		return simpleContainers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#listOnePageContainers(java.lang.
	 * Integer, int, int, com.sgcc.devops.model.ContainerModel)
	 */
	@Override
	public GridBean listOnePageContainers(String userId, int pagenum, int pagesize, ContainerModel model) {
		PageHelper.startPage(pagenum, pagesize);
		List<Container> containers = listAllContainer();
		List<ContainerModel> containerModels = conListToModellist(containers);

		int totalPage = ((Page<?>) containers).getPages();
		Long totalNum = ((Page<?>) containers).getTotal();

		return new GridBean(pagenum, totalPage, totalNum.intValue(), containerModels);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#getContainer(com.sgcc.devops.
	 * entity.Container)
	 */
	@Override
	public Container getContainer(Container container) {
		try {
			return containerMapper.selectContainer(container);
		} catch (Exception e) {
			logger.error("Get one container error: "+e.getMessage());
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#getContainerJsonObject(com.sgcc.
	 * devops.entity.Container)
	 */
	@Override
	public JSONObject getContainerJsonObject(Container container) throws Exception {
		return JSONUtil.parseObjectToJsonObject(getContainer(container));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#addContaier(com.sgcc.devops.
	 * entity.Container)
	 */
	@Override
	public int addContaier(Container container) throws Exception {
		if (!StringUtils.hasText(container.getConId())) {
			container.setConId(KeyGenerator.uuid());
		}
		return containerMapper.insertContainer(container);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.ContainerService#modifyContainer(java.lang.
	 * Integer)
	 */
	@Override
	public int modifyContainer(Container container) throws Exception {
		return containerMapper.updateContainer(container);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#modifyContainerPower(java.lang.
	 * Integer, net.sf.json.JSONArray)
	 */
	@Override
	public int modifyContainerPower(ContainerExpand ce) throws Exception {
		return containerMapper.updateContainerPower(ce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#modifyContainerStatus(java.lang.
	 * Integer, net.sf.json.JSONArray)
	 */
	@Override
	public int modifyContainerStatus(ContainerExpand ce) throws Exception {
		return containerMapper.updateContainerStatus(ce);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.ContainerService#trashContainer(java.lang.
	 * Integer)
	 */
	@Override
	public int removeContainer(String[] containerIds) throws Exception {
		return containerMapper.deleteContainer(containerIds);
	}

	@Override
	public List<Container> listContainersByHostId(String hostId) throws Exception {
		return containerMapper.selectContainerByHostId(hostId);
	}

	@Override
	public List<Container> selectContainerId(String[] containerIds) throws Exception {

		return containerMapper.selectContainerId(containerIds);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#selectContainersByImageId(java.
	 * lang.Integer)
	 */
	@Override
	public List<Container> selectContainersByImageId(String conImgid) {
		return containerMapper.selectContainersByImageId(conImgid);
	}

	public GridBean selectContainersByImageId(String userId, int pagenum, int pagesize, String conImgid) {
		PageHelper.startPage(pagenum, pagesize);
		List<Container> containers = containerMapper.selectContainersByImageId(conImgid);
		List<ContainerModel> containerModels = conListToModellist(containers);

		int totalPage = ((Page<?>) containers).getPages();
		Long totalNum = ((Page<?>) containers).getTotal();
		return new GridBean(pagenum, totalPage, totalNum.intValue(), containerModels);

	}

	public List<ContainerModel> conListToModellist(List<Container> containers) {
		List<ContainerModel> containerModels = new ArrayList<>();

		for (Container container : containers) {
			ContainerModel containerModel = new ContainerModel();
			containerModel.setSysId(container.getSysId());
			containerModel.setConCreatetime(container.getConCreatetime());
			containerModel.setConDesc(container.getConDesc());
			containerModel.setConId(container.getConId());
			containerModel.setConImgid(container.getConImgid());
			containerModel.setConPower(container.getConPower());
			containerModel.setConStatus(container.getConStatus());
			containerModel.setConName(container.getConName());
			containerModel.setConTempName(container.getConTempName());
			containerModel.setConUuid(container.getConUuid());
			containerModel.setConType(container.getConType());
			containerModel.setHostId(container.getHostId());
			// Image image =
			// imageMapper.selectByPrimaryKey(container.getConImgid());
			if(null!=container.getHostId()){
				Host host = new Host();
				host.setHostId(container.getHostId());
				host = hostMapper.selectHost(host);
				if (host != null) {
					containerModel.setHostName(host.getHostName());
				} else {
					logger.warn("container host not exist");
				}
			}
			
			String systemNameString = systemMapper.getSystemNameByContainerId(container.getConId());
			String systemCodeString = systemMapper.getSystemCodeByContainerId(container.getConId());
			if (systemNameString != null) {
				containerModel.setSystemName(systemNameString);
			}
			if (systemCodeString != null) {
				containerModel.setSystemCode(systemCodeString);
			}else{
				//TO DO
			}
			containerModels.add(containerModel);
		}
		return containerModels;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#gridBeanContainersByHostId(int)
	 */
	@Override
	public GridBean gridBeanContainersByHostId(String hostId) throws Exception {
		List<Container> containers = this.listContainersByHostId(hostId);
		List<ContainerModel> containerModels = conListToModellist(containers);

		return new GridBean(1, 1, containerModels.size(), containerModels);
	}

	@Override
	public GridBean listContainerByType(User user,PagerModel pagerModel, Container container)
			throws Exception {
		
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		List<Container> containerCount = containerMapper.selectContainersByType(container);
		if(!containerCount.isEmpty()){
			if(containerCount.size()<(page-1)*pageSize){
				page = 1;
			}
		}
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("conTempName")){
				PageHelper.startPage(page, pageSize,"CON_TEMP_NAME "+sord);
			}
			if(sidx.equals("hostName")){
				PageHelper.startPage(page, pageSize,"HOST_ID "+sord);
			}
			if(sidx.equals("power")){
				PageHelper.startPage(page, pageSize,"CON_POWER "+sord);
			}
			if(sidx.equals("systemName")){
				PageHelper.startPage(page, pageSize,"SYS_ID "+sord);
			}
			if(sidx.equals("conCreatetime")){
				PageHelper.startPage(page, pageSize,"CON_CREATETIME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		//环境变量
		container.setConEnv(user.getUserCompany());
		List<Container> containers = containerMapper.selectContainersByType(container);
		List<ContainerModel> containerModels = conListToModellist(containers);
		int totalPage = ((Page<?>) containers).getPages();
		Long totalNum = ((Page<?>) containers).getTotal();
		return new GridBean(page, totalPage, totalNum.intValue(), containerModels);
	}

	@Override
	public List<Container> selectAllContainerOfHost(String hostId) throws Exception {

		return containerMapper.selectAllContainerOfHost(hostId);
	}

	@Override
	public List<Container> listContainerByPower(Container container) throws Exception {

		return containerMapper.selectContainersByPower(container);

	}

	@Override
	public int selectContainerCount(Container container) {
		int result = 0;
		try {
			result = containerMapper.selectContainerCount(container);
		} catch (Exception e) {
			logger.error("select container count fail: " + e.getMessage());
			return 0;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ContainerService#getBySysId(java.lang.Integer)
	 */
	@Override
	public List<Container> getBySysId(String systemId) {

		return containerMapper.selectContainerBySysId(systemId);
	}

}
