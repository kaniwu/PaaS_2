/**
 * 
 */
package com.sgcc.devops.service;

import com.sgcc.devops.dao.entity.Driver;
import com.sgcc.devops.dao.entity.User;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:13:09
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DriverService.java
 * description： 数据库驱动数据 维护接口类
 */
public interface DriverService {
	/**
	 * TODO
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年11月25日
	 */
	public JSONArray listByDatabaseId(String databaseId, User user);

	/**
	 * TODO
	 * @author mayh
	 * @return Driver
	 * @version 1.0 2015年11月30日
	 */
	public Driver selectByPrimaryId(String driverId);
	
	/**
	 * DB 查询
	 * TODO
	 * @author wangjx
	 * @return Driver
	 * @version 1.0 2015年11月30日
	 */
	public JSONObject getDatabaseDriverService(int DBType);
	
	
}
