/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.LocationModel;
import com.sgcc.devops.dao.entity.Location;

/**  
 * date：2016年4月6日 下午5:01:13
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LocationService.java
 * description：  
 */
public interface LocationService {

	/**
	* TODO
	* @author mayh
	* @return List<Location>
	* @version 1.0
	* 2016年4月6日
	*/
	List<Location> getLocationByParent(String parentId);

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	Result create(LocationModel locationModel);

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	Result update(Location location);

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	Result delete(LocationModel locationModel);

	/**
	* TODO
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年4月6日
	*/
	Location select(Location location);

	/**
	* TODO
	* @author mayh
	* @return List<Location>
	* @version 1.0
	* 2016年6月16日
	*/
	List<Location> selectByParamCode(String locationParamCode);

}
