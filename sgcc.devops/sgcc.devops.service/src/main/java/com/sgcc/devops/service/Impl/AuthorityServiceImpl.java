package com.sgcc.devops.service.Impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.dao.entity.Authority;
import com.sgcc.devops.dao.intf.AuthorityMapper;
import com.sgcc.devops.service.AuthorityService;

import net.sf.json.JSONObject;

/**
 * date：2015年8月14日 上午10:48:23 project name：sgcc-devops-service
 * 
 * @author langzi
 * @version 1.0
 * @param <AuthorityMapper>
 * @since JDK 1.7.0_21 file name：UserServiceImpl.java description：
 */
@Component
public class AuthorityServiceImpl implements AuthorityService {

	private static Logger logger = Logger.getLogger(AuthorityServiceImpl.class);
	@Resource
	private AuthorityMapper authorityMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public GridBean list(String owner, int pageNum, int pageSize, Authority authority) throws Exception {

		PageHelper.startPage(pageNum, pageSize);
		if (null != authority.getActionName()) {
			authority.setActionName(authority.getActionName());
		}
		if (null != owner) {
		}
		authority.setDialect(jdbcConfig.getDialect());
		List<Authority> authoritys = authorityMapper.selectAll(authority);
		int total = ((Page<?>) authoritys).getPages();
		int records = (int) ((Page<?>) authoritys).getTotal();
		GridBean gridbean = new GridBean(pageNum, total, records, authoritys);
		return gridbean;
	}

	@Override
	public int update(Authority authority) {

		try {
			return authorityMapper.updateByPrimaryKey(authority);
		} catch (Exception e) {
			logger.error("update authority fail" + e);
			return 0;
		}

	}

	@Override
	public List<Authority> getAllAuthList(String userId, Authority authority) throws Exception {
		authority.setDialect(jdbcConfig.getDialect());
		List<Authority> auths = authorityMapper.selectAll(authority);
		return auths;
	}

	@Override
	public List<Authority> ListByActionParentId(String actionParentId) throws Exception {

		List<Authority> authParents = authorityMapper.selectListByActionParentId(actionParentId);
		return authParents;
	}

	@Override
	public List<Authority> listAuthsByUserId(String userId) throws Exception {
		return authorityMapper.selectAllByUserId(userId);
	}

	@Override
	public GridBean advancedSearchAuth(String userId, int pagenum, int pagesize, Authority authority,
			JSONObject json_object) throws Exception {

		PageHelper.startPage(pagenum, pagesize);
		/* 组装应用查询数据的条件 */
		Authority authoritySraech = new Authority();
		// userSraech.setUserStatus(((byte)Status.USER.NORMAL.ordinal()));
		/* 获取用户填写的各项查询条件 */
		String[] params = json_object.getString("params").split(",");
		String[] values = json_object.getString("values").split(",");

		/* 遍历填充各项查询条件 */
		for (int array_count = 0, array_length = params.length; array_count < array_length; array_count++) {
			switch (params[array_count].trim()) {
			case ("1"): {
				authoritySraech.setActionName(values[array_count].trim());
				break;
			}
			case "2": {
				authoritySraech.setActionDesc(values[array_count].trim());
				break;
			}
			case "3": {
				authoritySraech.setActionRelativeUrl(values[array_count].trim());
				break;
			}
			case "4": {
				/** @bug211_begin: [权限管理]高级查询,以权限类型为条件列,检索无效 */
				if ("父节点".indexOf(values[array_count].trim()) != -1) {
					authoritySraech.setActionType((byte) Type.AUTHORITY.PARENT.ordinal());
				} else if ("子节点".indexOf(values[array_count].trim()) != -1) {
					authoritySraech.setActionType((byte) Type.AUTHORITY.CHILD.ordinal());
				} else {
					authoritySraech.setActionType((byte) Integer.MAX_VALUE);
				}
				/** @bug211_finish */
				break;
			}
			case "5": {
				authoritySraech.setActionRemarks(values[array_count].trim());
				break;
			}
			}
		}
		authoritySraech.setDialect(jdbcConfig.getDialect());
		List<Authority> authoritys = authorityMapper.selectAll(authoritySraech);
		int totalpage = ((Page<?>) authoritys).getPages();
		Long totalNum = ((Page<?>) authoritys).getTotal();
		GridBean gridBean = new GridBean(pagenum, totalpage, totalNum.intValue(), authoritys);
		return gridBean;
	}

	@Override
	public List<Authority> getAuthListByRoleId(String roleId, String authId) throws Exception {
		Map<String, String> map = new HashMap<String, String>();
		map.put("roleId", roleId);
		map.put("authId", authId);
		List<Authority> authParents = authorityMapper.getAuthListByRoleId(map);
		return authParents;
	}

}
