/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.model.HostComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.HostInstall;
import com.sgcc.devops.dao.intf.HostComponentMapper;
import com.sgcc.devops.dao.intf.RackMapper;
import com.sgcc.devops.service.HostComponentService;

@Component
public class HostComponentServiceImpl implements HostComponentService {
	private static Logger logger = Logger.getLogger(HostComponentServiceImpl.class);
	@Resource
	private HostComponentMapper hostComponentMapper;
	@Resource
	private RackMapper rackMapper;
	@Resource
	private JdbcConfig jdbcConfig;

	
	@Override
	public GridBean hostComponentListAll(PagerModel pagerModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("serverRoomRemark")){
				PageHelper.startPage(page, pageSize,"SERVER_ROOM_REMARK "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		List<HostComponent> hostComponents = hostComponentMapper.selectAll();
		int totalpage = ((Page<?>) hostComponents).getPages();
		Long totalNum = ((Page<?>) hostComponents).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), hostComponents);
		return gridBean;
	}
	@Override
	public GridBean hostComponentList(PagerModel pagerModel, HostComponentModel hostComponentModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		HostComponent hostComponent = new HostComponent(hostComponentModel.getId(), hostComponentModel.getType(), hostComponentModel.getVersion(),
				hostComponentModel.getFilename(), hostComponentModel.getStatusCommand() , hostComponentModel.getInstallCommand() ,
				hostComponentModel.getVersionCommand() , hostComponentModel.getUpgradeCommand() , hostComponentModel.getTransfer() ,
				hostComponentModel.getCreator() , hostComponentModel.getCreateTime(),hostComponentModel.getLocalPath() );
		hostComponent.setDialect(jdbcConfig.getDialect());
		List<HostComponent> HostComponents = hostComponentMapper.selectComByCon(hostComponent);
		int totalpage = ((Page<?>) HostComponents).getPages();
		Long totalNum = ((Page<?>) HostComponents).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), HostComponents);
		return gridBean;
	}
	@Override
	public HostComponent getHostComponent(HostComponent hostComponent) {
		try {
			hostComponent = hostComponentMapper.selectByPrimaryKey(hostComponent.getId());
		} catch (Exception e) {
			logger.error("select cluster fail: " + e.getMessage());
			return null;
		}
		return hostComponent;
	}
	@Override
	public int deleteByPrimaryKey(String id) throws Exception {
		int result = 1;
		try {
			hostComponentMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error("delete hostComponent fail: " + e.getMessage());
			result = 0;
		}
		return result;
		
	}
	@Override
	public HostComponent selectById(String id) {
		return hostComponentMapper.selectById(id);
		
	}
	@Override
	public boolean ifRepeat(HostComponentModel hostComponentModel){
		boolean repeat = false;
		try{
			String version = hostComponentModel.getVersion();
			String type =hostComponentModel.getType();
			HostComponent hostComponent = new HostComponent();
			hostComponent.setVersion(version);
			hostComponent.setType(type);
			int num = hostComponentMapper.countName(hostComponent);
			if(num > 0){
				repeat = true;
			}
		}catch(Exception e){
			repeat = true;
			logger.error("count hostComponent fail: " + e.getMessage());
		}
		return repeat;
	}
	@Override
	public int insert(HostComponentModel hostComponentModel) throws Exception {
		int result = 1;
		try{
			TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
			HostComponent hostComponent = new HostComponent(hostComponentModel.getId(), hostComponentModel.getType(), hostComponentModel.getVersion(),
					hostComponentModel.getFilename(), hostComponentModel.getStatusCommand() , hostComponentModel.getInstallCommand() ,
					hostComponentModel.getVersionCommand() , hostComponentModel.getUpgradeCommand() , hostComponentModel.getTransfer() ,
					hostComponentModel.getCreator() , new Date() ,hostComponentModel.getLocalPath());
			hostComponentMapper.insert(hostComponent);
		} catch (Exception e) {
			logger.error("insert hostComponent fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}
	@Override
	public int updateByPrimaryKeySelective(HostComponent hostComponent) {
		int result = 1;
		try{
			hostComponentMapper.updateByPrimaryKeySelective(hostComponent);
		}catch(Exception e){
			result = 0;
			logger.error("update rack fail:" +e.getMessage());
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostComponentService#selectHostInstallByHostId(java.lang.String)
	 */
	@Override
	public List<HostInstall> selectHostInstallByHostId(String hostId) {
		List<HostInstall> hostInstalls = hostComponentMapper.selectHostInstallByHostId(hostId);
		return hostInstalls;
	}
	@Override
	public Integer countHostComp() {
		return hostComponentMapper.countHostComp();
	}
}
	