/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.DeployDatabase;

/**  
 * date：2016年2月4日 下午3:02:13
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployDatabaseService.java
 * description：部署数据库业务数据维护接口类
 */
public interface DeployDatabaseService {

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年10月30日
	 */
	int create(DeployDatabase deployDatabase);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return void
	 * @version 1.0 2015年11月10日
	 */
	List<DeployDatabase> selectByDatabaseId(String databaseId);
	List<DeployDatabase> selectByNginxId(String databaseId);
	
	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年11月17日
	 */
	List<DeployDatabase> selectByDeployId(String deployId);

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年1月27日
	*/
	void deleteByDeployID(String deployId);
}
