package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.dao.entity.Logs;
import com.sgcc.devops.dao.intf.LogsMapper;
import com.sgcc.devops.dao.intf.UserMapper;
import com.sgcc.devops.service.LogsService;

/**  
 * date：2016年2月4日 下午3:22:57
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogsService.java
 * description：运行日志数据维护接口实现类
 */
@Service("logsService")
public class LogsServiceImpl implements LogsService {

	@Autowired
	private LogsMapper logsMapper;
	@Resource
	private UserMapper userMapper;

	@Override
	public GridBean list(Logs logs, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<Logs> logsList = logsMapper.selectAll(logs);
		int totalpage = ((Page<?>) logsList).getPages();
		Long totalNum = ((Page<?>) logsList).getTotal();
		GridBean gridBean = new GridBean(pageNum, totalpage, totalNum.intValue(), logsList);
		return gridBean;
	}

}
