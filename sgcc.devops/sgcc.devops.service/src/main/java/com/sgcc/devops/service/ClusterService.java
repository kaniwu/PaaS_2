package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ClusterWithIPAndUser;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午2:23:41
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ClusterService.java
 * description： 集群业务数据操作接口类 
 */
public interface ClusterService {

	/**
	 * @author luogan
	 * @param cluster
	 * @return create cluster
	 * @version 1.0 2015年8月28日
	 * @param host
	 */
	public abstract String createCluster(JSONObject jo, Host host);

	/**
	 * @author luogan
	 * @param cluster
	 * @return delete cluster
	 * @version 1.0 2015年8月28日
	 */
	public abstract int deleteCluster(JSONObject jo);

	/**
	 * @author luogan
	 * @param cluster
	 * @return delete cluster
	 * @version 1.0 2015年8月28日
	 */
	public abstract int deleteClusters(JSONObject jo);

	/**
	 * @author luogan
	 * @param cluster
	 * @return update cluster
	 * @version 1.0 2015年8月28日
	 */
	public abstract int update(Cluster cluster);

	/**
	 * @author luogan
	 * @param cluster
	 * @return update cluster
	 * @version 1.0 2015年8月28日
	 */
	public abstract int updateCluster(JSONObject jo);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterList
	 * @version 1.0 2015年8月28日
	 */
	public abstract GridBean getOnePageClusterList(User user, PagerModel pagerModel,
			ClusterWithIPAndUser clusterWithIPAndUser);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterList
	 * @version 1.0 2015年8月28日
	 */
	public abstract List<ClusterWithIPAndUser> getAllClusterList(User user,
			ClusterWithIPAndUser clusterWithIPAndUser);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterList
	 * @version 1.0 2015年8月28日
	 */
	public abstract List<Cluster> listClustersByhostId(String hostId);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterList
	 * @version 1.0 2015年8月28日
	 */
//	public abstract List<Cluster> getOnePageClusterMasterListxx();

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterList
	 * @version 1.0 2015年8月28日
	 */
	public abstract int countAllClusterList();

	/**
	 * @author luogan
	 * @param cluster
	 * @return select cluster
	 * @version 1.0 2015年8月28日
	 */
	public Cluster getCluster(Cluster cluster);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select cluster
	 * @version 1.0 2015年8月28日
	 */
	public JSONObject getJsonObjectOfCluster(Cluster cluster);

	/**
	 * @author luogan
	 * @param cluster
	 * @return select clusterByHost
	 * @version 1.0 2015年8月28日
	 */
	public Cluster getClusterByHost(Host host);

	/**
	 * @author zhangxin
	 * @param cluster
	 * @return select allcluster
	 * @version 1.0 2015年11月19日
	 */
	public List<Cluster> getAllCluster();

	/**
	 * @author zhangxin
	 * @param cluster
	 * @return select cluster by name
	 * @version 1.0 2015年11月19日
	 */
	public Cluster selectClusterByName(Cluster cluster);
	
	/**根据集群ID获取集群信息
	 * @param clusterId
	 * @return
	 */
	public Cluster getCluster(String clusterId);

	/**
	* TODO
	* @author mayh
	* @return List<Cluster>
	* @version 1.0
	* 2016年8月5日
	*/
	public List<Cluster> selectAllCluster(Cluster cluster);
	
}
