/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RegIdImageTypeModel;
import com.sgcc.devops.common.model.RegistryLbModel;
import com.sgcc.devops.common.model.RegistryModel;
import com.sgcc.devops.dao.entity.Registry;

/**  
 * date：2016年2月4日 下午3:34:34
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RegistryService.java
 * description：镜像仓库数据维护接口类
 */
public interface RegistryService {
	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年8月24日
	 */
	public JSONArray getOnePageRegistrys(String userId, RegistryModel RegistryModel);

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 11:00
	 * @description 将原来返回字符串的方法修改为GridBean的方式
	 */
	public GridBean getOnePageRegistrys(String userId, int pagenumber, int pagesize, RegistryModel RegistryModel);

	/**
	 * @author youngtsinglin
	 * @time 2015年9月9日 09:26
	 * @description 将原来返回字符串的方法修改为GridBean的方式
	 */
	public GridBean getOnePageRegistrysWithIP(String userId,PagerModel pagerModel,RegistryLbModel RegistryLbModel);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月24日
	 */
	public int createRegistry(JSONObject jo);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月24日
	 */
	public List<Registry> listRegistrysByHostId(String hostId) throws Exception;

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月24日
	 */
	public int updateRegistry(JSONObject jo);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月24日
	 */
	public int deleteRegistry(JSONObject jo);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年8月27日
	 */
	public JSONObject getRegistry(Registry record);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年8月27日
	 */
	public JSONArray getRegistryMster();

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return boolean
	 * @version 1.0 2015年8月31日
	 */
	public String checkImageIsExist(JSONObject jo);

	/**
	 * @author youngtsinglin
	 * @time 2015年9月9日 09:26
	 * @description 添加对于特定仓库下所有的镜像方法，返回GridBean类型
	 */
	GridBean getOnePageRegistrysSlaveImages(String userId,PagerModel pagerModel,
			RegIdImageTypeModel regIdImageTypeModel, String registryid, byte imagestatus);

	/**
	 * TODO
	 * 
	 * @author yangqinglin
	 * @return int
	 * @version 1.0 2015年9月16日
	 */
	public int SyncRegistryImages(String RegistryNodeIP, String ServerPort);

	public int registryCount();

	/**
	 * TODO
	 * 
	 * @author zx
	 * @return Registry
	 * @version 1.0 2015年11月26日
	 */
	public Registry selectRegistry(Registry registry);

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年4月13日
	*/
	public List<Registry> selectAll();
	
	public int updateRegistrySta(JSONObject jo);
	
	public List<Registry> selectRegistryByLbId(String LbId);
	
	public String checkImageIsExistByID(String id);
}
