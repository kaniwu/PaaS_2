package com.sgcc.devops.service;

import java.util.Date;

/**  
 * date：2016年2月4日 下午3:20:40
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogBackupService.java
 * description：日志备份数据维护接口类
 */
public interface LogBackupService {

	public abstract boolean trsOvertimedLog(Date savetime);
}
