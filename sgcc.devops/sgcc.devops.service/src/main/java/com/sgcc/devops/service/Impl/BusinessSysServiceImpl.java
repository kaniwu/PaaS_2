/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.BusinessSys;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.BusinessSysMapper;
import com.sgcc.devops.dao.intf.SystemMapper;
import com.sgcc.devops.service.BusinessSysService;
import com.sgcc.devops.dao.entity.System;
/**  
 * date：2015年12月30日 下午12:17:21
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessSysService.java
 * description：  业务系统与物理系统关联数据处理接口实现类
 */
@Component
public class BusinessSysServiceImpl implements BusinessSysService{
	@Resource
	private BusinessSysMapper businessSysMapper;
	@Resource
	private SystemMapper systemMapper;
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.BusinessSysService#load(com.sgcc.devops.dao.entity.BusinessSys)
	 */
	@Override
	public List<BusinessSys> load(BusinessSys businessSys) {
		
		return businessSysMapper.load(businessSys);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.BusinessSysService#getSysByBusinessId(com.sgcc.devops.dao.entity.User, java.lang.String)
	 */
	@Override
	public JSONArray getSysByBusinessId(User user, String businessId,String envId) {
		BusinessSys businessSys = new BusinessSys(businessId);
		List<BusinessSys> list = businessSysMapper.load(businessSys);
		List<System> systems = new ArrayList<System>();
		for(BusinessSys businessSys2:list){
			System system = systemMapper.selectByPrimaryKey(businessSys2.getSysId());
			if(StringUtils.isNotEmpty(envId)&&!envId.equals(system.getEnvId())){
				continue;
			}
			systems.add(system);
		}
		if(null==systems||systems.size()==0){
			return null;
		}else{
			return JSONUtil.parseObjectToJsonArray(systems);
		}
	}
}
