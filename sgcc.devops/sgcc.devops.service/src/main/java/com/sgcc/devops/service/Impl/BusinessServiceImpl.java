/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Business;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.BusinessMapper;
import com.sgcc.devops.service.BusinessService;

/**  
 * date：2016年2月4日 下午2:03:11
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessService.java
 * description：  业务系统是数据操作接口实现类
 */
@Component
public class BusinessServiceImpl implements BusinessService {

	private static Logger logger = Logger.getLogger(BusinessServiceImpl.class);

	@Resource
	private BusinessMapper businessMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.BusinessService#listAllOfBusiness(com.sgcc.devops
	 * .dao.entity.User)
	 */
	@Override
	public GridBean listAllOfBusiness(User user,PagerModel pagerModel,String businessName) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("businessName")){
				PageHelper.startPage(page, pageSize,"BUSSINESS_NAME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		
		Business business = new Business();
		business.setBusinessName(businessName);
		business.setDialect(jdbcConfig.getDialect());
		List<Business> businesses = businessMapper.listAllOfBusiness(business);
		int totalpage = ((Page<?>) businesses).getPages();
		Long totalNum = ((Page<?>) businesses).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), businesses);
		return gridBean;
	}

	@Override
	public int businessCount() {
		int result = 0;
		try {
			result = businessMapper.selectBusinessCount();
		} catch (Exception e) {
			logger.error("select bussiness count fail" + e.getMessage());
			return 0;
		}
		return result;
	}

	@Override
	public Business load(String businessId) {

		return businessMapper.selectByPrimaryKey(businessId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.BusinessService#update(com.sgcc.devops.dao.entity.Business)
	 */
	@Override
	public void update(Business business) throws Exception{
		businessMapper.update(business);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.BusinessService#countDeployStatus(int)
	 */
	@Override
	public Integer countDeployed() {
		return businessMapper.countDeployed();
	}
}
