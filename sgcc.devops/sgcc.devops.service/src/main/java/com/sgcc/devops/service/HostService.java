package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.HostInstall;
import com.sgcc.devops.dao.entity.User;

/**  
 * date：2016年2月4日 下午3:18:03
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：HostService.java
 * description：主机数据维护接口类
 */
public interface HostService {

	/**
	 * @author luogan
	 * @param host
	 * @return create host
	 * @version 1.0 2015年8月28日
	 */
	public abstract String createHost(JSONObject jo);

	/**
	 * @author luogan
	 * @param host
	 * @return delete host
	 * @version 1.0 2015年8月28日
	 */
	public abstract int deleteHost(JSONObject jo);

	/**
	 * @author luogan
	 * @param host
	 * @return delete host
	 * @version 1.0 2015年8月28日
	 */
	public abstract int deleteHosts(JSONObject jo);

	/**
	 * @author luogan
	 * @param host
	 * @return update host
	 * @version 1.0 2015年8月28日
	 */
	public abstract int update(Host host);

	/**
	 * @author luogan
	 * @param host
	 * @return select hostList
	 * @version 1.0 2015年8月28日
	 */
	public abstract GridBean getOnePageHostList(User user,PagerModel pagerModel, HostModel hostModel);

	/**
	 * @author luogan
	 * @param host
	 * @return select hostList
	 * @version 1.0 2015年8月28日
	 */
	public abstract int countAllHostList(Host host);

	/**
	 * @author luogan
	 * @param host
	 * @return select host
	 * @version 1.0 2015年8月28日
	 */
	public Host getHost(Host host);

	/**
	 * 获取主机信息
	 * 
	 * @param hostId
	 *            主机ID
	 * @return 主机实体类
	 */
	public Host getHost(String hostId);

	/**
	 * @author langzi
	 * @param hostIp
	 * @return
	 * @version 1.0 2015年9月11日
	 */
	public Host getHostByIp(String hostIp);

	/**
	 * @author luogan
	 * @param host
	 * @return select host
	 * @version 1.0 2015年8月28日
	 */
	public JSONObject getJsonObjectOfHost(Host host);

	/**
	 * @author luogan
	 * @param host
	 * @return select ip
	 * @version 1.0 2015年8月28日
	 */
	public abstract boolean queryIP(HostModel hostModel);

	/**
	 * @author langzi
	 * @param type
	 * @return
	 * @version 1.0 2015年9月11日
	 */
//	public List<Host> listHostByType(int type);

	/**
	 * @author langzi
	 * @param type
	 * @return
	 * @version 1.0 2015年9月11日
	 */
	public List<Host> listHostByTypeAndCluster(int type);

	/**
	 * @author luogan
	 * @param host
	 * @return select ip
	 * @version 1.0 2015年8月28日
	 */
	public List<Host> listHostByClusterId(String clusterId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年10月12日
	 */
	public JSONArray freeHostList(User user);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return List<Host>
	 * @version 1.0 2015年10月19日
	 */
	public List<Host> selectHostListByHost(Host host);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return List<Host>
	 * @version 1.0 2015年11月3日
	 */
	public List<Host> selectAllHost(Host host);

	/**
	* TODO
	* @author mayh
	* @return List<Host>
	* @version 1.0
	* 2016年3月8日
	*/
	public List<Host> allOnlineHost();

	/**
	* @author yueyong
	* @return List<ComponentHost>
	* @version 1.0
	* 2016年3月9日
	 */
	public List<ComponentHost> selectComponentHost(String componentId);

	/**
	* TODO
	* @author mayh
	* @return List<Host>
	* @version 1.0
	* 2016年3月16日
	*/
	public abstract List<Host> hostListByRack(Host host);
	
	public abstract byte[] exportHost(String locationId);
	
	public abstract byte[] importHostExcel();
	
	public int insert(HostInstall hostInstall);
	public List<HostInstall> selectAll(HostInstall hostInstall);
	public int delete(String id);

	public abstract JSONArray grateClusterHost(User user,String clusterId);
}
