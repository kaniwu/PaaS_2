package com.sgcc.devops.service;

import java.util.Date;
import java.util.List;

import com.sgcc.devops.dao.entity.Monitor;

/**  
 * date：2016年2月4日 下午3:24:56
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：MonitorService.java
 * description：主机、容器监控数据维护接口类
 */
public interface MonitorService {

	public int insert(Monitor monitor);

	public int deleteByPrimaryKey(String monitorId);

	public int deleteByType(byte targetType);

	public int deleteBytargetId(String targetId);

	public Monitor selectByPrimaryKey(String monitorId);

	public List<Monitor> selectByType(byte targetType);

	public List<Monitor> selectBytargetId(String targetId);

	public List<Monitor> select(Monitor monitor);

	public List<Monitor> selectByTime(Date startTime, Date endTime);

	/** 根据 type targetId 查寻最近的一条数据 */
	public Monitor selectByTypeAndId(Monitor monitor);

	/**
	 * 获取指定监控项类型和监控目标在过去的一段时间内的监控数据
	 * 
	 * @param targetType
	 *            监控目标类型
	 * @param targetId
	 *            监控目标ID
	 * @param lastTime
	 *            从现在开始向前的一段时间，单位为S
	 * @return
	 */
	public List<Monitor> selectLastMonitorData(byte targetType, String targetId, Long lastTime);

	/**
	* TODO
	* @author mayh
	* @return Map<String,Object>
	* @version 1.0
	* 2016年6月20日
	*/
	public Monitor getAvgData(Monitor monitor);
}
