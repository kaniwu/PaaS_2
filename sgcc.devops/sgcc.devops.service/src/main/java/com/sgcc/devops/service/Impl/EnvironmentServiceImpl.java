/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.EnvironmentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.intf.EnvironmentMapper;
import com.sgcc.devops.dao.intf.SystemAppMapper;
import com.sgcc.devops.dao.intf.SystemMapper;
import com.sgcc.devops.service.EnvironmentService;

/**  
 * date：2016年4月11日 上午9:24:26
 * project name：sgcc.devops.service
 * @author  panjing
 * @version 1.0   
 * @since JDK 1.7.0_21  
 */
@Component
public class EnvironmentServiceImpl implements EnvironmentService {
	private static Logger logger = Logger.getLogger(EnvironmentServiceImpl.class);

	@Resource
	private EnvironmentMapper environmentMapper;
	@Resource
	private SystemMapper systemMapper;
	@Resource
	private SystemAppMapper systemAppMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public GridBean environmentList(PagerModel pagerModel,EnvironmentModel environmentModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("eName")){
				PageHelper.startPage(page, pageSize,"E_NAME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		Environment environment = new Environment();

		if (environmentModel.geteName() != null) {
			environment.seteName(environmentModel.geteName());
		}
		environment.setDialect(jdbcConfig.getDialect());
		List<Environment> environments = environmentMapper.selectEnvironment(environment);
		int totalpage = ((Page<?>) environments).getPages();
		Long totalNum = ((Page<?>) environments).getTotal();
		List<EnvironmentModel> eModels = new ArrayList<EnvironmentModel>();
		for (Environment tempE : environments) {
			EnvironmentModel eModel = new EnvironmentModel();
			eModel.seteId(tempE.geteId());
			eModel.seteName(tempE.geteName());
			eModel.seteCreator(tempE.geteCreator());
			eModel.seteTime(tempE.geteTime());
			eModel.seteCreatorName(tempE.getUserName());
			eModel.seteDesc(tempE.geteDesc());
			eModels.add(eModel);
		}
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), eModels);
		return gridBean;
	}


	@Override
	public int insert(Environment record) {
		int result = 1;
		try{
			//所有物理系统赋予新增环境变量
			List<System> systems = systemMapper.selectAll(null);
			List<String> systemNames = new ArrayList<>();
			for(System system:systems){
				if(systemNames.contains(system.getSystemName())){
					continue;
				}else{
					systemNames.add(system.getSystemName());
				}
				String oldSystemId =system.getSystemId();
				String systemId = KeyGenerator.uuid();
				system.setSystemId(systemId);
				system.setEnvId(record.geteId());
				system.setSystemDeployStatus((byte) 2);
				system.setSystemElsasticityStatus((byte) 2);
				system.setOperation((byte)Type.OPERATION.UNUSED.ordinal());
				system.setDeployId("");
				int re = systemMapper.insertSystem(system);
				if(re==1){
					List<SystemApp> apps = systemAppMapper.selectSystemApps(oldSystemId);
					for(SystemApp app:apps){
						if(app.getStatus()==Status.SYSTEMAPP.NORMAL.ordinal()){
							app.setId(KeyGenerator.uuid());
							app.setSystemId(systemId);
							systemMapper.insertSystemApp(app);
						}
					}
				}
			}
			environmentMapper.insert(record);
		}catch(Exception e){
			result = 0;
			logger.error("insert environment fail:" + e.getMessage());
		}
		return result;
	}

	@Override
	public int deleteByPrimaryKey(String id) {
		int result = 1;
		try {
			environmentMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error("delete environment fail: " + e.getMessage());
			result = 0;
		}
		return result;
		
	}


	@Override
	public int updateByPrimaryKeySelective(Environment environment) {
		int result = 1;
		try{
			environmentMapper.updateByPrimaryKeySelective(environment);
		}catch(Exception e){
			result = 0;
			logger.error("update environment fail:" +e.getMessage());
		}
		return result;
	}


	@Override
	public List<Environment> environmentsList(Environment environment) {
		List<Environment> environments = environmentMapper.selectEnvironments(environment);
		return environments;
	}


	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.EnvironmentService#environmentById(com.sgcc.devops.dao.entity.Environment)
	 */
	@Override
	public Environment environmentById(String eId) {
		return environmentMapper.selectByPirmaryKey(eId);
	}
	
}
