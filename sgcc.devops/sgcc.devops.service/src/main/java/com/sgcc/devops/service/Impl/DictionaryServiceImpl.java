/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Service;

import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.Dictionary;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.ComponentMapper;
import com.sgcc.devops.dao.intf.DictionaryMapper;
import com.sgcc.devops.service.DictionaryService;

/**  
 * date：2016年2月4日 下午3:11:28
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DictionaryService.java
 * description：  数据字典数据维护接口实现类
 */
@Service("dictionaryService")
public class DictionaryServiceImpl implements DictionaryService {
	@Resource
	private DictionaryMapper dictionaryMapper;
	@Resource
	private ComponentMapper componentMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DictionaryService#listByDatabaseId(java.lang.
	 * String, com.sgcc.devops.dao.entity.User)
	 */
	@Override
	public JSONArray listByDatabaseId(String databaseId, User user) {
		Component component = new Component();
		component.setComponentId(databaseId);
		component = componentMapper.selectComponent(component);
		JSONArray jaArray = new JSONArray();
		List<Dictionary> dictionaries = new ArrayList<>();
		if (databaseId != null) {
			if (0 == component.getComponentDBType()) {// oracle
				dictionaries = dictionaryMapper.selectAllbyKey("oracle");
			}
			if (1 == component.getComponentDBType()) {// mysql
				dictionaries = dictionaryMapper.selectAllbyKey("mysql");
			}
			if (2 == component.getComponentDBType()) {// db2
				dictionaries = dictionaryMapper.selectAllbyKey("db2");
			}
			jaArray = JSONUtil.parseObjectToJsonArray(dictionaries);
		}

		return jaArray;
	}

}
