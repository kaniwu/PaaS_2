package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Log;
import com.sgcc.devops.dao.intf.LogMapper;
import com.sgcc.devops.dao.intf.UserMapper;
import com.sgcc.devops.service.LogService;

/**  
 * date：2016年2月4日 下午3:21:13
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogService.java
 * description：操作日志数据维护接口实现类
 */
@Service("logService")
public class LogServiceImpl implements LogService {

	@Autowired
	private LogMapper logMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public GridBean list(Log log,PagerModel pagerModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("logAction")){
				PageHelper.startPage(page, pageSize,"LOG_ACTION "+sord);
			}
			if(sidx.equals("logCreatetime")){
				PageHelper.startPage(page, pageSize,"LOG_CREATETIME "+sord);
			}
			if(sidx.equals("userName")){
				PageHelper.startPage(page, pageSize,"USER_NAME "+sord);
			}
			if(sidx.equals("userIp")){
				PageHelper.startPage(page, pageSize,"USER_IP "+sord);
			}
			if(sidx.equals("logResult")){
				PageHelper.startPage(page, pageSize,"LOG_RESULT "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		log.setDialect(jdbcConfig.getDialect());
		List<Log> logList = logMapper.selectAll(log);
		int totalpage = ((Page<?>) logList).getPages();
		Long totalNum = ((Page<?>) logList).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), logList);
		return gridBean;
	}

	@Override
	public Result save(Log log) {
		int result = this.logMapper.insertLog(log);
		return new Result(result != 0, "add log " + (result != 0));
	}
}
