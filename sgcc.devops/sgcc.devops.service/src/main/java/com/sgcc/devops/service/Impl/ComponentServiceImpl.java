package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.model.ComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.entity.DeployNgnix;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.Svn;
import com.sgcc.devops.dao.intf.ComponentExpandMapper;
import com.sgcc.devops.dao.intf.ComponentMapper;
import com.sgcc.devops.dao.intf.DeployDatabaseMapper;
import com.sgcc.devops.dao.intf.DeployMapper;
import com.sgcc.devops.dao.intf.DeployNgnixMapper;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.dao.intf.RegistryLbMapper;
import com.sgcc.devops.dao.intf.SvnMapper;
import com.sgcc.devops.service.ComponentService;

import net.sf.json.JSONObject;
/**  
 * date：2016年2月4日 下午2:48:29
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentService.java
 * description：组件业务数据操作接口实现类  
 */
@org.springframework.stereotype.Component
public class ComponentServiceImpl implements ComponentService {

	private static Logger logger = Logger.getLogger(ComponentServiceImpl.class);

	@Resource
	private ComponentMapper componentMapper;
	@Resource
	private SvnMapper svnMapper;
	@Resource
	private DeployNgnixMapper deployNgnixMapper;
	@Resource
	private DeployMapper deployMapper;
	@Resource
	private DeployDatabaseMapper deployDatabaseMapper;
	@Resource
	private ComponentExpandMapper componentExpandMapper;
	@Resource
	private HostMapper hostMapper;
	@Resource
	private RegistryLbMapper lbMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public int createComponent(ComponentModel componentModel,List<ComponentHost> componentHostList) {
		int result = 1;
		Component record = new Component();
		BeanUtils.copyProperties(componentModel, record);
		String majorKey = KeyGenerator.uuid();
		record.setComponentId(majorKey);
		record.setComponentStatus((byte) Status.COMPONENT.NORMAL.ordinal());
		if (componentModel.getComponentType() == 3) {
			record.setComponentJdbcurl(componentModel.getComponentIp());
		}
		if(6==componentModel.getComponentType()){
			record.setComponentIp(componentModel.getComponentExpand().getF5ExternalIp());
		}
		try {
			componentMapper.insertComponent(record);
//			if(componentModel.getComponentType() == 7){
//			Svn  svn = new Svn(record.getComponentId(),record.getComponentUserName(),record.getComponentPwd(),record.getComponentIp());
//			svnMapper.insertSvn(svn);
//			}
			//扩展信息保存
			if(componentModel.getComponentExpand()!=null){
				ComponentExpand expand = new ComponentExpand();
				BeanUtils.copyProperties(componentModel.getComponentExpand(), expand);
				expand.setCompId(majorKey);
				ComponentExpand expandExist = componentExpandMapper.selectByCompId(majorKey);
				if(expandExist==null){
					//新增
					componentExpandMapper.insertComponentExpand(expand);
				}else{
					//更新
					componentExpandMapper.updateComponentExpand(expand);
				}
			}
			//组件 附属主机信息保存
			if(componentHostList!=null && componentHostList.size()>0){
				for(ComponentHost componentHost : componentHostList){
					if(StringUtils.isEmpty(componentHost.getComponentHostId()))
						componentHost.setComponentHostId(KeyGenerator.uuid());
					componentHost.setDopComponentId(majorKey);
					hostMapper.insertComponentHost(componentHost);
					//更新主机类型
//					Host host = new Host();
//					host.setHostId(componentHost.getHostId());
//					host.setHostType((byte) Type.HOST.NGINX.ordinal());
//					hostMapper.updateHost(host);
				}
			}
		} catch (Exception e) {
			logger.error("create component fail" + e);
			result = 0;
			return result;
		}
		
		return result;
	}

	@Override
	public int updateComponent(ComponentModel componentModel,List<ComponentHost> componentHostList) {
		int result = 1;
		Component record = new Component();
		BeanUtils.copyProperties(componentModel, record);
		record.setComponentStatus((byte) Status.COMPONENT.NORMAL.ordinal());
		if (componentModel.getComponentType() == 3) {
			record.setComponentJdbcurl(componentModel.getComponentIp());
		}
		if(6==componentModel.getComponentType()){
			ComponentExpand componentExpand = componentExpandMapper.selectByCompId(componentModel.getComponentId());
			record.setComponentIp(componentExpand.getF5ExternalIp());
		}
		try {
			componentMapper.updateComponent(record);
			//TODO
			if(componentModel.getComponentType() == 7){
				Svn  svn = new Svn(record.getComponentId(),record.getComponentUserName(),record.getComponentPwd(),record.getComponentIp());
				svnMapper.updateSvn(svn);
				}
			//扩展信息保存
			if(componentModel.getComponentExpand()!=null){
				ComponentExpand expand = new ComponentExpand();
				BeanUtils.copyProperties(componentModel.getComponentExpand(), expand);
				expand.setCompId(record.getComponentId());
				
				ComponentExpand expandExist = componentExpandMapper.selectByCompId(record.getComponentId());
				if(expandExist==null){
					//新增
					componentExpandMapper.insertComponentExpand(expand);
				}else{
					//更新
					componentExpandMapper.updateComponentExpand(expand);
				}
			}
			//组件 附属主机信息保存
			if(componentHostList!=null && componentHostList.size()>0){
				for(ComponentHost componentHost : componentHostList){
					componentHost.setDopComponentId(record.getComponentId());
					if(StringUtils.isEmpty(componentHost.getComponentHostId())){
						//新增
						componentHost.setComponentHostId(KeyGenerator.uuid());
						hostMapper.insertComponentHost(componentHost);
						//更新主机类型
//						Host host = new Host();
//						host.setHostId(componentHost.getHostId());
//						host.setHostType((byte) Type.HOST.NGINX.ordinal());
//						hostMapper.updateHost(host);
					}else if(componentHost.getDelFlag()!=null && componentHost.getDelFlag().equals("1")){
						//删除
						hostMapper.deleteCompHost(componentHost.getComponentHostId());
						//更新主机类型
//						Host host = new Host();
//						host.setHostId(componentHost.getHostId());
//						host.setHostType((byte) Type.HOST.UNUSED.ordinal());
//						hostMapper.updateHost(host);
					}else{
						//更新
						hostMapper.updateComponentHost(componentHost);
					}
				}
			}
		} catch (Exception e) {
			logger.info("update component fail:" + e);
			result = 0;
		}
		
		return result;
	}

	@Override
	public GridBean getOnePageComponentList(String userId,PagerModel pagerModel, ComponentModel componentModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("componentTypeValue")){
				PageHelper.startPage(page, pageSize,"COMPONENT_TYPE "+sord);
			}
			if(sidx.equals("componentName")){
				PageHelper.startPage(page, pageSize,"COMPONENT_NAME "+sord);
			}
			if(sidx.equals("componentIp")){
				PageHelper.startPage(page, pageSize,"COMPONENT_IP "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		if(null==componentModel.getComponentType()){
			GridBean gridBean = new GridBean(page, 0,0, null);
			return gridBean;
		}
		if(0==componentModel.getComponentType()){
			componentModel.setComponentType(null);
		}
		Component component = new Component();
		component.setComponentName(componentModel.getComponentName());
		component.setComponentType(componentModel.getComponentType());
		component.setComponentId(componentModel.getComponentId());
		component.setDialect(jdbcConfig.getDialect());
		List<Component> components = componentMapper.selectAllComponent(component);
		int totalpage = ((Page<?>) components).getPages();
		Long totalNum = ((Page<?>) components).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), components);
		return gridBean;
	}

	@Override
	public Component getComponent(Component component) {
		try {
			return componentMapper.selectComponent(component);
		} catch (Exception e) {
			logger.error("get component fail" + e);
			return null;
		}
	}

	@Override
	public JSONObject getJsonObjectOfComponent(Component component) {
		return JSONUtil.parseObjectToJsonObject(component);
	}

	@Override
	public boolean queryIP(ComponentModel componentModel) {

		return false;
	}

	@Override
	public Result deleteComponent(Component component) {
		if (1 == component.getComponentType()) {// nginx
			List<DeployNgnix> deployNgnixs = deployNgnixMapper.selectByNginxId(component.getComponentId());
			if (!deployNgnixs.isEmpty()) {
				return new Result(false, "此nginx已被应用部署占用！");
			}
			RegistryLb lb = new RegistryLb();
			lb.setComponentId(component.getComponentId());
			List<RegistryLb> lbs = lbMapper.selectAllWithIP(lb);
			if (!lbs.isEmpty()) {
				return new Result(false, "此F5已被仓库集群占用！");
			}
		}
		if (2 == component.getComponentType()) {// F5
			Deploy deploy = new Deploy();
			deploy.setLoadBalanceId(component.getComponentId());
			List<Deploy> deploys = deployMapper.select(deploy);
			if (!deploys.isEmpty()) {
				return new Result(false, "此F5已被应用部署占用！");
			}
			RegistryLb lb = new RegistryLb();
			lb.setComponentId(component.getComponentId());
			List<RegistryLb> lbs = lbMapper.selectAllWithIP(lb);
			if (!lbs.isEmpty()) {
				return new Result(false, "此F5已被仓库集群占用！");
			}
			ComponentExpand componentExpand = new ComponentExpand();
			componentExpand.setF5CompId(component.getComponentId());
			List<ComponentExpand> componentExpands = componentExpandMapper.select(componentExpand);
			if (!componentExpands.isEmpty()) {
				return new Result(false, "此F5已被负载占用！");
			}
		}
		if (3 == component.getComponentType()) {// DB
			List<DeployDatabase> deployDatabases = deployDatabaseMapper.selectByComponentId(component.getComponentId());
			if (!deployDatabases.isEmpty()) {
				return new Result(false, "此数据库已被应用部署占用！");
			}
		}
		if (6 == component.getComponentType()) {// Nginx组
			Deploy deploy = new Deploy();
			deploy.setLoadBalanceId(component.getComponentId());
			List<Deploy> deploys = deployMapper.select(deploy);
			if (!deploys.isEmpty()) {
				return new Result(false, "此nginx已被应用部署占用！");
			}
			RegistryLb lb = new RegistryLb();
			lb.setComponentId(component.getComponentId());
			List<RegistryLb> lbs = lbMapper.selectAllWithIP(lb);
			if (!lbs.isEmpty()) {
				return new Result(false, "此F5已被仓库集群占用！");
			}
		}
		if (7 == component.getComponentType()) {// Svn组
			svnMapper.deleteSvn(component.getComponentId());
			//TODO
		}
		try {
			componentMapper.deleteComponent(component.getComponentId());
			//删除组件扩展信息
			componentExpandMapper.deleteComponentExpand(component.getComponentId());
			//删除组件 主机信息(删除前 更新对应主机类型为 4)
			List<ComponentHost> componentHostList = hostMapper.selectComponentHost(component.getComponentId());
			if(componentHostList!=null && componentHostList.size()>0)
				for(ComponentHost componentHost : componentHostList){
//					Host host = new Host();
//					host.setHostId(componentHost.getHostId());
//					host.setHostType((byte) Type.HOST.UNUSED.ordinal());
//					hostMapper.updateHost(host);
					
					hostMapper.deleteCompHost(componentHost.getComponentHostId());
				}
		} catch (Exception e) {
			logger.error("delete component fail" + e);
			return new Result(false, "删除组件【"+component.getComponentName()+"】失败："+e.getMessage());
		}
		return new Result(true, "删除组件【"+component.getComponentName()+"】成功！");
	}

	@Override
	public Integer componentCount(Component record) {
		int result = 0;
		try {
			result = componentMapper.selectComponentCount(record);
		} catch (Exception e) {
			logger.error("select component count fail" + e);
			return 0;
		}
		return result;
	}

	@Override
	public GridBean getSvnList() {
		// TODO Auto-generated method stub
		List<Svn> svns = svnMapper.getSvnList();
		GridBean gridBean = new GridBean(1, 65536, 65536, svns);
		return gridBean;
	}

}
