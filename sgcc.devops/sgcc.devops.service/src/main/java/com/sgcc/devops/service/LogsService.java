package com.sgcc.devops.service;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.dao.entity.Logs;

/**  
 * date：2016年2月4日 下午3:22:57
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogsService.java
 * description：运行日志数据维护接口类
 */
public interface LogsService {
	/**
	 * 日志分页
	 * 
	 * @param pageNum
	 *            页码
	 * @param pageSize
	 *            页面大小
	 * @return 分页信息
	 */
	public GridBean list(Logs logs, int pageNum, int pageSize);

}
