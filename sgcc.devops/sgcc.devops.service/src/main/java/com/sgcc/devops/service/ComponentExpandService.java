package com.sgcc.devops.service;

import com.sgcc.devops.dao.entity.ComponentExpand;

/**  
 * date：2016年3月8日 上午10:40:43
 * project name：sgcc.devops.service
 * @author  yueyong
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentExpandService.java
 * description：组件扩展数据维护接口类
 */
public interface ComponentExpandService {
	
	
	/**
	 * 更新组件F5(是否自动配置F5)
	 * @param componentExpand
	 * @return int
	 */
	public int upDateComponentExpandSer(ComponentExpand componentExpand);
	
	/**
	 * 根据componentid查询对应的expand表
	 * @param CompId  componentid
	 * @return
	 */
	public ComponentExpand selectByCompId(String CompId);

}
