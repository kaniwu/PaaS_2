/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONArray;

import com.sgcc.devops.dao.entity.BusinessSys;
import com.sgcc.devops.dao.entity.User;

/**  
 * date：2015年12月30日 下午12:17:21
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessSysService.java
 * description：  业务系统与物理系统关联数据处理接口类
 */
public interface BusinessSysService {

	/**
	* TODO
	* @author mayh
	* @return List<BusinessSys>
	* @version 1.0
	* 2015年12月30日
	*/
	List<BusinessSys> load(BusinessSys businessSys);

	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2015年12月30日
	*/
	JSONArray getSysByBusinessId(User user, String businessId,String envId);
	
}
