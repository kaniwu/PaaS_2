/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.DeployFile;

/**  
 * date：2016年1月26日 上午9:49:52
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployFileService.java
 * description：  部署配置文件数据维护接口类
 */
public interface DeployFileService {
	public List<DeployFile> selectByDeployId(String deployId);
	
	public int insert(DeployFile deployFile);

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年1月27日
	*/
	public void deleteByDeployId(String deployId);
}
