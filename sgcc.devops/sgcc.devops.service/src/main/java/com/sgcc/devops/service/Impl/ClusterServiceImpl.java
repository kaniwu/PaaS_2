package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ClusterWithIPAndUser;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.ClusterMapper;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.service.ClusterService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午2:23:41
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ClusterService.java
 * description： 集群业务数据操作接口实现类 
 */
@Component
public class ClusterServiceImpl implements ClusterService {

	private static Logger logger = Logger.getLogger(ClusterServiceImpl.class);
	@Resource
	private ClusterMapper clusterMapper;
	@Resource
	private HostMapper hostMapper;
	@Resource
	private JdbcConfig jdbcConfig;

	@Override
	public String createCluster(JSONObject jsonObject, Host host) {
		Cluster cluster = new Cluster();
		cluster.setClusterName(jsonObject.getString("clusterName"));
		cluster.setClusterPort(jsonObject.getString("clusterPort"));
		String cluUuid = UUID.randomUUID().toString();
		cluster.setClusterUuid(cluUuid);
		cluster.setMasteHostId(jsonObject.getString("masterHost"));
		cluster.setStandByHostId(jsonObject.getString("standByHostId"));
		cluster.setClusterDesc(jsonObject.getString("clusterDesc"));
		cluster.setClusterCreator(jsonObject.getString("userId"));
		cluster.setClusterType((byte) Type.CLUSTER.DOCKER.ordinal());
		cluster.setManagePath(jsonObject.getString("clusterConfFile"));
		cluster.setClusterStatus((byte) Status.CLUSTER.NORMAL.ordinal());
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		cluster.setClusterCreatetime(new Date());
		cluster.setClusterId(KeyGenerator.uuid());
        cluster.setRegistryLbId(jsonObject.getString("registryLbId"));
        cluster.setClusterEnv(jsonObject.getString("clusterEnv"));
		String clusterId = null;
		try {
			int result = clusterMapper.insertCluster(cluster);
			if (result == 1) {
				host.setClusterId(cluster.getClusterId());
				host.setHostType((byte) Type.HOST.SWARM.ordinal());
				hostMapper.updateHost(host);
				clusterId = cluster.getClusterId();
			}
		} catch (Exception e) {
			logger.error("create cluster fail: " + e.getMessage());
		}
		return clusterId;
	}

	@Override
	public int deleteCluster(JSONObject jo) {
		int result = 1;
		try {
			String id = jo.getString("clusterId");
			clusterMapper.deleteCluster(id);
		} catch (Exception e) {
			logger.error("delete cluster fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	@Override
	public int deleteClusters(JSONObject jo) {
		int result = 1;
		String arrayString = jo.getString("array");
		String[] jaArray = arrayString.split(",");
		List<String> list = new ArrayList<>();
		for (String appidString : jaArray) {
			if (appidString == null || "".equals(appidString)) {
			} else {
				list.add(appidString);
			}
		}
		try {
			clusterMapper.deleteClusters(list);
		} catch (Exception e) {
			logger.error("batch delete cluster fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	@Override
	public int update(Cluster cluster) {
		try {
			return clusterMapper.updateCluster(cluster);
		} catch (Exception e) {
			logger.error("update cluster fail: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public int updateCluster(JSONObject jo) {
		int result = 1;
		try {
			Cluster cluster = (Cluster) JSONUtil.parseJsonObjectToObject(jo, Cluster.class);
			clusterMapper.updateCluster(cluster);
		} catch (Exception e) {
			logger.error("update cluster fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	@Override
	public GridBean getOnePageClusterList(User user, PagerModel pagerModel,
			ClusterWithIPAndUser clusterWithIPAndUser) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("clusterName")){
				PageHelper.startPage(page, pageSize,"CLUSTER_NAME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		//环境变量
		clusterWithIPAndUser.setClusterEnv(user.getUserCompany());
		clusterWithIPAndUser.setDialect(jdbcConfig.getDialect());
		List<ClusterWithIPAndUser> clusters = clusterMapper.selectAllClusterWithIPAndUser(clusterWithIPAndUser);
		for(int i=0; i<clusters.size();i++){
			Host record = new Host();
			if(!StringUtils.isEmpty(clusters.get(i).getStandByHostId())){
				record.setHostId(clusters.get(i).getStandByHostId());
				record = hostMapper.selectHost(record);
				if(null!=record){
					clusters.get(i).setStandByHostIP(record.getHostIp());
				}
			}
		}
		int totalpage = ((Page<?>) clusters).getPages();
		Long totalNum = ((Page<?>) clusters).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), clusters);
		return gridBean;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ClusterWithIPAndUser> getAllClusterList(User user, ClusterWithIPAndUser clusterWithIPAndUser) {

		ClusterWithIPAndUser cluster = new ClusterWithIPAndUser();
		if(clusterWithIPAndUser!=null){
			cluster.setClusterStatus(clusterWithIPAndUser.getClusterStatus());
			cluster.setMasteHostId(clusterWithIPAndUser.getMasteHostId());
		}
		if(org.apache.commons.lang.StringUtils.isNotEmpty(user.getUserCompany())){
			cluster.setClusterEnv(user.getUserCompany());
		}
		cluster.setDialect(jdbcConfig.getDialect());
		List<ClusterWithIPAndUser> clusters = clusterMapper.selectAllClusterWithIPAndUser(cluster);
		JSONArray ja = new JSONArray();
		for (ClusterWithIPAndUser clu : clusters) {
			JSONObject jo = JSONUtil.parseObjectToJsonObject(clu);
			List<Host> hosts = hostMapper.selectHostListByClusterId(clu.getClusterId());
			if(null==hosts){
				jo.put("hostCount", 0);
			}else{
				jo.put("hostCount", hosts.size());
			}
			ja.add(jo);
		}
		return ja;
	}

//	@Override
//	public JSONArray getOnePageClusterMasterList() {
//
//		JSONArray ja = new JSONArray();
//		List<Host> hostList = hostMapper.selectHostList(0);
//		if (hostList != null) {
//			for (Host host : hostList) {
//				JSONObject jo = new JSONObject();
//				jo.put("hostip", host.getHostIp());
//				jo.put("hostuuid", host.getHostUuid());
//				jo.put("hostid", host.getHostId());
//				ja.add(jo);
//			}
//		}
//		return ja;
//	}

	@Override
	public int countAllClusterList() {
		int result = 0;
		try {
			result = clusterMapper.selectClusterCount();
		} catch (Exception e) {
			logger.error("select cluster count fail: " + e.getMessage());
			return 0;
		}
		return result;
	}

	@Override
	public Cluster getCluster(Cluster cluster) {
		try {
			cluster = clusterMapper.selectCluster(cluster.getClusterId());
		} catch (Exception e) {
			logger.error("select cluster fail: " + e.getMessage());
			return null;
		}
		return cluster;
	}

	@Override
	public Cluster getClusterByHost(Host host) {
		return clusterMapper.selectClusterByHost(host.getHostId());
	}

	@Override
	public JSONObject getJsonObjectOfCluster(Cluster cluster) {
		return JSONUtil.parseObjectToJsonObject(getCluster(cluster));
	}

	@Override
	public List<Cluster> listClustersByhostId(String hostId) {
		return clusterMapper.selectClustersByhostId(hostId);
	}

	@Override
	public List<Cluster> getAllCluster() {
		return clusterMapper.selectAllCluster(new Cluster());
	}

	@Override
	public Cluster selectClusterByName(Cluster record) {
		return clusterMapper.selectClusterByName(record);
	}

	@Override
	public Cluster getCluster(String clusterId) {
		Cluster cluster  = new Cluster();
		cluster.setClusterId(clusterId);
		return this.getCluster(cluster);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ClusterService#selectAllCluster(com.sgcc.devops.dao.entity.Cluster)
	 */
	@Override
	public List<Cluster> selectAllCluster(Cluster cluster) {
		return clusterMapper.selectAllCluster(cluster);
	}

}
