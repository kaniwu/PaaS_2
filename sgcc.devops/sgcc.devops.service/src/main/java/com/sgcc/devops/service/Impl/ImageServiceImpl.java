package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.model.ImageModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RegHost;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.RegIdImageType;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.RegistrySlaveImage;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.ClusterMapper;
import com.sgcc.devops.dao.intf.ImageMapper;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.dao.intf.RegistryLbMapper;
import com.sgcc.devops.dao.intf.RegistryMapper;
import com.sgcc.devops.service.ImageService;

import net.sf.json.JSONObject;

/**
 * 镜像服务接口，主要处理与DB相关的服务
 * 
 * @author dmw
 *
 */
@Component
public class ImageServiceImpl implements ImageService {

	private static Logger logger = Logger.getLogger(ImageServiceImpl.class);
	@Resource
	private ImageMapper imageMapper;
	@Autowired
	private RegImageMapper regImageMapper;
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	@Autowired
	private RegistryMapper registryMapper;
	@Autowired
	private RegistryLbMapper registryLbMapper;
	@Autowired
	private ClusterMapper clusterMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ImageService#create(com.sgcc.devops.entity.Image)
	 */
	@Override
	public String create(Image image) {
		image.setImageId(KeyGenerator.uuid());
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		image.setImageCreatetime(new Date());
		try {
			int result = imageMapper.insertImage(image);
			if (result > 0) {
				return image.getImageId();
			} else {
				logger.warn("save image in database error");
				return null;
			}
		} catch (Exception e) {
			logger.error("save image in database error: "+e.getMessage());
			return null;
		}
	}

	@Override
	public boolean delete(String imageId) {
		try {
			imageMapper.deleteImage(imageId);
			regImageMapper.deleteByImage(imageId);
			return true;
		} catch (Exception e) {
			logger.error("delete image in database error: "+e.getMessage());
			return false;
		}
	}

	@Override
	public boolean update(Image image) {
		return imageMapper.updateImage(image) > 0;
	}

	@Override
	public GridBean pagination(User user,PagerModel pagerModel, ImageModel imageModel) {
		
		RegIdImageType image = new RegIdImageType();
		image.setImageType(imageModel.getImageType());
		image.setTempName(imageModel.getImageName());
		image.setRegistryId(imageModel.getRegistryId());
		image.setDialect(jdbcConfig.getDialect());
		int count = registryMapper.selectCountImageInRegistry(image);
		int page = pagerModel.getPage();
		int rows = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		
		if(count<(page-1)*rows){
			page = 1;
		}
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("tempName")){
				PageHelper.startPage(page, rows,"TEMP_NAME "+sord);
			}
			if(sidx.equals("imageStatus")){
				PageHelper.startPage(page, rows,"IMAGE_STATUS "+sord);
			}
			if(sidx.equals("registryName")){
				PageHelper.startPage(page, rows,"REGISTRY_NAME "+sord);
			}
			if(sidx.equals("imageCreatetime")){
				PageHelper.startPage(page, rows,"IMAGE_CREATETIME "+sord);
			}
		}else{
			PageHelper.startPage(page, rows);
		}
		PageHelper.startPage(page, rows);
		
		
		//如果是应用镜像
		if(imageModel.getImageType()==1){
			image.setEnvId(user.getUserCompany());
		}
		image.setDialect(jdbcConfig.getDialect());
		List<RegistrySlaveImage> images = registryMapper.selectAllImageInRegistry(image);
		List<ImageModel> imageModels = new ArrayList<>();
		for (RegistrySlaveImage imageDemo : images) {
			String imageId = imageDemo.getImageId();
			ImageModel imageModel2 = new ImageModel();
			// 获取关联对象
			RegImage regImage = regImageMapper.selectByImageId(imageId);
			if (regImage != null) {
				String id = regImage.getRegistryId();
				RegistryLb record = new RegistryLb();
				// 获取registry对象
				record.setId(id);
				RegistryLb registryLb = registryLbMapper.selectRegistryLb(record);
				imageModel2.setRegistryId(id);
				
				if(null!=registryLb){
					imageModel2.setRegistryName(registryLb.getRegistryName());
					imageModel2.setRegistryLbCompId(registryLb.getComponentId());
					
				}
			}

			imageModel2.setImageDesc(imageDemo.getImageDesc());
			imageModel2.setImageId(imageDemo.getImageId());
			imageModel2.setImageName(imageDemo.getImageName());
			imageModel2.setImagePort(imageDemo.getImagePort());
			imageModel2.setImageUuid(imageDemo.getImageUuid());
			imageModel2.setImageType(imageDemo.getImageType());
			imageModel2.setImageSize(imageDemo.getImageSize());
			imageModel2.setImageStatus((byte) imageDemo.getImageStatus());
			imageModel2.setImageTag(imageDemo.getImageTag());

			imageModel2.setImageCreatetime(imageDemo.getImageCreatetime());
			imageModel2.setTempName(imageDemo.getTempName());
			imageModels.add(imageModel2);
		}
		int totalpage = ((Page<?>) images).getPages();
		Long totalNum = ((Page<?>) images).getTotal();
		// GridBean gridBean = new GridBean(page, totalpage,
		// totalNum.intValue(), images);
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), imageModels);
		return gridBean;
		
	}

	@Override
	public List<Image> selectActiveAllImages(Image image) {
		List<Image> images = imageMapper.selectAllImage(image);
		return images;
	}

	@Override
	public GridBean getImageListByappId(String userId, int page, int rows, String appId) {
		PageHelper.startPage(page, rows);
		List<Image> images = imageMapper.selectImagesBydeployId(appId);
		int totalpage = ((Page<?>) images).getPages();
		Long totalNum = ((Page<?>) images).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), images);
		return gridBean;
	}

	@Override
	public int countAllImageList(ImageModel imageModel) {

		return 0;
	}

	@Override
	public Image load(String id) {
		return imageMapper.selectByPrimaryKey(id);
	}

	@Override
	public JSONObject getJsonObjectOfImage(Image image) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RegHost loadByImageId(String imageId) {
		String sql = "select dop_host.host_id as hostid,dop_host.host_ip as hostip,dop_host.host_user as username,dop_host.host_pwd as hostpwd,"
				+ "reg_host.port as port from dop_host inner join (select reg.host_id as id, reg.registry_port as port"
				+ " from dop_registry  reg inner join dop_reg_img  reg_img on reg_img.image_id=:IMAGE_ID and reg_img.registry_id=reg.registry_id)"
				+ "  reg_host on dop_host.host_id=reg_host.id";
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("IMAGE_ID", imageId);
		List<Map<String, Object>> metaList = this.namedParameterJdbcTemplate.queryForList(sql, paramMap);
		if (null == metaList || metaList.isEmpty()) {
			return null;
		}
		Map<String, Object> meta = metaList.get(0);
		return buildHost(meta);
	}

	private RegHost buildHost(Map<String, Object> meta) {
		RegHost host = new RegHost();
		host.setId(meta.get("hostid").toString());
		host.setIp(meta.get("hostip").toString());
		host.setPassword(meta.get("hostpwd").toString());
		host.setPort(meta.get("port").toString());
		host.setUsername(meta.get("username").toString());
		return host;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.ImageService#getImageListByImageType(int,
	 * int, int)
	 */
	@Override
	public GridBean getImageListByImageType(String userId, int page, int rows,int imageType,String clusterId) {
		if(org.apache.commons.lang.StringUtils.isEmpty(clusterId)){
			return null;
		}
		Cluster cluster = clusterMapper.selectCluster(clusterId);
		PageHelper.startPage(page, rows);
		RegIdImageType image = new RegIdImageType();
		image.setImageType((byte)imageType);
		image.setRegistryId(cluster.getRegistryLbId());
		image.setDialect(jdbcConfig.getDialect());
		List<RegistrySlaveImage> images = registryMapper.selectAllImageInRegistry(image);
		int totalpage = ((Page<?>) images).getPages();
		Long totalNum = ((Page<?>) images).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), images);
		return gridBean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.ImageService#selectByPrimaryKey(java.lang.
	 * Integer)
	 */
	@Override
	public Image selectByPrimaryKey(String imageId) {
		return imageMapper.selectByPrimaryKey(imageId);
	}

	@Override
	public int selectImageCount(Image image) {
		//
		int result = 0;
		try {
			image.setDeployId(jdbcConfig.getDialect());
			result = imageMapper.selectImageCount(image);
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("select image count fail: " + e.getMessage());
			return 0;
		}
		return result;
	}

	@Override
	public Image selectImage(Image image) {

		try {
			image = imageMapper.selectImage(image);
		} catch (Exception e) {
			logger.error("select image error: " + e.getMessage());
			return null;
		}
		return image;
	}
}
