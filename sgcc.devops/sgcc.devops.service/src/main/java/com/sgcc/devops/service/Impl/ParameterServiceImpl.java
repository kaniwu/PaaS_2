/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.ParameterModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.dao.entity.Parameter;
import com.sgcc.devops.dao.intf.LocationMapper;
import com.sgcc.devops.dao.intf.ParameterMapper;
import com.sgcc.devops.service.ParameterService;

import net.sf.json.JSONArray;

/**  
 * date：2016年4月11日 上午9:24:26
 * project name：sgcc.devops.service
 * @author  panjing
 * @version 1.0   
 * @since JDK 1.7.0_21  
 */
@Component
public class ParameterServiceImpl implements ParameterService {
	private static Logger logger = Logger.getLogger(ParameterServiceImpl.class);
	@Resource
	private ParameterMapper parameterMapper;
	@Resource
	private LocationMapper locationMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public int deleteByPrimaryKey(String id) throws Exception {
		int result = 1;
		try {
			parameterMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error("delete parameter fail: " + e.getMessage());
			result = 0;
		}
		return result;
		
	}
	
	@Override
	public int insert(Parameter record) throws Exception {
		int result = 1;
		try{
			parameterMapper.insert(record);
		}catch(Exception e){
			result = 0;
			logger.error("insert parameter fail:" + e.getMessage());
		}
		return result;
	}

	
	@Override
	public Parameter selectByPrimaryKey(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public int updateByPrimaryKeySelective(Parameter record) throws Exception {
		int result = 1;
		try{
			parameterMapper.updateByPrimaryKeySelective(record);
		}catch(Exception e){
			result = 0;
			logger.error("update parameter fail:" +e.getMessage());
		}
		return result;
	}

	@Override
	public List<Parameter> selectAll() {
		return parameterMapper.selectAll();
	}


	@Override
	public int updateByPrimaryKey(Parameter record) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Parameter getParameter(Parameter parameter) {
		try {
			parameter = parameterMapper.selectByPrimaryKey(parameter.getId());
		} catch (Exception e) {
			logger.error("select parameter fail: " + e.getMessage());
			return null;
		}
		return parameter;
	}

	@Override
	public GridBean getSearchParaList(PagerModel pagerModel,
			ParameterModel parameterModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("paraName")){
				PageHelper.startPage(page, pageSize,"PARA_NAME "+sord);
			}
			if(sidx.equals("paraType")){
				PageHelper.startPage(page, pageSize,"PARA_TYPE "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		Parameter parameter = new Parameter();

		if (parameterModel.getParaName() != null) {
			parameter.setParaName(parameterModel.getParaName());
		}
		if(parameterModel.getParaType() != null){
			parameter.setParaType(parameterModel.getParaType());
		}
		parameter.setDialect(jdbcConfig.getDialect());
		List<Parameter> parameters = parameterMapper.selectParameter(parameter);
		int totalpage = ((Page<?>) parameters).getPages();
		Long totalNum = ((Page<?>) parameters).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), parameters);
		return gridBean;
	}

	@Override
	public JSONArray selectByType(String type) {
		Parameter parameter = new Parameter();
		if(!StringUtils.isEmpty(type)){
			parameter.setParaType(type);
			parameter.setDialect(jdbcConfig.getDialect());
			List<Parameter> parameters = parameterMapper.selectParameter(parameter);
			JSONArray ja = JSONUtil.parseObjectToJsonArray(parameters);
			return ja;
		}
		return null;
	}

	@Override
	public Parameter selectParameterByCode(Parameter parameter) {
		return parameterMapper.selectParameterByCode(parameter);
	}
	/**
	 */
	@Override
	public JSONArray selectByLocation(String parentid) {
		JSONArray jaArray = new JSONArray();
		try {
			if("0".equals(parentid)){
				Parameter parameter = new Parameter();
				parameter.setParaType("0");
				parameter.setDialect(jdbcConfig.getDialect());
				List<Parameter> parameters = parameterMapper.selectParameter(parameter);
				JSONArray ja = JSONUtil.parseObjectToJsonArray(parameters);
				return ja;
			}
			Location location = locationMapper.selectByPrimaryKey(parentid);
			if(null!=location){
				String paramCode = location.getLocationParamCode();
				Parameter parameter = new Parameter();
				parameter.setParaCode(paramCode);
				parameter.setDialect(jdbcConfig.getDialect());
				parameter = parameterMapper.selectParameterByCode(parameter);
				Parameter parameter2 = new Parameter();
				parameter2.setParentId(parameter.getId());
				parameter2.setDialect(jdbcConfig.getDialect());
				List<Parameter> list = parameterMapper.selectParameter(parameter2);
				for(Parameter param:list){
					jaArray.add(JSONUtil.parseObjectToJsonObject(param));
					parameter2.setParentId(param.getId());
					parameter2.setDialect(jdbcConfig.getDialect());
					List<Parameter> list2 = parameterMapper.selectParameter(parameter2);
					for(Parameter param2:list2){
						jaArray.add(JSONUtil.parseObjectToJsonObject(param2));
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jaArray;
	}
	@Override
	public JSONArray selectParent(String type) {
		Parameter parameter = new Parameter();

		if(!StringUtils.isEmpty(type)){
			parameter.setParaType(type);
			List<Parameter> parameters = parameterMapper.selectParent(parameter);
			JSONArray ja = JSONUtil.parseObjectToJsonArray(parameters);
			return ja;
		}
		return null;
	}

}
