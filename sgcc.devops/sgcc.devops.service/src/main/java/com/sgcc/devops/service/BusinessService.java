/**
 * 
 */
package com.sgcc.devops.service;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Business;
import com.sgcc.devops.dao.entity.User;

/**  
 * date：2016年2月4日 下午2:03:11
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessService.java
 * description：  业务系统是数据操作接口类
 */
public interface BusinessService {

	/**
	* TODO
	* 业务系统分页数据查询方法
	* 参数：系统登录用户、分页列表查询页、每页显示行数
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年2月4日
	 */
	GridBean listAllOfBusiness(User user,PagerModel pagerModel,String businessName);

	/**
	* TODO
	* 业务系统数量查询方法
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年2月4日
	 */
	int businessCount();

	/**
	* TODO
	* 单个业务系统查询方法
	* 参数：业务系统主键businessId
	* @author mayh
	* @return Business
	* @version 1.0
	* 2016年2月4日
	 */
	Business load(String businessId);

	/**
	* TODO
	* 更新业务系统数据方法
	* 参数：Business对象
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月30日
	*/
	void update(Business business2)  throws Exception;

	/**
	* TODO
	* @author mayh
	* @return List<Business>
	* @version 1.0
	* 2016年3月8日
	*/
	Integer countDeployed();
}
