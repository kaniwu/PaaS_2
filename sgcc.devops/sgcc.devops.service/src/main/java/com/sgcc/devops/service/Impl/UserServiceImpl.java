package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.entity.UserRole;
import com.sgcc.devops.dao.intf.EnvironmentMapper;
import com.sgcc.devops.dao.intf.UserMapper;
import com.sgcc.devops.dao.intf.UserRoleMapper;
import com.sgcc.devops.service.UserService;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:40:43
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：UserService.java
 * description：用户数据维护接口接口类
 */
@Component
public class UserServiceImpl implements UserService {

	private static Logger logger = Logger.getLogger(UserServiceImpl.class);
	@Resource
	private UserMapper userMapper;
	@Resource
	private UserRoleMapper userRoleMapper;
	@Resource
	private EnvironmentMapper environmentMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.cmbc.devops.service.UserService#checkLogin(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public User checkLogin(String userName, String password) {
		if (StringUtils.isEmpty(userName) || StringUtils.isEmpty(password)) {
			logger.error("用户名或者密码为空");
			return null;
		}
		try {
			User user = new User();
			user.setUserName(userName);
			user = getUser(user);
			if (null == user || StringUtils.isEmpty(user.getUserId())) {
				logger.warn("【" + userName + "】用户不存在！");
				return null;
			}
			if (user.getUserStatus() == Status.USER.DELETE.ordinal()) {
				logger.warn("【" + userName + "】用户被删除");
				return null;
			}
			String pass = user.getUserPass();
			if (pass.equals(password)) {
				return user;
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("用户登录失败：" + e.getMessage());
			return null;
		}
	}


	@Override
	public int updatePassword(String userName, String password) {
		int updateResult = 0;
		try {
			User user = new User();
			user.setUserName(userName);
			user = getUser(user);
			user.setUserPass(password);
			updateResult = userMapper.updateUser(user);
		} catch (Exception e) {
			logger.error("用户登录失败：" + e.getMessage());
		}
		return updateResult;
	}

	@Override
	public User getUser(User user) throws Exception {
		return userMapper.selectUser(user);
	}

	@Override
	public JSONObject getJsonObjectOfUser(User user) throws Exception {
		return JSONUtil.parseObjectToJsonObject(this.getUser(user));
	}

	@Override
	public GridBean list(String owner, int pageNum, int pageSize, User user) throws Exception {

		PageHelper.startPage(pageNum, pageSize);
		user.setUserStatus((byte) 1);
		if (null != user.getUserName()) {
			user.setUserName(user.getUserName());
		}
		if (null != owner) {
			user.setUserCreator(owner);
		}
		user.setDialect(jdbcConfig.getDialect());
		List<User> users = userMapper.selectAll(user);
		for(User user2:users){
			User creator = new User();
			creator.setUserId(user2.getUserCreator());
			creator = userMapper.selectUser(creator);
			if(creator!=null){
				user2.setUserCreator(creator.getUserName());
			}
			Environment record = new Environment();
			record = environmentMapper.selectByPirmaryKey(user2.getUserCompany());
			if(record!=null){
				user2.setUserCompany(record.geteName());
			}else{
				user2.setUserCompany("");
			}
		}
		int total = ((Page<?>) users).getPages();
		int records = (int) ((Page<?>) users).getTotal();
		GridBean gridbean = new GridBean(pageNum, total, records, users);
		return gridbean;
	}

	@Override
	public int create(User user) throws Exception {
		user.setUserId(KeyGenerator.uuid());
		return userMapper.insertUser(user);
	}

	@Override
	public int update(User user) throws Exception {
		return userMapper.updateUser(user);
	}

	@Override
	public int delete(String userId) throws Exception {
		return userMapper.deleteUser(userId);
	}

	@Override
	public int active(String userId) throws Exception {
		return userMapper.activeUser(userId);
	}
	
	@Override
	public int deletes(List<String> ids) {
		return 1;
	}

	@Override
	public int updateUserRole(UserRole userRole) throws Exception {
		return userRoleMapper.insert(userRole);
	}

	@Override
	public User getUserByName(String userName) throws Exception {
		return userMapper.selectUserByName(userName);
	}

	@Override
	public GridBean advancedSearchUser(String userId, int pagenum, int pagesize, User user, JSONObject json_object)
			throws Exception {

		PageHelper.startPage(pagenum, pagesize);
		/* 组装应用查询数据的条件 */
		User userSraech = new User();
		userSraech.setUserStatus(((byte) Status.USER.NORMAL.ordinal()));
		/* 获取用户填写的各项查询条件 */
		String[] params = json_object.getString("params").split(",");
		String[] values = json_object.getString("values").split(",");

		/* 遍历填充各项查询条件 */
		for (int array_count = 0, array_length = params.length; array_count < array_length; array_count++) {
			switch (params[array_count].trim()) {
			case ("1"): {
				userSraech.setUserName(values[array_count].trim());
				break;
			}
			case "2": {
				userSraech.setUserMail(values[array_count].trim());
				break;
			}
			case "3": {
				userSraech.setUserPhone(values[array_count].trim());
				break;
			}
			case "4": {
				userSraech.setUserCompany(values[array_count].trim());
				break;
			}
			}
		}
		userSraech.setDialect(jdbcConfig.getDialect());
		List<User> users = userMapper.selectAll(userSraech);

		int totalpage = ((Page<?>) users).getPages();
		Long totalNum = ((Page<?>) users).getTotal();

		GridBean gridBean = new GridBean(pagenum, totalpage, totalNum.intValue(), users);
		return gridBean;
	}
	
	@Override
	public int deleteByUserId(String userId) throws Exception {

		userRoleMapper.deleteByUserId(userId);
		return 1;
	}


	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.UserService#selectAllUsers(com.sgcc.devops.dao.entity.User)
	 */
	@Override
	public List<User> selectAllUsers(User user) throws Exception {
		user.setDialect(jdbcConfig.getDialect());
		return userMapper.selectAll(user);
	}
}
