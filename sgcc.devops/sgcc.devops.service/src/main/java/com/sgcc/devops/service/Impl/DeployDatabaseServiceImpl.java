/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.intf.DeployDatabaseMapper;
import com.sgcc.devops.service.DeployDatabaseService;

/**  
 * date：2016年2月4日 下午3:02:13
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployDatabaseService.java
 * description：部署数据库业务数据维护接口实现类
 */
@Component
public class DeployDatabaseServiceImpl implements DeployDatabaseService {
	@Resource
	private DeployDatabaseMapper deployDatabaseMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployDatabaseService#create(com.sgcc.devops.dao.
	 * entity.DeployDatabase)
	 */
	@Override
	public int create(DeployDatabase deployDatabase) {
		if (!StringUtils.hasText(deployDatabase.getId())) {
			deployDatabase.setId(KeyGenerator.uuid());
		}
		int result = deployDatabaseMapper.create(deployDatabase);
		return result;
	}

	@Override
	public List<DeployDatabase> selectByDatabaseId(String databaseId) {
		return deployDatabaseMapper.selectByDatabaseId(databaseId);
	}
	@Override
	public List<DeployDatabase> selectByNginxId(String databaseId) {
		return deployDatabaseMapper.selectByNginxId(databaseId);
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployDatabaseService#selectByDeployId(java.lang.
	 * Integer)
	 */
	@Override
	public List<DeployDatabase> selectByDeployId(String deployId) {
		return deployDatabaseMapper.selectByDeployId(deployId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployDatabaseService#deleteByDeployID(java.lang.String)
	 */
	@Override
	public void deleteByDeployID(String deployId) {
		deployDatabaseMapper.deleteByDeployId(deployId);
	}
}
