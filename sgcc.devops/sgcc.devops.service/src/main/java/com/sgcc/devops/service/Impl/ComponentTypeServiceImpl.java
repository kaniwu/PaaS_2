package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.ComponentTypeModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.ComponentType;
import com.sgcc.devops.dao.intf.ComponentTypeMapper;
import com.sgcc.devops.service.ComponentTypeService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**  
 * date：2016年2月4日 下午2:50:55
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentTypeService.java
 * description：  组件类型业务数据操作接口实现类
 */
@org.springframework.stereotype.Component
public class ComponentTypeServiceImpl implements ComponentTypeService {

	@Resource
	private ComponentTypeMapper componentTypeMapper;

	@Override
	public int createComponentType(JSONObject jo) {
		int result = 1;
		ComponentType record = new ComponentType();

		record.setComponentTypeName(jo.getString("componentTypeName"));
		record.setComponentTypeId(KeyGenerator.uuid());
		try {
			componentTypeMapper.insertComponentType(record);
		} catch (Exception e) {
			result = 0;
		}
		return result;
	}

	@Override
	public int updateComponentType(JSONObject jo) {
		int result = 1;
		ComponentType record = new ComponentType();
		record.setComponentTypeId(jo.getString("componentTypeId"));
		record.setComponentTypeName(jo.getString("componentTypeName"));

		try {
			componentTypeMapper.updateComponentType(record);
		} catch (Exception e) {
			result = 0;
		}
		return result;
	}

	@Override
	public ComponentType getComponentType(ComponentType componentType) {
		return componentTypeMapper.selectComponentType(componentType.getComponentTypeId());
	}

	@Override
	public JSONObject getJsonObjectOfComponentType(ComponentType componentType) {
		return JSONUtil.parseObjectToJsonObject(componentType);
	}

	@Override
	public int deleteComponentType(JSONObject jo) {
		int result = 1;
		try {
			componentTypeMapper.deleteComponentType(jo.getString("componentTypeId"));
		} catch (Exception e) {
			result = 0;
		}
		return result;
	}

	@Override
	public int deleteComponentTypes(JSONObject jo) {
		int result = 1;
		String arrayString = jo.getString("ids");
		String[] jaArray = arrayString.split(",");
		List<String> lists = new ArrayList<String>();
		for (String componentTypeId : jaArray) {
			if (componentTypeId == null || componentTypeId.equals("")) {

			} else {
				lists.add(componentTypeId);
			}
		}
		for (String componentTypeId : lists) {
			try {
				componentTypeMapper.deleteComponentType(componentTypeId);
			} catch (Exception e) {
				result = 0;
			}
		}
		return result;
	}

	@Override
	public GridBean getOnePageComponentTypeList(String userId, int page, int rows,
			ComponentTypeModel componentTypeModel) {
		PageHelper.startPage(page, rows);
		ComponentType componentType = new ComponentType();
		componentType.setComponentTypeId(componentTypeModel.getComponentTypeId());
		componentType.setComponentTypeName(componentTypeModel.getComponentTypeName());
		List<ComponentType> componentTypes = componentTypeMapper.selectAllComponentType(userId, page, rows,
				componentType);
		int totalpage = ((Page<?>) componentTypes).getPages();
		Long totalNum = ((Page<?>) componentTypes).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), componentTypes);
		return gridBean;
	}

	@Override
	public JSONObject selectAll(String userId) {
		List<ComponentType> componentTypes = componentTypeMapper.selectAllComponentType(userId, 0, 0, null);
		JSONObject jo = new JSONObject();
		JSONArray params = new JSONArray();
		for (ComponentType componentType : componentTypes) {
			JSONObject jsonObject = JSONUtil.parseObjectToJsonObject(componentType);
			params.add(jsonObject);
		}
		jo.put("params", params);
		return jo;
	}

}
