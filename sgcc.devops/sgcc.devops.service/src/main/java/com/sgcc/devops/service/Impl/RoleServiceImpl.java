package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Role;
import com.sgcc.devops.dao.entity.RoleAction;
import com.sgcc.devops.dao.intf.RoleActionMapper;
import com.sgcc.devops.dao.intf.RoleMapper;
import com.sgcc.devops.service.RoleService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * date：2015年8月14日 上午10:48:23 project name：cmbc-devops-service
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：UserServiceImpl.java description：
 */
@Component
public class RoleServiceImpl implements RoleService {

	private static Logger logger = Logger.getLogger(RoleServiceImpl.class);
	@Resource
	private RoleMapper roleMapper;
	@Resource
	private RoleActionMapper roleActionMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public Role getRole(Role role) throws Exception {
		return roleMapper.selectRole(role);
	}

	@Override
	public GridBean list(String owner, int pageNum, int pageSize, Role role) throws Exception {
		PageHelper.startPage(pageNum, pageSize);
		/* 暂时将所有状态的角色全部显示 */
		// role.setRoleStatus((byte) 1);
		if (null != role.getRoleName()) {
			role.setRoleName(role.getRoleName());
		}
		if (null != owner) {
		}
		role.setDialect(jdbcConfig.getDialect());
		List<Role> roles = roleMapper.selectAll(role);
		int total = ((Page<?>) roles).getPages();
		int records = (int) ((Page<?>) roles).getTotal();
		GridBean gridbean = new GridBean(pageNum, total, records, roles);
		return gridbean;
	}

	@Override
	public int update(Role role) {
		try {
			return roleMapper.updateByPrimaryKeySelective(role);
		} catch (Exception e) {
			logger.error("update role fail", e);
			return 0;
		}
	}

	@Override
	public int deleteByRoleId(String roleId) {
		try {
			roleActionMapper.deleteByRoleId(roleId);
			logger.error("update authority success");
			return 1;
		} catch (Exception e) {
			logger.error("update authority fail", e);
			return 0;
		}
	}

	@Override
	public JSONArray getAllRoleList(String userId, Role role) throws Exception {
		role.setDialect(jdbcConfig.getDialect());
		List<Role> roles = roleMapper.selectAll(role);
		JSONArray ja = JSONUtil.parseObjectToJsonArray(roles);
		return ja;
	}

	@Override
	public int updateAuth(RoleAction roleAction) {
		try {
			return roleActionMapper.insertSelective(roleAction);
		} catch (Exception e) {
			logger.error("update role auth fail", e);
			return 0;
		}
	}

	@Override
	public List<RoleAction> getRoleAuthList(String userId, RoleAction roleAction) throws Exception {
		List<RoleAction> ras = roleActionMapper.selectAll(roleAction);
		return ras.isEmpty() ? null : ras;
	}

	@Override
	public GridBean advancedSearchRole(String userId, int pagenum, int pagesize, Role role, JSONObject json_object)
			throws Exception {

		PageHelper.startPage(pagenum, pagesize);
		/* 组装应用查询数据的条件 */
		Role roleSraech = new Role();
		roleSraech.setRoleStatus(((byte) Status.USER.NORMAL.ordinal()));
		/* 获取用户填写的各项查询条件 */
		String[] params = json_object.getString("params").split(",");
		String[] values = json_object.getString("values").split(",");

		/* 遍历填充各项查询条件 */
		for (int array_count = 0, array_length = params.length; array_count < array_length; array_count++) {
			switch (params[array_count].trim()) {
			case ("1"): {
				roleSraech.setRoleName(values[array_count].trim());
				break;
			}
			case "2": {
				roleSraech.setRoleDesc(values[array_count].trim());
				break;
			}
			case "3": {
				if ("正常".indexOf(values[array_count].trim()) != -1) {
					roleSraech.setRoleStatus((byte) Status.ROLE.NORMAL.ordinal());
				} else if ("注销".indexOf(values[array_count].trim()) != -1) {
					roleSraech.setRoleStatus((byte) Status.ROLE.DELETE.ordinal());
				} else {
					roleSraech.setRoleStatus((byte) Integer.MAX_VALUE);
				}
				break;
			}
			}
		}
		roleSraech.setDialect(jdbcConfig.getDialect());
		List<Role> roles = roleMapper.selectAll(roleSraech);

		int totalpage = ((Page<?>) roles).getPages();
		Long totalNum = ((Page<?>) roles).getTotal();

		GridBean gridBean = new GridBean(pagenum, totalpage, totalNum.intValue(), roles);
		return gridBean;
	}

	@Override
	public List<Role> getRolesByUserId(String userId) throws Exception {
		List<Role> roles = roleMapper.selectRolesByUserId(userId);
		return roles;
	}

	@Override
	public List<Role> getAllRoleListByUserId(String userId) throws Exception {
		List<Role> roles = roleMapper.selectAllRolesByUserId(userId);
		return roles;
	}
}
