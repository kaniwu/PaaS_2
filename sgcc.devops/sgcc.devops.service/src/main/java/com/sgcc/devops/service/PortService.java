package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.CompPort;
import com.sgcc.devops.dao.entity.Port;

public interface PortService {
	/**
	 * 主键查询端口
	 * @param Id
	 * @return
	 */
	public Port selectByPrimaryKey(String id);
	/**
	 * 根据类型查询端口
	 * @param type
	 * @return
	 */
	public List<Port> selectAllPort(Port port);
	/**
	 * 删除端口
	 * @param id
	 * @return
	 */
	public int deletePort(String id);
	/**
	 * 新增端口
	 * @param port
	 * @return
	 */
	public int insertPort(Port port);
	/**
	 * 更新端口
	 * @param port
	 * @return
	 */
	public int update(Port port);
	/**
	 * 选择nginx端口关联关系
	 * @param portId
	 * @return
	 */
	public List<CompPort> selectAllCompPort(CompPort compPort);
	/**
	 * 删除nginx端口关联
	 * @param id
	 * @return
	 */
	public int deleteCompPort(String id);
	/**
	 * 新增nginx端口关联
	 * @param CompPort
	 * @return
	 */
	public int insertCompPort(CompPort CompPort);
	/**
	 * 
	 * @param pagerModel
	 * @param type
	 * @return
	 */
	public GridBean gridBean(PagerModel pagerModel, String type);
	/**
	 * 组件是否存在
	 * @param compPort
	 * @return
	 */
	public CompPort isExist(CompPort compPort);
	/**
	 * nginx分组查询
	 * @param compPort
	 * @return
	 */
	public List<String> selectGroupByNginx(CompPort compPort);
	public List<String> selectGroupByPort(CompPort temp);
	public List<String> selectGroupByUsedId(CompPort temp2);
	public void deleteCompPortByUsedId(String deployId);
	public GridBean getFreePort(PagerModel pagerModel, String type);
}
