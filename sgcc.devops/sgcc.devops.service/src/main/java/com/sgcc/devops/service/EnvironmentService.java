package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.EnvironmentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Environment;

public interface EnvironmentService {

	GridBean environmentList(PagerModel pagerModel,EnvironmentModel environmentModel);
	 
	int insert(Environment record);
	 
	int deleteByPrimaryKey(String id);

	int updateByPrimaryKeySelective(Environment environment);

	List<Environment> environmentsList(Environment environment);

	/**
	* TODO
	* @author mayh
	* @return Environment
	* @version 1.0
	* 2016年8月5日
	*/
	Environment environmentById(String eId);

}
