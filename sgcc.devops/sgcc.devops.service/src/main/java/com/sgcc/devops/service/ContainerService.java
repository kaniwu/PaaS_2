package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.SimpleContainer;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.entity.expand.ContainerExpand;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:01:44
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ContainerService.java
 * description：容器业务数据维护接口类
 */
public interface ContainerService {

	/**
	 * @author langzi
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public List<Container> listAllContainer() throws Exception;

	/**
	 * @author langzi
	 * @param appId
	 * @return
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<Container> listContainersByAppId(String appId) throws Exception;

	/**
	 * @author langzi
	 * @param appId
	 * @return
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<Container> listContainersByHostId(String hostId) throws Exception;

	/**
	 * @author langzi
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public JSONArray listAllContainersJsonArray() throws Exception;

	/**
	 * @author langzi
	 * @param userId
	 * @param pagenumber
	 * @param pagesize
	 * @param model
	 * @return
	 * @version 1.0 2015年9月6日
	 */
	public GridBean listOnePageContainers(String userId, int pagenum, int pagesize, ContainerModel model);

	/**
	 * @author langzi
	 * @param container
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public Container getContainer(Container container);

	/**
	 * @author langzi
	 * @param container
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public JSONObject getContainerJsonObject(Container container) throws Exception;

	/**
	 * @author langzi
	 * @param ja
	 * @return
	 * @version 1.0 2015年8月26日
	 */
	public List<SimpleContainer> selectContainerUuid(String[] containerIds) throws Exception;

	/**
	 * @author zhangxin
	 * @param ja
	 * @return
	 * @version 1.0 2015年10月10日
	 */
	public List<Container> selectContainerId(String[] containerIds) throws Exception;

	/**
	 * @author langzi
	 * @param container
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public int addContaier(Container container) throws Exception;

	/**
	 * @author langzi
	 * @param conId
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public int modifyContainer(Container container) throws Exception;

	/**
	 * @author langzi
	 * @param power
	 * @param ja
	 * @return
	 * @version 1.0 2015年8月26日
	 */
	public int modifyContainerPower(ContainerExpand ce) throws Exception;

	/**
	 * @author langzi
	 * @param status
	 * @param ja
	 * @return
	 * @version 1.0 2015年8月26日
	 */
	public int modifyContainerStatus(ContainerExpand ce) throws Exception;

	/**
	 * @author langzi
	 * @param conId
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public int removeContainer(String[] conIds) throws Exception;

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Container>
	 * @version 1.0 2015年10月16日
	 */
	public List<Container> selectContainersByImageId(String iamgeId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月22日
	 * @throws Exception
	 */
	public GridBean gridBeanContainersByHostId(String hostId) throws Exception;

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return GridBean
	 * @version 1.0 2015年10月23日
	 * @throws Exception
	 */
	public GridBean selectContainersByImageId(String userId, int pagenum, int pagesize, String conImgid);

	/**
	 * @author zhangxin
	 * @return
	 * @version 1.0 2015年10月23日
	 */
	public GridBean listContainerByType(User user, PagerModel pagerModel, Container container) throws Exception;

	/**
	 * @author zhangxin
	 * @return
	 * @version 1.0 2015年10月29日
	 */
	public List<Container> listContainerByPower(Container container) throws Exception;

	/**
	 * @author zhangxin
	 * @return
	 * @version 1.0 2015年10月23日
	 */
	public List<Container> selectAllContainerOfHost(String hostId) throws Exception;

	/**
	 * @author zhangxin
	 * @return
	 * @version 1.0 2015年11月20日
	 */
	public int selectContainerCount(Container container);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Container>
	 * @version 1.0 2015年12月5日
	 */
	public List<Container> getBySysId(String systemId);
}
