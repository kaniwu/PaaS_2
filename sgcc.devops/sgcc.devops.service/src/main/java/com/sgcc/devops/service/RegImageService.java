/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.Registry;

/**  
 * date：2016年2月4日 下午3:33:06
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RegImageService.java
 * description：  仓库镜像关联数据维护接口类
 */
public interface RegImageService {
	public List<RegImage> selectAll(String imageId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return Registry
	 * @version 1.0 2015年9月24日
	 */
	public Registry getRegistry(String imageId);
}
