/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.Driver;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.ComponentMapper;
import com.sgcc.devops.dao.intf.DriverMapper;
import com.sgcc.devops.service.DriverService;

/**  
 * date：2016年2月4日 下午3:13:09
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DriverService.java
 * description： 数据库驱动数据维护接口实现类
 */
@org.springframework.stereotype.Component
public class DriverServiceImpl implements DriverService {
	@Resource
	private DriverMapper driverMapper;
	@Resource
	private ComponentMapper componentMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DriverService#listByDatabaseId(java.lang.String,
	 * com.sgcc.devops.dao.entity.User)
	 */
	@Override
	public JSONArray listByDatabaseId(String databaseId, User user) {
		Component component = new Component();
		component.setComponentId(databaseId);
		component = componentMapper.selectComponent(component);
		JSONArray jaArray = new JSONArray();
		if (component != null) {
			List<Driver> drivers = driverMapper.selectByType(component.getComponentDBType());
			jaArray = JSONUtil.parseObjectToJsonArray(drivers);
		}
		return jaArray;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DriverService#selectByPrimaryId(java.lang.String)
	 */
	@Override
	public Driver selectByPrimaryId(String driverId) {
		return driverMapper.selectByPrimaryId(driverId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DriverService#getDatabaseDriverService(java.lang.Byte)
	 */
	@Override
	public JSONObject getDatabaseDriverService(int DBType) {
		JSONObject resultObject = new JSONObject();
		List<Driver> drivers = driverMapper.selectByType(DBType);
		Driver driver = null;
		if(drivers != null && drivers.size() != 0){
			driver = drivers.get(0);
		}
		resultObject.put("driver", driver);
		return resultObject;
	}

}
