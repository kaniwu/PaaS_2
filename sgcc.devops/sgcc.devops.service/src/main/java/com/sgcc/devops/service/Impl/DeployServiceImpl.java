package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.intf.DeployMapper;
import com.sgcc.devops.service.DeployService;
/**  
 * date：2016年2月4日 下午3:07:14
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployService.java
 * description：物理系统发布版本数据操作接口实现类
 */
@Component
public class DeployServiceImpl implements DeployService {

	private static Logger logger = Logger.getLogger(DeployServiceImpl.class);

	@Resource
	private DeployMapper deployMapper;

	@Override
	public String createDeploy(Deploy record) {
		try {
			if (!StringUtils.hasText(record.getDeployId())) {
				record.setDeployId(KeyGenerator.uuid());
			}
			deployMapper.insertSystem(record);
			return record.getDeployId();
		} catch (Exception e) {
			logger.error("create deploy fail: " + e.getMessage());
			return null;
		}
	}

	@Override
	public int deleteDeploy(String deployId) {
		int result = 1;
		try {
			deployMapper.deleteByPrimaryKey(deployId);
		} catch (Exception e) {
			logger.error("delete deploy fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	public GridBean getOnePageDeployList(String userId, int page, int rows, DeployModel deployModel) {
		PageHelper.startPage(page, rows);
		List<Deploy> deploys = deployMapper.selectAll(userId, page, rows);
		int totalpage = ((Page<?>) deploys).getPages();
		Long totalNum = ((Page<?>) deploys).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), deploys);
		return gridBean;
	}

	@Override
	public Deploy getDeploy(String deployId) {
		return deployMapper.selectByPrimaryKey(deployId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployService#getDeployByClusterId(java.lang.
	 * Integer)
	 */
	@Override
	public List<Deploy> getDeployByClusterId(String clusterId) {
		return deployMapper.getDeployByClusterId(clusterId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployService#updateDeploy(com.sgcc.devops.dao.
	 * entity.Deploy)
	 */
	@Override
	public int updateDeploy(Deploy deploy) {
		try {
			return deployMapper.updateByPrimaryKey(deploy);
		} catch (Exception e) {
			logger.error("update deploy fail: " + e.getMessage());
			return 0;
		}
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployService#select(com.sgcc.devops.dao.entity.Deploy)
	 */
	@Override
	public List<Deploy> select(Deploy deploy) {
		return deployMapper.select(deploy);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployService#getUndeployedByTimerTask()
	 */
	@Override
	public List<Deploy> getUndeployedByTimerTask(String dialect) throws Exception {
		
		return deployMapper.getUndeployedByTimerTask(dialect);
	}

}
