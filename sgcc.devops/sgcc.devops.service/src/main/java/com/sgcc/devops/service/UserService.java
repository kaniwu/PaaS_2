package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONObject;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.entity.UserRole;

/**  
 * date：2015年8月14日 上午10:33:39  
 * project name：cmbc-devops-service  
 * @author langzi  
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：UserService.java  
 * description：  
 */
public interface UserService {
	
	/**
	 * Check user login status
	 * @author langzi
	 * @param userName
	 * @param password
	 * @return
	 * @version 1.0
	 * 2015年8月17日
	 * @throws Exception 
	 */
	public User checkLogin(String userName, String password) throws Exception;
	
	/**
	 * Get User Object by userName
	 * @author langzi
	 * @param userName
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 * @throws Exception 
	 */
	public User getUser(User user) throws Exception;
	
	/**
	 * @author langzi
	 * @param user
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 * @throws Exception 
	 */
	public JSONObject getJsonObjectOfUser(User user) throws Exception;
	
	/**用户分页
	 * @param owner 所有者
	 * @param name 用户名称
	 * @param pageNum 当前页
	 * @param pageSize 页面行数
	 * @return
	 * @throws Exception 
	 */
	public GridBean list(String owner, int pageNum, int pageSize,User user) throws Exception;
	
	/**添加用户
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public int create(User user) throws Exception;
	
	/**更新用户
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public int update(User user) throws Exception;
	
	/**更新用户权限关系
	 * @param param
	 * @return
	 * @throws Exception 
	 */
	public int updateUserRole(UserRole userRole) throws Exception;
	
	/**删除用户
	 * @param paramId
	 * @return
	 * @throws Exception 
	 */
	public int delete(String userId) throws Exception;
	
	/**删除用户
	 * @param paramId
	 * @return
	 * @throws Exception 
	 */
	public int active(String userId) throws Exception;
	
	/**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年11月10日
	 */
//	public int login(String userId);
	
	/**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年11月10日
	 */
//	public int logout(String userId);
	
	/**批量删除用户
	 * @param ids
	 * @return
	 */
	public int deletes(List<String> ids);
	
	/**验证用户的唯一性
 	 * @param userName
	 * @return
	 * @throws Exception 
	 */
	public User getUserByName(String userName) throws Exception;
	
	/**
	 * @author luogan
	 * 用户列表高级查询
	 * @throws Exception 
	 */
	public GridBean advancedSearchUser(String userId, int pagenum, int pagesize, User user,JSONObject json_object) throws Exception;
	
	public int deleteByUserId(String userId) throws Exception;

	/**
	* TODO
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年3月10日
	*/
	int updatePassword(String userName, String password);

	/**
	* TODO
	* @author mayh
	* @return List<User>
	* @version 1.0
	* 2016年8月5日
	*/
	public List<User> selectAllUsers(User user)  throws Exception;

	/**
	 * 初始化用户登录状态，在系统重新启动时使用。
	 * @return
	 */
//	public Result initUser();
	
}
