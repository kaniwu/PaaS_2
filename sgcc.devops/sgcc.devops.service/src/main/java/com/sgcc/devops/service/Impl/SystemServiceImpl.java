/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.SystemAppModel;
import com.sgcc.devops.common.model.SystemModel;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.AppPrelib;
import com.sgcc.devops.dao.entity.Business;
import com.sgcc.devops.dao.entity.BusinessSys;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.entity.DeployFile;
import com.sgcc.devops.dao.entity.DeployNgnix;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.AppPrelibMapper;
import com.sgcc.devops.dao.intf.BusinessMapper;
import com.sgcc.devops.dao.intf.BusinessSysMapper;
import com.sgcc.devops.dao.intf.ComponentTypeMapper;
import com.sgcc.devops.dao.intf.ContainerMapper;
import com.sgcc.devops.dao.intf.DeployDatabaseMapper;
import com.sgcc.devops.dao.intf.DeployFileMapper;
import com.sgcc.devops.dao.intf.DeployMapper;
import com.sgcc.devops.dao.intf.DeployNgnixMapper;
import com.sgcc.devops.dao.intf.EnvironmentMapper;
import com.sgcc.devops.dao.intf.ImageMapper;
import com.sgcc.devops.dao.intf.SystemAppMapper;
import com.sgcc.devops.dao.intf.SystemMapper;
import com.sgcc.devops.dao.intf.UserMapper;
import com.sgcc.devops.service.SystemService;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:38:53
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：SystemService.java
 * description：物理系统业务数据维护接口实现类  
 */
@Component
public class SystemServiceImpl implements SystemService {

	private static Logger logger = Logger.getLogger(SystemServiceImpl.class);

	@Resource
	private SystemMapper systemMapper;
	@Resource
	private DeployMapper deployMapper;
	@Resource
	private ComponentTypeMapper componentTypeMapper;
	@Resource
	private ImageMapper imageMapper;
	@Resource
	private BusinessMapper businessMapper;
	@Resource
	private DeployDatabaseMapper deployDatabaseMapper;
	@Resource
	private AppPrelibMapper appPrelibMapper;
	@Resource
	private DeployNgnixMapper deployNgnixMapper;
	@Resource
	private ContainerMapper containerMapper;
	@Resource
	private BusinessSysMapper businessSysMapper;
	@Resource
	private DeployFileMapper deployFileMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private EnvironmentMapper environmentMapper;
	@Resource
	private SystemAppMapper systemAppMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.SystemService#getOnePageSystemlications(int)
	 */
	@Override
	public GridBean listOnePageSystems(PagerModel pagerModel,User user,String businessId,String systemName,Boolean isAllAuth,String envId) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		System system = new System();
		system.setSystemName(systemName);
		system.setBusinessId(businessId);
		system.setUserId(user.getUserId());
		system.setIsAllAuth(isAllAuth);
		
		//环境变量
		if(org.apache.commons.lang.StringUtils.isEmpty(user.getUserCompany())){
			system.setEnvId(envId);
		}else{
			system.setEnvId(user.getUserCompany());
		}
		int systemsCount = systemMapper.selectSystemCount(system);
		if(systemsCount<(page-1)*pageSize){
			page = 1;
		}
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("systemName")){
				PageHelper.startPage(page, pageSize,"SYSTEM_NAME "+sord);
			}
			if(sidx.equals("systemDeployStatus")){
				PageHelper.startPage(page, pageSize,"SYSTEM_DEPLOY_STATUS,SYSTEM_NAME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		system.setDialect(jdbcConfig.getDialect());
		// 应用状态（0 删除 1正常）
		List<System> systems = systemMapper.selectAll(system);
		int totalpage = ((Page<?>) systems).getPages();
		Long totalNum = ((Page<?>) systems).getTotal();
		List<SystemModel> systemModels = new ArrayList<SystemModel>();
		for (System system2 : systems) {
			SystemModel systemModel = new SystemModel();
			systemModel.setSystemId(system2.getSystemId());
			systemModel.setSystemName(system2.getSystemName());
			systemModel.setSystemCode(system2.getSystemCode());
			systemModel.setSystemDeployStatus(system2.getSystemDeployStatus());
			systemModel.setDeployId(system2.getDeployId());
			systemModel.setSystemElsasticityStatus(system2.getSystemElsasticityStatus());
			systemModel.setSupporter(system2.getSupporter());
			// 目前运行版本
			Deploy deploy = deployMapper.selectByPrimaryKey(system2.getDeployId());
			systemModel.setInitCount(deploy == null ? 0 :deploy.getInitCount());
			systemModel.setInstanceCount(deploy == null ? 0:deploy.getInstanceCount());
			systemModel.setRunningVersion(deploy == null ? "" : deploy.getDeployVersion());
			if(deploy!=null&&deploy.getUrl()!=null){
				String url = deploy.getHttp()+"://"+deploy.getDomainName()+"/"+deploy.getAppName()+"/"+deploy.getUrl();
				systemModel.setUrl(url);
			}
			// 最新版本
			List<Deploy> deploys = deployMapper.selectLastBySystemId(system2.getSystemId());
			if (null != deploys && deploys.size() > 0) {
				systemModel.setNewestVersion(deploys.get(0).getDeployVersion());
			}
			BusinessSys businessSys = new BusinessSys();
			businessSys.setSysId(system2.getSystemId());
			List<BusinessSys> list = businessSysMapper.load(businessSys);
			String businessName = "";
			for(BusinessSys businessSys2:list){
				Business business = businessMapper.selectByPrimaryKey(businessSys2.getBusinessId());
				if(null!=business){
					businessName+=business.getBusinessName()+" | ";
				}
			}
			if(null!=businessName&&!"".equals(businessName)){
				systemModel.setBusinessName(businessName.substring(0, businessName.length()-2));
			}
			
			systemModels.add(systemModel);
		}
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), systemModels);
		return gridBean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.SystemService#create(com.sgcc.devops.model.
	 * SystemModel, com.sgcc.devops.entity.User)
	 */
	@Override
	public int create(SystemModel systemModel, User user) {
		System record = new System();
		record.setSystemName(systemModel.getSystemName());
		record.setSupporter(systemModel.getSupporter());
		record.setSystemVersion(systemModel.getSystemVersion());
		record.setSystemDeployStatus((byte) 2);
		record.setSystemElsasticityStatus((byte) 2);
		record.setSystemCode(systemModel.getSystemCode());
		if (systemModel.getPhysicalId() == null) {
			record.setSystemType(new Integer(2));
		} else {
			record.setSystemType(new Integer(1));
		}
		record.setOperation((byte)Type.OPERATION.UNUSED.ordinal());
		List<Environment> environments = environmentMapper.selectEnvironments(new Environment());
		record.setSystemId(KeyGenerator.uuid());
		//所有物理系统赋予新增环境变量
		for(Environment environment:environments){
			String systemId = KeyGenerator.uuid();
			record.setSystemId(systemId);
			record.setEnvId(environment.geteId());
			int re = systemMapper.insertSystem(record);
			//同步应用管理
			if(re==1){
				List<SystemApp> apps = systemAppMapper.selectSystemApps(systemModel.getSystemId());
				for(SystemApp app:apps){
					if(app.getStatus()==Status.SYSTEMAPP.NORMAL.ordinal()){
						app.setId(KeyGenerator.uuid());
						app.setSystemId(systemId);
						systemMapper.insertSystemApp(app);
					}
				}
			}
		}
		return 1;
	}

	@Override
	public System loadById(String systemId) {
		return systemMapper.selectByPrimaryKey(systemId);
	}

	@Override
	public int updateSystem(System system) {
		return systemMapper.updateSystem(system);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.SystemService#getOnePageSystemDeployInfo(java.
	 * lang.Integer, int, int, com.sgcc.devops.common.model.DeployModel, int)
	 */
	@Override
	public GridBean getOnePageSystemDeployInfo(String userId,PagerModel pagerModel, DeployModel deployModel,
			String systemId) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("deployVersion")){
				PageHelper.startPage(page, pageSize,"DEPLOY_VERSION "+sord);
			}
			if(sidx.equals("createTime")){
				PageHelper.startPage(page, pageSize,"CREATE_TIMTE "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		List<Deploy> deploys = deployMapper.selectAllBySystemId(systemId);
		int totalpage = ((Page<?>) deploys).getPages();
		Long totalNum = ((Page<?>) deploys).getTotal();
		List<DeployModel> deployModels = new ArrayList<DeployModel>();
		for (Deploy deploy : deploys) {
			DeployModel deployModel2 = new DeployModel();
			deployModel2.setDeployId(deploy.getDeployId());
			deployModel2.setDeployVersion(deploy.getDeployVersion());
			deployModel2.setAutoDeploy(deploy.getVersionChange());
			// deployModel2.setDataSource(deploy.getDataSource());
			deployModel2.setInstanceCount(deploy.getInstanceCount());
			deployModel2.setInitCount(deploy.getInitCount());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			deployModel2.setCreateTime(sdf.format(deploy.getCreateTime()));
			deployModel2.setClusterId(deploy.getClusterId());
			Image image = imageMapper.selectByPrimaryKey(deploy.getAppImageId());
			System system = systemMapper.selectByPrimaryKey(systemId);
			if (system.getDeployId() != null && system.getDeployId().equals(deploy.getDeployId())) {
				deployModel2.setStatus("1");// 正在运行
			} else {
				if (Status.IMAGE.DELETED.ordinal() == image.getImageStatus()) {
					deployModel2.setStatus("2");// 镜像已删除
				}
				if (Status.IMAGE.NORMAL.ordinal() == image.getImageStatus()) {
					deployModel2.setStatus("3");// 镜像正常
				}
				if (Status.IMAGE.ABNORMAL.ordinal() == image.getImageStatus()) {
					deployModel2.setStatus("4");// 镜像连接异常
				}
			}

			deployModel2.setImageName(image.getTempName());
			List<DeployDatabase> databases = deployDatabaseMapper.selectByDeployId(deployModel2.getDeployId());
			String dataInfoString = "";
			for (DeployDatabase deployDatabase : databases) {
				dataInfoString += deployDatabase.getDataSource() + " | ";
			}
			if (!"".equals(dataInfoString)) {
				deployModel2.setDatabaseInfo(dataInfoString.substring(0, dataInfoString.length() - 2));
			}
			deployModels.add(deployModel2);
		}
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), deployModels);
		return gridBean;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.SystemService#getBaseDeploy(java.lang.Integer,
	 * com.sgcc.devops.dao.entity.User)
	 */
	@Override
	public JSONObject getBaseDeploy(String systemId, User user) {
		System system = systemMapper.selectByPrimaryKey(systemId);
		JSONObject jo = new JSONObject();

		if (system != null && system.getDeployId() != null) {
			Deploy deploy = deployMapper.selectByPrimaryKey(system.getDeployId());
			if (deploy != null) {
				jo = JSONObject.fromObject(deploy);
				jo.put("publishTime", deploy.getPublishTime());
				
				//数据库
				List<DeployDatabase> deployDatabases = deployDatabaseMapper.selectByDeployId(deploy.getDeployId());
				if (deployDatabases.size() == 0) {
					jo.put("deployDatabases", null);
				} else {
					jo.put("deployDatabases", deployDatabases);
				}
				//配置文件
				List<DeployFile> deployFiles = deployFileMapper.selectByDeployId(deploy.getDeployId());
				jo.put("deployFiles", deployFiles);
				//组件nginx
				if(2==deploy.getLoadBalanceType()){
					List<DeployNgnix> deployNgnixs = deployNgnixMapper.selectByDeployId(deploy.getDeployId());
					if (deployDatabases.size() == 0) {
						jo.put("deployNgnixs", null);
					} else {
						jo.put("deployNgnixs", deployNgnixs);
					}
				}
			}

		}

		return jo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.SystemService#getAllSystemDeployed()
	 */
	@Override
	public List<System> listDeployedSystem() {
		return systemMapper.getAllSystemDeployed(Status.SYSTEM.DEPLOY.ordinal());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.SystemService#getSystemById(int)
	 */
	@Override
	public System getSystemById(String systemId) {
		return systemMapper.selectByPrimaryKey(systemId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.SystemService#listContainers(int)
	 */
	@Override
	public List<Container> listContainers(String systemId) {
		List<Container> containers = containerMapper.selectContainerBySysId(systemId);
		return containers;
	}

	@Override
	public System selectSystem(System system) {
		try {
			system = systemMapper.selectSystem(system);
		} catch (Exception e) {
			logger.error("select system fail" + e.getMessage());
			return null;
		}
		return system;
	}

	@Override
	public int selectSystemCount(System system) {
		try {
			system.setDialect(jdbcConfig.getDialect());
			return systemMapper.selectSystemCount(system);
		} catch (Exception e) {
			logger.error("select system count error" + e.getMessage());
			return 0;
		}

	}

	@Override
	public List<SystemModel> getListByBusinessId(String businessId, User user,String envId) {
		BusinessSys businessSys = new BusinessSys(businessId);
		List<BusinessSys> list = businessSysMapper.load(businessSys);
		List<System> systems = new ArrayList<System>();
		for(BusinessSys businessSys2:list){
			System system = systemMapper.selectByPrimaryKey(businessSys2.getSysId());
			systems.add(system);
		}
		List<SystemModel> systemModels = new ArrayList<SystemModel>();
		for (System system2 : systems) {
			if(StringUtils.isNotEmpty(envId)&&!envId.equals(system2.getEnvId())){
				continue;
			}
			SystemModel systemModel = new SystemModel();
			systemModel.setSystemId(system2.getSystemId());
			systemModel.setSystemName(system2.getSystemName());
			systemModel.setSystemDeployStatus(system2.getSystemDeployStatus());
			systemModel.setDeployId(system2.getDeployId());
			// 目前运行版本
			Deploy deploy = deployMapper.selectByPrimaryKey(system2.getDeployId());
			systemModel.setRunningVersion(deploy == null ? null : deploy.getDeployVersion());
			// 最新版本
			List<Deploy> deploys = deployMapper.selectLastBySystemId(system2.getSystemId());
			if (null != deploys && deploys.size() > 0) {
				systemModel.setNewestVersion(deploys.get(0).getDeployVersion());
			}

			BusinessSys businessSys1 = new BusinessSys();
			businessSys1.setSysId(system2.getSystemId());
			List<BusinessSys> list2 = businessSysMapper.load(businessSys1);
			String businessName = null;
			for(BusinessSys businessSys2:list2){
				Business business = businessMapper.selectByPrimaryKey(businessSys2.getBusinessId());
				businessName+=business.getBusinessName()+" | ";
			}
			if(null!=businessName){
				systemModel.setBusinessName(businessName.substring(0, businessName.length()-1));
			}
			systemModels.add(systemModel);
		}
		return systemModels;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#getSystemNames(java.lang.String)
	 */
	@Override
	public List<System> getSystemNames(String hostId) {
		
		return systemMapper.getSystemNames(hostId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#listUnDeployedSystem()
	 */
	@Override
	public List<System> listUnDeployedSystem() {
		return systemMapper.getAllSystemDeployed(Status.SYSTEM.UNDEPLOY.ordinal());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.SystemService#getAppPackage(java.lang.String,
	 * com.sgcc.devops.dao.entity.User)
	 */
	@Override
	public GridBean getAppPackage(PagerModel pagerModel,String systemId) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("appVersion")){
				PageHelper.startPage(page, pageSize,"APP_VERSION "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		//List<Deploy> deploys = deployMapper.selectAllBySystemId(systemId);
		List<SystemApp> systemApps= systemMapper.getAppPackageBySysId(systemId);
		int totalpage = ((Page<?>) systemApps).getPages();
		Long totalNum = ((Page<?>) systemApps).getTotal();
		List<SystemAppModel> systemAppModels = new ArrayList<SystemAppModel>();
		System currentSystem = getSystemById(systemId);
		for (SystemApp systemApp : systemApps) {
			SystemAppModel SystemAppModel2 = new SystemAppModel();
			SystemAppModel2.setId(systemApp.getId());
			SystemAppModel2.setSystemId(systemApp.getSystemId());
			SystemAppModel2.setAppRoute(systemApp.getAppRoute());
			SystemAppModel2.setAppVersion(systemApp.getAppVersion());
			SystemAppModel2.setSystemName(currentSystem.getSystemName());
			SystemAppModel2.setStatus(systemApp.getStatus());
			SystemAppModel2.setAppType(systemApp.getAppType());
			systemAppModels.add(SystemAppModel2);
		}
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), systemAppModels);
		return gridBean;
	}
	/*
	 * (non-Javadoc)
	 * 新增物理系统应用包
	 * @see com.cmbc.devops.service.SystemService#createSystemAppPackage(com.cmbc.devops.model.SystemAppModel)
	 */
	@Override
	public int createSystemAppPackage(SystemApp systemApp) {
		systemApp.setId(KeyGenerator.uuid());
		systemApp.setStatus(1);
		int i = systemMapper.insertSystemApp(systemApp);
		return i;
	}



	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#selectAllAdminService()
	 * 查询所有的物理系统管理员
	 */
	@Override
	public List<User> selectAllAdminService() {
		
		return userMapper.selectAllAdmin();
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#selectAdminsBySystemId(java.lang.String)
	 * 查询当前物理系统的管理员
	 */
	@Override
	public List<User> selectAdminsBySystemId(String systemId) {
		
		return userMapper.selectSystemAdmins(systemId);
	}
	

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#addSysAdminService(java.util.List)
	 */
	@Override
	public int addSysAdminService(List<Map<String, Object>> list) {
		int result = 1;
		try{
			systemMapper.addSystemAdmins(list);
		} catch (Exception e) {
			logger.error("save SystemAdmin fail" + e);
			result = 0;
			return result;
		}
			return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#delSysAdminsService(java.util.List)
	 */
	@Override
	public int delSysAdminsService(List<Map<String, Object>> list) {
		int result = 1;
		try{
			systemMapper.delSystemAdmins(list);
		} catch (Exception e) {
			logger.error("delete SystemAdmins fail" + e);
			result = 0;
			return result;
		}
			return result;
	}

	/**
	 *  删除物理系统应用包
	 */
	@Override
	public int deleteSystemAppPackage(SystemAppModel systemAppModel) {
		SystemApp systemApp = new SystemApp();
		systemApp.setId(systemAppModel.getId());
		systemApp.setStatus(0);
		int i = systemMapper.updateSystemApp(systemApp);
		return i;
	}
	
	/**
	 * 检查物理系统应用包是否被使用
	 */
	@Override
	public boolean checkAppUsed(SystemAppModel systemAppModel) {
		Deploy deploy = new Deploy();
		deploy.setSystemAppId(systemAppModel.getId());
		List<Deploy> deploys = deployMapper.select(deploy);
		if (deploys.size()>0) {
			return true;
		}else{
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#createAppPrelib(com.sgcc.devops.dao.entity.AppPrelib)
	 */
	@Override
	public void createAppPrelib(AppPrelib appPrelib) {
		appPrelib.setId(KeyGenerator.uuid());
		appPrelibMapper.createAppPrelib(appPrelib);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#selectAllSystem(java.lang.String)
	 */
	@Override
	public List<System> selectAllSystem(System record) {
		record.setDialect(jdbcConfig.getDialect());
		return systemMapper.selectAll(record);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#delete(java.lang.String)
	 */
	@Override
	public boolean delete(String systemId) throws Exception {
		int i = systemMapper.deleteByPrimaryKey(systemId);
		if(i==1){
			return true;
		}
		return false;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.SystemService#listWaitContainers(java.lang.String)
	 */
	@Override
	public List<Container> listWaitContainers(String systemId) {
		List<Container> containers = containerMapper.listWaitContainersBySysId(systemId);
		return containers;
	}

	@Override
	public SystemApp getAppPackageById(String systemAppId) {
		// TODO Auto-generated method stub
		SystemApp systemApp = systemMapper.getAppPackageById(systemAppId);
		return systemApp;
	}

}
