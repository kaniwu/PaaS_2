/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.dao.intf.RegistryLbMapper;
import com.sgcc.devops.dao.intf.RegistryMapper;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.RegistryService;

@Component
public class RegistryLbServiceImpl implements RegistryLbService {
	Logger logger = Logger.getLogger(RegistryLbServiceImpl.class);

	@Resource
	private RegistryLbMapper registryLbMapper;
	@Resource
	private RegistryMapper registryMapper;
	@Resource
	private HostMapper hostMapper;
	@Resource
	private RegistryService registryService;
	@Resource
	private JdbcConfig jdbcConfig;
	
	@Override
	public int createRegistryLb(RegistryLb registryLb) {
		int result = 1;
		try {
			registryLbMapper.insertRegistryLb(registryLb);
		} catch (Exception e) {
			logger.error("(创建仓库失败)create registrylb exception: " + e.getMessage());
			result = 0;
		}
		return result;
	}
	@Override
	public RegistryLb registryLbDetails(String id) {
		RegistryLb registryLb = new RegistryLb();
		registryLb.setId(id);
		RegistryLb result =null;
		try {
			result=registryLbMapper.selectRegistryLb(registryLb);
		} catch (Exception e) {
			logger.error("select registrylb exception: " + e.getMessage());
		}
		return result;
	}
	@Override
	public int deleteRegistryLb(String id) {
		int result =0;
		try {
			List<String> listLb = new ArrayList<String>();
			List<String> list = new ArrayList<String>();
			listLb.add(id);
			Registry recordLb = new Registry();
			recordLb.setLbId(id);
			List<Registry> registrys= registryMapper.selectRegistryByLbId(recordLb);
			
			for (Registry registry : registrys) {
				if (org.apache.commons.lang.StringUtils.isNotEmpty(registry.getRegistryId())) {
					list.add(registry.getRegistryId());
				}
			}
			try {
				// 将仓库所在主机类型转为UNUSED
				for (String registryId : list) {
					Registry record = new Registry();
					record.setRegistryId(registryId);
					record = registryMapper.selectRegistry(record);
					String hostId = record.getHostId();
					Host host = new Host();
					host.setHostId(hostId);
					host = hostMapper.selectHost(host);
					host.setHostType((byte) Type.HOST.UNUSED.ordinal());
					hostMapper.updateHost(host);
				}
				if(!list.isEmpty()){
					registryMapper.changeStatus(list);
				}
			} catch (Exception e) {
				logger.error("(删除仓库失败)delete registry exception: " + e.getMessage() );
				result = 0;
			}
			result=registryLbMapper.changeStatus(listLb);
		} catch (Exception e) {
			logger.error("delete registrylb exception: " + e.getMessage());
		}
		return result;
	}
	@Override
	public int updateRegistryLb(RegistryLb registryLb) {
		int result =0;
		try {
			result=registryLbMapper.updateRegistryLb(registryLb);
		} catch (Exception e) {
			logger.error("update registrylb exception: " + e.getMessage());
		}
		return result;
	}
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RegistryLbService#getRegistryLb(com.sgcc.devops.dao.entity.RegistryLb)
	 */
	@Override
	public List<RegistryLb> getRegistryLb(RegistryLb lb) {
		try {
			lb.setDialect(jdbcConfig.getDialect());
			List<RegistryLb> registryLbs =registryLbMapper.selectAllWithIP(lb);
			return registryLbs;
		} catch (Exception e) {
			logger.error("select registrylb exception: " + e.getMessage());
			return null;
		}
	}
	@Override
	public List<RegistryLb> getAllRegistryLb() {
		List<RegistryLb> registryLbs = new ArrayList<RegistryLb>();
		try {
			registryLbs =registryLbMapper.selectAll();
			return registryLbs;
		} catch (Exception e) {
			logger.error("select registrylb exception: " + e.getMessage());
			return registryLbs;
		}
	}
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RegistryLbService#selectRegistryLb(com.sgcc.devops.dao.entity.RegistryLb)
	 */
	@Override
	public RegistryLb selectRegistryLb(RegistryLb registryLb) {
		return registryLbMapper.selectRegistryLb(registryLb);
	}
	@Override
	public Result registryLbDelete(String  id) {
		String exist = null;

		try {
			exist = registryService.checkImageIsExistByID(id);
		} catch (Exception e) {
			logger.error("检查仓库镜像是否存在失败: " + e.getMessage());
			return new Result(false, "检查仓库镜像是否存在失败."
					+ e.getMessage());
		}
		if (exist != null && !"".equals(exist)) {
			// json_object.put("result", 2);
			// json_object.put("exist", exist);
			logger.info("仓库中存在镜像，删除仓库失败！");
			return new Result(false, "仓库中存在镜像，删除仓库失败！");
		} else {
			try {
				int result = deleteRegistryLb(id);
				if (result == 1) {
					// json_object.put("result", result);
					logger.info("仓库删除成功。");
					return new Result(true, "仓库删除成功。");
				} else {
					// json_object.put("result", result);
					logger.info("仓库中无镜像，数据库操作失败！");
					return new Result(false, "仓库中无镜像，数据库操作失败！");
				}
			} catch (Exception e) {
				logger.error("删除仓库失败！" + e.getMessage());
				return new Result(false, "删除仓库失败！"
						+ e.getMessage());
			}
		}
	}

}
