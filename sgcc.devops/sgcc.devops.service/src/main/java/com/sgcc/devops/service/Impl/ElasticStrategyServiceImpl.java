package com.sgcc.devops.service.Impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.ElasticItem;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.intf.ElasticItemMapper;
import com.sgcc.devops.dao.intf.ElasticStrategyMapper;
import com.sgcc.devops.service.ElasticStrategyService;
/**  
 * date：2016年2月4日 下午3:17:35
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticStrategyService.java
 * description：  弹性伸缩策略数据维护接口实现类
 */
@Component
public class ElasticStrategyServiceImpl implements ElasticStrategyService {

	private static Logger logger = Logger.getLogger(ElasticStrategyServiceImpl.class);

	@Autowired
	private ElasticStrategyMapper elasticStrategyMapper;
	@Autowired
	private ElasticItemMapper elasticItemMapper;

	@Override
	public Result insert(ElasticStrategy record) {
		try {
			record.setId(KeyGenerator.uuid());
			elasticStrategyMapper.insert(record);
		} catch (Exception e) {
			logger.error("insert elasticStrategy fail: " + e.getMessage());
			return new Result(false, "策略添加失败");
		}

		return new Result(true, "添加成功");
	}

	@Override
	public Result delete(String id) {
		try {
			elasticStrategyMapper.delete(id);
		} catch (Exception e) {
			logger.error("delete elasticStrategy fail: " + e.getMessage());
			return new Result(false, "删除失败");
		}

		return new Result(true, "删除成功");
	}

	@Override
	public Result update(ElasticStrategy record) {
		try {
			elasticStrategyMapper.update(record);
		} catch (Exception e) {
			logger.error("update elastic strategy fail: " + e.getMessage());
			return new Result(false, "更新失败");
		}

		return new Result(true, "更新成功");
	}

	@Override
	public ElasticStrategy load(String systemId) {
		ElasticStrategy elasticStrategy = null;
		try {
			elasticStrategy = elasticStrategyMapper.loadBySystem(systemId);
			if(elasticStrategy==null){
				return null;
			}
			List<ElasticItem> items = elasticItemMapper.loadByStrategy(elasticStrategy.getId());
			elasticStrategy.setItems(items);
		} catch (Exception e) {
			logger.error("load elastic strategy fail: " + e.getMessage());
			return null;
		}
		return elasticStrategy;
	}

	@Override
	public ElasticStrategy loadBySystem(String systemId) {
		ElasticStrategy elasticStrategy = null;
		try {
			elasticStrategy = elasticStrategyMapper.loadBySystem(systemId);
			if (elasticStrategy != null) {
				List<ElasticItem> items = elasticItemMapper.loadByStrategy(elasticStrategy.getId());
				elasticStrategy.setItems(items);
			}
		} catch (Exception e) {
			logger.error("load system elastic strategy fail: " + e.getMessage());
			return null;
		}
		return elasticStrategy;
	}

}
