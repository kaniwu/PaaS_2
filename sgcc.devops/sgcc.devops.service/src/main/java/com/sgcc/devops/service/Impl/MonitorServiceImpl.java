package com.sgcc.devops.service.Impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.TimeUtils;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.dao.intf.MonitorMapper;
import com.sgcc.devops.service.MonitorService;
/**  
 * date：2016年2月4日 下午3:24:56
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：MonitorService.java
 * description：主机、容器监控数据维护接口实现类
 */
@Component
public class MonitorServiceImpl implements MonitorService {

	private static Logger logger = Logger.getLogger(MonitorServiceImpl.class);

	@Resource
	private MonitorMapper monitorMapper;

	@Override
	public int insert(Monitor monitor) {
		try {
			monitor.setMonitorId(KeyGenerator.uuid());
			return monitorMapper.insert(monitor);
		} catch (Exception e) {
			logger.error("insert monitor fail: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public int deleteByPrimaryKey(String monitorId) {
		try {
			return monitorMapper.deleteByPrimaryKey(monitorId);
		} catch (Exception e) {
			logger.error("delete monitor error: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public int deleteByType(byte targetType) {
		try {
			return monitorMapper.deleteByType(targetType);
		} catch (Exception e) {
			logger.error("delete monitor fail: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public int deleteBytargetId(String targetId) {
		try {
			return monitorMapper.deleteBytargetId(targetId);
		} catch (Exception e) {
			logger.error("delete monitor fail: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public Monitor selectByPrimaryKey(String monitorId) {
		try {
			return monitorMapper.selectByPrimaryKey(monitorId);
		} catch (Exception e) {
			logger.error("select monitor fail: " + e.getMessage());
			return null;
		}

	}

	@Override
	public List<Monitor> selectByType(byte targetType) {
		try {
			return monitorMapper.selectByType(targetType);
		} catch (Exception e) {
			logger.error("select one type monitors fail: " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<Monitor> selectBytargetId(String targetId) {
		try {
			return monitorMapper.selectBytargetId(targetId);
		} catch (Exception e) {
			logger.error("select targeId monitor fail" + e.getMessage());
			return null;
		}
	}

	@Override
	public List<Monitor> select(Monitor monitor) {
		try {
			return monitorMapper.select(monitor);
		} catch (Exception e) {
			logger.error("select monitor fail: " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<Monitor> selectByTime(Date startTime, Date endTime) {
		Map<String, Timestamp> time = new HashMap<>();
		if (startTime != null) {
			time.put("startTime", new Timestamp(startTime.getTime()));
		}
		if (endTime != null) {
			time.put("endTime", new Timestamp(endTime.getTime()));
		}
		try {
			return monitorMapper.selectByTime(time);
		} catch (Exception e) {
			logger.error("select monitor between satrtime and endetime fail: " + e.getMessage());
			return null;
		}
	}

	@Override
	public Monitor selectByTypeAndId(Monitor monitor) {
		try {
			List<Monitor> monitors = monitorMapper.selectByTypeAndId(monitor);
			return null==monitors||monitors.size()==0?null:monitors.get(0);
		} catch (Exception e) {
			logger.error("select monitor fail: " + e.getMessage());
			return null;
		}
		
	}

	@Override
	public List<Monitor> selectLastMonitorData(byte targetType, String targetId, Long lastTime) {
		Monitor monitor = new Monitor();
		monitor.setTargetId(targetId);
		monitor.setTargetType(targetType);
		Date startTime = TimeUtils.stringToDateTime(TimeUtils.formatTime(new Date(System.currentTimeMillis() - lastTime*1000)));
		monitor.setMonitorTime(startTime);
		List<Monitor> monitors = new ArrayList<>();
		try {
			monitors = monitorMapper.select(monitor);
		} catch (Exception e) {
			logger.error("select last monitor data fail: " + e.getMessage());
			return null;
		}
		return monitors;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.MonitorService#getAvgData(com.sgcc.devops.dao.entity.Monitor)
	 */
	@Override
	public Monitor getAvgData(Monitor monitor) {
		return monitorMapper.getAvgData(monitor);
	}
}
