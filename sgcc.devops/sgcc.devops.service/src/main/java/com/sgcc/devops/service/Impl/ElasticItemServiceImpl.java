package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.ElasticItem;
import com.sgcc.devops.dao.intf.ElasticItemMapper;
import com.sgcc.devops.service.ElasticItemService;
/**  
 * date：2016年2月4日 下午3:17:06
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticItemService.java
 * description：  弹性伸缩策略条目数据维护接口实现类
 */
@Component
public class ElasticItemServiceImpl implements ElasticItemService {

	@Autowired
	private ElasticItemMapper elasticItemMapper;

	private static Logger logger = Logger.getLogger(ElasticItemServiceImpl.class);

	@Override
	public Result insert(ElasticItem record) {
		try {
			record.setId(KeyGenerator.uuid());
			elasticItemMapper.insert(record);
		} catch (Exception e) {
			logger.error("insert elasticItem " + record.getItemName() + " fail: " + e.getMessage());
			return new Result(false, record.getItemName() + "添加失败");
		}
		return new Result(true, "添加成功");
	}

	@Override
	public Result delete(String elasticItemId) {
		try {
			elasticItemMapper.delete(elasticItemId);
		} catch (Exception e) {
			logger.error("delete elasticItem " + elasticItemId + " fail: " + e.getMessage());
			return new Result(false, "删除失败");
		}
		return new Result(true, "删除成功");
	}

	@Override
	public Result update(ElasticItem record) {
		try {
			elasticItemMapper.update(record);
		} catch (Exception e) {
			logger.error("update ElasticItem fail: " + e.getMessage());
			return new Result(false, record.getItemName() + "更新失败");
		}
		return new Result(true, "更新成功");
	}

	@Override
	public ElasticItem load(String id) {
		ElasticItem elasticItem = null;
		try {
			elasticItem = elasticItemMapper.load(id);
		} catch (Exception e) {
			logger.error("load elasticItem fail: " + e.getMessage());
			return null;
		}
		return elasticItem;
	}

	@Override
	public List<ElasticItem> loadByStrategy(String strategyId) {
		List<ElasticItem> elasticItems = new ArrayList<>();
		try {
			elasticItems = elasticItemMapper.loadByStrategy(strategyId);
		} catch (Exception e) {
			logger.error("load elasticItem fail: " + e.getMessage());
			return null;
		}
		return elasticItems;
	}

}
