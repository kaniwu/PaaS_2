/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RegIdImageTypeModel;
import com.sgcc.devops.common.model.RegistryLbModel;
import com.sgcc.devops.common.model.RegistryModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.RegIdImageType;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.RegistrySlaveImage;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.DeployMapper;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.dao.intf.ImageMapper;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.dao.intf.RegistryLbMapper;
import com.sgcc.devops.dao.intf.RegistryMapper;
import com.sgcc.devops.dao.intf.SystemMapper;
import com.sgcc.devops.dao.intf.UserMapper;
import com.sgcc.devops.service.RegistryService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:34:34
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RegistryService.java
 * description：镜像仓库数据维护接口实现类
 */
@Component
public class RegistryServiceImpl implements RegistryService {
	Logger logger = Logger.getLogger(RegistryServiceImpl.class);

	@Resource
	private RegistryMapper registryMapper;
	@Resource
	private RegistryLbMapper registryLbMapper;
	@Resource
	private HostMapper hostMapper;
	@Resource
	private RegImageMapper regImageMapper;
	@Resource
	private ImageMapper imageMapper;
	@Resource
	private DeployMapper deployMapper;
	@Resource
	private SystemMapper systemMapper;
	@Resource
	private UserMapper userMapper;
	@Resource
	private JdbcConfig jdbcConfig;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#getOnePageRegistrys(int,
	 * com.sgcc.devops.model.RegistryModel)
	 */
	@Override
	public JSONArray getOnePageRegistrys(String userId, RegistryModel RegistryModel) {
		PageHelper.startPage(RegistryModel.getPage(), RegistryModel.getLimit());
		Registry registry = new Registry();
		registry.setRegistryName(RegistryModel.getSearch());
		registry.setRegistryStatus((byte) Status.REGISTRY.NORMAL.ordinal());
		registry.setDialect(jdbcConfig.getDialect());
		List<Registry> registries = registryMapper.selectAll(registry);
		// page total
		long totalpage = ((Page<?>) registries).getPages();
		long currentpage = ((Page<?>) registries).getPageNum();
		long pageSize = ((Page<?>) registries).getPageSize();
		long totalNum = ((Page<?>) registries).getTotal();

		JSONArray ja = new JSONArray();
		ja.add(totalpage);
		ja.add(currentpage);
		ja.add(pageSize);
		ja.add(totalNum);

		for (Registry registry2 : registries) {
			JSONObject jo = JSONUtil.parseObjectToJsonObject(registry2);
			if (registry2.getHostId() != null && !"".equals(registry2.getHostId())) {
				Host host = new Host();
				host.setHostId(registry2.getHostId());
				host = hostMapper.selectHost(host);
				jo.put("hostName", host == null ? "" : host.getHostName());
			} else {
				jo.put("hostName", "");
			}

			ja.add(jo);
		}
		return ja;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#createRegistry(net.sf.json.
	 * JSONObject)
	 */
	@Override
	public int createRegistry(JSONObject jo) {
		int result = 1;
		Registry record = new Registry();
		record.setRegistryName(jo.getString("registryName"));
		record.setRegistryDesc(jo.getString("registryDesc"));
		record.setRegistryPort(jo.getInt("registryPort"));
		record.setRegistryStatus((byte) Status.REGISTRY.NORMAL.ordinal());
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		record.setRegistryCreatetime(new Date());
		record.setRegistryCreator(jo.getString("userId"));
		record.setHostId(jo.getString("hostId"));
		record.setLbId(jo.getString("LbId"));;
		record.setRegistryId(KeyGenerator.uuid());
		try {
			registryMapper.insertRegistry(record);
		} catch (Exception e) {
			logger.error("(创建仓库失败)create registry exception: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#updateRegistry(net.sf.json.
	 * JSONObject)
	 */
	@Override
	public int updateRegistry(JSONObject jo) {
		int result = 1;
		try {
			Registry record = new Registry();
			record.setRegistryId(jo.getString("registryId"));
			record.setRegistryName(jo.getString("registryName"));
			record.setRegistryDesc(jo.getString("registryDesc"));
			registryMapper.updateRegistry(record);
		} catch (Exception e) {
			logger.error("(更新仓库失败)update registry exception: "+e.getMessage());
			result = 0;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#deleteRegistry(net.sf.json.
	 * JSONObject)
	 */
	@Override
	public int deleteRegistry(JSONObject jo) {
		int result = 1;
		String arrayString = jo.getString("array");
		List<String> list = new ArrayList<String>();

		String[] jaArray = arrayString.split(",");
		for (String registryid : jaArray) {
			if (org.apache.commons.lang.StringUtils.isNotEmpty(registryid)) {
				list.add(registryid);
			}
		}
		try {
			// 将仓库所在主机类型转为UNUSED
			for (String registryId : list) {
				Registry record = new Registry();
				record.setRegistryId(registryId);
				record = registryMapper.selectRegistry(record);
				String hostId = record.getHostId();
				Host host = new Host();
				host.setHostId(hostId);
				host = hostMapper.selectHost(host);
				host.setHostType((byte) Type.HOST.UNUSED.ordinal());
				hostMapper.updateHost(host);
			}
			if(!list.isEmpty()){
				registryMapper.changeStatus(list);
			}
		} catch (Exception e) {
			logger.error("(删除仓库失败)delete registry exception: " + e.getMessage() );
			result = 0;
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#getRegistry(com.sgcc.devops.
	 * entity.Registry)
	 */
	@Override
	public JSONObject getRegistry(Registry record) {
		Registry registry = registryMapper.selectRegistry(record);
		JSONObject jo = JSONUtil.parseObjectToJsonObject(registry);
		return jo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.RegistryService#getRegistryMster()
	 */
	@Override
	public JSONArray getRegistryMster() {
		JSONArray ja = new JSONArray();
		List<Host> hostList = hostMapper.selectHostList(Type.HOST.REGISTRY.ordinal());
		if (hostList != null) {
			for (Host host : hostList) {
				JSONObject jo = new JSONObject();
				jo.put("hostName", host.getHostName());
				jo.put("hostIp", host.getHostIp());
				jo.put("hostId", host.getHostId());
				ja.add(jo);
			}
		}
		return ja;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.RegistryService#checkImageIsExist(net.sf.json.
	 * JSONObject)
	 */
	@Override
	public String checkImageIsExist(JSONObject jo) {
		String array = jo.getString("array");
		String[] ids = array.split(",");
		String result = "";
		for (String id : ids) {
			RegImage regImage = new RegImage();
			regImage.setRegistryId(id);
			List<RegImage> list = regImageMapper.selectAll(regImage);
			if (list.size() > 0) {// 如果存在镜像，返回对象中添加此仓库的名称
				for (RegImage regImage2 : list) {
					Image image = imageMapper.selectByPrimaryKey(regImage2.getImageId());
					if (image != null && image.getImageStatus() == Status.IMAGE.NORMAL.ordinal()) {// 判断此镜像是否可用,如果可用，仓库不能删除
						Registry record = new Registry();
						record.setRegistryId(id);
						record = registryMapper.selectRegistry(record);
						result += (result == null || result.equals("")) ? record.getRegistryDesc()
								: "," + record.getRegistryDesc();
						break;
					}
				}
			}
		}
		return result;
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 11:15
	 * @description 将原来返回json字符串的方法修改为GridBean的方式
	 */
	@Override
	public GridBean getOnePageRegistrys(String userId, int pagenumber, int pagesize, RegistryModel RegistryModel) {

		PageHelper.startPage(pagenumber, pagesize);
		Registry registry = new Registry();
		registry.setRegistryName(RegistryModel.getSearch());
		registry.setRegistryStatus((byte) Status.REGISTRY.NORMAL.ordinal());
		registry.setDialect(jdbcConfig.getDialect());
		List<Registry> registries = registryMapper.selectAll(registry);
		int totalpage = ((Page<?>) registries).getPages();
		Long totalNum = ((Page<?>) registries).getTotal();

		GridBean gridBean = new GridBean(pagenumber, totalpage, totalNum.intValue(), registries);
		return gridBean;
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 11:15
	 * @description 将原来返回json字符串的方法修改为GridBean的方式
	 */
	@Override
	public GridBean getOnePageRegistrysWithIP(String userId,PagerModel pagerModel,RegistryLbModel RegistryLbModel) {

		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("registryName")){
				PageHelper.startPage(page, pageSize,"REGISTRY_NAME "+sord);
			}
			if(sidx.equals("LbIp")){
				PageHelper.startPage(page, pageSize,"LB_IP "+sord);
			}
			if(sidx.equals("registryCreatetime")){
				PageHelper.startPage(page, pageSize,"REGISTRY_CREATETIME "+sord);
			}
			if(sidx.equals("registryCreator")){
				PageHelper.startPage(page, pageSize,"REGISTRY_CREATOR "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		RegistryLb registryLb = new RegistryLb();
		registryLb.setRegistryName(RegistryLbModel.getRegistryName());
//		registrywithip.setRegistryStatus((byte) Status.REGISTRY.NORMAL.ordinal());
		registryLb.setLbIp(RegistryLbModel.getLbIp());
		registryLb.setDialect(jdbcConfig.getDialect());
		List<RegistryLb> registryLbs = registryLbMapper.selectAllWithIP(registryLb);
		for(RegistryLb lb:registryLbs){
			if(null!=lb&&lb.getRegistryCreator()!=null){
				User user = new User();
				user.setUserId(lb.getRegistryCreator());
				try {
					user= userMapper.selectUser(user);
					if(user!=null){
						lb.setRegistryCreator(user.getUserName());
					}
				} catch (Exception e) {
				}
			}
		}
		int totalpage = ((Page<?>) registryLbs).getPages();
		Long totalNum = ((Page<?>) registryLbs).getTotal();

		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), registryLbs);
		return gridBean;
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 11:15
	 * @description 查询仓库ID下所有的镜像信息，将原来返回json字符串的方法修改为GridBean的方式
	 */
	@Override
	public GridBean getOnePageRegistrysSlaveImages(String userId, PagerModel pagerModel,
			RegIdImageTypeModel regIdImageTypeModel, String registryid, byte imagestatus) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("tempName")){
				PageHelper.startPage(page, pageSize,"img.TEMP_NAME "+sord);
			}
			if(sidx.equals("imageName")){
				PageHelper.startPage(page, pageSize,"img.IMAGE_NAME "+sord);
			}
			if(sidx.equals("imageStatus")){
				PageHelper.startPage(page, pageSize,"img.IMAGE_STATUS "+sord);
			}
			if(sidx.equals("imageCreatetime")){
				PageHelper.startPage(page, pageSize,"img.IMAGE_CREATETIME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		RegIdImageType redIdImageType = new RegIdImageType();
		redIdImageType.setImageStatus(imagestatus);
		redIdImageType.setRegistryId(registryid);
		redIdImageType.setDialect(jdbcConfig.getDialect());
		List<RegistrySlaveImage> regislaveimages = registryMapper.selectAllImageInRegistry(redIdImageType);

		Deploy deploy = null;
		System system = null;
		/* 向元素中添加应用名称信息 */
		for (RegistrySlaveImage reg_slave_img : regislaveimages) {
			if (reg_slave_img.getDeployId() == null) {
				reg_slave_img.setSystemName("");
			} else {
				deploy = deployMapper.selectByPrimaryKey(reg_slave_img.getDeployId());
				system = systemMapper.selectByPrimaryKey(deploy.getSystemId());

				reg_slave_img.setSystemName(system.getSystemName());
			}
		}

		int totalpage = ((Page<?>) regislaveimages).getPages();
		Long totalNum = ((Page<?>) regislaveimages).getTotal();

		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), regislaveimages);
		return gridBean;
	}

	@Override
	public List<Registry> listRegistrysByHostId(String hostId) throws Exception {
		return registryMapper.selectRegistryByHostId(hostId);
	}

	@Override
	public int SyncRegistryImages(String RegistryNodeIP, String ServerPort) {
		return 0;
	}

	@Override
	public int registryCount() {
		int result = 0;
		try {
			result = registryMapper.registryCount();
		} catch (Exception e) {
			logger.error("select registry count fail: "+e.getMessage());
			return 0;
		}
		return result;
	}

	@Override
	public Registry selectRegistry(Registry registry) {
		try {
			registry = registryMapper.selectRegistry(registry);
		} catch (Exception e) {
			logger.error("select registry fail" + e.getMessage());
			return null;
		}
		return registry;
	}
	
	public List<Registry> selectAll() {
		try {
			List<Registry> list = registryMapper.selectAll();
			return list;
		} catch (Exception e) {
			logger.error("select registry fail" + e.getMessage());
			return null;
		}
	}
	@Override
	public int updateRegistrySta(JSONObject jo) {
		int result = 1;
		try {
			Registry record = new Registry();
			record.setRegistryId(jo.getString("registryId"));
			record.setRegistryName(jo.getString("registryName"));
			record.setRegistryDesc(jo.getString("registryDesc"));
			if("1".equals(jo.getString("registryStatus"))){
				record.setRegistryStatus((byte)Status.REGISTRY.NORMAL.ordinal());
			}
			if("3".equals(jo.getString("registryStatus"))){
				record.setRegistryStatus((byte)Status.REGISTRY.STOP.ordinal());
			}
			registryMapper.updateRegistry(record);
		} catch (Exception e) {
			logger.error("(更新仓库失败)update registry exception: "+e.getMessage());
			result = 0;
		}

		return result;
	}
	public List<Registry> selectRegistryByLbId(String lbId){
		Registry record = new Registry();
		record.setLbId(lbId);
		return registryMapper.selectRegistryByLbId(record);
		
	}
	public String checkImageIsExistByID(String id) {
			RegImage regImage = new RegImage();
			regImage.setRegistryId(id);
			String result = "";
			List<RegImage> list = regImageMapper.selectAll(regImage);
			if (list.size() > 0) {// 如果存在镜像，返回对象中添加此仓库的名称
				for (RegImage regImage2 : list) {
					Image image = imageMapper.selectByPrimaryKey(regImage2.getImageId());
					if (image != null && image.getImageStatus() == Status.IMAGE.NORMAL.ordinal()) {// 判断此镜像是否可用,如果可用，仓库不能删除
						RegistryLb record = new RegistryLb();
						record.setId(id);
						record = registryLbMapper.selectRegistryLb(record);
						 result =  record.getRegistryName();
						break;
					}
				}
			}
		return result;
	}
}
