/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.ServerRoom;
import com.sgcc.devops.dao.intf.RackMapper;
import com.sgcc.devops.dao.intf.ServerRoomMapper;
import com.sgcc.devops.service.ServerRoomService;

/**  
 * date：2016年3月16日 上午9:22:21
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ServerRoomServiceImpl.java
 * description：  
 */
@Component
public class ServerRoomServiceImpl implements ServerRoomService {
	private static Logger logger = Logger.getLogger(ServerRoomServiceImpl.class);
	@Resource
	private ServerRoomMapper serverRoomMapper;
	@Resource
	private RackMapper rackMapper;
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#deleteByPrimaryKey(java.lang.String)
	 */
	@Override
	public int deleteByPrimaryKey(String id) throws Exception {
			int result = 1;
			try {
				serverRoomMapper.deleteByPrimaryKey(id);
			} catch (Exception e) {
				logger.error("delete serverRoom fail: " + e.getMessage());
				result = 0;
			}
			return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#insert(com.sgcc.devops.dao.entity.ServerRoom)
	 */
	@Override
	public int insert(ServerRoom record) throws Exception {
		int result = 1;
		try{
			serverRoomMapper.insert(record);
		} catch (Exception e) {
			logger.error("insert serverRoom fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#insertSelective(com.sgcc.devops.dao.entity.ServerRoom)
	 */
	@Override
	public int insertSelective(ServerRoom record) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#selectByPrimaryKey(java.lang.String)
	 */
	@Override
	public ServerRoom selectByPrimaryKey(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#updateByPrimaryKeySelective(com.sgcc.devops.dao.entity.ServerRoom)
	 */
	@Override
	public int updateByPrimaryKeySelective(ServerRoom record) throws Exception {
		int result = 1;
		try{
			serverRoomMapper.updateByPrimaryKeySelective(record);
		}catch(Exception e){
			result = 0;
			logger.error("update serverRoom fail:" + e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#updateByPrimaryKey(com.sgcc.devops.dao.entity.ServerRoom)
	 */
	@Override
	public int updateByPrimaryKey(ServerRoom record) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#selectAll()
	 */
	@Override
	public List<ServerRoom> selectAll() {
		List<ServerRoom> list = serverRoomMapper.selectAll();
		return list;
	}

	/*
	 * 查询有无重复的机房名称
	 * panjing
	 */
	public boolean ifRepeat(String serverRoomName){
		boolean repeat = false;
		try{
			int num = serverRoomMapper.countServerRoomName(serverRoomName);
			if(num > 0){
				repeat = true;
			}
		}catch(Exception e){
			repeat = true;
			logger.error("count serverRoomName fail: " + e.getMessage());
		}
		return repeat;
	}
	

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#serverRoomList(com.sgcc.devops.common.model.PagerModel)
	 */
	@Override
	public GridBean serverRoomList(PagerModel pagerModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("serverRoomRemark")){
				PageHelper.startPage(page, pageSize,"SERVER_ROOM_REMARK "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		List<ServerRoom> serverRooms = serverRoomMapper.selectAll();
		int totalpage = ((Page<?>) serverRooms).getPages();
		Long totalNum = ((Page<?>) serverRooms).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), serverRooms);
		return gridBean;
	}

 
	/*
	 * 获取机房信息
	 * panjing
	 */
	@Override
	public ServerRoom getServerRoom(ServerRoom serverRoom) {
		try {
			serverRoom = serverRoomMapper.selectByPrimaryKey(serverRoom.getId());
		} catch (Exception e) {
			logger.error("select cluster fail: " + e.getMessage());
			return null;
		}
		return serverRoom;
	}

	
}
	