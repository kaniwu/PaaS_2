package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONObject;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.ComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentHost;

/**  
 * date：2016年2月4日 下午2:48:29
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentService.java
 * description：组件业务数据操作接口类
 */
public interface ComponentService {

	/** 创建新的组件 */
	public abstract int createComponent(ComponentModel componentModel,List<ComponentHost> componentHostList);

	/** 删除组件 */
	public Result deleteComponent(Component component);

	/** 更新组件 */
	public abstract int updateComponent(ComponentModel componentModel,List<ComponentHost> componentHostList);

	/** 获取一页组件列表 */
	public abstract GridBean getOnePageComponentList(String userId,PagerModel pagerModel, ComponentModel componentModel);

	// public abstract int countAllClusterList(ComponentModel componentModel);
	/** 获取组件 */
	public Component getComponent(Component component);

	/** 将Component对象转换为json对象 */
	public JSONObject getJsonObjectOfComponent(Component component);

	public abstract boolean queryIP(ComponentModel componentModel);

	/** 获取组件数量 */
	public Integer componentCount(Component record);
	
	public abstract GridBean getSvnList();
}
