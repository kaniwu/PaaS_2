package com.sgcc.devops.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.intf.ConPortMapper;
import com.sgcc.devops.service.ConportService;

/**  
 * date：2016年2月4日 下午2:58:24
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ConportService.java
 * description：  容器端口数据维护接口实现类
 */
@Component
public class ConportServiceImpl implements ConportService {

	@Autowired
	private ConPortMapper conPortMapper;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ConportService#listConPorts(java.lang.Integer)
	 */
	@Override
	public List<ConPort> listConPorts(String containerId) throws Exception {
		return conPortMapper.selectConport(containerId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ConportService#listConPortsByAppId(java.lang.
	 * Integer)
	 */
	@Override
	public List<ConPort> listConPortsByAppId(String appId) throws Exception {
		return conPortMapper.selectConportByAppId(appId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ConportService#addConports(com.sgcc.devops.entity
	 * .ConPort)
	 */
	@Override
	public int addConports(ConPort port) throws Exception {
		if (!StringUtils.hasText(port.getId())) {
			port.setId(KeyGenerator.uuid());
		}
		return conPortMapper.insertConport(port);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ConportService#updateConports(com.sgcc.devops.
	 * entity.ConPort)
	 */
	@Override
	public int updateConports(ConPort port) throws Exception {
		return conPortMapper.updateConport(port);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.ConportService#removeConports(java.lang.String[])
	 */
	@Override
	public int removeConports(String[] containerIds) throws Exception {
		return conPortMapper.deleteConport(containerIds);
	}

}
