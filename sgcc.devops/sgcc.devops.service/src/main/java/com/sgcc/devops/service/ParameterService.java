/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import net.sf.json.JSONArray;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.ParameterModel;
import com.sgcc.devops.dao.entity.Parameter;

/**  
 * date：2016年4月1日 上午9:24:09
 * project name：sgcc.devops.service
 * @author panjing
 * @version 1.0   
 * @since JDK 1.7.0_21  
 */
public interface ParameterService {
	
    int deleteByPrimaryKey(String id) throws Exception;

    int insert(Parameter record) throws Exception;

    Parameter selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(Parameter record) throws Exception;

    int updateByPrimaryKey(Parameter record) throws Exception;
    
    List<Parameter> selectAll(); 
    
    Parameter getParameter(Parameter parameter);

	GridBean getSearchParaList(PagerModel pagerModel,
			ParameterModel parameterModel);

	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年4月6日
	*/
	JSONArray selectByType(String type);

	Parameter selectParameterByCode(Parameter parameter);
	
	JSONArray selectParent(String type);
	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年6月20日
	*/
	JSONArray selectByLocation(String parentid);
}
