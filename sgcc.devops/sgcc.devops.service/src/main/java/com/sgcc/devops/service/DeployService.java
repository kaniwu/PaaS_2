package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.dao.entity.Deploy;

/**  
 * date：2016年2月4日 下午3:07:14
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployService.java
 * description：物理系统发布版本数据操作接口  
 */
public interface DeployService {
	/**
	* TODO
	* 新建版本
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年2月4日
	 */
	String createDeploy(Deploy deploy);
	/**
	* TODO
	* 删除版本
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年2月4日
	 */
	int deleteDeploy(String deployId);
	/**
	* TODO
	* 分页查询
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年2月4日
	 */
	GridBean getOnePageDeployList(String userId, int page, int rows, DeployModel deployModel);
	/**
	* TODO
	* 查询版本信息
	* @author mayh
	* @return Deploy
	* @version 1.0
	* 2016年2月4日
	 */
	public Deploy getDeploy(String deployId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Deploy>
	 * @version 1.0 2015年10月22日
	 */
	List<Deploy> getDeployByClusterId(String clusterId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年11月17日
	 */
	int updateDeploy(Deploy deploy);

	/**
	* TODO
	* @author mayh
	* @return Deploy
	* @version 1.0
	* 2015年12月29日
	*/
	List<Deploy> select(Deploy deploy);
	/**
	* TODO
	* @author mayh
	* @return List<Deploy>
	* @version 1.0
	* 2016年5月26日
	 * @param dialect 
	*/
	List<Deploy> getUndeployedByTimerTask(String dialect) throws Exception;
}
