/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.dao.entity.DeployNgnix;

/**  
 * date：2016年2月4日 下午3:04:08
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployNgnixService.java
 * description：  部署nginx数据维护接口类
 */
public interface DeployNgnixService {

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年10月20日
	 */
	public int create(DeployNgnix deployNgnix);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年10月20日
	 */
	public int deleteByDeployId(String deployId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<DeployNgnix>
	 * @version 1.0 2015年10月20日
	 */
	public List<DeployNgnix> selectByDeployId(String deployId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<DeployNgnix>
	 * @version 1.0 2015年12月5日
	 */
	public List<DeployNgnix> selectByNginxId(String valueOf);

	/**
	* TODO
	* @author mayh
	* @return DeployNgnix
	* @version 1.0
	* 2015年12月16日
	*/
	public DeployNgnix selectBydeployNgnix(DeployNgnix deployNgnix);

	/**
	* TODO
	* @author mayh
	* @return DeployNgnix
	* @version 1.0
	* 2016年1月21日
	*/
	public DeployNgnix selectByPrimaryKey(String deployNginxId);
}
