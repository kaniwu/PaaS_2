package com.sgcc.devops.service.Impl;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.intf.ComponentExpandMapper;
import com.sgcc.devops.service.ComponentExpandService;
/**  
 * date：2016年3月8日 上午10:40:43
 * project name：sgcc.devops.service
 * @author  yueyong
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentExpandServiceImpl.java
 * description：组件扩展数据维护接口实现类
 */
@Component
public class ComponentExpandServiceImpl implements ComponentExpandService {
	private static Logger logger = Logger.getLogger(ComponentExpandServiceImpl.class);
	
	@Resource
	private ComponentExpandMapper componentExpandMapper;


	@Override
	public int upDateComponentExpandSer(ComponentExpand componentExpand) {
		int result = 1;
		try {
			componentExpandMapper.updateComponentExpand(componentExpand);
		} catch (Exception e) {
			logger.info("update F5component fail:" + e);
			result = 0;
		}
		return result;
	}

	@Override
	public ComponentExpand selectByCompId(String CompId) {
		try {
			return	componentExpandMapper.selectByCompId(CompId);
		} catch (Exception e) {
			logger.info("select  By  CompId fail:" + e);
 		    return null;
		}
	}
}
