/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.ServerRoom;

/**  
 * date：2016年3月16日 上午9:21:45
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ServerRoomService.java
 * description：  
 */
public interface ServerRoomService {
	int deleteByPrimaryKey(String id) throws Exception;

    int insert(ServerRoom record) throws Exception;

    int insertSelective(ServerRoom record) throws Exception;

    ServerRoom selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(ServerRoom record) throws Exception;

    int updateByPrimaryKey(ServerRoom record) throws Exception;
    
    List<ServerRoom> selectAll();

	/**
	* TODO
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年3月16日
	*/
	GridBean serverRoomList(PagerModel pagerModel);

	ServerRoom getServerRoom(ServerRoom serverRoom);

	boolean ifRepeat(String serverRoomName);

	
}
