/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.LocationModel;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.dao.intf.LocationMapper;
import com.sgcc.devops.service.LocationService;

/**  
 * date：2016年4月6日 下午5:01:36
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LocationServiceImpl.java
 * description：  
 */
@Component
public class LocationServiceImpl implements LocationService {
	@Resource
	private LocationMapper locationMapper;

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#getLocationByParent(java.lang.String)
	 */
	@Override
	public List<Location> getLocationByParent(String parentId) {
		return locationMapper.selectByParent(parentId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#create(com.sgcc.devops.common.model.LocationModel)
	 */
	@Override
	public Result create(LocationModel locationModel) {
		Location record = new Location();
		BeanUtils.copyProperties(locationModel, record);
		try {
			record.setId(KeyGenerator.uuid());
			locationMapper.insertSelective(record);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "添加位置【"+locationModel.getLocationName()+"】失败："+e);
		}
		return new Result(true, "添加位置【"+locationModel.getLocationName()+"】成功！");
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#update(com.sgcc.devops.common.model.LocationModel)
	 */
	@Override
	public Result update(Location location) {
		try {
			locationMapper.updateByPrimaryKeySelective(location);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "修改位置【"+location.getLocationName()+"】失败："+e);
		}
		return new Result(true, "修改位置【"+location.getLocationName()+"】成功！");
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#delete(com.sgcc.devops.common.model.LocationModel)
	 */
	@Override
	public Result delete(LocationModel locationModel) {
		String name = locationModel.getLocationName();
		try {
			locationMapper.deleteByPrimaryKey(locationModel.getId());
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "删除位置【"+name+"】失败："+e);
		}
		return new Result(true, "删除位置【"+name+"】成功！");
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#select(com.sgcc.devops.common.model.LocationModel)
	 */
	@Override
	public Location select(Location location) {
		try {
			location = locationMapper.selectByPrimaryKey(location.getId());
			return location;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.LocationService#selectByParamCode(java.lang.String)
	 */
	@Override
	public List<Location> selectByParamCode(String locationParamCode) {
		return locationMapper.selectByParamCode(locationParamCode);
	}
	
}
