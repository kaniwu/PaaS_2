package com.sgcc.devops.service.Impl;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.HostInstall;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.ClusterMapper;
import com.sgcc.devops.dao.intf.HostIntallMapper;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.dao.intf.LocationMapper;
import com.sgcc.devops.service.HostService;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午3:18:03
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：HostService.java
 * description：主机数据维护接口实现类
 */
@Component
public class HostServiceImpl implements HostService {

	private static Logger logger = Logger.getLogger(HostServiceImpl.class);
	@Resource
	private HostMapper hostMapper;
	@Resource
	private ClusterMapper clusterMapper;
	@Resource
	private LocationMapper locationMapper;
	@Resource
	private HostIntallMapper hostIntallMapper;
	@Resource
	private JdbcConfig jdbcConfig;
	@Override
	public String createHost(JSONObject jo) {
		String result = "";
		Host record = new Host();
		record.setHostName(jo.getString("hostName"));
		record.setHostUuid(jo.getString("hostUuid"));
		record.setHostCpu(jo.getInt("hostCpu"));
		record.setHostMem(jo.getInt("hostMem"));
		record.setHostKernelVersion(jo.getString("hostKernelVersion"));
		record.setHostIp(jo.getString("hostIp"));
		if (jo.getString("hostDesc") != null) {
			record.setHostDesc(jo.getString("hostDesc"));
		}
		record.setHostCreator(jo.getString("userId"));
		record.setHostStatus((byte) Status.HOST.NORMAL.ordinal());
		record.setHostUser(jo.getString("hostUser"));
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		record.setHostCreatetime(new Date());
		record.setHostType((byte) Type.HOST.UNUSED.ordinal());

		record.setHostPort(jo.getString("hostPort"));
		record.setHostPwd(jo.getString("password"));
		record.setDockerStatus((byte)jo.getInt("dockerStatus"));
		record.setLocationId(jo.getString("locationId"));
		record.setHostEnv(jo.getString("hostEnv"));
		try {
			record.setHostId(KeyGenerator.uuid());
			hostMapper.insertHost(record);
		} catch (Exception e) {
			logger.error("create host fail: " + e.getMessage());
			result = "create host fail: " + e.getMessage();
			return result;
		}
		return record.getHostId();
	}

	@Override
	public int deleteHost(JSONObject jo) {
		int result = 1;
		try {
			hostMapper.deleteHost(jo.getString("hostId"));
		} catch (Exception e) {
			logger.error("delete host fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	@Override
	public int deleteHosts(JSONObject jo) {
		int result = 1;
		String arrayString = jo.getString("array");
		String[] jaArray = arrayString.split(",");
		List<String> list = new ArrayList<String>();
		for (String appidString : jaArray) {
			if (appidString == null || "".equals(appidString)) {

			} else {
				list.add(appidString);
			}
		}
		try {
			result = hostMapper.deleteHosts(list);
		} catch (Exception e) {
			logger.error("batch delete host fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	@Override
	public int update(Host host) {
		try {
			return hostMapper.updateHost(host);
		} catch (Exception e) {
			logger.error("update host fail: " + e.getMessage());
			return 0;
		}
	}

	@Override
	public GridBean getOnePageHostList(User user,PagerModel pagerModel, HostModel hostModel) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		
		Host host = new Host();

		if (hostModel.getClusterId() != null) {
			host.setClusterId(hostModel.getClusterId());
		}
		if (hostModel.getHostIp() != null) {
			host.setHostIp(hostModel.getHostIp());
		}
		if (hostModel.getHostType() != null) {
			host.setHostType(hostModel.getHostType());
		}
		//所属位置
		if(!StringUtils.isEmpty(hostModel.getLocationId())){
			host.setLocationIdList(locationIdRecursion(hostModel.getLocationId()));
		}
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("hostName")){
				PageHelper.startPage(page, pageSize,"HOST_NAME "+sord);
			}
			if(sidx.equals("hostIp")){
				PageHelper.startPage(page, pageSize,"HOST_IP "+sord);
			}
			if(sidx.equals("clusterName")){
				PageHelper.startPage(page, pageSize,"CLUSTER_ID "+sord);
			}
			if(sidx.equals("hostTypeValue")){
				PageHelper.startPage(page, pageSize,"HOST_TYPE "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		host.setHostEnv(user.getUserCompany());
		host.setDialect(jdbcConfig.getDialect());
		List<Host> hosts = hostMapper.selectAllHost(host);
		List<HostModel> hostModels = new ArrayList<>();
		for (Host host1 : hosts) {
			HostModel model = new HostModel();
			if (host1.getClusterId() != null) {
				String clusterId = host1.getClusterId();
				Cluster cluster = clusterMapper.selectCluster(clusterId);
				if (cluster != null) {
					model.setClusterName(cluster.getClusterName());
				} else {
					logger.warn("host locate cluster not exist");
				}
			}
			model.setHostCpu(host1.getHostCpu());
			model.setHostEnv(host1.getHostEnv());
			model.setHostDesc(host1.getHostDesc());
			model.setHostId(host1.getHostId());
			model.setHostKernelVersion(host1.getHostKernelVersion());
			model.setHostName(host1.getHostName());
			model.setHostMem(host1.getHostMem());
			model.setHostPwd(host1.getHostPwd());
			model.setHostStatus(host1.getHostStatus());
			model.setHostType(host1.getHostType());
			model.setHostUuid(host1.getHostUuid());
			Cluster cluster = clusterMapper.selectCluster(host1.getClusterId());
			if(null!=cluster){
				model.setClusterId(host1.getClusterId());
			}
			model.setHostUser(host1.getHostUser());
			model.setHostIp(host1.getHostIp());
			model.setHostPort(host1.getHostPort());
			model.setHostCreatetime(host1.getHostCreatetime());
			model.setDockerStatus(host1.getDockerStatus());
			hostModels.add(model);
		}

		int totalpage = ((Page<?>) hosts).getPages();
		Long totalNum = ((Page<?>) hosts).getTotal();
		// GridBean gridBean = new GridBean(page, totalpage,
		// totalNum.intValue(), hosts);
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), hostModels);
		return gridBean;
	}

	@Override
	public int countAllHostList(Host host) {
		int result = 0;
		try {
			result = hostMapper.selectHostCount(host);
		} catch (Exception e) {
			logger.error("select host list count fail: " + e.getMessage());
			return 0;
		}
		return result;
	}

	@Override
	public Host getHost(Host host) {
		try {
			host = hostMapper.selectHost(host);
		} catch (Exception e) {
			logger.error("select host faile: " + e.getMessage());
			return null;
		}
		return host;
	}

	@Override
	public Host getHostByIp(String hostIp) {
		return hostMapper.selectHostByIp(hostIp);
	}

	@Override
	public JSONObject getJsonObjectOfHost(Host host) {
		return JSONUtil.parseObjectToJsonObject(getHost(host));
	}

	@Override
	public boolean queryIP(HostModel hostModel) {
		System.out.println("check ip");
		return true;
	}

//	@Override
//	public List<Host> listHostByType(int type) {
//		return hostMapper.selectHostList(type);
//	}

	@Override
	public List<Host> listHostByClusterId(String clusterId) {
		return hostMapper.selectHostListByClusterId(clusterId);
	}

	@Override
	public List<Host> listHostByTypeAndCluster(int type) {
		return hostMapper.selectHostListByCluster(type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.HostService#freeHostList()
	 */
	@Override
	public JSONArray freeHostList(User user) {
		JSONArray ja = new JSONArray();
		Host hostIn = new Host();
		hostIn.setHostEnv(user.getUserCompany());
		List<Host> hostList = hostMapper.freeHostList(hostIn);
		if (hostList != null) {
			for (Host host : hostList) {
				JSONObject jo = new JSONObject();
				jo.put("hostip", host.getHostIp());
				jo.put("hostuuid", host.getHostUuid());
				jo.put("hostid", host.getHostId());
				jo.put("hostname", host.getHostName());
				jo.put("hoststatus", host.getHostStatus());
				ja.add(jo);
			}
		}
		return ja;
	}

	@Override
	public List<Host> selectHostListByHost(Host host) {

		return hostMapper.selectHostListByHost(host);
	}

	@Override
	public List<Host> selectAllHost(Host host ) {
		host.setDialect(jdbcConfig.getDialect());
		return hostMapper.selectAllHost(host);
	}

	@Override
	public Host getHost(String hostId) {
		return hostMapper.loadHost(hostId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostService#allOnlineHost()
	 */
	@Override
	public List<Host> allOnlineHost() {
		
		return hostMapper.allOnlineHost();
	}

	public List<ComponentHost> selectComponentHost(String componentId) {
		
		return hostMapper.selectComponentHost(componentId);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostService#hostListByRack(java.lang.String)
	 */
	@Override
	public List<Host> hostListByRack(Host host) {
		return hostMapper.hostListByRack(host);
	}
	@Override
	public byte[] exportHost(String locationId){ 
		try{
			// 创建新的Excel 工作簿
			HSSFWorkbook workbook = new HSSFWorkbook();
			// 在Excel工作簿中建一工作表，其名为缺省值
			 HSSFSheet sheet = workbook.createSheet("主机信息");
			 CellStyle cellStyle4Header = workbook.createCellStyle();
			HSSFRow row = sheet.createRow((short)0);
			HSSFCell cell = row.createCell((short) 0);
			// 定义单元格为字符串类型
			//cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			// 在单元格中输入一些内容
			cell.setCellValue("主机名称");
			HSSFCell cell0 = row.createCell((short)1);
        	 cell0.setCellStyle(cellStyle4Header);
        	 cell0.setCellValue("IP地址");
        	 HSSFCell cell1 = row.createCell((short)2);
        	 cell1.setCellStyle(cellStyle4Header);
        	 cell1.setCellValue("CPU（核数）");
        	 HSSFCell cell2 = row.createCell((short)3);
        	 cell2.setCellStyle(cellStyle4Header);
        	 cell2.setCellValue("内存（MB）");
        	 HSSFCell cell3 = row.createCell((short)4);
        	 cell3.setCellStyle(cellStyle4Header);
        	 cell3.setCellValue("所在集群");
        	 HSSFCell cell4 = row.createCell((short)5);
        	 cell4.setCellStyle(cellStyle4Header);
        	 cell4.setCellValue("主机类型");
        	 HSSFCell cell5 = row.createCell((short)6);
        	 cell5.setCellStyle(cellStyle4Header);
        	 cell5.setCellValue("主机状态");
        	 HSSFCell cell6 = row.createCell((short)7);
        	 cell6.setCellStyle(cellStyle4Header);
        	 cell6.setCellValue("DOCKER状态");
        	 Host host = new Host();
        	//所属位置
     		if(!StringUtils.isEmpty(locationId)){
     			host.setLocationIdList(locationIdRecursion(locationId));
     		}
     		host.setDialect(jdbcConfig.getDialect());
        	 List<Host> hosts = hostMapper.selectAllHost(host);
        	 Cluster record =new Cluster();
        	 List<Cluster> Clusters = clusterMapper.selectAllCluster(record);
        	 Map<String,String> clusterMap =new HashMap<>();
        	 for (Cluster cluster : Clusters) {
        		 clusterMap.put(cluster.getClusterId(), cluster.getClusterName());
			}
        	 for (int i = 0; i < hosts.size(); i++) {
        		 HSSFRow rowdata = sheet.createRow((short) (i + 1));
                 rowdata.createCell((short) 0).setCellValue(hosts.get(i).getHostName());//主机名称
                 rowdata.createCell((short) 1).setCellValue(hosts.get(i).getHostIp());//IP地址
                 rowdata.createCell((short) 2).setCellValue(hosts.get(i).getHostCpu());//CPU（核数）
                 rowdata.createCell((short) 3).setCellValue(hosts.get(i).getHostMem());//内存（MB）
                 String cluString =hosts.get(i).getClusterId();
                 rowdata.createCell((short) 4).setCellValue(clusterMap.get(cluString));//所在集群
                 int  type =hosts.get(i).getHostType();
                 String typeValue="";
                 switch (type) {
				case 0:typeValue="swarm管理服务器";
					break;
				case 1:typeValue="docker服务器";
					break;
				case 2:typeValue="仓库主机";
					break;
				case 3:typeValue="负载均衡服务器";
					break;
				default:typeValue="暂未分配";
					break;
				}
                 rowdata.createCell((short) 5).setCellValue(typeValue);//主机类型
                 int status=hosts.get(i).getHostStatus();
                 String statusValue;
             	switch(status){
				case 0:	statusValue= "已删除";
				break;
				case 1: statusValue= "正常"; 
				break;
				case 2: statusValue= "监控异常";
				break;
				case 3: statusValue= "离线";
				break;
				default:statusValue= "未知";
				}
                 rowdata.createCell((short) 6).setCellValue(statusValue);//主机状态
                 int dockerStatus=hosts.get(i).getDockerStatus();
                 String dockerStatusValue="";
                 switch(dockerStatus){
					case 0:	dockerStatusValue= "未安装";
					break;
					case 1: dockerStatusValue= "正常    ";
					break;
					case 2: dockerStatusValue= "停止    ";
					break;
					default:dockerStatusValue= "未知    ";
				}	
                 rowdata.createCell((short) 7).setCellValue(dockerStatusValue);//DOCKER状态
				
			}
             //设置宽度
	           sheet.setColumnWidth(0, 21*256);
	           sheet.setColumnWidth(1, 16*256);
	           sheet.setColumnWidth(2, 10*256);
	           sheet.setColumnWidth(3, 10*256);
	           sheet.setColumnWidth(4, 10*256);
	           sheet.setColumnWidth(5, 18*256);
	           sheet.setColumnWidth(6, 10*256);
	           sheet.setColumnWidth(7, 14*256);
                
			// 新建一输出文件流
	           ByteArrayOutputStream out = null;
	           out = new ByteArrayOutputStream();
	           workbook.write(out);
//			FileOutputStream fOut = new FileOutputStream(outputFile);
//			// 把相应的Excel 工作簿存盘
//			workbook.write(fOut);
//			fOut.flush();
//			// 操作结束，关闭文件
//			fOut.close();
			return out.toByteArray();
			}catch(Exception e) {
			return null ;
			}
			}
	@Override
	public byte[] importHostExcel(){ 
		try{
			// 创建新的Excel 工作簿
			HSSFWorkbook workbook = new HSSFWorkbook();
			// 在Excel工作簿中建一工作表，其名为缺省值
			 HSSFSheet sheet = workbook.createSheet("主机信息");
			 CellStyle cellStyle4Header = workbook.createCellStyle();
			HSSFRow row = sheet.createRow((short)0);
			HSSFCell cell0 = row.createCell((short) 0);
			// 定义单元格为字符串类型
			//cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			// 在单元格中输入一些内容
			cell0.setCellValue("IP");
			HSSFCell cell1 = row.createCell((short)1);
        	 cell1.setCellStyle(cellStyle4Header);
        	 cell1.setCellValue("名称");
        	 HSSFCell cell2 = row.createCell((short)2);
        	 cell2.setCellStyle(cellStyle4Header);
        	 cell2.setCellValue("用户名（默认root)");
        	 HSSFCell cell3 = row.createCell((short)3);
        	 cell3.setCellStyle(cellStyle4Header);
        	 cell3.setCellValue("密码");
             //设置宽度
	           sheet.setColumnWidth(0, 15*256);
	           sheet.setColumnWidth(1, 15*256);
	           sheet.setColumnWidth(2, 20*256);
	           sheet.setColumnWidth(3, 15*256);
                
			// 新建一输出文件流
	           ByteArrayOutputStream out = null;
	           out = new ByteArrayOutputStream();
	           workbook.write(out);
			return out.toByteArray();
			}catch(Exception e) {
			return null ;
			}
	}
	//递归查询所选位置层级的所有子位置层级ID
	public List<String> locationIdRecursion(String locationId){
		List<String> locationIds = new ArrayList<>();
		locationIds.add(locationId);
		List<Location> list = locationMapper.selectByParent(locationId);
		for(Location location2:list){
//			locationIds.add(location2.getId());
			List<String> list2 = locationIdRecursion(location2.getId());
			if(list2!=null&&list2.size()>0){
				locationIds.addAll(list2);
			}
		}
		return locationIds;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostService#insert(com.sgcc.devops.dao.entity.HostInstall)
	 */
	@Override
	public int insert(HostInstall hostInstall) {
		hostInstall.setId(KeyGenerator.uuid());
		return hostIntallMapper.insert(hostInstall);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostService#selectAll(com.sgcc.devops.dao.entity.HostInstall)
	 */
	@Override
	public List<HostInstall> selectAll(HostInstall hostInstall) {
		return hostIntallMapper.selectAll(hostInstall);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.HostService#delete(java.lang.String)
	 */
	@Override
	public int delete(String id) {
		return hostIntallMapper.delete(id);
	}

	@Override
	public JSONArray grateClusterHost(User user,String clusterId) {
		JSONArray ja = new JSONArray();
		Cluster cluster = clusterMapper.selectCluster(clusterId);
		if(null!=cluster&&null!=cluster.getStandByHostId()){
			Host host = hostMapper.loadHost(cluster.getStandByHostId());
			if(null!=host){
				JSONObject jo = new JSONObject();
				jo.put("hostip", host.getHostIp());
				jo.put("hostuuid", host.getHostUuid());
				jo.put("hostid", host.getHostId());
				jo.put("hostname", host.getHostName());
				jo.put("hoststatus", host.getHostStatus());
				ja.add(jo);
			}
		}
		Host hostIn = new Host();
		hostIn.setHostEnv(user.getUserCompany());
		List<Host> hostList = hostMapper.freeHostList(hostIn);
		if (hostList != null) {
			for (Host host : hostList) {
				JSONObject jo = new JSONObject();
				jo.put("hostip", host.getHostIp());
				jo.put("hostuuid", host.getHostUuid());
				jo.put("hostid", host.getHostId());
				jo.put("hostname", host.getHostName());
				jo.put("hoststatus", host.getHostStatus());
				ja.add(jo);
			}
		}
		return ja;
	}
	
}
