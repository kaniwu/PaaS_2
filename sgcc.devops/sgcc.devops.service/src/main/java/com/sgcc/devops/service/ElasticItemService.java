package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.ElasticItem;

/**  
 * date：2016年2月4日 下午3:17:06
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticItemService.java
 * description：  弹性伸缩策略条目数据维护接口类
 */
public interface ElasticItemService {
	/** 插入弹性伸缩策略条目 */
	public Result insert(ElasticItem record);

	/** 删除弹性伸缩策略条目 */
	public Result delete(String elasticItemId);

	/** 更新弹性伸缩策略条目 */
	public Result update(ElasticItem record);

	/** 获取弹性伸缩策略条目 By id(主键) */
	public ElasticItem load(String id);

	/** 获取弹性伸缩策略条目 */
	public List<ElasticItem> loadByStrategy(String strategyId);
}
