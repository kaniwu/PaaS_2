/**
 * 
 */
package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.RegistryLb;

public interface RegistryLbService {
	public int createRegistryLb(RegistryLb registryLb);
	
	public RegistryLb registryLbDetails(String id);

	public int  deleteRegistryLb(String id);
	
	public int updateRegistryLb(RegistryLb registryLb);

	public List<RegistryLb> getAllRegistryLb();
	/**
	* TODO
	* @author mayh
	* @return List<RegistryLb>
	* @version 1.0
	* 2016年5月3日
	*/
	public List<RegistryLb> getRegistryLb(RegistryLb lb);

	/**
	* TODO
	* @author mayh
	* @return RegistryLb
	* @version 1.0
	* 2016年5月11日
	*/
	public RegistryLb selectRegistryLb(RegistryLb registryLb);
	
	public Result registryLbDelete(String id);
}