package com.sgcc.devops.service;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Log;

/**  
 * date：2016年2月4日 下午3:21:13
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogService.java
 * description：操作日志数据维护接口类
 */
public interface LogService {
	/**
	 * 日志分页
	 * 
	 * @param pageNum
	 *            页码
	 * @param pageSize
	 *            页面大小
	 * @return 分页信息
	 */
	public GridBean list(Log log,PagerModel model);

	/**
	 * 保存日志
	 * 
	 * @param log
	 *            日志实体
	 * @return
	 */
	public Result save(Log log);
}
