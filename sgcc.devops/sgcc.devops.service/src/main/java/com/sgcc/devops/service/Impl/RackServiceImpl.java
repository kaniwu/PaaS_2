/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Rack;
import com.sgcc.devops.dao.intf.RackMapper;
import com.sgcc.devops.service.RackService;

/**  
 * date：2016年3月16日 上午9:24:26
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：RackServiceImpl.java
 * description：  
 */
@Component
public class RackServiceImpl implements RackService {
	private static Logger logger = Logger.getLogger(RackServiceImpl.class);
	@Resource
	private RackMapper rackMapper;
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#deleteByPrimaryKey(java.lang.String)
	 */
	@Override
	public int deleteByPrimaryKey(String id) throws Exception {
		int result = 1;
		try {
			rackMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error("delete rack fail: " + e.getMessage());
			result = 0;
		}
		return result;
		
	}

	/*
	 * 查询有无重复的机架名称
	 * panjing
	 */
	public boolean ifRackNameRepeat(Rack rack){
		boolean repeat = false;
		try{
			int num = rackMapper.countRackName(rack);
			if(num > 0){
				repeat = true;
			}
		}catch(Exception e){
			repeat = true;
			logger.error("count rackName fail: " + e.getMessage());
		}
		return repeat;
	}
	
	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#insert(com.sgcc.devops.dao.entity.Rack)
	 */
	@Override
	public int insert(Rack record) throws Exception {
		int result = 1;
		try{
			rackMapper.insert(record);
		}catch(Exception e){
			result = 0;
			logger.error("insert rack fail:" + e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#selectByPrimaryKey(java.lang.String)
	 */
	@Override
	public Rack selectByPrimaryKey(String id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#updateByPrimaryKeySelective(com.sgcc.devops.dao.entity.Rack)
	 */
	@Override
	public int updateByPrimaryKeySelective(Rack record) throws Exception {
		int result = 1;
		try{
			rackMapper.updateByPrimaryKeySelective(record);
		}catch(Exception e){
			result = 0;
			logger.error("update rack fail:" +e.getMessage());
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#updateByPrimaryKey(com.sgcc.devops.dao.entity.Rack)
	 */
	@Override
	public int updateByPrimaryKey(Rack record) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.RackService#rackListByServerRoom(java.lang.String)
	 */
	@Override
	public List<Rack> rackListByServerRoom(String serverRoomId) {
		
		return rackMapper.rackListByServerRoom(serverRoomId);
	}
	/*
	 * 获取机架信息
	 * panjing
	 */
	@Override
	public Rack getRack(Rack rack) {
		try {
			rack = rackMapper.selectByPrimaryKey(rack.getId());
		} catch (Exception e) {
			logger.error("select cluster fail: " + e.getMessage());
			return null;
		}
		return rack;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.sgcc.devops.service.ServerRoomService#rackList(com.sgcc.devops.common.model.PagerModel, java.lang.String)
	 */
	@Override
	public GridBean rackList(PagerModel pagerModel, String serverRoomId) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("rackRemark")){
				PageHelper.startPage(page, pageSize,"RACK_REMARK "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		List<Rack> racks = rackMapper.rackListByServerRoom(serverRoomId);
		int totalpage = ((Page<?>) racks).getPages();
		Long totalNum = ((Page<?>) racks).getTotal();
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), racks);
		return gridBean;
	}

}
