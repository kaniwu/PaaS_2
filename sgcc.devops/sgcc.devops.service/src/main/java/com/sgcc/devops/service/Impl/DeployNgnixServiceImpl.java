/**
 * 
 */
package com.sgcc.devops.service.Impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.DeployNgnix;
import com.sgcc.devops.dao.intf.DeployNgnixMapper;
import com.sgcc.devops.service.DeployNgnixService;

/**  
 * date：2016年2月4日 下午3:04:08
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployNgnixService.java
 * description：  部署nginx数据维护接口实现类
 */
@Component
public class DeployNgnixServiceImpl implements DeployNgnixService {

	private static Logger logger = Logger.getLogger(DeployNgnixServiceImpl.class);
	@Resource
	private DeployNgnixMapper deployNgnixMapper;

	public int create(DeployNgnix deployNgnix) {
		if (!StringUtils.hasText(deployNgnix.getId())) {
			deployNgnix.setId(KeyGenerator.uuid());
		}
		int result = 0;
		try {
			result = deployNgnixMapper.insert(deployNgnix);
		} catch (Exception e) {
			logger.error("create deployNginx fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	public int delete(String deployNgnixId) {
		int result = 0;
		try {
			result = deployNgnixMapper.deleteByPrimaryKey(deployNgnixId);
		} catch (Exception e) {
			logger.error("delete deployNginx fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sgcc.devops.service.DeployNgnixService#deleteByDeployId(int)
	 */
	@Override
	public int deleteByDeployId(String deployId) {
		int result = 0;
		try {
			result = deployNgnixMapper.deleteByDeployId(deployId);
		} catch (Exception e) {
			logger.error("delete deployNginx fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployNgnixService#selectByDeployId(java.lang.
	 * Integer)
	 */
	@Override
	public List<DeployNgnix> selectByDeployId(String deployId) {

		return deployNgnixMapper.selectByDeployId(deployId);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sgcc.devops.service.DeployNgnixService#selectByNginxId(java.lang.
	 * Integer)
	 */
	@Override
	public List<DeployNgnix> selectByNginxId(String nginxid) {

		return deployNgnixMapper.selectByNginxId(nginxid);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployNgnixService#selectBydeployNgnix(com.sgcc.devops.dao.entity.DeployNgnix)
	 */
	@Override
	public DeployNgnix selectBydeployNgnix(DeployNgnix deployNgnix) {
		return deployNgnixMapper.selectBydeployNgnix(deployNgnix);
	}

	/* (non-Javadoc)
	 * @see com.sgcc.devops.service.DeployNgnixService#selectByPrimaryKey(java.lang.String)
	 */
	@Override
	public DeployNgnix selectByPrimaryKey(String id) {
		return deployNgnixMapper.selectByPrimaryKey(id);
	}
}
