package com.sgcc.devops.service;

import java.util.List;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.ImageModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RegHost;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.User;

import net.sf.json.JSONObject;

/**
 * 镜像服务接口，主要处理与DB相关的服务
 * 
 * @author dmw
 *
 */
public interface ImageService {
	/**
	 * 添加镜像
	 * 
	 * @param image
	 * @return
	 */
	public abstract String create(Image image);

	/**
	 * 删除镜像
	 * 
	 * @param imageId
	 * @return
	 */
	public abstract boolean delete(String imageId);

	/**
	 * 更新镜像
	 * 
	 * @param image
	 * @return
	 */
	public abstract boolean update(Image image);

	/**
	 * 镜像分页
	 * 
	 * @param userId
	 * @param page
	 * @param rows
	 * @param imageModel
	 * @return
	 */
	public abstract GridBean pagination(User user,PagerModel pagerModel, ImageModel imageModel);

	/**
	 * @author luogan
	 * @param host
	 * @return select imageList
	 * @version 1.0 2015年8月28日
	 */
	public abstract GridBean getImageListByappId(String userId, int page, int rows, String appId);

	/**
	 * @author luogan
	 * @param host
	 * @return select imageList
	 * @version 1.0 2015年8月28日
	 */
	public abstract int countAllImageList(ImageModel imageModel);

	public Image load(String id);

	/**
	 * @author luogan
	 * @param host
	 * @return select image
	 * @version 1.0 2015年8月28日
	 */
	public JSONObject getJsonObjectOfImage(Image image);

	/**
	 * @author yangqinglin
	 * @param null
	 * @return select all active image list
	 * @version 1.0 2015年9月24日
	 */
	public List<Image> selectActiveAllImages(Image image);

	/**
	 * 通过镜像ID获取镜像仓库所在主机的Ip Port。
	 * 
	 * @param imageId
	 * @return
	 */
	public RegHost loadByImageId(String imageId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月13日
	 */
	public abstract GridBean getImageListByImageType(String userId, int page, int rows,int imageType,String clusterId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return Image
	 * @version 1.0 2015年10月23日
	 */
	public abstract Image selectByPrimaryKey(String imageId);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return Image
	 * @version 1.0 2015年11月20日
	 */
	public int selectImageCount(Image image);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return Image
	 * @version 1.0 2015年11月27日
	 */
	public Image selectImage(Image image);

}
