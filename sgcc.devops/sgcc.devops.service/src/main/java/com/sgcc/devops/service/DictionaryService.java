/**
 * 
 */
package com.sgcc.devops.service;

import net.sf.json.JSONArray;

import com.sgcc.devops.dao.entity.User;

/**  
 * date：2016年2月4日 下午3:11:28
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DictionaryService.java
 * description：  数据字典数据维护接口类
 */
public interface DictionaryService {
	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年11月20日
	 */
	JSONArray listByDatabaseId(String databaseId, User user);

}
