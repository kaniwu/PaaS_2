package com.sgcc.devops.service;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.SystemAppModel;
import com.sgcc.devops.common.model.SystemModel;
import com.sgcc.devops.dao.entity.AppPrelib;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.entity.User;

/**  
 * date：2016年2月4日 下午3:38:53
 * project name：sgcc.devops.service
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：SystemService.java
 * description：物理系统业务数据维护接口类  
 */
public interface SystemService {
	/**
	* TODO
	* 分页查询接口
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年2月4日
	 */
	public GridBean listOnePageSystems(PagerModel pagerModel,User user,String businessId,String systemName,Boolean isAllAuth,String envId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年9月21日
	 */
	public int create(SystemModel systemModel, User user);

	/**
	 * TODO
	 * 
	 * @author yangqinglin
	 * @return GridBean
	 * @version 1.0 2015年9月23日
	 */
	public System loadById(String systemId);

	/**
	 * TODO
	 * 
	 * @author yangqinglin
	 * @return GridBean
	 * @version 1.0 2015年9月23日
	 */
	public int updateSystem(System system);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月15日
	 */
	public GridBean getOnePageSystemDeployInfo(String userId,PagerModel pagerModel, DeployModel deployModel,
			String systemId);

	/**
	 * TODO
	 * 
	 * @author zhangxin
	 * @return List<SystemModel>
	 * @version 1.0 2015年10月26日
	 */
	public List<SystemModel> getListByBusinessId(String businessId, User user,String envId);

	/**
	 * 获取平台下所有已部署的物理系统列表
	 * 
	 * @return
	 */
	public List<System> listDeployedSystem();

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return System
	 * @version 1.0 2015年11月18日
	 */
	public System getSystemById(String systemId);

	/**
	 * 获取当前物理系统下所有的容器列表
	 * 
	 * @param systemId
	 * @return
	 */
	public List<Container> listContainers(String systemId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年11月25日
	 */
	public JSONObject getBaseDeploy(String systemId, User user);

	public System selectSystem(System system);

	/**
	 * select count system
	 */
	public int selectSystemCount(System system);

	/**
	* TODO
	* @author mayh
	* @return List<String>
	* @version 1.0
	* 2016年2月23日
	*/
	public List<System> getSystemNames(String hostId);

	/**
	* TODO
	* @author mayh
	* @return List<System>
	* @version 1.0
	* 2016年3月8日
	*/
	public List<System> listUnDeployedSystem();

	/**
	 * 根据systemID获得 应用包信息
	 * 
	 * @author liyp
	 * @return GridBean
//	 * @version 1.0 2016年3月9日
	 */
	public GridBean getAppPackage(PagerModel pagerModel,String systemId);
	
	/**
	 * 新增物理系统应用包
	 * 
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月10日
	 */
	public int createSystemAppPackage(SystemApp app);

	public SystemApp getAppPackageById(String systemAppId);

	
	/**
	 * 查询所有物理系统管理员
	 * 
	 * @author wangjx
	 * @return JSONObject
	 * @version 1.0 2016年3月10日
	 */
	public List<User> selectAllAdminService();
	
	/**
	 * 查询当前物理系统管理员
	 * @author wangjx
	 * @param systemId
	 * @return
	 */
	public List<User> selectAdminsBySystemId(String systemId);
	
	
	/**
	 * 新增当前物理系统管理员
	 * @author wangjx
	 * @return int
	 * 
	 */
	public int addSysAdminService(List<Map<String,Object>> list);
	
	/**
	 * 删除当前物理系统管理员
	 * @param list
	 * @return
	 */
	public int delSysAdminsService(List<Map<String,Object>> list);

	/**
	 * 删除物理系统应用包
	 * 
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月16日
	 */
	public int deleteSystemAppPackage(SystemAppModel systemAppModel);
	
	/**
	 * 检查物理系统应用包是否被使用
	 * 
	 * @author liyp
	 * @return boolean
	 * @version 1.0 2016年3月16日
	 */
	public boolean checkAppUsed(SystemAppModel systemAppModel);

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年4月22日
	*/
	public void createAppPrelib(AppPrelib appPrelib);
	public List<System> selectAllSystem(System record);

	/**
	* TODO
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年8月3日
	*/
	public boolean delete(String systemId) throws Exception;

	/**
	* TODO
	* @author mayh
	* @return List<Container>
	* @version 1.0
	* 2016年8月8日
	*/
	public List<Container> listWaitContainers(String system);
}
