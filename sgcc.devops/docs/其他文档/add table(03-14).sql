/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50146
Source Host           : localhost:3306
Source Database       : paas_db

Target Server Type    : MYSQL
Target Server Version : 50146
File Encoding         : 65001

Date: 2016-03-14 13:05:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for dop_action
-- ----------------------------
DROP TABLE IF EXISTS `dop_action`;
CREATE TABLE `dop_action` (
  `ACTION_ID` varchar(64) NOT NULL COMMENT '行为ID',
  `ACTION_NAME` varchar(36) NOT NULL COMMENT '行为名称',
  `ACTION_DESC` varchar(300) NOT NULL COMMENT '行为描述',
  `ACTION_RELATIVE_URL` varchar(100) DEFAULT NULL COMMENT '行为连接',
  `ACTION_TYPE` tinyint(11) NOT NULL COMMENT '行为级别（0 页面 ,1操作, 2子页面, 3弹出层）',
  `ACTION_REMARKS` varchar(300) DEFAULT NULL COMMENT '行为标记',
  `ACTION_PARENT_ID` varchar(64) DEFAULT NULL COMMENT '行为父类ID',
  PRIMARY KEY (`ACTION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dop_action
-- ----------------------------
INSERT INTO `dop_action` VALUES ('1', '平台首页', '平台首页', '/index.html', '0', 'index', '0');
INSERT INTO `dop_action` VALUES ('2', '资源管理', '资源管理', null, '0', 'manage_resource', '0');
INSERT INTO `dop_action` VALUES ('21', '主机管理', '主机管理', '/host/index.html', '0', 'hostIndex', '2');
INSERT INTO `dop_action` VALUES ('211', '添加主机', '主机管理描述', '/host/create', '1', 'hostCreate', '21');
INSERT INTO `dop_action` VALUES ('212', '编辑主机', '修改主机描述', '/host/update', '1', 'hostUpdate', '21');
INSERT INTO `dop_action` VALUES ('213', '删除主机', '删除主机描述', '/host/delete', '1', 'hostDeleteOne', '21');
INSERT INTO `dop_action` VALUES ('214', '主机列表', '主机列表', '/host/list', '1', 'hostList', '21');
INSERT INTO `dop_action` VALUES ('22', '集群管理', '集群管理', '/cluster/index.html', '0', 'clusterIndex', '2');
INSERT INTO `dop_action` VALUES ('221', '创建集群', '添加集群描述', '/cluster/create', '1', 'clusterCreate', '22');
INSERT INTO `dop_action` VALUES ('222', '编辑集群', '编辑集群', '/cluster/update', '1', 'clusterUpdate', '22');
INSERT INTO `dop_action` VALUES ('223', '删除集群', '删除集群', '/cluster/delete', '1', 'clusterDelete', '22');
INSERT INTO `dop_action` VALUES ('224', '添加主机', '添加主机', '/cluster/addHost', '1', 'clusterAddHost', '22');
INSERT INTO `dop_action` VALUES ('225', '解绑主机', '解绑主机', '/cluster/removeHost', '1', 'clusterRemoveHost', '22');
INSERT INTO `dop_action` VALUES ('23', '仓库管理', '仓库管理', '/registry/index.html', '0', 'registryIndex', '2');
INSERT INTO `dop_action` VALUES ('231', '创建仓库', '创建仓库', '/registry/create', '1', 'registryCreate', '23');
INSERT INTO `dop_action` VALUES ('232', '编辑仓库', '编辑仓库', '/registry/update', '1', 'registryUpdate', '23');
INSERT INTO `dop_action` VALUES ('233', '仓库列表', '仓库列表', '/registry/list', '1', 'registryList', '23');
INSERT INTO `dop_action` VALUES ('234', '删除仓库', '删除仓库', '/registry/delete', '1', 'registryDeleteOne', '23');
INSERT INTO `dop_action` VALUES ('235', '所含镜像', '查看仓库中镜像信息', '/registry/images.html', '1', 'registryImages', '23');
INSERT INTO `dop_action` VALUES ('24', '镜像管理', '镜像管理', '/image/index.html', '0', 'imageIndex', '2');
INSERT INTO `dop_action` VALUES ('241', '镜像列表', '镜像列表', '/image/list', '1', 'imageList', '24');
INSERT INTO `dop_action` VALUES ('243', '批量删除', '批量删除', '/image/remove/batch', '1', 'imageRemove', '24');
INSERT INTO `dop_action` VALUES ('244', '发布镜像', '发布镜像', '/image/fastpush', '1', 'imagFastpush', '24');
INSERT INTO `dop_action` VALUES ('245', '删除镜像', '删除镜像', '/image/remove', '1', 'imageRemove', '24');
INSERT INTO `dop_action` VALUES ('25', '容器管理', '容器管理', '/container/index.html', '0', 'containerIndex', '2');
INSERT INTO `dop_action` VALUES ('26', '组件管理', '组件管理', '/component/index.html', '0', 'componentIndex', '2');
INSERT INTO `dop_action` VALUES ('27', '预加载包管理', '预加载包管理', '/library/index.html', '0', 'libraryIndex', '2');
INSERT INTO `dop_action` VALUES ('3', '系统管理', '系统管理', null, '0', 'manage_businessSystem', '0');
INSERT INTO `dop_action` VALUES ('31', '业务系统', '业务系统', '/business/index.html', '0', 'businessIndex', '3');
INSERT INTO `dop_action` VALUES ('32', '物理系统', '物理系统', '/system/index.html', '0', 'systemIndex', '3');
INSERT INTO `dop_action` VALUES ('4', '日志管理', '日志管理', null, '0', 'manage_log', '0');
INSERT INTO `dop_action` VALUES ('41', '访问日志', '访问日志', '/log/index.html', '0', 'logIndex', '4');
INSERT INTO `dop_action` VALUES ('42', '运行日志', '运行日志', '/logs/index.html', '0', 'logsIndex', '4');
INSERT INTO `dop_action` VALUES ('6', '权限管理', '权限管理xxx', null, '0', 'manage_auth', '0');
INSERT INTO `dop_action` VALUES ('61', '用户管理', '用户管理描述', '/user/index.html', '0', 'userIndex', '6');
INSERT INTO `dop_action` VALUES ('611', '创建用户', '创建用户', '/user/create', '1', 'userCreate', '61');
INSERT INTO `dop_action` VALUES ('612', '编辑用户', '编辑用户', '/user/update', '1', 'userUpdate', '61');
INSERT INTO `dop_action` VALUES ('613', '冻结用户', '冻结用户', '/user/delete', '1', 'userDelete', '61');
INSERT INTO `dop_action` VALUES ('614', '用户列表', '用户列表', '/user/all', '1', 'userList', '61');
INSERT INTO `dop_action` VALUES ('615', '用户授权', '用户添加角色', '/user/authToUser', '1', 'userAddRole', '61');
INSERT INTO `dop_action` VALUES ('616', '激活用户', '激活用户', '/user/active', '1', 'activeUser', '61');
INSERT INTO `dop_action` VALUES ('62', '角色管理', '角色管理描述', '/role/index.html', '0', 'roleIndex', '6');
INSERT INTO `dop_action` VALUES ('621', '角色授予权限', '角色授予权限', '/role/authToRole', '1', 'roleAuth', '62');
INSERT INTO `dop_action` VALUES ('622', '编辑角色', '编辑角色', '/role/update', '1', 'roleUpdate', '62');
INSERT INTO `dop_action` VALUES ('623', '角色列表', '角色列表', '/role/all', '1', 'roleList', '62');
INSERT INTO `dop_action` VALUES ('63', '权限管理', '权限管理描述', '/auth/index.html', '0', 'authIndex', '6');
INSERT INTO `dop_action` VALUES ('631', '编辑权限', '修改权限', '/auth/update', '1', 'authUpdate', '63');
INSERT INTO `dop_action` VALUES ('632', '权限列表', '权限列表', '/auth/all', '1', 'authList', '63');

-- ----------------------------
-- Table structure for dop_role
-- ----------------------------
DROP TABLE IF EXISTS `dop_role`;
CREATE TABLE `dop_role` (
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色ID',
  `ROLE_NAME` varchar(80) CHARACTER SET utf8 NOT NULL COMMENT '角色名称',
  `ROLE_DESC` varchar(200) CHARACTER SET utf8 DEFAULT NULL COMMENT '角色描述信息',
  `ROLE_REMARKS` tinyint(4) DEFAULT NULL COMMENT '角色标记',
  `ROLE_STATUS` tinyint(4) NOT NULL COMMENT '角色状态（1 正常，0注销）',
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dop_role
-- ----------------------------
INSERT INTO `dop_role` VALUES ('1', '管理员', '管理员权限', '1', '1');
INSERT INTO `dop_role` VALUES ('2', '超级用户', '超级用户权限', '2', '1');
INSERT INTO `dop_role` VALUES ('3', '普通用户', '普通用户权限', '3', '1');
INSERT INTO `dop_role` VALUES ('system_user', '物理系统用户', '物理系统用户权限', '3', '1');

-- ----------------------------
-- Table structure for dop_role_action
-- ----------------------------
DROP TABLE IF EXISTS `dop_role_action`;
CREATE TABLE `dop_role_action` (
  `ID` varchar(64) NOT NULL COMMENT 'ID',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色ID',
  `ACTION_ID` varchar(64) NOT NULL COMMENT '用户ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dop_role_action
-- ----------------------------
INSERT INTO `dop_role_action` VALUES ('1', '1', '1');
INSERT INTO `dop_role_action` VALUES ('10', '1', '21');
INSERT INTO `dop_role_action` VALUES ('11', '1', '61');
INSERT INTO `dop_role_action` VALUES ('110', '1', '62');
INSERT INTO `dop_role_action` VALUES ('111', '1', '612');
INSERT INTO `dop_role_action` VALUES ('112', '1', '22');
INSERT INTO `dop_role_action` VALUES ('113', '1', '211');
INSERT INTO `dop_role_action` VALUES ('117', '1', '223');
INSERT INTO `dop_role_action` VALUES ('118', '1', '234');
INSERT INTO `dop_role_action` VALUES ('119', '1', '245');
INSERT INTO `dop_role_action` VALUES ('12', '1', '32');
INSERT INTO `dop_role_action` VALUES ('120', '1', '23');
INSERT INTO `dop_role_action` VALUES ('123', '1', '632');
INSERT INTO `dop_role_action` VALUES ('13', '1', '24');
INSERT INTO `dop_role_action` VALUES ('14', '1', '25');
INSERT INTO `dop_role_action` VALUES ('15', '1', '26');
INSERT INTO `dop_role_action` VALUES ('16', '1', '212');
INSERT INTO `dop_role_action` VALUES ('17', '1', '213');
INSERT INTO `dop_role_action` VALUES ('18', '1', '214');
INSERT INTO `dop_role_action` VALUES ('187', '1', '221');
INSERT INTO `dop_role_action` VALUES ('188', '1', '222');
INSERT INTO `dop_role_action` VALUES ('189', '1', '611');
INSERT INTO `dop_role_action` VALUES ('19', '1', '224');
INSERT INTO `dop_role_action` VALUES ('190', '1', '225');
INSERT INTO `dop_role_action` VALUES ('191', '1', '231');
INSERT INTO `dop_role_action` VALUES ('192', '1', '232');
INSERT INTO `dop_role_action` VALUES ('2', '1', '233');
INSERT INTO `dop_role_action` VALUES ('20', '1', '235');
INSERT INTO `dop_role_action` VALUES ('21', '1', '241');
INSERT INTO `dop_role_action` VALUES ('22', '1', '243');
INSERT INTO `dop_role_action` VALUES ('221', '1', '244');
INSERT INTO `dop_role_action` VALUES ('222', '1', '613');
INSERT INTO `dop_role_action` VALUES ('223', '1', '614');
INSERT INTO `dop_role_action` VALUES ('224', '1', '615');
INSERT INTO `dop_role_action` VALUES ('23', '1', '616');
INSERT INTO `dop_role_action` VALUES ('239', '1', '621');
INSERT INTO `dop_role_action` VALUES ('24', '1', '622');
INSERT INTO `dop_role_action` VALUES ('240', '1', '2');
INSERT INTO `dop_role_action` VALUES ('241', '1', '623');
INSERT INTO `dop_role_action` VALUES ('242', '1', '631');
INSERT INTO `dop_role_action` VALUES ('243', '1', '3');
INSERT INTO `dop_role_action` VALUES ('244', '1', '4');
INSERT INTO `dop_role_action` VALUES ('245', '1', '63');
INSERT INTO `dop_role_action` VALUES ('246', '1', '6');
INSERT INTO `dop_role_action` VALUES ('247', '1', '31');
INSERT INTO `dop_role_action` VALUES ('248', '1', '41');
INSERT INTO `dop_role_action` VALUES ('249', '1', '42');

-- ----------------------------
-- Table structure for dop_user
-- ----------------------------
DROP TABLE IF EXISTS `dop_user`;
CREATE TABLE `dop_user` (
  `USER_ID` varchar(64) NOT NULL COMMENT '用户ID',
  `USER_NAME` varchar(32) CHARACTER SET utf8 NOT NULL COMMENT '用户名称',
  `USER_PASS` varchar(255) CHARACTER SET utf8 NOT NULL COMMENT '用户密码',
  `USER_MAIL` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户邮箱',
  `USER_PHONE` varchar(16) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户电话',
  `USER_COMPANY` varchar(256) CHARACTER SET utf8 DEFAULT NULL COMMENT '用户公司名称',
  `USER_LEVEL` int(11) NOT NULL COMMENT '用户级别（0 管理员，1普通用户）',
  `USER_STATUS` tinyint(4) NOT NULL COMMENT '用户状态（1 正常 0 注销）',
  `USER_LOGIN_STATUS` varchar(36) DEFAULT NULL COMMENT '用户登录sessionid',
  `USER_ROLEID` varchar(64) DEFAULT NULL COMMENT '用户角色',
  `USER_CREATEDATE` datetime NOT NULL COMMENT '创建时间',
  `USER_CREATOR` varchar(64) DEFAULT NULL COMMENT '用户创建人。除了顶级管理员，其余用户均应有对应的创建人员。',
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dop_user
-- ----------------------------
INSERT INTO `dop_user` VALUES ('08e91640a83e454cb4dde0274681ec05', 'undefined', 'E10ADC3949BA59ABBE56E057F20F883E', '867808253@qq.com', '13511601420', 'xxxx', '1', '1', null, null, '2016-03-11 10:39:04', '1');
INSERT INTO `dop_user` VALUES ('1', 'admin', 'E10ADC3949BA59ABBE56E057F20F883E', 'cmbc15@163.com', '15100118888', null, '0', '1', '8692B218974099DDE81F172E2AE2C6A8', null, '2015-11-05 10:04:06', '1');
INSERT INTO `dop_user` VALUES ('1111', 'admin2', 'E10ADC3949BA59ABBE56E057F20F883E', 'cmbc15@163.com', '15100118888', null, '0', '1', '29C97EF44AC40E40B773E986D34CA8C2', null, '2015-11-05 10:04:06', '1');
INSERT INTO `dop_user` VALUES ('1fca6e16e492487db6b3f164e19fe384', '111', '{AES}YhV8yOQ0GsfcOo3tH0+p9QAA7ff719uS6koJbPJxuJg=', '11@qq.com', '13011111111', null, '1', '1', null, null, '2016-03-11 09:11:16', '1');
INSERT INTO `dop_user` VALUES ('2', 'JunitUserNameUpdate', '123456', 'junit@sina.com.cn', '61450079', null, '1', '0', null, '1', '2016-02-24 09:23:50', '1');
INSERT INTO `dop_user` VALUES ('2222', 'sgcc', 'E10ADC3949BA59ABBE56E057F20F883E', 'fkdtz2008@163.com', '15100115128', 'fsadfad', '1', '1', null, '1', '2014-06-16 16:04:06', '1');

-- ----------------------------
-- Table structure for dop_user_role
-- ----------------------------
DROP TABLE IF EXISTS `dop_user_role`;
CREATE TABLE `dop_user_role` (
  `ID` varchar(64) NOT NULL COMMENT 'ID',
  `USER_ID` varchar(64) NOT NULL COMMENT '用户ID',
  `ROLE_ID` varchar(64) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dop_user_role
-- ----------------------------
INSERT INTO `dop_user_role` VALUES ('1', '1', '1');
INSERT INTO `dop_user_role` VALUES ('4', '23', '1');
INSERT INTO `dop_user_role` VALUES ('5', '24', '1');
INSERT INTO `dop_user_role` VALUES ('7', '26', '1');
INSERT INTO `dop_user_role` VALUES ('d1122db1b9504ffebef72cb998d69c75', '08e91640a83e454cb4dde0274681ec05', '2');
