package org.sgcc.devops.web;

import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.util.Base64;


public class AESTest {
	public static void main(String[] args) {
		System.out.println(Encrypt.encrypt("anNlcGMwMSE=", "D:/SGCC-TEMP/security/"));
		System.out.println(Encrypt.decrypt("{AES}9n5UeKAC8ITwGJr7D2xMgF6lI4Nw/dR8Y+c+ahilbjw=", "D:/SGCC-TEMP/security/"));
		System.out.println(Base64.getFromBase64("MTIzNDU2"));
		System.out.println(Base64.getBase64("jsepc01!"));
		
	}
}
