;(function( $ ){ 
  'use strict';
  var defaults = {};
  var register = function( cytoscape, $ ){
    if( !cytoscape ){ return; }

    cytoscape('core', 'cxtmenu', function(params){
        var options = $.extend(true, {}, defaults, params),
          commands = options.commands,
          commandObj = null,
          selectorPrefix = 'node_cxtmenu_',
          events = {},
          eventHandler = 'taphold cxttapend tapend vclick tap',
          cy = this,
          $divElement = $('<div class="cytoscap_cxtmenu"></div>').hide(),
          $container = $( cy.container() ),
          tempHtml = '<ul>',
          timestamp = null;
        
        function getTimeStamp(){
          var timestamp = (new Date()).getTime();
          if(events[selectorPrefix + timestamp]){
            getTimeStamp(timestamp);
          }else
            return selectorPrefix + timestamp;
        }
        for( var i = 0; i < commands.length; i++ ){
          commandObj = commands[i];
          timestamp = getTimeStamp();
          events[timestamp] = commandObj.select;
          tempHtml += '<li><a href="javascript:void(0);" id="' + timestamp + '">' + commandObj.content +'</a></li>';
        }
        tempHtml +='</ul>';
        $divElement.append(tempHtml);
        $container.append( $divElement );

        var hide = function(){
            $divElement.hide();
//            cy.userZoomingEnabled( true );
//            cy.userPanningEnabled( true );
        }

        cy.on(eventHandler,hide);
        cy.on('cxttapstart', params.selector, function(){
          cy.off(eventHandler,hide);
          var _this = this,
            isCy = _this === cy,
            position = _this.position();

//          cy.userZoomingEnabled( false );
//          cy.userPanningEnabled( false );

          //确定显示位置
          var rp, rw, rh;
            if( !isCy && _this.isNode() ){
              rp = _this.renderedPosition();
              rw = _this.renderedWidth();
              rh = _this.renderedHeight();
            } else {
              rp = _this.cyRenderedPosition;
              rw = 1;
              rh = 1;
            }
          $divElement.css({
              left : Math.ceil(rp.x + _this.width() / 2) + 'px',
              top : Math.ceil(rp.y) + 'px'
            }).show(function(){
            	cy.on(eventHandler,hide);
            }).find('a').off().on('click',function(e){
              var eventTarget = $(e.target),
              id = eventTarget.attr('id');
            if(id && id.indexOf('node_cxtmenu_') == 0)
              events[id].apply(_this);
            else
              events[eventTarget.parents('[id^="node_cxtmenu_"]').attr('id') + ''].apply(_this);
            hide();
          });
        });
    });

  }; // reg

  if( typeof module !== 'undefined' && module.exports ){ // expose as a commonjs module
    module.exports = register;
  }

  if( typeof define !== 'undefined' && define.amd ){ // expose as an amd/requirejs module
    define('cytoscape-cxtmenu', function(){
      return register;
    });
  }

  if( typeof cytoscape !== 'undefined' ){ // expose to global cytoscape (i.e. window.cytoscape)
    register( cytoscape, $ );
  }

})( jQuery );
