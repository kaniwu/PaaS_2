<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<link rel="stylesheet" href="<%=basePath %>ace/assets/css/chosen.css" />
<script src="<%=basePath %>ace/assets/js/chosen.jquery.min.js"></script>
<script src="<%=basePath %>js/console/cluster.js"></script>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
<style type="text/css">
	.checkbox + .checkbox, .radio + .radio {
	    margin-top: 10px;
	}
</style>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="cluster_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a href="<%=basePath %>index.html"><strong>首页</strong></a>
							</li>
							<li class="active"><b>集群管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
							<c:if test="${fn:contains(authButton,'authList')}">
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>集群名称：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="clusterName" type="text" class="form-control" />
										</div>
									</div>
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>所在主机：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="hostIp" type="text" class="form-control" />
										</div>
									</div>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchHost()">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
									</div>
								</div>
								</c:if>
								<div>
									<button class="btn btn-sm btn-success btn-round"
										onclick="showCreateClusterWin()">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<button class="btn btn-sm btn-warning btn-round "
										onclick="updateClusterWin()">
										<i class="ace-icon fa fa-pencil-square-o bigger-125"></i> <b>编辑</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round hide"
										onclick="isolateCluster()">
										<i class="ace-icon fa fa-exclamation-circle bigger-125 "></i> <b>隔离</b>
									</button>
									<a  href='#migrateCluster' class="btn btn-sm btn-danger btn-round hide"
										onclick="migrateCluster()">
										<i class="ace-icon fa fa-external-link bigger-125"></i> <b>迁移</b>
									</a>
									<button class="btn btn-sm btn-danger btn-round" 
										onclick="removeCluster()">
										<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
									</button>
								</div>
								<div>
									<table id="cluster_list"></table>
									<div id="cluster_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<div id="modal-wizard" class="modal" data-backdrop="static" style="height: 100%;">
			<div class="modal-dialog" style="height: 500px;">
				<div class="modal-content" style="height: 500px;">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal"
							class="bootbox-close-button close" type="button">×</button>
						<h4 class="modal-title">
							<b>集群主机管理</b>
						</h4>
					</div>
					<input type="hidden" id="clusterId"> <input type="hidden"
						id="masterIp">
					<div class="modal-body">
						<div class="left" style="float: left; width: 40%; height: 360px">
							<select class="width-100 chosen-select" id="freeHost_select" data-placeholder="">
								<option value="">请选择</option>
							</select>
							<a href="#" class="list-group-item active"> 空闲DOCKER主机 </a>
							<div id="freeDockerHost"
								style="float: left; width: 100%; height: 320px; overflow: auto; border: 1px solid #ddd;">

							</div>
						</div>
						<div
							style="float: left; width: 20%; text-align: center; margin-top: 80px">
							<button type="button" class="btn btn-success btn-sm btn-round"
								onclick="addToCluster();" >
								<span class="glyphicon glyphicon-forward"></span>添加
							</button>
							<button type="button" class="btn btn-success btn-sm btn-round"
								style="margin-top: 10px" onclick="unbindFromCluster();">
								<span class="glyphicon glyphicon-backward"></span>解绑
							</button>
							<button class="btn btn-warning btn-sm  btn-round" style="margin-top: 10px"
								data-dismiss="modal">
								<i class="ace-icon fa fa-times"></i> 取消
							</button>
							<div id="spinner2" style="margin-top: 80px;display: none">
								<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等
							</div>
						</div>
						<div class="left" style="float: right; width: 40%; height: 360px">
							<select class="width-100 chosen-select" id="clusterHost_select" data-placeholder="">
								<option value="">请选择</option>
							</select>
							<a href="#" class="list-group-item active"> 集群中的DOCKER主机 </a>
							<div id="dockerInCluster"
								style="float: left; width: 100%; height: 320px; overflow: auto; border: 1px solid #ddd;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="migrateCluster" class="modal" data-backdrop="static">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal"
							class="bootbox-close-button close" type="button">×</button>
						<h4 class="modal-title">
							<b>集群主机迁移</b>
						</h4>
					</div>
					<div class="modal-body">
					<form class="form form-horizontal" id="create-form"
								style="margin: 0px 10px">
								<input type="hidden" id="clusterId_">
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>集群名称：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="clusterName_" name="clusterName_"
											class="form-control" readonly="readonly"/>
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>当前主机：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="clusterHostIP" name="clusterHostIP"
											class="form-control" readonly="readonly"/>
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>目标主机：</b>
										</div></label>
									<div class="col-sm-9">
										<select id="hostList" name="hostList" class='form-control'>
											<option value="">请选择</option>
										</select>
									</div>
								</div>
							</form>
					</div>
					<div class="modal-footer wizard-actions" >
						<button id="migrateCluster_btn" class="btn btn-danger btn-sm  btn-round" onclick="migrateCluster();" disabled>
							迁移 <i class="ace-icon fa fa-external-link icon-on-right"></i>
						</button>
						<button class="btn btn-warning btn-sm  btn-round"
							data-dismiss="modal">
							<i class="ace-icon fa fa-times"></i> 取消
						</button>
					</div>
				</div>
			</div>
		</div>
		<script id="add_host" type="text/html">	
               <div class='well' style='margin-top:1px;'>
	        	   <form class='form-horizontal' role='form''>
                  <div class='form-group'  >
	      	        <label class='col-sm-3'><div align='left'><b>IP</b></div></label>

                    <label class='col-sm-6'><div align='left'><b>docker版本</b></div></label>

                    <label class='col-sm-3'><div align='left'><b>安装版本</b></div></label>
	              </div>
           {{if list}}
                {{each list}}
	        	  <div class='form-group' >
	      	    <label class='col-sm-3'><div align='left'><b>{{$value.hostIp}}</b></div></label>

                <label class='col-sm-6'><div align='left'><b>{{$value.dockerVersion}}</b></div></label>

	      	      <div class='col-sm-3'>
	      	       	<select   class='form-control hostDocker_select' id="{{$value.hostID}}">
                          <option value="0">不进行安装</option>
	      			  </select>
	              	</div>
	            </div>
                 {{/each}}
            {{/if}}
                   </form>
	            </div>
        </script>				
</body>
</html>
