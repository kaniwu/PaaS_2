<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>ace/assets/js/jquery.validate.min.js"></script>
<script src="<%=basePath %>ace/assets/js/additional-methods.min.js"></script>
<script src="<%=basePath %>ace/assets/js/jquery.maskedinput.min.js"></script>
<script src="<%=basePath %>ace/assets/js/select2.min.js"></script>
<script src="<%=basePath %>ace/assets/js/fuelux/fuelux.wizard.min.js"></script>
<script src="<%=basePath %>js/console/image.js"></script>
<script src="<%=basePath %>js/task/switchImageTask.js"></script>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="image_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>镜像管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<ul id="myTab" class="nav nav-tabs">
									<li class="active"><a href="#midImage"
										data-toggle="tab" onclick="imageTypeChange(0)">中间件镜像</a></li>
									<li class="hide"><a href="#nginxImage"
										data-toggle="tab" onclick="imageTypeChange(2)">NGINX镜像</a></li>
									<li><a href="#baseImage" data-toggle="tab"
										onclick="imageTypeChange(3)">基础镜像</a></li>
									<li><a href="#appImage" data-toggle="tab"
										onclick="imageTypeChange(1)">应用镜像</a></li>
									<div id="push_image_loading" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
									</div>
								</ul>
								<div id="myTabContent" class="tab-content"
									style="padding-bottom: 0px">
									<input type="hidden" id="switchImageId">
									<div class="tab-pane fade in active" id="midImage">
									<c:if test="${fn:contains(authButton,'authList')}">
										<div class="well well-sm">
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="imageNameLable"
												for="form-field-1-1"><strong>镜像名称：</strong> </label>
											<div class="col-sm-2" id="imageNameDiv">
												<div class="select-group">
													<input id="imageName_0" type="text" class="form-control" />
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="registryLabel"
												for="form-field-1-1"><strong>所在仓库：</strong> </label>
											<div class="col-sm-2" id="registryDiv">
												<div class="select-group">
													<select id="registryName_0" class="form-control" >
													</select>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-primary btn-round"
												onclick="searchHost(0)">
												<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
												<b>查询</b>
											</button>
										</div>
										</c:if>
										<div>
											<button class="btn btn-sm btn-success btn-round" id="push_add_menu"
												onclick="showCreateImageModal(0)">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
											</button>
											<button class="btn btn-sm btn-warning btn-round" onclick="pushImage()">
												<i class="ace-icon fa fa-arrow-up bigger-125"></i> <b>发布</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round" id="push_del_menu"
												onclick="removeImage()">
												<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
											</button>
											<button class="btn btn-sm btn-success btn-round" id="export_image"
												onclick="ExportImage()">
												<i class="ace-icon fa fa-arrow-down bigger-125"></i> <b>下载镜像</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round" id="switchImageButt"
												onclick="SwitchImage()">
												<i class="ace-icon fa fa-refresh bigger-125"></i> <b>切换镜像</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round hide" id="switchImageProcess"
												onclick="openMessageInfo()">
												<i class="ace-icon fa fa-refresh bigger-125"></i> <b>查看切换进度</b>
											</button>
										</div>
									</div>
								<div class="tab-pane fade" id="nginxImage">
										<div class="well well-sm">
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="imageNameLable"
												for="form-field-1-1"><strong>镜像名称：</strong> </label>
											<div class="col-sm-2" id="imageNameDiv">
												<div class="select-group">
													<input id="imageName_2" type="text" class="form-control" />
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="registryLabel"
												for="form-field-1-1"><strong>所在仓库：</strong> </label>
											<div class="col-sm-2" id="registryDiv">
												<div class="select-group">
													<select id="registryName_2" class="form-control" >
													</select>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-primary btn-round"
												onclick="searchHost(2)">
												<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
												<b>查询</b>
											</button>
										</div>
										<div>
											<button class="btn btn-sm btn-success btn-round" id="push_add_menu"
												onclick="showCreateImageModal(2)">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
											</button>
											<button class="btn btn-sm btn-warning btn-round"  onclick="pushImage()">
												<i class="ace-icon fa fa-arrow-up bigger-125"></i> <b>发布</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round" id="push_del_menu"
												onclick="removeImage()">
												<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
											</button>
										</div>
									</div>
									<div class="tab-pane fade" id="baseImage">
										<div class="well well-sm">
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="imageNameLable"
												for="form-field-1-1"><strong>镜像名称：</strong> </label>
											<div class="col-sm-2" id="imageNameDiv">
												<div class="select-group">
													<input id="imageName_3" type="text" class="form-control" />
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="registryLabel"
												for="form-field-1-1"><strong>所在仓库：</strong> </label>
											<div class="col-sm-2" id="registryDiv">
												<div class="select-group">
													<select id="registryName_3" class="form-control" >
													</select>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-primary btn-round"
												onclick="searchHost(3)">
												<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
												<b>查询</b>
											</button>
										</div>
										<div>
											<button class="btn btn-sm btn-success btn-round" id="push_add_menu"
												onclick="showCreateImageModal(3)">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
											</button>
											<button class="btn btn-sm btn-warning btn-round"  onclick="pushImage()">
												<i class="ace-icon fa fa-arrow-up bigger-125"></i> <b>发布</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round" id="push_del_menu"
												onclick="removeImage()">
												<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
											</button>
										</div>
									</div>
									<div class="tab-pane fade" id="appImage">
										<div class="well well-sm">
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="imageNameLable"
												for="form-field-1-1"><strong>镜像名称：</strong> </label>
											<div class="col-sm-2" id="imageNameDiv">
												<div class="select-group">
													<input id="imageName_1" type="text" class="form-control" />
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding-right" style="margin-top: 5px;" id="registryLabel"
												for="form-field-1-1"><strong>所在仓库：</strong> </label>
											<div class="col-sm-2" id="registryDiv">
												<div class="select-group">
													<select id="registryName_1" class="form-control" >
													</select>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-primary btn-round"
												onclick="searchHost(1)">
												<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
												<b>查询</b>
											</button>
										</div>
									</div>
								</div>
								<div>
									<table id="image_list"></table>
									<div id="image_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal-wizard" class="modal" draggable="true"
		data-backdrop="static"></div>
	<div id="wizard_content_div" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" onclick="cancelImageCreate()"
						class="bootbox-close-button close" type="button">×</button>
					<h4 class="modal-title">
						<b>上传镜像</b>
					</h4>
				</div>
				<div class="modal-body">
					<form class="form form-horizontal" id="create-form"
								style="margin: 0px 10px">
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>目标仓库：</b>
										</div></label>
									<div class="col-sm-9">
										<select id="registry_select" name="registry_select"
											class='form-control'></select>
									</div>
								</div>
								<div class='form-group '>
									<label class='col-sm-3'><div align='right'>
											<b>镜像类型：</b>
										</div></label>
									<div class="col-sm-9">
										<div class="radio-inline" style="padding-top: 0px;">
											<label>
											<input id="appImage_radio" name="image_type" type="radio" value="0"
											class="ace"  /> <span class="lbl"> <b>中间件镜像</b></span>
											</label>
										</div>
<!-- 										<input id="nginxImage_radio" name="image_type" type="radio" -->
<!-- 											value="2" class="ace" /> <span class="lbl"> <b>NGINX镜像</b></span> -->
										<div class="radio-inline" style="padding-top: 0px;">
											<label>
											<input id="baseImage_radio" name="image_type" type="radio"
											value="3" class="ace" /> <span class="lbl"> <b>基础镜像</b></span>
											</label>
										</div>
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>镜像文件：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="hidden" id="image_id" /> <input type="file"
											id="image_file" name="image_file" class="form-control" onchange="uploadImageFile()"/>
										<span id="percentage"></span>
									    <br />
										<progress id="progressBar" value="0" max="100" hidden="true"> </progress>
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>镜像名称：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="templatename" name="templatename"
											placeholder="请输镜像名称" class="form-control" onblur="checkImageName_valid()"  />
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>镜像版本：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="templateversion" name="templateversion"
											placeholder="请输入镜像版本【a-z】.【0-9】" class="form-control"
											onblur="checkImageVersion_valid()" />
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>镜像别名：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="tempName" name="tempName"
											placeholder="请输镜像别名" class="form-control"
											onblur="checkImageTempName_valid()" />
									</div>
								</div>
								<div class='form-group'>
									<label class='col-sm-3'><div align='right'>
											<b>服务端口：</b>
										</div></label>
									<div class="col-sm-9">
										<input type="text" id="imagePort" name="imagePort"
											placeholder="请输服务端口，多个服务端口用逗号分隔" class="form-control"
											onblur="checkImagePort_valid()" />
									</div>
								</div>
								<div class="item" id="imagestatus" style="display: none;">
									<div id="imagemessage" class="alert alert-warning"
										style="margin: 10px 10px" role="alert">
										<b>镜像制作中，请耐心等待...</b>
									</div>
								</div>
								<div id="pid" style="display: none;">
									<div style="" id="showId">已经完成0%</div>
									<div class="progress">
										<div id="progressid"
											class="progress-bar progress-bar-success progress-bar-striped"
											role="progressbar" aria-valuenow="40" aria-valuemin="0"
											aria-valuemax="100" style="width: 0%;"></div>
									</div>
								</div>
							</form>
				</div>
				<div class="modal-footer wizard-actions">
					<button id="make_image_btn"
						class="btn btn-primary btn-sm btn-create btn-round"
						type="button" onclick="makeImageFun()" disabled="disabled">
						<i class="ace-icon fa fa-cog bigger-110 icon-only"></i> <span
							id="make-image_btn-text"><b>制作</b></span>
					</button>
					<button id="image_publish_cancel"
						class="btn btn-danger btn-sm btn-round"
						onclick="cancelImageCreate()">
						<i class="ace-icon fa fa-times"></i> 取消
					</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="changeImage">
<div style="margin-top:1px;" class="well">
<form class="form-horizontal" role="form">
        <div class='form-group'  >
		    <label class='col-sm-3'><div align='right'><b>原镜像：</b></div></label>
		      <div class='col-sm-7'>
		       	<input type="text" class="form-control" disabled="disabled" id='oldImageName'/>
	        	</div>
	    </div>
        <div class='form-group'  >
		    <label class='col-sm-3'><div align='right'><b>目标镜像：</b></div></label>
		      <div class='col-sm-7'>
		       	<select class='form-control' id="baseImageId" name="baseImageId">
				</select>
	        </div>
	    </div>
      <div class='form-group'  >
          <div class="col-sm-12">
            	<ul id="myTabSj" class="nav nav-tabs">
									
		       </ul>
     	</div>
      </div>
      <div id="myTabSjContent" class="tab-content" style="padding-bottom: 0px;max-height: 250px;
    overflow-y: auto;">
		
           </div>
      </div> 
     </div>
</form>
</div>

    </script>
    <script type="text/html" id="openProcess">
<div style="margin-top:1px;" class="well" id="messageInfo">
</div>
    </script>
</body>
</html>
