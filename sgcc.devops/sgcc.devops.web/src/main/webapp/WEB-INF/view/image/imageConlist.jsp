<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<link rel="stylesheet" href="<%=basePath %>css/user/container.css" />
<script src="<%=basePath %>ace/assets/js/jquery.validate.min.js"></script>
<script src="<%=basePath %>ace/assets/js/additional-methods.min.js"></script>
<script src="<%=basePath %>ace/assets/js/jquery.maskedinput.min.js"></script>
<script src="<%=basePath %>ace/assets/js/select2.min.js"></script>
<script src="<%=basePath %>js/bootstrap-paginator.min.js"></script>
<script src="<%=basePath %>ace/assets/js/fuelux/fuelux.wizard.min.js"></script>
<script src="<%=basePath %>js/console/imageContainer.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<input type="hidden" id="imageId" name="imageId" value="${imageId}" />
		<input type="hidden" id="imageType" name="imageType" value="${imageType}" />
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="image_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>

			<div class="main-content">

				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><i class="ace-icon fa home-icon"></i><b>镜像管理</b></li>
							<li class="active"><b>包含容器列表</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<button class="btn btn-primary btn-sm btn-round"
										onclick="history.go(-1)">
										<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
									</button>
									<span class="text-muted" style="position: relative; left: 70px">
										<strong>镜像名称 : ${imageName }</strong>
									</span>
								</div>
								<div>
									<table id="imageContainer_list"></table>
									<div id="imageContainer_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
