<%@page import="com.sgcc.devops.web.image.Encrypt"%>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>应用云平台</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet"
	href="<%=basePath %>ace/assets/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<%=basePath %>css/login.css" />
<script src="<%=basePath %>ace/assets/js/jquery.min.js"></script>
<script src="<%=basePath %>ace/assets/js/ace-extra.min.js"></script>
<script src="<%=basePath %>ace/assets/js/bootstrap.min.js"></script>
<script src="<%=basePath %>ace/assets/js/ace/ace.js"></script>
<script src="<%=basePath %>ace/assets/js/bootbox.min.js"></script>
<script src="<%=basePath %>ace/assets/js/jquery.gritter.min.js"></script>
<script src="<%=basePath %>ace/assets/js/jquery.validate.min.js"></script>
<script src="<%=basePath %>js/base64.js"></script>
<script	src="<%=basePath %>js/validate.js"></script>
<link rel="shortcut icon" href="fav.ico" />
<%
request.getSession().invalidate();//清空session
if(request.getCookies() != null){
	Cookie cookie = request.getCookies()[0];//获取cookie
	if(cookie!=null){
		cookie.setMaxAge(0);//让cookie过期
	}
}
%>
</head>
<body>
	<div style="width: 100%;background-color: darkcyan;height: 100%;position: absolute;filter: url(blur.svg#blur);-webkit-filter: blur(100px); -moz-filter: blur(10px); -ms-filter: blur(10px);filter: blur(10px);"></div>
    <div style="overflow-y: auto;min-width: 850px;width: 100%;height: 100%;position: relative;">
        <div>
            <img style="width:280px;height:120px" src="img/bg2.png" alt="logo"/>
        </div>
        <div>
            <div style="text-align:center; margin-bottom: 12px;">
                <div style="font-size: 48px;color: #fff;font-weight: bolder;">电力应用云平台</div>
            </div>
            <div id="warnbox" class="alert alert-warning text-center" style="margin-top: 50px;display:none" role="alert">
				您的浏览器对Html5的支持性不是很好！为了您拥有最佳的用户体验，请您更换以下浏览器访问: <br /> <br />
				<ul class="text-left" style="margin-left: 400px;">
					<li>IE浏览器,我们仅支持IE9+,最好使用IE10及以上版本</li>
					<li>Firefox浏览器,我们建议使用 Firefox 30+ 版本</li>
					<li>Chrome浏览器,我们建议使用 Chrome 35+ 版本</li>
				</ul>
				<br /> 推荐使用新版Chrome <a href="/file/Chrome.rar">点击下载</a> &nbsp;&nbsp;
				Explore10 64位下载 <a href="/file/Explore_64.rar">点击下载</a> &nbsp;&nbsp;
				Explore10 32位下载 <a href="/file/Explore_32.rar">点击下载</a>
			</div>
            <div id="loginbox" style="display:none;width: 420px;height: auto;text-align: center;border-radius: 20px;margin: auto;padding: 10px 0;box-shadow: 0px 0px 35px white;">
                <form action="<%=basePath %>login" method="post" id="form1">
                	<input type="hidden" id="userName" name="userName"/>
                	<input type="hidden" id="pwd" name="pwd" />
<!--                 	<input type="hidden" id="vercode" name="vercode" /> -->
                </form>
                <div class="login_content">
	                    <input type="text" id="userName_" name="userName_" class="login_input" placeholder="用户名"value=""/>
                	</div>
                	<div class="login_content">
                    	<input type="password" id="password_" name="password_" class="login_input" placeholder="密码" value="" autocomplete="off"/>
                	</div>
<!-- 					<div class="login_content"> -->
<!-- 	                    <input type="text" class="login_input" placeholder="验证码" id="vercode_" name="vercode_" value=""/> -->
<!-- 	                    <div style="width: 280px;padding-top: 6px;margin: auto;text-align: left;"> -->
<!-- 							<img class="verify" alt="验证码" id="authImg"> -->
<!-- 		                    <a class="ver-change" style="color:deepskyblue;" href="javascript:refresh();">换一个？</a> -->
<!-- 		                </div>  -->
<!-- 					</div> -->
					<label class="error" style="color: red;">${message}</label>
					<div class="login_content">
	                    <input type="button" onclick="submitForm()" class="btn btn-primary" style="font-weight: bolder;width: 280px;" value="登&nbsp;&nbsp;&nbsp;录"/>
					</div>
            </div>
        </div>
    </div>
	<script>
		function submitForm(){
			var pattern = new RegExp("[;%@$`&()=|{},\\[\\].<>/?~\n\r]");
			var user = $("#userName_").val();
		    if(pattern.test(user)){
		    	bootbox.alert("用户名不能使用特殊字符");
		    	return false;
		    }
		    if(pattern.test($("#password_").val())){
		    	bootbox.alert("密码不能使用特殊字符");
		    	return false;
		    }
// 		    if(pattern.test($("#vercode_").val())){
// 		    	bootbox.alert("验证码输入异常");
// 		    	return false;
// 		    }
			var b = new Base64();
	 		var userName = b.encode($("#userName_").val());
	 		var password = b.encode($("#password_").val());
// 	 		var vercode = b.encode($("#vercode_").val());
	 		$("#userName").val(userName);
	 		$("#pwd").val(password);
// 	 		$("#vercode").val(vercode);
	 		$("#form1").submit();
		}
		
		$(function() {
			refresh();
		});

		function refresh() {
			$("#authImg").attr("src", "<%=basePath %>captcha?" + Math.random());
		}

		$(function() {
			if (window.File && 'WebSocket' in window) {
				$("#loginbox").show();
				$("#warnbox").hide();

			} else {
				$("#loginbox").hide();
				$("#warnbox").show();
			}
		});
		document.onkeydown=function(event){ 
			e = event ? event :(window.event ? window.event : null); 
			if(e.keyCode==13){ 
				//执行的方法 
				submitForm();
			} 
		};
	</script>
</body>
</html>