<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/password.js"></script>
<script src="<%=basePath %>js/base64.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class=".container" style="margin-top: 100px; width: 250%;">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="username" class="col-sm-2 control-label">用户名</label>
					<div class="col-sm-4">
						<input type="text" class="form-control" style="width: 250px;" readonly="readonly"
							id="username"  value="${username }"><span
							id="usernameTip" style="display: none; color: red;"></span>
					</div>
					<div class="col-sm-6">
					</div>
				</div>
				<div class="form-group">
					<label for="newpass" class="col-sm-2 control-label">新密码</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" style="width: 250px;"
							id="newpass" ><span
							id="newpassTip" style="display: none; color: red;"></span>
					</div>
					<div class="col-sm-6">
					</div>
				</div>
				<div class="form-group">
					<label for="newpassAgain" class="col-sm-2 control-label">确认新密码</label>
					<div class="col-sm-4">
						<input type="password" class="form-control" style="width: 250px;"
							id="newpassAgain" ><span
							id="newpassAgainTip" style="display: none; color: red;"></span>
					</div>
					<div class="col-sm-6">
					</div>
				</div>
				<div class="form-group" style="margin-top: 50px;">
					<label class="col-sm-2 control-label"> </label>
					<div class="col-sm-4">
						<button type="submit" class="btn btn-sm btn-success btn-round" id="submit" 
						style="text-align: center;width: 100px">确认</button>
						<button type="button" class="btn btn-sm btn-warning btn-round" id="submit" onclick="history.go(-1);"
						style="text-align: center;width: 100px">返回</button>
					</div>
					<div class="col-sm-6">
					</div>
				</div>
			</form>
		</div>
		<div id="modifySuccess" class="alert alert-success alert-dismissable"
			style="width: 50%; margin-left: 40%; display: none;">
			 <strong>Success!</strong> 你已成功修改密码！
		</div>
	</div>
</body>
</html>
