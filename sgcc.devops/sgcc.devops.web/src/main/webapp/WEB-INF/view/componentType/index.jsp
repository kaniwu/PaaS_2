<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<jsp:include page="../js.jsp"></jsp:include>
		<script src="<%=basePath %>js/console/componentType.js"></script>
	</head>
	<body class="no-skin">
		<div id="index_index_body" class="index_index_body">
			<jsp:include page="../header.jsp"></jsp:include>
			<div class="main-container" id="main-container">
				<script type="text/javascript">
					try{ace.settings.check('main-container' , 'fixed')}catch(e){}
				</script>
				<jsp:include page="../nav.jsp">
					<jsp:param value="componentType_admin" name="page_index"/>
					<jsp:param value="manage_system" name="parent_index"/>
				</jsp:include>	
				<div class="main-content">
					<div class="main-content-inner">
						<div class="breadcrumbs" id="breadcrumbs">
							<ul class="breadcrumb">
								<li>
									<i class="ace-icon fa fa-home home-icon"></i>
									<a href="<%=basePath %>index.html"><strong>首页</strong></a>
								</li>
								<li class="active"><b>组件配置</b></li>
							</ul>
						</div>
						<div class="page-content">
							<div class="row">
								<div class="col-xs-12">
									 <div class="well well-sm">
										<button class="btn btn-sm btn-primary" onclick="showCreateComponentTypeWin()"> 
											<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>
											<b>添加组件类型</b>
										</button>
										<button class="btn btn-sm btn-primary" onclick="showDeleteComponentTypes()"> 
											<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>
											<b>删除组件类型</b>
										</button>									
									</div> 
									<div>
										<table id="componentType_list"></table>
										<div id="componentType_page"></div>
									</div>
								</div>
							</div>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
