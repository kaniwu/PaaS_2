<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
	

<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/parameter.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed');
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="parameter" name="page_index" />
				<jsp:param value="casd" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="menu-icon fa fa-book"></i> 
								<strong>数据字典管理</strong></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
							
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 9px;"><strong>名称</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="para_name" type="text" class="form-control" style="margin-top: 5px;" />
										</div>
									</div>
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 9px;"><strong>类型：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-type">
											<select id="type_name" class="form-control" style="margin-top: 5px;">
												<option value="">请选择</option>
												<option value="0">位置属性层级意义</option>
												<option value="1">安装组件配置</option>
											</select>
										</div>
									</div>
									
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchParameter()" style="margin-top: 5px;">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
									</button>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="createParameter()" style="margin-top: 5px;">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
									</div>
								</div>
								<div>
									<table id="parameter_list"></table>
									<div id="parameter_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
