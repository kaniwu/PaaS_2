<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>

<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape.min.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape-qtip.js"></script>

<script src="<%=basePath %>js/console/topologys.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<input id="systemId" type="hidden" name="systemId" value="${systemId}">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="business_admin" name="page_index" />
				<jsp:param value="manage_businessSystem" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<input type="hidden" id="businessId" value="${businessId}">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a href="<%=basePath %>index.html"><strong>首页</strong></a>
							</li>
							<li class="active"><b>系统部署</b></li>
						</ul>
					</div>
					<div class="page-content" style="overflow:hidden;">
						<div class="well well-sm">
							<button class="btn btn-sm btn-primary btn-round" onclick="history.go(-1);">
								<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
							</button>
							<span class="text-muted" style="position: relative;left: 70px">
									<strong>业务系统名称 : ${businessName }</strong>
							</span>
							<div id="spinner" style="float: right; display: none;">
								<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<ul id="myTabSj" class="nav nav-tabs">
								</ul>
								<div id="canvasupParent" class="tab-content"
									style="padding-bottom: 0px"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
