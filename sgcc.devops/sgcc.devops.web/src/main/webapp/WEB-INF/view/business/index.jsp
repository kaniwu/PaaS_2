<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/business.js"></script>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="business_admin" name="page_index" />
				<jsp:param value="manage_businessSystem" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a href="<%=basePath %>index.html"><strong>首页</strong></a>
							</li>
							<li class="active"><b>业务管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
							<c:if test="${fn:contains(authButton,'authList')}">
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" style="width: 150px;margin-top: 5px;"
										for="businessName"><strong>系统名称：</strong> </label>
									<div class="col-sm-2">
										<div class="input-group">
											<input id="businessName" type="text" class="form-control" />
										</div>
									</div>
									
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchBusiness()">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
										<b>查询</b>
									</button>
									
									<button class="btn btn-sm btn-primary btn-round"
										onclick="loadBusiness()">
										<i class="ace-icon fa fa-refresh bigger-125"></i> <b>同步</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
									</div>
								</div>
								</c:if>
								<div style="padding-right: 10px">
									<table id="business_list"></table>
									<div id="business_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal-wizard" class="modal" data-backdrop="static">
		<div class="modal-dialog"
			style="width: 50%; margin-top: 20px; margin-bottom: 10px;">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal"
						class="bootbox-close-button close" type="button">×</button>
					<h4 class="modal-title">
						<b id="businessNametemp"></b>
					</h4>
				</div>
				<div class='form-group'  >
			          <div class="col-sm-12">
			            	<ul id="myTabSj" class="nav nav-tabs">
					       </ul>
			     	</div>
			      </div>
				<div class="modal-body " style="max-height:400px; overflow-y: scroll;">
					<div class="well">
						<table id="system_list" style="width: 100%;border: 1px;border-style:inset;" class="table">
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-warning btn-sm btn-round" data-dismiss="modal">
						<i class="ace-icon fa fa-times"></i>关闭
					</button>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
