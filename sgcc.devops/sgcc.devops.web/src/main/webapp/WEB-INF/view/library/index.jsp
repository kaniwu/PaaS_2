<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/library.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="library_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>预加载包管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>名称：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="libraryName" type="text" class="form-control" />
										</div>
									</div>
									
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchLibrary()">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
									</div>
									
								</div>
								<div>
									<button class="btn btn-sm btn-success btn-round"
										onclick="showCreateLibraryWin()">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<button class="btn btn-sm btn-warning btn-round"
										onclick="updateLibraryWin()">
										<i class="ace-icon fa fa-pencil-square-o bigger-125"></i> <b>编辑</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round" 
										onclick="removeLibrary()">
										<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
									</button>
								</div>
								<div>
									<table id="library_list"></table>
									<div id="library_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script id="setLibrary_template" type="text/html">
	<div id="setLibrary">
		<div class="well " style="margin-top:1px;">
			<form class="form-horizontal" role="form" id="add_library_frm">
				<div class="form-group">
					<label class="col-sm-3"><b>预加载包文件：</b></label>
					<div class="col-sm-9">
						<input type="file" id="upLibrary" name="upLibrary" class="form-control"  />
						<span id="percentage"></span>
						<br />
						<progress id="progressBar" value="0" max="100" hidden="true"> </progress>
					</div>
					<i class="glyphicon glyphicon-asterisk" style="font-size: 10px;color:#FF0000;top:7px"></i>
				</div>
				<div class="form-group">
					<label class="col-sm-3"><b>名称：</b></label>
					<div class="col-sm-9">
						<input id="addlibraryName" onblur='checkAddlibraryName();' type="text" class="form-control" placeholder=" 输入名称..."/>
					</div>
					<i class="glyphicon glyphicon-asterisk" style="font-size: 10px;color:#FF0000;top:7px"></i>
				</div>
				<div class="form-group">
					<label class="col-sm-3"><b>预加载包说明：</b></label>
					<div class="col-sm-9">
						<textarea id="libraryDescription" class="form-control" rows="3" placeholder="输入说明信息..."></textarea>
					</div>
				</div>
				<div class='form-group'>
					<div id="libraryPid" style="width: 100%;height:100%;display: none;">
						<div style="" id="libraryShowId">已经完成0%</div>
						<div class="progress">
							<div id="libraryProgressId" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
					</div>
				</div>
				<input type="hidden" id="libraryFilePath" name="libraryFilePath" value=""/>
			</form>
		</div>
	</div>
	</script>
</body>
</html>
