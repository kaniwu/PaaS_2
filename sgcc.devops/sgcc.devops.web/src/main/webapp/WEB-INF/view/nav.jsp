<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String pageIndex = request.getParameter("page_index");
String parentIndex = request.getParameter("parent_index");
pageIndex = null == pageIndex? "" : pageIndex;
parentIndex = null == parentIndex? "" : parentIndex;
%>
<%
String basePath = request.getContextPath()+"/";
%>
<script src="<%=basePath %>ace/assets/js/ace/ace.sidebar.js"></script>
<script src="<%=basePath %>ace/assets/js/ace/ace.submenu-1.js"></script>
<c:set var="authStr" value='${pagesAuth}'></c:set>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
	<div id="sidebar" class="sidebar responsive">
		<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
		</script>
		<ul class="nav nav-list">
			<c:if test="${fn:contains(authStr,'index')}">
			<li id="index">
				<a href="<%=basePath %>index.html"> 
					<i class="menu-icon fa fa-home home-icon"></i> 
					<span class="menu-text" style="font-size:13px"><b>平台首页</b> </span>
				</a>
			</li>
			</c:if>
<!-- 			<li id="manage_platform" class=""> -->
<!-- 				<a href="#" class="dropdown-toggle">  -->
<!-- 					<i class="menu-icon fa fa-globe"></i>  -->
<!-- 					<span class="menu-text"><b>平台集成</b> </span> -->
<!-- 					<b class="arrow fa fa-angle-down"></b> -->
<!-- 				</a> -->
<!-- 				<b class="arrow"></b> -->
<!-- 				<ul class="submenu"> -->
<!-- 					<li id="network_comm" class=""> -->
<%-- 						<a href="<%=basePath %>host/index.html"> --%>
<!-- 							<i class="menu-icon fa fa-exchange"></i> -->
<!-- 							<span class="menu-text"> <b>内外网通讯</b> </span> -->
<!-- 						</a> -->
<!-- 						<b class="arrow"></b> -->
<!-- 					</li> -->
<!-- 				</ul> -->
<!-- 			</li> -->
			<c:if test="${fn:contains(authStr,'manage_resource')}">
			<li id="manage_resource" class="">
				<a href="#" class="dropdown-toggle"> 
					<i class="menu-icon glyphicon glyphicon-th"></i> 
					<span class="menu-text"><b>资源管理</b> </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu">
					<c:if test="${fn:contains(authStr,'hostIndex')}">
						<li id="host_admin" class="">
							<a href="<%=basePath %>host/index.html">
								<i class="menu-icon fa fa-desktop"></i>
								<span class="menu-text"> <b>主机管理</b> </span>
							</a>
							<b class="arrow"></b>
						</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'hostComponentIndex')}">
						<li id="host_component" class="">
							<a href="<%=basePath %>hostComponent/index.html">
								<i class="menu-icon fa fa-desktop"></i>
								<span class="menu-text"> <b>主机组件管理</b> </span>
							</a>
							<b class="arrow"></b>
						</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'registryIndex')}">
					<li id="registry_admin"class="">
						<a href="<%=basePath %>registry/index.html" > 
							<i class="menu-icon fa fa-home"></i> 
							<span class="menu-text"><b>仓库管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'clusterIndex')}">
					<li id="cluster_admin" class="">
						<a href="<%=basePath %>cluster/index.html"> 
							<i class="menu-icon fa fa-globe"></i> 
							<span class="menu-text"><b>集群管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'imageIndex')}">
					<li id="image_admin" class="">
						<a href="<%=basePath %>image/index.html"> 
							<i class="menu-icon fa fa-camera"></i> 
							<span class="menu-text"><b>镜像管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'containerIndex')}">
					<li id="container_admin" class="">
						<a href="<%=basePath %>container/index.html"> 
							<i class="menu-icon fa fa-inbox"></i> 
							<span class="menu-text"><b>容器管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'componentIndex')}">
					<li id="component_admin" class="">
						<a href="<%=basePath %>component/index.html">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text"> <b>组件管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'libraryIndex')}">
					<li id="library_admin" class="">
						<a href="<%=basePath %>library/index.html">
							<i class="menu-icon fa fa-briefcase"></i>
							<span class="menu-text"> <b>预加载包管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
					<li id="server_room" class="hide">
						<a href="<%=basePath %>serverRoom/index.html">
							<i class="menu-icon fa fa-desktop"></i>
							<span class="menu-text"> <b>机房管理</b> </span>
						</a>
						<b class="arrow"></b>
					</li>
				</ul>
			</li>
			</c:if>
			<c:if test="${fn:contains(authStr,'manage_businessSystem')}">
			<li id="manage_businessSystem" class="">
				<a href="#" class="dropdown-toggle"> 
					<i class="menu-icon fa fa-cogs"></i> 
					<span class="menu-text"><b>系统部署</b> </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu">
<%-- 					<c:if test="${fn:contains(authStr,'hostIndex')}"> --%>
<!-- 						<li id="business_admin"> -->
<%-- 							<a href="<%=basePath %>business/index.html" >  --%>
<!-- 								<i class="menu-icon fa fa-folder-o"></i>  -->
<!-- 								<span class="menu-text"><b>业务系统</b> </span> -->
<!-- 							</a> -->
<!-- 							<b class="arrow"></b> -->
<!-- 						</li> -->
<%-- 					</c:if> --%>
					<c:if test="${fn:contains(authStr,'systemIndex')}">
					<li id="physical_admin">
						<a href="<%=basePath %>system/index.html" > 
							<i class="menu-icon fa fa-folder-o"></i> 
							<span class="menu-text"><b>物理系统</b></span>
						</a>
						<b class="arrow"></b>
					</li>
					</c:if>
				</ul>
			</li>
			</c:if>
			<c:if test="${fn:contains(authStr,'manage_log')}">
			<li id="manage_log" class="">
				<a href="#" class="dropdown-toggle"> 
					<i class="menu-icon fa fa-book"></i> 
					<span class="menu-text"><b>日志管理</b> </span>
					<b class="arrow fa fa-angle-down"></b>
				</a>
				<b class="arrow"></b>
				<ul class="submenu">
					<c:if test="${fn:contains(authStr,'logIndex')}">
						<li id="platform_log">
							<a href="<%=basePath %>log/index.html" > 
								<i class="menu-icon fa fa-folder-o"></i> 
								<span class="menu-text"><b>访问日志</b></span>
							</a>
							<b class="arrow"></b>
						</li>
					</c:if>
					<c:if test="${fn:contains(authStr,'logsIndex')}">
						<li id="platform_logs">
							<a href="<%=basePath %>logs/index.html" > 
								<i class="menu-icon fa fa-folder-o"></i> 
								<span class="menu-text"><b>运行日志</b></span>
							</a>
							<b class="arrow"></b>
						</li>
					</c:if>
				</ul>
			</li>
			</c:if>
			<!--权限管理-->
		<c:if test="${fn:contains(authStr,'manage_auth')}">
			<li class="manage_auth" id="casd"><a href="#" class="dropdown-toggle">
					<i class="menu-icon fa fa-users"></i> <span class="menu-text"><b>系统管理</b>
				</span> <b class="arrow fa fa-angle-down"></b>
			</a> <b class="arrow"></b>
				<ul class="submenu">
					<!-- 数据字典管理 -->
					<li id="parameter" class="">
						<a href="<%=basePath %>parameter/index.html" > 
							<i class="menu-icon fa fa-book"></i> 
							<span class="menu-text"><b>数据字典管理 </b> </span>
							<b class="arrow"></b>
						</a>
					</li>
					<!-- 环境管理 -->
					<li id="environment" class="">
						<a href="<%=basePath %>environment/index.html" > 
							<i class="menu-icon fa fa-book"></i> 
							<span class="menu-text"><b>环境管理 </b> </span>
							<b class="arrow"></b>
						</a>
					</li>
					<c:if test="${fn:contains(authStr,'userIndex')}">
						<li id="manage_user" class=""><a
							href='<%=basePath %>user/index.html'> <i
								class="menu-icon fa fa-user"></i> <span class="menu-text"><b>用户管理</b>
							</span>
						</a> <b class="arrow"></b></li>
					</c:if>
					<c:if test="${fn:contains(authStr,'roleIndex')}">
						<li id="manage_role" class=""><a
							href='<%=basePath %>role/index.html'> <i
								class="menu-icon fa fa-flag"></i> <span class="menu-text"><b>角色管理</b>
							</span>
						</a> <b class="arrow"></b></li>
					</c:if>
					<c:if test="${fn:contains(authStr,'authIndex')}">
						<li id="manage_right" class=""><a
							href='<%=basePath %>auth/index.html'> <i
								class="menu-icon fa fa-key"></i> <span class="menu-text"><b>权限管理</b>
							</span>
						</a> <b class="arrow"></b></li>
					</c:if>
				</ul></li>
		</c:if>
		</ul>
		<!-- 菜单伸缩按钮 -->
		<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
			<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
		</div>
		<script type="text/javascript">
			try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
			$(document).ready(function(){
				var page_index = "<%=pageIndex %>";
				var parent_index = "<%=parentIndex %>";
				if("" != page_index){
					$("#"+page_index).addClass('active');
				}
				if("" != parent_index){
					$("#"+parent_index).addClass('active');
				}
			});
		</script>
	</div>