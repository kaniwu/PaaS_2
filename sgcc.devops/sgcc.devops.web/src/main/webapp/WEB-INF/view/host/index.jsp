<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/host.js"></script>
<link rel="stylesheet" href="<%=basePath %>css/user/host.css" />
<jsp:include page="form.jsp"></jsp:include>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="host_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>主机管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row" class="col-xs-10">
							<div class="col-xs-12">
								<!-- 左侧树 -->
								<div class="col-sm-2 well well-sm" id="treeDiv" style="height: 503px;">
									<div>
										<div class="btn-group">
											<button data-toggle="dropdown" class="btn btn-sm btn-success btn-round">
													新增
													<i class="icon-angle-down icon-on-right"></i>
												</button>
											<ul class="dropdown-menu dropdown-primary dropdown-menu-left">
												<li id="addBrather">
													<a class="glyphicon" href="#" onclick="addLocationLevel(0)">新增同级</a>
												</li>
												<li id="addChild">
													<a class="glyphicon" href="#" onclick="addLocationLevel(1)">新增下级</a>
												</li>
											</ul>
										</div>
										<div class="btn-group">
											<button class="btn btn-sm btn-warning btn-round" style="width: 43px;height: 34px;"
												onclick="updateLocationLevel()"> <b>编辑</b>
											</button>
										</div>
										<div class="btn-group">
											<button class="btn btn-sm btn-danger btn-round" style="width: 43px;height: 34px;"
												onclick="deleteLocationLevel()"> <b>删除</b>
											</button>
										</div>
										</div>
									<div id="locationLevel" class="ztree"></div>
								</div>
								<div id="formdiv" class="col-sm-10">
									<div id="serverRoomManager">
										<c:if test="${fn:contains(authButton,'authList')}">
										<div class="well well-sm">
											<label class="col-sm-1 control-label no-padding" for="form-field-1-1" style="margin-top: 5px;margin-left: 1px;margin-right: 1px;"><strong>IP地址：</strong>
											</label>
											<div class="col-sm-2">
												<div class="select-group">
													<input id="ip" type="text" class="form-control" />
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding" for="form-field-1-1" style="margin-top: 5px;margin-left: 1px;margin-right: 1px;"><strong>所在集群：</strong>
											</label>
											<div class="col-sm-2">
												<div class="select-group">
													<select id="clusterName" class="form-control" >
														<option value="">请选择</option>
													</select>
												</div>
											</div>
											<label class="col-sm-1 control-label no-padding" for="form-field-1-1" style="margin-top: 5px;margin-left: 1px;margin-right: 1px;"><strong>主机类型：</strong>
											</label> 
											<div class="col-sm-2">
												<div class="select-group">
													<select id="hostType" class="form-control" >
														<option value="">请选择</option>
														<option value="0">swarm管理服务器</option>
														<option value="1">docker服务器</option>
														<option value="2">仓库主机</option>
														<option value="4">暂未分配</option>
													</select>
												</div>
											</div>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-sm btn-primary btn-round"
												onclick="searchHost()">
												<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
											</button>
											<div id="spinner" style="float: right; display: none;">
												<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
											</div>
											
										</div>
										</c:if>
									
										<div>
											<button class="btn btn-sm btn-success btn-round"
												onclick="showCreateHostWin()">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
											</button>
											<button class="btn btn-sm btn-success btn-round"
												onclick="showCreateGroupHostWin()">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>批量新增</b>
											</button>
											<button class="btn btn-sm btn-warning btn-round"
												onclick="updateHostWin()">
												<i class="ace-icon fa fa-pencil-square-o bigger-125"></i> <b>编辑</b>
											</button>
											<button class="btn btn-sm btn-danger btn-round" 
												onclick="removeHost()">
												<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
											</button>
											<button class="btn btn-sm btn-primary btn-round" 
												onclick="importHost()">
												<i class="ace-icon fa fa-arrow-down bigger-125"></i> <b>导入</b>
											</button>
											<div  style=" display: none;">
											<form action="importHost" method="post"
												enctype="multipart/form-data" id="importHostForm" >
												<input type="file" name="file" id="excelFile" onchange="excelFilefun()">
												<input type="hidden" name="locationId" id="locationId">
											</form>
											</div>
											<button class="btn btn-sm btn-primary btn-round" 
												onclick="exportHost()">
												<i class="ace-icon fa  fa-arrow-up bigger-125"></i> <b>导出</b>
											</button>
											<button class="btn btn-sm btn-success btn-round"
												onclick="showUpdateWin()">
												<i class="ace-icon fa fa-refresh bigger-125"></i> <b>组件升级安装</b>
											</button>
											<div  style="float: right; ">
												<button class="btn btn-sm btn-primary btn-round" 
												onclick="importHostExcel()">
												<i class="ace-icon fa  fa-arrow-down bigger-125"></i> <b>导入模板</b>
											</button>
											</div>
										</div>
										<div >
											<table id="host_list"></table>
											<div id="host_page"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="hostDetail">
<div class="well" style="margin-top:1px;width:100%">
		<form class="form-horizontal" role="form" id="update_host_frm">
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>主机名称：</b></div></label>
				<div class='col-sm-4'>
					<input type='text' readonly='readonly' value='{{hostName}}' class='form-control' />
				</div>
				<label class='col-sm-2'><div align='right'><b>IP地址：</b></div></label>
				<div class='col-sm-4'>
					<input type='text' readonly='readonly' value='{{hostIp}}' class='form-control' />
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>主机类型 ：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostType}}' class='form-control' />
				</div>
				<label class='col-sm-2'><div align='right'><b>主机状态 ：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostStatus}}' class='form-control' />
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>CPU ：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostCpu}}' class='form-control' />
				</div>
				<label class='col-sm-2'><div align='right'><b>内存：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostMem}}' class='form-control' />
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>内核版本 ：</b></div></label>
				<div class='col-sm-10'>
					<input  type='text' readonly='readonly' value='{{hostKernelVersion}}' class='form-control' />
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>创建人 ：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostCreator}}' class='form-control' />
				</div>
				<label class='col-sm-2'><div align='right'><b>创建时间 ：</b></div></label>
				<div class='col-sm-4'>
					<input  type='text' readonly='readonly' value='{{hostCreatetime}}' class='form-control' />
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>主机环境：</b></div></label>
				<div class='col-sm-10'>
					<select disabled='disabled' class='form-control' id='hostEnv' ></select>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>主机描述：</b></div></label>
				<div class='col-sm-10'>
					<textarea readonly='readonly' class='form-control'  rows='3' >{{hostDesc}}</textarea>
				</div>
			</div>
			<div class='form-group'>
				<label class='col-sm-2'><div align='right'><b>已安装组件：</b></div></label>
				<div class='col-sm-10' id='hostComTable'>
				</div>
			</div>
		</form>
	  </div>
</div>
	</script>
</body>
</html>
