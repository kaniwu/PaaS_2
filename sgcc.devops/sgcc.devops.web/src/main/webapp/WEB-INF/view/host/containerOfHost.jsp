<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/containerOfHost.js"></script>
<script type="text/javascript">
	// 						$(document).ready(function(){
	// 							var systemId = ${systemId};
	// 							alert(systemId)
	// 						});
</script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="host_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<input type="hidden" id="systemId" value="${systemId }">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i><a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>主机管理</b></li>
							<li class="active"><b>容器列表</b></li>
						</ul>
					</div>
					<input type="hidden" id="hostId" value="${hostId}">
					<input type="hidden" id="flag" value="${flag}">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<button class="btn btn-sm btn-primary btn-round"
										onclick="gobackpage()">
										<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round" id="clearButt"
										onclick="clearCon('${hostId}')">
										<i class="ace-icon glyphicon glyphicon-remove-circle bigger-125"></i> <b>清空并解绑</b>
									</button>
									<span class="text-muted" style="position: relative; left: 70px">
										<strong>主机IP : ${hostIp}</strong>
									</span>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
									</div>
								</div>
								<div>
									<table id="container_list"></table>
									<div id="container_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
