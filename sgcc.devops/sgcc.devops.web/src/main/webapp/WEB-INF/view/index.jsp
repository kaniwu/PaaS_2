<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<jsp:include page="js.jsp"></jsp:include>
		<script src="<%=basePath %>js/console/index.js"></script>
	</head>
	<body class="no-skin">
		<div id="index_index_body" class="index_index_body">
			<jsp:include page="header.jsp"></jsp:include>
			<div class="main-container" id="main-container">
				<script type="text/javascript">
					try{ace.settings.check('main-container' , 'fixed')}catch(e){}
				</script>
				<jsp:include page="nav.jsp">
					<jsp:param value="manage_dashboard" name="page_index"/>
				</jsp:include>		
				<div class="main-content">
					<div class="main-content-inner">
						<div class="breadcrumbs" id="breadcrumbs">
							<ul class="breadcrumb">
								<li>
									<i class="ace-icon fa fa-home home-icon"></i>
									<a href="<%=basePath %>index.html"><strong>首页</strong></a>
								</li>
							</ul>
						</div>
						<div class="page-content">
							<div class="row">
								<div class="col-sm-6" style="border-right: 1px dashed rgb(204, 204, 204);">
									<div class="widget-box" style="border:none">
										<div class="widget-header widget-header-flat widget-header-small" style="background:none">
											<h5 class="widget-title"><b>容器状态</b></h5>
										</div>
										<div class="widget-body">
											<div class="widget-main" style="padding : 0">
												<div id="dashboard_container_placeholder" style="height:200px"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="widget-box" style="border:none">
										<div class="widget-header widget-header-flat widget-header-small" style="background:none">
											<h5 class="widget-title"><b>主机类型</b></h5>
										</div>
										<div class="widget-body">
											<div class="widget-main" style="padding : 0">
												<div id="dashboard_host_placeholder" style="height:200px"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="widget-box" style="border:none">
										<div
											class="widget-header widget-header-flat widget-header-small" style="background:none">
											<h5 class="widget-title">
												<i class="ace-icon fa fa-signal"></i><b>统计信息</b>
											</h5>
										</div>
										<div class="widget-body">
											<div class="widget-main">
												<div class="clearfix">
													<div class="grid3">
														<a href="<%=basePath %>hostComponent/index.html" id="dashboard_bssSystem_num">
															<img src="img/dashboard_bss.png" width="80px" height="80px">
															<b>业务系统</b>
														</a>
													</div>
													<div class="grid3">
														<a href="<%=basePath %>system/index.html" id="dashboard_phySystem_num">
															<img src="img/dashboard_phy.png" width="80px" height="80px">
															<b>物理系统</b>
														</a>
													</div>
													<div class="grid3">
														<a href="<%=basePath %>host/index.html" id="dashboard_host_num">
															<img src="img/dashboard_host.png" width="80px" height="80px">
															<b>主机</b>
														</a>
													</div>
												</div>
												
												<div class="clearfix">
													<div class="grid3">
														<a href="<%=basePath %>container/index.html" id="dashboard_container_num">
															<img src="img/dashboard_container.png" width="80px" height="80px">
															<b>容器</b>
														</a>
													</div>
													<!-- fa-globe -->
													<div class="grid3">
														<a href="<%=basePath %>image/index.html" id="dashboard_image_num">
															<img src="img/dashboard_image.png" width="80px" height="80px">
															<b>镜像</b>
														</a>
													</div>
													<div class="grid3">
														<a href="javascript:void(0);" id="dashboard_lb_num">
															<img src="img/dashboard_ng.png" width="80px" height="80px">
															<b>负载均衡</b>
														</a>
													</div>
												</div>
												
												<div class="clearfix">
													<div class="grid3">
														<a href="<%=basePath %>cluster/index.html" id="dashboard_cluster_num">
															<img src="img/dashboard_cluster.png" width="80px" height="80px">
															<b>集群</b>
														</a>
													</div>
													<div class="grid3">
														<a href="<%=basePath %>registry/index.html" id="dashboard_registry_num">
															<img src="img/dashboard_registry.png" width="80px" height="80px">
															<b>仓库</b>
														</a>
													</div>
													<div class="grid3">
														<a href="<%=basePath %>component/index.html" id="dashboard_component_num">
															<img src="img/dashboard_com.png" width="80px" height="80px">
															<b>组件</b>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>