<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/registry.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="registry_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index"/>
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i><a href="<%=basePath %>index.html"><strong>首页</strong></a>
							</li>
							<li class="active"><b>仓库管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>仓库名称：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="registryName" type="text" class="form-control" />
										</div>
									</div>
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>负载地址：</strong>
									</label>
									<div class="col-sm-2">
										<div class="select-group">
											<input id="LbIp" type="text" class="form-control" />
										</div>
									</div>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchHost()">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
									</div>
								</div>
								<div>
									<button class="btn btn-sm btn-success btn-round"
										onclick="insertRegistry()">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<button class="btn btn-sm btn-warning btn-round"
										onclick="editRegistry()">
										<i class="ace-icon fa fa-pencil-square-o bigger-125"></i> <b>编辑</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round" 
										onclick="deleteRegistry()">
										<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
									</button>
								</div>
								<div>
									<table id="registry_list"></table>
									<div id="registry_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="registryLbDetails">
	<div style="margin-top:1px;" class="well">
		<form class="form-horizontal" role="form">
         <input type="hidden"  name="id" value="{{id}}" />
         <div class="form-group">
					<label class="col-sm-4"><b>仓库名称：</b></label>
                    <div class='col-sm-6'><input type="text"  style="width:100%; " name="registryName"  id="registryName" value="{{registryName}}" readonly="readonly"/></div>
         </div>
          <div class="form-group">
					<label class="col-sm-4"><b>负载：</b></label>
                    <div class='col-sm-6'><input type="radio" checked ="true"  name="lbType"  id="lbType" />
                   {{if lbType==2}}
					<b> &nbsp F5</b>
                    {{else}}
				   <b>&nbsp   nginx</b>
                      {{/if}}        
             </div>
         </div>  
         <div class="form-group">
                    {{if lbType==2}}
					<label class="col-sm-4"><b>选择F5：</b></label>
                    {{else}}
					<label class="col-sm-4"><b>选择nginx：</b></label>
                      {{/if}}
                    <div class='col-sm-6'><input type="text"  name="componentId" id="componentId" value="{{component_name}}" style="width:100%; " readonly="readonly"> </div>
         </div> 
         <div class="form-group">
                     {{if lbType==2}}
					<label class="col-sm-4"><b>对外Ip端口：</b></label>
                     {{else}}
					<label class="col-sm-4"><b>Ip端口：</b></label>
                       {{/if}}
                    <div class='col-sm-6'><input type="text"  name="lbPortip" id="lbPortip" value="{{lbIp}}:{{lbPort}}" style="width:100%; " readonly="readonly"></div>
         </div> 
          <div class="form-group">
					<label class="col-sm-4"><b>主机一：</b></label>
                    <div class='col-sm-6'><input type="text"  style="width:100%; " name="host_ipport"  id="host_ipport" value="{{host_ip}}:{{host_port}}" readonly="readonly"/></div>
         </div>  
          <div class="form-group">
					<label class="col-sm-4"><b>状态：</b></label>
                  {{if host_state==1}}
                  <div class='col-sm-3'>
                                                            正常   
                   </div>
                     <a class="btn btn-xs btn-success btn-round" onclick="stopRegistry('{{host_id}}',5)">
                     <i class="ace-icon fa fa-stop bigger-125"></i>
                     <b>停止</b></a> &nbsp
                   {{else if host_state==3}}
                   <div class='col-sm-3'>
                                                             停止   
                   </div>
                      <a class="btn btn-xs btn-success btn-round" onclick="startRegistry('{{host_id}}','{{id}}')">
                      <i class="ace-icon fa fa-refresh bigger-125"></i>
                      <b>启动</b></a> &nbsp
                      {{else}}
                      <div class='col-sm-3'>
                                                                             未知   
                  </div>
                      <a class="btn btn-xs btn-success btn-round" disabled="disabled">
                       <b> 无操作 </b></a> &nbsp
                          {{/if}}
         </div>  
          <div class="form-group">
					<label class="col-sm-4"><b>主机二：</b></label>
{{if null!=stand_id}}
                    <div class='col-sm-6'><input type="text"  style="width:100%; " name="stand_ipport"  id="stand_ipport" value="{{stand_ip}}:{{stand_port}}" readonly="readonly"/></div>
         </div> 
          <div class="form-group">
					<label class="col-sm-4"><b>状态：</b></label>
                    {{if stand_state==1}}
                  <div class='col-sm-3'>
                                                            正常   
                   </div>
                     <a class="btn btn-xs btn-success btn-round" onclick="stopRegistry('{{stand_id}}',5)">
                                       <i class="ace-icon fa fa-stop bigger-125"></i>
                                        <b>停止</b></a> &nbsp
                   {{else if stand_state==3}}
                     <div class='col-sm-3'>
                                                                         停止   
                    </div>
                      <a class="btn btn-xs btn-success btn-round" onclick="startRegistry('{{stand_id}}')">
                      <i class="ace-icon fa fa-refresh bigger-125"></i>
                      <b>启动</b></a> &nbsp
                      {{else}}
                           <div class='col-sm-3'>
                                                                         未知   
                    </div>
                      <a class="btn btn-xs btn-success btn-round" disabled="disabled">
                       <b> 无操作 </b></a> &nbsp
                          {{/if}}
{{else}}
<div class='col-sm-6'>没有备用主机</div>
{{/if}}
         </div>  
         <div class="form-group">
					<label class="col-sm-4"><b>描述：</b></label>
                    <div class='col-sm-6'><textarea rows="5" cols="32" name="registryDesc" id="registryDesc" disabled="disabled">{{registryDesc}}</textarea></div>
         </div> 
			</form>
    </div>
	</script>
	<script type="text/html" id="registryUpdate">
         <div class='well ' style='margin-top:1px;'>
        <form class='form-horizontal' role='form' id='add_item_frm'>
        <div class='form-group'>
        <label class='col-sm-4'><div align='right'><b>仓库名称：</b></div></label>
        <div class='col-sm-6'>
        <input placeholder=" 请填写仓库名称" id="registry_name" type='text' class="form-control"  value="{{registryName}}" '/>
        </div>
        <i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>
        </div>
       <div class="form-group">
					<label class="col-sm-4"><b>负载：</b></label>
                    <div class='col-sm-6'><input type="radio" checked ="true"  name="lbType"  id="lbType" />
                   {{if lbType==2}}
					<b> &nbsp F5</b>
                    {{else}}
				   <b>&nbsp   nginx</b>
                      {{/if}}        
             </div>
         </div>  
         <div class="form-group">
                    {{if lbType==2}}
					<label class="col-sm-4"><b>选择F5：</b></label>
                    {{else}}
					<label class="col-sm-4"><b>选择nginx：</b></label>
                      {{/if}}
                    <div class='col-sm-6'><input type="text"  name="componentId" id="componentId" value="{{component_name}}" style="width:100%;" readonly="readonly"></div>
         </div> 
         <div class="form-group">
                     {{if lbType==2}}
					<label class="col-sm-4"><b>对外Ip端口：</b></label>
                     {{else}}
					<label class="col-sm-4"><b>Ip端口：</b></label>
                       {{/if}}
                    <div class='col-sm-6'><input type="text"  name="lbPortip" id="lbPortip" value="{{lbIp}}:{{lbPort}}" style="width:100%;" readonly="readonly"></div>
         </div> 
    
        <div class='form-group'>
        <label class='col-sm-4'><div align='right'><b>负载域名：</b></div></label>
        <div class='col-sm-6'>
        <input placeholder=" 请填写负载域名" id="load_domain" type='text' class="form-control" value="{{domainName}}" />
        </div>
        <i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>
        </div>
        <div class='form-group'>
        <label class='col-sm-1'></label>
        <label class='col-sm-3'><div align='right'><b>主机一：</b></div></label>
        <div class='col-sm-6'>
        <input type="text"  style="width:100%; " name="host_ip"  id="host_ip" value="{{host_ip}}" readonly="readonly"/>
        </div>
        <div class='col-sm-2'>
        <input placeholder=" 请输入端口号" id="host_port" type='number' class="form-control" value="{{host_port}}" readonly="readonly"/>
        </div>
        </div>
        <div class='form-group'>
        <label class='col-sm-1'><div align='right'>
                      <a class="btn btn-xs btn-success btn-round" onclick="changeRegistry('{{stand_id}}',1,'{{stand_ip}}','{{stand_port}}','{{id}}')">
                      <i class="ace-icon fa fa-refresh bigger-125"></i>
                      <b>更改</b></a></div></label>
        <label class='col-sm-3'><div align='right'><b>主机二：</b></div></label>
        <div class='col-sm-6'>
        <input type="text"  style="width:100%; " name="stand_ipport"  id="stand_ipport" value="{{stand_ip}}" readonly="readonly"/>
        </div>
        <div class='col-sm-2'>
        <input  id="standHost_port" type='number' class="form-control" value="{{stand_port}}" readonly="readonly"/>
        </div>
        </div>
        <div class='form-group'>
        <label class='col-sm-4'><div align='right'><b>仓库描述：</b></div></label>
        <div class='col-sm-6'>
        <textarea placeholder="请输入仓库的描述信息"  id="registry_desc" name="registry_desc" onblur="validToolObj.length('#registry_desc',200)" class="form-control" rows="3">{{registryDesc}}</textarea>
        </div>
        </div>
        </div>
        </form>
    </script>
    <script type="text/html" id="changeRegistry">
<div style="margin-top:1px;" class="well">
<form class="form-horizontal" role="form">
{{if "" != change_ip}}
         <div class='form-group'>
        <label class='col-sm-3'><div align='right'><b>当前主机:</b></div></label>
         <div class='col-sm-7'><input type="text" class="form-control"   value="{{ip}}" readonly="readonly"/></div>
         <div class='col-sm-2'><input   type='number' class="form-control"  value="{{port}}" readonly="readonly"/></div>
        </div>
{{/if}}
        <div class='form-group'>
        <label class='col-sm-3'><div align='right'><b>更改主机：</b></div></label>
        <div class='col-sm-7'>
        <select id='change_ip'  class='form-control'>
        <option value='0'>请选择主机</option>
        </select>
        </div>
        <div class='col-sm-2'>
        <input placeholder=" 请输入端口号" id="change_port" type='number' class="form-control" value='5000'/>
        </div>
        </div>
 <div class='form-group' id='registryIns' >
 <label class='col-sm-3'><div align='right'><b>docker安装：</b></div></label>
	      <div class='col-sm-7'>
	       	<select id='dockerIns_select' name='dockerIns_select' class='form-control' >
		      <option value="0">自动选择</option>
			  </select>
        	</div>
        </div>
        <div class='form-group' id='registryIns' >
		    <label class='col-sm-3'><div align='right'><b>registry安装：</b></div></label>
		      <div class='col-sm-7'>
		       	<select id='registryIns_select' name='registryIns_select' class='form-control' >
			      <option value="0">自动选择</option>
				  </select>"+
	        	</div>
	    </div>
</form>
</div>
    </script>

</body>
</html>
