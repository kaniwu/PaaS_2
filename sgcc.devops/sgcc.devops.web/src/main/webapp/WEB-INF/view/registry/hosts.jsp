<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/registryOfHost.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="registry_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i><a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>仓库管理</b></li>
							<li class="active"><b>编辑仓库主机</b></li>
						</ul>
					</div>
					<input type="hidden" id="lbId" value="${lbId }">
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<button class="btn btn-sm btn-primary btn-round"
										onclick="gobackpage()">
										<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
									</button>
									<span class="text-muted" style="position: relative; left: 70px">
										<strong id="registry_name">仓库名称 : ${registryName }</strong>
									</span>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
									</div>
								</div>
								<div>
									<button class="btn btn-sm btn-success btn-round"
										onclick="insertHost()">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round" 
										onclick="deleteHost()">
										<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
									</button>
								</div>
								<div>
									<table id="registry_host_list"></table>
									<div id="registry_host_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
