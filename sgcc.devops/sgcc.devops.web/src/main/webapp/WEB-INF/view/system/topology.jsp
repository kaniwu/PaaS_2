<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>

<link rel="stylesheet" href="<%=basePath %>ace/assets/js/cytoscape/cytoscape.cxtmenu.css" />
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape.min.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/beyc-cxtmenu.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape-qtip.js"></script>

<script src="<%=basePath %>js/console/topology.js"></script>
<script type="text/javascript">
var topologyObj = undefined;
$(function() {
	topologyObj = new topology();
});
</script>
<style type="text/css">
body{
background-color:white;
}
</style>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<input id="systemId" type="hidden" name="systemId" value="${systemId}">
		<input id="conCount" name="conCount" type="hidden">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="physical_admin" name="page_index" />
				<jsp:param value="manage_businessSystem" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<input type="hidden" id="deployId" value="${deployId}">
				<input type="hidden" id="systemId" value="${systemId}">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a href="<%=basePath %>index.html"><strong>首页</strong></a>
							</li>
							<li class="active"><b>系统部署</b></li>
							<li class="active"><b>监控查看</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="well well-sm">
							<button class="btn btn-sm btn-primary btn-round" onclick="gobackpage();">
								<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
							</button>
							<button class="btn btn-sm btn-success btn-round" onclick="topologyObj.addHtmlFunction()" id="addContainer">
								<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增容器</b>
							</button>
							<span class="text-muted" style="position: relative; left: 70px">
										<strong>系统名称 : ${systemName }</strong>
									</span>
							<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>请稍等...
									</div>
						</div>
						<div id="canvasup" class="col-sm-12" style="height:460px;padding-left:0px;padding-right:0px;"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="modal-logs" class="modal" data-backdrop="static">
		<div class="modal-dialog"
			style="width: 50%; margin-top: 10px; margin-bottom: 10px;">
			<div class="modal-content">
				<div class="modal-header">
					<button aria-hidden="true" data-dismiss="modal" onclick="cleanLogs()"
						class="bootbox-close-button close" type="button">×</button>
					<h4 class="modal-title">
						<b id="conTempName"></b>
					</h4>
				</div>
				<div class="modal-body " style="height: 450px; overflow-y: scroll;">
					<div id="logList">
					</div>
					<div id="logspinner" style="display: none;width: 100%; f" align="center">
						<i class="ace-icon fa fa-spinner fa-spin blue bigger-225" ></i>请稍等...
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
