<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/system.js"></script>
<script src="<%=basePath %>js/task/rollbackTask.js"></script>
<script type="text/javascript">
	// 						$(document).ready(function(){
	// 							var systemId = ${systemId};
	// 							alert(systemId)
	// 						});
</script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="physical_admin" name="page_index" />
				<jsp:param value="manage_businessSystem" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<input type="hidden" id="systemId" value="${systemId }">
				<input type="hidden" id="systemName" value="${systemName }">
				 
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i><a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>系统部署</b></li>
							<li class="active"><b>基础部署版本列表</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<button class="btn btn-sm btn-primary btn-round"
										onclick="gobackpage()">
										<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
									</button>
									<span class="text-muted" style="position: relative; left: 70px">
										<strong>系统名称 : ${systemName }</strong>
									</span>
								</div>
								<div>
									<table id="delpoy_list"></table>
									<div id="deploy_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
