<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored ="false" %>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<link rel="stylesheet" href="<%=basePath %>ace/assets/css/bootstrap-datetimepicker.css" />
<link rel="stylesheet" href="<%=basePath %>ace/assets/js/cytoscape/cytoscape.cxtmenu.css" />
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape.min.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape-supportimages.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/beyc-cxtmenu.js"></script>
<script src="<%=basePath %>ace/assets/js/cytoscape/cytoscape-qtip.js"></script>
<script src="<%=basePath %>js/task/task.js"></script>
<script src="<%=basePath %>ace/assets/js/date-time/moment.min.js"></script>
<script src="<%=basePath %>js/console/arrangement.js"></script>
<script src="<%=basePath %>ace/assets/js/jquery-ui.custom.min.js"></script>
<script src="<%=basePath %>ace/assets/js/date-time/bootstrap-datetimepicker.min.js"></script>
<style type="text/css">
body{
background-color:white;
}
.formpadding{
	padding: 0em;
}
</style>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="physical_admin" name="page_index" />
				<jsp:param value="manage_businessSystem" name="parent_index" />
			</jsp:include>
			<div class="main-content">
			<input type="hidden" id="systemName" name="systemName" value="${systemName}" readonly="readonly" class='form-control' />
			<input type="hidden" id="systemId" value="${systemId }">
			<input type="hidden" id="deployId" value="${deployId }">
			<input type="hidden" id="driverId">
			<input type="hidden" id="timestamp">
			<input type="hidden" id="newVersion"  name="newVersion" value="false" />
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i><a href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><a href="<%=basePath %>system/index.html"><b>系统部署</b></a></li>
							<li class="active"><b>编排部署</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div id="canvasup" class="col-sm-6" style="height:100%;background-color:#f5f5f5;padding-left:0px;padding-right:0px;"></div>
						<div id="form_module" class="col-sm-6" style="height:100%;background-color:#eff3f8;padding-top:10px;margin-left:0px;float: right;
							border-left-style: solid;border-left-color:white;padding-right: 15px">
							<form id="arrangement_form" role="form" class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-3" style="padding: 0em;">
										<b>系统版本：</b>
									</label>
									<div class="col-sm-3" style="padding: 0em;">
										<input type="text" class="form-control" id="arrangement_deployVersion" name="arrangement_deployVersion" readonly="readonly">
									</div>
								 	<label class="col-sm-2" style="padding: 0em;">
									 	<i class='glyphicon glyphicon-asterisk' style='font-size: 1px;color:#FF0000;'></i>
									 	<b>应用版本：</b>
								 	</label>
									<div class="col-sm-3" style="padding: 0em;">
										<select class='form-control' name="arrangement_systemAppId" id="arrangement_systemAppId" onchange="versionSet();">
										</select>
									</div> 
								</div>
								<div class="form-group">
									<div class="item">
										<label class="col-sm-3" style="padding: 0em;">
											<i class='glyphicon glyphicon-asterisk' style='font-size: 1px;color:#FF0000;'></i>
											<b>分布位置级别：</b>
										</label>
										<div class="col-sm-3" style="padding: 0em;">
											<select class='form-control' name="arrangement_location_leavel" id="arrangement_location_leavel">
											</select>
										</div>
										<label class='col-sm-2' style="padding: 0em;">
											<i class='glyphicon glyphicon-asterisk' style='font-size: 1px;color:#FF0000;'></i><b>分布策略：</b>
										</label>
										<div class="col-sm-4" style="padding: 0em;">
											<div style="padding-top: 3px;" class="radio-inline">
												<label>
											      <input type="radio" name="location_stragegy" id="location_stragegy_1"
											         value="1" checked="checked"> <b>资源最优</b>
											   </label>
											</div>
											<div style="padding-top: 3px;" class="radio-inline">
												<label>
											      <input type="radio" name="location_stragegy" id="location_stragegy_2"
											         value="2"> <b>随机分布</b>
											   </label>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-3" style="padding: 0em;">
										<b>目标集群：</b>
									</label>
									<div class="col-sm-3" style="padding: 0em;">
										<select name="arrangement_cluster" id="arrangement_cluster" 
											onchange="checkTargetCluster();" class="form-control"></select>
									</div>
									<label class="col-sm-2" style="padding: 0em;">
										<b>基础镜像：</b>
									</label>
									<div class="col-sm-3" style="padding: 0em;">
										<select name="arrangement_baseImageId" id="arrangement_baseImageId" 
										 	onchange="checkarrBaseImageId();" class="form-control"></select>
									</div>
								</div>
								<div class='form-group'>
									<label class="col-sm-3" style="padding: 0em;">
										<i class='glyphicon glyphicon-asterisk' style='font-size: 1px;color:#FF0000;'></i>
										<b>ContentPath：</b>
									</label>
									<div class="col-sm-3" style="padding: 0em;">
										<input type="text" id="contentPath" name="contentPath"
											 onblur="checkContentPath()"  onchange="versionSet();" class='form-control' />
									</div>
									<label class="col-sm-2" style="padding: 0em;">
										<b>版本切换：</b>
									</label>
									<div class="col-sm-4" style="padding: 0em;">
										<div style="padding-top: 3px;" class="radio-inline disabled">
											<label>
										      <input type="radio" value="2" name="arrangement_version" id="optionsRadios1" checked="checked"> <b>自动</b>
										   </label>
										</div>
										<div style="padding-top: 3px;" class="radio-inline">
											<label>
										      <input type="radio" value="1" name="arrangement_version" id="optionsRadios2"> <b>手动</b>
										   </label>
										</div>
									</div>
								</div>
								<div class="form-group" style='margin-bottom: 5px;' id="timerDiver">
									<label class="col-sm-3" style="padding: 0em;">
										<b>是否定时发布：</b>
									</label>
									<div class="col-sm-3" style="padding: 0em;">
										<select class='form-control' id="regularlyPublish" name="regularlyPublish">
											<option value="0">否</option>
											<option value="1">是</option>
										</select>
									</div>
									<label class='col-sm-2 formpadding'>
										<b>发布时间：</b>
									</label>
									<div class="col-sm-4" style="padding: 0em;">
										<div class="input-group">
											<input id="publishTime" type="text" class="form-control"/>
											<span class="input-group-addon">
												<i class="fa fa-clock-o bigger-110"></i>
											</span>
										</div>
									</div>
								</div>
								
								<div class='form-group' style='margin-bottom: 5px;'>
									<div class="item" align="right">
										<div id="deployFiles" style="list-style-type: none; margin: 0px 0px 0px 0px;">
										</div>
									</div>
								</div>
								<div class="form-group" style='margin-bottom: 5px;' id="DNSInfo">
									<label class="col-sm-3 formpadding">
										<b>CPU占比：</b>
									</label>
									<div class="col-sm-2 formpadding" >
										<input type="text" id="cpu" name="cpu" value="" placeholder="cpu" onblur="checkCPU()"
											class='form-control' />
									</div>
									<div class='col-sm-1' align="left" style="top:7px">
										<b>%</b>
									</div>
									<label class="col-sm-3 formpadding">
										<b>内存上限：</b>
									</label>
									<div class="col-sm-2  formpadding" >
										<input type="text" id="memery" name="memery" value="" placeholder="内存" onblur="checkMemery()"
											class='form-control' />
									</div>
									<div class='col-sm-1' align="left" style="top:7px">
										<b>G</b>
									</div>
								</div>
								<div class='form-group' style='margin-bottom: 5px;'>
									<div class="item">
										<label class="col-sm-3 formpadding">
											<b>JVM最大内存：</b>
										</label>
										<div class="col-sm-2  formpadding" >
											<input type="number" id="Xmxmax" name="Xmxmax" value="" onblur="checkXmxmax()"  onchange="versionSet();"
												class='form-control' />
										</div>
										<div class='col-sm-1' align="left" style="top:7px">
											<b>M</b>
										</div>
									</div>
									<div class="item">
										<label class="col-sm-3 formpadding">
											<b>JVM最小内存：</b>
										</label>
										<div class="col-sm-2  formpadding" >
											<input type="number" id="Xmxmin" name="Xmxmin" value="" onblur="checkXmxmin()"  onchange="versionSet();"
												class='form-control' />
										</div>
										<div class='col-sm-1' align="left" style="top:7px">
											<b>M</b>
										</div>
									</div>
								</div>
								<div class="form-group hide">
									<label class="col-sm-5">
										<b>日志路径：</b>
									</label>
									<div class="col-sm-7">
										<input type="text" id="appLogPath" name="appLogPath" class="form-control" />
									</div>
								</div>
								<div class="form-group hide">
									<label class="col-sm-5">
										<b>反向通道：</b>
									</label>
									<div class="col-sm-7">
										<input type="checkbox" name="arrangement_fxtdEnable">
										<span class="lbl" style="margin-left: 5px"><b>开启</b></span>
									</div>
								</div>
								<div class='form-group' style='margin-bottom: 5px;'>
									<div class="item">
										<label class="col-sm-3" style="padding: 0em;" for='DNSEnable'>
											<b>启用DNS：</b>
										</label>
										<div class="col-sm-3" style="padding: 0em;">
											<input type="checkbox" id="DNSEnable" name="DNSEnable" />
										</div>
										<div  id="dnsDiv" class="col-sm-6" style="display: none;">
											<label class="col-sm-3" style="padding: 0em;">
												<b>域名：</b>
											</label>
											<div class="col-sm-8" style="padding: 0em;">
												<input type="text" id="domainName_" name="domainName_" placeholder="域名" 
													onblur="checkDomainName()" class='form-control'/>
											</div>
										</div>
									</div>
								</div>
								<div style=" height:2px; width:100%;background:#FFFFFF; overflow:hidden;margin-bottom: 5px"></div>
								<div class="form-group" style='margin-bottom: 5px;' id="DNSInfo">
									<div class="item">
										<label class="col-sm-2 formpadding">
											<b>校验连接：</b>
										</label>
										<div class="col-sm-2" style="padding-right: 1px">
											<select id="http" name="http" class='form-control'>
												<option value="http">http</option>
												<option value="https">https</option>
											</select>
										</div>
										<div class="col-sm-2" style="padding-left: 1px;padding-right: 1px">
											<input type="text" id="domainName" name="domainName" placeholder="域名" class='form-control' readonly="readonly"/>
										</div>
										<div class="col-sm-2" style="padding-left: 1px;padding-right: 1px">
											<input type="number" id="port" name="port" value="" placeholder="port" class='form-control' readonly="readonly"/>
										</div>
										<div class="col-sm-2" style="padding-left: 1px;padding-right: 1px">
											<input type="text" id="appName" name="appName" value="" placeholder="应用名"  class='form-control' readonly="readonly"/>
										</div>
										<div class="col-sm-2" style="padding-left: 1px;">
											<input type="text" id="url" name="url" value="" placeholder="校验连接"  class='form-control'/>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2">
										<b>Dockerfile：</b>
									</label>
									<div class="col-sm-10">
										<textarea id="Dockerfile" name="Dockerfile" onblur="validToolObj.length('#Dockerfile',4000)"  onchange="versionSet();" class="form-control" rows="3"></textarea>
									</div>
								</div>
								
								<div class="modal-footer" style="padding: 0em;">
									<!-- <div id="pid" style="width: 100%;height:100%;float: left;display: none;">
										<div style="" id="showId">已经完成0%</div>
										<div class="progress">
											<div id="progressid"
												class="progress-bar progress-bar-success progress-bar-striped"
												role="progressbar" aria-valuenow="40" aria-valuemin="0"
												aria-valuemax="100" style="width: 0%;"></div>
										</div>
									</div> -->
									<button class="btn btn-sm btn-danger btn-round" type="button" id="arrangement_create" disabled="disabled">
										<i class="ace-icon fa fa-floppy-o bigger-125"></i><b>部署</b>
									</button>
									<a class="btn btn-sm btn-warning btn-round" type="button" href="<%=basePath %>system/index.html" onclick="disconnect();">
										<i class="ace-icon fa fa-times gray bigger-125"></i><b>取消</b>
									</a>
								</div>
							</form>
						</div>
<!-- 						<div id="testProgress" class="col-xs-10 col-offset-1"></div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
