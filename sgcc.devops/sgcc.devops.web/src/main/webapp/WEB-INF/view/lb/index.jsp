<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<jsp:include page="../js.jsp"></jsp:include>
		<link rel="stylesheet" href="<%=basePath %>css/user/loadbalance.css" />
		<script src="<%=basePath %>js/console/lb.js"></script>
	</head>
	<body class="no-skin">
		<div id="index_index_body" class="index_index_body">
			<jsp:include page="../header.jsp"></jsp:include>
			<div class="main-container" id="main-container">
				<script type="text/javascript">
					try{ace.settings.check('main-container' , 'fixed')}catch(e){}
				</script>
				<jsp:include page="../nav.jsp">
					<jsp:param value="lb_admin" name="page_index"/>
				</jsp:include>		
				<div class="main-content">
					<div class="main-content-inner">
						<div class="breadcrumbs" id="breadcrumbs">
							<ul class="breadcrumb">
								<li>
									<i class="ace-icon fa fa-home home-icon"></i>
									<a href="<%=basePath %>index.html"><strong>首页</strong></a>
								</li>
								<li class="active"><b>负载均衡</b></li>
							</ul>
						</div>
						<div class="page-content">
							<div class="row">
								<div class="col-xs-12">
									<div class="well well-sm">
										<button class="btn btn-sm btn-primary" onclick="createBalance()"> 
											<i class="ace-icon fa fa-cogs bigger-125"></i>
											<b>添加负载</b>
										</button>
										<div class="btn-group">
											<button data-toggle="dropdown" class="btn btn-primary btn-sm dropdown-toggle">
												<i class="ace-icon fa fa-wrench  bigger-110 icon-only"></i>
												<b>更多操作</b>
												<i class="ace-icon fa fa-angle-down icon-on-right"></i>
											</button>
											<ul class="dropdown-menu dropdown-primary dropdown-menu-right">
												<li><a class="btn-forbidden" id="addApp" onclick="addApp()"><span
														class="glyphicon glyphicon-plus"></span>&nbsp;添加应用</a></li>
												<li><a class="btn-forbidden" id="reload"><span
														class="glyphicon glyphicon-repeat"></span>&nbsp;动态更新</a></li>
												<li><a class="btn-forbidden" id="remove"><span
														class="glyphicon glyphicon-trash"></span>&nbsp;批量删除</a></li>
											</ul>
										</div>
									</div>
									<div>
										<table id="lb_list"></table>
										<div id="lb_page"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Add load balance -->
				<div id="edit_balance_win" class="hide">
					<div class='well ' style='margin-top:1px;'>
						<form class='form-horizontal' role='form' id='add_balance_frm'>
					  		<div class='form-group'>
					  			<label class='col-sm-3' style="text-align:right;"><b>负载均衡名称：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
					  				<input id='balance_id' type='hidden'/>
					  				<input id="balance_name" type='text' class="form-control" placeholder="输入名称..."/>
		    	      			</div>
		    	      		</div>
		    	      		<div class='form-group'>
					  			<label class='col-sm-3' style="text-align:right;"><b>主服务器：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
		    	      				<select id='main_host' style="width:100%">
		    	      					<option value="0">选择服务器</option>
		    	      				</select>
		    	      			</div>
		    	      		</div>
		    	      		<div class='form-group'>
		    	      			<label class='col-sm-3' style="text-align:right;"><b>配置文件位置：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
		    	      				<input id="main_conf" type='text' class="form-control" placeholder="输入配置文件位置：例如:/home/..."/>
		    	      			</div>
		    	      		</div>
		    	      		<div class='form-group'>
					  			<label class='col-sm-3' style="text-align:right;"><b>备用服务器：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
		    	      				<select id='backup_host' style="width:100%">
		    	      					<option value="0">选择服务器</option>
		    	      				</select>
		    	      			</div>
		    	      		</div>
		    	      		<div class='form-group'>
		    	      			<label class='col-sm-3' style="text-align:right;"><b>配置文件位置：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
		    	      				<input id="backup_conf" type='text' class="form-control" placeholder="输入配置文件位置：例如:/home/..."/>
		    	      			</div>
		    	      		</div>
				      		<div class='form-group'>
				      			<label class='col-sm-3' style="text-align:right;"><b>描述信息：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
		    	      				<textarea id="balance_desc" class="form-control" rows="3" placeholder="输入描述信息..."></textarea>
		    	      			</div>
		    	      		</div>
		    	      	</form>
		    	      </div>
				</div>
				<!-- Add application tp lb -->
				<div id="add_application" class="hide">
					<div class='well ' style='margin-top:1px;'>
						<form class='form-horizontal' role='form' id='add_application_frm'>
					  		<div class='form-group'>
					  			<label class='col-sm-3' style="text-align:right;"><b>应用名称：</b></label>
					  			<div class='col-sm-9' style="margin-left:-20px;">
					  				<select class="dropdown-select" id="app_select" name="app_select" style="width:81%">
										<option value="0">请选择应用</option>
							    	</select>
		    	      			</div>
		    	      		</div>
		    	      	</form>
		    	      </div>
				</div>
	</body>
</html>
