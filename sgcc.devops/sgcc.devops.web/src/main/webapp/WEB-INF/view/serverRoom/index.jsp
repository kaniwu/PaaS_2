<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="<%=basePath %>img/title_cloud.png" />
<title>机房管理</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<link rel="stylesheet" href="<%=basePath %>css/user/host.css" />
<script src="<%=basePath %>js/console/serverRoom.js"></script>
<c:set var="authButton" value='${buttonsAuth}'></c:set>

</head>
<body class="no-skin">
	<jsp:include page="../header.jsp"></jsp:include>
	<div class="main-container" id="main-container">
		<script type="text/javascript">
			try {
				ace.settings.check('main-container', 'fixed')
			} catch (e) {
			}
		</script>
		<jsp:include page="../nav.jsp">
			<jsp:param value="server_room" name="page_index" />
			<jsp:param value="manage_resource" name="parent_index" />
		</jsp:include>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a
							href="<%=basePath %>index.html"><strong>首页</strong></a></li>
						<li class="active"><b>机房管理</b></li>
					</ul>
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12" style="height: 100%">
							<div id="roleAuthTree" class="ztree col-sm-3 well well-sm"></div>
							<div id="formdiv" class="col-sm-9"></div>
							<div id="formdiv" class="col-sm-9">
								<div class="page-content" id="serverRoomManager" style="display: none">
									<div class="page-header">
										<h1>
											机房管理 
											<small>
												<i class="icon-double-angle-right"></i>
												SERVER ROOM MANAGER
											</small>
										</h1>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 widget-container-span" >
											<button class="btn btn-sm btn-success btn-round" 
												 onclick="showCreateRoomManager()">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>添加</b>
											</button>
													</div>
										<div style="margin-top: 60px;margin-left: 13px" >
											<table id="serverRoom_list"></table>
											<div id="serverRoom_page"></div>
										</div>
									</div>
								</div>
								<div class="page-content" id="rackManager" style="display: none">
									<div class="page-header">
										<h1>
											<span id="roomId">机房一</span>/ 机架管理
											<small>
												<i class="icon-double-angle-right"></i>
												RACK MANAGER
											</small>
										</h1>
									</div>
									<div class="row">
										<div class="col-xs-12 col-sm-12 widget-container-span">
											<button class="btn btn-sm btn-success btn-round" onclick="showCreateRack()">
												<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>添加</b>
											</button>
													</div>
										<div  style="margin-top: 60px;margin-left: 13px">
											<table id="rack_list"></table>
											<div id="rack_page"></div>
										</div>
									</div>
								</div> 
								</div> 
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="modal fade" id="addRoomModal" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">添加机房</h4>
							</div>
							<div class="modal-body">
								<div class="well" style="margin-top: 1px;">
									<form class='form-horizontal' role='form' id='create_user_form'>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机房名称：</b></label>
											<div class='col-sm-9'>
												<input id="user_name" name='user_name' type='text'
													class="form-control" placeholder="只能包含中文、英文、数字、下划线..." />
											</div>
										</div>

										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机房备注：</b></label>
											<div class='col-sm-9'>
												<input id="user_mail" name='user_mail' type='text'
													class="form-control" placeholder="字数限制2000字..." />
											</div>
										</div>
									</form>
								</div>
		
							</div>
							<div class="modal-footer">
								<button id="cancel" type="button" class="btn btn-round btn-danger" >
									取消</button>
								<button id="submit" type="button"
									class="btn btn-round btn-success">提交</button>
							</div>
						</div>
					</div>
				</div> -->
				<div class="modal fade" id="editeRoomModal" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">修改机房信息</h4>
							</div>
							<div class="modal-body">
								<div class="well" style="margin-top: 1px;">
									<form class='form-horizontal' role='form' id='create_user_form'>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机房名称：</b></label>
											<div class='col-sm-9'>
												<input id="user_name" name='user_name' type='text' value="机房一"
													class="form-control" placeholder="只能包含中文、英文、数字、下划线..." />
											</div>
										</div>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机房备注：</b></label>
											<div class='col-sm-9'>
												<input id="user_mail" name='user_mail' type='text' value="机房备注息"
													class="form-control" placeholder="字数限制2000字..." />
											</div>
										</div>
									</form>
								</div>
		
							</div>
							<div class="modal-footer">
								<button id="cancel" type="button" class="btn btn-round btn-danger">
									取消</button>
								<button id="submit" type="button"
									class="btn btn-round btn-success">提交</button>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="modal fade" id="addRackModal" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">添加机架</h4>
							</div>
							<div class="modal-body">
								<div class="well" style="margin-top: 1px;">
									<form class='form-horizontal' role='form' id='create_user_form'>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机架名称：</b></label>
											<div class='col-sm-9'>
												<input id="user_name" name='user_name' type='text'
													class="form-control" placeholder="只能包含中文、英文、数字、下划线..." />
											</div>
										</div>
										<div class='form-group'>
											<label class='col-sm-3'><b>登录密码：</b></label>
											<div class='col-sm-9'>
												<input id="user_pass" name='user_pass' type='password'
													class="form-control" placeholder="输入密码..." />
											</div>
										</div>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机架备注：</b></label>
											<div class='col-sm-9'>
												<input id="user_mail" name='user_mail' type='text'
													class="form-control" placeholder="字数限制2000字..." />
											</div>
										</div>
									</form>
								</div>
		
							</div>
							<div class="modal-footer">
								<button id="cancel" type="button" class="btn btn-round btn-danger" >
									取消</button>
								<button id="submit" type="button"
									class="btn btn-round btn-success">提交</button>
							</div>
						</div>
					</div>
				</div> -->
				<div class="modal fade" id="editeRackModal" tabindex="-1"
					role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
					data-backdrop="static">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"
									aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">修改机架信息</h4>
							</div>
							<div class="modal-body">
								<div class="well" style="margin-top: 1px;">
									<form class='form-horizontal' role='form' id='create_user_form'>
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机架名称：</b></label>
											<div class='col-sm-9'>
												<input id="user_name" name='user_name' type='text' value="机架1"
													class="form-control" placeholder="只能包含中文、英文、数字、下划线..." />
											</div>
										</div>
										<!-- <div class='form-group'>
											<label class='col-sm-3'><b>登录密码：</b></label>
											<div class='col-sm-9'>
												<input id="user_pass" name='user_pass' type='password'
													class="form-control" placeholder="输入密码..." />
											</div>
										</div> -->
										<div class='form-group'>
											<label class='col-sm-3'><b><font color="red">*</font>&nbsp;机架备注：</b></label>
											<div class='col-sm-9'>
												<input id="user_mail" name='user_mail' type='text' value="机架备注息"
													class="form-control" placeholder="字数限制2000字..." />
											</div>
										</div>
									</form>
								</div>
		
							</div>
							<div class="modal-footer">
								<button id="cancel" type="button" class="btn btn-round btn-danger">
									取消</button>
								<button id="submit" type="button"
									class="btn btn-round btn-success">提交</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
