<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>plugins/echarts/echarts.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="host_admin" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
					<script src="<%=basePath %>js/console/hostMonitor.js"></script>	
					<input id="hostId" type="hidden" value="${host }"/>
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li class="active"><b>主机管理</b></li>
							<li class="active"><b>主机监控</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="well well-sm">
							<a href="<%=basePath %>host/index.html" data-toggle="modal"
								class="btn btn-sm btn-primary btn-round"> <i
								class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
							</a>
							<span class="text-muted" style="position: relative;left: 70px">
									<strong>主机名称 : ${hostName }</strong>
							</span>
							
							<select  id="timeSelect" style="float: right;" > 
								<option value='0'>实时</option>
								<option value='1'>1小时</option>
								<option value='2'>4小时</option>
								<option value='3'>12小时</option>
								<option value='4'>24小时</option>
							</select>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="col-sm-6">
									<div id="host_mem_chart"></div>
								</div>
								<div class="col-sm-6">
									<div id="host_cpu_chart"></div>
								</div>
								<div class="col-sm-12">
									<div id="host_net_chart"></div>
								</div>
								<div class="col-sm-12">
									<div id="host_df_chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
