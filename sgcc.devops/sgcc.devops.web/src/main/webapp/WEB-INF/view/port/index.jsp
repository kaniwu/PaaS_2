<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<script src="<%=basePath %>js/console/port.js"></script>
</head>
<body class="no-skin">
	<div id="index_index_body" class="index_index_body">
		<jsp:include page="../header.jsp"></jsp:include>
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try {
					ace.settings.check('main-container', 'fixed')
				} catch (e) {
				}
			</script>
			<jsp:include page="../nav.jsp">
				<jsp:param value="component_port" name="page_index" />
				<jsp:param value="manage_resource" name="parent_index" />
			</jsp:include>
			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs" id="breadcrumbs">
						<ul class="breadcrumb">
							<li><i class="ace-icon fa fa-home home-icon"></i> <a
								href="<%=basePath %>index.html"><strong>首页</strong></a></li>
							<li><a
								href="<%=basePath %>component/index.html"><strong>组件管理</strong></a></li>
							<li class="active"><b>端口管理</b></li>
						</ul>
					</div>
					<div class="page-content">
						<div class="row">
							<div class="col-xs-12">
								<div class="well well-sm">
									<label class="col-sm-1 control-label no-padding-right" for="form-field-1-1" style="margin-top: 5px;"><strong>端口类型：</strong>
									</label> 
									<div class="col-sm-2">
										<div class="select-group">
											<select id="type" class="form-control" >
												<option value="">请选择</option>
												<option value="nginx">nginx</option>
<!-- 												<option value="nginxGroup">nginx组</option> -->
											</select>
										</div>
									</div>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<button class="btn btn-sm btn-primary btn-round"
										onclick="searchHost()">
										<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i> <b>查询</b>
									</button>
									<button class="btn btn-sm btn-primary btn-round"
										onclick="history.go(-1);">
										<i class="ace-icon fa fa-arrow-left bigger-125"></i> <b>返回</b>
									</button>
									<div id="spinner" style="float: right; display: none;">
										<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
									</div>
								</div>
								<div>
									<button class="btn btn-sm btn-success btn-round"
										onclick="showCreateWin()">
										<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
									</button>
									<button class="btn btn-sm btn-warning btn-round"
										onclick="updateWin()">
										<i class="ace-icon fa fa-pencil-square-o bigger-125"></i> <b>编辑</b>
									</button>
									<button class="btn btn-sm btn-danger btn-round" 
										onclick="removePort()">
										<i class="ace-icon fa fa-minus-circle bigger-125"></i> <b>删除</b>
									</button>
								</div>
								<div>
									<table id="port_list"></table>
									<div id="port_page"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
