<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
String basePath = request.getContextPath()+"/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
<link rel="shortcut icon" href="<%=basePath %>img/title_cloud.png" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<meta name="description" content="overview &amp; stats" />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
<jsp:include page="../js.jsp"></jsp:include>
<link rel="stylesheet" href="<%=basePath %>css/user/host.css" />
<link rel="stylesheet" href="<%=basePath %>css/user/hostComponent.css" />
<script src="<%=basePath %>js/console/hostComponent.js"></script>
<c:set var="authButton" value='${buttonsAuth}'></c:set>
<title>主机组件管理</title>
</head>
<body class="no-skin">
	<jsp:include page="../header.jsp"></jsp:include>
	<div class="main-container" id="main-container">
		<script type="text/javascript">
			try {
				ace.settings.check('main-container', 'fixed')
			} catch (e) {
			}
		</script>
		<jsp:include page="../nav.jsp">
			<jsp:param value="host_component" name="page_index" />
			<jsp:param value="manage_resource" name="parent_index" />
		</jsp:include>
		<div class="main-content">
			<div class="main-content-inner">
				<div class="breadcrumbs" id="breadcrumbs">
					<ul class="breadcrumb">
						<li><i class="ace-icon fa fa-home home-icon"></i> <a
							href="<%=basePath %>index.html"><strong>首页</strong></a></li>
						<li class="active"><b>主机组件管理</b></li>
						<li class="active"><b><span id="hostComName"></span></b></li>
					</ul>
				</div>
				<div class="page-content">
					<div class="row">
						<div class="col-xs-12" style="height: 100%">
							<!-- 左侧树 -->
							<div class="col-sm-2 well well-sm" id="treeDiv" style="height: 503px;">
								<div id="roleAuthTree" class="ztree"></div>
							</div>
							<div id="formdiv" class="col-sm-10">
								<div id="hostComponentManager">
									<div class="well well-sm">
										<label class="col-sm-1 control-label no-padding-right"
											for="form-field-1-1" style="margin-top: 5px;"><strong>版本号：</strong>
										</label>
										<div class="col-sm-2">
											<div class="select-group">
												<input id="hostComVersion" name="hostComVersion" type="text"
													class="form-control" />
											</div>
										</div>
										<label class="col-sm-2 control-label no-padding-right"
											for="form-field-1-1" style="margin-top: 5px;"> </label> 
										<button class="btn btn-sm btn-primary btn-round"
											onclick="selectVersionClick()">
											<i class="ace-icon glyphicon glyphicon-zoom-in bigger-125"></i>
											<b>查询</b>
										</button>
										<button class="btn btn-sm btn-success btn-round"
											onclick="showCreateHostComponent()">
											<i class="ace-icon fa fa-plus-circle bigger-125"></i> <b>新增</b>
										</button>
										<div id="spinner" style="float: right; display: none;">
					                        <i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>
					                      </div>
									</div>
									<div>
										<table id="component_list"></table>
										<div id="component_page"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/html" id="hostComponent">
	<div style="margin-top:1px;" class="well">
		<form class="form-horizontal" role="form">
			<input type="hidden"  name="appFilePath" id="appFilePath" />
			<input type="hidden"  name="hostCompUUid" id="hostCompUUid" />
         <div class="form-group">
					<label class="col-sm-2"><b>版本号：</b></label>
                    <div class='col-sm-4'><input type="text"  style="width:100%; " name="version"  id="version" value="{{version}}"/></div>
                    <label class="col-sm-2"><b>文件：</b></label>
                    <div class='col-sm-4'>
						<input type="file"  name="file" id="file"  class="form-control" />
						<span id="percentage"></span>
						<progress id="progressBar" value="0" max="100" hidden="true"> </progress>
					</div>
         </div>
		<div class='form-group'>
				<label class="col-sm-2"></label>
					<div  class='col-sm-10' id="hostCompPid" style="display: none;">
						<div style="" id="hostCompShowId">已经完成0%</div>
						<div class="progress">
							<div id="hostCompProgressId" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
					</div>
				</div>
		<div class="form-group">
			<label class="col-sm-2"><b>文件路径：</b></label>
                     <div class='col-sm-10'><input type="text"  style="width:100%; "   name="hostInsallPath" id="hostInsallPath" readOnly="readOnly"/></div>
         </div>
		<div class="form-group">
                    <label class="col-sm-2"><b>安装脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; " maxlength="4000" name="installCommand" id="installCommand" data-no-space='true'>{{installCommand}}</textarea></div>
         </div> 
		<div class="form-group">
                    <label class="col-sm-2"><b>升级脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; " maxlength="4000" name="upgradeCommand" id="upgradeCommand" data-no-space='true'>{{upgradeCommand}}</textarea></div>
         </div> 
         <div class="form-group">
					<label class="col-sm-2"><b>状态命令：</b></label>
                    <div class='col-sm-10'><textarea rows="2" style="width:100%; " maxlength="4000" name="statusCommand" id="statusCommand" data-no-space='true'>{{statusCommand}}</textarea></div>
         </div>
         <div class="form-group">
					<label class="col-sm-2"><b>版本命令：</b></label>
                    <div class='col-sm-10'><textarea rows="2" style="width:100%; " maxlength="4000" name="versionCommand" id="versionCommand" data-no-space='true'>{{versionCommand}}</textarea></div>
         </div> 
         <div class="form-group" style="margin-left: 0">   
           <label class="col-sm-2"></label>
          <label class="col-sm-10" style="margin-left: 0"><input type="checkbox" name="transfer"  id="transfer" {{if transfer==1}} checked="checked" {{/if}}/><b>更新是否转移容器</b></label>
          </div>    
               <div class='form-group'>
					<div id="appPid" style="width: 100%;height:100%;display: none;">
						<div style="" id="appShowId">已经完成0%</div>
						<div class="progress">
							<div id="appProgressId" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
					</div>
				</div>  
			</form>
    </div>
	</script>
	<script type="text/html" id="hostComponentUpdate">
	<div style="margin-top:1px;" class="well">
		<form class="form-horizontal" role="form">
			<input type="hidden"  name="appFilePath" id="appFilePath" />
         <div class="form-group">
					<label class="col-sm-2"><b>版本号：</b></label>
                    <div class='col-sm-4'><input type="text"  style="width:100%; " name="version"  id="version" value="{{version}}"/></div>
                    <label class="col-sm-2"><b>文件：</b></label>
                    <div class='col-sm-4'><input type="text" 	 style="width:100%; " name="filename" value="{{filename}}" readonly="true"/></div>
         </div>
          <div class="form-group">
                    <label class="col-sm-2"><b>安装脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; " maxlength="4000" name="installCommand" id="installCommand" data-no-space='true'>{{installCommand}}</textarea></div>
         </div>  
		<div class="form-group">
                    <label class="col-sm-2"><b>升级脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; " maxlength="4000" name="upgradeCommand" id="upgradeCommand" data-no-space='true'>{{upgradeCommand}}</textarea></div>
         </div> 
		<div class="form-group">
                    <label class="col-sm-2"><b>状态命令：</b></label>
                     <div class='col-sm-10'><textarea rows="2" style="width:100%; " maxlength="4000" name="statusCommand" id="statusCommand" data-no-space='true'>{{statusCommand}}</textarea></div>
         </div>  
         <div class="form-group">
					<label class="col-sm-2"><b>版本命令：</b></label>
                    <div class='col-sm-10'><textarea rows="2" style="width:100%; " maxlength="4000" name="versionCommand" id="versionCommand" data-no-space='true'>{{versionCommand}}</textarea></div>
         </div> 
         <div class="form-group">   
           <label class="col-sm-2"></label>
          <label class="col-sm-10" style="margin-left: 0"><input type="checkbox" name="transfer"  id="transfer" {{if transfer==1}} checked="checked" {{/if}}/><b>更新是否转移容器</b></label>
          </div>    
               <div class='form-group'>
					<div id="appPid" style="width: 100%;height:100%;display: none;">
						<div style="" id="appShowId">已经完成0%</div>
						<div class="progress">
							<div id="appProgressId" class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
						</div>
					</div>
				</div>  
			</form>
    </div>
	</script>
	<script type="text/html" id="hostComponentRead">
	<div style="margin-top:1px;" class="well">
		<form class="form-horizontal" role="form">
            <input type="text" value="{{type}}"  name="type" hidden="hidden"/>
         <div class="form-group">
					<label class="col-sm-2"><b>版本号：</b></label>
                    <div class='col-sm-4'><input type="text"  style="width:100%; " name="version" value="{{version}}" disabled/></div>
                    <label class="col-sm-2"><b>文件：</b></label>
                     <div class='col-sm-4'><input type="text" 	 style="width:100%; " name="filename" value="{{filename}}" readonly="true"/></div>
         </div>
		<div class="form-group">
                    <label class="col-sm-2"><b>安装脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; " name="installCommand"  disabled>{{installCommand}}</textarea></div>
         </div>
<div class="form-group">
                    <label class="col-sm-2"><b>升级脚本：</b></label>
                     <div class='col-sm-10'><textarea rows="5" style="width:100%; "  name="upgradeCommand"  disabled>{{upgradeCommand}}</textarea></div>
         </div> 
 <div class="form-group">
					<label class="col-sm-2"><b>状态命令：</b></label>
                    <div class='col-sm-10'><textarea rows="2" style="width:100%; " name="statusCommand"  disabled>{{statusCommand}}</textarea></div>
         </div>  
         <div class="form-group">
					<label class="col-sm-2"><b>版本命令：</b></label>
                    <div class='col-sm-10'><textarea rows="2" style="width:100%; "  name="versionCommand"  disabled>{{versionCommand}}</textarea></div>
         </div> 
         <div class="form-group">   
           <label class="col-sm-2" ></label>
          <label class="col-sm-10"><input type="checkbox" name="transfer"  disabled {{if transfer==1}} checked="checked" {{/if}}   ><b>更新是否转移容器</b></label>
          </div>      
			</form>
    </div>
	</script>
</body>
</html>
