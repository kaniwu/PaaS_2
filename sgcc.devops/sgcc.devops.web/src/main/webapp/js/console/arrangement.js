var arrangement = undefined;
var fileindex=1;
var deployConfiFile;
var loadBalanceType =null;
var loadBalanceTId = null;
var nginxPort="",nginxPortId="";nginxIp=null;
var socket_upconfig = undefined;
function connect() {
	if ('WebSocket' in window) {
		var host = window.location.host+base+"/";
		if(window.location.protocol=='http:'){
			socket_upconfig = new WebSocket('ws://' + host + '/uploadService');
        }else if(window.location.protocol=='https:'){
        	socket_upconfig = new WebSocket('wss://' + host + '/uploadService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
		
        socket_upconfig.onopen = function () {};
        socket_upconfig.onclose = function () {};
        socket_upconfig.onerror = function(e) {};
        if(window.location.protocol=='http:'){
        	socket2 = new WebSocket('ws://' + host + '/uploadService');
        }else if(window.location.protocol=='https:'){
        	socket2 = new WebSocket('wss://' + host + '/uploadService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
		
		socket2.onopen = function() {
		};
		socket2.onclose = function() {
		};
		socket2.onerror = function(e) {
		};
		socket2.onmessage = function(event) {
			var obj = JSON.parse(event.data);
			if (obj.messageType == "ws_up_file") {
				sendArraybuffer(obj);
			}
		};
	} else {
		console.log('Websocket not supported');
	}
};

function validate(filename) {
	var reg = new RegExp("[u4e00-u9fa5]");
	var pattern = new RegExp(
			"[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]");
	if (!reg.test(filename) && !pattern.test(filename)) {
		bootbox.alert("上传的文件名不能包含汉字和特殊字符");
		return false;
	} else {
		return true;
	}
};

function disconnect() {
	socket.close();
	socket2.close();
	socket3.close();
	console.log("Disconnected");
};
/*数据源*/
var canvasGrapHeight = 0, paragraph = 4 * 1024 * 1024, appFile, configFile, startSize, endSize = 0, i = 0, j = 0, count = 0,
arrangement_open_dbChange = function(){
	var databaseId = $('#arrangement_open_db option:selected').val();
	$.ajax({
        type: 'get',
        url: base+'driver/listByDatabaseId',
        dataType: 'json',
        data:{databaseId:databaseId},
        success: function (array) {
        	$('#arrangement_open_drive').empty();
        	var driverId = $("#driverId").val();
            $.each (array, function (index, obj){
				var id = obj.id;
				var dValue = obj.driverName;
				if(driverId==id){
					$('#arrangement_open_drive').append('<option value="'+id+'" selected="selected">'+dValue+'</option>');
				}else{
					$('#arrangement_open_drive').append('<option value="'+id+'">'+dValue+'</option>');
				}
				
            });
        }
	});
};
/**
 * @author yangqinglin
 * @datetime 2015年9月10日 17:12
 * @description 返回上一个页面
 */
function gobackpage(){
	history.go(-1);
}
function ckForm(deployVersion,systemAppId,instanceCount,domainName,appName){
	if(isNull(deployVersion)||deployVersion==""){
		showMessage("请输入系统版本");
		return false;
	}
	if(!systemAppId==null||systemAppId==''){
    	showMessage("请选择应用程序包！");
    	return false;
    }
	if(!isInteger(instanceCount)){
		showMessage("实例数量请输入正确的数值！");
		return false;
	}
	if(isNull(domainName)){
		showMessage("请输入域名！");
		return false;
	}
	if(isNull(appName)){
		showMessage("请输入应用名！");
		return false;
	}
	return true;
}

function arrangementObject(){
	var _this = this;
	this.cy = undefined;
	var options = {
			f5Html : "<div class='well ' style='margin-top:1px;'>"
	            + "<form class='form-horizontal' role='form' id='topology_form'>"
	            + "<div class='form-group'>"
	            + "<label class='col-sm-3'><b>F5：</b></label>"
	            + "<div class='col-sm-9'>"
	            + "<select id='arrangement_open_f5' name='arrangement_open_f5' onchange='validF5()' class='form-control'>", 
	        f5HtmlEnd : "</select>"
	            + "</div>" + "</div>" + "</form>" + "</div>", 
	        NginxHtml : "<div class='well ' style='margin-top:1px;'>"
	            + "<form class='form-horizontal' role='form' id='topology_form'>"
	            + "<div class='form-group'>"
	            + "<label class='col-sm-3'><b>nginx组：</b></label>"
	            + "<div class='col-sm-9'>"
	            + "<select id='arrangement_open_Nginx' name='arrangement_open_Nginx' onchange='validPhyNginx()' class='form-control'>",
	        NginxHtmlEnd : "</select>"
	            + "</div>" + "</div>", 
            NginxPort : "<div class='form-group'>"
	            + "<label class='col-sm-3'><b>nginx组端口：</b></label>"
                + "<div class='col-sm-9'>"
	            + "<select id='arrangement_open_nginxPort' name='arrangement_open_nginxPort' class='form-control'>", 
            NginxPortEnd : "</select>"
	            + "</div>" + "</div>" + "</form>" + "</div>", 
	        containerNginxHtml : "<div class='well ' style='margin-top:1px;'>"
	            + "<form class='form-horizontal' role='form' id='topology_form'>"
	            + "<div class='form-group'>"
	            + "<label class='col-sm-3'><b>容器nginx：</b></label>"
	            + "<div class='col-sm-9'>"
	            + "<select id='arrangement_open_componentNginx' name='arrangement_open_componentNginx' onchange='validCompNginx()' class='form-control'>", 
	        containerNginxHtmlEnd : "</select>"
	            + "</div>" + "</div>" + "</form>" + "</div>", 
	            
            dockerHtml : "<div class='well ' style='margin-top:1px;'>"
                + "<form class='form-horizontal' role='form' id='topology_form'>"
                + "<div class='form-group'>"
                + "<label class='col-sm-3'><b>容器数量：</b></label>"
                + "<div class='col-sm-9'>"
                + "<input type='text' id='arrangement_open_docker' class='form-control' onblur='validUrl();'>",
            
            dbHtml : "<div class='well ' style='margin-top:1px;'>"
            + "<form class='form-horizontal' role='form' id='topology_form'>"
            + "<div class='form-group'>"
            + "<label class='col-sm-3'><b>数据源：</b></label>"
            + "<div class='col-sm-9'>"
            + "<select id='arrangement_open_db' name='arrangement_open_db' onchange='validDb()' class='form-control'>"
            +"<option value=''>请选择</option>", 
            
            dbHtmlEnd : "</select>"
                + "</div>"
                + "</div>"
                + "<div class='form-group'>"
                + "<label class='col-sm-3'><b>驱动包：</b></label>"
                + "<div class='col-sm-9'>"
                + "<select id='arrangement_open_drive' name='arrangement_open_drive' onchange='validDrive()' class='form-control'><option value=''>请选择</option></select>"
                + "</div>"
                + "</div>"
                + "<div class='form-group'>"
                + "<label class='col-sm-3'><b>JNDI：</b></label>"
                + "<div class='col-sm-9'>"
                + "<input type='text' id='arrangement_open_db_url' onchange='validDb()' class='form-control' placeholder='请输入JNDI名称' >"
                + "</div>" + "</div>"
                + "<div class='form-group'>"
                + "<label class='col-sm-3'><b>初始化脚本：</b></label>"
                + "<div class='col-sm-9'>"
                + "<input type='text' id='arrangement_open_initSql' class='form-control' placeholder='<init-sql>属性配置' >"
                + "</div>" + "</div>"
                + "</form>" + "</div>",
                
                
            selectedNode : undefined, 
            f5Variable : 'f5_', 
            NginxVariable : 'compNginx_',
            conNginxVariable : 'conNginx_', 
            dockerVariable : 'docker_', 
            dbVariable : 'db_',
            position : undefined,
            
            f5Position : {}, 
    		nextPhyNgPosition : {}, 
    		nextCompNgPosition : {}, 
    		nextDockerPosition : {}, 
    		nextDbPosition : {}, 
    		rateX : 120, 
    		rateY : 40,
    		
    		f5Storage : {}, 
    		ngStorage : {},	
    		dockerStorage : {},
    		dbStorage : {}
	}
	// 加载組件的列表
    $.get(base + 'component/list?componentType=0&page=1&rows=65536', null, function(response) {
        var f5SelectStr = '', ngSelectStr = '', dbSelectStr = '',
        componentId = undefined, componentName = undefined,
                componentType = undefined,componentIp=undefined;
        $.each(response.rows, function(index, obj) {
            componentId = obj.componentId;
            componentName = obj.componentName;
            componentType = obj.componentType;
            componentIp = obj.componentIp;

            // 暂时写死 ，3代表数据库
            if (componentType == '3') {
                dbSelectStr += '<option value="' + componentId + '">' + componentName + '</option>';
            }
            // 暂时写死 ，2代表F5
            if (componentType == '2') {
            	if(loadBalanceType==1&&loadBalanceId==componentId){
            		f5SelectStr += '<option value="' + componentId + '" ip="'+componentIp+'" selected="selected" >' + componentName + '</option>';
            		loadBalanceTId=componentId;
            	}else{
            		f5SelectStr += '<option value="' + componentId + '" ip="'+componentIp+'">' + componentName + '</option>';
            	}
            }
            // 暂时写死 ，6代表NGNIX组
            if (componentType == '6') {
            	if(loadBalanceType==2&&loadBalanceId==componentId){
            		ngSelectStr += '<option value="' + componentId + '" ip="'+componentIp+'" selected="selected" >' + componentName + '</option>';
            		loadBalanceTId=componentId;
            		nginxIp=componentIp;
            	}else{
            		ngSelectStr += '<option value="' + componentId + '" ip="'+componentIp+'">' + componentName + '</option>';
            	}
            }
        });
        options.f5Html = options.f5Html + f5SelectStr + options.f5HtmlEnd;
        options.NginxHtml = options.NginxHtml + ngSelectStr + options.NginxHtmlEnd;
        options.dbHtml = options.dbHtml + dbSelectStr + options.dbHtmlEnd;
    }, 'json');
	//nginx组件端口
	$.get(base+'port/list?page=1&rows=65536&type=nginx',null,function(response){
		var ngPortSelectStr='';
		$.each (response.rows, function (pIndex, obj){
			if(nginxPort==obj.port){
				ngPortSelectStr+='<option value="'+obj.id+'" selected="selected" >'+obj.port+'</option>';
			}else{
				ngPortSelectStr+='<option value="'+obj.id+'" >'+obj.port+'</option>';
			}
			
        });
		options.NginxPort = options.NginxPort + ngPortSelectStr + options.NginxPortEnd;
	},'json');
	var data=null;
	$.get(base+'image/list?imageType=2&page=1&rows=65536',data,function(response){
		var conNginxHtml = '';
		
		$.each (response.rows, function (index, obj){
			var imageId = obj.imageId;
			var tempName = obj.tempName;
			conNginxHtml+='<option value="'+imageId+'">'+tempName+'</option>';
        });
		options.containerNginxHtml = options.containerNginxHtml + conNginxHtml
        + options.containerNginxHtmlEnd;
	},'json');
   
    $('#timestamp').val(Math.round(new Date().getTime() / 1000));

    $('#arrangement_create').click(function() {
        if ($("#arrangement_form").valid()) {
        	var flag = true;
        	//拼接配置文件
        	var deployFiles = "";
        	$("#deployFiles li").each(function(){
        		var indexnum = $(this).find("[name=deployFile]").attr("fileindex"),
        		obj = document.getElementById("deployFile"+indexnum),
        		attr = obj.attributes.type.value,
        		deployFileName = null;
        		if(attr=='text'){
        			deployFileName = obj.value;
        		}else if(attr=='file'){
        			var deployFile = obj.files[0];
        			deployFile= $('#deployFile'+indexnum).val().split('\\');
        			deployFileName = deployFile[deployFile.length-1];
        		}
        		var filePath = $("#filePath"+indexnum).val();
        		if(!deployFileName||deployFileName==""){
        		}else{
        			if(filePath==null||filePath==''){
        				showMessage("请输入配置文件上传路径！");
        				flag=false;
        			}
        			deployFiles+=deployFileName + "==="+filePath+" ";
        		}
        	});
        	if(!flag){
        		return;
        	}
        	if($("#Xmxmax").val()<$("#Xmxmin").val()){
        		showMessage("JVM最大内存不能小于最小内存！");
        		flag=false;
        	}
            var params = undefined, 
            	deployVersion = $("#arrangement_deployVersion").val(), //部署版本，如果版本不变，无需重新生成镜像
            	versionChange = $('input[name="arrangement_version"]:checked').val(),//版本切换
            	locationStragegy=$('input[name="location_stragegy"]:checked').val(),//分布策略
            	systemAppId= $("#arrangement_systemAppId").val(), //选择应用程序包
            	locationleavel=$("#arrangement_location_leavel option:selected").val(),//位置层级设置
            	cluster_select = $("#arrangement_cluster").val(), //选择集群
            	imageId = $("#arrangement_baseImageId").val(), //基础镜像
            	contentPath=$("#contentPath").val(), 
            	cpu=$("#cpu").val(),//容器cpu限制
            	memery = $("#memery").val(),//容器内存限制
            	Xmxmax= $("#Xmxmax").val(),//jvm最大使用内存
            	Xmxmin= $("#Xmxmin").val(),//jvm最小使用内存
            	regularlyPublish=$("#regularlyPublish").val(),//是否定时发布
				publishTime=$("#publishTime").val(),//发布时间
				dnsEnable = $('input[name="DNSEnable"]:checked').val(), //启用DNS
				domainName_=$("#domainName_").val(), 
				http=$("#http").val(), //http类型
				domainName = $("#domainName").val(), //域名
				port=$("#port").val(), 
				appName = $("#appName").val(), //应用名称
            	url = $("#url").val(),//校验连接
            	Dockerfile=$("#Dockerfile").val(),//Dockerfile段
            	timestamp = $("#timestamp").val(), //时间戳，用于创建唯一文件夹目录
            	deployId  = $("#deployId").val(),//当前已部署版本ID
            	systemId = $("#systemId").val(), //当前物理系统
            	systemName = $("#systemName").val(), 
            	instanceCount = arrangement.cy.nodes();
	            var instanceCount = 0, dbNode = undefined;
            for ( var key in arrangement.options.f5Storage) {
                if (arrangement.options.f5Storage.hasOwnProperty(key)){
                    f5Id = arrangement.options.f5Storage[key];
                    f5Enable = true;
                    loadBalanceType=1;
                }
                    
            }
            
            /*for ( var key in arrangement.options.ngStorage) {
            	if(key.split('_')[0]=='compNginx'){
            		nginxType = 2;
            		if (arrangement.options.ngStorage.hasOwnProperty(key)){
                        ngnix.push(arrangement.options.ngStorage[key]);
                        nginxEnable=true;
                    }
            	}
            	if(key.split('_')[0]=='conNginx'){
            		nginxType = 1;
            		if (arrangement.options.ngStorage.hasOwnProperty(key)){
            			nginxImageId = arrangement.options.ngStorage[key];
                        nginxEnable=true;
                    }
            	}
            }*/
            for ( var key in arrangement.options.ngStorage) {
                if (arrangement.options.ngStorage.hasOwnProperty(key)) {
                    dbNode = arrangement.options.ngStorage[key];
                    nginxPortId=dbNode.nginxPortId;
                    nginxPort=dbNode.nginxPort;
                    nginxEnable=true;
                    loadBalanceType=2;
                    loadBalanceTId=dbNode.nginxId;
                }
            }
            jQuery.each(arrangement.cy.nodes('node[id^="docker_"]'),function(){
                ++instanceCount;
            });
            //拼接数据库
        	var databaseInfo = "";
        	var dataBaseInfos = new Array();
            for ( var key in arrangement.options.dbStorage) {
                if (arrangement.options.dbStorage.hasOwnProperty(key)) {
                    dbNode = arrangement.options.dbStorage[key];
                    var dbInfo = new Object();
        		    dbInfo.databaseId=dbNode.type;
        			dbInfo.dataSource=dbNode.url;
        			dataBaseInfos.push(dbInfo);
                    //databaseInfo += dbNode.type + "===" + dbNode.url + "===" + dbNode.drive +"===" +dbNode.initSql + "|~~|";
                }
            }
           // JSONArray格式的字符串后台解析
        	databaseInfo = JSON.stringify(dataBaseInfos);
        	flag = ckForm(deployVersion,systemAppId,instanceCount,domainName,appName);
        	if(flag){
	            params = {
	            	timestamp:timestamp,
	                deployVersion : deployVersion,
	                versionChange : versionChange,
	                locationStragegy:locationStragegy,
	                systemAppId:systemAppId,
	                locationLeavel:locationleavel,
	                clusterId : cluster_select,
	                baseImageId : imageId,
	                instanceCount : instanceCount,
					contentPath:contentPath,
	                deployFiles : deployFiles,//上传配置文件及上传路径
	                cpu:cpu,//容器cpu限制
					memery:memery,//容器内存限制
					Xmxmax:Xmxmax,//jvm最大使用内存
					Xmxmin:Xmxmin,//jvm最小使用内存
					regularlyPublish:regularlyPublish,//是否定时发布
					publishTime:publishTime,//发布时间
					databaseInfo : databaseInfo,
					dnsEnable : dnsEnable,
					loadBalanceType:loadBalanceType,//负载类型
					loadBalanceId:loadBalanceTId,//负载ID
					//f5ExternalPort:f5ExternalPort,//f5对外端口
					//f5ExternalIp:$('#f5ExternalIp').val(),//f5对外IP
					//f5Enable:f5Enable,
					nginxPortId:nginxPortId,//nginx端口
					nginxPort:nginxPort,
					//nginxEnable:nginxEnable,
	                http:http,
	                domainName : domainName,
	                appName:appName,
	                port:port,
	                url : url,
	                deployId:deployId,
					systemId:systemId,//当前物理系统
					Dockerfile:Dockerfile,//Dockerfile段
					deployConfiFile:deployConfiFile,//更正上一版本配置文件与应用包
					systemName:systemName
	            };
	            $("#arrangement_create").addClass("disabled");
	            gobackpage();
	            $.post(base+'system/deploy',params,function(resp){
	                 $("#arrangement_create").removeClass("disabled");
	            });
        	}
        }
    });
    this.options = options;
    
    arrangementInit();
    function arrangementInit(){
    	$('input:radio[name="arrangement_version"]').change(function(){
    		if($('input[name="arrangement_version"]:checked').val()==1){
    			$("#timerDiver").css("display","none");
    		}else{
    			$("#timerDiver").css("display","");
    		}
    	});
    	var DNSCheckbox = document.getElementById("DNSEnable");
		DNSCheckbox.onclick = function() {
			setDomainName();
		    if(this.checked) {
		    	 $("#dnsDiv").css("display","");
		    }
		    else {
		    	 $("#dnsDiv").css("display","none");
		    }
		};
		//定时时间
		$('#publishTime').datetimepicker({
			format:'YYYY-MM-DD HH:mm:00'
		}).next().on(ace.click_event, function(){
			$(this).prev().focus();
		});
    	$.ajax({
    		type : 'get',
    		url : base + 'topology/arrangementInit?deployId=' + $("#deployId").val(),
    		dataType : 'json',
    		success : function(result) {
				//初始化组件部分
				$('#arrangement_create').removeAttr("disabled");
				//如果response.deployId为空，说明是第一次部署
				//部署版本
				$("#arrangement_deployVersion").val(result.version);
				$("#appLogPath").val(result.appLogPath);
				//版本切换
				var versionChange=result.versionChange;
				if(versionChange=='1'){
					$("#optionsRadios2").attr('checked','checked');
					$("#optionsRadios1").removeAttr('checked');
					$("#timerDiver").css("display","none");
				}else{
					$("#optionsRadios1").attr('checked','checked');
					$("#optionsRadios2").removeAttr('checked');
					$("#timerDiver").css("display","");
				}
				var location_stragegy=result.locationStragegy;
				if(location_stragegy=='1'){
					$("#location_stragegy_1").prop('checked',true);;
					$("#location_stragegy_2").prop('checked',false);
				}else if(location_stragegy=='2'){
					$("#location_stragegy_1").prop('checked',false);
					$("#location_stragegy_2").prop('checked',true);
				}
				$("#contentPath").val(result.contentPath);
				$("#arrangement_location_leavel").val(result.locationLeavel);
				$("#arrangement_cluster").val(result.clusterId);
				$("#cpu").val(result.cpu);
				$("#memery").val(result.memery);
				$("#arrangement_baseImageId").val(result.baseImageId);
				//是否定时 发布
				var regularlyPublish = result.regularlyPublish;
				$("#regularlyPublish").val(regularlyPublish);
				//定时 发布时间
				var publishTime = result.publishTime;
				$("#publishTime").val(publishTime);
				if(result.dnsEnable){
					document.getElementById('DNSEnable').checked=true;
				}
				if(result.dnsEnable==1){
					$("#DNSEnable").prop('checked',true);
					$("#dnsDiv").css("display","");
				}else{
					$("#DNSEnable").prop('checked',false);
					$("#dnsDiv").css("display","none");
				}
				$("#domainName_").val(result.domainName);
				if(result.http!=null&&result.http!=""){
					$("#http").val(result.http);
				}
				$("#domainName").val(result.domainName);
				nginxPort=result.nginxPort;
				loadBalanceType =result.loadBalanceType;
				loadBalanceTId = result.loadBalanceTId;
				$("#port").val(result.port);
				$("#appName").val(result.appName);
				$("#url").val(result.url);
				$("#Dockerfile").val(result.dockerfile);
				//初始化位置层级
				location_leavel(result.locationLeavel);
				//初始化应用程序包
				getSystemApp($("#systemId").val(),result.systemAppId);
				//初始化集群
				getClusters(result.clusterId);
				//初始化镜像
				getImages(result.baseImageId);
				//配置文件初始化
				var deployFiles = result.deployFiles;
				deployConfigFile(deployFiles);
				if (result && result.success){
					_this.dataAnalysisFun(result.data);
				}else{
					_this.dataAnalysisFun([]);
				}
    		}
    	});
    }
    $("#regularlyPublish").change(function(){
		if($("#regularlyPublish").val()==1){
			var url = base + "system/selectDeployTimer";
			var systemId = $("#systemId").val();
		    $.ajax({
		        url: url,
		        type: 'get',
		        data: 'systemId='+systemId,
		        // 加载方式
		        dataType: 'json',
		        // 返回数据格式
		        success: function (response) {
		        	if(null!=response){
		        		var deployVersion = response.deployVersion;
		        		var publishTime = response.publishTime;
		        		bootbox.dialog({
		        			title : "<b>定时发布</b>",
		        			message : "已存在一个定时发布版本："+deployVersion+";预定发布时间："+publishTime+"。选择继续，上一个定时发布版本将失效。是否继续？",
		        			buttons : {
		        				"success" : {
		        					"label" : "<b>是</b>",
		        					"className" : "btn-sm btn-danger btn-round",
		        					"callback" : function() {
		        						$("#regularlyPublish").val(1);
		        						
		        						$.post(base + "system/setDeployTimer",{systemId:systemId},function(response){
		    								$("#spinner").css("display","none");
		    								if(!response.success){
		    									showMessage(response.message);
		    								}
		    							});
		        					}	
		        				},
		        				"cancel" : {
		        					"label" : "<i class='icon-info'></i> <b>否</b>",
		        					"className" : "btn-sm btn-warning btn-round",
		        					"callback" : function() {
		        						$("#regularlyPublish").val(0);
		        					}
		        				}
		        			},
		        			className : 'appmodal'
		        		});
		        	}
		        	
		        },
		        error: function (response) {
		            console.log(response);
		        }
		    });
		}
		
	});
}
arrangementObject.prototype = {
		circulationAnalysis : function (array, node) {
			var edges = [], tempNode = undefined;
			$.each(array, function(i, v) {
				tempNode = {
					id : v + node.id,
					source : v,
					target : node.id,
				};
				edges.push({
					data : tempNode
				});
			});
			return edges;
		},dataAnalysisFun : function (data) {
			var _this = this,nodes = [], edges = [], tempEdges = undefined, tempNode = undefined, f5Array = [], compNginxArray = [], phyNginxArray = [], dockerArray = [], dbArray = [], tempId = undefined;
			$.each(data, function(index, obj) {
				tempId = obj.id;
				if(obj.status == 0)
					tempId += '_exception';
				if (obj.id.indexOf('f5_') == 0) {
					f5Array.push(tempId);
				} else if (tempId.indexOf('conNginx_') == 0) {
					compNginxArray.push(tempId);
				} else if (obj.id.indexOf('compNginx_') == 0) {
					phyNginxArray.push(tempId);
				} else if (tempId.indexOf('docker_') == 0) {
					dockerArray.push(tempId);
				} else if (tempId.indexOf('db_') == 0) {
					dbArray.push(tempId);
				}
				nodes.push({
					data : obj
				});
			});
			$.each(data, function(index, obj) {
				tempId = obj.id;
				if(obj.status == 0){
					tempId += '_exception';
					obj.id = tempId;
				}
				if (tempId.indexOf('conNginx_') == 0 || tempId.indexOf('compNginx_') == 0) {
					edges = edges.concat(_this.circulationAnalysis(f5Array, obj));
				} else if (tempId.indexOf('docker_') == 0) {
					if (compNginxArray.length > 0) 
						edges = edges.concat(_this.circulationAnalysis(compNginxArray,obj));
					else if (phyNginxArray.length > 0)
						edges = edges.concat(_this.circulationAnalysis(phyNginxArray, obj));
					else if (f5Array.length > 0)
						edges = edges.concat(_this.circulationAnalysis(f5Array, obj));
				} else if (tempId.indexOf('db_') == 0) {
					edges = edges.concat(_this.circulationAnalysis(dockerArray, obj));
				}
			});
			_this.drawCytoscapeFun(nodes, edges);
		},drawCytoscapeFun : function (nodes, edges) {
			nodes = nodes.concat([ {
                data : {
                    id : 'f5',
                    name : 'f5',
                    type : 'f5',
                    sourceNode : true
                }
            }, {
                data : {
                    id : 'compNginx',
                    name : 'Nginx组',
                    type : 'compNginx',
                    sourceNode : true
                }
//            }, {
//                data : {
//                    id : 'conNginx',
//                    name : '容器Nginx',
//                    type : 'conNginx',
//                    sourceNode : true
//                }
            }, {
                data : {
                    id : 'docker',
                    name : '应用服务器',
                    type : 'docker',
                    sourceNode : true
                }
            }, {
                data : {
                    id : 'db',
                    name : '数据源',
                    type : 'db',
                    sourceNode : true
                }
            } ]);
			var _this = this;
			this.cy = cytoscape({
		        container : document.getElementById('canvasup'),
		        userZoomingEnabled : true,
		        userPanningEnabled : false,
		        style : [ {
		            selector : 'node',
		            css : {
		                'text-valign' : 'bottom',
		                'background-color' : '#f5f5f5',
		                'width' : '30px',
		                'height' : '30px'
		            }
		        }, {
		            selector : 'edge',
		            css : {
		                'target-arrow-shape' : 'triangle',
		                'target-arrow-color' : '#9DD6FA',
		                'line-color' : '#9DD6FA'
		            }
		        }, {
		            selector : ':selected',
		            css : {
		                'background-color' : 'black',
		                'line-color' : 'black',
		                'target-arrow-color' : 'black',
		                'source-arrow-color' : 'black'
		            }
		        } ],
		        elements : {
		            nodes : nodes,
		            edges : edges
		        }
		    });
		    this.cy.cxtmenu({
                selector : 'node[id^="f5_"],[id^="compNginx_"],[id^="conNginx_"],[id^="db_"]',
                commands : [{
                    content : '<icon class="ace-icon fa fa-pencil-square-o"></icon><span class="bigger-125" style="margin-left:5px">编辑</span>',
                    select : function() {
                        var option = undefined, nodeParams = this.data(), nodeId = nodeParams.id, type = nodeId.substring(0, nodeId.indexOf('_') + 1);

                        if (type == _this.options.f5Variable) {
                            option = {
                                node : this,
                                title : 'F5',
                                html : _this.options.f5Html,
                                success : _this.addF5SelectedFun
                            };
                           _this.addHtmlFunction(option);
                            $('#arrangement_open_f5').val(_this.options.f5Storage[nodeId]);
                        } else if (type == _this.options.NginxVariable) {
                            option = {
                                node : this,
                                title : 'Nginx组',
                                html : _this.options.NginxHtml+_this.options.NginxPort,
                                success : _this.addPhysicalNgSelectedFun
                            };
                            _this.addHtmlFunction(option);
                           // $('#arrangement_open_Nginx').val( _this.options.ngStorage[nodeId]);
                        } else if (type == _this.conNginxVariable) {
                            option = {
                                node : this,
                                title : '容器组件负载',
                                html : _this.options.containerNginxHtml,
                                success : _this.addConNgSelectedFun
                            };
                            addHtmlFunction(option);
                            $('#arrangement_open_componentNginx').val(_this.options.ngStorage[nodeId]);
                        } 
//		                              else if (type == dockerVariable) {
//		                                  option = {
//		                                      node : this,
//		                                      title : '容器',
//		                                      html : dockerHtml,
//		                                      success : addDcokerFun
//		                                  };
//		                                  addHtmlFunction(option);
//		                                  $('#arrangement_open_docker').val(
//		                                          dockerStorage[nodeId]);
//		                              } 
                        else if (type == _this.options.dbVariable) {
                            option = {
                                node : this,
                                title : '数据源',
                                html : _this.options.dbHtml,
                                beforFun : arrangement_open_dbChange,
                                success : _this.addDbSelectedFun
                            };
                            _this.addHtmlFunction(option);
                        }
                    }
                },{
                    content : '<icon class="ace-icon fa fa-trash-o bigger-125"></icon><span class="bigger-125" style="margin-left:5px">删除</span>',
                    select : function() {
                        this.remove();
                        _this.removeRelation(this.json().data.id);
                    }
                } ]
            });
		    this.cy.cxtmenu({
		        selector : 'node[id^="docker_"]',
		        commands : [{
		            content : '<icon class="ace-icon fa fa-trash-o bigger-125"></icon><span class="bigger-125" style="margin-left:5px">删除</span>',
		            select : function() {
		                this.remove();
		                _this.removeRelation(this.json().data.id);
		            }
		        } ]
		    });
		    this.cy.nodes().qtip({
		                content : function() {
		                    return this.data().name
		                },
		                show : {
		                    event : 'mouseover',
		                },
		                hide : {
		                    event : 'mouseout',
		                },
		                position : {
		                    my : 'top right',
		                    at : 'top left'
		                },
		                style : {
		                    classes : 'qtip-bootstrap'
		                }
		            });
		    var newPosition = {
		        x : 30,
		        y : 20
		    };
		    this.cy.ready(function() {
		    	_this.cy.zoom(1.1);
		    	_this.cy.pan({
		    		x : 0,
		    		y : 0
		    	});
		    	_this.cy.nodes().width(30);
		    	_this.cy.nodes().height(30);
		    	/*定位*/
		    	var width = _this.cy.width(), 
					f5Count = _this.cy.nodes('node[id^="f5_"]').length,
					phyNgCount = _this.cy.nodes('node[id^="compNginx_"]').length,
					compNgCount = _this.cy.nodes('node[id^="conNginx_"]').length,
					dockerCount = _this.cy.nodes('node[id^="docker_"]').length,
					dbCount = _this.cy.nodes('node[id^="db_"]').length,
					countY = 0, countX = 0;
	
				f5Count > 0 && ++countX;
				phyNgCount > 0 && ++countX;
				compNgCount > 0 && ++countX;
				dockerCount > 0 && ++countX;
				dbCount > 0 && ++countX;
	
				countY = phyNgCount > countY ? phyNgCount : countY;
				countY = compNgCount > countY ? compNgCount : countY;
				countY = dbCount > countY ? dbCount : countY;
	
				_this.options.rateX = width / (countX + 2);
				
				
				/**f5**/
				_this.options.f5Position = f5Count > 0 ?  {
					x : _this.options.rateX,
					y : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2)
				} : undefined;
				/**f5**/
				
				/**物理负载**/
				_this.options.nextPhyNgPosition = {
					x : 0,
					y : 0
				};
				_this.options.nextPhyNgPosition.y = phyNgCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
				_this.options.nextPhyNgPosition.x = f5Count > 0 ? 2 * _this.options.rateX : _this.options.rateX;
				/**物理负载**/
				
				/**组件负载**/
				_this.options.nextCompNgPosition = {
					x : 0,
					y : 0
				};
				_this.options.nextCompNgPosition.y = compNgCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
				_this.options.nextCompNgPosition.x = f5Count > 0 ? 2 * _this.options.rateX : _this.options.rateX;
				/**组件负载**/
				
				/**容器**/
				_this.options.nextDockerPosition = {
					x : 3 * _this.options.rateX,
					y : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2)
				};
				_this.options.nextDockerPosition.y = dockerCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
				_this.options.nextDockerPosition.x = f5Count == 0 ? _this.options.nextDockerPosition.x - _this.options.rateX : _this.options.nextDockerPosition.x; 
				_this.options.nextDockerPosition.x = phyNgCount == 0 && compNgCount == 0 ? _this.options.nextDockerPosition.x - _this.options.rateX : _this.options.nextDockerPosition.x; 
				/**容器**/
				
				/**数据源**/
				_this.options.nextDbPosition = {
					x : 4 * _this.options.rateX,
					y : _this.options.rateY
				};
				_this.options.nextDbPosition.y = dbCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
				_this.options.nextDbPosition.x = f5Count == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x; 
				_this.options.nextDbPosition.x = phyNgCount == 0 && compNgCount == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x;
				_this.options.nextDbPosition.x = dockerCount == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x; 
				/*定位*/
		    
//		        _this.cy.nodes().each(function(i) {
//                    var position = this.json().position;
//                    newPosition.x = newPosition.x > position.x || newPosition.x == 0 ? position.x : newPosition.x;
//                });
//		        newPosition.x = newPosition.x - 10;
		        _this.cy.nodes().each(function(i) {
		        	var position = this.json().position,
		        		isOk = false;
	            		id = this.data().id;
		        	
		        	if(id == 'f5' || id == 'compNginx' || id == 'conNginx' || id == 'docker' || id == 'db'){
				            newPosition.y = newPosition.y + 42;
				            this.animate({
				                position : {
				                    x : newPosition.x,
				                    y : newPosition.y
				                }
				            }, {
				                duration : 200
				            });
		        	}
		            if (id == 'f5' || (isOk = id.indexOf('f5_') == 0)){
		            	if(isOk){
		            		if(id.indexOf('_exception') > 0)
		            			_this.options.f5Storage[id] = id.substring(id.indexOf('_') + 1,id.lastIndexOf('_'));
		            		else
		            			_this.options.f5Storage[id] = id.substring(id.indexOf('_') + 1);
		            	}
		            	_this.addF5Bg(this, isOk,_this.options.f5Position);
		            }else if (id == 'compNginx' || (isOk = id.indexOf('compNginx_') == 0)){
		            	if(isOk){
		            		_this.options.ngStorage[id] = {
	            				nginx : this.data().name,
	            				nginxId : this.data().nginxId,
	            				nginxPort:this.data().nginxPort,
            					nginxPortId:this.data().nginxPortId
		            		};
		            	}
		            	_this.options.nextPhyNgPosition = _this.addNginxBg(this,id.indexOf('compNginx_') == 0,_this.options.nextPhyNgPosition);
		            }else if (id == 'conNginx' || (isOk = id.indexOf('conNginx_') == 0)){
		            	if(isOk){
		            		if(id.indexOf('_exception') > 0)
		            			_this.options.ngStorage[id] = id.substring(id.indexOf('_') + 1,id.lastIndexOf('_'));
		            		else
		            			_this.options.ngStorage[id] = id.substring(id.indexOf('_') + 1);
		            	}
		            	_this.options.nextCompNgPosition = _this.addConNginxBg(this,isOk,_this.options.nextCompNgPosition);
		            }else if (id == 'docker' || (isOk = id.indexOf('docker_') == 0)){
		            	if(isOk){
		            		if(id.indexOf('_exception') > 0)
		            			_this.options.dockerStorage[id] = id.substring(id.indexOf('_') + 1,id.lastIndexOf('_'));
		            		else
		            			_this.options.dockerStorage[id] = id.substring(id.indexOf('_') + 1);
		            	}
		            	_this.options.nextDockerPosition = _this.addDockerBg(this, id.indexOf('docker_') == 0,_this.options.nextDockerPosition);
		        	}else if (id == 'db' || (isOk = id.indexOf('db_') == 0)){
		        		if(isOk){
		            			_this.options.dbStorage[id] = {
			            				type : this.data().database,
			    	                    url : this.data().jndi,
			    	                    drive : this.data().driver,
			    	                    initSql : this.data().initSql
			            		};
		            	}
		            	_this.options.nextDbPosition = _this.addDbBg(this, id.indexOf('db_') == 0,_this.options.nextDbPosition);
		            }
		        });
		        
		        this.supportimages().addSupportImage({
					name : 'jquery',
					url : '../img/back.png',
					locked : true,
					bounds : {
						x : -35,
						y : 0
					}
				});
		        
		        var winHeight = $(window).height() - 45 - 40 - 10;
		        
		        winHeight = winHeight > _this.options.nextCompNgPosition.y ? winHeight : _this.options.nextCompNgPosition.y + countY * 4;
		        winHeight = winHeight > _this.options.nextDockerPosition.y ? winHeight : _this.options.nextDockerPosition.y + countY * 4;
				winHeight = winHeight > _this.options.nextDbPosition.y ? winHeight : _this.options.nextDbPosition.y + countY * 4;
				
		        winHeight = winHeight < 445 ? 445 : winHeight;
		        
		        $('#form_module').height(winHeight - 10);
		        $('#canvasup').height(winHeight + 10);
		        $('#sidebar').height(winHeight + 40 + 10);
		        
		        var elements = $(_this.cy.container()),
		        backElement = elements.find('#support-image-extension-div'),
		        canvas = elements.find('canvas');
		    
		    
		        elements.height(winHeight);
		        elements.find('canvas').height(winHeight);
		        backElement.height(winHeight).find('canvas').height(winHeight);
		        _this.cy.resize();
		        _this.cy.nodes('[id="f5"],[id="compNginx"],[id="conNginx"],[id="docker"],[id="db"]').on('vmousedown', _this.mousedown(this));
		        _this.cy.nodes('[id="f5"],[id="compNginx"],[id="conNginx"],[id="docker"],[id="db"]').on('vmouseup', _this.mouseup(this));
		    });
		},
		mousedown : function() {
			var _this = this;
			return function(){
				var data = this.data(), nowPosition = this.position();
		        if (data.sourceNode) {
		            if (!_this.options.position) {
		            	_this.options.position = {
		                    x : nowPosition.x,
		                    y : nowPosition.y
		                }
		            }
		        }
			}
	    }, mouseup : function() {
	    	var _this = this;
	    	return function(){
	    		var data = this.data(), nowPosition = this.position(), positionX = _this.options.position.x, positionY = _this.options.position.y, timestamp = (new Date())
		                .getTime();
		        if (positionX != nowPosition.x && positionY != nowPosition.y
		                && nowPosition.x - positionX > 50) {
		
		            if (data.id == 'f5'
		                    && _this.cy.nodes('[id^="' + _this.options.f5Variable + '"]').length > 0) {
		            } else if (data.id.indexOf('compNginx') == 0
		                    && _this.cy.nodes('[id^="' + _this.options.NginxVariable + '"]').length > 0) {
		            } else if (data.id.indexOf('conNginx') == 0
		                    && _this.cy.nodes('[id^="' + _this.options.conNginxVariable + '"]').length > 0) {
		//          } else if (data.id.indexOf('docker') == 0
		//                  && _this.cy.nodes('[id^="' + dockerVariable + '"]').length > 0) {
		            } else {
		                var node = {
		                    data : {
		                        id : data.id + '_' + timestamp,
		                        name : data.name
		                    },
		                    position : {
		                        x : nowPosition.x,
		                        y : nowPosition.y
		                    },
		                    group : "nodes"
		                }
		                node = this.cy().add(node);
		                _this.addParamsFunction(node);
		            }
		        }
		        this.animate({
		            position : {
		                x : positionX,
		                y : positionY
		            },
		        }, {
		            duration : 100
		        });
		        _this.options.position = undefined;
	    	}
	    },addQtipFun : function(key, content, type) {
	        var node = this.cy.nodes('#' + key), nodeParams = node.data();

	        if (key.indexOf(this.options.dbVariable) == 0) {
	            content = content;
	        }
	        if (!type) {
	            this.cy.nodes('#' + key).off('tap');
	        }
	        node.data().name = content;
	        node.qtip({
	            content : function() {
	                return this.data().name;
	            },
	            show : {
	                event : 'mouseover',
	            },
	            hide : {
	                event : 'mouseout',
	            },
	            position : {
	                my : 'top right',
	                at : 'top left'
	            },
	            style : {
	                classes : 'qtip-bootstrap'
	            }
	        });

	    }, addF5SelectedFun : function(key, type,_this) {
	        var element = $('#arrangement_open_f5 option:selected');
	        if (element.length > 0) {
	            var v = element.val(), text = element.text();
	            if (key && v)
	            	_this.options.f5Storage[key] = v;
	            _this.addQtipFun(key, text, type);
	        }
	    }, addConNgSelectedFun : function(key, type,_this) {
	        var element = $('#arrangement_open_componentNginx option:selected');
	        if (element.length > 0) {
	            var v = element.val(), text = element.text();
	            if (key && v)
	            	_this.options.ngStorage[key] = v;
	            _this.addQtipFun(key, text, type);
	        }
	    }, addPhysicalNgSelectedFun : function(key, type,_this) {
	        var element = $('#arrangement_open_Nginx option:selected');
	        if (element.length > 0) {
	            var v = element.val(), text = element.text(), nginxIp_=element.attr("ip"),
		            nginxPort_=$('#arrangement_open_nginxPort option:selected'),
	        		nginxPort_v=nginxPort_.val(),
	        		nginxPort_text=nginxPort_.text();
	            if (key && v)
	            	_this.options.ngStorage[key] ={
	            		nginx:text,
	            		nginxId:v,
	            		nginxPort:nginxPort_text,
	            		nginxPortId:nginxPort_v
	            };
//	            _this.addQtipFun(key, text+':'+v+':'+nginxPort_text+':'+nginxPort_v, type);马云浩0802改
	            _this.addQtipFun(key, text+':'+nginxPort_text, type);
	        }
	        nginxPort=nginxPort_text;
	        nginxIp=nginxIp_;
	        loadBalanceType=2;
	        setDomainName();
	    }, 
	    addDcokerFun : function(key, type,_this) {
	        var element = $('#arrangement_open_docker');
	        if (element.length > 0) {
	            var v = element.val();
	            if (key && v)
	            	_this.options.dockerStorage[key] = v;
//	          addQtipFun(key, v);
	        }
	    }, 
	    addDbSelectedFun : function(key, type,_this) {
	        var element = $('#arrangement_open_db option:selected');
	        if (element.length > 0) {
	            var v = element.val(), text = element.text(), 
	            		url = $('#arrangement_open_db_url').val(),
	                    drive = $('#arrangement_open_drive option:selected'),
	                    drive_v = drive.val(),
	                    drive_text = drive.text(),
	                    initSql_v = $('#arrangement_open_initSql').val();
	            if (key && v && url)
	                _this.options.dbStorage[key] = {
	                    type : v,
	                    url : url,
	                    drive : drive_v,
	                    initSql : initSql_v
	                };
//	            _this.addQtipFun(key, text + ':' + url + ':' + drive_text, type);马云浩0802改
	            _this.addQtipFun(key, text);
	        }
	    }, removeSelectedFun : function(key) {
	        if (key) {
	            if (this.options.f5Storage[key])
	                delete this.options.f5Storage[key];
	            if (this.options.ngStorage[key])
	                delete this.options.ngStorage[key];
	            if (this.options.dbStorage[key])
	                delete this.options.dbStorage[key];
	        }
	    }, addParamsFunction : function(node) {
	        var data = node.data(), type = data && data.id
	                && data.id.substring(0, data.id.indexOf('_')), option = {};

	        switch (type) {
	        case 'f5':
	            option = {
	                title : 'F5',
	                html : this.options.f5Html,
	                success : this.addF5SelectedFun
	            };
	            this.addF5Bg(node);
	            break;
	        case 'compNginx':
	            option = {
	                title : 'nginx组',
	                html : this.options.NginxHtml+ this.options.NginxPort,
	                success : this.addPhysicalNgSelectedFun
	            };
	            this.addNginxBg(node);
	            break;
//	        case 'conNginx':
//	            option = {
//	                title : '容器nginx',
//	                html : this.options.containerNginxHtml,
//	                success : this.addConNgSelectedFun
//	            };
//	            this.addConNginxBg(node);
//	            break;
	        case 'docker':
	            option = {
	                title : '容器',
	                html : this.options.dockerHtml,
	                success : this.addDcokerFun
	            };
//	          addQtipFun(data.id, data.name);
	            this.addDockerBg(node);
	            break;
	        case 'db':
	            option = {
	                title : '数据源',
	                html : this.options.dbHtml,
	                beforFun : this.arrangement_open_dbChange,
	                success : this.addDbSelectedFun
	            };
	            this.addDbBg(node);
	            break;
	        default:
	            console.info('nothing');
	        }
	        option.node = node;
	        option.type = type;
	        option.relation = true;

	        this.addHtmlFunction(option);
	    }, addHtmlFunction : function(option) {
	    	var _this = this;
	        if (option.title && option.html) {
	            bootbox
	                    .dialog({
	                        title : option.title,
	                        message : option.html,
	                        buttons : {
	                            "success" : {
	                                "label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='arrangement_save'></i> <b>保存</b>",
	                                "className" : "btn-sm btn-danger btn-round",
	                                "callback" : function(a, b, c) {
	                                    if (option.success && typeof (option.success) == 'function')
	                                        option.success(option.node.data().id, option.relation,_this);
	                                    if (option.relation)
	                                    	_this.createRelation(option);
	                                }
	                            },
	                            "cancel" : {
	                                "label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
	                                "className" : "btn-sm btn-warning btn-round",
	                                "callback" : function() {
	                                    if (option.relation)
	                                        option.node.remove();
	                                }
	                            }
	                        }
	                    });
//	          if ($('#arrangement_open_db_url').length > 0) {
//	              $("#arrangement_save").parent().attr("disabled", "disabled");
//	          }
	            if ($('#arrangement_open_Nginx').length > 0&&$('#arrangement_open_nginxPort').length > 0) {
	                var data = _this.options.ngStorage['' + option.node.data().id];
	                if(data){
	                    $('#arrangement_open_Nginx').val(data.nginxId);
	                    $('#arrangement_open_nginxPort').val(data.nginxPortId);
	                }
	            }
	            if ($('#arrangement_open_drive').length > 0) {
	                var data = _this.options.dbStorage['' + option.node.data().id];
	                if(data){
	                    $('#arrangement_open_db').val(data.type);
	                    $('#driverId').val(data.drive);
	                    $('#arrangement_open_drive').val(data.drive);
	                    $('#arrangement_open_db_url').val(data.url);
	                    $('#arrangement_open_initSql').val(data.initSql);
	                }
	            }
	            if(option.beforFun && typeof (option.beforFun) == 'function'){
	                option.beforFun();
	            }
//	          $("#arrangement_save").parent().attr("disabled","disabled");
	        } else {
	        	_this.createRelation(option);
	        }
	    },canvasHeight : function(position,number){
	        var h = number  * 40 + position.y,
	            winHeight = $(window).height() - 45 - 40 - 10;
	        
	        winHeight = winHeight < 445 ? 445 : winHeight;
	        
	        canvasGrapHeight = h > canvasGrapHeight ? h : canvasGrapHeight;
	        
	        canvasGrapHeight = canvasGrapHeight < winHeight ? winHeight : canvasGrapHeight;

	        var elements = $(this.cy.container()),
	            backElement = elements.find('#support-image-extension-div'),
	            canvas = elements.find('canvas');
	        
	        $('#form_module').height(canvasGrapHeight - 10);
	        $('.main-container').height(canvasGrapHeight - 10);
	        $('#sidebar').height(canvasGrapHeight + 40 + 10);
	        
	        elements.height(canvasGrapHeight);
	        elements.find('canvas').height(canvasGrapHeight);
	        backElement.height(canvasGrapHeight).find('canvas').height(canvasGrapHeight);
	        this.cy.resize();
	        
	    },addDockerNodeFun : function(id,number){
	        var node = this.cy.nodes('#' + id),
	            position = node.position(),
	            nodeHeight = node.height(),
	            nginxArray = this.cy.nodes("[id^='" + this.options.NginxVariable + "'],[id^='" + this.options.conNginxVariable + "']"),
	            dbArray = nodeArray = this.cy.nodes("[id^='" + this.options.dbVariable + "']"),
	            tempNode = node,
	            tempNodeId = 0;
	        
	        this.canvasHeight(position,number);
	        
	        if (nginxArray.length == 0) {
	            nginxArray = this.cy.nodes("[id^='" + this.options.f5Variable + "']");
	        }
	        --number;
	        for(var i = 0; i < number; i++){
	            tempNodeId = id + '_' + i + '_'+ (new Date()).getTime();
	            tempNode = {
	                data : {
	                    id : tempNodeId
	                },
	                position : {
	                    x : position.x,
	                    y : position.y + nodeHeight * (i + 1)
	                },
	                group : "nodes"
	            }
	            tempNode = this.cy.add(tempNode);
	            this.cy.add(this.getArrayForRelation(nginxArray, tempNodeId, true));
	            this.cy.add(this.getArrayForRelation(dbArray, tempNodeId, false));
	            this.addDockerBg(tempNode);
	        }
	    }, getArrayForRelation : function(array, id, isTarget) {
	        var addNodes = [], node = undefined, nodeId = undefined;
	        $.each(array, function(a, node) {
	            nodeId = node.data().id;
	            if (isTarget) {
	                node = {
	                    data : {
	                        id : nodeId + id,
	                        source : nodeId,
	                        target : id
	                    },
	                    group : "edges"
	                }
	            } else {
	                node = {
	                    data : {
	                        id : id + nodeId,
	                        source : id,
	                        target : nodeId
	                    },
	                    group : "edges"
	                }
	            }
	            addNodes.push(node);
	        });
	        return addNodes;
	    }, removeRelation : function(id) {
	    	var _this = this,nodeArray = nodeArray = _this.cy.edges("[id^='" + id + "'],[id$='" + id  + "']");
	        $.each(nodeArray, function(a, node) {
	            _this.cy.remove(node);
	        });
	        nodeArray = _this.cy.nodes("[id^='" + _this.options.NginxVariable + "'],[id^='"
	                + _this.options.conNginxVariable + "']");

	        if ((id.indexOf(_this.options.NginxVariable) >= 0 || id.indexOf(_this.options.conNginxVariable) >= 0) && nodeArray.length == 0) {
	            id = _this.cy.nodes("[id^='" + _this.options.f5Variable + "']");
	            if (id.length > 0)
	                _this.addRelation({
	                    id : id.json().data.id,
	                    type : _this.options.f5Variable.substring(0, _this.options.f5Variable.length - 1)
	                });
	        }else if(id.indexOf(_this.options.dockerVariable) >= 0){
	            checkAddTopologyForm();
	        }
	        _this.removeSelectedFun(id);
	    }, addRelation : function(params) {
	    	var _this = this,nodeId = params.id, nodeArray = undefined, source = undefined, target = undefined;

	        if (params.type == 'f5') {
	            nodeArray = _this.cy.nodes("[id^='" + _this.options.NginxVariable + "'],[id^='" + _this.options.conNginxVariable + "']");
	            if (nodeArray.length == 0) {
	                nodeArray = _this.cy.nodes("[id^='" + _this.options.dockerVariable + "']");
	            }
	            _this.cy.add(_this.getArrayForRelation(nodeArray, nodeId, false));
	        } else if (params.type == 'compNginx' || params.type == 'conNginx') {
	            nodeArray = _this.cy.edges("[id^='" + _this.options.f5Variable + "']");
	            $.each(nodeArray, function(a, node) {
	                target = node.data().target;
	                if (target.indexOf(_this.options.dockerVariable) >= 0)
	                    _this.cy.remove(node);
	            });

	            source = _this.cy.nodes("[id^='" + _this.options.f5Variable + "']");
	            _this.cy.add(_this.getArrayForRelation(source, nodeId, true));

	            nodeArray = _this.cy.nodes("[id^='" + _this.options.dockerVariable + "']");
	            _this.cy.add(_this.getArrayForRelation(nodeArray, nodeId, false));
	        } else if (params.type == 'docker') {
	            checkAddTopologyForm();
	            
	            nodeArray = _this.cy.nodes("[id^='" + _this.options.NginxVariable + "'],[id^='" + _this.options.conNginxVariable + "']");
	            if (nodeArray.length == 0) {
	                nodeArray = _this.cy.nodes("[id^='" + _this.options.f5Variable + "']");
	            }
	            _this.cy.add(_this.getArrayForRelation(nodeArray, nodeId, true));
	            nodeArray = _this.cy.nodes("[id^='" + _this.options.dbVariable + "']");
	            _this.cy.add(_this.getArrayForRelation(nodeArray, nodeId, false));
	            _this.addDockerNodeFun(nodeId,_this.options.dockerStorage[nodeId]);
	        } else if (params.type == 'db') {
	            nodeArray = _this.cy.nodes("[id^='" + _this.options.dockerVariable + "']");
	            _this.cy.add(_this.getArrayForRelation(nodeArray, nodeId, true));
	        }
	    }, createRelation : function(option) {
	        var params = {
	            id : option.node.json().data.id,
	            type : option.type
	        }
	        this.addRelation(params);
	    }, addF5Bg : function(node,isMove,position) {
	        node.css('background-image', '../img/f5.svg');
	        if(isMove){
	        	node.position(position);
	        	position =  {
	    			x : position.x,
	    			y : position.y + this.options.rateY
	    		}
	        }
	        return position;
	    }, addNginxBg : function(node,isMove,position) {
	        node.css('background-image', '../img/phyNginx.svg');
	        if(isMove){
	        	node.position(position);
	        	position =  {
	    			x : position.x,
	    			y : position.y + this.options.rateY
	    		}
	        }
	        return position;
	    }, addConNginxBg : function(node,isMove,position) {
	        node.css('background-image', '../img/compNginx.svg');
	        if(isMove){
	        	node.position(position);
	        	position =  {
	    			x : position.x,
	    			y : position.y + this.options.rateY
	    		}
	        }
	        return position;
	    }, addDockerBg : function(node,isMove,position) {
	        node.css('background-image', '../img/appservice.svg');
	        if(isMove){
	        	node.position(position);
	        	position = {
        			x : position.x,
        			y : position.y + this.options.rateY
        		}
	        }
	        return position;
	        	
	    }, addDbBg : function(node,isMove,position) {
	        node.css('background-image', '../img/source.svg');
	        if(isMove){
	        	node.position(position);
	        	position =  {
	    			x : position.x,
	    			y : position.y + this.options.rateY
	    		}
	        }
	        return position;
	    }
}
jQuery(function($) {arrangement = new arrangementObject();
	canvas = $('#testProgress');
	connect();
	$(window).unload(function() {
		disconnect();
	});
});


function removefileli(obj){
	if ($("#deployFiles li").length > 1) {
		$("#deployFiles"+obj).remove();
		$("#process"+obj).remove();
	}
}

function uploadFiles(indexnum,fileName){
	versionSet();
	$("#deployFileName"+indexnum).val("");
	if(undefined!=deployConfiFile){
		deployConfiFile = deployConfiFile.replace(fileName,"");
	}
	if($("#arrangement_baseImageId").val()!=null){
		var deployFile = document.getElementById("deployFile"+indexnum).files[0];
        if(undefined == deployFile || null == deployFile){
        	$("#showId"+indexnum).hide();
        	showMessage("请选择配置文件！");
        	return;
        }
        var deployFileName = deployFile.name.split(".");
        if(!validate(deployFileName[0])){
        	return false;
        }
        if(deployFile!=null) {
        	 console.log(deployFile);
        	 count = parseInt(deployFile.size/paragraph)+1;
        	 $("#showId"+indexnum).text("开始上传！");
        	 $("#showId"+indexnum).show();
        	 var timestamp = $("#timestamp").val();
        	 var systemId = $("#systemId").val();
        	 if ('WebSocket' in window) {
        	        socket_upconfig.onmessage = function (event) {
        	        	console.log(event.data);
        	            var obj = JSON.parse(event.data);
        	            if(obj.messageType =="ws_up_file"){
        	            	console.log(obj);
        	                if(obj.content == "OK"){
        	            	    if(endSize < deployFile.size){
        	            	    	var blob;
        	            	        startSize = endSize;
        	            	        endSize += paragraph ;
        	            	        if (deployFile.webkitSlice) { 
        	            	              blob = deployFile.webkitSlice(startSize, endSize);
        	            	        } else if (deployFile.mozSlice) {
        	            	              blob = deployFile.mozSlice(startSize, endSize);
        	            	        }
        	            	        else{
        	            	        	 blob = deployFile.slice(startSize, endSize);
        	            	        }
        	            	        var reader = new FileReader();
        	            	        reader.readAsArrayBuffer(blob);
        	            	        reader.onload = function loaded(evt) {
        	                        var ArrayBuffer = evt.target.result;
        	                        i++;
        	                        var isok = (i / count) *100;
        	                        $("#showId"+indexnum).text("已经上传"+isok.toFixed(2) +"%");
        	                        socket_upconfig.send(ArrayBuffer);
        	                        };
        	            	    } else{
        	                        startSize = endSize = 0;
        	                        i = 0;
        	                        $("#showId"+indexnum).text("上传完成！");
        	                        $("#deployFileName"+indexnum).val(deployFile.name);
        	            	    }
        	                } else if(obj.content == "TRUE"){
        	                	$("#progressid"+indexnum).width("100%");
         	                	$("#showId"+indexnum).text("文件上传并转存完成！");
        	                	showMessage("文件["+deployFile.name+"]上传并转存完成！",hideProgress(indexnum));
//        	            	    socket_upconfig.close();
        	                } else if(obj.content == "FALSE"){
        	            	    $("#showId"+indexnum).text("转存失败！");
        	            	    $("#pid"+indexnum).hide();
        	            	    showMessage("文件["+deployFile.name+"]转存失败！",hideProgress(indexnum));
//        	            	    socket_upconfig.close();
        	                } else if(obj.content == "NOHOST"){
        	            	    $("#showId"+indexnum).text("仓库主机在数据库中不存在！");
        	            	    showMessage("文件["+deployFile.name+"]上传失败，仓库主机在数据库中不存在！",hideProgress(indexnum));
        	            	    $("#pid"+indexnum).hide();
//        	            	    socket_upconfig.close();
        	                } else if(obj.content == "DBERROR"){
        	            	    $("#showId"+indexnum).text("数据保存异常！");
        	            	    showMessage("文件["+deployFile.name+"]数据保存异常！",hideProgress(indexnum));
        	            	    $("#pid"+indexnum).hide();
//        	            	    socket_upconfig.close();
        	                } else if(obj.content == "CONNERROR"){
        	            	    $("#showId"+indexnum).text("仓库主机连接异常！");
        	            	    showMessage("文件["+deployFile.name+"]仓库主机连接异常！",hideProgress(indexnum));
        	            	    $("#pid"+indexnum).hide();
//        	            	    socket_upconfig.close();
        	                } else if(obj.content == "ISEXIT"){
        	            	    $("#showId"+indexnum).text("该文件已经存在！");
        	            	    showMessage("文件["+deployFile.name+"]该文件已经存在！",hideProgress(indexnum));
        	            	    $("#pid"+indexnum).hide();
//        	            	    socket_upconfig.close();
        	                } else{
        	                	console.log(obj.content);
//        	                	socket_upconfig.close();
        	                }
        	                
        	        	}
        	        };
        	        setTimeout(socket_upconfig.send(JSON.stringify({
	        	        'appFileStartName': deployFile.name,'webcontainer':$("#arrangement_baseImageId").val(),'systemId':systemId,'timestamp':timestamp
	            	})), 1000);
        	        
        	    } else {
        	        console.log('Websocket not supported');
        	    }
        }else{
        	showMessage("请先选择需要上传的配置文件！");
        	return;
    	}
	}else{
		showMessage("文件将上传到镜像仓库所在主机，请先选择基础镜像！");
    	return;
	}
}



function hideProgress(indexnum){
	$("#pid"+indexnum).hide();
	$("#process"+indexnum).addClass("hide");
}
//初始化集群
function getClusters(clusterId){
	$.ajax({
        type: 'get',
        url: base+'cluster/all',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var clustername = decodeURIComponent(obj.clusterName);
				var clusterStatus = obj.clusterStatus;
				//集群中的主机数量，如果小于等于1个，此集群没有节点
				var hostCount = obj.hostCount;
				if(clusterStatus==1&&hostCount>1){
					if(clusterId==obj.clusterId){
						$('#arrangement_cluster').append('<option value="'+obj.clusterId+'" selected="selected">'+clustername+'</option>');
					}else{
						$('#arrangement_cluster').append('<option value="'+obj.clusterId+'">'+clustername+'</option>');
					}
				}
				
            });
        }
	 });
}

function getImages(baseImageId){
	//加载镜像的列表
	$.get(base+'image/list?page=1&rows=65536&imageType=0',null,function(response){
		$.each (response.rows, function (index, obj){
			var tempName = obj.tempName;
			var imageStatus = obj.imageStatus;
			if(imageStatus==1){
				if(baseImageId==obj.imageId){
					$('#arrangement_baseImageId').append('<option value="'+obj.imageId+'" selected="selected">'+tempName+'</option>');
				}else{
					$('#arrangement_baseImageId').append('<option value="'+obj.imageId+'">'+tempName+'</option>');
				}
			}
        });
	},'json');
}
//部署位置层级
function location_leavel(locationCode){
	var url = base + "parameter/selectByType";
    $.ajax({
        url: url,
        type: 'get',
        data: 'type=0',
        // 加载方式
        dataType: 'json',
        // 返回数据格式
        success: function (response) {
        	var options = "";
            if (response == "") {
                showMessage("数据加载异常！");
            } else {
                $.each(response, function (index, data) {
                	if(locationCode==data.paraCode){
    					options += "<option value='"+data.paraCode+"' selected='selected'>"+data.paraValue+"</option>";
    				}else{
    					options += "<option value='"+data.paraCode+"'>"+data.paraValue+"</option>";
    				}
                	
                });
            }
            $("#arrangement_location_leavel").html(options);
        },
        error: function (response) {
            console.log(response);
        }
    });
}
function getSystemApp(systemId,systemAppId){
	$.ajax({
		type: 'get',
		url: base+'systemApp/list',
		dataType: 'json',
		data:{
			systemId:systemId
		},
		success: function (array) {
			$('#arrangement_systemAppId').empty();
			$('#arrangement_systemAppId').append('<option value="">请选择程序包</option>');
			$.each (array, function (index, obj){
				var appVersion = obj.appVersion;
				var appName = obj.appVersion;
				if(systemAppId==obj.id){
					var temp ='<option value="'+obj.id+'" selected="selected">'+appVersion+'</option>';
					$('#arrangement_systemAppId').append(temp);
				}else{
					var temp ='<option value="'+obj.id+'">'+appVersion+'</option>';
					$('#arrangement_systemAppId').append(temp);
				}
			});
		}
	});
}
//初始化部署配置文件
function deployConfigFile(deployFiles){
	$("#deployFiles").html('');
	if(!deployFiles||deployFiles.length==0){
		var filediv = '<li class="param" id="deployFiles0">'+
			'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
			'<label class="col-sm-3"> <div align="right"><b>配置文件：</b></div></label>'	+
			'<div class="col-sm-5"> '	+
			'<input type="text" name="deployFileName" id="deployFileName0" fileindex="0" value="" class="form-control" readonly="readonly" />'+			
			'</div>'	+
			'<div class="col-sm-4" >'	+
			'<input type="file" name="deployFile" id="deployFile0" fileindex="0" onchange="uploadFiles(0,\'\')" style="float: left;padding-left: 0px"/>'+		
			'</div>'	+
			'</div>'+
			'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
			'<label class="col-sm-3"> '	+
			'<div align="right"> <b>上传路径：</b> </div> '		+
			'</label>'	+
			'<div class="col-sm-7">'	+
			'<input type="text" name="filePath" id="filePath0" placeholder="输入配置文件上传路径" style="width:100%;border: 1px solid #ccc;" class="form-control">'+		
			'</div>'	+
			'<div class="col-sm-2" align="right" style="padding:0px">'	+
			'<a class="btn btn-primary btn-round" id="add-file" type="button" style="color: #fff; width: 60px;top: 5px;">'+			
			'<div style="margin: -7px -7px -7px -15px">'			+
			'<span class="glyphicon glyphicon-plus"></span> <span class="text">添加</span>'	+			
			'</div>'+			
			'</a>'	+	
			'</div>'+	
			'</div>'+
		'</li>';
		//添加配置文件选项
		$("#deployFiles").append(filediv);
		//隐藏配置文件添加按钮
		$("#add-file").removeClass("hide");
		$("#add-file").addClass("show");
	}else{
		$.each (deployFiles, function (index, obj){
			fileindex = index;
			var fileName = obj.fileName;
			var filePath = obj.filePath;
			var tmp = '';
			if(fileindex==0){
				tmp = '<div class="col-sm-2" align="right" style="padding:0px">'	+
				'<a class="btn btn-primary btn-round" id="add-file" type="button" style="color: #fff; width: 60px;top: 5px;">'+			
				'<div style="margin: -7px -7px -7px -15px">'			+
				'<span class="glyphicon glyphicon-plus"></span> <span class="text">添加</span>'	+			
				'</div>'+			
				'</a>'	+	
				'</div>';	
			}else{
				tmp = '<div class="col-sm-2" align="right" style="padding:0px">'	+
				'<a href="javascript:removefileli('+fileindex+',\''+fileName+'\');" id="remove-param" > <span class="glyphicon glyphicon-remove delete-param" style="top:7px;margin-bottom:5px"></span> </a>'	+	
				'</div>';
			}
			
			var filediv = '<li class="param" id="deployFiles'+fileindex+'">'+
				'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
				'<label class="col-sm-3"> <div align="right"><b>配置文件'+(fileindex+1)+'：</b></div></label>'	+
				'<div class="col-sm-5"> '	+
				'<input type="text" name="deployFileName" id="deployFileName'+fileindex+'" fileindex="'+fileindex+'" value="'+fileName+'" class="form-control" readonly="readonly" />'+			
				'</div>'	+
				'<div class="col-sm-4" >'	+
				'<input type="file" name="deployFile" id="deployFile'+fileindex+'" fileindex="'+fileindex+'" onchange="uploadFiles('+fileindex+',\''+fileName+'\')" style="float: left;padding-left: 0px"/>'+		
				'</div>'+
				'</div>'+
				'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
				'<label class="col-sm-3"> '	+
				'<div align="right"> <b>上传路径'+(fileindex+1)+'：</b> </div> '		+
				'</label>'	+
				'<div class="col-sm-7">'	+
				'<input type="text" name="filePath" id="filePath'+fileindex+'" value="'+filePath+'" placeholder="输入配置文件上传路径" style="width:100%;border: 1px solid #ccc;" class="form-control">'+		
				'</div>'	+
				tmp
				'</div>'+
			'</li>';
			//添加配置文件选项
			$("#deployFiles").append(filediv);
        });
	}
	//配置文件添加按钮触发事件
	$("#add-file").on('click', function(event) {
		fileindex =fileindex+1;
		if(fileindex==0){
			tmp = '<div class="col-sm-2" align="right" style="padding:0px">'	+
			'<a class="btn btn-primary btn-round" id="add-file" type="button" style="color: #fff; width: 60px;top: 5px;">'+			
			'<div style="margin: -7px -7px -7px -15px">'			+
			'<span class="glyphicon glyphicon-plus"></span> <span class="text">添加</span>'	+			
			'</div>'+			
			'</a>'	+	
			'</div>';	
		}else{
			tmp = '<div class="col-sm-2" align="right" style="padding:0px">'	+
			'<a href="javascript:removefileli('+fileindex+',\'\');" id="remove-param" > <span class="glyphicon glyphicon-remove delete-param" style="top:7px;margin-bottom:5px"></span> </a>'	+	
			'</div>';
		}
		
		var filediv = '<li class="param" id="deployFiles'+fileindex+'">'+
			'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
			'<label class="col-sm-3"> <div align="right"><b>配置文件'+(fileindex+1)+'：</b></div></label>'	+
			'<div class="col-sm-5"> '	+
			'<input type="text" name="deployFileName" id="deployFileName'+fileindex+'" fileindex="'+fileindex+'"  class="form-control" readonly="readonly" />'+			
			'</div>'	+
			'<div class="col-sm-4" >'	+
			'<input type="file" name="deployFile" id="deployFile'+fileindex+'" fileindex="'+fileindex+'" onchange="uploadFiles('+fileindex+',\'\')" style="float: left;padding-left: 0px"/>'+		
			'</div>'	+
			'</div>'+
			'<div class="item col-sm-12" style="margin-left:-15px;padding-top: 5px">'+
			'<label class="col-sm-3"> '	+
			'<div align="right"> <b>上传路径'+(fileindex+1)+'：</b> </div> '		+
			'</label>'	+
			'<div class="col-sm-7">'	+
			'<input type="text" name="filePath" id="filePath'+fileindex+'" placeholder="输入配置文件上传路径" style="width:100%;border: 1px solid #ccc;" class="form-control">'+		
			'</div>'	+
			tmp
			'</div>'+
		'</li>';
		//添加配置文件选项
		$("#deployFiles").append(filediv);
	});
}
//域名检查
function checkDomainName(){
	validToolObj.isNull('#domainName_') || validToolObj.realTimeValid(base + 'system/checkDomainName',$('#deployId').val(),'#domainName_','域名已存在，请重新输入');
	setDomainName();
}
//域名设置
function setDomainName(){
	$("#domainName").val("");
	var DNSCheckbox = document.getElementById("DNSEnable");
	if(DNSCheckbox.checked) {
		$("#domainName").val($("#domainName_").val());
		if(loadBalanceType==1){//f5
			$("#port").val($("#f5ExternalPort").find('option:selected').text());
		}else if(loadBalanceType==2){//nginx
			$("#port").val(nginxPort);
		}
	}else {
		if(loadBalanceType==1){//f5
			$("#port").val($("#f5ExternalPort").find('option:selected').text());
			$("#domainName").val($("#arrangement_open_f5").attr("ip"));
		}else if(loadBalanceType==2){//nginx
			$("#port").val(nginxPort);
			$("#domainName").val(nginxIp);
		}
	}
}
function versionSet(){
	var versionPre = $("#arrangement_systemAppId").find('option:selected').text();
	if(isNull($("#arrangement_systemAppId").val())){
		$("#arrangement_deployVersion").val("");
		showMessage("请选择应用程序包！");
		return false;
	}
	var timestamp = $("#timestamp").val();
	$("#arrangement_deployVersion").val(versionPre+"_"+timestamp);
	//新版本标记
	$("#newVersion").val(true);
	checkAddTopologyForm();
}

//实例数
function checkInstance(){
	validToolObj.isNull('#instanceCount') ||validToolObj.number('#instanceCount');
}
function checkContentPath(){
	$("#appName").val($("#contentPath").val());
	validToolObj.isNull('#contentPath');
}
//CPU
function checkCPU(){
	if(!isNull($("#cpu").val())){
		validToolObj.number('#cpu');
	}
//	validToolObj.isNull('#cpu') ||validToolObj.number('#cpu');
}
//内存
function checkMemery(){
	if(!isNull($("#memery").val())){
		validToolObj.number('#memery');
	}
}
function checkXmxmax(){
	if(!isNull($("#Xmxmax").val())){
		validToolObj.number('#Xmxmax');
	}
	if($("#Xmxmax").val()<$("#Xmxmin").val()){
		showMessage("JVM最大内存不能小于最小内存！");
		flag=false;
	}
}
function checkXmxmin(){
	if(!isNull($("#Xmxmin").val())){
		validToolObj.number('#Xmxmin');
	}
	if($("#Xmxmax").val()<$("#Xmxmin").val()){
		showMessage("JVM最大内存不能小于最小内存！");
		flag=false;
	}
}
//应用名
function checkAppName(){
	validToolObj.isNull('#appName');
}
function validF5(){
	validToolObj.isNull('#arrangement_open_f5');
	checkTopologyForm(['#arrangement_open_f5']);
}
function validPhyNginx(){
	validToolObj.isNull('#arrangement_open_Nginx');
	checkTopologyForm(['#arrangement_open_Nginx']);
}
function validCompNginx(){
	validToolObj.isNull('#arrangement_open_componentNginx');
	checkTopologyForm(['#arrangement_open_componentNginx']);
}
function validDb(){
	if(!validToolObj.isNull('#arrangement_open_db')){
		arrangement_open_dbChange();
	}
	checkTopologyForm(['#arrangement_open_db','#arrangement_open_drive','#arrangement_open_db_url']);
}
function validDrive(){
	validToolObj.isNull('#arrangement_open_drive');
	checkTopologyForm(['#arrangement_open_db','#arrangement_open_drive','#arrangement_open_db_url']);
}
function validUrl(){
	validToolObj.isNull('#arrangement_open_db_url');
	checkTopologyForm(['#arrangement_open_db','#arrangement_open_drive','#arrangement_open_db_url']);
}

function checkTopologyForm(inputArray){
	validToolObj.validForm('#topology_form',"#arrangement_save",inputArray);
}

//目标集群
function checkTargetCluster(){
	validToolObj.isNull('#arrangement_cluster');
	checkAddTopologyForm();
}

//基础镜像
function checkarrBaseImageId(){
	validToolObj.isNull('#arrangement_baseImageId');
	checkAddTopologyForm();
}
function checkAddTopologyForm(){
	if(validToolObj.validForm('#arrangement_form',"#arrangement_create",['#arrangement_deployVersion','#arrangement_cluster','#arrangement_baseImageId'],false)){
		var instanceCount = arrangement.cy.nodes('node[id^="docker_"]').length;
		if(instanceCount == 0){
			showMessage("请加入容器！");
			$('#arrangement_create').attr("disabled","disabled");
		}else{
			$('#arrangement_create').removeAttr("disabled");
		}
			
	}else{
		$('#arrangement_create').attr("disabled","disabled");
	}
}