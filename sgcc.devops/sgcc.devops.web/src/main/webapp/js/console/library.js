var grid_selector = "#library_list";
var page_selector = "#library_page";
var timestamp;
var socket_libraryupfile = undefined;
$().ready(function(){
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
		url : base + 'library/list',
		datatype : "json",
		height : '100%',
		autowidth: true,  
        shrinkToFit: true,
		colNames : [ 'ID','状态','名称','说明', '文件目录','创建人','创建时间'],//,'弹性伸缩设置'
		colModel : [
		            {name : 'libraryId',index : 'libraryId',width : 15,sortable : false,hidden:true},
		            {name : 'status',index : 'status',sortable : false,hidden:true},
					{name : 'libraryName',index : 'libraryName',width : 15,hidden:false},
					{name : 'libraryDescription',index : 'libraryDescription',sortable : false,width : 11},
					{name : 'libraryFilePath',index : 'libraryFilePath',sortable : false,width : 9},
					{name : 'libraryCreatorName',index : 'libraryCreatorName',width : 6},
					{name : 'libraryCreatetime',index : 'libraryCreatetime',width : 6}
				],
		viewrecords : true,
		rowNum : 10,
		rowList : [10, 10, 20, 50, 100, 1000 ],
		pager : page_selector,
		altRows : true, 
		sortname: 'libraryName',
		sortorder: "asc",
		multiselect: true,
		multiboxonly:true,
		jsonReader : {
			root : "rows",
			total : "total",
			page : "page",
			records : "records",
			repeatitems : false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
});

function searchLibrary(){
	var libraryName = $('#libraryName').val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'library/list?libraryName='+libraryName}).trigger("reloadGrid");
}

//全局的websocket变量
var paragraph = 4*1024*1024;//64*1024
var libraryFile;
var startSize,endSize = 0;
var i = 0; j = 0;
var count =0;
var socket_upconfig = undefined;

/**
 * 上传应用包文件 开始
 * @param libraryFileStartName
 */
function sendLibraryFileStart(libraryFileStartName){
	socket_libraryupfile.send(JSON.stringify({
        'libraryFileStartName': libraryFileStartName
	}));
}
//发送文件上传消息
function sendfileEnd(){
	socket_libraryupfile.send(JSON.stringify({
	        'libraryFileSendover': 'libraryFileSendover'
	  }));
}
function libraryUpDisconnect(){
//	socket.close();
	socket_libraryupfile.close();
    console.log("Disconnected");
}
/**
 * 上传应用包文件
 * @param obj
 */
function sendLibraryUpArraybuffer(obj){
	console.log(obj);
    if(obj.content == "OK"){
	    if(endSize < libraryFile.size){
	    	var blob;
	        startSize = endSize;
	        endSize += paragraph ;
	        if (libraryFile.webkitSlice) { 
	              blob = libraryFile.webkitSlice(startSize, endSize);
	        } else if (libraryFile.mozSlice) {
	              blob = libraryFile.mozSlice(startSize, endSize);
	        }
	        else{
	        	 blob = libraryFile.slice(startSize, endSize);
	        }
	        var reader = new FileReader();
	        reader.onload = function loaded(evt) {
	            var ArrayBuffer = evt.target.result;
	            i++;
	            var isok = (i / count) *100;
	            $("#libraryShowId").text("已经上传"+isok.toFixed(2) +"%");
	            $("#libraryProgressId").width((isok.toFixed(2)*0.7) +"%");
	            socket_libraryupfile.send(ArrayBuffer);
            };
            reader.readAsArrayBuffer(blob);
	    } else{
            startSize = endSize = 0;
            i = 0;
            $("#libraryShowId").text("上传完成！");
            $("#libraryProgressId").width("100%");
            sendfileEnd();
            libraryUpDisconnect();
	    }
    }  else{
    	console.log(obj.content);
    }
}

function libraryUpConnect(){
	 if ('WebSocket' in window) {
        var host = window.location.host+base+"/";
        if(window.location.protocol=='http:'){
        	socket_libraryupfile = new WebSocket('ws://' + host + '/uploadService');
        }else if(window.location.protocol=='https:'){
        	socket_libraryupfile = new WebSocket('wss://' + host + '/uploadService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        
        socket_libraryupfile.onopen = function () {};
        socket_libraryupfile.onclose = function () {};
        socket_libraryupfile.onerror = function(e) {};
        socket_libraryupfile.onmessage = function (event) {
        	console.log(event.data);
            var obj = JSON.parse(event.data);
            if(obj.messageType =="ws_up_file"){
        	    sendLibraryUpArraybuffer(obj);
        	}
            if(obj.libraryFilePath && obj.libraryUuid){
            	$("#libraryFilePath").val(obj.libraryFilePath);
            	$("#libraryId").val(obj.libraryUuid);
            }
        };
    } else {
        console.log('Websocket not supported');
    }
}
function validate(filename){
	var reg = new RegExp("[u4e00-u9fa5]");
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]");
    if(!reg.test(filename) && !pattern.test(filename)){
    	bootbox.alert("上传的文件名不能包含汉字和特殊字符");
    	return false;
    }else{
    	return true;
    }
}
function initLibraryUpDialog(){
	libraryUpConnect();
	if (window.File && window.FileReader && window.FileList && window.Blob){
		$('#upLibrary').change(function() {
			libraryFile = document.getElementById("upLibrary").files[0];
	        if(undefined == libraryFile || null == libraryFile){
	        	showMessage("请选择应用文件！");
	        	return;
	        }
	        var libraryFileName = libraryFile.name.split(".");
	        if(!validate(libraryFileName[0])){
	        	showMessage("上传文件包含非法字符");
	        	return;
	        }
	        if ("jar" != libraryFileName[libraryFileName.length - 1]) {
				showMessage("只能上传.jar文件！");
				return;
			}
	        if(libraryFile!=null) {
	        	 console.log(libraryFile);
	        	 count = parseInt(libraryFile.size/paragraph)+1;
	        	 $("#libraryPid").show();
	        	 $("#libraryShowId").text("开始上传！");
	        	 sendLibraryFileStart(libraryFile.name);
	        }else{
	        	showMessage("请先选择需要上传的应用文件！");
	        	return;
        	}
	    });
	}else {
	    	bootbox.alert('您的浏览器不支持html5 Web socket 上传功能.');
	}

}
function showCreateLibraryWin(){
	timestamp=Math.round(new Date().getTime()/1000);
	var html = template('setLibrary_template');
	//初始化文件上传进度条
	$("#libraryPid").hide();
	$("#upLibrary").val("");
	$("#libraryProgressId").width("0%");
	//初始化应用包上传页面
	bootbox.dialog({
		title : "<b>添加预加载包</b>",
		message : html,
		buttons : {
			"success" : {
				"label" : "<i class='icon-ok' id='careteLibraryButt'></i> <b>新建</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
					var libraryFilePath = $("#libraryFilePath").val();
					var libraryName = $("#addlibraryName").val();
					var libraryDescription = $("#libraryDescription").val();
					if(libraryFilePath.length==0 ){
						showMessage('文件未上传成功，无法新建预加载包记录');
						return false;
					}else if(isNull(libraryName)){
						showMessage('请输入名称');
						return false;
					}else{
						createLibrary(libraryFilePath,libraryName,libraryDescription);
					}
				}
			},
			"cancel" : {
				"label" : "<i class='icon-info'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {}
			}
		}
	});
	initLibraryUpDialog();
	$("#careteLibraryButt").parent().attr("disabled",'disabled');
}
function createLibrary(libraryFilePath,libraryName,libraryDescription){
	var data = {
			libraryName : libraryName,
			libraryDescription : libraryDescription,
			libraryFilePath :libraryFilePath
	};
	$.ajax({
        type: 'get',
        url : base + '/library/create',
        dataType: 'json',
        data:data,
        success: function (data) {
        	showMessage('新建预加载包成功');
        	jQuery(grid_selector).jqGrid().trigger("reloadGrid");
        },
        error:function(response){
        	showMessage('新建预加载包失败'+ data.logDetail);
        	return false;
        }
	});
}

//新建-预加载包文件校验
function checkUpLibrary(){
	validToolObj.isNull('#upLibrary',true);
	checkLibraryCreate();
}
//新建-名称校验
function checkAddlibraryName(){
	validToolObj.isNull('#addlibraryName',true);
	checkLibraryCreate();
}
function checkLibraryCreate(){
    validToolObj.validForm('#add_library_frm',"#careteLibraryButt",['#upLibrary','#addlibraryName']);
}

//编辑-名称校验
function checkUpdatelibraryName(){
	validToolObj.isNull('#editLibraryName',true);
	checkLibraryUpdate();
}
function checkLibraryUpdate(){
    validToolObj.validForm('#update_library_frm',"#update_library_Butt",['#editLibraryName']);
}
function updateLibraryWin(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var libraryId =rowData.libraryId;
		var libraryName = rowData.libraryName;
		var libraryDescription = rowData.libraryDescription;
		var libraryFilePath = rowData.libraryFilePath;
		var status = rowData.status;
		
		var url= base+'library/update';
		var title  = '<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>&nbsp;<b>编辑预加载包信息</b>';
		bootbox.dialog({
			title : title,
			message :"<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='update_library_frm'>"+
						"<div class='form-group'>"+
				  			"<label class='col-sm-3'><div align='right'><b>文件目录：</b></div></label>"+
				  			"<div class='col-sm-9'>"+
    	      					"<input type='text' readonly='readonly' value='"+libraryFilePath+"'class='form-control'/>"+
    	      				"</div>"+
	    	      		"</div>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>名称：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='editLibraryName' onblur='checkUpdatelibraryName();' type='text' value='"+libraryName+"'class='form-control'/>"+
		    	      			"</div>"+
		    	      			"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      		"</div>"+
				      		"<div class='form-group'>"+
				      			"<label class='col-sm-3'><div align='right'><b>预加载包说明：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<textarea id='editLibraryDescription' class='form-control' rows='3'>"+libraryDescription+"</textarea>"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      		"<input type='hidden' id='editLibraryId' value='"+libraryId+"'/>"+
		    	      	"</form>"+
		    	      "</div>"+
				"</div>",
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='update_library_Butt'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						var libraryId = $('#editLibraryId').val();
						var libraryName = $('#editLibraryName').val();
						var libraryDescription = $('#editLibraryDescription').val();
						data={
								libraryId:libraryId,
								libraryName:libraryName,
								libraryDescription:libraryDescription,
								status:status
								};
						$.post(url,data,function(response){
								showMessage(response.message);
								$(grid_selector).trigger("reloadGrid");
						});
					}
				},
				"cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
						$(grid_selector).trigger("reloadGrid");
					}
				}
			}
		});
	}
}
function removeLibrary(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		bootbox.dialog({
		    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;你确定要删除此记录吗?</div>',
		    title: "提示",
		    buttons: {
		        main: {
		        	label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
		            callback: function () {
		            	var rowData = $(grid_selector).jqGrid("getRowData",ids);
						var libraryId =rowData.libraryId;
						data = {
								libraryId:libraryId
						};
						$.ajax({
					        type: 'get',
					        url : base + '/library/deleteLibrary',
					        dataType: 'json',
					        data:data,
					        success: function (data) {
					        	if(data.success){
					        		bootbox.alert('删除成功');
						        	$(grid_selector).trigger("reloadGrid");
					        	}else{
					        		showMessage(data.message);
						        	$(grid_selector).trigger("reloadGrid");
					        	}
					        },
					        error:function(response){
					        	bootbox.alert('删除失败'+ data.logDetail);
					        	return false;
					        }
						});
		            }
		        },
		        cancel: {
		        	label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
		            callback: function () {
		            }
		        }
		    }
		});
	}
}
////上传文件
//function UpladFile() {
//    var fileObj = document.getElementById("upLibrary").files[0]; // 获取文件对象
//    var FileController = "/file/uploadFile";                    // 接收上传文件的后台地址 
//    // FormData 对象
//    var form = new FormData();
//    form.append("timestamp", timestamp); 
//    form.append("file", fileObj); 
//    form.append("fileType", "library");// 上传文件类型
//    // XMLHttpRequest 对象
//    var xhr = new XMLHttpRequest();
//    xhr.open("post", FileController, true);
//    xhr.onload = function (resp) {
//    	$("#libraryFilePath").val(timestamp+"/"+fileObj.name);
//    	showMessage("上传完成!");
//    };
//    xhr.upload.addEventListener("progress", progressFunction, false);
//    $("#progressBar").attr("hidden",false);
//    xhr.send(form);
//}
//function progressFunction(evt) {
//    var progressBar = document.getElementById("progressBar");
//    var percentageDiv = document.getElementById("percentage");
//    if (evt.lengthComputable) {
//        progressBar.max = evt.total;
//        progressBar.value = evt.loaded;
//        percentageDiv.innerHTML = Math.round(evt.loaded / evt.total * 100) + "%";
//        if(evt.loaded==evt.total){
//        	setTimeout('$("#progressBar").attr("hidden",true)', 2000);
//        }
//    }
//
//} 