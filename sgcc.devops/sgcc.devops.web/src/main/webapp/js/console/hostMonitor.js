$(document).ready(
		  //一开始pie图无数据
		function() {
			var step = 2;
			var host = $("#hostId").val();
			try {
				ace.settings.check('breadcrumbs', 'fixed');
			} catch (e) {
			}
			$('#host_cpu_chart').css('height', 300);
			$('#host_mem_chart').css('height', 300);
			$('#host_net_chart').css('height', 300);
			$('#host_df_chart').css('height', 400);
			// 配置路径
			require.config({
				paths : {
					echarts : base + 'plugins/echarts'
				}
			});
			// 按需加载
			require([ 'echarts', 'echarts/chart/line' ], function(ec) {
				echarts = ec;
			});
			require([ 'echarts', 'echarts/chart/bar' ], function(ec) {
				echarts = ec;
			});

			var cpuChartObj = document.getElementById('host_cpu_chart');
			var memChartObj = document.getElementById('host_mem_chart');
			var netChartObj = document.getElementById('host_net_chart');
			var dfChartObj = document.getElementById('host_df_chart');
			var cpuChart = echarts.init(cpuChartObj);
			var memChart = echarts.init(memChartObj);
			var netChart = echarts.init(netChartObj);
			var dfChart = echarts.init(dfChartObj);
			getInitDiskInfo();
			cpuChart.showLoading({
				text : '数据加载中...'
			});
			memChart.showLoading({
				text : '数据加载中...'
			});
			netChart.showLoading({
				text : '数据加载中...'
			});
			dfChart.showLoading({
				text : '数据加载中...'
			});
			var dates = [];
			var cpus = [];
			var mems = [];
			var nins = [];
			var nous = [];
			var df = [];
			$.ajax({
				url : base + 'monitor/getMonitorData?id=' + host + "&type=0",
				async : false,
				success : function(json) {
					dates = json.time;
					cpus = json.cpu;
					mems = json.mem;
					nins = json.nin;
					nous = json.nou;

					if (dates == null || dates.length == 0) {
						var now = new Date();
						var len = 20;
						while (len--) {
							dates.unshift(now.pattern("HH:mm:ss"));
							now = new Date(now - step * 1000);
							cpus.push(0);
							mems.push(0);
							nins.push(0);
							nous.push(0);
						}
					}
				}
			});
			var cpuOption = {
				title : {
					text : '物理机CPU负载监控',
					subtext : '百分比'
				},
				tooltip : {
					show : true
				},
				legend : {
					data : [ 'CPU负载' ]
				},
				xAxis : [ {
					type : 'category',
					data : dates,
					splitNumber : 20
				} ],
				yAxis : [ {
					type : 'value',
					axisLabel : {
						formatter : '{value} %'
					}
				} ],
				series : [ {
					'name' : 'CPU负载',
					'type' : 'line',
					'data' : cpus,
					'smooth' : true
				} ]
			};
			var memOption = {
				title : {
					text : '主机内存使用监控',
					subtext : '单位：百分比'
				},
				tooltip : {
					show : true
				},
				legend : {
					data : [ '内存负载' ]
				},
				xAxis : [ {
					type : 'category',
					data : dates,
					splitNumber : 40
				} ],
				yAxis : [ {
					type : 'value',
					axisLabel : {
						formatter : '{value} %'
					}
				} ],
				series : [ {
					'name' : '内存负载',
					'type' : 'line',
					'data' : mems,
					'smooth' : true
				} ]
			};
			var netOption = {
				title : {
					text : '网络IO监控',
					subtext : '单位：KB/S'
				},
				tooltip : {
					show : true
				},
				legend : {
					data : [ '网络收包速率', '网络发包速率' ]
				},
				xAxis : [ {
					type : 'category',
					data : dates,
					splitNumber : 20
				} ],
				yAxis : [ {
					type : 'value',
					axisLabel : {
						formatter : '{value} KB/S'
					}
				} ],
				series : [ {
					'name' : '网络收包速率',
					'type' : 'line',
					'data' : nins,
					'smooth' : true
				}, {
					'name' : '网络发包速率',
					'type' : 'line',
					'data' : nous,
					'smooth' : true
				} ]
			};
			cpuChart.setOption(cpuOption);
			cpuChart.hideLoading();
			memChart.setOption(memOption);
			memChart.hideLoading();
			netChart.setOption(netOption);
			netChart.hideLoading();
			cpu = 0;
			mem = 0;
			nin = 0;
			nou = 0;
			date = null;
         
			timeTicket = setInterval(function() {
				$.getJSON(base + 'monitor/host/' + host, function(json) {
					date = new Date().pattern("HH:mm:ss");
					cpu = json.cpu;
					mem = json.mem;
					nin = json.nin;
					nou = json.nou;
					df = json.diskinfo;
				});
				if(date != null){
					// 动态数据接口 addData
					cpuChart.addData([ [ 0, // 系列索引
					cpu, // 新增数据
					false, // 新增数据是否从队列头部插入
					false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
					date ] ]);
					memChart.addData([ [ 0, // 系列索引
					mem, // 新增数据
					false, // 新增数据是否从队列头部插入
					false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
					date // 坐标轴标签
					] ]);
					netChart.addData([ [ 0, // 系列索引
					nin, // 新增数据
					false, // 新增数据是否从队列头部插入
					false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
					date ], [ 1, // 系列索引
					nou, // 新增数据
					false, // 新增数据是否从队列头部插入
					false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
					date // 坐标轴标签
					] ]);
					createDiskInfo(df);
				}
			}, step * 1000);

			// 选择框
			$("#timeSelect").change(function() {
				var model = $("#timeSelect").val();
				if (model == 0) {
					window.location.reload();
				} else if (model == 1) {
					window.clearInterval(timeTicket);
					addData(1, cpuChart, memChart, netChart);
				} else if (model == 2) {
					window.clearInterval(timeTicket);
					addData(4, cpuChart, memChart, netChart);
				} else if (model == 3) {
					window.clearInterval(timeTicket);
					addData(12, cpuChart, memChart, netChart);
				} else if (model == 4) {
					window.clearInterval(timeTicket);
					addData(24, cpuChart, memChart, netChart);
				}

			});
			// 获取并添加数据
			function addData(model, cpuChart, memChart, netChart) {
				cpuChart.clear();
				memChart.clear();
				netChart.clear();

				cpuChart.showLoading({
					text : '数据加载中...'
				});
				memChart.showLoading({
					text : '数据加载中...'
				});
				netChart.showLoading({
					text : '数据加载中...'
				});

				// var dates = [];
				// var cpus = [];
				// var mems = [];
				// var nins = [];
				// var nous = [];

				$.getJSON(base + 'monitor/host/getData?hostId=' + host
						+ "&model=" + model, function(json) {
					var dates = [];
					var cpus = [];
					var mems = [];
					var nins = [];
					var nous = [];

					dates = json.time;
					cpus = json.cpu;
					mems = json.mem;
					nins = json.nin;
					nous = json.nou;

					if (dates == null || dates.length == 0) {
						var now = new Date();
						var len = 5;
						var step = model * 60 / 4;
						while (len--) {
							dates.unshift(now.pattern("HH:mm:ss"));
							now = new Date(now - step * 1000 * 60);
							cpus.push(0);
							mems.push(0);
							nins.push(0);
							nous.push(0);
						}
					}

					var cpuOption = {
						title : {
							text : '物理机CPU负载监控',
							subtext : '百分比'
						},
						tooltip : {
							show : true
						},
						legend : {
							data : [ 'CPU负载' ]
						},
						xAxis : [ {
							type : 'category',
							data : dates,
							splitNumber : 20
						} ],
						yAxis : [ {
							type : 'value',
							axisLabel : {
								formatter : '{value} %'
							}
						} ],
						series : [ {
							'name' : 'CPU负载',
							'type' : 'line',
							'data' : cpus,
							'smooth' : true
						} ]
					};
					var memOption = {
						title : {
							text : '主机内存使用监控',
							subtext : '百分比'
						},
						tooltip : {
							show : true
						},
						legend : {
							data : [ '内存负载' ]
						},
						xAxis : [ {
							type : 'category',
							data : dates,
							splitNumber : 40
						} ],
						yAxis : [ {
							type : 'value',
							axisLabel : {
								formatter : '{value} %'
							}
						} ],
						series : [ {
							'name' : '内存负载',
							'type' : 'line',
							'data' : mems,
							'smooth' : true
						} ]
					};
					var netOption = {
						title : {
							text : '网络IO监控',
							subtext : '单位：KB/S'
						},
						tooltip : {
							show : true
						},
						legend : {
							data : [ '网络收包速率', '网络发包速率' ]
						},
						xAxis : [ {
							type : 'category',
							data : dates,
							splitNumber : 20
						} ],
						yAxis : [ {
							type : 'value',
							axisLabel : {
								formatter : '{value} KB/S'
							}
						} ],
						series : [ {
							'name' : '网络收包速率',
							'type' : 'line',
							'data' : nins,
							'smooth' : true
						}, {
							'name' : '网络发包速率',
							'type' : 'line',
							'data' : nous,
							'smooth' : true
						} ]
					};
					cpuChart.setOption(cpuOption);
					cpuChart.hideLoading();
					memChart.setOption(memOption);
					memChart.hideLoading();
					netChart.setOption(netOption);
					netChart.hideLoading();
				});

			}
			function createDiskInfo(data) {
			    var   fileSystem=[];
			    var   mountedon=[];
			    var   avail     =[];
			    var   size      =[];
			    var   used      =[];
			    //设备，挂载目录    size和used取出来是int类型 /100保留2位小数
			    for(var i in data){
			    	fileSystem[i]=data[i].Filesystem+"("+data[i].Mountedon+")";
			    	avail[i]=data[i].Avail/100;
			    	size[i]=data[i].Size/100;
			    	used[i]=data[i].Used/100;
			    }
			    option = {
			    		title : {
							text : '磁盘使用情况',
						},
			    	    tooltip : {
			    	        trigger: 'axis',
			    	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
			    	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			    	        }
			    	    },
			    	    legend: {
			    	        data:['已使用','未使用']
			    	    },
			    	    grid: {
			    	        left: '3%',
			    	        right: '4%',
			    	        bottom: '3%',
			    	        containLabel: true
			    	    },
			    	    xAxis : [
			    	        {
			    	            type : 'category',
			    	            data : fileSystem
			    	        }
			    	    ],
			    	    yAxis : [
			    	        {
			    	        	name: '单位(G)',
			    	            type : 'value',
			    	        }
			    	    ],
			    	    series : [
			    	        {
			    	            name:'已使用',
			    	            type:'bar',
			    	            stack: '使用状况',
			    	            itemStyle : { normal: {label : {show: true, position: 'inside'}}},
			    	            data:used
			    	            
			    	        },
			    	        {
			    	            name:'未使用',
			    	            type:'bar',
			    	            stack: '使用状况',
			    	            itemStyle : { normal: {label : {show: true, position: 'inside'}}},
			    	            data:avail
			    	        },
			    	    ]
			    	};
				dfChart.setOption(option);
				dfChart.hideLoading();  
			}
			//初始化无数据，其他三个是从数据库中取得 ，这里直接访问control取得
			function  getInitDiskInfo(){
				var df =[];
				$.getJSON(base + 'monitor/host/' + host,function(json) {
					df = json.diskinfo;
					createDiskInfo(df);
			});
			}
			// function dynamicGetData(host,cpuChart,memChart,netChart) {
			// $.getJSON(base + 'monitor/host/' + host, function(json) {
			// // date = new Date().toLocaleTimeString().replace(/^\D*/,'');
			// date = new Date().pattern("HH:mm:ss");
			// cpu = json.cpu;
			// mem = json.mem;
			// nin = json.nin;
			// nou = json.nou;
			// });
			//		
			// }

		});
