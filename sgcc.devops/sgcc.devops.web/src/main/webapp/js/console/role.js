var grid_selector = "#role_list";
var page_selector = "#role_page";

var authTreeData = [];
var setting = {
    check: {
        enable: true
    },
    data: {
        simpleData: {
            enable: true
        }
    }
};
function createZTreeFun(selector) {
    var tree = $.fn.zTree.init($(selector), setting, authTreeData);
    tree.expandAll(true);
}
function destroyZTreeFun(selector) {
    $.fn.zTree.destroy(selector);
}

jQuery(function($) {
    $(window).on('resize.jqGrid',
    function() {
        $(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
        $(grid_selector).closest(".ui-jqgrid-bdiv").css({
            'overflow-x': 'hidden'
        });
    });
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    $(document).on('settings.ace.jqGrid',
    function(ev, event_name, collapsed) {
        if (event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed') {
            setTimeout(function() {
                $(grid_selector).jqGrid('setGridWidth', parent_column.width());
            },
            0);
        }
    });
    jQuery(grid_selector).jqGrid({
        url: base + 'role/all',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: ['角色ID', '角色名', '角色描述', '角色标记', '角色状态', '快捷操作'],
        colModel: [{
            name: 'roleId',
            index: 'roleId',
            width: 10,
            hidden: true
        },
        {
            name: 'roleName',
            index: 'roleName',
            width: 10,
            formatter: function(cell, opt, obj) {
                return '<i class="fa fa-cubes"></i><a href="' + base + 'role/detail/' + obj.roleId + '.html">' + cell + '</a>';
            }
        },
        {
            name: 'roleDesc',
            index: 'roleDesc',
            width: 10
        },
        {
            name: 'roleRemarks',
            index: 'roleRemarks',
            width: 10,
            hidden: true
        },
        {
            name: 'roleStatus',
            index: 'roleStatus',
            width: 10,
            formatter: function(cellvalue, options, rowObject) {
                switch (rowObject.roleStatus) {
                case 1:
                    return '正常';
                default:
                    return '注销';
                }
            }
        },
        {
            name: '',
            title:false,
            index: '',
            width: 120,
            fixed: true,
            sortable: false,
            resize: false,
            formatter: function(cellvalue, options, rowObject) {
                var strHtml = "";

                var upda = $("#update_role").val();
                if (typeof(upda) != "undefined") {
                    strHtml += "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"modifyRoleWin('" + rowObject.roleId + "','" + rowObject.roleName + "','" + rowObject.roleDesc + "','" + rowObject.roleRemarks + "')\"><b>编辑</b></button> &nbsp;";
                }
                return strHtml;

            }
        }],
        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 50, 100, 1000],
        pager: page_selector,
        altRows: true,
        multiselect: true,
        multiboxonly:true,
        jsonReader: {
            root: "rows",
            total: "total",
            page: "page",
            records: "records",
            repeatitems: false
        },
        loadComplete: function() {
            var table = this;
            setTimeout(function() {
                updatePagerIcons(table);
                enableTooltips(table);
            },
            0);
        }
    });
    $(window).triggerHandler('resize.jqGrid'); // 窗口resize时重新resize表格，使其变成合适的大小
    jQuery(grid_selector).jqGrid( //分页栏按钮
    'navGrid', page_selector, { // navbar options
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: true,
        refreshstate: 'current',
        refreshicon: 'ace-icon fa fa-refresh',
        view: false
    },{},{},{},{},{});

    function updatePagerIcons(table) {
        var replacement = {
            'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
            'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
            'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
            'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
        };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function() {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
            if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
        })
    }

    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({
            container: 'body'
        });
        $(table).find('.ui-pg-div').tooltip({
            container: 'body'
        });
    }

    //修改角色提交按钮
    $('#modify_submit').click(function() {
        if ($("#modify_role_form").valid()) {
            url = base + 'role/update';
            id = $('#role_id_edit').val();
            name = $('#role_name_edit').val();
            desc = $('#role_desc_edit').val();
            roleRemarks = $('#role_remarks_edit').val();

            data = {
                roleId: id,
                roleName: name,
                roleDesc: desc,
                roleRemarks: roleRemarks
            };
            $('#modifyRoleModal').modal('hide');
            $.post(url, data,
            function(response) {
                if (response == "") {
                    showMessage("修改角色异常！");
                } else {
                    showMessage(response.message);
                }
                $(grid_selector).trigger("reloadGrid");
            });
        }
    });

    //编辑角色取消按钮
    $('#modify_cancel').click(function() {
        $(grid_selector).trigger("reloadGrid");
        $('#modifyRoleModal').modal('hide');
        $('#modify_role_form')[0].reset();
        $('label.error').remove();
    });

    //编辑角色信息校验
    $("#modify_role_form").validate({
        rules: {
            role_name_edit: {
                required: true,
                stringCheck: true,
                maxlength: 20
            },
            role_desc_edit: {
                maxlength: 200,
                stringCheck: true
            },
            role_remarks_edit: {
                required: true,
                stringCheck: true,
                maxlength: 20
            }
        },
        messages: {
            role_name_edit: {
                required: "角色名不能为空",
                maxlength: $.validator.format("角色名不能大于20个字符")
            },
            role_desc_edit: {
                maxlength: $.validator.format("角色描述不能大于200个字符")
            },
            role_remarks_edit: {
                required: "角色标记不能为空",
                maxlength: $.validator.format("角色标记不能大于20个字符")
            }
        }
    });

    //角色授权按钮
    $('#authToRole').click(function() {
        var rowData = "";
        var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
        var roles = "";
        for (var i = 0; i < ids.length; i++) {
            rowData = $(grid_selector).jqGrid("getRowData", ids[i]);
            roles += (i == ids.length - 1 ? rowData.roleId: rowData.roleId + ",");
        }
        if (ids.length > 1) {
            $(grid_selector).trigger("reloadGrid");
            showMessage("一次只能给一个角色授权！");
            return;
        }
        if (ids.length > 0) {
            if (rowData.roleRemarks == 1) {
                $(grid_selector).trigger("reloadGrid");
                showMessage("不能给管理员授权！");
            } else {
                var treeStr = "";
                $.ajax({
                    url: base + "role/roleAuth",
                    data: {
                        roleId: roles
                    },
                    type: 'get',
                    dataType: 'json',
                    success: function(obj) {
                        $.each(obj,function(index, json) {
                            treeStr += json.actionId + ",";
                        });
                        createZTreeFun("#roleAuthTree");
                        //替换为ztree
                        var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
                        var treeStrSplit = treeStr.split(",");
                        var treeIds = [];
                        for (var i = 0; i < treeStrSplit.length; i++) {
                            var node = tree.getNodeByParam('id', treeStrSplit[i], null);
                            if (node) tree.checkNode(node, true);
                        }
                        $('#authToRoleModal').modal('show');
                    }
                });
            }
        } else {
            $(grid_selector).trigger("reloadGrid");
            showMessage("请先选中角色，再授权！");
        }
    });

    //
    $('#authAuth_submit').click(function() {
        var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
        var roles = "";
        for (var i = 0; i < ids.length; i++) {
            var rowData = $(grid_selector).jqGrid("getRowData", ids[i]);
            roles += (i == ids.length - 1 ? rowData.roleId: rowData.roleId + ",");
        }
        var tree = $.fn.zTree.getZTreeObj('roleAuthTree'),
        nodeId = '',
        items = tree.getCheckedNodes();
        if(items.length==0){
        	showMessage("至少选择一个权限进行授权！");
        }else{
        	for (var i in items) if (items.hasOwnProperty(i)) {
        		var item = items[i];
        		nodeId += item.id + ',';
        	}
        	nodeId = nodeId.substring(0, nodeId.length - 1);
        	authToRoles(roles, nodeId);
        }
    });

    //角色授权隐藏按钮
    $('#authAuth_cancel').click(function() {
        $(grid_selector).trigger("reloadGrid");
        $('#authToRoleModal').modal('hide');
    });

    //替换ztree
    $.ajax({
        url: base + "auth/tree",
        //要加载数据的地址
        type: 'get',
        //加载方式
        dataType: 'json',
        //返回数据格式
        success: function(response) {
            if (response == "") {
                showMessage("权限数据加载异常！");
            } else {
                authTreeData = response.data;
            }
        },
        error: function(response) {
            console.log(response);
        }
    });

    /**
	 * 向查询按钮添加请求提交操作
	 */
    $("#advanced_role_search").on('click',
    function(event) {
        /* 保存各项栏目的名称数组 */
        var column_array = new Array();
        /* 保存用户填写的各项信息数组 */
        var value_array = new Array();

		/* 获取选择栏目的名称 */
		$("select[name=meter]").each(function() {
			column_array.push($(this).val());
		});
		
		/** @bug208_begin [高级查询]当输入重复的条件列及搜索值,查询结果取其交集，禁用重复选择******* */
		/** 判断查询的键值是否存在重复的内容。* */
		var sort_array = column_array.sort();
		for (var count = 0; count < sort_array.length; count++) {
			if (sort_array[count] == sort_array[count + 1]) {
				showMessage("查询【列名称】选择重复，请重新选择后进行查询！");
				return;
			}
		}
		/** @bug208_finish************************************** */
		
		/* 获取填写的参数值信息 */
		$("input[name=search_role_value]").each(function() {
			value_array.push($(this).val());
		});
		/* 查询是否存在关键词相关的结果 */
		jQuery(grid_selector).jqGrid(
				'setGridParam',
				{
					url : base + 'role/advancedSearch?params='
							+ column_array + '&values=' + value_array
				}).trigger("reloadGrid");
		//
		$('#advancedSearchRoleModal').modal('hide');
		$('#advanced_search_frm')[0].reset();

		/** @bug152_begin 清空用户多选的参数 */
		while ($("#params li").length > 1) {
			$("#remove-param").parent().remove();
		}
		/** @bug152_finish */
	});


    /**
	 * 向高级搜索的取消按钮添加重置隐藏
	 */
    $("#advanced_role_cancel").on('click',
    function(event) {
        event.preventDefault();
        $('#advancedSearchRoleModal').modal('hide');
        $('#advanced_search_frm')[0].reset();
    });
    /**
	 * 添加高级搜索的参数项
	 */
	$("#add-param").on('click', function(event) {
		event.preventDefault();
		$("#params li:first").clone(true).appendTo("#params");
		$("#params li").not(":first").find("#remove-param").show();
		$("#params li:first").find("#remove-param").hide();
		var str = $("#params li:last").find("#meter").val();
		/** @bug152_begin 新增查询参数时，新增栏参数内容置空 */
		$("#params li:last").find("#search_role_value").val("");
		/** @bug152_finish */
	});


    /**
	 * 删除高级索索的参数项
	 */
    $("#remove-param").on('click',
    function(event) {
        event.preventDefault();
        if ($("#params li").length > 1) {
            $(this).parent().remove();
        }
    });
});


/**
 * 修改角色信息
 * @param id	角色id
 * @param name	角色名称
 * @param desc	角色描述
 * @param remarks	角色标记
 */
function modifyRoleWin(id, name, desc, remarks) {
    $("#role_id_edit").attr("value", id);
    $("#role_name_edit").attr("value", name);
    $("#role_desc_edit").attr("value", (desc == null || desc == "null") ? "": desc);
    $("#role_remarks_edit").attr("value", remarks);
    $("#modifyRoleModal").modal('show');
}

/**
 * 根据输入的角色名进行模糊查询
 */
function searchRoles() {
    var roleName = $('#search_role').val();
    jQuery(grid_selector).jqGrid('setGridParam', {
        url: base + 'role/all?roleName=' + roleName
    }).trigger("reloadGrid");
}

/**
 * 角色授权
 * @param roles		角色
 * @param auths		权限
 */
function authToRoles(roles, auths) {
    if (roles == null) {
        showMessage("请选择角色！");
        $(grid_selector).trigger("reloadGrid");
    } else {
        var url = base + 'role/authToRole';
        data = {
            roles: roles,
            auths: auths
        };
        $('#authToRoleModal').modal('hide');
        $.post(url, data,
        function(response) {
            showMessage("角色授权成功！");
            $(grid_selector).trigger("reloadGrid");
        });
    }
}

//获取权限列表
function getAuthlist() {
    $("#authbody").html("");
    $.ajax({
        url: base + 'auth/list',
        data: {},
        dataType: "json",
        success: function(obj) {
            var tableStr = "";
            $.each(obj,
            function(index, json) {
                var thistr = '<tr rowid="' + json.actionId + '">';
                thistr += '<td class="rcheck"><input type="checkbox" name="checkbox_auth"></td><td name="id">' + json.actionId + '</td><td name="name">' + json.actionName + '</td></tr>';
                tableStr += thistr;
            });
            $('#authbody').html(tableStr);
        }
    });
}

function AdvancedSearchRoles() {
	/** @bug152_begin 清空用户多选的参数 */
	while ($("#params li").length > 1) {
		$("#remove-param").parent().remove();
	}
	/* 隐藏高级查询第一行的删除打叉按钮 */
	$("#params li:first").find("#remove-param").hide();
	/** 打开高级搜索窗口，之前输入全部清空 */
	$("#params li:first").find("#search_role_value").val("");
	$("#params li:first").find("#meter").val("0");
	/** @bug152_finish */

	$('#advancedSearchRoleModal').modal('show');
}
