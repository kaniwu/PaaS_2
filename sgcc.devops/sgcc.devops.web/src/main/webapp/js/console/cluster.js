var grid_selector = "#cluster_list";
var page_selector = "#cluster_page";
jQuery(function($) {
	
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
		url : base+'cluster/list',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','UUID','集群名称', '主服务器ID','所在主机','服务端口','用户ID','','创建人','备用主机','备用主机ID','创建时间','描述','环境','状态','快捷操作'],
		colModel : [ 
            {name : 'clusterId',index : 'clusterId',width : 20,hidden:true},
            {name : 'clusterUuid',index : 'clusterUuid',width : 15,hidden:true},
			{name : 'clusterName',index : 'clusterName',width : 10,
//            	formatter:function(cellvalue, options, rowObject){
//            		return '<i class="fa fa-university"></i><a href="'+base+'cluster/clusterHost?clusterId='+rowObject.clusterId+'">'+ cellvalue+'</a>';
			},
			{name : 'masteHostId',index : 'masteHostId',width : 10,hidden:true},
			{name : 'hostIP',index : 'hostIP',width : 10,align:'left'},
			{name : 'clusterPort',index : 'clusterPort',width : 5},
			{name : 'clusterCreator',index : 'clusterCreator',width : 5,hidden:true},
			{name : 'managePath',index : 'managePath',width : 10,hidden:true},
			{name : 'creatorName',index : 'creatorName',width : 10,align:'left'},
			{name : 'standByHostIP', index :'standByHostIP',width :10,hidden:true},
			{name : 'standByHostId', index :'standByHostId',width :10,hidden:true},
			{name : 'clusterCreatetime',index : 'clusterCreatetime',width : 10},
			{name : 'clusterDesc',index : 'clusterDesc',width : 15,hidden:true},
			{name : 'clusterEnv',index : 'clusterEnv',width : 15,hidden:true},
			{
		    	name : 'clusterStatus',
				index : 'clusterStatus',
				width : 150,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					switch (cellvalue) {
					case 1: return '正常    '
					+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"stopSwarm('"+rowObject.clusterId+"',5)\">"
					+"<i class=\"ace-icon fa fa-stop bigger-125\"></i>"
					+"<b>停止</b></button> &nbsp;";
					default:return "异常    "
					+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"startSwarm('"+rowObject.clusterId+"')\">"
					+"<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
					+"<b>启动</b></button> &nbsp;";
					}
				}
		    },
			{
		    	name : '',
				index : '',
				width : 280,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					rowObject.clusterDesc = stringTool.escape(rowObject.clusterDesc);
					var buts = "<a href='#modal-wizard' data-toggle='modal'><button class=\"btn btn-xs btn-primary btn-round\" onclick=\"showClusgerHostWin('"+rowObject.clusterId+"','"+rowObject.hostIP+"','"+rowObject.clusterName+"')\">"
					+"<i class=\"ace-icon fa fa-cogs bigger-125\"></i>"
					+"<b>节点管理</b></button></a> &nbsp;";
					
					if(rowObject.clusterStatus!=1){
						buts = "<a><button class=\"btn btn-xs btn-primary btn-round disabled\" >"
						+"<i class=\"ace-icon fa fa-cogs bigger-125\"></i>"
						+"<b>节点管理</b></button></a> &nbsp;";
					}
					buts+="<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"helthCheck('"+rowObject.clusterId+"')\">"
					+"<i class=\"ace-icon fa fa-cogs bigger-125\"></i>"
					+"<b>健康检查</b></button> &nbsp;"
					+"<a href='#migrateCluster' data-toggle='modal'><button class=\"btn btn-xs btn-danger btn-round\" onclick=\"showmigrateClusterWin('"+rowObject.clusterId+"','"+rowObject.hostIP+"','"+rowObject.clusterName+"')\">"
					+"<i class=\"ace-icon fa fa-external-link bigger-125\"></i>"
					+"<b>迁移</b></button></a> &nbsp;";
					return  buts;
				}
		    }
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
		
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {}
});

//
function removeCluster(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.clusterId;
		var clusterName =rowData.clusterName;
		var url = base+"cluster/delete";
		$("cluster_id").attr("value",id);
		data = {clusterId:id};
		bootbox.dialog({
	        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除集群  ('+clusterName+') 吗?</div>',
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	$("#spinner").css("display","block");
	                	$.post(url,data,function(response){
	                		$("#spinner").css("display","none");
	                		showMessage(response.message);
	    					$(grid_selector).trigger("reloadGrid");
	    				});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}
}

//
function showCreateClusterWin(){
	getAllRegistryLb();
	getClusterMster();
	bootbox.dialog({
		title : "<b>添加集群</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='add_item_frm'>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>集群名称：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"cluster_name\" name='cluster_name' type='text' class=\"form-control\"  placeholder='请输入集群名称' onblur='checkClusterName();'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>集群端口：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"cluster_port\" name='cluster_port' type='number' class=\"form-control\" onblur='checkClusterPort();'  placeholder='请输入集群端口' value='5010'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>所在主机：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
				  					"<select id='hostMaster_select' name='hostMaster_select' class='form-control' onchange='checkHost();'>"+
				  					"<option value=\"0\">请选择主机</option>"+
 				  					"</select>"+
				  				"</div>"+
				  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
			  				"<label class='col-sm-3'><div align='right'><b>备用主机：</b></div></label>"+
			  				"<div class='col-sm-9'>"+
			  					"<select id='standby_host_select' name='standby_host_select' class='form-control' >"+
			  					"<option value=\"0\">请选择主机</option>"+
				  					"</select>"+
			  				"</div>"+
//			  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
    	      			"</div>"+
    	      			"<div class='form-group'  >"+
	  				        "<label class='col-sm-3'><div align='right'><b>仓库名称：</b></div></label>"+
	  				            "<div class='col-sm-9'>"+
	  					           "<select id='registry_select' name='registry_select' class='form-control' >"+
	  					              "<option value=\"0\">请选择仓库</option>"+
		  					        "</select>"+
	  				      "</div>"+
	  				    "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
    			       "</div>"+
    			       "<div class='form-group'  >"+
	 				        "<label class='col-sm-3'><div align='right'><b>环境：</b></div></label>"+
	 				            "<div class='col-sm-9'>"+
	 					           "<select id='clusterEnv' name='clusterEnv' class='form-control' onchange='checkClusterCreate()'></select>"+
	 				      "</div>"+
	 				    "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
				       "</div>"+
	    	      			"<div class='form-group hide'>"+
				  				"<label class='col-sm-3'><div align='right'><b>管理文件：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id=\"cluster_manage\" name='cluster_manage' type='text' class=\"form-control\" onblur='checkManage();'  placeholder='请输入管理文件,格式为字母、数字、下划线'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
			      				"<label class='col-sm-3'><div align='right'><b>集群描述：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<textarea id=\"cluster_desc\" onblur=\"validToolObj.length('#cluster_desc',200)\" name=\"cluster_desc\" class=\"form-control\" rows=\"3\"></textarea>"+
	    	      				"</div>"+
	    	      			"</div>"+
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
			            var clustername = $('#cluster_name').val();
			            var clusterport = $('#cluster_port').val();
			            var masterip = $('#hostMaster_select').val();
			            var standByIp = $('#standby_host_select').val();
			            var clumanagepath = $('#cluster_manage').val();
			            var clusterdesc = $('#cluster_desc').val();
			            var registryLbId = $('#registry_select').val();
			            var clusterEnv = $('#clusterEnv').val();
			            if(registryLbId==0){
			            	showMessage("必须要选择仓库，如不存在，请新建仓库！");
			            	return false;
			            }
			            if($('#hostMaster_select').val() ==0){
			            	showMessage("请选择创建集群所在的主机！");
			            	$(grid_selector).trigger("reloadGrid");
			            }else{
				       			data2={
				       					hostID:masterip,
				       					type:"swarm"
				       			};
				       			var url2 = base + 'host/getVersion';
				       			$("#spinner").css("display","block");
				       			$.post(url2,data2,function(response){
				       			$("#spinner").css("display","none");
			        			bootbox.dialog({
					        	    message:"<div class='well ' style='margin-top:1px;padding-bottom:1px;'>"+
									"<form class='form-horizontal' role='form' >"+ 
									"<div class='form-group'  >"+
									"<label class='col-sm-4'><div align='right'><b>docker当前版本：</b></div></label>"+
									 "<div class='col-sm-8'>"+
		  				         	"<input  type='text' value='"+response.dockerVersion+"' class=\"form-control\" readonly=‘readonly’/>"+
		  			        	    "</div>"+
									"</div>"+
					        	    "<div class='form-group' id='nginxIns' >"+
				  				       "<label class='col-sm-4'><div align='right'><b>docker安装：</b></div></label>"+
				  				        "<div class='col-sm-8'>"+
				  				      "<select id='hostDocker_select' name='hostDocker_select' class='form-control' >"+
			  					      "<option value=\"0\">不进行安装</option>"+
				  					  "</select>"+
				  			        	"</div>"+
			      			         "</div>"+
			      			       "<div class='form-group'  >"+
									"<label class='col-sm-4'><div align='right'><b>cluster当前版本：</b></div></label>"+
									 "<div class='col-sm-8'>"+
		  				         	"<input  type='text' value='"+response.swarmVersion+"' class=\"form-control\" readonly=‘readonly’/>"+
		  			        	    "</div>"+
									"</div>"+
					        	    "<div class='form-group' id='nginxIns' >"+
				  				       "<label class='col-sm-4'><div align='right'><b>cluster安装：</b></div></label>"+
				  				        "<div class='col-sm-8'>"+
				  				      "<select id='swarmIns_select' name='swarmIns_select' class='form-control' >"+
			  					      "<option value=\"0\">不进行安装</option>"+
				  					  "</select>"+
				  			        	"</div>"+
			      			         "</div>"+
				  			        "</form>"+
					    	      	"</div><script>getDocker();getSwarm();</script>",
					        	    title: "提示",
					        	    buttons: {
					        	    	"success" : {
					    					"label" : "<i class='icon-ok' id='saveButt'></i> <b>保存</b>",
					    					"className" : "btn-sm btn-danger btn-round",
					    					"callback" : function() {
					    						 var swarmIns = $('#swarmIns_select').val();
					    						 var hostDocker = $('#hostDocker_select').val();
					    						data={
														clusterName: clustername,
								                    	clusterPort: clusterport,
								                    	masteHostId: masterip,
								                    	standByHostId:standByIp,
								                    	managePath: clumanagepath,
								                    	clusterDesc:clusterdesc,
								                    	hostDocker:hostDocker,
								                    	registryLbId:registryLbId,
								                    	clusterEnv:clusterEnv,
								                    	swarmIns:swarmIns
												};
												console.log(data);
												url=base+"cluster/create";
												$("#spinner").css("display","block");
												$.post(url,data,function(response){
													showMessage(response.message);
													$(grid_selector).trigger("reloadGrid");
													$("#spinner").css("display","none");
												});
					    					}
					        	    	},
			        			     "cancel" : {
			     					       "label" : "<i class='icon-info'></i> <b>取消</b>",
			     					         "className" : "btn-sm btn-warning btn-round",
			     					        "callback" : function() {}
			     				    }
			        		     }
			        			});
				       			});
			        		}
			            }
				}
			},
			"cancel" : {
				"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {
					$(grid_selector).trigger("reloadGrid");
				}
			}
	});
	getEnvironment("clusterEnv","");
	$("#saveButt").parent().attr("disabled","disabled");
	$(grid_selector).trigger("reloadGrid");
}


/**数据校验 begin*/

//集群名称检查
function checkClusterName(){
	validToolObj.isNull('#cluster_name') || validToolObj.validName('#cluster_name') || validToolObj.realTimeValid(base + 'cluster/checkName',undefined,'#cluster_name');
	checkClusterCreate();
}

//集群端口检查
function checkClusterPort(){
	validToolObj.isNull('#cluster_port',false) || validToolObj.isPort('#cluster_port');
	checkClusterCreate();
}
/**
 * 是否为port
 */
function isPort(str){
	if(!isNull(str)&&isInteger(str)&&str>0&&str<65536){
		return true;
    }else{
    	return false;
    }
}

//检查集群的主机
function checkHost(){
//	var hostMasterSelect=$('#hostMaster_select').val();
//	if(isNull(hostMasterSelect)){
//		var help= $(".hostMasterSelect").text();
//		var tip="<span class='help-block hostMasterSelect' style='color:#FF0000;'>所在主机不能为空</span>";
//		if(help==null||help==''){
//			$('#hostMaster_select').parent('div').append(tip);
//		}
//    	return;
//    }else{
//    	$(".hostMasterSelect").remove();
//    }	
	validToolObj.isNull('#hostMaster_select');
	getStandByHost();
	checkClusterCreate();
	var hostId=$('#hostMaster_select').val();
	if(hostId==0){
		return false;
	}
//	$.ajax({
//		type: 'get',
//		url: base+'host/isInstalled',
//		dataType: 'json',
//		data:{hostId:hostId}, 
//		success: function (reponese) {
//			if(reponese.success){
//				$("#hostDocker").css("display","none");
//			}
//			else{
//				$("#hostDocker").css("display","block");
//				 getDocker();
//			}
//		}
//	});
//	var name=$(this).children('option:selected').val();
}
//检查配置文件
function checkManage(){
	validToolObj.isNull('#cluster_manage') || validToolObj.validUserName('#cluster_manage');
	checkClusterCreate();
}
//检查保存按钮会否可用
function checkClusterCreate(){
    validToolObj.validForm('#add_item_frm',"#saveButt",['#cluster_name','#cluster_port','#hostMaster_select','#hostMaster_select','#cluster_manage','#clusterEnv']);
}
//检查编辑的集群名称
function checkEditClusterName(){
	if(validToolObj.isNull('#cluster_name_edit') || validToolObj.validName('#cluster_name_edit') ||validToolObj.isNull('#cluster_env_edit')|| validToolObj.realTimeValid(base + 'cluster/checkName',$('#cluster_id_edit').val(),'#cluster_name_edit')){
		$("#saveButt").parent().attr("disabled","disabled");
    }else{
    	$("#saveButt").parent().removeAttr("disabled");
    }
}


/**数据校验end*/

//
function getClusterMster(){
	
	$('#hostMaster_select').empty();
	$.ajax({
        type: 'get',
        url: base+'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var hostid = obj.hostid;
				var hostip = decodeURIComponent(obj.hostip);
				$('#hostMaster_select').append('<option value="'+hostid+'">'+hostip+'</option>');
            });
        }
	});
}
//获取仓库
function getAllRegistryLb(){
	$.ajax({
		type: 'get',
		url: base+'registry/getAllRegistryLb',
		dataType: 'json',
		success: function (reponese) {
            $.each (reponese, function (index, obj){
            	flag = 1;
				var id = obj.id;
				var registryName = obj.registryName;
					$('#registry_select').append('<option value="'+id+'">'+registryName+'</option>');
				
            });
        
		}
	});
}

/*获取备用主机*/
function getStandByHost(){
	$.ajax({
        type: 'get',
        url: base+'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var hostid = obj.hostid;
				var hostip = decodeURIComponent(obj.hostip);
				var name=$('#hostMaster_select').children('option:selected').val();
				if(name != hostid){
					$('#standby_host_select').append('<option value="'+hostid+'">'+hostip+'</option>');
				}
				
            });
        }
	});
}

function getStandByHostEdit(standByHostId){
	$.ajax({
        type: 'get',
        url: base+'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var hostid = obj.hostid;
				var hostip = decodeURIComponent(obj.hostip);
				var name=$('#hostMaster_edit').val();
				if(name != hostid){
					if(hostid == standByHostId){
						$('#standby_host_edit').append('<option value="'+hostid+'" selected="selected">'+hostip+'</option>');
					}
					else{
						$('#standby_host_edit').append('<option value="'+hostid+'">'+hostip+'</option>');
					}
				}
				
            });
        }
	});
}

//
function showClusterAddHostWin(){
	
	//获取集群信息，再添加未在集群的slave类型主机
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids==null||ids.length==0){
		showMessage("请先选择集群！");
	}
	var hosts = "";
	for(var i=0;i<ids.length;i++){
		var rowData = $(grid_selector).jqGrid("getRowData", ids[i]);
		hosts += (i==ids.length-1 ? rowData.clusterId : rowData.clusterId+",");
		clusterId = rowData.clusterId;
		if(rowData.clusterId == null && rowData.hostTpye == 1){
			showMessage("请先选择集群！");
		}else{
			getHostList(clusterId);
			bootbox.dialog({
				title : "<b>添加主机</b>",
				message : "<div class='well ' style='margin-top:1px;'>"+
								"<form class='form-horizontal' role='form' id='add_host_form'>"+
			    	      			"<div class='form-group'>"+
						  				"<label class='col-sm-3'><b>主机列表：</b></label>"+
						  				"<div class='col-sm-9'>"+
						  					"<select id='host_select' name='host_select' class='form-control'"+
						  					"<option value='0:0'>请选择主机</option>"+
		 				  					"</select>"+
						  				"</div>"+
			    	      			"</div>"+
			    	      		"</form>"+
			    	      	"</div>",
				buttons : {
					"success" : {
						"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>保存</b>",
						"className" : "btn-sm btn-danger btn-round",
						"callback" : function() {
			                	var ids = $('#host_select').val().split(":");
			                	clusterAddHost(hosts,ids[0],ids[1]);
			                }	
					},
					    "cancel" : {
						"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
						"className" : "btn-sm btn-warning btn-round",
						"callback" : function() {
							$(grid_selector).trigger("reloadGrid");
						}
					}
				}
			});
		}
	}
}

//
function showClusterRemoveHostWin(){
	
	//获取集群信息，再添加未在集群的slave类型主机
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids==null||ids.length==0){
		showMessage("请先选择集群！");
	}
	var hosts = "";
	for(var i=0;i<ids.length;i++){
		var rowData = $(grid_selector).jqGrid("getRowData", ids[i]);
		hosts += (i==ids.length-1 ? rowData.clusterId : rowData.clusterId+",");
		
		clusterId = rowData.clusterId;
		if(rowData.clusterId == null && rowData.hostTpye == 1){
			showMessage("请先选择集群！");
		}else{
			getClusterHostList(clusterId);
			bootbox.dialog({
				title : "<b>解绑主机</b>",
				message : "<div class='well ' style='margin-top:1px;'>"+
								"<form class='form-horizontal' role='form' id='add_host_form'>"+
			    	      			"<div class='form-group'>"+
						  				"<label class='col-sm-3'><b>主机列表：</b></label>"+
						  				"<div class='col-sm-9'>"+
						  					"<select id='remove_host_select' name='remove_host_select' class='form-control'>"+
						  					"<option value='0:0'>请选择主机</option>"+
		 				  					"</select>"+
						  				"</div>"+
			    	      			"</div>"+
			    	      		"</form>"+
			    	      	"</div>",
				buttons : {
					"success" : {
						"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>保存</b>",
						"className" : "btn-sm btn-danger btn-round",
						"callback" : function() {
			                	var ids = $('#remove_host_select').val().split(":");
			                	clusterRemoveHost(hosts,ids[0],ids[1]);
			                }	
					},
					    "cancel" : {
						"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
						"className" : "btn-sm btn-warning btn-round",
						"callback" : function() {
							$(grid_selector).trigger("reloadGrid");
						}
					}
				}
			});
		}
	}
}

//
function clusterAddHost(hosts,clusterId,hostId){
	
	if(clusterId == 0){
		showMessage("请选择集群！");
		$(grid_selector).trigger("reloadGrid");
	}else{
		var url = base+'cluster/addHost';
		data={  hosts : hosts,
				clusterId : clusterId,
				hostId:hostId
	         };
		$.get(url,data,function(response){
			$(grid_selector).trigger("reloadGrid");
		});
	}
}
//
function clusterRemoveHost(hosts,clusterId,hostId){
	
	if(clusterId == 0){
		showMessage("请选择集群！");
		$(grid_selector).trigger("reloadGrid");
	}else{
		var url = base+'cluster/removeHost';
		data={  hosts : hosts,
				clusterId : clusterId,
				hostId:hostId
	         };
		$.get(url,data,function(response){
			$(grid_selector).trigger("reloadGrid");
		});
	}
}

//
function showDeleteClusters(){
	
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.clusterId == 1){
			showMessage("集群" + rowData.hostId + "不能删除！");
			return;
		}
		infoList += rowData.clusterId+" ";
	}
	bootbox.dialog({
	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除主机&nbsp;' + infoList + '?</div>',
	    title: "提示",
	    buttons: {
	        main: {
	        	label: "<i class='ace-icon fa fa-floppy-o bigger-125'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
	            callback: function () {
	            	var clusterids = new Array();
	            	for(var i=0; i<ids.length; i++){
	            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	            		clusterids = (clusterids + rowData.clusterId) + (((i + 1)== ids.length) ? '':',');
	            	}
	            	deleteClusters(clusterids);
	            }
	        },
	        cancel: {
	        	label: "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
	            callback: function () {
	            	$(grid_selector).trigger("reloadGrid");
	            }
	        }
	    }
	});
  }

//
function getHostList(clusterId){
	
	$('#host_select').empty();
    //
	$.get(base+'host/freeHostList',null,function(response){
		$.each (response, function (index, obj){
			var hostid = obj.hostid;
			var hostip = decodeURIComponent(obj.hostip);
			$('#host_select').append('<option value="'+clusterId+':'+hostid+'">'+hostip+'</option>');
        });
	},'json');
}

//
function deleteClusters(hostidArray){
	
	var data= {
     	ids : hostidArray
     };
	$.post(base+'cluster/deletes',data,function(){
		$(grid_selector).trigger("reloadGrid");
	});
	
}

//
function getClusterHostList(clusterId){
	
	$('#remove_host_select').empty();
    //
	$.get(base+'host/removeAll',{clusterId:clusterId},function(response){
		$.each (response, function (index, obj){
			//var clusterid = obj.clusterId;
			var hostid = obj.hostId;
			var hostIp = obj.hostIp;
			$('#remove_host_select').append('<option value="'+clusterId+':'+hostid+'">'+hostIp+'</option>');
		});
	},'json');
}

//
function updateClusterWin(){
//	getStandByHostEdit();
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.clusterId;
		var name = rowData.clusterName;
		var clusterPort =rowData.clusterPort;
		var hostIP = rowData.hostIP;
		var managePath = rowData.managePath;
		var clusterEnv=rowData.clusterEnv;
		var desc = rowData.clusterDesc;
		var standByHostId = rowData.standByHostId;
		var standByHostIP = rowData.standByHostIP;
		
		var url = base+'cluster/update';
		var title = '<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>&nbsp;<b>修改集群</b>';
		bootbox.dialog({
			title : title,
			message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='update_cluster_frm'>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>集群名称：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
					  				"<input id='cluster_id_edit' type='hidden' value='"+id+"'/>"+
		    	      				"<input id='cluster_name_edit' type='text' class='form-control' onblur='checkEditClusterName();' value='"+name+"'/>"+
		    	      			"</div>"+
		    	      			"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      		"</div>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>集群端口：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='cluster_port_edit' type='number' class='form-control' value='"+clusterPort+"' readonly='readonly'/>"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>所在主机：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='cluster_host_edit' type='text' class='form-control' value='"+hostIP+"' readonly='readonly' />"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      		"<div class='form-group'>"+
			  				"<label class='col-sm-3'><div align='right'><b>备用主机：</b></div></label>"+
			  				"<div class='col-sm-9'>"+
			  					"<select id='standby_host_edit' name='standby_host_edit' class='form-control' >"+
				  					"</select>"+
			  				"</div>"+
    	      			"</div>"+
    	      			"<div class='form-group'>"+
			  				"<label class='col-sm-3'><div align='right'><b>环境：</b></div></label>"+
			  				"<div class='col-sm-9'>"+
			  					"<select id='cluster_env_edit' name='cluster_env_edit' class='form-control' onchange='checkEditClusterName()'></select>"+
			  				"</div>"+
			  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		      			"</div>"+
	    	      		"<div class='form-group hide'>"+
					  			"<label class='col-sm-3'><div align='right'><b>管理文件：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='cluster_filepath_edit' type='text' class='form-control' value='"+managePath+"' readonly='readonly' />"+
		    	      			"</div>"+
		    	      		"</div>"+
				      		"<div class='form-group'>"+
				      			"<label class='col-sm-3'><div align='right'><b>集群描述：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<textarea id='cluster_desc_edit' onblur=\"validToolObj.length('#cluster_desc_edit',200)\" class='form-control' value='"+desc+"' rows='3'>"+desc+"</textarea>"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      	"</form>"+
		    	      "</div>",
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						id = $('#cluster_id_edit').val();
						name = $('#cluster_name_edit').val();
						standByHostId = $('#standby_host_edit').val();
						desc =$('#cluster_desc_edit').val();
						managePath = $('#cluster_filepath_edit').val();
						cluster_env_edit = $('#cluster_env_edit').val();
						data={
								clusterId:id,
								clusterName:name,
								standByHostId:standByHostId,
								managePath:managePath,
								clusterDesc:desc,
								clusterEnv:cluster_env_edit
								
							};
						$.post(url,data,function(response){
							showMessage(response.message);
							$(grid_selector).trigger("reloadGrid");
					  });
					}
				},
				"cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
						$(grid_selector).trigger("reloadGrid");
					}
				}
			}
		});
		$('#standby_host_edit').empty();
		if(standByHostIP!=null){
			$('#standby_host_edit').append('<option value="'+standByHostId+'" selected="selected">'+standByHostIP+'</option>');
		}else{
			$('#standby_host_edit').append("<option value=''>请选择主机</option>");
		}
		getStandByHostEdit(standByHostId);
		getEnvironment('cluster_env_edit',clusterEnv);
	}
}
//
function showHealthCheckClusters(){
	
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var hosts = "";
	for(var i=0;i<ids.length;i++){
		var rowData = $(grid_selector).jqGrid("getRowData", ids[i]);
		hosts += (i==ids.length-1 ? rowData.clusterId : rowData.clusterId+",");
		clusterId = rowData.clusterId;
		if(clusterId == 0){
			showMessage("请选择集群！");
			$(grid_selector).trigger("reloadGrid");
		}else{
			var url = base+'cluster/healthCheck';
			data={  
				  clusterId : clusterId
		         };
			$.get(url,data,function(response){
				showMessage(response.message);
				$(grid_selector).trigger("reloadGrid");
			});
		}
	}
}
function showClusgerHostWin(clusterId,masterIp,clusterName){
	$("#masterIp").val(masterIp);
	$("#clusterId").val(clusterId);
	$('#freeDockerHost').empty();
	$('#freeHost_select').empty();
	$("#freeHost_select").append('<option value="">请选择</option>');
	$.get(base+'host/freeHostList',null,function(response){
		$.each (response, function (index, obj){
			var hostid = obj.hostid;
			var hostIp = decodeURIComponent(obj.hostip);
			$('#freeDockerHost').append('<div name="'+hostIp+'" class="checkbox" style="border-bottom:  1px solid #ddd;"> <label> <input name="unbindDocker" type="checkbox" value="'+clusterId+':'+hostid+'">'+hostIp+' </label> </div>');
			
			//初始化 空闲主机搜索框 option值
			$("#freeHost_select").append('<option value="'+clusterId+':'+hostid+'">'+hostIp+'</option>');
        });
		
		initSearchSelect("#freeHost_select");
		searchTextSelect_free();
	},'json');
	$('#dockerInCluster').empty();
	$('#clusterHost_select').empty();
	$("#clusterHost_select").append('<option value="">请选择</option>');
	$.get(base+'host/removeAll',{clusterId:clusterId},function(response){
		$.each (response, function (index, obj){
			//var clusterid = obj.clusterId;
			var hostid = obj.hostId;
			var hostIp = obj.hostIp;
			var conCount = obj.conCount;
			if( hostIp!=masterIp){
				$('#dockerInCluster').append('<div name="'+hostIp+'" class="radio" style="border-bottom:  1px solid #ddd;"> <label> <input name="addToDocker" type="radio" value="'+clusterId+':'+hostid+':'+hostIp+':'+conCount+'">'+hostIp+' </label> </div>');
				
				//初始化 集群中主机搜索框 option值
				$("#clusterHost_select").append('<option value="'+clusterId+':'+hostid+'">'+hostIp+'</option>');
			}
		});

		initSearchSelect("#clusterHost_select");
		searchTextSelect_cluster();
	},'json');
}

/**
 * 主机搜索框初始化
 */
function initSearchSelect(selector){
	//主机 搜索框初始化
	$(selector).chosen();
	$(selector+"_chosen").css("width","100%");
	$(selector+"_chosen").css("margin-bottom","10px");
	$(selector+"_chosen").find(".chosen-single>span").empty();
}

/**
 * 主机搜索选择 事件(空闲)
 */
function searchTextSelect_free(){
	$("#freeHost_select_chosen").on('click','.chosen-results>li',function(){
		var selectText = $(this).text();
		if(selectText=="请选择"){
			$('#freeDockerHost').find("div").css("display","block");
		}else{
			$('#freeDockerHost').find("div").css("display","none");
			$('#freeDockerHost').find("div[name='"+selectText+"']").css("display","block");
			
		}
		
	});
}
/**
 * 主机搜索选择 事件(集群)
 */
function searchTextSelect_cluster(){
	$("#clusterHost_select_chosen").on('click','.chosen-results>li',function(){
		var selectText = $(this).text();
		if(selectText=="请选择"){
			$('#dockerInCluster').find("div").css("display","block");
		}else{
			$('#dockerInCluster').find("div").css("display","none");
			$('#dockerInCluster').find("div[name='"+selectText+"']").css("display","block");
			
		}
		
	});
}

function addToCluster(){
	var obj = document.getElementsByName("unbindDocker");
	var check_val = new Array();
	for(k in obj){
		if(obj[k].checked){
			check_val.push(obj[k].value);
		}
	}
	if(check_val.length == 0){
		showMessage("请选择添加的主机！");
	}else{
//		getDocker();
		var url = base+'host/getDockersVersion';
		var data1 ={  
				hostIds:check_val.toString(),
	         };
		$("#spinner2").show();
		$.post(url, data1, function(response) {
			$("#spinner2").hide();
			var obj = new Object();
		    obj.list = response;
		    getDockers();
		bootbox.dialog({
	        message: template('add_host',obj) ,
//	        	"<div class='well ' style='margin-top:1px;'>" +
//	        	 "<form class='form-horizontal' role='form''>" +
//	        	 "<div class='form-group'  >"+
//		      	    "<label class='col-sm-7'><div ><b>确定要将此主机添加到集群中吗?</b></div></label>"+
//		      	  "</div>"+
//	        	  "<div class='form-group' id='dockerIns' >"+
//	      	    "<label class='col-sm-3'><div align='right'><b>docker安装：</b></div></label>"+
//	      	      "<div class='col-sm-7'>"+
//	      	       	"<select id='hostDocker_select' name='hostDocker_select' class='form-control' >"+
//	      			  "</select>"+
//	              	"</div>"+
//	                "</div>"+
//	                "</form>"+
//	              "</div>",
	        title: "安装确定",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	var check_val2 = new Array();
	                	for(k in check_val){
	                		var hostId = check_val[k].split(":")[1];
	                		var  dockerIns =$("#"+hostId).val();
	                		check_val2.push( check_val[k]+":"+dockerIns);
	                		}
	                	var url = base+'cluster/addHost';
	            		data={  
	            				hostIds:check_val2.toString(),
	            	         };
	            		$("#spinner2").show();
	            		$.get(url,data,function(response){
	            			var masterIp = $("#masterIp").val();
	            			var clusterId = $("#clusterId").val();
	            			showMessage(response.message);
	            			showClusgerHostWin(clusterId,masterIp);
	            			$("#spinner2").hide();
	            		});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
		});
	}
}
function doUnbind(conCount,check_val,timer,html){
	var boxContent = '';
	if(html!=''){
		boxContent = '<div>'+
			'<label for="form-field-select-2">此主机运行的物理系统：</label>'+
			'<select class="form-control" id="form-field-select-2" multiple="multiple">'+
			html+
			'</select>'+
		'</div>';
	}
	var thtml = '';
	if(conCount==0){
		thtml = '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>'+
				'&nbsp;此主机空闲，可以从集群移除。确定要将此主机从集群中移除吗?</div>'+
				'<div align="center"><span class="badge badge-danger">'+timer+'</span></div>';
	}else{
		thtml = '<div class="alert alert-warning" style="margin:10px">'+
				'<span class="glyphicon glyphicon-info-sign"></span>'+
				'&nbsp;此主机上有个 '+conCount+' 容器正在运行，解绑主机将会移除所有正在运行的容器！是否继续？'+
				boxContent+
				'</div>'+
				'<div align="center"><span class="badge badge-danger">'+timer+'</span></div>';
	}
	//询问5次
	if(timer>1){
		bootbox.dialog({
			 title: "提示",
			message : thtml,
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>确定</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						doUnbind(conCount,check_val,--timer,html);
		             }	
				},	
				    "cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
						$(grid_selector).trigger("reloadGrid");
					}
				}
			}
		});
	}else{
		bootbox.dialog({
			 title: "提示",
			message : thtml,
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>确定</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						var url = base+'cluster/removeHost';
		           		data={  
		           				hostIds:check_val.toString()
		           	         };
		           		$("#spinner2").show();
		           		$.get(url,data,function(response){
		           			var masterIp = $("#masterIp").val();
		           			var clusterId = $("#clusterId").val();
		           			showMessage(response.message);
		           			showClusgerHostWin(clusterId,masterIp);
		           			$("#spinner2").hide();
		           		});
		             }	
				},
				    "cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
						$(grid_selector).trigger("reloadGrid");
					}
				}
			}
		});
	}
	
}
function unbindFromCluster(){
	var obj = document.getElementsByName("addToDocker");
	var check_val = new Array();
	for(k in obj){
		if(obj[k].checked){
			check_val.push(obj[k].value);
			var conCount = obj[k].value.split(":")[3];
			var hostId = obj[k].value.split(":")[1];
			if(conCount>0){
				if(obj.length<=1){
					bootbox.dialog({
						 title: "提示",
						message : '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;集群上只有个 '+obj.length+' docker服务器节点，解绑后此容器运行的所有应用将被移除！是否继续？</div>',
						buttons : {
							"success" : {
								"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>继续</b>",
								"className" : "btn-sm btn-danger btn-round",
								"callback" : function() {
									var html = '';
					           		$.get(base+'host/getSystemNames',{hostId:hostId},function(response){
					           			$.each (response, function (index, obj){
					           				var systemName = obj.systemName;
					           				html += '<option value="AL">'+systemName+'</option>';
					           			});
					           			doUnbind(conCount,check_val,5,html);
					           		},'json');
					            }	
							},
							    "cancel" : {
								"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
								"className" : "btn-sm btn-warning btn-round",
								"callback" : function() {
									return;
									$(grid_selector).trigger("reloadGrid");
								}
							}
						}
					});
				}else{
					var html = '';
	           		$.get(base+'host/getSystemNames',{hostId:hostId},function(response){
	           			$.each (response, function (index, obj){
	           				var systemName = obj.systemName;
	           				html += '<option value="AL">'+systemName+'</option>';
	           			});
	           			doUnbind(conCount,check_val,5,html);
	           		},'json');
				}
			}else{
				this.doUnbind(0,check_val,5,'');
			}
		}
	}
	if(check_val.length == 0){
		showMessage("请选择解绑的主机！");
	}
}
/*显示docker容器列表*/
function showContainers(hostId,hostIp){
	window.location = "/host/containerOfHost.html?hostId="+hostId+"&hostIp="+hostIp+"&flag=0";
}


//查询条件 
function searchHost(){
	var clusterName = $('#clusterName').val();
	var hostIp = $('#hostIp').val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'cluster/list?clusterName='+clusterName+'&hostIP='+hostIp}).trigger("reloadGrid");
}

//暂时不要删除
$().ready(function(){
	$("#hostList").change(function(){
		if($("#hostList").val()!=""){
			$('#migrateCluster_btn').removeAttr("disabled");
		}else{
			$('#migrateCluster_btn').attr("disabled","disabled");
		}
	});
//	$.ajax({
//		url:base+'host/hostList',
//		success:function(data){
//			var options=$("#hostList");
//			var clusterArray=jQuery.parseJSON(data);
//			$.each(clusterArray,function(i,value){
//				var hostId=value.hostId;
//				var hostIp = value.hostIp;
//				var option="<option value='"+hostId+"'>"+hostIp+"</option>";
//				options.append(option);
//			});
//		}
//	});
});


function showmigrateClusterWin(clusterId,hostIP,clusterName){
	$("#clusterHostIP").val(hostIP);
	$("#clusterId_").val(clusterId);
	$("#clusterName_").val(clusterName)
	$('#hostList').empty();
	var hostListHtml='<option value="">请选择</option>';
	$.get(base+'host/grateClusterHost',{clusterId:clusterId},function(response){
		$.each (response, function (index, obj){
			var hostid = obj.hostid;
			var hostIp = decodeURIComponent(obj.hostip);
			hostListHtml +='<option value="'+hostid+'">'+hostIp+'</option>';
        });
	$('#hostList').append(hostListHtml);
	},'json');
}

function migrateCluster(){
	var clusterId = $("#clusterId_").val();
	var clusterHostIP = $("#clusterHostIP").val();
	var newHostId=$("#hostList").val();
	var newHostIP=$("#hostList").find("option:selected").text();
	bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要将此集群从('+clusterHostIP+')迁移到主机('+newHostIP+')吗?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                	var url = base+'cluster/migrate';
            		data={  
            				clusterId:clusterId,
            				hostId:newHostId
            	         };
            		$("#spinner").show();
            		$('#migrateCluster').modal("hide");
            		$.post(url,data,function(response){
            			showMessage(response.message);
            			$("#spinner").hide();
            			$(grid_selector).trigger("reloadGrid");
            		});
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}




/**
 * mayh
 * 启动swarm
 */
function startSwarm(clusterId){
	bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;启动SWARM进程?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                	var data = {clusterId:clusterId,opration:'start'};
                	var url = base+"cluster/swarmDaemon";
                	$("#spinner").css("display","block");
                	$.get(url,data,function(response){
                		showMessage(response.message);
                		$(grid_selector).trigger("reloadGrid");
                		$("#spinner").css("display","none");
                	});
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}
/**
 * mayh
 * 停止swarm
 */
function stopSwarm(clusterId,timer){
	var thtml = '<div class="alert alert-warning" style="margin:10px">'+
	'<span class="glyphicon glyphicon-info-sign"></span>停止SWARM进程?</div>'+
	'<div align="center"><span class="badge badge-danger">'+timer+'</span></div>';
	if(timer>1){
		bootbox.dialog({
	        message: thtml,
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	stopSwarm(clusterId,--timer);
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}else{
		bootbox.dialog({
	        message: thtml,
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	var data = {clusterId:clusterId,opration:'stop'};
	                	var url = base+"cluster/swarmDaemon";
	                	$("#spinner").css("display","block");
	                	$.get(url,data,function(response){
	                		showMessage(response.message);
	                		$(grid_selector).trigger("reloadGrid");
	                		$("#spinner").css("display","none");
	                	});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}
}

/**
 * mayh
 * 检查swarm
 */
function helthCheck(clusterId){
	var data = {clusterId:clusterId,opration:'check'};
	var url = base+"cluster/swarmDaemon";
	$("#spinner").css("display","block");
	$.get(url,data,function(response){
		showMessage(response.message);
		$(grid_selector).trigger("reloadGrid");
		$("#spinner").css("display","none");
	});
}

function getDocker(){
	
	$('#hostDocker_select').empty();
	$('#hostDocker_select').append('<option value="0">不进行安装</option>');
	$.get(base+'hostComponent/hostComponentList?type=docker&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('#hostDocker_select').append('<option value="'+id+'">'+version+'</option>');
        });
	},'json');
}
function getSwarm(){
	
	$('#swarmIns_select').empty();
	$('#swarmIns_select').append('<option value="0">不进行安装</option>');
	$.get(base+'hostComponent/hostComponentList?type=swarm&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('#swarmIns_select').append('<option value="'+id+'">'+version+'</option>');
		});
	},'json');
}
function getDockers(){
	
	$.get(base+'hostComponent/hostComponentList?type=docker&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('.hostDocker_select').append('<option value="'+id+'">'+version+'</option>');
        });
	},'json');
}
function getEnvironment(id,environmentId){
	$.ajax({
		type: 'get',
		url: base+'environment/list',
		dataType: 'json',
		success: function (array) {
			$('#'+id).empty();
//			$('#'+id).append('<option value="">请选择环境</option>');
			$.each (array, function (index, obj){
				var environmentName = obj.eName;
				if(environmentId==obj.eId){
					var temp ='<option value="'+obj.eId+'" selected="selected">'+environmentName+'</option>';
					$('#'+id).append(temp);
				}else{
					var temp ='<option value="'+obj.eId+'">'+environmentName+'</option>';
					$('#'+id).append(temp);
				}
			});
		}
	});
}