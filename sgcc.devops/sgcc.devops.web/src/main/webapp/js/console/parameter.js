var grid_selector = "#parameter_list";
var page_selector = "#parameter_page";


/*数据字典列表展示*/
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
		url : base+'parameter/searchlist',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','编码','值','描述', '类型','父级','快捷操作'],
		colModel : [
		    {name : 'paraId',index : 'paraId',width : 8,hidden:true},
	        {name : 'paraCode',index : 'paraCode',width : 8},
			{name : 'paraValue',index : 'paraValue',width : 8},
			{name : 'paraDesc',index : 'paraDesc',width : 8},
			{name : 'paraType',index : 'paraType',width : 6,title:false,
				formatter:function(cell,opt,obj){
					switch(obj.paraType){
					case "0":	return "位置属性层级意义";
					case "1": return "安装组件配置"; 
					default:return "未知";
					}					
				}		
			},
			{name : 'parentId',index : 'parentId',width : 8,hidden:true},
			{
		    	name : '',
				index : '',
				width : 300,
				align :'left',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue,options,rowObject) {
							return "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"checkParameterWin('"
							+rowObject.id+"','"+rowObject.paraCode+"','"+rowObject.paraName+"','"+rowObject.paraValue+"','"+rowObject.paraType+"','"+rowObject.parentId+"','"+rowObject.paraDesc+"')\">"
							+"<i class=\"ace-icon fa fa-search bigger-125\"></i>"
							+"<b>查看</b></button> &nbsp&nbsp;"
							+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"updateParameter('"
							+rowObject.id+"','"+rowObject.paraCode+"','"+rowObject.paraName+"','"+rowObject.paraValue+"','"+rowObject.paraType+"','"+rowObject.parentId+"','"+rowObject.paraDesc+"')\">"
							+"<i class=\"ace-icon fa fa-pencil-square-o bigger-125\"></i>"
							+"<b>编辑</b></button> &nbsp&nbsp;"
							+"<button class=\"btn btn-xs btn-danger btn-round\" onclick=\"deleteParameter('"+rowObject.id+"')\">"
							+"<i class=\"ace-icon fa fa-minus-circle bigger-125\"></i>"
							+"<b>删除</b></button> &nbsp&nbsp;";
				}
			}
		],
		
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'paraCode',
		sortname: 'paraType',
		sortorder: "asc",
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false,
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			};
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);
	
	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}
	
	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
	//	$(table).find('input:checkbox').addClass('ace')
	//	.wrap('<label />')
	//	.after('<span class="lbl align-top" />')
	//	$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
	//	.find('input.cbox[type=checkbox]').addClass('ace')
	//	.wrap('<label />').after('<span class="lbl align-top" />');
	}
});

/* 查看数据字典 */
function checkParameterWin(paraId,paraCode,paraName,paraValue,paraType,_parentId,paraDesc){
	 bootbox.dialog({
	        title: '<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>&nbsp;<b>字典详细信息</b>',
	        message: "<div class='well' style='margin-top: 1px;'>"
	        	+ "<form class='form-horizontal' role='form' id='create_user_form'>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;编码：</b></label>" 
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id=\"para_code2\" readonly='readonly' name='para_code' value='" + paraCode+ "'  type='text' class=\"form-control\" />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>名称：</b></label>"
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id='para_name2'  readonly='readonly' name='para_name' value='" + paraName + "'  type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;值：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_value2' readonly='readonly' name='para_value' value='" + paraValue + "'  type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;类型：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+"<select id='para_type2' class='form-control' readonly='readonly'>"
	        	+"<option value=''>请选择</option>"
	        	+"<option value='0' "+ (paraType==0 ?"selected=:\'selected\'":'')+">位置属性层级意义</option>"
	        	+"<option value='1' "+ (paraType==1 ?"selected=:\'selected\'":'')+">安装组件配置</option>"
				+"</select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group' id='parentidDiv' style='display:none'>" 
	        	+ "<label class='col-sm-3'><b>父级：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<select id='para_parentId' class='form-control' readonly='readonly'></select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>描述：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_desc2' readonly='readonly' name='para_desc' value='" + paraDesc + "' type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "</form>" 
	        	+ "</div>",
	    });
	 cheackParentDiv();
	 getParent(_parentId);
}

/* 编辑数据字典 */
function updateParameter(paraId,paraCode,paraName,paraValue,paraType,_parentId,paraDesc){
	 bootbox.dialog({
	        title: "<b>修改数据字典</b>",
	        message: "<div class='well' style='margin-top: 1px;'>"
	        	+ "<form class='form-horizontal' role='form' id='edit_parameter_form'>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;编码：</b></label>" 
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id=\"para_code3\"  name='para_code' value='" + paraCode+ "'  type='text' class=\"form-control\" onblur='checkEditParaCode();'/>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>名称：</b></label>"
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id='para_name3' name='para_name' value='" + paraName + "'  type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;值：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_value3' name='para_value' value='" + paraValue + "'  type='text' class=\"form-control\"' onblur='checkEditParaValue();' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;类型：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<select id='para_type3' class='form-control' onchange='checkEditParaType();'>"
	        	+"<option value=''>请选择</option>"
	        	+"<option value='0' "+ (paraType==0 ?"selected=:\'selected\'":'')+">位置属性层级意义</option>"
	        	+"<option value='1' "+ (paraType==1 ?"selected=:\'selected\'":'')+">安装组件配置</option>"
				+"</select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group' id='parentidDiv' style='display:none'>" 
	        	+ "<label class='col-sm-3'><b>父级：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<select id='para_parentId' class='form-control'></select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>描述：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_desc3' name='para_desc' value='" + paraDesc + "' type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "</form>" 
	        	+ "</div>",
	        buttons: {
	            "success": {
	                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
	                "className": "btn-sm btn-danger btn-round",
	                "callback": function() {
	                	
	                    var paraCode = $('#para_code3').val();
	                    var paraName = $('#para_name3').val();
	                    var paraValue=$('#para_value3').val();
	                    var paraType = $('#para_type3').val();
	                    var parentId = $('#para_parentId').val();
	                    if(parentId==null||parentId==undefined){
	                    	parentId='';
	                    }
	                    var paraDesc=$('#para_desc3').val();
	                    data = {
	                    	paraId:paraId,
	                        paraCode:paraCode,
	                        paraName:paraName,
	                        paraValue:paraValue,
	                        paraType:paraType,
	                        parentId:parentId,
	                        paraDesc:paraDesc
	                    };
	                    console.log(data);
	                    url = base + "parameter/updateParameter";
	                    $("#spinner").css("display", "block");
	                    $.post(url, data,
	                    function(response) {
	                        showMessage(response.message);
	                        $(grid_selector).trigger("reloadGrid");
	                        $("#spinner").css("display", "none");
	                    });
	                }
	            },
	            "cancel": {
	                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
	                "className": "btn-sm btn-warning btn-round",
	                "callback": function() {
	                    $(grid_selector).trigger("reloadGrid");
	                }
	            }
	        }
	    });
	 cheackParentDiv();
	 getParent(_parentId);
}

/* 删除数据字典 */
function deleteParameter(paraId){
    var url = base + "parameter/deleteParameter";
    data = {
        paraId: paraId
    };
    bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除该数字字典 吗?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function() {
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                    });
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function() {}
            }
        }
    });
}

/* 新增数据字典 */
function createParameter(){
	   bootbox.dialog({
	        title: "<b>添加数据字典</b>",
	        message: "<div class='well' style='margin-top: 1px;'>"
	        	+ "<form class='form-horizontal' role='form' id='create_parameter_form'>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;编码：</b></label>" 
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id=\"para_code1\"  name='para_code' type='text' class=\"form-control\" onblur='checkParaCode();'/>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>名称：</b></label>"
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id=\"para_name1\" name='para_name' type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;值：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_value1' name='para_value' type='text' class=\"form-control\"' onblur='checkParaValue();' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;类型：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<select id='para_type1' class='form-control' onchange='checkParaType();' >"
	        	+"<option value=''>请选择</option>"
	        	+"<option value='0'>位置属性层级意义</option>"
	        	+"<option value='1'>安装组件配置</option>"
				+"</select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group' id='parentidDiv' style='display:none'>" 
	        	+ "<label class='col-sm-3'><b>父级：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<select id='para_parentId' class='form-control'></select>" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b>描述：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='para_desc1' name='para_desc' type='text' class=\"form-control\"' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "</form>" 
	        	+ "</div>",
	        buttons: {
	            "success": {
	                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
	                "className": "btn-sm btn-danger btn-round",
	                "callback": function() {
	                    var paraCode = $('#para_code1').val();
	                    var paraName = $('#para_name1').val();
	                    var paraValue = $('#para_value1').val();
	                    var paraType = $('#para_type1').val();
	                    var parentId = $('#para_parentId').val();
	                    if(parentId==null||parentId==undefined){
	                    	parentId='';
	                    }
	                    var paraDesc = $('#para_desc1').val();
	                    data = {
	                        paraCode:paraCode,
	                        paraName:paraName,
	                        paraValue:paraValue,
	                        paraType:paraType,
	                        parentId:parentId,
	                        paraDesc:paraDesc
	                    };
	                    console.log(data);
	                    url = base + "parameter/createParameter";
	                    $("#spinner").css("display", "block");
	                    $.post(url, data,
	                    function(response) {
	                        showMessage(response.message);
	                        $(grid_selector).trigger("reloadGrid");
	                        $("#spinner").css("display", "none");
	                    });
	                }
	            },
	            "cancel": {
	                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
	                "className": "btn-sm btn-warning btn-round",
	                "callback": function() {
	                    $(grid_selector).trigger("reloadGrid");
	                }
	            }
	        }
	    });
	   getParent("");
}

/* 查询数据字典 */
function searchParameter(){
	var paraName = $('#para_name').val();
	var paraType = $('#type_name').val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'parameter/searchlist?paraName='+paraName+'&paraType='+paraType}).trigger("reloadGrid");
}

/*数据校验 begin*/
/*编码不能为空且不能重复*/
function checkParaCode(){
	validToolObj.isNull('#para_code1') || validToolObj.validName('#para_code1') || validToolObj.realTimeValid(base + 'parameter/checkParaCode',undefined,'#para_code1');
	checkParameterCreate();
}
/*值不能为空*/
function checkParaValue(){
	validToolObj.isNull('#para_value1');
	checkParameterCreate();
}
/*类型不能为空*/
function checkParaType(){
	validToolObj.isNull('#para_type1');
	checkParameterCreate();
	cheackParentDiv();
	
}

/*检查保存按钮是否可用*/
function checkParameterCreate(){
    validToolObj.validForm('#create_parameter_form',"#saveButt",['#para_code1','#para_value1','#para_type1']);
}
/*修改字典时编码不能为空且不能重复*/
function checkEditParaCode(){
	validToolObj.isNull('#para_code3') || validToolObj.validName('#para_code3') || validToolObj.realTimeValid(base + 'parameter/checkParaCode',paraId,'#para_code3');
	checkEditParameterCreate();
}
/*修改字典时值不能为空*/
function checkEditParaValue(){
	validToolObj.isNull('#para_value3');
	checkEditParameterCreate();
}
/*修改字典时类型不能为空*/
function checkEditParaType(){
	validToolObj.isNull('#para_type3');
	checkEditParameterCreate();
	cheackParentDiv();
}
function cheackParentDiv(){
	if($("#para_type1").val()=="0"||$("#para_type2").val()=="0"||$("#para_type3").val()=="0"){
		document.getElementById("parentidDiv").style.display="block";
		
	}else{
		document.getElementById("parentidDiv").style.display="none";
	}
	
}
function getParent(parentId){
	$.ajax({
		type: 'get',
		url: base+'parameter/list',
		dataType: 'json',
		data:{
			type:0
		},
		success: function (array) {
			$('#para_parentId').empty();
			$('#para_parentId').append('<option value="">请选择父级</option>');
			$.each (array, function (index, obj){
				var parentName = obj.paraName;
				if(parentId==obj.id){
					var temp ='<option value="'+obj.id+'" selected="selected">'+parentName+'</option>';
					$('#para_parentId').append(temp);
				}else{
					var temp ='<option value="'+obj.id+'">'+parentName+'</option>';
					$('#para_parentId').append(temp);
				}
			});
		}
	});
}
/*修改字典时检查保存按钮是否可用*/
function checkEditParameterCreate(){
    validToolObj.validForm('#edit_parameter_form',"#saveButt",['#para_code3','#para_value3','#para_type3']);
}
/*数据校验 end*/