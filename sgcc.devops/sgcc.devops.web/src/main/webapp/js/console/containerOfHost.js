var grid_selector = "#container_list";
var page_selector = "#container_page";
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	var hostId = $("#hostId").val();
	var flag = $("#flag").val();
	if(flag==0){
		$("#clearButt").removeClass("hide");
	}else{
		$("#clearButt").addClass("hide");
	}
	jQuery(grid_selector).jqGrid({
		url : base+'container/listContainersByHostId?hostId='+hostId,
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','UUID', '名称', '依赖镜像', '','状态','端口', '所属物理系统','容器类型', '创建时间','操作'],
		colModel : [ 
		    {name : 'conId',index : 'conId',width : 10,hidden:true},
			{name : 'conUuid',index : 'conUuid',width : 10,hidden:true,
				formatter:function(cellvalue, options, rowObject){
					return '<a href="#"> c-'+ cellvalue.substr(0, 8)+'</a>';
				}
			},
			{name : 'conTempName',index : 'conTempName',width : 15},
			{name : 'conImgid',index : 'conImgid',width : 15, hidden:true},
			{name : 'conPower',index : 'conPower',width : 10, hidden:true},
			{name : 'power',index : 'power',width : 10,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conPower){
						case 0:
							return '<i class="fa fa-stop text-danger">&nbsp; 已停止</i>';
						case 1:
							return '<i class="fa fa-play-circle text-success"><b> &nbsp;运行中<b></i>';
					}
				}
			},
			{name : 'port',index:'port',width :14,
				formatter:function(cellvalue, options, rowObject){
					return getPortInfos(rowObject.conId);
				}},
			{name : 'systemName', index : 'systemName',width : 10},
			{name : 'conType', index : 'conType',width : 10,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conType){
					case 0:
						return '应用服务器容器';
					case 1:
						return '应用容器';
					case 2:
						return 'NGINX容器';
				}
				}
			},
			{name : 'conCreatetime', index : 'conCreatetime',width : 10},
			{
		    	name : '',
				index : '',
				width : 120,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue,options,rowObject) {
					rowObject.hostDesc = stringTool.escape(rowObject.hostDesc);
					return "<button class=\"btn btn-xs btn-info btn-round\" onclick=\"toMonitor('"+rowObject.conId+"')\">"
					+"<i class=\"ace-icon fa fa-eye bigger-125\"></i>"
					+"<b>监控</b></button> &nbsp;";
				}
			}
		]
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	function getPortInfos(conId){
		var ports = "";
		$.ajax({
	        type: 'get',
	        url: base+'container/listPort',
	        data:{
	        	conId:conId
	        },
	        async:false,
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
	            	portInfo = obj.conIp+":"+obj.pubPort+"-->"+obj.priPort + (((index + 1)== array.length) ? '':'\n');
					ports += portInfo;
	            });
	        }
		});
		return ports;
	}
});
/**
 * @author yangqinglin
 * @datetime 2015年9月10日 17:12
 * @description 返回上一个页面
 */
function gobackpage(){
	history.go(-1);
}	
	
function toMonitor(id){
	location.href = base+'monitor/container/'+id+'.html';
}
function clearCon(hostId){
	bootbox.dialog({
		 title: "提示",
			message : '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;此操作将清空本机上所有容器并从集群中移除！是否继续执行？</div>',
		buttons : {
			"success" : {
				"label" : "<i class='ace-icon fa fa-floppy-o bigger-125'></i> <b>确定</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
					$("#spinner").css("display","block");
					$.ajax({
				        type: 'post',
				        url: base+'container/clear',
				        data:{
				        	hostId:hostId
				        },
				        async:true,
				        dataType: 'json',
				        success: function (response) {
				        	$("#spinner").css("display","none");
				            showMessage(response.message, function(){
				            	$(grid_selector).trigger("reloadGrid");
				            });
				        }
					});
	             }	
			},
			    "cancel" : {
				"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {
					$(grid_selector).trigger("reloadGrid");
				}
			}
		}
	});
}