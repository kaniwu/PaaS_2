var grid_selector1 = "#serverRoom_list";
var page_selector1 = "serverRoom_page";
var grid_selector2 = "#rack_list";
var page_selector2 = "rack_page";
var rack_serverRoomId = "";
var tree;
var treeData = [];
var setting = {
    check: {
        enable: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: zTreeOnClick
    }
};

function createZTreeFun(selector) {
    //    var tree = $.fn.zTree.init($(selector), setting, treeData);
    tree = $.fn.zTree.init($(selector), setting, treeData);
    tree.expandAll(true);
}
jQuery(function($) {
    //初始化树
    $.ajax({
        url: base + "serverRoom/tree",
        //要加载数据的地址
        type: 'get',
        //加载方式
        dataType: 'json',
        //返回数据格式
        success: function(response) {
            if (response == "") {
                showMessage("数据加载异常！");
            } else {
                treeData = response.data;
                var treeStr = "";
                $.ajax({
                    url: base + "role/roleAuth",
                    data: {
                        roleId: 3
                    },
                    type: 'get',
                    dataType: 'json',
                    success: function(obj) {
                        $.each(obj,
                        function(index, json) {
                            treeStr += json.actionId + ",";
                        });
                        createZTreeFun("#roleAuthTree");
                        //替换为ztree
                        var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
                        var treeStrSplit = treeStr.split(",");
                        var treeIds = [];
                        for (var i = 0; i < treeStrSplit.length; i++) {
                            var node = tree.getNodeByParam('id', treeStrSplit[i], null);
                            if (node) tree.checkNode(node, true);
                        }
                    }
                });
            }
        },
        error: function(response) {
            console.log(response);
        }
    });

});

//节点点击事件
function zTreeOnClick(event, treeId, treeNode) {
    if (treeNode.name == "机房管理") {
        $("#serverRoomManager").show();
        $("#rackManager").hide();
        serverRoomManager();
    }
    if (treeNode.type == "room") {
        rack_serverRoomId = treeNode.nodeId;
        $("#roomId").html(treeNode.name);
        $("#serverRoomManager").hide();
        $("#rackManager").show();
        rackManager(treeNode.nodeId);
        jQuery(grid_selector2).clearGridData().setGridParam({
            postData: {
                'serverRoomId': treeNode.nodeId
            }
        }).trigger("reloadGrid");
    }
}

//机房管理表
function serverRoomManager() {
    jQuery(grid_selector1).jqGrid({
        url: base + 'serverRoom/serverRoomList',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: ['机房名称', '机房备注', '快捷操作'],
        colModel: [{
            name: 'serverRoomName',
            index: 'serverRoomName',
            width: 10
        },
        {
            name: 'serverRoomRemark',
            index: 'serverRoomRemark',
            width: 15
        },
        {
            name: '',
            index: '',
            width: 300,
            align: 'left',
            fixed: true,
            sortable: false,
            resize: false,
            title: false,
            formatter: function(cellvalue, options, rowObject) {
                return "<a href='#modal-wizard' data-toggle='modal'><button class=\"btn btn-xs btn-danger btn-round\" onclick=\"removeServerRoom('" + rowObject.id + "')\">" + "<i class=\"ace-icon fa fa-minus-circle bigger-125\"></i>" + "<b>删除</b></button></a> &nbsp;" + "<a href='#migrateCluster' data-toggle='modal'><button class=\"btn btn-xs btn-primary btn-round\" onclick=\"updateServerRoomWin('" + rowObject.id + "','" + rowObject.serverRoomName + "','" + rowObject.serverRoomRemark + "')\">" + "<i class=\"ace-icon fa fa-pencil-square-o bigger-125\"></i>" + "<b>编辑</b></button></a> &nbsp;";
            }
        }],
        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 50, 100, 1000],
        pager: page_selector1,
        altRows: true,
        sortname: 'serverRoomName',
        sortname: 'serverRoomRemark',
        sortorder: "asc",
        jsonReader: {
            root: "rows",
            total: "total",
            page: "page",
            records: "records",
            repeatitems: false
        },
        loadError: function(resp) {
            if (resp.responseText.indexOf("会话超时") > 0) {
                alert('会话超时，请重新登录！');
                document.location.href = '/login.html';
            }
        },
        loadComplete: function() {
            var table = this;
            setTimeout(function() {
                //					styleCheckbox(table);
                updateActionIcons(table);
                updatePagerIcons(table);
                enableTooltips(table);
            },
            0);
        }
    });
}

$(window).triggerHandler('resize.jqGrid'); // 窗口resize时重新resize表格，使其变成合适的大小
jQuery(grid_selector1).jqGrid( //分页栏按钮
'navGrid', page_selector1, {
    edit: false,
    add: false,
    del: false,
    search: false,
    refresh: true,
    refreshstate: 'current',
    refreshicon: 'ace-icon fa fa-refresh',
    view: false
},
{},
{},
{},
{},
{});

jQuery(grid_selector2).jqGrid( //分页栏按钮
'navGrid', page_selector2, { // navbar options
    edit: false,
    add: false,
    del: false,
    search: false,
    refresh: true,
    refreshstate: 'current',
    refreshicon: 'ace-icon fa fa-refresh',
    view: false
},
{},
{},
{},
{},
{});
function updateActionIcons(table) {}
function updatePagerIcons(table) {
    var replacement = {
        'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function() {
        var icon = $(this);
        var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
        if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
    });
}

function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({
        container: 'body'
    });
    $(table).find('.ui-pg-div').tooltip({
        container: 'body'
    });
}
function styleCheckbox(table) {
    //	$(table).find('input:checkbox').addClass('ace')
    //	.wrap('<label />')
    //	.after('<span class="lbl align-top" />')
    //	$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
    //	.find('input.cbox[type=checkbox]').addClass('ace')
    //	.wrap('<label />').after('<span class="lbl align-top" />');
}

//机架管理表
function rackManager(serverRoomId) {
    var postData = {
        serverRoomId: serverRoomId
    };
    jQuery(grid_selector2).jqGrid({
        url: base + 'serverRoom/rackList',
        datatype: "json",
        postData: postData,
        height: '100%',
        autowidth: true,
        colNames: ['机架名称', '机架备注', '快捷操作'],
        colModel: [{
            name: 'rackName',
            index: 'rackName',
            width: 10
        },
        {
            name: 'rackRemark',
            index: 'rackRemark',
            width: 15
        },
        {
            name: '',
            index: '',
            width: 300,
            align: 'left',
            fixed: true,
            sortable: false,
            resize: false,
            title: false,
            formatter: function(cellvalue, options, rowObject) {
                debugger;
                return "<a href='#modal-wizard' data-toggle='modal'><button class=\"btn btn-xs btn-danger btn-round\" onclick=\"removeRack('" + rowObject.id + "')\">" + "<i class=\"ace-icon fa fa-minus-circle bigger-125\"></i>" + "<b>删除</b></button></a> &nbsp;" + "<a href='#migrateCluster' data-toggle='modal'><button class=\"btn btn-xs btn-primary btn-round\" onclick=\"updateRackWin('" + rowObject.id + "','" + rowObject.rackName + "','" + rowObject.rackRemark + "','" + rowObject.serverRoomId + "')\">" + "<i class=\"ace-icon fa fa-pencil-square-o bigger-125\"></i>" + "<b>编辑</b></button></a> &nbsp;";
            }
        }],
        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 50, 100, 1000],
        pager: page_selector2,
        altRows: true,
        sortname: 'rackName',
        sortname: 'rackRemark',
        sortorder: "asc",
        jsonReader: {
            root: "rows",
            total: "total",
            page: "page",
            records: "records",
            repeatitems: false
        },
        loadError: function(resp) {
            if (resp.responseText.indexOf("会话超时") > 0) {
                alert('会话超时，请重新登录！');
                document.location.href = '/login.html';
            }
        },
        loadComplete: function() {
            var table = this;
            setTimeout(function() {
                styleCheckbox(table);
                updateActionIcons(table);
                updatePagerIcons(table);
                enableTooltips(table);
            },
            0);
        }
    });
}

//删除机房
function removeServerRoom(serverRoomId) {
    var url = base + "serverRoom/deleteServerRoom";
    data = {
        serverRoomId: serverRoomId
    };
    bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除该机房  吗?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function() {
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector1).trigger("reloadGrid");
                    });
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function() {}
            }
        }
    });
}

//编辑机房
function updateServerRoomWin(serverRoomId, serverRoomName, serverRoomRemark) {
    bootbox.dialog({
        title: "修改机房信息",
        message: "<div class='well' style='margin-top: 1px;'>" + "<form class='form-horizontal' role='form' id='create_user_form'>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机房名称：</b></label>" + "<div class='col-sm-9'>" + "<input id=\"serverRoom_name\"  name='serverRoom_name' type='text' value='" + serverRoomName + "' class=\"form-control\" placeholder='只能包含中文、英文、数字、下划线...' />" + "</div>" + "</div>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机房备注：</b></label>" + "<div class='col-sm-9'>" + "<input id='serverRoom_remark' name='serverRoom_remark' type='text' value='" + serverRoomRemark + "' class=\"form-control\" placeholder='字数限制2000字...' />" + "</div>" + "</div>" + "</form>" + "</div>",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                    var serverRoomName = $('#serverRoom_name').val();
                    var serverRoomRemark = $('#serverRoom_remark').val();
                    data = {
                        serverRoomId: serverRoomId,
                        serverRoomName: serverRoomName,
                        serverRoomRemark: serverRoomRemark
                    };
                    console.log(data);
                    url = base + "serverRoom/updateServerRoom";
                    $("#spinner").css("display", "block");
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector1).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                    $(grid_selector1).trigger("reloadGrid");
                }
            }
        }
    });
}

//新增机房
function showCreateRoomManager() {
    bootbox.dialog({
        title: "<b>添加机房</b>",
        message: "<div class='well' style='margin-top: 1px;'>" + "<form class='form-horizontal' role='form' id='create_user_form'>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机房名称：</b></label>" + "<div class='col-sm-9'>" + "<input id=\"serverRoom_name\"  name='serverRoom_name' type='text' class=\"form-control\" placeholder='只能包含中文、英文、数字、下划线...' />" + "</div>" + "</div>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机房备注：</b></label>" + "<div class='col-sm-9'>" + "<input id='serverRoom_remark' name='serverRoom_remark' type='text' class=\"form-control\" placeholder='字数限制2000字...' />" + "</div>" + "</div>" + "</form>" + "</div>",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                    var serverRoomName = $('#serverRoom_name').val();
                    var serverRoomRemark = $('#serverRoom_remark').val();
                    data = {
                        serverRoomName: serverRoomName,
                        serverRoomRemark: serverRoomRemark
                    };
                    console.log(data);
                    url = base + "serverRoom/createServerRoom";
                    $("#spinner").css("display", "block");
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector1).trigger("reloadGrid");
                        //tree.reAsyncChildNodes(node, "refresh",true);
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                    $(grid_selector1).trigger("reloadGrid");
                    //tree.reAsyncChildNodes(node, "refresh",true);
                }
            }
        }
    });
}

//新增机架
function showCreateRack() {
    bootbox.dialog({
        title: "<b>添加机架</b>",
        message: "<div class='well' style='margin-top: 1px;'>" + "<form class='form-horizontal' role='form' id='create_user_form'>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机架名称：</b></label>" + "<div class='col-sm-9'>" + "<input id=\"rack_name\"  name='rack_name' type='text' class=\"form-control\" placeholder='只能包含中文、英文、数字、下划线...' />" + "</div>" + "</div>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机架备注：</b></label>" + "<div class='col-sm-9'>" + "<input id='rack_remark' name='rack_remark' type='text' class=\"form-control\" placeholder='字数限制2000字...' />" + "</div>" + "</div>" + "</form>" + "</div>",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                    var rackName = $('#rack_name').val();
                    var rackRemark = $('#rack_remark').val();
                    data = {
                        rackName: rackName,
                        rackRemark: rackRemark,
                        serverRoomId: rack_serverRoomId
                    };
                    console.log(data);
                    url = base + "serverRoom/createRack";
                    $("#spinner").css("display", "block");
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector2).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                    $(grid_selector2).trigger("reloadGrid");
                }
            }
        }
    });
}

//删除机架
function removeRack(rackId) {
    var url = base + "serverRoom/deleteRack";
    data = {
        rackId: rackId
    };
    bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除该机架 吗?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function() {
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector2).trigger("reloadGrid");
                    });
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function() {}
            }
        }
    });
}

//编辑机架
function updateRackWin(rackId, rackName, rackRemark, serverRoomId) {
    bootbox.dialog({
        title: "<b>修改机架信息</b>",
        message: "<div class='well' style='margin-top: 1px;'>" + "<form class='form-horizontal' role='form' id='create_user_form'>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机架名称：</b></label>" + "<div class='col-sm-9'>" + "<input id=\"rack_name\"  name='rack_name' type='text' value='" + rackName + "' class=\"form-control\" placeholder='只能包含中文、英文、数字、下划线...' />" + "</div>" + "</div>" + "<div class='form-group'>" + "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;机架备注：</b></label>" + "<div class='col-sm-9'>" + "<input id='rack_remark' name='rack_remark' type='text' value='" + rackRemark + "' class=\"form-control\" placeholder='字数限制2000字...' />" + "</div>" + "</div>" + "</form>" + "</div>",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                    var rackName = $('#rack_name').val();
                    var rackRemark = $('#rack_remark').val();
                    data = {
                        rackId: rackId,
                        rackName: rackName,
                        rackRemark: rackRemark,
                        serverRoomId: serverRoomId
                    };
                    console.log(data);
                    url = base + "serverRoom/updateRack";
                    $("#spinner").css("display", "block");
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector2).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                    $(grid_selector2).trigger("reloadGrid");
                }
            }
        }
    });
}