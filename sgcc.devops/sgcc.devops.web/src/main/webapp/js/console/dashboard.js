var dashboard_line_obj = undefined,
	dashboard_line_num = 55;

$(document).ready(function(){
	$('#index_index_body').scroll(function() {
		var top = $(this).scrollTop(),
			height = 55 - $(this).scrollTop();
		if(top <= 45)
			dashboard_line_num = height;
		else{
			dashboard_line_num = 10;
		}
		dashboard_line_obj.height(dashboard_line_num);
	});
	var dashboard_line = 
		$('<div class="dashboard_switch">'+
			'<div  id="dashboard_line" style="width:10px;top: 0;height:55px;">'+
				'<div style="width:1px;margin-left: 4.5px;height:100%;background-color:#438EB9;"></div>'+
				'<div style="border-radius: 18px;float: left;position: relative;width: 10px;background-color: #438EB9;height: 10px;"></div>' + 
				'<div style="border-radius: 18px;width: 6px;position: absolute;bottom: 2px;left: 2px;background-color: white;height: 6px;"></div>' +
			'</div>'+
		'</div>');
	dashboard_line_obj = dashboard_line.find('#dashboard_line').on('click',function(){
		swithHeader();
	});
	$('body').append(dashboard_line);
});
function hideAndShow(){
	var indexBody = $('#index_index_body');
	if(indexBody.position().top == 0){
		indexBody.animate({top:'100%' }, "slow");
		$('div[id^="header_dashboard_"]').animate({top:'0' }, "slow");
		dashboard_line_obj.height(10);
	}else{
		$('div[id^="header_dashboard_"]').animate({top:'-100%' }, "slow");
		indexBody.animate({top:'0' }, "slow");
		dashboard_line_obj.height(dashboard_line_num);
	}
}
function topHideAndShow(hideElement,showElement){
	hideElement.hide();
	showElement.hide().css('position','static').slideDown(300);
//	hideElement.slideToggle(300,function(){showElement.hide().css('position','static').slideDown();});
}

function swithHeader(){
	var timestamp = (new Date()).getTime(),
	dashboard = $('div[id^="header_dashboard_"]');
	if(dashboard.length > 0){
		hideAndShow();
	}else{
		var height = $(window).height() - 8 * 2 - 3 * 6 - 31 * 3,
			top10Height = height * 0.45,
			normalHeight = height * 0.2525,
			html = '<div class="page-content" id="header_dashboard_' + timestamp + '" style="position:absolute;width: 100%;">'+
			'<div class="row" id="dashboard_container">' +
				'<div class="col-sm-12" id="dashboard_top10_cluster">' +
					'<div class="widget-box" style="border:none">' + 
						'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
							'<h5 class="widget-title"><b>集群负载 TOP10</b></h5>' +
						'</div>' + 
						'<div class="widget-body">' + 
							'<div class="widget-main" style="padding : 0">' + 
								'<div style="height:' + top10Height + 'px;"></div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
				
				'<div class="col-sm-6" style="border-right: 1px dashed rgb(204, 204, 204);">' +
					'<div class="widget-box" style="border:none">' + 
						'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
							'<h5 class="widget-title"><b>主机类型</b></h5>' +
						'</div>' + 
						'<div class="widget-body">' + 
							'<div class="widget-main" style="padding : 0">' + 
								'<div id="dashboard_ring-placeholder" style="height:' + normalHeight + 'px;"></div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
				
				'<div class="col-sm-6">' +
					'<div class="widget-box" style="border:none">' + 
						'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
							'<h5 class="widget-title"><b>组件类型</b></h5>' +
						'</div>' + 
						'<div class="widget-body">' + 
							'<div class="widget-main" style="padding : 0">' + 
								'<div id="dashboard_nginx-placeholder" style="height:' + normalHeight + 'px;"></div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
				
				'<div class="col-sm-6" style="border-right: 1px dashed rgb(204, 204, 204);">' +
					'<div class="widget-box" style="border:none">' + 
						'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
							'<h5 class="widget-title"><b>容器状态</b></h5>' +
						'</div>' + 
						'<div class="widget-body">' + 
							'<div class="widget-main" style="padding : 0">' + 
								'<div id="dashboard_pie-placeholder" style="height:' + normalHeight + 'px;"></div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
				
				'<div class="col-sm-6">' +
					'<div class="widget-box" style="border:none">' + 
						'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
							'<h5 class="widget-title"><b>镜像</b></h5>' +
						'</div>' + 
						'<div class="widget-body">' + 
							'<div class="widget-main" style="padding : 0">' + 
								'<div id="dashboard_image-placeholder" style="height:' + normalHeight + 'px;"></div>' +
							'</div>' +
						'</div>' +
					'</div>' +
				'</div>' +
			
			'</div>' +
		'</div>';
		$('body').prepend(html);
		
		/*真数据*/
		$.ajax({
			type : 'post',
			url : base + 'dashboard/getJson',
			dataType : 'json',
			success : function(result) {
				if (result && result.success)
					dataAnalysisFun(result.data);
			}
		});
		/*真数据*/
		
		var parentElement = $('#dashboard_container'),
			top10_cluster = $('#dashboard_top10_cluster'),
			ringElement = $('#dashboard_ring-placeholder'),
			pieElement = $('#dashboard_pie-placeholder'),
			nginxElement = $('#dashboard_nginx-placeholder'),
			topElement = $('.widget-main div',parentElement),
			imageElement = $('#dashboard_image-placeholder');
		
		/*假数据*/
//		var result = {"success":true,"data":{"hostInfo":{"docker":85,"swarm":5,"nginx":3,"registry":7},"containerInfo":{"running":70,"stop":30},"nginxInfo":{"con":20,"com":30},"imageInfo":[{"value":40,"name":"ubuntu"},{"value":60,"name":"mysql"}],"hostTopInfo":[{"name":"name","value":90},{"name":"name1","value":80},{"name":"name2","value":70},{"name":"name3","value":60},{"name":"name4","value":50},{"name":"name5","value":40},{"name":"name6","value":30},{"name":"name7","value":20},{"name":"数据源mysql","value":10},{"name":"name9","value":10}],"clusterInfo":20,"registryInfo":20,"businessInfo":20,"systemInfo":20}};
//		dataAnalysisFun(result.data);
		/*假数据*/
		
		var topHtml = '<div class="col-sm-12" id="top10_container">' +
			'<div class="widget-box" style="border:none">' +
				'<div class="widget-header widget-header-flat widget-header-small" style="background:none">' +
					'<h5 class="widget-title"><b>集群负载 TOP10</b></h5>' +
					'<div style="float: right"><a href="javascript:void(0);"><i class="glyphicon glyphicon-remove"></i></a></div>' +
				'</div>' +
				'<div class="widget-body">' +
					'<div class="widget-main" style="padding : 0">' +
						'<div style="height:' + top10Height + 'px;"></div>' +
					'</div>' +
				'</div>' +
			'</div>' +
		'</div>';
	
		function dataAnalysisFun(data) {
			createHostInfo(data.hostInfo);
			createContainerInfo(data.containerInfo);
			createImage(data.imageInfo);
			creagteNginx(data.nginxInfo);
			createTop10(topElement,data.hostTopInfo,true);
			
			hideAndShow();
		}
		// 主机
		function createHostInfo(data) {
			data = jQuery.extend({
				docker : 0,
				swarm : 0,
				registry : 0
			},data);
			var datas = [ {
				value : data.docker,
				name : 'Docker(' + data.docker + '台)'
			}, {
				value : data.swarm,
				name : 'Swarm(' + data.swarm + '台)'
			},{
				value : data.registry,
				name : 'Registry(' + data.registry + '台)'
			} ];
			
			if(data.docker == 0 && data.swarm == 0)
				datas = [];
			var option = {
				tooltip : false,
				legend : false,
				toolbox : false,
				calculable : false,
//				backgroundColor : '#e4e6e9',
				color : [ '#FFB980', '#5AB1EF', '#2EC7C9' ],
				series : [ {
					type : 'pie',
					startAngle : 120,
					radius : [ '60%', '80%' ],
					center : [ '50%', '60%' ],
					itemStyle : {
						normal : {
							label : {
								show : true
							},
							labelLine : {
								show : true
							}
						}
					},
					data : datas
				} ]
			}, myChart = echarts.init(ringElement.get(0));
			myChart.setOption(option);
		}
		// 容器统计信息
		function createContainerInfo(data) {
			data = jQuery.extend({
				running : 0,
				stop : 0
			},data);
			var datas = [ {
				value : data.running,
				name : '运行中(' + data.running + '台)'
			}, {
				value : data.stop,
				name : '已停止(' + data.stop + '台)'
			} ];
			if(data.running == 0 && data.stop == 0)
				datas = [];
			
			var option = {
				tooltip : false,
				legend : false,
				toolbox : false,
				calculable : false,
//				backgroundColor : '#e4e6e9',
				color : [ '#32CD32', 'red' ],
				series : [ {
					type : 'pie',
					radius : '80%',
					center : [ '50%', '60%' ],
					data : datas
				} ]
			}, myChart = echarts.init(pieElement.get(0));
			myChart.setOption(option);
		}
		// 镜像
		function createImage(data) {
			var values = [];data = data || [];
			jQuery.each(data,function(){
				values.push({
					name : this.name + '(' + this.value + '个)',
					value : this.value
				});
			});
			
			var option = {
				tooltip : false,
				legend : false,
				toolbox : false,
				calculable : false,
//				backgroundColor : '#e4e6e9',
				color : ['#5AB1EF', '#B5C334', '#2EC7C9','#C1232B', '#B5C334', '#FCCE10',
							'#E87C25', '#27727B', '#FE8463', '#9BCA63',
							'#FAD860', '#F3A43B', '#60C0DD', '#D7504B',
							'#C6E579', '#F4E001', '#F0805A'],
				series : [ {
					type : 'pie',
					radius : [ '60%', '80%' ],
					center : [ '50%', '60%' ],
					itemStyle : {
						normal : {
							label : {
								show : true
							},
							labelLine : {
								show : true
							}
						}
					},
					data : values
				} ]
			}, myChart = echarts.init(imageElement.get(0));
			myChart.setOption(option);
		}
		// 负载
		function creagteNginx(data) {
			data = jQuery.extend({
				con : 0,
				com : 0
			},data);
			
			var datas = [ {
				value : data.con,
				name : '容器Nginx(' + data.con + '台)'
			}, {
				value : data.com,
				name : '物理Nginx(' + data.com + '台)'
			} ];
			
			if(data.con == 0 && data.com == 0)
				datas = [];
			
			var option = {
				tooltip : false,
				legend : false,
				toolbox : false,
				calculable : false,
//				backgroundColor : '#e4e6e9',
				color : ['#B6A2DE', '#2EC7C9'],
				series : [ {
					type : 'pie',
					radius : [ '60%', '80%' ],
					center : [ '50%', '60%' ],
					itemStyle : {
						normal : {
							label : {
								show : true
							},
							labelLine : {
								show : true
							}
						}
					},
					data : datas
				} ]
			}, 
			myChart = echarts.init(nginxElement.get(0));
			myChart.setOption(option);
		}
		// host top 10
		function createTop10(element,data,click) {
			var values = [],names = [],markPoint = [];data = data || [];
			jQuery.each(data,function(i){
				values.push(this.value);
				names.push(this.name);
				markPoint.push({
					xAxis : i,
					y : 350,
					symbolSize : 20,
					name : this.name
				});
			});
			var option = {
				calculable : true,
				grid : {
					borderWidth : 0,
					x : 30,
					x2 : 1,
					y : 40,
					y2 : 1
				},
				xAxis : [ {
					type : 'category',
					show : false,
					data : names
				} ],
				yAxis : [ {
					type : 'value',
					show : false
				} ],
//				backgroundColor : '#e4e6e9',
				series : [ {
					type : 'bar',
					itemStyle : {
						normal : {
							color : function(params) {
								var colorList = [ '#C1232B', '#B5C334', '#FCCE10',
										'#E87C25', '#27727B', '#FE8463', '#9BCA63',
										'#FAD860', '#F3A43B', '#60C0DD', '#D7504B',
										'#C6E579', '#F4E001', '#F0805A' ];
								return colorList[params.dataIndex]
							},
							label : {
								show : true,
								position : 'top',
								formatter : '{b}\n{c}%'
							}
						}
					},
					data : values,
					markPoint : {
						data : markPoint
					}
				} ]
			}, myChart = echarts.init(element.get(0));
			myChart.setOption(option);
			click && myChart.on(echarts.config.EVENT.CLICK, function(param) {
		    	var timestamp = 'top10_' + param.data + '_' + param.dataIndex ,
		    		element = $('[id^="' + timestamp + '"]');
		    	if(element.length > 0){
		    		topHideAndShow(top10_cluster,element);
		    		return;
		    	}
		    	timestamp += '_' + (new Date()).getTime()
		    	parentElement.prepend(topHtml);
		    	var element = $('#top10_container',parentElement).attr('id', timestamp),
		    	data = {
		    		name : param.name,
		    		data : param.data
		    	};
		    	element.find('a').click(function(){
		    		topHideAndShow(element,top10_cluster);
		    	});
		    	element.find('.widget-title b').html(param.name + '集群TOP10');
		    	$.ajax({
		    		type : 'post',
		    		url : base + 'dashboard/getTop10Json',
		    		dataType : 'json',
		    		data : data,
		    		success : function(result) {
		    			if (result && result.success)
		    				createTop10(element.find('.widget-main div'),result.data,false);
		    				topHideAndShow(top10_cluster,element);
		    		}
		    	});
		    });
		}
	}
}