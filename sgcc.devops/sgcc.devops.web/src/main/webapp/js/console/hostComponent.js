var grid_selector = "#component_list";
var page_selector = "#component_page";
var tree;
var timestamp;
var treeData = [];
var setting = {
    check: {
        enable: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: zTreeOnClick
    }
};

var treeUrl = base + "parameter/selectByType";

function createZTreeFun(selector) {
    tree = $.fn.zTree.init($(selector), setting, treeData);
    tree.expandAll(true);
}
jQuery(function ($) {
	initTree();
	hostComponentManager();
});
function initTree(){
	// 初始化树
    $.ajax({
        url: treeUrl,
        // 要加载数据的地址
        type: 'get',
        data: 'type=1',
        // 加载方式
        dataType: 'json',
        // 返回数据格式
        success: function (response) {
            if (response == "") {
                showMessage("数据加载异常！");
            } else {
                treeData.push({
                    id: 0,
                    name: '组件'
                });

                $.each(response, function (index, data) {
                    treeData.push({
                        id: data.id,
                        name: data.paraValue,
                        nodeId: data.id,
                        code: data.paraCode,
                        type: 'component',
                        pId: 0
                    });
                });
                createZTreeFun("#roleAuthTree");
            }
        },
        error: function (response) {
            console.log(response);
        }
    });
}
// 节点点击事件
function zTreeOnClick(event, treeId, treeNode) {
	selectVersionClick();
}
function selectVersionClick() {
	var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	var hostComType = '',code='';
	if(nodes.length>0&&nodes[0].code!=0){
		code = nodes[0].code; //获取选中节点的值
		hostComType = nodes[0].type; //获取选中节点的值
	}
	if (hostComType == "component") {
		var version =$("#hostComVersion").val();
	    jQuery(grid_selector).jqGrid('setGridParam',{url : base+'hostComponent/hostComponentList?version='+version+'&type='+code}).trigger("reloadGrid");
	}else{
		
	}
    
}

// 机房管理表
function hostComponentManager() {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
        jQuery(grid_selector).jqGrid({
        url: base + 'hostComponent/hostComponentList',
        datatype: "json",
        height: '100%',
        autowidth: true,
        colNames: ['版本号', '文件', '升级脚本', '安装脚本', '更新是否转移容器', '操作'],
        colModel: [{
                name: 'version',
                index: 'version',
                width: 5
            }, {
                name: 'filename',
                index: 'filename',
                width: 5
            },{
                name: 'upgradeCommand',
                index: 'upgradeCommand',
                width: 5
            },{
                name: 'installCommand',
                index: 'installCommand',
                width: 5
            },{
                name: 'transfer',
                index: 'transfer',
                formatter : function(cellvalue, options,
						rowObject) {
					if(cellvalue==1) {
					return "是";
					}
					else{
					return 	"否";
					}
				},
                width: 5
            }, {
                name: '',
                index: '',
                width: 300,
                align: 'left',
                fixed: true,
                sortable: false,
                resize: false,
                title: false,
                formatter: function (cellvalue, options,
                                     rowObject) {
                    return "<a href='#modal-wizard' data-toggle='modal'>"
                    + "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"selectById('"
                    + rowObject.id
                    + "')\">"
                    + "<i class=\"ace-icon glyphicon glyphicon-zoom-in bigger-125\"></i>"
                    + "<b>查看</b>"
                    + "</button>"
                    + "</a> &nbsp;"
                    + "<a href='#modal-wizard' data-toggle='modal'>"
                    + "<button class=\"btn btn-xs btn-danger btn-round\" onclick=\"removehostComponent('"
                    + rowObject.id
                    + "')\">"
                    + "<i class=\"ace-icon fa fa-minus-circle bigger-125\"></i>"
                    + "<b>删除</b>"
                    + "</button>"
                    + "</a> &nbsp;"
                    + "<a href='#migrateCluster' data-toggle='modal'>"
                    + "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"updatehostComponent('"
                    + rowObject.id
                    + "')\">"
                    + "<i class=\"ace-icon fa fa-pencil-square-o bigger-125\"></i>"
                    + "<b>编辑</b>"
                    + "</button>"
                    + "</a> &nbsp;"
                    + "<a href='#modal-wizard' data-toggle='modal'>"
                    + "<button class=\"btn btn-xs btn-success btn-round\" id=\"autoHostComponent\" onclick=\"autohostComponent('"
                    + rowObject.id
                    + "')\">"
                    + "<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
                    + "<b>自动升级</b>"
                    + "</button>"
                    + "</a> &nbsp;";
                }
            }],
        viewrecords: true,
        rowNum: 10,
        rowList: [10, 20, 50, 100, 1000],
        pager: page_selector,
        altRows: true,
        sortname: 'version',
        sortorder: "asc",
        jsonReader: {
            root: "rows",
            total: "total",
            page: "page",
            records: "records",
            repeatitems: false
        },
        loadError: function (resp) {
            if (resp.responseText.indexOf("会话超时") > 0) {
                alert('会话超时，请重新登录！');
                document.location.href = '/login.html';
            }
        },
        loadComplete: function () {
            var table = this;
            setTimeout(function () {
                updateActionIcons(table);
                updatePagerIcons(table);
                enableTooltips(table);
            }, 0);
            $("#formdiv").show();
        }
    });
}

$(window).triggerHandler('resize.jqGrid'); // 窗口resize时重新resize表格，使其变成合适的大小
jQuery(grid_selector).jqGrid( // 分页栏按钮
    'navGrid', page_selector, {
        edit: false,
        add: false,
        del: false,
        search: false,
        refresh: true,
        refreshstate: 'current',
        refreshicon: 'ace-icon fa fa-refresh',
        view: false
    }, {}, {}, {}, {}, {});

function updateActionIcons(table) {
}
function updatePagerIcons(table) {
    var replacement = {
        'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
        .each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
            if ($class in replacement)
                icon.attr('class', 'ui-icon ' + replacement[$class]);
        });
}

function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({
        container: 'body'
    });
    $(table).find('.ui-pg-div').tooltip({
        container: 'body'
    });
}

function autohostComponent(hostComponentId){
	 var url = base + "hostComponent/autoHostComponent";
	    data = {
	    		id: hostComponentId
	    };

	    $("#spinner").css("display", "block");
	    $("#autoHostComponent").attr("disabled", true);
        $.post(url, data, function (response) {
            showMessage(response.message);
            $(grid_selector).trigger("reloadGrid");
            $("#spinner").css("display", "none");
            $("#autoHostComponent").attr("disabled", false);
        });
    
}
function removehostComponent(hostComponentId) {
    var url = base + "hostComponent/deleteHostComponent";
    data = {
    		id: hostComponentId
    };
    bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除该主机组件 吗?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function() {
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                    });
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function() {}
            }
        }
    });
}

// 修改主机组件信息
function updatehostComponent(id) {
	var url = base + "hostComponent/selectById";
    data = {
    		id: id
    };
	$.post(url, data, function(response) {
    bootbox.dialog({
        title: "<i class='ace-icon fa fa-pencil-square-o bigger-125'></i>&nbsp;<b>修改主机组件信息</b>",
        message:template('hostComponentUpdate',response) ,
        className: "hostComponent",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function () {
                    var version = $('#version').val();
                    var statusCommand = $('#statusCommand').val();
                    var installCommand = $('#installCommand').val();
                    var upgradeCommand = $('#upgradeCommand').val();
                    var versionCommand = $('#versionCommand').val();
                    
                    var transfer = $('#transfer').val();
                    if ($('#transfer').get(0).checked) {
                    	transfer=1;
                    }
                    else {
                    	transfer=0;
                    }
                    
                    var appFilePath = $('#appFilePath').val();
                    var appUuid = $('#appUuid').val();
                    
//                    var file = document.getElementById("file").files[0];
//                    var filename ="";
//                    if(file){
//                    	 filename = file.name;
//                    }else{
//                    	filename=document.getElementById("filename").value;
//                    }
                    
                    data = {
                    		id: id,
                    		version: version,
                    		filename:appFilePath,
                    		statusCommand:statusCommand,
                    		installCommand:installCommand,
                    		versionCommand:versionCommand,
                    		upgradeCommand:upgradeCommand,
                    		transfer:transfer,
                    };
                    console.log(data);
                    url = base + "hostComponent/updateHostComponent";
                    $("#spinner").css("display", "block");
                    $.post(url, data, function (response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function () {
                    $(grid_selector).trigger("reloadGrid");
                }
            }
        }
    });
	});
}

// 新增机房
function showCreateHostComponent() {
	var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	if(nodes.length==0){
		bootbox.alert('请选择组件类型');
		return false;
	}
	timestamp=Math.round(new Date().getTime()/1000);
    bootbox.dialog({
        title: "<i class='ace-icon fa fa-pencil-square-o bigger-125'></i>&nbsp;<b>添加主机组件</b>",
        message: template('hostComponent'),
        className: "hostComponent",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function () {
                	//选中节点
                	var selectnodecode = nodes[0].code; //获取选中节点的值
                	
                	 var version = $('#version').val();
                     var statusCommand = $('#statusCommand').val();
                     var installCommand = $('#installCommand').val();
                     var upgradeCommand = $('#upgradeCommand').val();
                     var versionCommand = $('#versionCommand').val();

                     var transfer = $('#transfer').val();
                     if ($('#transfer').get(0).checked) {
                     	transfer=1;
                     }
                     else {
                     	transfer=0;
                     }
                     
                     var appFilePath = $('#appFilePath').val();
                     var hostInsallPath = $('#hostInsallPath').val();
                     var hostCompUUid =  $('#hostCompUUid').val();
                     if(version==""||null==version){
                    	 bootbox.alert("请填写版本号");
                    	 return false;
                     }
                     if(appFilePath==""||null==appFilePath){
                    	 bootbox.alert("请选择需要上传的文件");
                    	 return false;
                     }
                    data = {
                    		id:hostCompUUid,
                    		type:selectnodecode,
                    		version: version,
                    		filename:appFilePath,
                    		statusCommand:statusCommand,
                    		installCommand:installCommand,
                    		versionCommand:versionCommand,
                    		upgradeCommand:upgradeCommand,
                    		localPath:hostInsallPath,
                    		transfer:transfer
                    };
                    console.log(data);
                    url = base + "hostComponent/createHostComponent";
                    $("#spinner").css("display", "block");
                    $.post(url, data, function (response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                        // tree.reAsyncChildNodes(node, "refresh",true);
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function () {
                    $(grid_selector).trigger("reloadGrid");
                    // tree.reAsyncChildNodes(node, "refresh",true);
                }
            }
        }
    });
    initFileUpDialog();
}
function selectById(hostComponentId) {
    var url = base + "hostComponent/selectById";
    data = {
    		id: hostComponentId
    };
	$.post(url,data,function(response){
		 bootbox.dialog({
		    	title: "<b>查看主机组件信息</b>",
		        message: template('hostComponentRead',response),
		        className: "hostComponent",
		        buttons: {
		            cancel: {
		                label: "<i class='icon-info'></i> <b>返回</b>",
		                className: "btn-sm btn-danger btn-round",
		                callback: function() {}
		            }
		        }
		    });
	});
   
}
//var paragraph = 4*1024*1024;//64*1024
//var startSize,endSize = 0;
//var i = 0; j = 0;
//var count =0;
//function validate(filename){
//	var reg = new RegExp("[u4e00-u9fa5]");
//    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]");
//    if(!reg.test(filename) && !pattern.test(filename)){
//    	bootbox.alert("上传的文件名不能包含汉字和特殊字符");
//    	return false;
//    }else{
//    	return true;
//    }
//}
////上传文件
//function UpladFilexxx() {
//	var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
//	//选中节点
//	var nodes=tree.getSelectedNodes(true);
//	if(nodes.length==0){
//		showMessage('请选择节点');
//		return false;
//	}
//	var selectnodecode = nodes[0].code; //获取选中节点的值
//	
//    var fileObj = document.getElementById("file").files[0]; // 获取文件对象
//    var FileController = "/file/uploadFile";                    // 接收上传文件的后台地址 
//    // FormData 对象
//    var form = new FormData();
//    form.append("timestamp", selectnodecode+"/"+timestamp); 
//    form.append("file", fileObj); 
//    form.append("fileType", "hostCom");// 上传文件类型
//    // XMLHttpRequest 对象
//    var xhr = new XMLHttpRequest();
//    xhr.open("post", FileController, true);
//    xhr.onload = function (resp) {
//    	$("#appFilePath").val(selectnodecode+"/"+timestamp+"/"+fileObj.name);
//    	showMessage("上传完成!");
//    };
//    xhr.upload.addEventListener("progress", progressFunction, false);
//    $("#progressBar").attr("hidden",false);
//    xhr.send(form);
//}
//function progressFunction(evt) {
//    var progressBar = document.getElementById("progressBar");
//    var percentageDiv = document.getElementById("percentage");
//    if (evt.lengthComputable) {
//        progressBar.max = evt.total;
//        progressBar.value = evt.loaded;
//        percentageDiv.innerHTML = Math.round(evt.loaded / evt.total * 100) + "%";
//        if(evt.loaded==evt.total){
//        	setTimeout('$("#progressBar").prop("hidden",true)', 2000);
//        }
//    }
//
//} 
//var xmlhttp;
//function UpladFile(){
//	var tree = $.fn.zTree.getZTreeObj('roleAuthTree');
//	//选中节点
//	var nodes=tree.getSelectedNodes(true);
//	if(nodes.length==0){
//		showMessage('请选择节点');
//		return false;
//	}
//	var selectnodecode = nodes[0].code; //获取选中节点的值
//	
//    var fileObj = document.getElementById("file").files[0]; // 获取文件对象
//    var url = "/file/uploadFile";                    // 接收上传文件的后台地址 
//    // FormData 对象
//    var form = new FormData();
//    form.append("timestamp", selectnodecode+"/"+timestamp); 
//    form.append("file", fileObj); 
//    form.append("fileType", "hostCom");// 上传文件类型
//    
//    
//	var xmlhttprequest;
//    if(window.XMLHttpRequest){
//        xmlhttprequest=new XMLHttpRequest();
//        if(xmlhttprequest.overrideMimeType){
//            xmlhttprequest.overrideMimeType("text/xml");
//        }
//    }else if(window.ActiveXObject){
//        var activeName=["MSXML2.XMLHTTP","Microsoft.XMLHTTP"];
//        for(var i=0;i<activeName.length;i++){
//            try{
//                xmlhttprequest=new ActiveXObject(activeName[i]);
//                break;
//            }catch(e){
//                       
//            }
//        }
//    }
//    if(xmlhttprequest==undefined || xmlhttprequest==null){
//        alert("XMLHttpRequest对象创建失败！！");
//    }else{
//        xmlhttp=xmlhttprequest;
//    }
//    xmlhttp.onreadystatechange=callback;
////    xmlhttp.open("post", FileController, true);
//    //进度条
//    xmlhttp.upload.addEventListener("progress", progressFunction, false);
//    $("#progressBar").attr("hidden",false);
//  //解决缓存的转换
////    if(url.indexOf("?")>=0){
////        url=url + "&t=" + (new Date()).valueOf();
////    }else{
////        url=url+"?t="+(new Date()).valueOf();
////    }
////    
////    //解决跨域的问题
////    if(url.indexOf("http://")>=0){
////        url.replace("?","&");
////        url="Proxy?url=" +url;
////    }
//    xmlhttp.open("post",url,true);
//    
//    //如果是POST方式，需要设置请求头
////    xmlhttp.setRequestHeader("Content-type","application/x-www-four-urlencoded");
//    xmlhttp.onload = function (resp) {
//    	$("#appFilePath").val(selectnodecode+"/"+timestamp+"/"+fileObj.name);
//    };
//    xmlhttp.send(form);
//}
//function callback(){
//	if(xmlhttp.readyState==4){
//		 //表示服务器的相应代码是200；正确返回了数据 
//		if(xmlhttp.status==200){
//            var responseText=xmlhttp.responseText;
//            var responseXML=xmlhttp.reponseXML;
//            if(callback==undefined || callback==null){
//                alert("没有设置处理数据正确返回的方法");
//                alert("返回的数据：" + responseText);
//            }else{
//            	showMessage("上传完成!");
//            }
//        }else{
//        	showMessage("上传失败，"+"HTTP的响应码：" + xmlhttp.status + ",响应码的文本信息：" + xmlhttp.statusText);
//        }
//	}
//}

//全局的websocket变量
var paragraph = 4*1024*1024;//64*1024
var uploadFile;
var startSize,endSize = 0;
var i = 0; j = 0;
var count =0;
var socket_upconfig = undefined;

/**
 * 上传应用包文件 开始
 * @param libraryFileStartName
 */
function sendFileStart(hostCompFileStartName){
	socket_upconfig.send(JSON.stringify({
        'hostCompFileStartName': hostCompFileStartName
	}));
}
//发送文件上传消息
function sendfileEnd(){
	socket_upconfig.send(JSON.stringify({
	        'hostCompFileSendover': 'hostCompFileSendover'
	  }));
}
function fileUpDisconnect(){
//	socket.close();
	socket_upconfig.close();
    console.log("Disconnected");
}
/**
 * 上传应用包文件
 * @param obj
 */
function sendFileUpArraybuffer(obj){
	console.log(obj);
    if(obj.content == "OK"){
	    if(endSize < uploadFile.size){
	    	var blob;
	        startSize = endSize;
	        endSize += paragraph ;
	        if (uploadFile.webkitSlice) { 
	              blob = uploadFile.webkitSlice(startSize, endSize);
	        } else if (uploadFile.mozSlice) {
	              blob = uploadFile.mozSlice(startSize, endSize);
	        }
	        else{
	        	 blob = uploadFile.slice(startSize, endSize);
	        }
	        var reader = new FileReader();
	        reader.onload = function loaded(evt) {
	            var ArrayBuffer = evt.target.result;
	            i++;
	            var isok = (i / count) *100;
	            $("#hostCompShowId").text("已经上传"+isok.toFixed(2) +"%");
	            $("#hostCompProgressId").width((isok.toFixed(2)*0.7) +"%");
	            socket_upconfig.send(ArrayBuffer);
            };
            reader.readAsArrayBuffer(blob);
	    } else{
            startSize = endSize = 0;
            i = 0;
            $("#hostCompShowId").text("上传完成！");
            $("#hostCompProgressId").width("100%");
            sendfileEnd();
            setTimeout(function() {
            	$("#hostCompPid").hdie();
			}, 1000);
            fileUpDisconnect();
	    }
    }  else{
    	console.log(obj.content);
    }
}

function initFileUpDialog(){
	WebsocketConnect();
	if (window.File && window.FileReader && window.FileList && window.Blob){
		$('#file').change(function() {
			uploadFile = document.getElementById("file").files[0];
	        if(undefined == uploadFile || null == uploadFile){
	        	showMessage("请选择应用文件！");
	        	return;
	        }
	        var uploadFileName = uploadFile.name.split(".");
	        if(!validate(uploadFileName[0])){
	        	showMessage("上传文件包含非法字符");
	        	return;
	        }
	        if(uploadFile!=null) {
	        	 console.log(uploadFile);
	        	 count = parseInt(uploadFile.size/paragraph)+1;
	        	 $("#hostCompPid").show();
	        	 $("#hostCompShowId").text("开始上传！");
	        	 sendFileStart(uploadFile.name);
	        }else{
	        	showMessage("请先选择需要上传的应用文件！");
	        	return;
        	}
	    });
	}else {
	    	bootbox.alert('您的浏览器不支持html5 Web socket 上传功能.');
	}

}
function WebsocketConnect(){
	 if ('WebSocket' in window) {
       var host = window.location.host+base+"/";
       if(window.location.protocol=='http:'){
       	socket_upconfig = new WebSocket('ws://' + host + '/uploadService');
       }else if(window.location.protocol=='https:'){
       	socket_upconfig = new WebSocket('wss://' + host + '/uploadService');
       }else{
       	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
       }
       
       socket_upconfig.onopen = function () {};
       socket_upconfig.onclose = function () {};
       socket_upconfig.onerror = function(e) {};
       socket_upconfig.onmessage = function (event) {
		   console.log(event.data);
		   var obj = JSON.parse(event.data);
		   if(obj.messageType =="ws_up_file"){
		       sendFileUpArraybuffer(obj);
		   }
           if(obj.hostCompFilePath && obj.hostCompUUid){
           	$("#appFilePath").val(obj.hostCompFilePath);
           	$("#hostInsallPath").val(obj.hostInsallPath);
           	$("#hostCompUUid").val(obj.hostCompUUid);
           }
       };
   } else {
       console.log('Websocket not supported');
   }
}
function validate(filename){
	var reg = new RegExp("[u4e00-u9fa5]");
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]");
    if(!reg.test(filename) && !pattern.test(filename)){
    	bootbox.alert("上传的文件名不能包含汉字和特殊字符");
    	return false;
    }else{
    	return true;
    }
}
