var grid_selector = "#port_list";
var page_selector = "#port_page";
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	
	
    
	jQuery(grid_selector).jqGrid({
		url : base+'port/list',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','端口', '类型'],
		colModel : [ 
            {name : 'id',index : 'id',width : 15,hidden:true},
			{name : 'port',index : 'port',width : 15},
			{name : 'type',index : 'type',width : 11}
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'type',
		sortorder: "asc",
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("登录超时") > 0 ){
				alert('登录超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
	}

});

function removePort(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.id;
		var port = rowData.port;
		data = {
				id:id,
				port:port
			};
		bootbox.dialog({
	        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除端口  ('+port+') 吗?</div>',
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	$.post(base+'port/delete',data,function(response){
	                		showMessage(response.message);
							$(grid_selector).trigger("reloadGrid");
						});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}
	
}

//
function showCreateWin(){
	bootbox.dialog({
		title : "<b>添加端口</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_port_form'>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>端口：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id='port' name='port' type='text' class=\"form-control\" placeholder='  请输入端口' onblur='checkPortForm()'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
			  					"<label class='col-sm-3'><div align='right'><b>类型：</b></div></label>"+
			  					"<div class='col-sm-9'>"+
			  						"<select id='type_create' name='type' class='form-control'>"+
			  						"<option value='nginx'>nginx</option>"+
//			  						"<option value='nginxGroup'>nginx组</option>"+
				  					"</select>"+
				  				"</div>"+
				  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
				  			"</div>"+
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
					    	    var port = $('#port').val();
					            var type = $('#type_create').val();
							data={
									port: port,
									type: type
							};
							console.log(data);
							url=base+"port/insert";
							$("#spinner").css("display","block");
							$.post(url,data,function(response){
								showMessage(response.message);
								$(grid_selector).trigger("reloadGrid");
								$("#spinner").css("display","none");
							});
					    }
			},
			    "cancel" : {
				"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {
					$(grid_selector).trigger("reloadGrid");
				}
			}
		}
	});
	$("#saveButt").parent().attr("disabled","disabled");
}

function checkPortForm(){
	validToolObj.isPort("#port");
    validToolObj.validForm('#create_port_form',"#saveButt",['#port']);
}

function updateWin(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.id;
		var port = rowData.port;
		var type = rowData.type;
		var url= base+'port/update';
		var title  = '<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>&nbsp;<b>修改端口</b>';
		bootbox.dialog({
			title : title,
			message :"<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_port_form'>"+
							"<input type='hidden' name='id' id='id' value='"+id+"'>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>端口：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
				  					"<input id='port' name='port' type='text' value='"+port+"' class=\"form-control\" placeholder='  请输入端口' onblur='checkPortForm()'/>"+
				  				"</div>"+
				  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
				  			"</div>"+
				  			"<div class='form-group'>"+
									"<label class='col-sm-3'><div align='right'><b>类型：</b></div></label>"+
									"<div class='col-sm-9'>"+
										"<select id='update_type' name='update_type' class='form-control'>"+
										"<option value='nginx'>nginx</option>"+
				  					"</select>"+
				  				"</div>"+
				  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
				  			"</div>"+
				  		"</form>"+
				  		"</div>",
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						var port = $('#port').val();
						var type = $('#update_type').val();
						data={
								id:id,
								port:port,
								type:type
								};
						$.post(url,data,function(response){
								showMessage(response.message);
								$(grid_selector).trigger("reloadGrid");
						});
					}
				},
				"cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
						$(grid_selector).trigger("reloadGrid");
					}
				}
			}
		});
	}
	
	$(grid_selector).trigger("reloadGrid");
}

function searchHost(){
	type = $('#type').val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'port/list?type='+type}).trigger("reloadGrid");
}
