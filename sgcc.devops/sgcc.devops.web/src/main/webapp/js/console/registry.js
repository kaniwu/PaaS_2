var grid_selector = "#registry_list";
var page_selector = "#registry_page";
/*设置保存查询镜像列表的仓库ID全局变量*/
jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
        $(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
        $(grid_selector).closest(".ui-jqgrid-bdiv").css({
            'overflow-x': 'hidden'
        });
    });
    var parent_column = $(grid_selector).closest('[class*="col-"]');
    $(document).on(
        'settings.ace.jqGrid',
        function (ev, event_name, collapsed) {
            if (event_name === 'sidebar_collapsed'
                || event_name === 'main_container_fixed') {
                setTimeout(function () {
                    $(grid_selector).jqGrid('setGridWidth',
                        parent_column.width());
                }, 0);
            }
        });
//    jQuery(grid_selector)
//        .jqGrid(
//            {
//                url: base + 'registry/listWithIP',
//                datatype: "json",
//                height: '100%',
//                autowidth: true,
//                colNames: ['仓库ID', '仓库名称', '仓库端口', '主机ID', '主机地址',
//                    '描述信息', '生成时间', '用户ID', '创建人', '仓库状态', '快捷操作'],
//                colModel: [
//                    {name: 'registryId', index: 'registryId', width: 5, hidden: true},
//                    {name: 'registryName', index: 'registryName', width: 8, align: 'left'},
//                    {name: 'registryPort', index: 'registryPort', width: 4, sortable: false, align: 'left'},
//                    {name: 'hostId', index: 'hostId', width: 8, hidden: true},
//                    {name: 'hostIP', index: 'hostIP', width: 8, align: 'left'},
//                    {
//                        name: 'registryDesc', index: 'registryDesc', width: 14, sortable: false,
//                        formatter: function (cellvalue, options, rowObject) {
//                            return stringTool.escape(rowObject.registryDesc);
//                        }
//                    },
//                    {name: 'registryCreatetime', index: 'registryCreatetime', width: 8},
//                    {name: 'registryCreator', index: 'registryCreator', width: 8, hidden: true},
//                    {name: 'creatorName', index: 'creatorName', width: 8, align: 'left'},
//                    {
//                        name: 'registryStatus', index: 'registryStatus', width: 6, align: 'left',
//                        formatter: function (cellvalue, options,
//                                             rowObject) {
//                            switch (cellvalue) {
//                                case 0:
//                                    return '删除   ';
//                                case 1:
//                                    return '正常    '
//                                        + "<button class=\"btn btn-xs btn-success btn-round\" onclick=\"stopRegistry('" + rowObject.registryId + "',5)\">"
//                                        + "<i class=\"ace-icon fa fa-stop bigger-125\"></i>"
//                                        + "<b>停止</b></button> &nbsp;";
//                                case 2:
//                                    return '异常   '
//                                        + "<button class=\"btn btn-xs btn-success btn-round\" disabled=\"disabled\">"
//                                        + "<b> 无操作 </b></button> &nbsp;";
//                                case 3:
//                                    return '停止   '
//                                        + "<button class=\"btn btn-xs btn-success btn-round\" onclick=\"startRegistry('" + rowObject.registryId + "')\">"
//                                        + "<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
//                                        + "<b>启动</b></button> &nbsp;";
//                                default:
//                                    return '未知   '
//                                        + "<button class=\"btn btn-xs btn-success btn-round\" disabled=\"disabled\">"
//                                        + "<b> 无操作 </b></button> &nbsp;";
//                            }
//                        }
//                    },
//                    {
//                        name: '',
//                        index: '',
//                        width: 80,
//                        fixed: true, /* 固定像素长度 */
//                        sortable: false,
//                        resize: false,
//                        align: 'center',
//                        title: false,
//                        formatter: function (cellvalue, options,
//                                             rowObject) {
//                            rowObject.registryDesc = stringTool.escape(rowObject.registryDesc);
//                            return "<button class=\"btn btn-purple btn-xs btn-round\" onclick=\"queryRegistryImages('"
//                                + rowObject.registryId + "','"
//                                + rowObject.registryName
//                                + "')\"><i class=\"ace-icon fa fa-bullseye\"></i>&nbsp;<b>镜像</b></button> &nbsp;";
//                        }
//                    }],
    jQuery(grid_selector)
    .jqGrid(
        {
            url: base + 'registry/listWithIP',
            datatype: "json",
            height: '100%',
            autowidth: true,
            colNames: ['仓库ID', '仓库名称', '负载类型', '负载地址','负载地址','端口','描述信息', '生成时间',  '创建人', '仓库状态', '快捷操作'],
            colModel: [
                  {name: 'id', index: 'id', width: 5, hidden: true},
                      {name: 'registryName', index: 'registryName', width: 8, align: 'left'},
                      {name: 'lbType', index: 'lbType', width: 8,sortable: false,
                    	   formatter: function (cellvalue, options,
                                   rowObject) {
                      switch (cellvalue) {
                      case 1:
                          return 'nginx    ';
                      case 2:
                          return 'f5   ';
                      default:
                          return '未知   ';
                    }
                   }},
                      {name: 'lbIp', index: 'lbIp', width: 8, align: 'left',sortable: false, hidden: true},
                      {name: 'domainName', index: 'domainName', width: 8, align: 'left',sortable: false},
                      {name: 'lbPort', index: 'lbPort', width: 8, align: 'left',sortable: false},
                      {
                      	name: 'registryDesc', index: 'registryDesc', width: 14, sortable: false,
                      	formatter: function (cellvalue, options, rowObject) {
                      		return stringTool.escape(rowObject.registryDesc);
                      	}
                      },
                      {name: 'registryCreatetime', index: 'registryCreatetime', width: 8},
                      {name: 'registryCreator', index: 'registryCreator', width: 8},
                      {
                        name: 'status', index: 'status', width: 6, align: 'left',sortable: false,
                        formatter: function (cellvalue, options,
                                             rowObject) {
                            switch (cellvalue) {
                                case 0:
                                    return '删除   ';
                                case 1:
                                    return '正常    ';
                                case 2:
                                    return '停止   ';
                                default:
                                    return '未知   ';
                            }
                        }
                     },
                     {
                       name: '',
                       index: '',
                       width: 150,
                       fixed: true, /* 固定像素长度 */
                       sortable: false,
                       resize: false,
                       align: 'center',
                       title: false,
                       formatter: function (cellvalue, options,
                                            rowObject) {
                           rowObject.registryDesc = stringTool.escape(rowObject.registryDesc);
                           return  "<button class=\"btn btn-primary btn-xs btn-round\" onclick=\"registryLbDetails('"
                           + rowObject.id + "')\"><i class=\"ace-icon glyphicon glyphicon-zoom-in bigger-125\"></i>&nbsp;<b>查看</b></button> &nbsp;" 
                               +"<button class=\"btn btn-purple btn-xs btn-round\" onclick=\"queryRegistryImages('"
                               + rowObject.id + "','"
                               + rowObject.registryName
                               + "')\"><i class=\"ace-icon fa fa-bullseye\"></i>&nbsp;<b>镜像</b></button> &nbsp;";
                       }
                   }],
                viewrecords: true,
                rowNum: 10,
                rowList: [10, 20, 50, 100, 1000],
                pager: page_selector,
                altRows: true,
                sortname: 'registryName',
                sortname: 'LbIp',
                sortname: 'registryCreatetime',
                sortname: 'registryCreator',
                sortorder: "asc",
                multiselect: true,
                multiboxonly:true,
                jsonReader: {
                    root: "rows",
                    total: "total",
                    page: "page",
                    records: "records",
                    repeatitems: false
                },
                loadError: function (resp) {
                    if (resp.responseText.indexOf("会话超时") > 0) {
                        alert('会话超时，请重新登录！');
                        document.location.href = '/login.html';
                    }
                },
                loadComplete: function () {
                    var table = this;
                    setTimeout(function () {
                        styleCheckbox(table);
                        updateActionIcons(table);
                        updatePagerIcons(table);
                        enableTooltips(table);
                    }, 0);
                }
            });
    $(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
    jQuery(grid_selector).jqGrid(// 分页栏按钮
        'navGrid', page_selector, { // navbar options
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: true,
            refreshstate: 'current',
            refreshicon: 'ace-icon fa fa-refresh',
            view: false
        }, {}, {}, {}, {}, {});
    function updateActionIcons(table) {
    }

    function updatePagerIcons(table) {
        var replacement = {
            'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
            'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
            'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
            'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
        };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
            .each(
                function () {
                    var icon = $(this);
                    var $class = $.trim(icon.attr('class').replace(
                        'ui-icon', ''));
                    if ($class in replacement)
                        icon.attr('class', 'ui-icon '
                            + replacement[$class]);
                });
    }

    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({
            container: 'body'
        });
        $(table).find('.ui-pg-div').tooltip({
            container: 'body'
        });
    }

    function styleCheckbox(table) {
//		$(table).find('input:checkbox').addClass('ace').wrap('<label />')
//				.after('<span class="lbl align-top" />')
//		$('.ui-jqgrid-labels th[id*="_cb"]:first-child').find(
//				'input.cbox[type=checkbox]').addClass('ace').wrap('<label />')
//				.after('<span class="lbl align-top" />');
    }
});

/**
 * @author yangqinglin
 * @datetime 2015年9月25日 18:48
 * @description 批量同步仓库下镜像的函数
 */
function batchSyncRegistryImages() {
    var ids = $(grid_selector).jqGrid("getGridParam",
        "selarrrow");
    var regis_name_string = "";
    var regis_ipaddr_string = "";
    var regis_port_string = "";
    var regis_ids = "";
    if (ids.length == 0) {
        showMessage("请点击选择需要同步的仓库!");
        return;
    }
    for (var i = 0; i < ids.length; i++) {
        var rowData = $(grid_selector).jqGrid("getRowData",
            ids[i]);
        regis_ids += (i == ids.length - 1 ) ? rowData.registryId
            : rowData.registryId + ',';
        regis_name_string += (i == ids.length - 1) ? rowData.registryName
            : rowData.registryName + ',';
        regis_ipaddr_string += (i == ids.length - 1) ? rowData.hostIP
            : rowData.hostIP + ',';
        regis_port_string += (i == ids.length - 1) ? rowData.registryPort
            : rowData.registryPort + ',';
    }
    bootbox.confirm({
        buttons: {
            confirm: {
                label: '确认',
                className: 'btn-primary btn-sm btn-round'
            },
            cancel: {
                label: '取消',
                className: 'btn-danger btn-sm btn-round'
            }
        },
        message: "<b>你确定要同步以下仓库吗?</b><br><font color=\"blue\"><b><i>" + regis_name_string + "</i></b></font>",
        callback: function (result) {
            if (result) {
                $.post(base + 'registry/sync/batch', {
                    regi_ids: regis_ids,
                    regi_names: regis_name_string,
                    regi_ipaddrs: regis_ipaddr_string,
                    regi_ports: regis_port_string
                }, function (response) {
                    if (response.success) {
                        showMessage(response.message, function () {
                            $(grid_selector).trigger("reloadGrid");
                        });
                    } else {
                        showMessage("<font color=\"red\">错误</font>：同步(<font color=\"blue\"><i>" + regis_name_string
                            + "</i></font>)仓库失败！<br><font color=\"green\">原因</font>：" + response.message);
                        $(grid_selector).trigger("reloadGrid");
                    }
                });
            } else {
            }
        },
        title: "同步仓库",
    });
}

/**
 * @author yangqinglin
 * @datetime 2015年9月25日 18:48
 * @description 批量删除仓库函数
 */
function batchRemoveRegistrys() {
    var ids = $(grid_selector).jqGrid("getGridParam",
        "selarrrow");
    var regis_ids = "";
    var regis_name_string = "";
    if (ids.length == 0) {
        showMessage("请点击选择批量删除的仓库!");
        return;
    }
    for (var i = 0; i < ids.length; i++) {
        var rowData = $(grid_selector).jqGrid("getRowData",
            ids[i]);
        regis_ids += (i == ids.length - 1 ) ? rowData.registryId
            : rowData.registryId + ',';
        regis_name_string += (i == ids.length - 1) ? rowData.registryName
            : rowData.registryName + ',';
    }
    bootbox.confirm({
        buttons: {
            confirm: {
                label: '确认',
                className: 'btn-primary btn-sm btn-round'
            },
            cancel: {
                label: '取消',
                className: 'btn-danger btn-sm btn-round'
            }
        },
        message: "<b>你确定要删除以下仓库吗?</b><br><font color=\"blue\"><b><i>" + regis_name_string + "</i></b></font>",
        callback: function (result) {
            if (result) {
                $.post(base + 'registry/remove/batch', {
                    regi_ids: regis_ids,
                    regi_names: regis_name_string
                }, function (response) {
                    if (response.success) {
                        showMessage(response.message, function () {
                            $(grid_selector).trigger("reloadGrid");
                        });
                    } else {
                        showMessage("<font color=\"red\">错误</font>：删除(<font color=\"blue\"><i>" + regis_name_string
                            + "</i></font>)仓库失败！<br><font color=\"green\">原因</font>：" + response.message);
                        $(grid_selector).trigger("reloadGrid");
                    }
                });
            } else {
            }
        },
        title: "删除仓库",
    });

}

/**
 * @description 删除仓库函数
 */
function deleteRegistry() {
    var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
    if (ids.length == 0) {
        showMessage("请先选择一条记录！");
    } else if (ids.length > 1) {
        showMessage("只能选择一条记录！");
    } else {
        var rowData = $(grid_selector).jqGrid("getRowData", ids);
        var registry_id = rowData.registryId;
        var registry_name = rowData.registryName;
        var id =rowData.id;

        var url = base + "registry/registryLbDelete";
        var data = {
        		id:id,
            registryIds: registry_id,
            registryName: registry_name
        };
        bootbox.dialog({
            message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除(' + registry_name + ')仓库吗?</div>',
            title: "提示",
            buttons: {
                main: {
                    label: "<i class='icon-info'></i><b>确定</b>",
                    className: "btn-sm btn-success btn-round",
                    callback: function () {
                         $("#spinner").css("display", "block");
                        $.post(url, data, function (response) {
                            if (response.success) {
                                showMessage(response.message, function () {
                                    $(grid_selector).trigger("reloadGrid");
                                });
                            } else {
                                showMessage("<font color=\"red\">错误</font>：删除(<font color=\"blue\"><i>" + registry_name
                                    + "</i></font>)仓库失败！<br><font color=\"green\">原因</font>：" + response.message);
                                $(grid_selector).trigger("reloadGrid");
                            }
                            $("#spinner").css("display", "none");        
                        });
                    }
                },
                cancel: {
                    label: "<i class='icon-info'></i> <b>取消</b>",
                    className: "btn-sm btn-danger btn-round",
                    callback: function () {
                    }
                }
            }
        });
    }
}

/**
 * @author yangqinlgin
 * @datetime 2015年9月6日 19:00
 * @description 同步仓库函数
 */
function syncRegistry(host_ip, server_port, regi_id) {
    var sync_url = base + 'registry/syncRegiImgInfo';

    sync_data = {
        registry_port: server_port,
        registry_ipaddr: host_ip,
        registry_id: regi_id
    };

    /*验证Docker仓库地址和端口的可达性*/
    $.post(sync_url, sync_data, function (response) {
        if (response.success) {
            showMessage(response.message);
            $(grid_selector).trigger("reloadGrid");
        } else {
            showMessage(response.message);
            $(grid_selector).trigger("reloadGrid");
        }
    });
}

/**
 * @author yangqinlgin
 * @datetime 2015年9月25日 19:00
 * @description 同时同步多个仓库函数，无输入参数，根据用户选择的确定
 * */
function syncMultiRegistry() {
    var sync_url = base + 'registry/syncRegiImgInfo';

    sync_data = {
        registry_port: server_port,
        registry_ipaddr: host_ip,
        registry_id: regi_id
    };

    /*验证Docker仓库地址和端口的可达性*/
    $.post(sync_url, sync_data, function (response) {
        if (response.success) {
            showMessage(response.message);
            $(grid_selector).trigger("reloadGrid");
        } else {
            showMessage(response.message);
            $(grid_selector).trigger("reloadGrid");
        }
    });
}

/**
 * 获取主机的ID和IP地址列表
 * */
function getClusterList() {
    /*首先清空元素下的所有节点*/
    //$('#registry_hostid').empty();
    $.ajax({
        type: 'get',
        url: base + 'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each(array, function (index, obj) {
                var hostid = obj.hostid;
                var hostip = decodeURIComponent(obj.hostip);
                $('#host_Ip').append('<option value="' + hostid + '">' + hostip + '</option>');
            });
        }
    });
}

/**
 * 获取主机的ID和IP地址列表(已有选项的情况下)
 * */
function getClusterListSelected(select_host_id, select_host_Id) {
    /*首先清空元素下的所有节点*/
    $('#registry_hostid').empty();
    $.ajax({
        type: 'get',
        url: base + 'registry/registryHosts',
        dataType: 'json',
        success: function (array) {
            $.each(array, function (index, obj) {
                var hostid = obj.hostId;
                var hostip = decodeURIComponent(obj.hostIp);
                $('#registry_hostid').append('<option value="' + hostid + '">' + hostip + '</option>');
            });
        }
    });
}

function insertRegistry() {
    var url = base + 'registry/createlb';
    var check="checked=\'checked\'";
    bootbox.dialog({
        title: "<b>添加仓库</b>",
        message: "<div class='well ' style='margin-top:1px;'>" +
        "<form class='form-horizontal' role='form' id='add_item_frm'>" +
        "<div class='form-group'>" +
        "<label class='col-sm-3'><div align='right'><b>仓库名称：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<input placeholder=\" 请填写仓库名称\" id=\"registry_name\" type='text' class=\"form-control\"  '/>" +
        "</div>" +
        "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>" +
        "</div>" +
        "<div class='form-group' style='margin-bottom: 5px' id='db_type'>" +
        "<label class='col-sm-3'><div align='right'><b>负载：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<label class='col-sm-3'><input type='radio' value='2' name='lb' "+check+"  onclick='typeClick(2)'>  F5 </label>"+
        "<label class='col-sm-4'><input type='radio' value='1' name='lb'   onclick='typeClick(1)'>  nginx </label>"+
        "</div>" +
        "</div>" +
        "<div class='form-group' id='f5Select'  style='margin-bottom: 5px' id='db_type'>"+
			"<label class='col-sm-3'><div align='right'><b>选择F5：</b></div></label>"+
        "<div class='col-sm-7'>" +
        "<select id='f5_Id'  class='form-control'  >"+
			"</select>"+
        "</div>"+
        "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>" +
        "</div>" +
        
        "<div class='form-group' id='nginxSelect' hidden = true style='margin-bottom: 5px' id='db_type'>"+
		"<label class='col-sm-3'><div align='right'><b>选择nginx：</b></div></label>"+
        "<div class='col-sm-7'>" +
        "<select id='nginx_Id'  class='form-control'  '>"+
	  	"</select>"+
        "</div>"+
        "<div class='col-sm-2'>" +
        "<select id='nginx_port'  class='form-control'  >"+
		"</select>"+
        "</div>"+
        "</div>" +
    
        "<div class='form-group'>" +
        "<label class='col-sm-3'><div align='right'><b>负载域名：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<input placeholder=\" 请填写负载域名\" id=\"load_domain\" type='text' class=\"form-control\"  '/>" +
        "</div>" +
        "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>" +
        "</div>" +
        "<div class='form-group'  id='f5Port'>" +
        "<label class='col-sm-3'><div align='right'><b>对外Ip端口：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<input placeholder=\" 请填写ip\" id=\"load_ip\" type='text' class=\"form-control\" '/>" +
        "</div>" +
        "<div class='col-sm-2'>" +
        "<input placeholder=\" 请输入端口号\" id=\"load_port\" type='number' class=\"form-control\" value='5000'/>" +
        "</div>"+
        "</div>" +
        "<div class='form-group'>" +
        "<label class='col-sm-3'><div align='right'><b>主机一：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<select id='host_Ip' name='registry_hostid' class='form-control' onchange=\" getStandByHost();\">" +
        "<option value='0'>请选择主机</option>" +
        "</select>" +
        "</div>" +
        "<div class='col-sm-2'>" +
        "<input placeholder=\" 请输入端口号\" id=\"host_port\" type='number' class=\"form-control\" value='5000'/>" +
        "</div>"+
        "</div>" +
        "<div class='form-group'>" +
        "<label class='col-sm-3'><div align='right'><b>主机二：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<select id='standHost_Id'  class='form-control' >" +
        "<option value='0'>请选择备份主机</option>" +
        "</select>" +
        "</div>" +
        "<div class='col-sm-2'>" +
        "<input placeholder=\" 请输入端口号\" id=\"standHost_port\" type='number' class=\"form-control\" value='5000'/>" +
        "</div>"+
        "</div>" +
        "<div class='form-group'>" +
        "<label class='col-sm-3'><div align='right'><b>仓库描述：</b></div></label>" +
        "<div class='col-sm-7'>" +
        "<textarea placeholder=\"请输入仓库的描述信息\"  id=\"registry_desc\" name=\"registry_desc\" onblur=\"validToolObj.length('#registry_desc',200)\" class=\"form-control\" rows=\"3\"></textarea>" +
        "</div>" +
        "</div>" +
        "</div>" +
        "</form>" +
        "</div><script>getClusterList();typeClick(2);</script>",
        buttons: {
            "success": {
                "label": "<i id=\"saveButt\" class=\"fa fa-floppy-o\"></i>&nbsp;<b>保存</b>",
                "className": "btn-success btn-sm btn-round",
                "callback": function () {
                    reg_name = $('#registry_name').val();
                    var reg_loadtype=$("input[name='lb']:checked").val();
                    f5_Id = $('#f5_Id').val();
                    var nginx_Id = $('#nginx_Id').val();
                    var nginx_port = $('#nginx_port').val();
                    
                    var load_domain = $('#load_domain').val();
                    var load_hostId = "";
                    var load_ip=$("#load_ip").val();
                    var load_port=$("#load_port").val();
                    
                    var host_port=$("#host_port").val();
                    var host_Id=$("#host_Ip").val();
                    
                    var standHost_Id=$("#standHost_Id").val();
                    var standHost_port=$("#standHost_port").val();
                    reg_desc = $('#registry_desc').val();
                    if(reg_loadtype==1){
                    	load_ip = $("#nginx_Id").find('option:selected').attr("compip");
                    	load_hostId = $("#nginx_Id").val();
                       }
                    if(reg_name==""||null==reg_name){
                   	 bootbox.alert("请输入仓库名称");
                   	 return false;
                    }
                    if((f5_Id==""||null==f5_Id)&&(nginx_Id==""||null==nginx_Id)){
                      	 bootbox.alert("请选择负载");
                      	 return false;
                       }
                    if(host_Id==""||null==host_Id){
                      	 bootbox.alert("请选择主机");
                      	 return false;
                       }
                    
                	data2={
                			hostID:host_Id,
	       					type:"registry"
	       			};
                	$("#spinner").css("display","block");
	       		  var url2 = base + 'host/getVersion';
	       			$.post(url2,data2,function(response){
	       			$("#spinner").css("display","none");
        			bootbox.dialog({
		        	    message:"<div class='well ' style='margin-top:1px;padding-bottom:1px;'>"+
						"<form class='form-horizontal' role='form' >"+ 
						"<div class='form-group'  >"+
						"<label class='col-sm-4'><div align='right'><b>docker当前版本：</b></div></label>"+
						 "<div class='col-sm-8'>"+
				         	"<input  type='text' value='"+response.dockerVersion+"' class=\"form-control\" readonly=‘readonly’/>"+
			        	    "</div>"+
						"</div>"+
		        	    "<div class='form-group' id='nginxIns' >"+
	  				       "<label class='col-sm-4'><div align='right'><b>docker安装：</b></div></label>"+
	  				        "<div class='col-sm-8'>"+
	  				      "<select id='dockerIns_select' name='dockerIns_select' class='form-control' >"+
	  					  "</select>"+
	  			        	"</div>"+
      			         "</div>"+
      			       "<div class='form-group'  >"+
						"<label class='col-sm-4'><div align='right'><b>registry当前版本：</b></div></label>"+
						 "<div class='col-sm-8'>"+
				         	"<input  type='text' value='"+response.registryVersion+"' class=\"form-control\" readonly=‘readonly’/>"+
			        	    "</div>"+
						"</div>"+
		        	    "<div class='form-group' id='nginxIns' >"+
	  				       "<label class='col-sm-4'><div align='right'><b>registry安装：</b></div></label>"+
	  				        "<div class='col-sm-8'>"+
	  				  	      "<select id='registryIns_select' name='registryIns_select' class='form-control' >"+
	  				           "</select>"+
	  			        	"</div>"+
     			         "</div>"+
	  			        "</form>"+
		    	      	"</div><script>getDocker();getRegistry();</script>",
		        	    title: "提示",
		        	    buttons: {
		        	    	"success" : {
		    					"label" : "<i class='icon-ok' id='saveButt'></i> <b>保存</b>",
		    					"className" : "btn-sm btn-danger btn-round",
		    					"callback" : function() {
		    						 var registryIns=$("#registryIns_select").val();
		    		                    var dockerIns=$("#dockerIns_select").val();
		    						 data = {
		    		                    		reg_name: reg_name,
		    		                    		reg_loadtype: reg_loadtype,
		    		                    		f5_Id: f5_Id,
		    		                    		nginx_Id: nginx_Id,
		    		                    		nginx_port:nginx_port,
		    		                    		load_domain:load_domain,
		    		                    		load_hostId:load_hostId,
		    		                    		load_ip:load_ip,
		    		                    		load_port:load_port,
		    		                    		host_Id:host_Id,
		    		                    		host_port:host_port,
		    		                    		standHost_Id:standHost_Id,
		    		                    		standHost_port:standHost_port,
		    		                    		reg_desc:reg_desc,
		    		                    		registryIns:registryIns,
		    		                    		dockerIns:dockerIns
		    		                    };
		    		                    $("#spinner").css("display", "block");
		    		                    $.post(url, data, function (response) {
		    		                    	showMessage(response.message, function () {
		    		                            $(grid_selector).trigger("reloadGrid");
		    		                       });
		    		                        $("#spinner").css("display", "none");
		    		                    });
		    					}
		        	    	},
        			     "cancel" : {
     					       "label" : "<i class='icon-info'></i> <b>取消</b>",
     					         "className" : "btn-sm btn-warning btn-round",
     					        "callback" : function() {}
     				    }
        		     }
        			});
	       			});
                    
                    
                    
                }
            },
            "cancel": {
                "label": "<i class=\"fa fa-times-circle\" ></i>&nbsp;<b>取消</b>",
                "className": "btn-sm btn-round btn-danger",
                "callback": function () {
                }
            }
        }
    });
   // $("#saveButt").parent().addClass("disabled");
}

function validateReachRegiHost() {
//	check_data = {
//		  registry_port:reg_port,
//		  registry_ipaddr:reg_ipaddr
//	};
//	
//	if(validToolObj.isNull('#registry_hostid',false) || validToolObj.isNull('#registry_port',false))
//		return;
//
//	var check_url = base+'registry/reachRegiHost',
//		/*获取选择主机ID的IP地址信息*/
    reg_ipaddr = $("#registry_hostid").find("option:selected").val();
//		reg_port = $('#registry_port').val(),
//		check_data = {
//			  registry_port:reg_port,
//			  registry_ipaddr:reg_ipaddr
//		};
//	/*验证Docker仓库地址和端口的可达性*/
//	if($('#registry_hostid').val()==0){
//		return;
//	}
//	$.post(check_url, check_data, function(response) {
//		if (response.success) {
//			$(".registry_port",$('#registry_port').parent()).remove();
//			$("#saveButt").parent().removeClass("disabled");
//		} else {
//			showMessage(response.message);
//			validToolObj.showTip('#registry_port','端口无效，请重新输入！');
//			$("#saveButt").parent().addClass("disabled");
//		}
//	});
    if (reg_ipaddr != 0) {
        $("#saveButt").parent().removeClass("disabled");
        checkRegistry();
    }
    else {
        $("#saveButt").parent().addClass("disabled");
    }
}

/**
 * @author yangqinglin
 * @datetime 2015年9月10日 13:06
 * @description 查看某个仓库中所有的镜像列表页面
 */
function queryRegistryImages(registryid, registryName) {
    window.location = base + "registry/images.html?registryid=" + registryid + "&registryName=" + registryName;
}

/**
 * @author yangqinglin
 * @datetime 2015年9月10日 17:12
 * @description 返回上一个页面
 */
function gobackpage() {
    history.go(-1);
}

/**
 * @author yangqinlgin
 * @datetime 2015年9月7日 17:51
 * @description 添加修改仓库部分的功能函数
 */
function editRegistry() {
    var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
    if (ids.length == 0) {
        showMessage("请先选择一条记录！");
    } else if (ids.length > 1) {
        showMessage("只能选择一条记录！");
    } else {
        var rowData = $(grid_selector).jqGrid("getRowData", ids);
        var id = rowData.id;
	var url =  base+'registry/registryLbDetails';
    data = {
    		id: id
    };
	$.post(url, data, function(response) {
    bootbox.dialog({
        title: "更新仓库",
        message:template('registryUpdate',response) ,
        buttons: {
        	  "success": {
                "label": "<i class=\"fa fa-floppy-o\" id='saveButt'></i>&nbsp;<b>保存</b>",
                "className": "btn-sm btn-round btn-success",
                "callback": function () {
                    reg_name = $('#registry_name').val();
                    reg_desc = $('#registry_desc').val();
                    var load_domain = $('#load_domain').val();
                    if(reg_name==""||null==reg_name){
                     	 bootbox.alert("请输入名称");
                     	 return false;
                      }
                    data = {
                    		id: id,
                    		reg_name: reg_name,
                    		reg_desc: reg_desc,
                    		load_domain:load_domain
                    };
                    url=base+'registry/editRegistryLb';;
                    $.post(url, data, function (response) {
                        showMessage(response.message, function () {
                            $(grid_selector).trigger("reloadGrid");
                        });
                    });
                }
            },
            "cancel": {
              "label": "<i class=\"fa fa-times-circle\" id='cancelBtn'></i>&nbsp;<b>取消</b>",
              "className": "btn-sm btn-round btn-danger",
              "callback": function () {
              }
          }
        }
    });
    getClusterList();
    });
    }

//        var registry_name = rowData.registryName;
//        var registry_port = rowData.registryPort;
//        var host_id = rowData.hostId;
//        var registry_hostip = rowData.hostIP;
//        var registry_desc = rowData.registryDesc;
//
//        var url;
//        if (null == registry_id) {
//            url = base + 'registry/update';
//        } else {
//            url = base + 'registry/update';
//        }
//        if (registry_desc == null) {
//            registry_desc = "";
//        }
//        bootbox.dialog({
//            title: "<b>编辑仓库</b>",
//            message: "<div class='well ' style='margin-top:1px;'>" +
//            "<form class='form-horizontal' role='form' id='add_item_frm'>" +
//            "<div class='form-group'>" +
//            "<label class='col-sm-3'><div align='right'><b>仓库名称：</b></div></label>" +
//            "<div class='col-sm-9'>" +
//            "<input id='registry_id' value='" + registry_id + "' type='hidden'/>" +
//            "<input id=\"registry_name\" type='text' class=\"form-control\" value='" + registry_name + "' onblur='checkRegistry();'/>" +
//            "</div>" +
//            "<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>" +
//            "</div>" +
//            "<div class='form-group'>" +
//            "<label class='col-sm-3'><div align='right'><b>主机IP：</b></div></label>" +
//            "<div class='col-sm-9'>" +
//            "<input id=\"registry_hostip\" readonly=\"readonly\" type='text' value='" + registry_hostip + "' class=\"form-control\" />" +
//            "<input id=\"host_id\" readonly=\"readonly\" type='hidden' value='" + host_id + "' class=\"form-control\" />" +
//            "</div>" +
//            "</div>" +
//            "<div class='form-group'>" +
//            "<label class='col-sm-3'><div align='right'><b>端口号：</b></div></label>" +
//            "<div class='col-sm-9'>" +
//            "<input id=\"registry_port\" readonly=\"readonly\" type='text' value='" + registry_port + "' class=\"form-control\" />" +
//            "</div>" +
//            "</div>" +
//            "<div class='form-group'>" +
//            "<label class='col-sm-3'><div align='right'><b>仓库描述：</b></div></label>" +
//            "<div class='col-sm-9'>" +
//            "<textarea id=\"registry_desc\" onblur=\"validToolObj.length('#registry_desc',200)\" class=\"form-control\" rows=\"3\">" + registry_desc + "</textarea>" +
//            "</div>" +
//            "</div>" +
//            "</div>" +
//            "</form>" +
//            "</div>",
//            buttons: {
//                "success": {
//                    "label": "<i class=\"fa fa-floppy-o\" id='saveButt'></i>&nbsp;<b>保存</b>",
//                    "className": "btn-sm btn-round btn-success",
//                    "callback": function () {
//                        reg_id = $('#registry_id').val();
//                        if ("undefined" == reg_id) {
//                            reg_id = null;
//                        }
//                        reg_name = $('#registry_name').val();
//                        reg_desc = $('#registry_desc').val();
//                        data = {
//                            registry_id: reg_id,
//                            registry_name: reg_name,
//                            registry_desc: reg_desc
//                        };
//                        $.post(url, data, function (response) {
//                            showMessage(response.message, function () {
//                                $(grid_selector).trigger("reloadGrid");
//                            });
//                        });
//                    }
//                },
//                "cancel": {
//                    "label": "<i class=\"fa fa-times-circle\"></i>&nbsp;<b>取消</b>",
//                    "className": "btn-sm btn-round btn-danger",
//                    "callback": function () {
//                    }
//                }
//            }
//        });
//        /*添加默认显示当前主机的IP地址信息*/
//        $('#registry_hostid').append('<option value=' + host_id + ' selected=\"selected\">' + registry_hostip + '</option>');
//    }

}

function checkRegistry() {
    if (validToolObj.isNull('#registry_name') || validToolObj.validName('#registry_name') )
        $("#saveButt").parent().attr("disabled", "disabled");
    else
        $("#saveButt").parent().removeAttr("disabled");
}


/**
 * @author yangqinlgin
 * @datetime 2015年9月10日 17:25
 * @description 添加在仓库下所有镜像列表(2015年9月11日)的内容
 */
var grid_regimg_selector = "#registry_image_list";
var page_regimg_selector = "#registry_image_page";
jQuery(function ($) {
    $(window).on('resize.jqGrid', function () {
        $(grid_regimg_selector).jqGrid('setGridWidth', $(".page-content").width());
        $(grid_regimg_selector).closest(".ui-jqgrid-bdiv").css({
            'overflow-x': 'hidden'
        });
    });
    var parent_column = $(grid_regimg_selector).closest('[class*="col-"]');

    $(document).on(
        'settings.ace.jqGrid',
        function (ev, event_name, collapsed) {
            if (event_name === 'sidebar_collapsed'
                || event_name === 'main_container_fixed') {
                setTimeout(function () {
                    $(grid_regimg_selector).jqGrid('setGridWidth',
                        parent_column.width());
                }, 0);
            }
        });
    var registryid = $("#registryid").val();
    jQuery(grid_regimg_selector)
        .jqGrid(
            {
                url: base + 'registry/listslaveimages?registry_id=' + registryid,
                datatype: "json",
                height: '100%',
                autowidth: true,
                colNames: ['镜像ID', '镜像UUID', '镜像名称', '镜像物理名称', '镜像版本', '镜像状态', '镜像大小',
                    '镜像描述', '应用ID', '所属物理系统名称', '镜像端口', '创建时间'],
                colModel: [
                    {name: 'imageId', index: 'imageId', width: 6, hidden: true},
                    {name: 'imageUuid', index: 'imageUuid', width: 10, align: 'left', hidden: true},
                    {name: 'tempName', index: 'tempName', width: 12, align: 'left'},
                    {name: 'imageName', index: 'imageName', width: 12, align: 'left'},
                    {name: 'imageTag', index: 'imageTag', width: 6, sortable: false, align: 'left'},
                    {
                        name: 'imageStatus', index: 'imageStatus', width: 6, align: 'left',
                        formatter: function (cellvalue, options,
                                             rowObject) {
                            switch (cellvalue) {
                                case 0:
                                    return '删除';
                                case 1:
                                    return '正常';
                                default:
                                    return '连接异常';
                            }
                        }
                    },
                    {name: 'imageSize', index: 'imageSize', width: 6, align: 'left', sortable: false, hidden: true},
                    {
                        name: 'imageDesc', index: 'imageDesc', width: 6, sortable: false, align: 'left',
                        formatter: function (cellvalue, options, rowObject) {
                            return stringTool.escape(rowObject.imageDesc);
                        }
                    },
                    {name: 'appId', index: 'appId', width: 6, align: 'left', hidden: true},
                    {name: 'systemName', index: 'systemName', width: 8, sortable: false, align: 'left'},
                    {name: 'imagePort', index: 'imagePort', width: 8, align: 'left', hidden: true},
                    {name: 'imageCreatetime', index: 'imageCreatetime', width: 10, align: 'left'}],
                viewrecords: true,
                rowNum: 10,
                rowList: [10, 20, 50, 100, 1000],
                pager: page_regimg_selector,
                altRows: true,
                sortname: 'tempName',
                sortname: 'imageName',
                sortname: 'imageStatus',
                sortname: 'imageCreatetime',
                sortorder: "desc",
				multiselect : true,
                jsonReader: {
                    root: "rows",
                    total: "total",
                    page: "page",
                    records: "records",
                    repeatitems: false
                },
                loadError: function (resp) {
                    if (resp.responseText.indexOf("会话超时") > 0) {
                        showMessage('会话超时，请重新登录！', function () {
                            document.location.href = '/login.html';
                        });
                    }
                },
                loadComplete: function () {
                    var table = this;
                    setTimeout(function () {
                        styleCheckbox(table);
                        updateActionIcons(table);
                        updatePagerIcons(table);
                        enableTooltips(table);
                    }, 0);
                }
            });
    $(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
    jQuery(grid_regimg_selector).jqGrid(// 分页栏按钮
        'navGrid', page_regimg_selector, { // navbar options
            edit: false,
            add: false,
            del: false,
            search: false,
            refresh: true,
            refreshstate: 'current',
            refreshicon: 'ace-icon fa fa-refresh',
            view: false
        }, {}, {}, {}, {}, {});
    function updateActionIcons(table) {
    }

    function updatePagerIcons(table) {
        var replacement = {
            'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
            'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
            'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
            'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
        };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
            .each(
                function () {
                    var icon = $(this);
                    var $class = $.trim(icon.attr('class').replace(
                        'ui-icon', ''));
                    if ($class in replacement)
                        icon.attr('class', 'ui-icon '
                            + replacement[$class]);
                });
    }

    function enableTooltips(table) {
        $('.navtable .ui-pg-button').tooltip({
            container: 'body'
        });
        $(table).find('.ui-pg-div').tooltip({
            container: 'body'
        });
    }

    function styleCheckbox(table) {
        $(table).find('input:checkbox').addClass('ace').wrap('<label />')
            .after('<span class="lbl align-top" />');
        $('.ui-jqgrid-labels th[id*="_cb"]:first-child').find(
            'input.cbox[type=checkbox]').addClass('ace').wrap('<label />')
            .after('<span class="lbl align-top" />');
    }
});

//查询条件 
function searchHost() {
    var registryName = $('#registryName').val();
    var LbIp = encodeURI($('#LbIp').val());
    jQuery(grid_selector).jqGrid('setGridParam', {url: base + 'registry/listWithIP?registryName=' + registryName + '&LbIp=' + LbIp}).trigger("reloadGrid");
}
function stopRegistry(registryId,timer) {
    var thtml = '<div class="alert alert-warning" style="margin:10px">' +
        '<span class="glyphicon glyphicon-info-sign"></span>停止Registry进程?</div>' +
        '<div align="center"><span class="badge badge-danger">' + timer + '</span></div>';
    if (timer > 1) {
        bootbox.dialog({
            message: thtml,
            title: "提示",
            buttons: {
                main: {
                    label: "<i class='icon-info'></i><b>确定</b>",
                    className: "btn-sm btn-success btn-round",
                    callback: function () {
                        stopRegistry(registryId,--timer);
                    }
                },
                cancel: {
                    label: "<i class='icon-info' ></i> <b>取消</b>",
                    className: "btn-sm btn-danger btn-round",
                    callback: function () {
                    }
                }
            }
        });
    } else {
        bootbox.dialog({
            message: thtml,
            title: "提示",
            buttons: {
                main: {
                    label: "<i class='icon-info'></i><b>确定</b>",
                    className: "btn-sm btn-success btn-round",
                    callback: function () {
                        var data = {registryId: registryId, opration: 'stop'};
                        var url = base + "registry/registryDaemon";
                        $("#spinner").css("display", "block");
                        $.get(url, data, function (response) {
                        	showMessage(response.message);
                            $("#spinner").css("display", "none");
                            $(grid_selector).trigger("reloadGrid");
                            $('#cancelBtn').trigger("click");
                        });
                    }
                },
                cancel: {
                    label: "<i class='icon-info' ></i> <b>取消</b>",
                    className: "btn-sm btn-danger btn-round",
                    callback: function () {
                    }
                }
            }
        });
    }
}
function startRegistry(registryId,id) {
    bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;开启Registry进程?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                    var data = {registryId: registryId, opration: 'start'};
                    var url = base + "registry/registryDaemon";
                    $("#spinner").css("display", "block");
                    $.get(url, data, function (response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                        $('#cancelBtn').trigger("click");
                    });
                }
            },
            cancel: {
                label: "<i class='icon-info' ></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}
function typeClick(type){
    var options=$("#f5_Id");
    if(type==1){
        $("#f5Select").hide();
        $("#nginxSelect").show();
        $("#f5Port").hide();
         options=$("#nginx_Id");
         nginxChange();

    }
    if(type==2){
        $("#f5Select").show();
        $("#nginxSelect").hide();
        $("#f5Port").show();
    }
    if(null==type){
    	type = '0';
    }
	$.ajax({
        type: 'get',
        url:base+'component/list?componentType='+type+'&nginxCategory=2',
        data:{
        },
        async:false,
        dataType: 'json',
        success: function (response) {
            options.empty();
            $.each (response.rows, function (index, obj){
                var cComponentId = obj.componentId;
                var componentName = obj.componentName;
                var option="<option value='"+cComponentId+"' compip='"+obj.componentIp+"'>"+componentName+"</option>";
                options.append(option);
			});
        }
	});

}
function getStandByHost(){
	$('#standHost_Id').empty();
	$('#standHost_Id').append('<option value="0">'+"请选择备份主机"+'</option>');
	$.ajax({
        type: 'get',
        url: base+'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var hostid = obj.hostid;
				var hostip = decodeURIComponent(obj.hostip);
				var name=$('#host_Ip').children('option:selected').val();
				if(name != hostid){
					$('#standHost_Id').append('<option value="'+hostid+'">'+hostip+'</option>');
				}
				
            });
        }
	});
}
function registryLbDetails(id){
	var url =  base+'registry/registryLbDetails';
    data = {
    		id: id
    };
	$.post(url, data, function(response) {
    bootbox.dialog({
        title: "查看仓库详细信息",
        message:template('registryLbDetails',response) ,
        buttons: {
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125' id='cancelBtn'></i> <b>返回</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function () {
                    $(grid_selector).trigger("reloadGrid");
                }
            }
        }
    });
	});
}
function nginxChange(){
	 var options=$("#nginx_port");
	 $.ajax({
	        type: 'get',
	        url:base+'port/getFreePort',
	        data:{
	        },
	        async:false,
	        dataType: 'json',
	        success: function (response) {
	            options.empty();
	            $.each (response.rows, function (index, obj){
	                var option="<option value='"+obj.id+"'>"+obj.port+"</option>";
	                options.append(option);
				});
	        }
		});
}
function changeRegistry(registryId,type,ip,port,id){
	 var res=[];
	 var url=base+'registry/changeRegistryLb';
	 getDocker();
	 getRegistry();
	 res.ip=ip;
	 res.port=port;
	   bootbox.dialog({
	        title: "更改仓库主机",
	        message:template('changeRegistry',res) ,
	        buttons: {
	        	"success": {
	                "label": "<i class=\"fa fa-floppy-o\" id='saveButt'></i>&nbsp;<b>保存</b>",
	                "className": "btn-sm btn-round btn-success",
	                "callback": function () {
	                	var change_id = $("#change_ip").val();
	                	var change_port = $("#change_port").val();
	                	var registryIns = $("#registryIns_select").val();
	                	var dockerIns = $("#dockerIns_select").val();
	                	if(change_id==0){
	                		 bootbox.alert("请选择更换的主机");
	                		return false;
	                	}
	                	data = {
	                			id:id,
	               			registryId: registryId,
	               			type:type,
	               			change_id:change_id,
	               			change_port:change_port,
	               			registryIns:registryIns,
	               			dockerIns:dockerIns
	               	    };
	                	$('#cancelBtn').trigger("click");
	                	$("#spinner").css("display", "block");
	                    $.post(url, data, function (response) {
	                        showMessage(response.message, function () {
	                            $(grid_selector).trigger("reloadGrid");
	                            $("#spinner").css("display", "none");
	                        });
	                    });
	                }
	            },
	            "cancel": {
	                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>返回</b>",
	                "className": "btn-sm btn-warning btn-round",
	                "callback": function () {
	                    $(grid_selector).trigger("reloadGrid");
	                }
	            }
	        }
	    });
	   getChangeIpList();
}
function getChangeIpList() {
    /*首先清空元素下的所有节点*/
    //$('#registry_hostid').empty();
    $.ajax({
        type: 'get',
        url: base + 'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each(array, function (index, obj) {
                var change_id = obj.hostid;
                var change_ip = decodeURIComponent(obj.hostip);
                $('#change_ip').append('<option value="' + change_id + '">' + change_ip + '</option>');
            });
        }
    });
}
function getRegistry(){
	
	$('#registryIns_select').empty();
    //
	$('#registryIns_select').append('<option value="0">不进行安装</option>');
	$.get(base+'hostComponent/hostComponentList?type=registry&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('#registryIns_select').append('<option value="'+id+'">'+version+'</option>');
        });
	},'json');
}
function getDocker(){
	
	$('#dockerIns_select').empty();
    //
	$('#dockerIns_select').append('<option value="0">不进行安装</option>');
	$.get(base+'hostComponent/hostComponentList?type=docker&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('#dockerIns_select').append('<option value="'+id+'">'+version+'</option>');
        });
	},'json');
}