var grid_selector = "#image_list";
var page_selector = "#image_page";
var wizardContent =null;
var cancelUpload = false;
var socket,socket2;
jQuery(function($) {
	wizardContent= $('#wizard_content_div').html();
	$("script[src='"+base+"js/message.js']").remove();
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
				
		url : base+'image/list?imageType=0',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['','镜像ID','镜像名称','镜像物理名称','版本','镜像类型','状态','所在仓库','创建时间','操作','仓库ID'],
		colModel : [ 
		    {name : 'imageId',index : 'imageId',width:1,hidden:true},
			{name : 'imageUuid',index : 'imageUuid',width : 9,hidden:true},
			{name : 'tempName',index : 'tempName',width :10},
			{name : 'imageName',index : 'imageName',sortable : false,width : 22},
			{name : 'imageTag',index : 'imageTag',sortable : false,width : 5},
			{name : 'imageType',index : 'imageType',width : 13,hidden:true,
				formatter:function(cell,opt,obj){
					if(0 == cell){
						return "<b>中间件镜像</b>";
					}else if(1 == cell){
						return "<b>应用镜像</b>";
					}else if(2 == cell){
						return "<b>NGINX基础镜像</b>";
					}else if(3 == cell){
						return "<b>基础镜像</b>";
					}else{
						return "<b>未知类型</b>";
					}
				}
			},
			{name : 'imageStatus',index : 'imageStatus',width : 5,
				formatter:function(cell,opt,obj){
					switch(obj.imageStatus){
					case 0:	return "已删除";
					case 1: return "已发布";
					case 2: return "已制作";
					default:return "未知态";
					}
			}},
			{name : 'registryName',index : 'registryName',width : 8},
			{name : 'imageCreatetime',index : 'imageCreatetime',width : 12},
			{
		    	name : '',
				index : '',
				width : 100,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					return "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"selectContainer('"+rowObject.imageId+"','"+rowObject.imageType+"')\">"
					+"<i class=\"ace-icon fa fa-search bigger-125\"></i>"
					+"<b>容器列表</b></button> &nbsp;";
				}
		    },
		    {name : 'registryId',index : 'registryId',width:1,hidden:true}
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'tempName',
		sortname: 'imageStatus',
		sortname: 'registryName',
		sortname: 'imageCreatetime',
		sortorder: "desc",
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {}
	
	current =0;
	
	//批量删除镜像函数
	$('#batch_delete_menu').on('click', function(){
		var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
		var message = "";
		var idList="";
		if(ids.length == 0){
			showMessage("请先选择需要删除的镜像!");
			return;
		}
		for(var i=0; i<ids.length; i++){
			var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
			idList += i == ids.length-1 ? rowData.imageId:rowData.imageId+',';
			message+= rowData.tempName+":"+rowData.imageTag+"<br/>";
		}
		confirm("你确定要删除以下镜像?<br>"+message, function() {
			$.post(base+'image/remove/batch',{ids:idList},function(response){
				showMessage(response.message,function(){
					$(grid_selector).trigger("reloadGrid");
				});
			});
		});
	});
});
//发布镜像行数
function pushImage(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择需要发布的镜像！");
	}else if(ids.length >1){
		showMessage("一次只能发布一个镜像！",function(){
			$(grid_selector).trigger("reloadGrid");
		});
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.imageStatus =="已制作"){
			var imageId =rowData.imageId;
			var imageName = rowData.imageName;
			var imageTag = rowData.imageTag;
			var registryId = rowData.registryId;
			data= {
					imageId:imageId,
					imageTag:imageTag,
					imageName:imageName,
					registryId:registryId
			};
			$("#push_image_loading").show();
			$.post(base+'image/push',data,function(resp){
				showMessage(resp.message);
				$(grid_selector).trigger("reloadGrid");
				$("#push_image_loading").hide();
			});
		}else{
			showMessage("该镜像不满足发布条件！请选择其他镜像",function(){
				$(grid_selector).trigger("reloadGrid");
			});
		}
	}
}
//删除单个镜像
function removeImage(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.imageId;
		var type = rowData.imageType;
		
		if("APP" == type){
			showMessage("应用镜像拒绝删除！");
			return;
		}else{
			bootbox.dialog({
		        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除此镜像吗?</div>',
		        title: "提示",
		        buttons: {
		            main: {
		                label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success btn-round",
		                callback: function () {
		                	var url = base+"image/remove/"+id;
		    				var data = {};
		    				$.post(url,data,function(response){
		    					showMessage(response.message);
		    					if(response.success){
		    						$(grid_selector).trigger("reloadGrid");
		    					}
		    				});
		                }
		            },
		            cancel: {
		                label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger btn-round",
		                callback: function () {
		                }
		            }
		        }
		    });
		}
	}
	
}

/*导出镜像*/
function ExportImage(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var imageId =rowData.imageId;
		var imageTempName = rowData.imageName;
		var i = imageTempName.lastIndexOf('/');
		var imageName = "";
		if(i>0){
			imageName = imageTempName.substring(i+1);
		}else{
			imageName = imageTempName;
		}
		$("#export_image").addClass("disabled");
		var imageTag = rowData.imageTag; //版本信息
		$.ajax({
	        type: 'get',
	        url: base+'image/saveIamge',
	        data: {
	        	imageId : imageId,
	        	imageName:imageName
	        	
	        },
	        dataType: 'json',
	        success: function (response) {
	        	if(response.isOk){
	        		scpImage(imageId,imageName,imageTag,response.fullpath);
	        	}else{
	        		showMessage(response.message);
	        	}
	        	
			}
		});
	}
}

function scpImage(imageId,imageName,imageTag,fullpath){
	$.ajax({
        type: 'get',
        url: base+'image/scpImage',
        data: {
        	imageId:imageId,
        	imageName:imageName,
        	imageTag:imageTag,
        	remotePath:fullpath
        },
        dataType: 'json',
        success: function (response) {
        	$("#export_image").removeClass("disabled");
        	if(!response.success){
        		showMessage(response.message);
        	}else{
        		location.href=base+'image/downloadImage?imageId='+imageId+'&imageName='+imageName+'&imageTag='+imageTag;
        	}
		}
	});
}


//取消按钮点击时触发的函数
function cancelImageCreate(){
	$('#modal-wizard').modal("hide");
	disconnect();
	current =0;
}
//窗口显示时调用的函数
function showCreateImageModal(type){
	connect();
	$('#modal-wizard').empty();
	$('#modal-wizard').html(wizardContent);
	$('#modal-wizard').modal('show');
	if(type==3){
		$("#baseImage_radio").attr("checked","checked");
	}else if(type==2){
		$("#nginxImage_radio").attr("checked","checked");
	}else{
		$("#appImage_radio").attr("checked","checked");
	}
	$.post(base+'registry/listWithIP?page=1&rows=65536',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var componentId = obj.componentId;
			var comp = id+":"+componentId;
			var registryname = decodeURIComponent(obj.registryName);
			$('#registry_select').append('<option value="'+comp+'">'+registryname+'</option>');
		});
	},'json');
	return;
	showMessage("点击确定提交镜像发布任务！",function(){
			var imageId = $('#image_id').val();
			var imageName = $('#templatename').val();
			var imageTag = $('#templateversion').val();
			var imagePort = $('#imagePort').val();
			var comp = $("#registry_select").val();
			var ids = comp.split(":");
			var registryId = ids[0];
			var componentId=ids[1];
			data= {
					imageId:imageId,
					imageTag:imageTag,
					imageName:imageName,
					componentId:componentId,
					registryId:registryId,
					imagePort:imagePort
			};
			$.post(base+'image/push',data,function(resp){
			});
			$('#modal-wizard').modal('hide');
			successNotice("发布镜像","制作镜像任务已经提交！正在制作中...");
			$("#push_image_loading").show();
			setTimeout(function(){
				$(grid_selector).trigger("reloadGrid");
    		},3000);
		});
	$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
}
//全局的websocket变量
var paragraph = 4*1000*1024;//64*1024
var file;
var startSize,endSize = 0;
var i = 0; j = 0;
var count =0;

$(function () {
    connect();
});

$(window).unload(function() {
	disconnect();
});

function connect() {
    if ('WebSocket' in window) {
        var host = window.location.host+base+"/";
        if(window.location.protocol=='http:'){
        	socket = new WebSocket('ws://' + host + '/messagingService');
        }else if(window.location.protocol=='https:'){
        	socket = new WebSocket('wss://' + host + '/messagingService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        socket.onopen = function () {};
        socket.onclose = function () {};
        socket.onmessage = function (event) {
            var obj = JSON.parse(event.data);
            message = JSON.parse(obj.content);
            if(obj.messageType == 'ws_sticky'){
            	if(message.title == "制作镜像"){
            		if(message.success){
            			$('#imagemessage').text('镜像制作成功！');
            			current = 2;
            			$(grid_selector).trigger("reloadGrid");
            		}else{
            			$('#imagemessage').text('镜像制作失败！');
            			current = 0;
            		}
            		showNotice(message);
            	}else if(message.title == "镜像发布"){
            		current = 0;
            		showNotice(message);
            		$("#push_image_loading").hide();
        			$(grid_selector).trigger("reloadGrid");
            	}else if(message.title == "IMAGEID" && message.success){
            		$('#image_id').val(message.message);
            	}
            }
        };
        if(window.location.protocol=='http:'){
        	socket2 = new WebSocket('ws://' + host + '/uploadBasicImageService');
        }else if(window.location.protocol=='https:'){
        	socket2 = new WebSocket('wss://' + host + '/uploadBasicImageService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        
        socket2.onopen = function () {};
        socket2.onclose = function () {};
        socket2.onerror = function(e) {};
        socket2.onmessage = function (event) {
            var obj = JSON.parse(event.data);
            if(obj.messageType =="ws_up_file"){
        	    sendArraybuffer(obj);
        	}
        };
    } else {
        console.log('Websocket not supported');
    }
}

//对发送进行封装，添加callback
this.send = function (message, callback) {
    this.waitForConnection(function () {
        socket2.send(message);
        if (typeof callback !== 'undefined') {
          callback();
        }
    }, 1000, 3);
}

// 等待连接，
this.waitForConnection = function (callback, interval, count) {
    if (socket2.readyState === 1) {
        callback();
    } else if (count === 0){
		showMessage("等待连接服务超时，请重新登陆！");
    } else {
        var that = this;
        // optional: implement backoff for interval here
        setTimeout(function () {
            that.waitForConnection(callback, interval, count--);
        }, interval);
    }
}

function disconnect() {
	socket.close();
    socket2.close();
    console.log("Disconnected");
}

//发送文件开始消息
function sendfileStart(name,componentId){
	 send(JSON.stringify({
	        'fileStartName': name,'componentId':componentId
	  }));
}
//发送取消上传文件的消息
function sendCancelMessage(file){
	send(JSON.stringify({
        'cancel': 'cancel'
	}));
}
//发送文件上传消息
function sendfileEnd(){
	 send(JSON.stringify({
	        'sendover': 'sendover'
	  }));
}
//显示通知函数
function showNotice(notice){
	try{
		if(notice.success){
			showMessage(notice.message);
		}else{
			showMessage(notice.message);
		}
	}catch (e) {
	}
}
//发送数据包函数
function sendArraybuffer(obj){
    if(obj.content == "OK"){
	    if(endSize < file.size){
	    	var blob;
	        startSize = endSize;
	        endSize += paragraph ;
	        if (file.webkitSlice) { 
	              blob = file.webkitSlice(startSize, endSize);
	        } else if (file.mozSlice) {
	              blob = file.mozSlice(startSize, endSize);
	        }
	        else{
	        	 blob = file.slice(startSize, endSize);
	        }
	        var reader = new FileReader();
	        reader.readAsArrayBuffer(blob);
	        reader.onload = function loaded(evt) {
	            var ArrayBuffer = evt.target.result;
	            i++;
	            var isok = (i / count) *100;
	            $("#showId").text("已经上传"+isok.toFixed(2) +"%");
	            $("#progressid").width((isok.toFixed(2)*0.7) +"%");
	            if(cancelUpload){
	            	 $("#showId").text("上传文件已取消");
	            	 startSize=0;
	            	 endSize=0;
	            	 i=0;
	            	 $("#progressid").width("0.00%");
	            }else{
	            	socket2.send(ArrayBuffer);
	            }
            };
	    } else{
            startSize = endSize = 0;
            i = 0;
            $("#showId").text("上传完成！");
            $("#progressid").width("70%");
            $(".btn-next").prop("disabled",false);
            sendfileEnd();
            $("#showId").text("上传完成！正在转存中，请稍等...");
	    }
    } else if(obj.content == "TRUE"){
    	$("#progressid").width("100%");
	    $("#showId").text("镜像文件上传并转存完成！");
	    current = 1;
//	    $('#upload_image_btn').addClass("hide");
	    $("#make_image_btn").removeAttr("disabled");
    } else if(obj.content == "FALSE"){
	    $("#showId").text("转存失败！");
    } else if(obj.content == "NOHOST"){
	    $("#showId").text("仓库主机在数据库中不存在！");
    } else if(obj.content == "DBERROR"){
	    $("#showId").text("镜像数据保存异常！");
    } else if(obj.content == "CONNERROR"){
	    $("#showId").text("仓库主机连接异常！");
    } else if(obj.content == "ISEXIT"){
	    $("#showId").text("该文件已经存在！");
    } else if(obj.content == "CANCEL"){
    	console.log(obj.content);
    }else{
    	console.log(obj.content);
    }
}

//function UpladFile() {
//    var fileObj = document.getElementById("image_file").files[0]; // 获取文件对象
//    var FileController = '/file/uploadFile';                    // 接收上传文件的后台地址 
//    // FormData 对象
//    var form = new FormData();
//    form.append("componentId", $("#registry_select").val().split(":")[1]);  
//    form.append('file', fileObj); 
//    form.append('fileType', 'baseImage');// 上传文件类型
//    // XMLHttpRequest 对象
//    var xhr = new XMLHttpRequest();
//    xhr.upload.addEventListener('progress', progressFunction, false);
//    xhr.upload.addEventListener('load', onLoadHandler, false);
//    xhr.upload.addEventListener('error', onErrorHandler, false);
//    xhr.open('POST', FileController, true);
//    xhr.onreadystatechange = onReadyStateHandler;
//    $("#progressBar").attr("hidden",false);
//    xhr.send(form);
//}
//var onLoadHandler = function() {
//	$("#make_image_btn").removeAttr("disabled");
//	showMessage("上传完成!");
//};
//
//var onErrorHandler = function() {
//	var percentageDiv = document.getElementById("percentage");
//	percentageDiv.innerHTML = "";
//	showMessage("上传失败!");
//};
//var onReadyStateHandler = function(event) {
//  if( event.target.readyState == 4 && event.target.status == 200){
//  }
//};
//function progressFunction(evt) {
//    var progressBar = document.getElementById("progressBar");
//    var percentageDiv = document.getElementById("percentage");
//    if (evt.lengthComputable) {
//        progressBar.max = evt.total;
//        progressBar.value = evt.loaded;
//        percentageDiv.innerHTML = Math.round(evt.loaded / evt.total * 100) + "%";
//        if(evt.loaded==evt.total){
//        	setTimeout('$("#progressBar").attr("hidden",true)', 2000);
//        }
//    }
//
//} 
//上传文件函数
function uploadImageFile(){
    var label=$('#upload-image_btn-text').text();
    if("取消"== label){
    	file = document.getElementById("image_file").files[0];
    	sendCancelMessage(file.name);
    	$('#upload-image_btn-text').text("上传");
    	cancelUpload = true;
    	return;
    }
    cancelUpload = false;
    if($("#registry_select").val()!=null)
	{
        file = document.getElementById("image_file").files[0];
        if(undefined == file || null == file){
        	showMessage("请选择需要上传的文件");
        	return;
        }
        var filename = file.name.split(".");
        if(!validate(filename[0])){
        	return;
        }
        if("zip" != filename[filename.length-1]&&"tar" != filename[filename.length-1]){
        	if("tar.gz" != (filename[filename.length-2]+"."+filename[filename.length-1])){
        		showMessage("只能上传.zip文件或者tar.gz文件！");
	        	return;
        	}
        }
        if(file!=null) {
        	$("#uploadImage").submit();
        	count = parseInt(file.size/paragraph)+1;
        	$("#pid").show();
        	$("#showId").text("开始上传！");
        	sendfileStart(file.name,$("#registry_select").val().split(":")[1]);
        	$('#upload-image_btn-text').text("取消");
        }else{
        	showMessage("请先选择需要上传的文件！");
    	}
	}
}
//制作镜像函数
function makeImageFun(){
	$("#make_image_btn").attr("disabled","disabled");
	if(checkImageName_valid()&&checkImageVersion_valid()&&checkImageTempName_valid()){
	}else{
		return;
	}
//	if(current<1){
//		showMessage("请先上传镜像文件！");
//		return;
//	}
	file = $('#image_file').val().split('\\');
	filename = file[file.length-1];
	var imageName = $('#templatename').val();
	var tempName = $('#tempName').val();
	var imageTag = $('#templateversion').val();
	var comp = $("#registry_select").val();
	var imagePort = $("#imagePort").val();
	var ids = comp.split(":");
	var registryId = ids[0];
	var componentId=ids[1];
	var imageId = $('#image_id').val();
	
	var imageType = $("input[name=image_type]:checked").val();
	$("#push_image_loading").show();
	disconnect();
	$("#progressid").width("0%");
	$("#pid").hide();
	if(validate(templatename)&&validate(templateversion)){
		$('#imagestatus').show();
		data= {
        	imageTag:imageTag,
        	imageName:imageName,
        	tempName:tempName,
        	componentId:componentId,
        	registryId:registryId,
        	fileName:filename,
        	imageId:imageId,
        	imagePort:imagePort,
        	imageType:imageType
        };
		$.post(base+'image/make',data,function(response){
			showMessage(response.message);
			$("#push_image_loading").hide();
			$('#imagemessage').removeClass("alert-warning");
			$('#imagemessage').addClass("alert-success");
			$(grid_selector).trigger("reloadGrid");
		});
    }
//	showMessage("制作镜像任务已经提交！正在制作中...");
	$('#modal-wizard').modal('hide');
}
//名称校验函数
function validate(filename){
	var reg = new RegExp("[u4e00-u9fa5]");
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）&mdash;—|{}【】‘；：”“'。，、？]");
	if(!reg.test(filename) && !pattern.test(filename)){
		bootbox.alert("上传的文件名不能包含汉字和特殊字符");
		return false;
	}else{
		return true;
	}
}
$(document).ready(function(){
	if (window.File && window.FileReader && window.FileList && window.Blob){
	}else {
	    bootbox.alert('您的浏览器不支持html5 Web socket 上传功能.');
	}
});
function imageTypeChange(imageType){
	var url = base+'image/list?imageType='+imageType;
	var options=$("#registryName_"+imageType);
	options.empty();
	options.append('<option value="">请选择</option>');
	$.ajax({
		url:base+'registry/listWithIP?page=1&rows=9999',
		success:function(data){
			$.each(data.rows,function(i,value){
				var registryName=value.registryName;
				var registryId = value.id;
				var option="<option value='"+registryId+"'>"+registryName+"</option>";
				options.append(option);
			});
		}
	});
	jQuery(grid_selector).jqGrid('setGridParam',{url:url}).trigger("reloadGrid");
}
function checkImageName_valid(){
//	validToolObj.isNull('#templatename') || validToolObj.validByRegExp(/^[a-zA-Z0-9_-]+$/,'#templatename','格式不正确，只能输入字母、数字、中划线、下划线！') || validToolObj.realTimeValid(base + 'image/checkName',undefined,'#templatename');
	if(validToolObj.isNull('#templatename')){
		return false;
	}else{
		return true;
	}
}
function checkImageVersion_valid(){
	var templateversion=$('#templateversion').val(),isOk = false,errorInfo = '版本号不能为空',reg = /^[A-Za-z0-9\.]+$/;
	if(validToolObj.isNull('#templateversion')){
		isOk = true;
		return false;
    }else if(!reg.test(templateversion)){
    	errorInfo = '格式不正确，只能输入字母、数字、符号!';
    	isOk = true;
    	return false;
    }
	if(isOk){
		validToolObj.showTip('#templateversion',errorInfo);
	}else{
		validToolObj.removeTip('#templateversion');
    }
	return true;
}
function checkImageTempName_valid(){
	if(validToolObj.isNull('#tempName') || validToolObj.validName('#tempName') || validToolObj.realTimeValid(base + 'image/checkName',undefined,'#tempName')){
		return false
	}else{
		return true;
	}
}
/**
 * 是否为port
 */
//function isPort(str){
//	if(!isNull(str)&&isInteger(str)&&str>0&&str<65536){
//		return true;
//    }else{
//    	return false;
//    }
//}
function checkImagePort_valid(){
	if(!isNull($("#imagePort").val())&&$("#imagePort").val()!=''){
		validToolObj.isPortByMany('#imagePort');
	}
//	validToolObj.isNull('#imagePort',false) || validToolObj.isPortByMany('#imagePort');
}

function selectContainer(id,imageType){
	location.href=base+"image/conlist?imageId="+id+"&imageType="+imageType;	
}

//查询条件
function searchHost(imageType){
	var imageName = $('#imageName_'+imageType).val();
	var registryId = $('#registryName_'+imageType).val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'image/list?imageType='+imageType+'&registryId='+registryId+'&imageName='+imageName}).trigger("reloadGrid");
}

$().ready(function(){
	var options=$("#registryName_0");
	options.empty();
	options.append('<option value="">请选择</option>');
	$.ajax({
		url:base+'registry/listWithIP?page=1&rows=9999',
		success:function(data){
			$.each(data.rows,function(i,value){
				var registryName=value.registryName;
				var registryId = value.id;
				var option="<option value='"+registryId+"'>"+registryName+"</option>";
				options.append(option);
			});
		}
	});
});


function SwitchImage(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var imageTempName = "";
	var imageId = "";
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
		return;
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
		return;
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		imageTempName = rowData.tempName;
		imageId = rowData.imageId;
	}
	bootbox.dialog({
        title: "<b>切换镜像</b>",
        message:template('changeImage') ,
        buttons: {
            "success": { "label": "<i id=\"saveButt\" class=\"fa fa-floppy-o\"></i>&nbsp;<b>提交</b>",
                "className": "btn-success btn-sm btn-round",
                "callback": function () {
                	var baseImageId = $("#baseImageId").val();
                	var switchSystems =''; 
                	$('input[name="switchSystem"]:checked').each(function(){
                		switchSystems += $(this).attr("id")+"===";
                	});
                	if(baseImageId==null){
                		bootbox.alert('你还没有选择目标镜像！');
                		return;
                	}
                	if(switchSystems.length==0){
                		bootbox.alert('你还没有选择任何物理系统！');
                		return;
                	}
                	var data= {
                			baseImageId:baseImageId,
                			switchSystems:switchSystems.toString()
                	};
                	$("#switchImageId").val(baseImageId);
                	$.post(base+'system/switchImage',data,function(response){
					});
                }
            },
			     "cancel" : {
				       "label" : "<i class='icon-info'></i> <b>取消</b>",
				         "className" : "btn-sm btn-warning btn-round",
				        "callback" : function() {}
			    }
        }
    });
	$("#oldImageName").val(imageTempName);
	var envId =getShuju();
	//镜像列表
	this.addImages(imageId);
	this.getImageHjInfo(envId);
                
}
//id
function getImageHjInfo(id){
	var url =base+'system/searchRunningByEnv?envId='+id;
	var options = $("#"+id);
	options.empty();
	var str = "";
	var groupStart  = "<div class='form-group'  >";
	var groupEnd   ="</div>";
	$.ajax({
        type: 'get',
        async:false,
        url: url,
        dataType: 'json',
        success: function (response) {
        	$.each(response,function(i,value){
        		var checkboxInfo=" <div class='col-sm-6'><div class='radio-inline' style='padding-top: 0px;width: 100%;float: left;'> <label>"
                    +"<input type='checkbox' name='switchSystem' id='"+value.systemId+"'/>" +value.systemName
                    +"</label></div> </div>";
        		if(i%2==0){
        			if(i==0){
        				str +=  groupStart+ checkboxInfo;
        			}else{
        				str +=  groupEnd +groupStart+ checkboxInfo;
        			}
        		}else{
        			str += checkboxInfo;
        		}
        	});
        	if(response.rows&&response.rows.length>0){
        		str +=  groupEnd;
        	}
        	options.append(str);
     }});
	
}
function getShuju(){
	var myTabSj = $("#myTabSj");
	var myTabSjContent = $("#myTabSjContent");
	myTabSj.empty();
	myTabSjContent.empty();
	var url  = base + 'environment/searchByEnv';
	var myTabSjInfo ="";
	var myTabSjContentInfo ="";
	var firstId ="";
	$.ajax({
        type: 'get',
        async:false,
        url: url,
        dataType: 'json',
        success: function (response) {
		 $.each(response,function(i,value){
				if(i==0){
					myTabSjInfo +="<li class='active'><a href='#"+value.eId+"' onclick='getImageHjInfo(\""+value.eId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
					 myTabSjContentInfo +="<div class='tab-pane fade in active' id='"+value.eId+"'></div>";
					 firstId = value.eId;
				}else{
					myTabSjInfo +="<li ><a href='#"+value.eId+"' onclick='getImageHjInfo(\""+value.eId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
					 myTabSjContentInfo +="<div class='tab-pane fade ' id='"+value.eId+"'></div>";
				}
		           
			});
		 myTabSj.append(myTabSjInfo);
		 myTabSjContent.append(myTabSjContentInfo);
     }});
	 return firstId;
}

function addImages(imageId){
	//加载镜像的列表
	$.get(base+'image/list?page=1&rows=65536&imageType=0',null,function(response){
		$.each (response.rows, function (index, obj){
			var tempName = obj.tempName;
			var imageStatus = obj.imageStatus;
//			if(imageStatus==1){
			if(imageStatus==1&&obj.imageId!=imageId){
				if(baseImageId==obj.imageId){
					$('#baseImageId').append('<option value="'+obj.imageId+'" selected="selected">'+tempName+'</option>');
				}else{
					$('#baseImageId').append('<option value="'+obj.imageId+'">'+tempName+'</option>');
				}
			}
	    });
	},'json');
}