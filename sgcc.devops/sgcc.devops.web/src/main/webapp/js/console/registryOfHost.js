/**
 * @author yueyong
 * @datetime 2016年3月15日 14:01
 * @description 仓库主机列表
 */
var grid_reghost_selector = "#registry_host_list";
var page_reghost_selector = "#registry_host_page";
jQuery(function($) {
	var url = base+'registry/selectLb?id='+$("#lbId").val();
	$.get(url,function(response){
		var registryName = response.registryName;
		$("#registry_name").html("仓库名称："+registryName);
	});
	
	$(window).on('resize.jqGrid', function() {
		$(grid_reghost_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_reghost_selector).closest(".ui-jqgrid-bdiv").css({
			'overflow-x' : 'hidden'
		});
	});
	var parent_column = $(grid_reghost_selector).closest('[class*="col-"]');
	
	$(document).on(
			'settings.ace.jqGrid',
			function(ev, event_name, collapsed) {
				if (event_name === 'sidebar_collapsed'
						|| event_name === 'main_container_fixed') {
					setTimeout(function() {
						$(grid_reghost_selector).jqGrid('setGridWidth',
								parent_column.width());
					}, 0);
				}
			});
		var lbId = $("#lbId").val();
		jQuery(grid_reghost_selector)
			.jqGrid(
					{
						url : base + 'registry/listHosts?lbId='+lbId,
						datatype : "json",
						height : '100%',
						autowidth : true,
						colNames : [ '仓库主机表ID', '主机id',  '主机IP','域名','端口','仓库状态', '创建时间','创建人id','创建人' ],
						colModel : [
								{name : 'registryId',index : 'registryId',width : 6,hidden : true},
								{name : 'hostId',index : 'hostId',width : 10,align:'left',hidden : true},
								{name : 'hostIP',index : 'hostIP',width : 12,align:'left'},
								{name : 'registryDns',index : 'registryDns',width : 12,align:'left'},
								{name : 'registryPort',index : 'registryPort',width : 6,sortable : false,align:'left'},
								{name : 'registryStatus',index : 'registryStatus',width : 6,align:'left',
									formatter : function(cellvalue, options,
											rowObject) {
										switch (cellvalue) {
										case 0:
											return '删除';
										case 1:
											return '未安装';
										case 2:
											return '正常';
										case 3:
											return '停止';
										default:
											return '未知';
										}
									}
								},
								{name : 'registryCreatetime',index : 'registryCreatetime',width : 10,align:'left'},
								{name : 'registryCreator',index : 'registryCreator',width : 8, hidden:true},
								{name : 'creatorName',index : 'creatorName',width : 4,align:'left'}],
						viewrecords : true,
						rowNum : 10,
						rowList : [ 10, 20, 50, 100, 1000 ],
						pager : page_reghost_selector,
						altRows : true,
						sortname: 'hostIP',
						sortname: 'registryDns',
						sortname: 'registryStatus',
						sortname: 'registryCreatetime',
						sortname: 'creatorName',
						sortorder: "desc",
						multiselect : true,
						jsonReader : {
							root : "rows",
							total : "total",
							page : "page",
							records : "records",
							repeatitems : false
						},
						loadError:function(resp){
							if(resp.responseText.indexOf("会话超时") > 0 ){
								showMessage('会话超时，请重新登录！',function(){
									document.location.href='/login.html';
								});
							}
						},
						loadComplete : function() {
							var table = this;
							setTimeout(function() {
								styleCheckbox(table);
								updateActionIcons(table);
								updatePagerIcons(table);
								enableTooltips(table);
							}, 0);
						}
					});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_reghost_selector).jqGrid(// 分页栏按钮
	'navGrid', page_reghost_selector, { // navbar options
		edit : false,
		add : false,
		del : false,
		search : false,
		refresh : true,
		refreshstate : 'current',
		refreshicon : 'ace-icon fa fa-refresh',
		view : false
	}, {}, {}, {}, {}, {});
	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
				.each(
						function() {
							var icon = $(this);
							var $class = $.trim(icon.attr('class').replace(
									'ui-icon', ''));
							if ($class in replacement)
								icon.attr('class', 'ui-icon '
										+ replacement[$class]);
						});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
		/*$(table).find('input:checkbox').addClass('ace').wrap('<label />')
				.after('<span class="lbl align-top" />');
		$('.ui-jqgrid-labels th[id*="_cb"]:first-child').find(
				'input.cbox[type=checkbox]').addClass('ace').wrap('<label />')
				.after('<span class="lbl align-top" />');*/
	}
});

function gobackpage(){
	history.go(-1);
}

/**
 * 获取主机的ID和IP地址列表
 * */
function getClusterList(){
	/*首先清空元素下的所有节点*/
	//$('#registry_hostid').empty();
	$.ajax({
        type: 'get',
        url: base+'host/freeHostList',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
            	var hostid = obj.hostid;
				var hostip = decodeURIComponent(obj.hostip);
				$('#host_id').append('<option value="'+hostid+'">'+hostip+'</option>');
            });
        }
	});
}

function insertHost(){
	var url = base+'registry/create';
	bootbox.dialog({
		title : "<b>添加仓库主机</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='add_item_frm'>"+
	    	      			"<div class='form-group' id='lb_host'>"+
			  					"<label class='col-sm-3'><div align='right'><b>选择主机：</b></div></label>"+
			  					"<div class='col-sm-9'>"+
			  						"<select id='host_id' name='host_id' class='form-control' onchange=\"validateReachRegiHost();\">"+
			  						"<option value=''>请选择主机</option>"+
				  					"</select>"+
				  				"</div>"+
				  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
				  			"</div>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>域名：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
				  					"<input placeholder=\" 请填写域名\" id=\"registry_dns\" name='registry_dns' type='text' class=\"form-control\"  onblur='checkDomainName();'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
				  			"<div class='form-group'>"+
			  					"<label class='col-sm-3'><div align='right'><b>端口号：</b></div></label>"+
			  					"<div class='col-sm-9'>"+
		      						"<input placeholder=\" 请输入仓库的端口号\" id=\"registry_port\" type='number' class=\"form-control\" onblur=\"validateReachRegiHost()\" value='5000'/>"+
		      					"</div>"+
		      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		      				"</div>"+
	    	      		"</form>"+
	    	      	"</div><script>getClusterList();</script>",
		buttons : {
			"success" : {
				"label" : "<i id=\"saveButt\" class=\"fa fa-floppy-o\"></i>&nbsp;<b>保存</b>",
				"className" : "btn-success btn-sm btn-round",
				"callback" : function() {
					var host_id = $('#host_id').val();
					var registry_dns = $("#registry_dns").val();
					var registry_port = $('#registry_port').val();
					
					data={
						  hostId:host_id,
						  registryDns:registry_dns,
						  registryPort:registry_port,
						  lbId:$("#lbId").val()
						  };
					$("#spinner").css("display","block");					
					$.post(url,data,function(response){
						$(grid_reghost_selector).trigger("reloadGrid");
						$("#spinner").css("display","none");
					});
				}
			},
			"cancel" : {
				"label" : "<i class=\"fa fa-times-circle\"></i>&nbsp;<b>取消</b>",
				"className" : "btn-sm btn-round btn-danger",
				"callback" : function() {}
			}
		}
	});
	$("#saveButt").parent().addClass("disabled");
}

function validateReachRegiHost(){
	if(validToolObj.isNull('#host_id',false) || validToolObj.isNull('#registry_port',false))
		return;

	var check_url = base+'registry/reachRegiHost',
		//获取选择主机ID的IP地址信息
		reg_ipaddr =$("#host_id").find("option:selected").text(),
		reg_port = $('#registry_port').val(),
		check_data = {
			  registry_port:reg_port,
			  registry_ipaddr:reg_ipaddr
		};
	//验证Docker仓库地址和端口的可达性
	$.post(check_url, check_data, function(response) {
		if (response.success) {
			$(".registry_port",$('#registry_port').parent()).remove();
			$("#saveButt").parent().removeClass("disabled");
		} else {
			showMessage(response.message);
			validToolObj.showTip('#registry_port','端口无效，请重新输入！');
			$("#saveButt").parent().addClass("disabled");
		}
	});
	check();
}

function checkDomainName(){
	validToolObj.isNull('#registry_dns');
	check();
}

//数据提交校验
function check(){
	validToolObj.validForm('#add_item_frm',"#saveButt",['#host_id','#registry_dns','#registry_port']);
}

function deleteHost(){
	var ids=$(grid_reghost_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_reghost_selector).jqGrid("getRowData",ids);
		var registryId = rowData.registryId;
		var hostIP = rowData.hostIP;

		data = {
			registryId:registryId
		};
		var url = base+'registry/deleteHost';
		bootbox.dialog({
	        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除仓库主机  ('+hostIP+') 吗?</div>',
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	$.post(url,data,function(response){
							if (response.success) {
	    						showMessage(response.message,function(){
	    							$(grid_reghost_selector).trigger("reloadGrid");
	    						});
	    					} else {
	    						showMessage("<font color=\"red\">错误</font>：删除(<font color=\"blue\"><i>" + hostIP 
	    								+ "</i></font>)仓库主机失败！<br><font color=\"green\">原因</font>："+response.message);
	    						$(grid_reghost_selector).trigger("reloadGrid");
	    					}
							
						});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}
}