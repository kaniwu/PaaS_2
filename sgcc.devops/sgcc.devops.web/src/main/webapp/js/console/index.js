
jQuery(function($) {
	/*真数据*/
	$.ajax({
		type : 'post',
		url : base + 'dashboard/getIndexJson',
		dataType : 'json',
		success : function(result) {
			if (result && result.success)
				dataAnalysisFun(result.data);
		}
	});
	/*真数据*/
	
	var hostElement = $('#dashboard_host_placeholder'),
		containerElement = $('#dashboard_container_placeholder'),
		businessElement = $('#dashboard_business_placeholder'),
		systemElement = $('#dashboard_system_placeholder');
	
	/*假数据*/
//	var result = {"success":true,"data":{"bssSystemNum":15,"phySystemNum":20,"hostNum":18,"containerNum":25,"imageNum":98,"lbNum":10,"clusterNum":15,"registryNum":5,"componentNum":8,"hostInfo":{"docker":85,"swarm":5,"nginx":3,"registry":7},"containerInfo":{"running":70,"stop":30}}};
//	dataAnalysisFun(result.data);
	/*假数据*/
	
	function dataAnalysisFun(data) {
		$('#dashboard_bssSystem_num b').html(data.hostCompNum+ '个主机组件');
		$('#dashboard_phySystem_num b').html(data.phySystemNum + '个物理系统');
		$('#dashboard_host_num b').html(data.hostNum + '台主机');
		$('#dashboard_container_num b').html(data.containerNum + '个容器');
		$('#dashboard_image_num b').html(data.imageNum + '个镜像');
		$('#dashboard_lb_num b').html(data.lbNum + '个负载均衡');
		$('#dashboard_cluster_num b').html(data.clusterNum + '个集群');
		$('#dashboard_registry_num b').html(data.registryNum + '个仓库');
		$('#dashboard_component_num b').html(data.componentNum + '个组件');
		
		createHostInfo(data.hostInfo);
		createContainerInfo(data.containerInfo);
		createBusinessInfo(data.businessInfo);
		createSystemInfo(data.physicalInfo);
	}
	// 主机
	function createHostInfo(data) {
		data = jQuery.extend({
			docker : 0,
			swarm : 0,
			registry : 0
		},data);
		var datas = [ {
			value : data.docker,
			name : 'Docker(' + data.docker + '台)'
		}, {
			value : data.swarm,
			name : 'Swarm(' + data.swarm + '台)'
		}, {
			value : data.registry,
			name : 'Registry(' + data.registry + '台)'
		} ];
		if(data.docker == 0 && data.swarm == 0 && data.registry == 0)
			datas = [];
		var option = {
			tooltip : false,
			legend : false,
			toolbox : false,
			calculable : false,
//			backgroundColor : '#e4e6e9',
			color : [ '#FFB980', '#5AB1EF', '#2EC7C9' ],
			series : [ {
				type : 'pie',
				startAngle : 120,
				radius : [ '60%', '80%' ],
				center : [ '50%', '60%' ],
				itemStyle : {
					normal : {
						label : {
							show : true
						},
						labelLine : {
							show : true
						}
					}
				},
				data : datas
			} ]
		}, myChart = echarts.init(hostElement.get(0));
		myChart.setOption(option);
	}
	// 容器统计信息
	function createContainerInfo(data) {
		data = jQuery.extend({
			running : 0,
			stop : 0
		},data);
		var datas = [ {
			value : data.running,
			name : '已部署(' + data.running + '台)'
		}, {
			value : data.stop,
			name : '未部署(' + data.stop + '台)'
		} ];
		if(data.running == 0 && data.stop == 0)
			datas = [];
		
		var option = {
			tooltip : false,
			legend : false,
			toolbox : false,
			calculable : false,
//			backgroundColor : '#e4e6e9',
			color : [ '#68BC31', '#DA5430' ],
			series : [ {
				type : 'pie',
				startAngle : 120,
				radius : [ '60%', '80%' ],
				center : [ '50%', '60%' ],
				itemStyle : {
					normal : {
						label : {
							show : true
						},
						labelLine : {
							show : true
						}
					}
				},
				data : datas
			} ]
		}, myChart = echarts.init(containerElement.get(0));
		myChart.setOption(option);
	}
	// 业务系统统计信息
	function createBusinessInfo(data) {
		data = jQuery.extend({
			running : 0,
			stop : 0
		},data);
		var datas = [ {
			value : data.deployedNum,
			name : '已部署(' + data.deployedNum + '台)'
		}, {
			value : data.unDployedNum,
			name : '未部署(' + data.unDployedNum + '台)'
		} ];
		if(data.deployedNum == 0 && data.unDployedNum == 0)
			datas = [];
		
		var option = {
			tooltip : false,
			legend : false,
			toolbox : false,
			calculable : false,
//			backgroundColor : '#e4e6e9',
			color : ['#68BC31', '#DA5430' ],
			series : [ {
				type : 'pie',
				startAngle : 120,
				radius : [ '60%', '80%' ],
				center : [ '50%', '60%' ],
				itemStyle : {
					normal : {
						label : {
							show : true
						},
						labelLine : {
							show : true
						}
					}
				},
				data : datas
			} ]
		}, myChart = echarts.init(businessElement.get(0));
		myChart.setOption(option);
	}
	// 物理系统统计信息
	function createSystemInfo(data) {
		data = jQuery.extend({
			running : 0,
			stop : 0
		},data);
		var datas = [ {
			value : data.deployedNum,
			name : '已部署(' + data.deployedNum + '台)'
		}, {
			value : data.unDployedNum,
			name : '未部署(' + data.unDployedNum + '台)'
		} ];
		if(data.deployedNum == 0 && data.unDployedNum == 0)
			datas = [];
		
		var option = {
			tooltip : false,
			legend : false,
			toolbox : false,
			calculable : false,
//			backgroundColor : '#e4e6e9',
			color : [ '#68BC31', '#DA5430' ],
			series : [ {
				type : 'pie',
				startAngle : 120,
				radius : [ '60%', '80%' ],
				center : [ '50%', '60%' ],
				itemStyle : {
					normal : {
						label : {
							show : true
						},
						labelLine : {
							show : true
						}
					}
				},
				data : datas
			} ]
		}, myChart = echarts.init(systemElement.get(0));
		myChart.setOption(option);
	}
});