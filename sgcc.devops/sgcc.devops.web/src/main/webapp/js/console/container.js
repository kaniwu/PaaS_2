var grid_selector = "#container_list";
var page_selector = "#container_page";
jQuery(function($) {
	
	var options = {
		bootstrapMajorVersion : 3,
		currentPage : 1,
		totalPages : 1,
		numberOfPages : 0,
		onPageClicked : function(e, originalEvent, page) {
			if(page == "next"){
				page = parseInt($('#currentPage').val())+1;
			}else{
				page = parseInt($('#currentPage').val())-1;
			}
//			getImageList(page,5,0);
		},

		shouldShowPage : function(type, page, current) {
			switch (type) {
				case "first" :
				case "last" :
					return false;
				default :
					return true;
			}
		}
	};
	$('#tplpage').bootstrapPaginator(options);
//	getImageList(1,5,0);
	
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
//	var containerType=$('#containerType').val();
	
	jQuery(grid_selector).jqGrid({
		url : base+'container/list?conType=1',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','UUID', '名称','容器名称', '依赖镜像', '','状态', '应用', '端口', '所属系统','系统编码','所在主机','容器类型','备注','环境', '创建时间', '操作'],
		colModel : [ 
		    {name : 'conId',index : 'conId',width : 10,hidden:true},
			{name : 'conUuid',index : 'conUuid',width : 10,hidden:true
				/*formatter:function(cellvalue, options, rowObject){
					return '<a href="#"> c-'+ cellvalue.substr(0, 8)+'</a>';
				}*/
			},
			{name : 'conTempName',index : 'conTempName',width : 15},
			{name : 'conName',index : 'conName',width : 15,hidden:true},
			{name : 'conImgid',index : 'conImgid',width : 15, hidden:true},
			{name : 'conPower',index : 'conPower',width : 10, hidden:true},
			{name : 'power',index : 'power',width : 10,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conStatus){
					case 0:
						return '<i class="fa fa-stop text-danger"><b> &nbsp; 已删除<b> </i>';
					case 1:
						return '<i class="fa fa-stop text-danger"><b> &nbsp;已退出<b></i>';
					case 2:
						switch(rowObject.conPower){
							case 0:
								return '<i class="fa fa-stop text-danger"><b> &nbsp; 已停止<b> </i>';
							case 1:
								return '<i class="fa fa-play-circle text-success"><b> &nbsp;运行中<b></i>';
						}
					case 3:
						return '<i class="fa fa-stop text-warning"><b> &nbsp;挂起<b></i>';
					}
				}
			},
			{name : 'appId',index : 'appId',width : 15, hidden:true},
			{name : 'port',index:'port',width :16,sortable : false,
				formatter:function(cellvalue, options, rowObject){
					return getPortInfos(rowObject.conId);
				}},
			{name : 'systemName', index : 'systemName',width : 15},
			{name : 'systemCode', index : 'systemCode',width : 15,hidden:true},
			{name : 'hostName', index : 'hostName',width : 10 },
			{name : 'conType', index : 'conType',width : 10, hidden:true,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conType){
					case 0:
						return '基础镜像容器';
					case 1:
						return '应用容器';
					}
				}
			},
			{name : 'conDesc', index : 'conDesc',width : 10, hidden:true},
			{name : 'conEnv', index : 'conEnv',width : 10, hidden:true},
			{name : 'conCreatetime', index : 'conCreatetime',width : 12},
			{
		    	name : '',
				index : '',
				width : 160,
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					var button = '';
					switch(rowObject.conType){
					case 1:
//						button = button + "<a class='btn btn-xs btn-success btn-round' onclick=\"load('"+rowObject.conId+"')\"><span class='ace-icon fa fa-arrow-down bigger-125'></span>应用日志</a> &nbsp;";
						button = button + "<a href='#modal-logs' data-toggle='modal' onclick='loadLogs(\""
							+rowObject.conId+"\",\""+rowObject.systemCode+"\",\""+rowObject.conTempName
							+"\")' class='btn btn-xs btn-success btn-round'' ><i class='ace-icon fa fa-arrow-down bigger-125'></i><b>应用日志</b></a>&nbsp;";
					}
					
					button = button + "<button class=\"btn btn-xs btn-info btn-round\" onclick=\"toMonitor('"+rowObject.conId+"')\"><span class=\"glyphicon glyphicon-success\"></span><i class=\"ace-icon fa fa-eye bigger-125\"></i>监控</button> &nbsp;";
					return button;
				}
		    }
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'conTempName',
		sortname: 'hostName',
		sortname: 'power',
		sortname: 'systemName',
		sortname: 'conCreatetime',
		sortorder: "desc",
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ 
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh red',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {}
	
	function getPortInfos(conId){
		var ports = "";
		$.ajax({
	        type: 'get',
	        url: base+'container/listPort',
	        data:{
	        	conId:conId
	        },
	        async:false,
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
	            	portInfo = obj.conIp+":"+obj.pubPort+"-->"+obj.priPort + (((index + 1)== array.length) ? '':'\n');
					ports += portInfo;
	            });
	        }
		});
		return ports;
	}
	
	
	$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
	
	/*
	 * Get selected image from imageList
	 * 
	 * */
	$('.imagelist').on('click', '.image-item', function(event) {
		event.preventDefault();
		$('div', $('#imagelist')).removeClass('selected');
		$(this).addClass('selected');
		checkCreateContainer();
	});
});
function createContainer(){
	var imageid = $('.imagelist').find('.selected').attr("imageid");
	if(imageid == ""||imageid==null){
		showMessage("请选择依赖的镜像");
		return;
	}
	var imageName = $('.imagelist').find('.selected').attr("imagename");
	var clusterId = $('#cluster_select option:selected').val();
	if(clusterId == ""||clusterId==null){
		showMessage("请选择集群");
		return;
	}
	var createModel = $('#create_mode option:selected').val();
	var conNumber = $('#container_num').val();
	if(!isInteger(conNumber)){
		showMessage("容器数量输入值有误，请重新输入");
		return;
	}
	var conName = $('#container_name').val();
	var conType = $("#conType").val();
	var conCommand = $("#container_command").val();
	var conPorts = $("#container_ports").val();
	var conEnv=$("#containerEnv").val();
	if(conEnv == ""||conEnv==null||conEnv==undefined){
		showMessage("请选择环境");
		return;
	}
	$("#spinner").css("display","block");
	$.ajax({
        type: 'post',
        url: base+'container/create',
        dataType: 'json',
        data:{
        	conType:conType,
        	imageId:imageid,
        	imageName:imageName,
        	clusterId:clusterId,	
        	createModel:createModel,
        	conNumber:conNumber,
        	conName:conName,
        	conEnv:conEnv,
        	conCommand:conCommand,
        	conPorts:conPorts
        },
        	success: function (response) {
        		$("#spinner").css("display","none");
        		console.log(response);
        		showMessage(response.message, function(){
            		$(grid_selector).trigger("reloadGrid");
            	});
        	},
        	error:function(response){
    	    	showMessage(response.message, function(){
            		$(grid_selector).trigger("reloadGrid");
            	});
    	    }
	 });
	$('#modal-wizard').modal('hide');
}
function toMonitor(id){
	location.href = base+'monitor/container/'+id+'.html';
}
/**
 * Start container
 * */
function start(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.conPower != 0){
			showMessage("容器 (" + rowData.conTempName + ")已经启动, 请重新选择！");
			return;
		}
		infoList += rowData.conTempName+" ";
	}
	if(infoList != ""){
		bootbox.dialog({
	        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;启动容器&nbsp;（' + infoList + '）?</div>',
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	var conids = new Array();
	                	for(var i=0; i<ids.length; i++){
	                		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	                		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
	                	}
//	                	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
	                	startContainer(conids);
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}else{
		showMessage("请选中要启动的容器！");
		return;
	}
}

/**
 * stop container
 * */
function stop(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.conPower != 1){
			showMessage("容器 (" + rowData.conTempName + ")已经停止, 请重新选择！");
			return;
		}
		infoList += rowData.conTempName+" ";
	}
	if(infoList != ""){
		bootbox.dialog({
	        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;停止容器&nbsp;（' + infoList + '）?</div>',
	        title: "提示",
	        buttons: {
	            main: {
	            	label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	var conids = new Array();
	                	for(var i=0; i<ids.length; i++){
	                		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	                		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
	                	}
//	                	$('.btn-group').append('<i id="spinner" class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
	                	stopContainer(conids);
	                }
	            },
	            cancel: {
	            	label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}else{
		showMessage("请选中要停止的容器！");
		return;
	}
}

function trash(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.conPower == 1){
			showMessage("容器( " + rowData.conTempName + ")没有停止, 不能删除！");
			return;
		}
		infoList += rowData.conTempName+" ";
	}
	if(infoList != ""){
		bootbox.dialog({
		    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除容器&nbsp;（' + infoList + '）?</div>',
		    title: "提示",
		    buttons: {
		        main: {
		        	label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
		            callback: function () {
		            	var conids = new Array();
		            	for(var i=0; i<ids.length; i++){
		            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		            		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
		            	}
//		            	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
		            	trashContainer(conids);
		            }
		        },
		        cancel: {
		        	label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
		            callback: function () {
		            }
		        }
		    }
		});
	}else{
		showMessage("请选中要删除的容器！");
	}
}
/**
 * Get image list
 */
function getImageList(page,limit,imageType,cluster_id){
	getEnvironment("containerEnv","");
	$("#conType").val(imageType);
	$('#imagelist').html("");
	var appmessage = {page:page,rows:limit,imageType:imageType,clusterId:cluster_id};
	$.ajax({
	    type: 'get',
	    url: base+'image/listByImageType',
	    data: appmessage,
	    dataType: 'json',
	    success: function (array) {
	    	var tableStr = "";
			if (array.rows.length >= 1) {
				var totalnum = array.records;
				var totalp = 1;
				if (totalnum != 0) {
					totalp = Math.ceil(totalnum / limit);
				}
				options = {
					totalPages : totalp
				};
				$('#tplpage').bootstrapPaginator(options);
				modalPageUpdate(page, totalp);
				$('#currentPage').val(page);
				var rowData = array.rows;
				var flag = 0;
				for (var i = 0; i < rowData.length; i++) {
					var obj = rowData[i];
					var imageId = obj.imageId;
					var imageStatus = obj.imageStatus;
					var imageName = obj.imageName;
					var tempName = obj.tempName;
					var tag = obj.imageTag;
					var imgUrl=imageName+":"+tag;
					if(tag==null||tag==""){
						imgUrl=imageName;
					}
					if(imageStatus!=1){
						continue;
					}
					if(flag==0){
						if (tag != null) {
							tableStr = tableStr
										+ '<div class="image-item selected" imageid="'+imageId+'" imagename="'
										+ imgUrl + '">' + tempName + '</div>';
						} else {
							tableStr = tableStr
										+ '<div class="image-item selected" imageid="'+imageId+'" imagename="'
										+ imgUrl + '">' + tempName + '</div>';
						}
					}else{
						if (tag != null) {
							tableStr = tableStr
										+ '<div class="image-item" imageid="'+imageId+'" imagename="'
										+ imgUrl + '">' + tempName + '</div>';
						} else {
							tableStr = tableStr
										+ '<div class="image-item" imageid="'+imageId+'" imagename="'
										+ imgUrl + '">' + tempName + '</div>';
						}
					}
					flag++;
				}
				$('#imagelist').html(tableStr);
			}
	    }
	});
}

/**
 * Start container 
 */
function startContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/start',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$("#spinner").css("display","none");
        	$(grid_selector).trigger("reloadGrid");
        },
        error:function(response){
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}


/**
 * Stop single container
 * */
function stopSingleContainer(conid, conUuid){
	bootbox.dialog({
        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;停止容器&nbsp;c-' + conUuid.substr(0,8) + '?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                	stopContainer(conid);
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}

/**
 * Stop container 
 */
function stopContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/stop',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$("#spinner").css("display","none");
        	$(grid_selector).trigger("reloadGrid");
        },
        error:function(response){
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}


/**
 * Remove single container
 */
function removeSingleContainer(conid, conUuid, conPower){
	if(conPower ==1){
		showMessage("容器 c-" + conUuid.substr(0,8) + "没有停止, 不能删除！");
		return;
	}
	$("#spinner").css("display","block");
	bootbox.dialog({
        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除容器&nbsp;c-' + conUuid.substr(0,8) + '?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                	$("#spinner").css("display","none");
//                	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
                	trashContainer(conid);
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}

/**
 * Trash container 
 */
function trashContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/trash',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        },
        error: function(response){
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}

/**
 * Modify page status
 * @param current
 * @param total
 */
function modalPageUpdate(current, total) {
	$('#currentPtpl').html(current);
	$('#totalPtpl').html(total);
}


function checkCluster(){
	var cluster=$('#cluster_select').val();
	if(isNull(cluster)||cluster==0){
		showMessage("请选择集群！");
    	return false;
    }else{
    	return true;
    }	
}

function checkCreateContainer(){
	if(validToolObj.validForm('#create_container_form',"#saveButt",['#cluster_select','#container_num','#container_name','#hostPwd','#containerEnv'],false)){
		var imageid = $('.imagelist').find('.selected').attr("imageid");
		if(validToolObj.validNull(imageid)){
			if($('#container_select_image_list').length == 0)
				showMessage("请选择依赖的镜像",function(){},'container_select_image_list');
		}else
			$('#create_container_btn').removeAttr("disabled");
	}else{
		$('#create_container_btn').attr("disabled","disabled");
	}
		
}

function checkContainerCluster(){
	validToolObj.isNull('#cluster_select',false);
	checkCreateContainer();
	var cluster_id = $("#cluster_select").val();
	getImageList(1,5,imageType,cluster_id);
}

function checkContainerNum(){
	validToolObj.isNull('#container_num',false) || validToolObj.number('#container_num');
	checkCreateContainer();
}

function checkContainerName(){
	validToolObj.isNull('#container_name') || validToolObj.validUserName('#container_name') || validToolObj.realTimeValid(base + 'container/checkName',undefined,'#container_name');
	checkCreateContainer();
}
function checkContainerEnv(){
	validToolObj.isNull('#containerEnv',false);
	checkCreateContainer();
}
//查询条件 
function searchHost(containerType){
	getClusters();
	checkContainerCluster();
	var hostId = '';
	var systemId = '';
	if(containerType==1||containerType==2){
		hostId = $('#hostId_'+containerType).val();
		systemId = encodeURI($('#systemId_'+containerType).val());
	}
	if(containerType==4){
		hostId = $('#hostId_'+containerType).val();
	}
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'container/list?conType='+containerType+'&sysId='+systemId+"&hostId="+hostId}).trigger("reloadGrid");
}
$().ready(function(){
	$.ajax({
		url:base+'host/hostList',
		success:function(data){
			var options1=$("#hostId_1");
			var options2=$("#hostId_2");
			var options4=$("#hostId_4");
			var clusterArray=jQuery.parseJSON(data);
			$.each(clusterArray,function(i,value){
				var hostId=value.hostId;
				var hostIp = value.hostIp;
				var hostType = value.hostType;
				var option="<option value='"+hostId+"'>"+hostIp+"</option>";
				if(hostType==1){
					options1.append(option);
					options4.append(option);
				}
				options2.append(option);
			});
		}
	});
	$.ajax({
		url:base+'system/listWithIP?page=1&rows=9999',
		success:function(data){
			var options1=$("#systemId_1");
			var options2=$("#systemId_2");
			$.each(data.rows,function(i,value){
				var systemId=value.systemId;
				var systemName = value.systemName;
				var option="<option value='"+systemId+"'>"+systemName+"</option>";
				options1.append(option);
				options2.append(option);
			});
		}
	});
});

function loadLogs(conId,systemCode,conTempName){
	$("#conTempName").html(conTempName);
	$("#logspinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/loadLogs',
        data: {
        	conId : conId,
        	systemCode : systemCode
        },
        dataType: 'json',
        success: function (response) {
        	$("#logspinner").css("display","none");
        	var content ='';
        	$.each (response, function (index, obj){
        		content +="<a href=\"javascript:scpLogs('"+conId+"','"+systemCode+"','"+obj.fullpath+"','"+obj.logs+"')\">"+obj.logs+"</a>&nbsp;&nbsp;<a href=\"javascript:timeViewLog('"+conId+"','"+systemCode+"','"+obj.fullpath+"','"+obj.logs+"')\">实时查看</a><br/>";
        	});
        	if(content==''){
        		content='无日志文件';
        	}
        	$("#logList").html(content);
		}
	});
}
function cleanLogs(){
	$("#logList").html('');
}
function scpLogs(conId,systemCode,fullpath,logName){
	$("#logspinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/scpLogs',
        data: {
        	conId : conId,
        	systemCode :systemCode,
        	remotePath:fullpath
        },
        dataType: 'json',
        success: function (response) {
        	$("#logspinner").css("display","none");
        	if(!response.success){
        		showMessage(response.message);
        	}else{
        		location.href=base+'container/download?conId='+conId+'&systemCode='+systemCode+'&logName='+logName;
        	}
		}
	});
}
//实时日志查看	
var isLogClose = false;
//如果上次请求时文件大小为0，则请求10行最新的日志；如果上次请求时文件大小不为0，则从上次的文件位置请求最新的。
var lastFileSize = 0;

function timeViewLog(conId,systemCode,conTempName,logName){
		//点击查看实时目录，去执行挂载目录操作
		/*$.ajax({
			type : 'get',
			url: base+'container/mountTimeLog',
			data:{
				conId:conId,
				systemCode:systemCode,
				conTempName :conTempName,
				lastFileSize:lastFileSize
			},
			async:false,
			dataType:'json',
			success:function(response){
				if(!response.success){
	        		showMessage(response.message);
	        	}
			}
		});*/
	
	
	
		//conTempName = "D:/test.log"
	    isLogClose = false;
		var siv = setInterval(test,2000);
		//滚动框需要显示的所有日志信息
		var allShowLogs="";
		
		function test(){
			//一次Ajax请求到的信息
			var logs="";
			$.ajax({
		        type: 'get',
		        url: base+'container/getRealTimeLog',
		        data: {
		        	conId:conId,
		        	systemCode:systemCode,
		        	conTempName : conTempName,
		        	lastFileSize: lastFileSize,
		        	logName:logName
		        	
		        },
		        //设置为同步，请求到数据后再显示
		        async:false,
		        dataType: 'json',
		        success: function (response) {
		        	if(!response.success){
		        		showMessage(response.message);
		        	}else{
		        		//防止关闭页面后进行ajax请求再给lastFileSize赋值。
		        		if(!isLogClose){
			   		        var obj = JSON.parse(response.message);
			        		logs = obj[0].content;
			        		lastFileSize = obj[1].lastFileSize;
		        		}else{
		        			lastFileSize=0;
		        		}
		        	}
				}
			});
			if(!isLogClose){
				for(var i=0;i<logs.length;i++){
					allShowLogs=allShowLogs+logs[i]+"</br>"
				}
				$("#logList_timeView").html(allShowLogs);
				//设置滚动条一直处于最下面
				document.getElementById('logList_timeView_modal').scrollTop = document.getElementById('logList_timeView_modal').scrollHeight
				$("#modal-timeViewlogs").modal('show');
				$("#conTempName_").text(logName);
			}else{
				//关闭循环请求
				clearInterval(siv);
			}
		}
	
}	
//点击关闭日志实时查看
//关闭时，将上次日志访问大小设置为0，以便再次打开时可以请求最新的10行。同时将文件是否关闭标志设置为true。
function cleanTimeViewLogs(){
	isLogClose = true;
	lastFileSize = 0;
	$("#logList_timeView").html('');
	$("#modal-timeViewlogs").modal('hidden');
}

	/*导出容器镜像*/
	function ConImage(){
		var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
		if(ids.length ==0){
			showMessage("请先选择一条记录！");
		}else if(ids.length >1){
			showMessage("只能选择一条记录！");
		}else{
			bootbox.dialog({
		        message: template('loadImage') ,
		        title: "下载镜像",
		        buttons: {
		            main: {
		                label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success btn-round",
		                callback: function () {
		                	var loadImageName = $("#loadImageName").val();
		                	var loadImageTag = $("#loadImageTag").val();
		                	if(loadImageName==null||loadImageName==""){
		                		showMessage("请输入镜像名称");
		                		return;
		                	}
		                	if(loadImageTag==null||loadImageTag==""){
		                		showMessage("请输入镜像版本");
		                		return;
		                	}
		                	var rowData = $(grid_selector).jqGrid("getRowData",ids);
		        			var conId =rowData.conId; //容器ID
		        			var conUuid = rowData.conUuid; //容器Uuid
		        			$("#spinner").css("display","block");
		        			$("#loadBaseImage").addClass("disabled");
		        			$("#loadMidImage").addClass("disabled");
		        			$("#loadAppImage").addClass("disabled");
		        			$.ajax({
		        		        type: 'get',
		        		        url: base+'container/commitContainer',
		        		        data: {
		        		        	conId : conId,
		        		        	conUuid : conUuid,
		        		        	imageName : loadImageName,
		        		        	imageTag : loadImageTag
		        		        },
		        		        dataType: 'json',
		        		        success: function (response) {
		        		        	if(response.success){
		        		        		scpConImage(conId,loadImageName,loadImageTag,response.fullpath);
		        		        	}else{
		        		        		showMessage(response.message);
		        		        	}
		        		        	$("#spinner").css("display","none");
		        		        	$("#loadBaseImage").removeClass("disabled");
				        			$("#loadMidImage").removeClass("disabled");
				        			$("#loadAppImage").removeClass("disabled");
		        				}
		        			});
		                }
		            },
		            cancel: {
		                label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger btn-round",
		                callback: function () {
		                }
		            }
		        }
		    });
		}
	}

	function scpConImage(conId,imageName,imageTag,fullpath){
		$.ajax({
	        type: 'get',
	        url: base+'container/scpConImage',
	        data: {
	        	conId:conId,
	        	remotePath:fullpath,
	        	imageName:imageName,
	        	imageTag:imageTag
	        },
	        dataType: 'json',
	        success: function (response) {
	        	if(!response.success){
	        		showMessage(response.message);
	        	}else{
	        		location.href=base+'container/downloadConImage?conName='+imageName+"_"+imageTag;
	        	}
			}
		});
	}
	
	/*function getRepoInfos(conId){
		var repo = "";
		$.ajax({
	        type: 'get',
	        url: base+'container/listPort',
	        data:{
	        	conId:conId
	        },
	        async:false,
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
	            	portInfo = obj.conIp+":"+obj.pubPort;
	            	repo += portInfo;
	            });
	        }
		});
		return repo;
	}*/
	
	function getEnvironment(id,environmentId){
		$.ajax({
			type: 'get',
			url: base+'environment/list',
			dataType: 'json',
			success: function (array) {
				$('#'+id).empty();
				$('#'+id).append('<option value="">请选择环境</option>');
				$.each (array, function (index, obj){
					var environmentName = obj.eName;
					if(environmentId==obj.eId){
						var temp ='<option value="'+obj.eId+'" selected="selected">'+environmentName+'</option>';
						$('#'+id).append(temp);
					}else{
						var temp ='<option value="'+obj.eId+'">'+environmentName+'</option>';
						$('#'+id).append(temp);
					}
				});
			}
		});
	}
	var imageType = 0;
	function setImageType(type){
		imageType = type;
	}
	/**
	 * Get cluster list
	 * 
	 * */
	getClusters();
	function getClusters(){
		$('#cluster_select').empty();
		$('#cluster_select').append('<option value="">请选择集群</option>');
		$.ajax({
	        type: 'get',
	        url: base+'cluster/all',
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
					var clusterId = obj.clusterId;
					var clustername = decodeURIComponent(obj.clusterName);
					var clusterStatus = obj.clusterStatus;
					if(clusterStatus==1){
						$('#cluster_select').append('<option value="'+clusterId+'">'+clustername+'</option>');
					}
	            });
	        }
		 });
	}
	
	function checkHostContaniners(){
		$("#spinner").css("display","block");
		$.ajax({
	        type: 'get',
	        url: base+'container/checkHostContaniners',
	        data: {},
	        dataType: 'json',
	        success: function (response) {
	        	$("#spinner").css("display","none");
	        	if(!response.success){
	        		showMessage(response.message);
	        	}else{
	        		showMessage(response.message);
	        		$(grid_selector).trigger("reloadGrid");
	        	}
			}
		});
	}