var containerCount = 0, f5Position = {}, nextPhyNgPosition = {}, nextCompNgPosition = {}, nextDockerPosition = {}, nextDbPosition = {}, rateX = 300, rateY = 45;

$(function() {
	var businessId = $("#businessId").val();
	
	var envId = getShuju(businessId);
	$("#spinner").show();
	$.ajax({
		type : 'post',
		url : base + 'topology/topologys?businessId=' + businessId+'&envId='+envId,
		dataType : 'json',
		success : function(result) {
			if(result&&result.length==0){
				return showMessage("尚未有部署的物理系统");
			}else{
				dataAnalysisFun(result);
			}
		}
	});
});

function dataAnalysisFun(array) {
	$("#spinner").hide();
	var parent = $('#canvasupParent'),tempCanvasupId = 0,maxCount = 0, nodes = [], edges = [], bssArray = [], f5Array = [], compNginxArray = [], phyNginxArray = [], dockerArray = [], dbArray = [], tempBssId = undefined, tempNodeId = undefined;
	
	$.each(array, function(index, obj) {
		if(obj.success){
			maxCount = 0;
			nodes = [];
			edges = [];
			f5Array = [];
			compNginxArray = [];
			phyNginxArray = [];
			dockerArray = [];
			dbArray = [];
			
			tempBssId = 'bss_' + obj.systemId;
			
			$.each(obj.data, function(index_index, obj_obj) {
				tempNodeId = obj_obj.id;
				if(obj_obj.status == 0)
					tempNodeId += '_exception';
				if (tempNodeId.indexOf('f5_') == 0) {
					f5Array.push(tempNodeId);
				} else if (tempNodeId.indexOf('conNginx_') == 0
						&& compNginxArray.indexOf(tempNodeId) == -1) {
					compNginxArray.push(tempNodeId);
				} else if (tempNodeId.indexOf('compNginx_') == 0
						&& phyNginxArray.indexOf(tempNodeId)) {
					phyNginxArray.push(tempNodeId);
				} else if (tempNodeId.indexOf('docker_') == 0
						&& dockerArray.indexOf(tempNodeId)) {
					dockerArray.push(tempNodeId);
//					obj_obj.name = obj_obj.normal + '个容器正常,' + obj_obj.fault + '个容器异常';
				} else if (tempNodeId.indexOf('db_') == 0
						&& dbArray.indexOf(tempNodeId)) {
					dbArray.push(tempNodeId);
				}
				nodes.push({
					data : obj_obj
				});
			});
			
			$.each(obj.data,function(index_index, obj_obj) {
				tempNodeId = obj_obj.id;
				if(obj_obj.status == 0){
					tempNodeId += '_exception';
					obj_obj.id = tempNodeId;
				}
				if (tempNodeId.indexOf('conNginx_') == 0
						|| tempNodeId.indexOf('compNginx_') == 0) {
					edges = edges.concat(circulationAnalysis(f5Array,
							obj_obj));
				} else if (tempNodeId.indexOf('docker_') == 0) {
					if (compNginxArray.length > 0)
						edges = edges.concat(circulationAnalysis(
								compNginxArray, obj_obj));
					else if (phyNginxArray.length > 0)
						edges = edges.concat(circulationAnalysis(
								phyNginxArray, obj_obj));
					else if (f5Array.length > 0)
						edges = edges.concat(circulationAnalysis(
								f5Array, obj_obj));
				} else if (tempNodeId.indexOf('db_') == 0) {
					edges = edges.concat(circulationAnalysis(
							dockerArray, obj_obj));
				}
			});
			maxCount = f5Array.length > compNginxArray.length ? f5Array.length : compNginxArray.length;
			maxCount = phyNginxArray.length > maxCount ? phyNginxArray.length : maxCount;
			maxCount = dockerArray.length > maxCount ? dockerArray.length : maxCount;
			maxCount = dbArray.length > maxCount ? dbArray.length : maxCount;
			
			maxCount = maxCount * 36;
			tempCanvasupId = "canvasup_" + index;
			parent.append('<div><div><h3 style="font-weight: bold;display: inline-block;">' + obj.systemName + '</h3><span style="margin-left: 10px;">当前版本：' + obj.version + '</span></div><div id="' + tempCanvasupId + '" style="height:' + maxCount + 'px;width:100%;"></div></div>');
			drawCytoscapeFun(tempCanvasupId, nodes, edges);
		}
	});
	if(document.getElementById('canvasupParent').innerHTML==''){
		parent.append('<div><h5 style="font-weight: bold;display: inline-block;">无部署物理系统</h5></div>');
	}
}
function circulationAnalysis(array, node) {
	var edges = [];
	$.each(array, function(i, v) {
		edges.push({
			data : {
				id : v + node.id,
				source : v,
				target : node.id,
			}
		});
	});
	return edges;
}

function drawCytoscapeFun(selector,nodes, edges) {
	var cy = cytoscape({
		container : document.getElementById(selector),
		minZoom : 0.8,
		maxZoom : 20,
		userPanningEnabled : false,
		style : [ {
			selector : 'node',
			css : {
				'content' : 'data(name)',
				'color' : '#080808',
				'text-valign' : 'bottom',
				'text-halign' : 'center',
				'font-size' : '12px',
				'font-weight' : 'bold',
				'background-color' : 'white'
			}
		}, {
			selector : 'node[id^="bss_"]',
			css : {
				'content' : 'data(name)',
				'color' : '#21251C',
				'font-family' : '"Crimson Text",serif',
				'font-weight' : 'bold',
				'padding-top' : '10px',
				'padding-left' : '50px',
				'padding-bottom' : '10px',
				'padding-right' : '50px',
				'border-color' : "#93999B",
				'border-opacity' : "1",
				'border-style' : "dashed",
				'border-width' : '1px',
				'text-valign' : 'top',
				'text-halign' : 'center'
			}
		}, {
			selector : 'node[id^="docker_"]',
			css : {
				'content' : 'data(name)',
				'text-valign' : 'bottom',
				'text-halign' : 'center',
				'font-size' : '10px',
				'color' : '#21251C',
				'font-family': '"Crimson Text",serif',
				
			}
		}, {
			selector : 'node[id$="_exception"]',
			css : {
				'color' : 'red',
				'background-color' : 'red'
			}
		},{
			selector : 'edge',
			css : {
				'target-arrow-shape' : 'triangle',
				'target-arrow-color' : '#9ff6FA',
				'line-color' : '#9DD6FA'
			}
		}, {
			selector : ':selected',
			css : {
				'background-color' : 'black',
				'line-color' : 'black',
				'target-arrow-color' : 'black',
				'source-arrow-color' : 'black',

			}
		} ],
		elements : {
			nodes : nodes,
			edges : edges
		}
	});
	cy.ready(function() {
		var width = cy.width(),height = cy.height(), f5Count = cy
				.nodes('node[id^="f5_"]').length, phyNgCount = cy
				.nodes('node[id^="compNginx_"]').length, compNgCount = cy
				.nodes('node[id^="conNginx_"]').length, dockerCount = cy
				.nodes('node[id^="docker_"]').length, dbCount = cy
				.nodes('node[id^="db_"]').length, countY = 0, countX = 0;

		cy.pan({
			x : 0,
			y : 0
		});
		cy.zoom(0.8);
		f5Count > 0 && ++countX;
		phyNgCount > 0 && ++countX;
		compNgCount > 0 && ++countX;
		dockerCount > 0 && ++countX;
		dbCount > 0 && ++countX;

		countY = phyNgCount > countY ? phyNgCount : countY;
		countY = compNgCount > countY ? compNgCount : countY;
		countY = dockerCount > countY ? dockerCount : countY;
		countY = dbCount > countY ? dbCount : countY;

		rateX = width / countX;
		
		/** f5* */
		f5Position = {
			x : rateX,
			y : height / 2
		};
		f5Position.y = f5Count > 1 ? 15 : f5Position.y;

		/** f5* */

		/** 物理负载* */
		nextPhyNgPosition = {
			x : 0,
			y : height / 2
		};
		nextPhyNgPosition.y = phyNgCount > 1 ? 15 : nextPhyNgPosition.y;
		nextPhyNgPosition.x = f5Count > 0 ? 2 * rateX : rateX;
		/** 物理负载* */

		/** 组件负载* */
		nextCompNgPosition = {
			x : 0,
			y : height / 2
		};
		nextCompNgPosition.y = compNgCount > 1 ? 15 : nextCompNgPosition.y;
		nextCompNgPosition.x = f5Count > 0 ? 2 * rateX : rateX;
		/** 组件负载* */

		/** 容器* */
		nextDockerPosition = {
			x : 3 * rateX,
			y : height / 2
		};
		nextDockerPosition.y = dockerCount > 1 ? 15 : nextDockerPosition.y;
		nextDockerPosition.x = f5Count == 0 ? nextDockerPosition.x - rateX : nextDockerPosition.x;
		nextDockerPosition.x = phyNgCount == 0 && compNgCount == 0 ? nextDockerPosition.x - rateX : nextDockerPosition.x;
		/** 容器* */

		/** 数据源* */
		nextDbPosition = {
			x : 4 * rateX,
			y : height / 2
		};
		nextDbPosition.y = dbCount > 1 ? 15 : nextDbPosition.y;
		nextDbPosition.x = f5Count == 0 ? nextDbPosition.x - rateX
				: nextDbPosition.x;
		nextDbPosition.x = phyNgCount == 0 && compNgCount == 0 ? nextDbPosition.x
				- rateX
				: nextDbPosition.x;
		nextDbPosition.x = dockerCount == 0 ? nextDbPosition.x
				- rateX : nextDbPosition.x;

		/** 数据源* */
		// 添加背景图片
		cy.nodes().each(
				function(i) {
					if (this.data().id.indexOf('f5_') == 0)
						f5Position = addF5Bg(this, f5Position);
					if (this.data().id.indexOf('compNginx_') == 0)
						nextPhyNgPosition = addPhysicalNginxBg(
								this, nextPhyNgPosition);
					if (this.data().id.indexOf('conNginx_') == 0)
						nextCompNgPosition = addComponentNginxBg(
								this, nextCompNgPosition);
					if (this.data().id.indexOf('docker_') == 0)
						nextDockerPosition = addDockerBg(this,
								nextDockerPosition);
					if (this.data().id.indexOf('db_') == 0)
						nextDbPosition = addDbBg(this,
								nextDbPosition);
				});
		// 添加提示
		cy
				.nodes(
						'node[id^="f5_"],[id^="compNginx_"],[id^="conNginx_"],[id^="docker_"],[id^="db_"]')
				.qtip({
					content : function() {
						return this.data().name;
					},
					show : {
						event : 'mouseover',
					},
					hide : {
						event : 'mouseout',
					},
					position : {
						my : 'top right',
						at : 'top left'
					},
					style : {
						classes : 'qtip-bootstrap'
					}
				});
	});
}
var addBssBg = function(node, position) {
	node.css('background-image', '../img/f5.svg');
	node.position(position);
	return {
		x : position.x,
		y : position.y + rateY
	};
}, addF5Bg = function(node, position) {
	node.css('background-image', '../img/f5.svg');
	node.position(position);
	return {
		x : position.x,
		y : position.y + rateY
	};
}, addPhysicalNginxBg = function(node, position) {
	node.css('background-image', '../img/phyNginx.svg');
	node.position(position);
	return {
		x : position.x,
		y : position.y + rateY
	};
}, addComponentNginxBg = function(node, position) {
	node.css('background-image', '../img/compNginx.svg');
	node.position(position);
	return {
		x : position.x,
		y : position.y + rateY
	};
}, addDockerBg = function(node, position) {
	node.css('background-image', '../img/appservice.svg');
	node.position(position);
	console.info('----------------------' + position.y);
	return {
		x : position.x,
		y : position.y + rateY
	};
}, addDbBg = function(node, position) {
	node.css('background-image', '../img/source.svg');
	node.position(position);
	return {
		x : position.x,
		y : position.y + rateY
	};
};
function getShuju(businessId){
	var myTabSj = $("#myTabSj");
	myTabSj.empty();
	var url  = base + 'environment/searchByEnv';
	var myTabSjInfo ="";
	var firstId ="";
	$.ajax({
        type: 'get',
        async:false,
        url: url,
        dataType: 'json',
        success: function (response) {
		 $.each(response,function(i,value){
				if(i==0){
					myTabSjInfo +="<li class='active'><a href='#"+value.eId+"' onclick='getSystemList(\""+value.eId+"\",\""+businessId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
					 firstId = value.eId;
				}else{
					myTabSjInfo +="<li ><a href='#"+value.eId+"' onclick='getSystemList(\""+value.eId+"\",\""+businessId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
				}
			});
		 myTabSj.append(myTabSjInfo);
     }});
	 return firstId;
}
function getSystemList(envId,businessId){
	$("#spinner").show();
	$('#canvasupParent').empty();
	$.ajax({
		type : 'post',
		url : base + 'topology/topologys?businessId=' + businessId+'&envId='+envId,
		dataType : 'json',
		success : function(result) {
			$("#spinner").hide();
			if(result&&result.length==0){
				return showMessage("尚未有部署的物理系统");
			}else{
				dataAnalysisFun(result);
			}
		}
	});
}