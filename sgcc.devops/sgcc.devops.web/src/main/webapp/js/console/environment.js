var grid_selector = '#environment_list';
var page_selector = '#environment_page';

$().ready(function(){
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
		url : base + 'environment/searchlist',
		datatype : "json",
		height : '100%',
		autowidth: true,  
        shrinkToFit: true,
        colNames : ['ID','环境名称','环境描述','创建时间', '创建人'],
		colModel : [
		    {name : 'eId',index : 'eId',width : 8,hidden:true},
	        {name : 'eName',index : 'eName',width : 6},
			{name : 'eDesc',index : 'eDesc',width : 6},
			{name : 'eTime',index : 'eTime',width : 6},
			{name : 'eCreatorName',index : 'eCreatorName',width : 6},
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [10, 10, 20, 50, 100, 1000 ],
		pager : page_selector,
		altRows : true, 
		sortname: 'eName',
		sortorder: "asc",
		multiselect: true,
		multiboxonly:true,
		jsonReader : {
			root : "rows",
			total : "total",
			page : "page",
			records : "records",
			repeatitems : false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
});

/* 新增环境 */
function insertEnvironment(){
	   bootbox.dialog({
	        title: "<b>环境维护</b>",
	        message: "<div class='well' style='margin-top: 1px;'>"
	        	+ "<form class='form-horizontal' role='form' id='create_environment_form'>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><b><font color='red'>*</font>&nbsp;环境名称：</b></label>"
	        	+ "<div class='col-sm-9'>" 
	        	+ "<input id=\"e_Name_insert\" name='e_Name_insert' type='text' class=\"form-control\"  onblur='checkAddName(this);' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "<div class='form-group'>" 
	        	+ "<label class='col-sm-3'><b><b><font color='red'>*</font>&nbsp;环境描述：</b></label>"
	        	+ "<div class='col-sm-9'>"
	        	+ "<input id='e_desc_insert' name='e_desc_insert' type='text' class=\"form-control\" onblur='checkAddDes(this);' />" 
	        	+ "</div>" 
	        	+ "</div>" 
	        	+ "</form>" 
	        	+ "</div>",
	        buttons: {
	            "success": {
	                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
	                "className": "btn-sm btn-danger btn-round",
	                "callback": function() {
	                    var eName = $('#e_Name_insert').val();
	                    var eDesc = $('#e_desc_insert').val();
	                    if (eName==null || eName==''){
	                    	showMessage("环境名称不能为空！");
	                    	return false;
	                    }
	                    if (eDesc==null || eDesc==''){
	                    	showMessage("环境描述不能为空！");
	                    	return false;
	                    }
	                    data = {
	                        eName:eName,
	                        eDesc:eDesc
	                    };
	                    console.log(data);
	                    url = base + "environment/createEnvironment";
	                    $("#spinner").css("display", "block");
	                    $.post(url, data,
	                    function(response) {
	                        showMessage(response.message);
	                        $(grid_selector).trigger("reloadGrid");
	                        $("#spinner").css("display", "none");
	                    });
	                }
	            },
	            "cancel": {
	                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
	                "className": "btn-sm btn-warning btn-round",
	                "callback": function() {
	                    $(grid_selector).trigger("reloadGrid");
	                }
	            }
	        }
	    });
} 

   /* 编辑环境 */
   function updateEnvironment(){
	   var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
    if (ids.length == 0) {
        showMessage("请先选择一条记录！");
    } else if (ids.length > 1) {
        showMessage("只能选择一条记录！");
    } else {
        var rowData = $(grid_selector).jqGrid("getRowData", ids);
        var eId = rowData.eId;
        var eName = rowData.eName;
        var eDesc = rowData.eDesc;
      
 bootbox.dialog({
        title: "<b>环境维护</b>",
        message: "<div class='well' style='margin-top: 1px;'>"
        	+ "<form class='form-horizontal' role='form' id='edit_environment_form'>" 
        	+ "<div class='form-group'>" 
        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;环境名称：</b></label>" 
        	+ "<div class='col-sm-9'>" 
        	+ "<input id=\"edit_eName\"  name='edit_eName' value='" + eName+ "'  type='text' class=\"form-control\"   onblur='checkUpdateName();'/>" 
        	+ "</div>" 
        	+ "</div>" 
        	+ "<div class='form-group'>" 
        	+ "<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;环境描述：</b></label>"
        	+ "<div class='col-sm-9'>" 
        	+ "<input id='edit_eDesc' name='edit_eDesc' value='" + eDesc + "'  type='text' class=\"form-control\"   onblur='checkUpdateDes();' />" 
        	+ "</div>" 
        	+ "</div>" 
        	+ "</form>" 
        	+ "</div>",
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                	var eName = $('#edit_eName').val();
                	var eDesc = $('#edit_eDesc').val();
                    data = {
                    	eId:eId,
 		        		eName:eName,
 		        		eDesc:eDesc
                    };
                    console.log(data);
                    url = base + "environment/updateEnvironment";
                    $("#spinner").css("display", "block");
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        $(grid_selector).trigger("reloadGrid");
                        $("#spinner").css("display", "none");
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                    $(grid_selector).trigger("reloadGrid");
	   	                }
	   	            }
	   	        }
	   	    });
	   }
 }	    

   
/* 删除数据字典 */
   function deleteEnvironment(){
	   var ids = $(grid_selector).jqGrid("getGridParam", "selarrrow");
    if (ids.length == 0) {
        showMessage("请先选择一条记录！");
    } else if (ids.length > 1) {
        showMessage("只能选择一条记录！");
    } else {
        var rowData = $(grid_selector).jqGrid("getRowData", ids);
        var eId =rowData.eId;
        var url = base + "environment/deleteEnvironment";
        var data = {
        		eId:eId
        };
        bootbox.dialog({
            message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除该环境吗?</div>',
            title: "提示",
            buttons: {
                main: {
                    label: "<i class='icon-info'></i><b>确定</b>",
                    className: "btn-sm btn-success btn-round",
                    callback: function () {
                         $("#spinner").css("display", "block");
                        $.post(url, data, function (response) {
                            if (response.success) {
                                showMessage(response.message, function () {
                                    $(grid_selector).trigger("reloadGrid");
                                });
                            } else {
                                showMessage("删除环境失败!" + response.message);
                                $(grid_selector).trigger("reloadGrid");
                            }
                            $("#spinner").css("display", "none");        
                        });
                    }
                },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
	                    callback: function () {
	                    }
	                }
	            }
	        });
	 }
}
   
 /*根据条件查询*/
   function searchEnvironment(){
		var eName = $('#e_name').val();
		jQuery(grid_selector).jqGrid('setGridParam',{url : base+'environment/searchlist?eName='+eName}).trigger("reloadGrid");
	}
  
/*数据校验*/
 //新建-名称校验
   function checkAddName(){
	//alert('test');
	validToolObj.isNull('#e_Name_insert');
	
   	checkEnvironmentCreate();
   }
   function checkEnvironmentCreate(){
       validToolObj.validForm('#create_environment_form',"#saveButt",['#e_Name_insert']);
   }
   
   function checkAddDes(){
		validToolObj.isNull('#e_desc_insert');
   }

   //编辑-名称校验
   function checkUpdateName(){
	validToolObj.isNull('#edit_eName');
   	checkEnvironmentUpdate();
   }
   function checkEnvironmentUpdate(){
       validToolObj.validForm('#edit_environment_form',"#saveButt",['#edit_eName']);
   }
   function checkUpdateDes(){
		validToolObj.isNull('#edit_eDesc');
  }

   

