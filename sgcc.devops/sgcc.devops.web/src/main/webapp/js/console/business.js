var grid_selector = "#business_list";
var page_selector = "#business_page";
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
    /*10.12删除主机类型和所在集群列*/
	jQuery(grid_selector).jqGrid({
		url : base+'business/listAll',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','业务名称','操作'],
		colModel : [ 
            {name : 'businessId',index : 'businessId',width : 15,hidden:true},
			{name : 'businessName',index : 'businessName',width : 14},
			{
		    	name : '',
				index : '',
				width : 240,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue,options,rowObject) {
					return "<a href='#modal-wizard' data-toggle='modal' onclick='querySystems("
					+rowObject.businessId+",\""+rowObject.businessName
					+"\")' class='btn btn-xs btn-info btn-round' ><i class='ace-icon fa fa-search bigger-125'></i><b>下属物理系统</b></a> &nbsp;"+
					"<a href='"+base+"topology/viewTopologys?businessId="+rowObject.businessId+"' data-toggle='modal' class='btn btn-xs btn-info btn-round' ><i class='ace-icon fa fa-list bigger-125'></i><b>查看拓扑</b></a> &nbsp;";
				}
		    }
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'businessName',
		sortorder: "asc",
		multiselect: true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
//		$(table).find('input:checkbox').addClass('ace')
//		.wrap('<label />')
//		.after('<span class="lbl align-top" />')
//		$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
//		.find('input.cbox[type=checkbox]').addClass('ace')
//		.wrap('<label />').after('<span class="lbl align-top" />');
	}

});
//var system_selector = "#system_list";
function querySystems(businessId,businessName){
	//环境变量
	var envId =getShuju(businessId);
	$("#businessNametemp").html(businessName);
	$('#system_list').empty();
	var tableStr = '<thead> <tr> <th style="border: 1px;border-style:inset;width:40%">物理系统名称</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">运行版本</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">最新版本</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">部署状态</th> </tr> </thead>';
	$.ajax({
	    type: 'get',
	    url: base+'system/getListByBusinessId',
	    data: {businessId:businessId,envId:envId},
	    dataType: 'json',
	    success: function (array) {
	    	$.each(array,function(index, obj){
	    		var systemName = obj.systemName;
	    		var systemDeployStatus = '';
	    		if(obj.systemDeployStatus ==1){
	    			systemDeployStatus = '已部署';
	    		}else if(obj.systemDeployStatus ==2){
	    			systemDeployStatus = '未部署';
	    		}else{
	    			systemDeployStatus = '未知';
	    		}
	    	
	    		tableStr = tableStr
				+ '<tr>'
				+'<td style="border: 1px;border-style:inset;">' + systemName + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + obj.runningVersion + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + obj.newestVersion + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + systemDeployStatus + '</td>'
				+'</tr>';
	    	});
	    	$('#system_list').html(tableStr);
	    }
	});
}
function topologys(businessId,businessName){
	url = base + "topology/viewTopologys";
	data = {businessId:businessId};
	$.ajax({  
        type:"get",//请求方式  
        url:url,//发送请求地址  
        dataType:"json",  
        data:data,//发送给数据库的数据  ,  
    });
}
function loadBusiness(){
	$("#spinner").css("display","block");
	var url = base + "business/loadBusiness";
	$.ajax({  
        type:"get",//请求方式  
        url:url,//发送请求地址  
        dataType:"json", 
        success:function(resp){
        	$("#spinner").css("display","none");
        	showMessage(resp.message, function (){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}

//查询条件
function searchBusiness(){
	var businessName=$('#businessName').val();
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'business/listAll?businessName='+businessName}).trigger("reloadGrid");
}
function getShuju(businessId){
	var myTabSj = $("#myTabSj");
	myTabSj.empty();
	var url  = base + 'environment/searchByEnv';
	var myTabSjInfo ="";
	var firstId ="";
	$.ajax({
        type: 'get',
        async:false,
        url: url,
        dataType: 'json',
        success: function (response) {
		 $.each(response,function(i,value){
				if(i==0){
					myTabSjInfo +="<li class='active'><a href='#"+value.eId+"' onclick='getSystemList(\""+value.eId+"\",\""+businessId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
					 firstId = value.eId;
				}else{
					myTabSjInfo +="<li ><a href='#"+value.eId+"' onclick='getSystemList(\""+value.eId+"\",\""+businessId+"\")'"
					+	"data-toggle='tab' >"+value.eName+"</a></li>";
				}
			});
		 myTabSj.append(myTabSjInfo);
     }});
	 return firstId;
}
function getSystemList(envId,businessId){
	$('#system_list').empty();
	var tableStr = '<thead> <tr> <th style="border: 1px;border-style:inset;width:40%">物理系统名称</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">运行版本</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">最新版本</th>'+
		'<th style="border: 1px;border-style:inset;width:20%">部署状态</th> </tr> </thead>';
	$.ajax({
	    type: 'get',
	    url: base+'system/getListByBusinessId',
	    data: {businessId:businessId,envId:envId},
	    dataType: 'json',
	    success: function (array) {
	    	$.each(array,function(index, obj){
	    		var systemName = obj.systemName;
	    		var systemDeployStatus = '';
	    		if(obj.systemDeployStatus ==1){
	    			systemDeployStatus = '已部署';
	    		}else if(obj.systemDeployStatus ==2){
	    			systemDeployStatus = '未部署';
	    		}else{
	    			systemDeployStatus = '未知';
	    		}
	    	
	    		tableStr = tableStr
				+ '<tr>'
				+'<td style="border: 1px;border-style:inset;">' + systemName + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + obj.runningVersion + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + obj.newestVersion + '</td>'
				+'<td style="border: 1px;border-style:inset;">' + systemDeployStatus + '</td>'
				+'</tr>';
	    	});
	    	$('#system_list').html(tableStr);
	    }
	});
}