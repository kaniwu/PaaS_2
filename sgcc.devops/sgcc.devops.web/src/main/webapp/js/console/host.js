//左侧树初始化--------------开始
var tree;
var treeData = [];
var setting = {
    check: {
        enable: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        onClick: zTreeOnClick
    }
};

function createZTreeFun(selector) {
    //    var tree = $.fn.zTree.init($(selector), setting, treeData);
    tree = $.fn.zTree.init($(selector), setting, treeData);
    tree.expandAll(true);
}
jQuery(function($) {
	initTree();
	initTable();
});
function initTree(){
	$("#treeDiv").css("height",$(".page-content").height());
	//初始化树
    $.ajax({
        url: base + "location/tree",
        //要加载数据的地址
        type: 'get',
        //加载方式
        dataType: 'json',
        //返回数据格式
        success: function(response) {
            if (response == "") {
                showMessage("数据加载异常！");
            } else {
                treeData = response.data;
                createZTreeFun("#locationLevel");
            }
        },
        error: function(response) {
            console.log(response);
        }
    });

}
//节点点击事件
function zTreeOnClick(event, treeId, treeNode) {
	//判断是否有子节点
	$.ajax({
        url: base + "parameter/hasChild?treeId="+treeNode.id,
        //要加载数据的地址
        type: 'get',
        //加载方式
        dataType: 'json',
        //返回数据格式
        success: function(response) {
            if (response.hasChild) {
            	if(treeNode.id==0){
            		$("#addBrather").addClass("hide");
            		$("#addChild").removeClass("hide");
            	}else{
            		$("#addBrather").removeClass("hide");
            		$("#addChild").removeClass("hide");
            	}
            } else {
            	$("#addBrather").addClass("hide");
            	$("#addChild").addClass("hide");
            	showMessage("未设置位置层级关系，请到参数管理中添加！");
            }
        },
        error: function(response) {
            console.log(response);
        }
    });
	searchHost();
}

//左侧树初始化--------------结束


var grid_selector = "#host_list";
var page_selector = "#host_page";


var grid_selector_component = "#component_list";
var page_selector_component = "#component_page";

function initTable(){
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $("#formdiv").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	
    /*10.12删除主机类型和所在集群列*/
	jQuery(grid_selector).jqGrid({
		url : base+'host/list?locationId=0',
		datatype : "json",
		height : '100%',
		width:500,
//		autowidth: true,
		colNames : ['ID','UUID','主机名称', '用户名','密码','IP地址','SSH端口','CPU','内存（MB）','所在集群','主机环境','描述','主机类型code','主机类型code','主机类型','操作系统','主机状态','DOCKER状态','创建时间','快捷操作'],
		colModel : [ 
            {name : 'hostId',index : 'hostId',width : 5,hidden:true},
			{name : 'hostUuid',index : 'hostUuid',width : 5,hidden:true},
			{name : 'hostName',index : 'hostName',width : 80},
			
			{name : 'hostUser',index : 'hostUser',width : 5,hidden:true},
			{name : 'hostPwd',index : 'hostPwd',width : 5,hidden:true},
			
			{name : 'hostIp',index : 'hostIp',width : 80},
			{name : 'hostPort',index : 'hostPort',width : 40},
			
			{name : 'hostCpu',index : 'hostCpu',sortable : false,width : 30},
			{name : 'hostMem',index : 'hostMem',sortable : false,width : 60},
			{name : 'clusterName',index : 'clusterName',width : 50},
			{name : 'hostEnv',index : 'hostEnv',width : 5,hidden:true},
			{name : 'hostDesc',index : 'hostDesc',width : 5,hidden:true},
			{name : 'clusterId',index : 'clusterId',width : 5,hidden:true},
			{name : 'hostType',index : 'hostType',width : 5,hidden:true},
			{name : 'hostTypeValue',index : 'hostTypeValue',width : 70,title : false,
				formatter:function(cell,opt,obj){
					switch(obj.hostType){
					case 0:	return "swarm管理服务器";
					case 1: return '<a href="#" data-toggle="modal" onclick="showContainers(\''+obj.hostId+'\',\''+obj.hostIp+'\');">docker服务器</a>'; 
					case 2: return "仓库主机";
					case 3: return "负载均衡服务器";
					default:return "暂未分配";
					}					
				}		
			},
			{name : 'hostKernelVersion',index : 'hostKernelVersion',sortable : false,width : 14,hidden:true},
			{name : 'hostStatus',index : 'hostStatus',width : 50,title : false,
				formatter:function(cell,opt,obj){
					switch(obj.hostStatus){
					case 0:	return "已删除";
					case 1: return '正常'; 
					case 2: return "监控异常";
					case 3: return "离线";
					default:return "未知";
					}					
				}		
			},
			{name : 'dockerStatus',index : 'dockerStatus',width : 100,title : false,
				formatter:function(cell,opt,rowObject){
					switch(rowObject.dockerStatus){
						case 0:	return "未安装"
						+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"installDocker('"+rowObject.hostId+"')\">"
						+"<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
						+"<b>安装</b></button> &nbsp;";
						case 1: return '正常    '
						+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"stopDocker('"+rowObject.hostId+"',5)\">"
						+"<i class=\"ace-icon fa fa-stop bigger-125\"></i>"
						+"<b>停止</b></button> &nbsp;";
						case 2: return "停止    "
						+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"startDocker('"+rowObject.hostId+"')\">"
						+"<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
						+"<b>启动</b></button> &nbsp;";
						default:return "未知    "
						+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"checkDocker('"+rowObject.hostId+"')\">"
						+"<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
						+"<b>检查</b></button> &nbsp;";
					}					
				}		
			},
			{name : 'hostCreatetime',index : 'hostCreatetime',width : 100,hidden:true},
			{
		    	name : '',
				index : '',
				width : 150,
				align :'left',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue,options,rowObject) {
					rowObject.hostDesc = stringTool.escape(rowObject.hostDesc);
					switch(rowObject.hostStatus){
						case 0:	return "";
						case 1: 
							return "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"checkHostWin('"
							+rowObject.hostId+"','"+rowObject.hostUuid+"','"+rowObject.hostName+"','"+rowObject.hostIp+"','"+rowObject.hostUser+"','"+rowObject.hostPwd+"','"+rowObject.hostDesc+"')\">"
							+"<i class=\"ace-icon fa fa-search bigger-125\"></i>"
							+"<b>查看</b></button> &nbsp;"
							+"<button class=\"btn btn-xs btn-info btn-round\" onclick=\"toMonitor('"+rowObject.hostId+"')\">"
							+"<i class=\"ace-icon fa fa-eye bigger-125\"></i>"
							+"<b>监控</b></button> &nbsp;";
						case 2: 
							return "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"checkHostWin('"
							+rowObject.hostId+"','"+rowObject.hostUuid+"','"+rowObject.hostName+"','"+rowObject.hostIp+"','"+rowObject.hostUser+"','"+rowObject.hostPwd+"','"+rowObject.hostDesc+"')\">"
							+"<i class=\"ace-icon fa fa-search bigger-125\"></i>"
							+"<b>查看</b></button> &nbsp;";
						case 3: 
							return "<button class=\"btn btn-xs btn-primary btn-round\" onclick=\"checkHostWin('"
							+rowObject.hostId+"','"+rowObject.hostUuid+"','"+rowObject.hostName+"','"+rowObject.hostIp+"','"+rowObject.hostUser+"','"+rowObject.hostPwd+"','"+rowObject.hostDesc+"')\">"
							+"<i class=\"ace-icon fa fa-search bigger-125\"></i>"
							+"<b>查看</b></button> &nbsp;"
							+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"reTest('"+rowObject.hostId+"')\">"
							+"<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
							+"<b>重连</b></button> &nbsp;";
						default:return "";
					}
				}
		    }
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'hostName',
		sortname: 'hostIp',
		sortname: 'clusterName',
		sortname: 'hostTypeValue',
		sortorder: "asc",
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
	}
}
//导出host
function exportHost(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	var nodes=tree.getSelectedNodes(true);
    var locationId = '';
    if(nodes.length>0&&nodes[0].id!=0){
	  locationId = nodes[0].id; //获取选中节点的值
    }
	var url = base + "host/exportHost?locationId="+locationId;
	 window.location.href=url;
}
//导出hostExcel模板
function importHostExcel(){
	var url = base + "host/importHostExcel";
	 window.location.href=url;
}
//导入
function importHost(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	var nodes=tree.getSelectedNodes(true);
    var locationId = '';
    if(nodes.length>0&&nodes[0].id!=0){
	  locationId = nodes[0].id; //获取选中节点的值
    }else{
	   showMessage("请选择主机所属层级（跟节点除外）！");
	return false;
     }
    $("#locationId").val(locationId);
	$("#excelFile").click();
}
function excelFilefun(){
	    $("#spinner").css("display","block");
        fileName = document.getElementById("excelFile").files[0];
        if(undefined == fileName || null == fileName){
        	showMessage("请选择应用文件！");
        	return;
        }
        var fileNameName = fileName.name.split(".");
        if("xls" != fileNameName[fileNameName.length - 1] && "xlsx" != fileNameName[fileNameName.length - 1] ) {
        	showMessage("格式不正确");
        	return;
		}
	  $("#importHostForm").ajaxSubmit(function (responseResult) {
		  $(grid_selector).trigger("reloadGrid");
		  $("#spinner").css("display","none");
		  showMessage(responseResult.message);
		 });

	
}
//
function removeHost(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.hostId;
		var type = rowData.hostType;
		var clusterId = rowData.clusterId;
		var hostIp = rowData.hostIp;
		var hostTypeValue = rowData.hostTypeValue;
		if(hostTypeValue!='暂未分配'){
			showMessage("主机已分配为："+hostTypeValue+"。不能删除！");
			return;
		}
		var url = base+"host/delete";
		$("#host_id").attr("value",id);
		$("#host_type").attr("value",type);
		$("#host_clusterId").attr("value",clusterId);
		if(clusterId!=""&&clusterId != null){
			showMessage("主机属于集群，请先解绑再删除！");
			return;
		}else{
			data = {
				hostId:id,
				hostType:type
			};
			bootbox.dialog({
		        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;确定要删除主机  ('+hostIp+') 吗?</div>',
		        title: "提示",
		        buttons: {
		            main: {
		                label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success btn-round",
		                callback: function () {
		                	$.post(url,data,function(response){
		                		showMessage(response.message);
								$(grid_selector).trigger("reloadGrid");
							});
		                }
		            },
		            cancel: {
		                label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger btn-round",
		                callback: function () {
		                }
		            }
		        }
		    });
		}
	}
	
}
/**
 * mayh
 * 重新测试主机连接
 */
function reTest(hostId){
	var data = {hostId:hostId};
	var url = base+"host/reTest";
	$("#spinner").css("display","block");
	$.get(url,data,function(response){
		showMessage(response.message);
		$(grid_selector).trigger("reloadGrid");
		$("#spinner").css("display","none");
	});
	
}
/**
 * mayh
 * 安装docker
 */
function installDocker(hostId){
	var data = {hostId:hostId,opration:'install'};
	var url = base+"host/dockerDaemon";
	$("#spinner").css("display","block");
	$.get(url,data,function(response){
		showMessage(response.message);
		$(grid_selector).trigger("reloadGrid");
		$("#spinner").css("display","none");
	});
}
/**
 * mayh
 * 启动docker
 */
function startDocker(hostId){
	bootbox.dialog({
        message: '<div class="alert alert-warning" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;启动Docker进程?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
                callback: function () {
                	var data = {hostId:hostId,opration:'start'};
                	var url = base+"host/dockerDaemon";
                	$("#spinner").css("display","block");
                	$.get(url,data,function(response){
                		showMessage(response.message);
                		$(grid_selector).trigger("reloadGrid");
                		$("#spinner").css("display","none");
                	});
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
                callback: function () {
                }
            }
        }
    });
}
/**
 * mayh
 * 停止docker
 */
function stopDocker(hostId,timer){
	var thtml = '<div class="alert alert-warning" style="margin:10px">'+
	'<span class="glyphicon glyphicon-info-sign"></span>停止Docker进程?</div>'+
	'<div align="center"><span class="badge badge-danger">'+timer+'</span></div>';
	if(timer>1){
		bootbox.dialog({
	        message: thtml,
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	stopDocker(hostId,--timer);
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}else{
		bootbox.dialog({
	        message: thtml,
	        title: "提示",
	        buttons: {
	            main: {
	                label: "<i class='icon-info'></i><b>确定</b>",
	                className: "btn-sm btn-success btn-round",
	                callback: function () {
	                	var data = {hostId:hostId,opration:'stop'};
	                	var url = base+"host/dockerDaemon";
	                	$("#spinner").css("display","block");
	                	$.get(url,data,function(response){
	                		showMessage(response.message);
	                		$(grid_selector).trigger("reloadGrid");
	                		$("#spinner").css("display","none");
	                	});
	                }
	            },
	            cancel: {
	                label: "<i class='icon-info'></i> <b>取消</b>",
	                className: "btn-sm btn-danger btn-round",
	                callback: function () {
	                }
	            }
	        }
	    });
	}
}
/**
 * mayh
 * 检查docker
 */
function checkDocker(hostId){
	var data = {hostId:hostId,opration:'check'};
	var url = base+"host/dockerDaemon";
	$("#spinner").css("display","block");
	$.get(url,data,function(response){
		showMessage(response.message);
		$(grid_selector).trigger("reloadGrid");
		$("#spinner").css("display","none");
	});
}
//新增单个主机
function showCreateHostWin(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	var locationId = '';
	if(nodes.length>0&&nodes[0].id!=0){
		locationId = nodes[0].id; //获取选中节点的值
	}else{
		showMessage("请选择主机所属层级（根节点除外）！");
		return false;
	}
	
	bootbox.dialog({
		title : "<i class='ace-icon fa fa-pencil-square-o bigger-125'></i>&nbsp;<b>添加主机</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_host_form'>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>主机名称：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"host_name\" name='host_name' type='text' class=\"form-control\" placeholder='  请输入主机名称' onblur='checkHostName(this);'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>IP地址：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"host_ip\" name='host_ip' type='text' class=\"form-control\" placeholder='  请输入IP地址' onblur='checkHostIP();'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
			  				"<label class='col-sm-3'><div align='right'><b>SSH端口：</b></div></label>"+
			  				"<div class='col-sm-9'>"+
    	      					"<input id=\"host_port\" name='host_port' type='text' class=\"form-control\" value='22' placeholder='  请输入SHH端口' />"+
    	      				"</div>"+
    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录账号：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id=\"hostUser\" name='hostUser' type='text' class=\"form-control\" placeholder='  请输入登录账号' onblur='checkHostUser();'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录密码：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id=\"hostPwd\" name='hostPwd' type='password' class=\"form-control\" placeholder='请输入登录密码' onblur='checkHostPwd();'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>主机环境：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<select id=\"hostEnv\" name=\"hostEnv\" class=\"form-control\" onchange='checkHostCreate();'></select>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
			      				"<label class='col-sm-3'><div align='right'><b>主机描述：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<textarea id=\"host_desc\" onblur=\"validToolObj.length('#host_desc',200)\" name=\"host_desc\" class=\"form-control\" rows=\"3\"></textarea>"+
	    	      				"</div>"+
	    	      			"</div>"+
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
					parentid = nodes[0].id; //获取选中节点的值
		    	    var hostName = $('#host_name').val();
		            var hostIp = $('#host_ip').val();
		            var hostDesc = $('#host_desc').val();
		            var hostUser = $('#hostUser').val();
		            var hostPwd = $('#hostPwd').val();
		            var hostEnv = $('#hostEnv').val();
		            var hostPort = $('#host_port').val();
					data={
							hostName: hostName,
	                        hostIp: hostIp,
//			                    	hostType: hostType,
	                        hostDesc: hostDesc,
	                        hostUser:hostUser,
	                        hostPwd:hostPwd,
	                        hostPort:hostPort,
	                        locationId:locationId,
	                        hostEnv:hostEnv
	                        
					};
					console.log(data);
					url=base+"host/create";
					$("#spinner").css("display","block");
					$.post(url,data,function(response){
						showMessage(response.message);
						$(grid_selector).trigger("reloadGrid");
						$("#spinner").css("display","none");
					});
			    }
			},
			    "cancel" : {
				"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {
					$(grid_selector).trigger("reloadGrid");
				}
			}
		}
	});
	getEnvironment("hostEnv","");
	$("#saveButt").parent().attr("disabled","disabled");
}
//批量新增主机
function showCreateGroupHostWin(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	var locationId = '';
	if(nodes.length>0&&nodes[0].id!=0){
		locationId = nodes[0].id; //获取选中节点的值
	}else{
		showMessage("请选择主机所属层级（根节点除外）！");
		return false;
	}
	
	bootbox.dialog({
		title : "<i class='ace-icon fa fa-pencil-square-o bigger-125'></i>&nbsp;<b>批量添加主机</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_host_form_'>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>开始IP：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"begin_host_ip\" name='begin_host_ip' type='text' class=\"form-control\" placeholder='  开始IP' onblur='checkGroupHostIP();'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>结束IP：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"end_host_ip\" name='end_host_ip' type='text' class=\"form-control\" placeholder='  结束IP' onblur='checkGroupHostIP();'/>"+
	    	      				"</div>"+
	    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      			"</div>"+
	    	      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录账号：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id=\"groupHostUser\" name='groupHostUser' type='text' class=\"form-control\" value='root'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录密码：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id=\"groupHostPwd\" name='groupHostPwd' type='password' class=\"form-control\" placeholder='请输入登录密码' onblur='checkHostPwd();'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>主机环境：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<select id=\"groupHostEnv\" name='groupHostEnv' class=\"form-control\" onchange='checkHostCreate();'></select>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt_'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger btn-round",
				"callback" : function() {
					parentid = nodes[0].id; //获取选中节点的值
		    	    var begin_host_ip = $('#begin_host_ip').val();
		            var end_host_ip = $('#end_host_ip').val();
		            var groupHostUser = $('#groupHostUser').val();
		            var groupHostPwd = $('#groupHostPwd').val();
		            var groupHostEnv = $('#groupHostEnv').val();
					data={
							beginIp: begin_host_ip,
	                        endIp: end_host_ip,
	                        hostUser: groupHostUser,
	                        hostPwd:groupHostPwd,
	                        locationId:locationId,
	                        hostEnv:groupHostEnv
					};
					console.log(data);
					url=base+"host/createGroup";
					$("#spinner").css("display","block");
					$.post(url,data,function(response){
						showMessage(response.message);
						$(grid_selector).trigger("reloadGrid");
						$("#spinner").css("display","none");
					});
			    }
			},
			    "cancel" : {
				"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning btn-round",
				"callback" : function() {
					$(grid_selector).trigger("reloadGrid");
				}
			}
		}
	});
	getEnvironment("groupHostEnv","");
	$("#saveButt").parent().attr("disabled","disabled");
}
//hostName 校验
function checkHostName(){
	validToolObj.isNull('#host_name')|| validToolObj.realTimeValid(base + 'host/checkName',undefined,'#host_name');
	checkHostCreate();
}

//Ip校验
function checkHostIP(){
	validToolObj.isNull('#host_ip',false) || validToolObj.isIp('#host_ip') || validToolObj.realTimeValid(base + 'host/checkIp',$('#host_ip').val(),'#host_ip','IP已存在，请重新输入！');
	checkHostCreate();
}



//Ip校验
function checkGroupHostIP(){
	validToolObj.isNull('#begin_host_ip',false) || validToolObj.isIp('#begin_host_ip') || validToolObj.realTimeValid(base + 'host/checkIp',$('#begin_host_ip').val(),'#begin_host_ip','IP已存在，请重新输入！');
	validToolObj.isNull('#end_host_ip',false) || validToolObj.isIp('#end_host_ip') || validToolObj.realTimeValid(base + 'host/checkIp',$('#end_host_ip').val(),'#end_host_ip','IP已存在，请重新输入！');
	var begin_host_ip = $("#begin_host_ip").val();
	var end_host_ip = $("#end_host_ip").val();
	if(begin_host_ip==''||end_host_ip==''){
		return;
	}
	var beginIndex = begin_host_ip.lastIndexOf('.');
	var beginStr = begin_host_ip.substr(0,beginIndex);
	var endIndex = end_host_ip.lastIndexOf('.');
	var endStr = end_host_ip.substr(0,endIndex);
	if(beginStr!=endStr){
		showMessage("开始IP与结束IP不在同一网段，请重新输入！");
	}
	var beginIp = begin_host_ip.substr(beginIndex+1);
	var endIp = end_host_ip.substr(endIndex+1);
	if(parseInt(beginIp)>parseInt(endIp)){
		showMessage("开始IP不能小于结束IP，请重新输入！");
	}
}
//hostName 校验
function checkHostUser(){
	validToolObj.isNull('#hostUser') || validToolObj.validUserName('#hostUser');
	checkHostCreate();
}

//密码校验
function checkHostPwd(){
	validToolObj.isNull('#hostPwd',true,48);
	checkHostCreate();
}

function checkHostCreate(){
//	var hostName=$('#host_name').val();
//	var hostIp = $('#host_ip').val();
//    var hostUser = $('#hostUser').val();
//    var hostPwd = $('#hostPwd').val();
//    
//    if(!isNull(hostName)&&!isNull(hostIp)&&!isNull(hostUser)&&!isNull(hostPwd)&&isIp(hostIp) || $('.' + validToolObj.classNameByError,'#create_host_form').length > 0){
//    	$("#saveButt").parent().removeAttr("disabled");
//    }else{
//    	$("#saveButt").parent().attr("disabled","disabled");
//    }
    validToolObj.validForm('#create_host_form',"#saveButt",['#host_name','#host_ip','#host_port','#hostUser','#hostPwd','#hostEnv']);
}

//
function getClusterList(){
	
	$('#cluster_select').empty();
    //
	$.get(base+'cluster/all',null,function(response){
		$.each (response, function (index, obj){
			var clusterid = obj.clusterId;
			var clusterhostid = obj.masteHostId;
			var clusterName = obj.clusterName;
			$('#cluster_select').append('<option value="'+clusterhostid+':'+clusterid+'">'+clusterName+'</option>');
		});
	},'json');
}


//
function showDeleteHosts(){
	
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		if(rowData.clusterId == 1){
			showMessage("主机" + rowData.hostId + "不能删除！");
			return;
		}
		infoList += rowData.hostId+" ";
	}
	bootbox.dialog({
	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除主机&nbsp;' + infoList + '?</div>',
	    title: "提示",
	    buttons: {
	        main: {
	        	label: "<i class='ace-icon fa fa-floppy-o bigger-125'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
	            callback: function () {
	            	var hostids = new Array();
	            	for(var i=0; i<ids.length; i++){
	            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	            		hostids = (hostids + rowData.hostId) + (((i + 1)== ids.length) ? '':',');
	            	}
	            	deleteHosts(hostids);
	            }
	        },
	        cancel: {
	        	label: "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
	            callback: function () {
	            	$(grid_selector).trigger("reloadGrid");
	            }
	        }
	    }
	});
  }
  
//
function deleteHosts(hostidArray){
	
	var data= {
     	ids : hostidArray
     };
	$.post(base+'host/deletes',data,function(){
		$(grid_selector).trigger("reloadGrid");
	});
	
}

//
function updateHostWin(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var id =rowData.hostId;
		var uuid = rowData.hostUuid;
		var name = rowData.hostName;
		var user =rowData.hostUser;
		var pwd = rowData.hostPwd;
		var ip = rowData.hostIp;
		var port = rowData.hostPort;
		var type = rowData.hostType;
		var env= rowData.hostEnv;
		var desc=rowData.hostDesc;
		var url= base+'host/update';
		var title  = '<i class="ace-icon fa fa-pencil-square-o bigger-125"></i>&nbsp;<b>修改主机</b>';
		if(desc==null||desc=="null"){
			desc="";
		}
		bootbox.dialog({
			title : title,
			message :"<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='update_host_frm'>"+
					  		"<div class='form-group hide'>"+
					  			"<label class='col-sm-3'><div align='right'><b>UUID：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
					  				"<input id='host_id_edit' name='host_id_edit'  value='"+id+"' type='hidden'/>"+
					  				"<input id='host_uuid_edit' name='host_uuid_edit' readonly='readonly' type='text' value='"+uuid+"'class='form-control'/>"+
					  				"<input id='host_type_edit' name='host_type_edit'  value='"+type+"' type='hidden'/>"+
					  			"</div>"+
		    	      		"</div>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>主机名称：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='host_name_edit' name='host_name_edit' type='text' value='"+name+"'class='form-control' onblur='checkHostNameEdit()'/>"+
		    	      			"</div>"+
		    	      			"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      		"</div>"+
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>IP地址 ：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='host_ip_edit' name='host_ip_edit' type='text' readonly='readonly' value='"+ip+"'class='form-control' />"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      		
		    	      		"<div class='form-group'>"+
					  			"<label class='col-sm-3'><div align='right'><b>SSH端口 ：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<input id='host_port_edit' name='host_port_edit' type='text'  value='"+port+"'class='form-control' />"+
		    	      			"</div>"+
	    	      			"</div>"+
		    	      		
		    	      		
			    	      	"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录账号：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id='host_user_edit' name='host_user_edit' type='text' value='"+user+"' class='form-control' placeholder='  请输入登录账号' onblur='checkHostUser();'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			      			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>登录密码：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<input id='host_pwd_edit' name='host_pwd_edit' type='password' value='"+pwd+"' class='form-control' placeholder='请输入登录密码' onblur='checkHostPwd();'/>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
			    	      		
			    	      		
		    	      		
		    	      		"<div class='form-group'>"+
				  				"<label class='col-sm-3'><div align='right'><b>主机环境：</b></div></label>"+
				  				"<div class='col-sm-9'>"+
			      					"<select id=\"host_env_edit\" name='host_env_edit' class=\"form-control\" onchange='checkHostNameEdit();'></select>"+
			      				"</div>"+
			      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			      			"</div>"+
				      		"<div class='form-group'>"+
				      			"<label class='col-sm-3'><div align='right'><b>主机描述：</b></div></label>"+
					  			"<div class='col-sm-9'>"+
		    	      				"<textarea id='host_desc_edit' class='form-control'  onblur=\"validToolObj.length('#host_desc_edit',200)\" rows='3'>"+desc+"</textarea>"+
		    	      			"</div>"+
		    	      		"</div>"+
		    	      	"</form>"+
		    	      "</div>"+
				"</div>",
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						id = $('#host_id_edit').val();
						uuid = $('#host_uuid_edit').val();
						var hostName = $('#host_name_edit').val();
						var hostUser = $('#host_user_edit').val();
						var hostPwd = $('#host_pwd_edit').val();
						var hostIp = $('#host_ip_edit').val();
						var hostType = $('#host_type_edit').val();
						var hostPort = $('#host_port_edit').val();
						var hostDesc =$('#host_desc_edit').val();
						var hostEnv =$('#host_env_edit').val();
						data={
								hostId:id,
								hostUuid:uuid,
								hostName:hostName,
								hostUser:hostUser,
								hostPwd:hostPwd,
								hostIp:hostIp,
								hostType:hostType,
								hostPort:hostPort,
								hostDesc:hostDesc,
								hostEnv:hostEnv
								};
						$.post(url,data,function(response){
								showMessage(response.message);
								$(grid_selector).trigger("reloadGrid");
						});
					}
				},
				"cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
					}
				}
			}
		});
		getEnvironment('host_env_edit',env);
	}
}

//hostName 校验
function checkHostNameEdit(){
	if(validToolObj.isNull('#host_name_edit') || validToolObj.validName('#host_name_edit') ||validToolObj.isNull('#host_env_edit') || validToolObj.realTimeValid(base + 'host/checkName',$('#host_id_edit').val(),'#host_name_edit')){
		$("#saveButt").parent().attr("disabled",'disabled');
    }else{
    	validToolObj.removeTip('#host_name_edit');
    	$("#saveButt").parent().removeAttr("disabled");
    }
}

function checkHostWin(id){
	var title = '&nbsp;<b>主机详细信息</b>';
	 var url = base + "host/hostById";
	    data = {
	    		Id: id
	    };
		$.get(url,data,function(response){
			 bootbox.dialog({
			    	title: title,
			        message: template('hostDetail',response),
			        className: "host",
			        buttons: {
			            cancel: {
			                label: "<i class='icon-info'></i> <b>返回</b>",
			                className: "btn-sm btn-danger btn-round",
			                callback: function() {}
			            }
			        }
			    });
			 getEnvironment('hostEnv',response.hostEnv);
			 $.get(base+"hostComponent/selectHostInstallByHostId",{hostId:id},function(response){
				 var html ='<table class="table table-striped table-bordered table-hover"> <thead class="thin-border-bottom"> '+
				 '<tr> <th> <i class="icon-user"></i> 组件名称 </th> <th> 版本 </th> </tr> </thead>';
				 $.each (response, function (index, obj){
					 if(obj!=null){
						 html +='<tbody> <tr> <td class="">'+obj.type+'</td> <td>'+obj.version+'</td> </tr> </tbody>';
					 }
				 });
				 html+='</table>';
				 $("#hostComTable").html(html);
			 });
		});
}


/*显示docker容器列表*/
function showContainers(hostId,hostIp){
	window.location = "containerOfHost.html?hostId="+hostId+"&hostIp="+hostIp+"&flag=1";
}
function toMonitor(id){
	location.href = base+'monitor/host/'+id+'.html';
}

//查询条件 查询主机列表
function searchHost(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	var locationId = '';
	if(nodes.length>0&&nodes[0].id!=0){
		locationId = nodes[0].id; //获取选中节点的值
	}else{
		locationId=0;
	}
	
	hostIp = $('#ip').val();
	clusterId = $('#clusterName').val();
	hostType = $('#hostType').val();
	
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'host/list?hostIp='+hostIp+'&clusterId='+clusterId+'&hostType='+hostType+'&locationId='+locationId}).trigger("reloadGrid");
}


$().ready(function(){
	$.ajax({
		url:base+'cluster/list?page=1&rows=9999',
		success:function(data){
			var options=$("#clusterName");
			$.each(data.rows,function(i,value){
				var clusterId=value.clusterId;
				var clusterName = value.clusterName;
				var option="<option value='"+clusterId+"'>"+clusterName+"</option>";
				options.append(option);
			});
		}
	});
	
	
});

//新增左侧菜单位置信息，level=0新增同级，level=1新增下级
function addLocationLevel(level){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	if(nodes.length==0){
		showMessage('请选择节点');
		return false;
	}
	var parentid='';
	var parentname='';
	if(level==0){
		parentid = nodes[0].getParentNode().id; //获取选中节点的值
		parentname = nodes[0].getParentNode().name; //获取选中节点的值
	}
	if(level==1){
		parentid = nodes[0].id; //获取选中节点的值
		parentname = nodes[0].name; //获取选中节点的值
	}
	var htmlContent ="<div class='well' style='margin-top: 1px;'>"+
					"			<form class='form-horizontal' role='form' id='create_user_form'>"+
					"				<input id='location_parent_id' name='location_parent_id' type='hidden'/>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;名称：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='location_name' name='location_name' type='text'"+
					"							class='form-control' placeholder='只能包含中文、英文、数字、下划线...' onblur='checkLocationName();' />"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;编码：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='location_code' name='location_code' type='text'"+
					"							class='form-control' placeholder='英文、数字、下划线...' onblur='checkLocationCode();'/>"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;层级意义：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<select id='location_param_code' name='location_param_code' class='form-control'><option value=''>请选择</option></select>"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;父节点：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='location_parent' name='location_parent' type='text' readOnly='readOnly'"+
					"							class='form-control'/>"+
					"					</div>"+
					"				</div>"+
					"			</form>"+
					"		</div>";
	
	bootbox.dialog({
        title: "<b>添加位置</b>",
        message: htmlContent,
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                    var locationParent = $('#location_parent_id').val();
                    var locationName = $('#location_name').val();
                    var locationCode = $('#location_code').val();
                    var locationParamCode = $('#location_param_code').val();
                    data = {
                    		locationParent: locationParent,
                    		locationName: locationName,
                    		locationCode: locationCode,
                    		locationParamCode:locationParamCode
                    };
                    console.log(data);
                    url = base + "location/create";
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        initTree();
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                }
            }
        }
    });
	$("#location_parent_id").val(parentid);
	$("#location_parent").val(parentname);
	getparam(parentid);
}
function getparam(parentid){
	$('#location_param_code').empty();
	$.ajax({
        type: 'get',
        url: base+'parameter/selectByLocation?parentid='+parentid,
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var paraCode = obj.paraCode;
				var paraValue = obj.paraValue;
				$('#location_param_code').append('<option value="'+paraCode+'">'+paraValue+'</option>');
            });
        }
	});
}

/*校验机房机架名称不能为空且不能重复*/
function checkLocationName(){
	validToolObj.isNull('#location_name') || validToolObj.validName('#location_name') || validToolObj.realTimeValid(base + 'host/checkTreeName',undefined,'#location_name');
	checkSaveButt();
}
/*校验机房机架编码不能为空*/
function checkLocationCode(){
	validToolObj.isNull('#location_code') || validToolObj.validName('#location_code')||validToolObj.validByRegExp(/^[a-zA-Z0-9_-]+$/,"#location_code","");
	checkSaveButt();
}
/*检查保存按钮是否可用*/
function checkSaveButt(){
	validToolObj.validForm('#create_user_form',"#saveButt",['#location_name','#location_code','#location_param_code']);
}
function checkUpdateLocationCode(){
	validToolObj.isNull('#update_location_code') || validToolObj.validName('#update_location_code')||validToolObj.validByRegExp(/^[a-zA-Z0-9_-]+$/,"#update_location_code","");
	checkSaveButt();
}
//修改左侧菜单位置信息
function updateLocationLevel(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	if(nodes.length==0){
		showMessage('请选择节点');
		return false;
	}
	var selectnodeid = nodes[0].id; //获取选中节点的值
	if(selectnodeid==0){
		showMessage('根节点不允许修改');
		return false;
	}
	var htmlContent ="<div class='well' style='margin-top: 1px;'>"+
					"			<form class='form-horizontal' role='form' id='create_user_form'>"+
					"				<input id='update_location_id' name='location_id' type='hidden'/>"+
					"				<input id='update_location_parent_id' name='location_parent_id' type='hidden'/>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;名称：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='update_location_name' name='location_name' type='text'"+
					"							class='form-control' placeholder='只能包含中文、英文、数字、下划线...'  />"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;编码：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='update_location_code' name='location_code' type='text'"+
					"							class='form-control' placeholder='英文、数字、下划线...'  onblur='checkUpdateLocationCode();' />"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;层级意义：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<select id='update_location_param_code' name='location_param_code' class='form-control'><option value=''>请选择</option></select>"+
					"					</div>"+
					"				</div>"+
					"				<div class='form-group'>"+
					"					<label class='col-sm-3'><b><font color='red'>*</font>&nbsp;父节点：</b></label>"+
					"					<div class='col-sm-9'>"+
					"						<input id='update_location_parent' name='location_parent' type='text' readOnly='readOnly'"+
					"							class='form-control'/>"+
					"					</div>"+
					"				</div>"+
					"			</form>"+
					"		</div>";
	
	bootbox.dialog({
        title: "<b>修改位置</b>",
        message: htmlContent,
        buttons: {
            "success": {
                "label": "<i class='ace-icon fa fa-floppy-o bigger-125' id='saveButt'></i> <b>提交</b>",
                "className": "btn-sm btn-danger btn-round",
                "callback": function() {
                	var id = $('#update_location_id').val();
                    var locationParent = $('#update_location_parent_id').val();
                    var locationName = $('#update_location_name').val();
                    var locationCode = $('#update_location_code').val();
                    var locationParamCode = $('#update_location_param_code').val();
                    data = {
                    		id:id,
                    		locationParent: locationParent,
                    		locationName: locationName,
                    		locationCode: locationCode,
                    		locationParamCode:locationParamCode
                    };
                    console.log(data);
                    var url = base + "location/update";
                    $.post(url, data,
                    function(response) {
                        showMessage(response.message);
                        initTree();
                    });
                }
            },
            "cancel": {
                "label": "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                "className": "btn-sm btn-warning btn-round",
                "callback": function() {
                }
            }
        }
    });
	$('#update_location_param_code').empty();
	selectLocationBykey(selectnodeid);
}
function selectLocationBykey(id){
	var url = base + "location/select";
	$.ajax({
        type: 'get',
        url: url,
        dataType: 'json',
        data:{id:id},
        success: function (response) {
        	$('#update_location_id').val(id);
            $('#update_location_parent_id').val(response.locationParent);
            $('#update_location_name').val(response.locationName);
            $('#update_location_code').val(response.locationCode);
//            $('#temp').val(response.locationParamCode);
            $('#update_location_parent').val(response.locationParentName);
            $.ajax({
                type: 'get',
                url: base+'parameter/selectByType?type=0',
                dataType: 'json',
                success: function (array) {
                    $.each (array, function (index, obj){
        				var paraCode = obj.paraCode;
        				var paraValue = obj.paraValue;
        				if(response.locationParamCode==paraCode){
        					$('#update_location_param_code').append('<option value="'+paraCode+'" selected="selected">'+paraValue+'</option>');
        				}else{
        					$('#update_location_param_code').append('<option value="'+paraCode+'">'+paraValue+'</option>');
        				}
        				
                    });
                }
        	});
        }
	});
}


//删除位置信息
function deleteLocationLevel(){
	var tree = $.fn.zTree.getZTreeObj('locationLevel');
	//选中节点
	var nodes=tree.getSelectedNodes(true);
	if(nodes.length==0){
		showMessage('请选择节点');
		return false;
	}
	var selectnodeid = nodes[0].id; //获取选中节点的值
	var selectnodename = nodes[0].name; //获取选中节点的值
	if(selectnodeid==0){
		showMessage('根节点不允许删除');
		return false;
	}
	if(nodes[0].isParent){
		showMessage('节点【'+selectnodename+'】有'+nodes[0].children.length+'个子节点，不允许删除！');
		return false;
	}
	bootbox.dialog({
	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除位置&nbsp;【' + selectnodename + '】?</div>',
	    title: "提示",
	    buttons: {
	        main: {
	        	label: "<i class='ace-icon fa fa-floppy-o bigger-125'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
	            callback: function () {
	            	var url = base + "location/delete";
	                $.post(url, {id:selectnodeid,locationName:selectnodename },
	                function(response) {
	                    showMessage(response.message);
	                    initTree();
	                });
	            }
	        },
	        cancel: {
	        	label: "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
	            callback: function () {
	            }
	        }
	    }
	});
}

/*组件升级安装*/
function showUpdateWin(){
	getComponentType();
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var hostId = rowData.hostId;
		bootbox.dialog({
	        title: "<b>组件升级安装</b>",
	        message : "<div style='margin-top:1px;'>"
			+ "<form class='form-horizontal' role='form' id='component_item_frm'>"
			+ "<div>"
			+ "<div class='well'>"
			+ "<div class='form-group'>"
			+ "<label class='col-sm-3'><div ><b>组件类型：</b></div></label>"
			+ "<div class='col-sm-4'>"
			+ "<select id='component_select' name='component_select' class='form-control' onchange=\"getComponentGrid('"+hostId+"')\">"
			+ "<option value=\"\">请选择组件类型</option>"
			+ "</select>" 
			+ "</div>"
			+"<div id='install_software_loading' style='float: right; display: none;'>"
			+"<i class='ace-icon fa fa-spinner fa-spin blue bigger-225'></i>请稍等..."
			+"</div>"
			+ "</div>"	
			+ "</div>"
			+ "<div >"
			+ "<table id='component_list'></table>"
			+ "<div id='component_page'></div>"
			+ "</div>"
			+ "</div>" 
			+ "</form>" 
			+ "</div>",
	        
		});
		initWin(hostId);
	}
}




/*SSH登录校验*/
function checkSSHLogin(){
	getComponentType();
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var hostId = rowData.hostId;
		var hostIp = rowData.hostIp;
		var hostName = rowData.hostName;
		var hostUser = rowData.hostUser;
		var hostPwd = rowData.hostPwd;
		var hostPort = rowData.hostPort;
		var hostType = rowData.hostType;
		data={
				hostId:hostId,
				hostName:hostName,
				hostUser:hostUser,
				hostPwd:hostPwd,
				hostIp:hostIp,
				hostType:hostType,
				hostPort:hostPort
				};
		//alert (hostUser+hostPort+hostType+base);
		$.ajax({
	        type: 'get',
	        url: base+'host/checkSSH',
//	        url: base+'job/checkSSH',
	        data: data,
	        dataType: 'json',
	        success: function (result) {
	        	//alert(result.success);
	        	if(result.success==false){
	        		showMessage("SSH连接失败，请检查主机密码、端口是否修改！");
	        	}else{
	        		showMessage("SSH连接成功！");
	        	}
	        }
	        
		});
	}
}

/*根据不同类型获取组件表*/
function getComponentGrid(hostId){
	var value = $('#component_select option:selected').val();
	jQuery(grid_selector_component).jqGrid('setGridParam',{url : base+'hostComponent/hostComponentList?type='+value}).trigger("reloadGrid");
}


/*弹窗表格*/
function initWin(hostId){
	jQuery(grid_selector_component).jqGrid({
	    url: base + 'hostComponent/hostComponentList',
	    datatype: "json",
	    height: '100%',
	    autowidth: true,
	    colNames: ['ID','类型','版本号', '操作'],
	    colModel: [{
            name: 'id',
            index: 'id',
            width: (($(".modal-content").width()/3)-20),
            hidden:true
        	},{
	            name: 'type',
	            index: 'type',
	            width: (($(".modal-content").width()/3)-20)
	        },{
	            name: 'version',
	            index: 'version',
	            width: (($(".modal-content").width()/3)-20)
	        }, {
	            name: '',
	            index: '',
	            width: (($(".modal-content").width()/3)-20),
	            align: 'left',
	            fixed: true,
	            sortable: false,
	            resize: false,
	            title: false,
	            formatter: function (cellvalue, options,
	                                 rowObject) {
	                return "<a href='#modal-wizard' data-toggle='modal'>"
	                + "<button class=\"btn btn-xs btn-success btn-round\" id=\"autoHostComponent\" onclick=\"installSoftware('"
	                + rowObject.id + "','" + hostId + "','1"
	                + "')\">"
	                + "<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
	                + "<b>安装</b>"
	                + "</button>"
	                + "</a> &nbsp;"
	                + "<a href='#modal-wizard' data-toggle='modal'>"
	                + "<button class=\"btn btn-xs btn-success btn-round\" id=\"autoHostComponent1\" onclick=\"installSoftware('"
	                + rowObject.id + "','" + hostId + "','2"
	                + "')\">"
	                + "<i class=\"ace-icon fa fa-refresh bigger-125\"></i>"
	                + "<b>升级</b>"
	                + "</button>"
	                + "</a> &nbsp;";
	            }
	        }],
	    viewrecords: true,
	    rowNum: 10,
	    rowList: [10, 20, 50, 100, 1000],
	    pager: page_selector_component,
	    altRows: true,
	    sortname: 'version',
	    sortorder: "asc",
	    jsonReader: {
	        root: "rows",
	        total: "total",
	        page: "page",
	        records: "records",
	        repeatitems: false
	    },
	    loadError: function (resp) {
	        if (resp.responseText.indexOf("会话超时") > 0) {
	            alert('会话超时，请重新登录！');
	            document.location.href = '/login.html';
	        }
	    },
	    loadComplete: function () {
	        var table = this;
	        setTimeout(function () {
	        	updateActionIcons(table);
	            updatePagerIcons(table);
	            enableTooltips(table);
	        }, 0);
	        $("#formdiv").show();
	    }
	});
	function updateActionIcons(table) {
	}
	jQuery(grid_selector_component).jqGrid(//分页栏按钮
			'navGrid',
			page_selector_component,
			{ 
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh red',
				view : false
			},{},{},{},{},{}
	);
}
function updatePagerIcons(table) {
    var replacement = {
        'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
        'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
        'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
        'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
    };
    $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
        .each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
            if ($class in replacement)
                icon.attr('class', 'ui-icon ' + replacement[$class]);
        });
}

function enableTooltips(table) {
    $('.navtable .ui-pg-button').tooltip({
        container: 'body'
    });
    $(table).find('.ui-pg-div').tooltip({
        container: 'body'
    });
}
/*组件类型获取*/
function getComponentType(){
	$.ajax({
        type: 'get',
        url: base+'parameter/selectByType',
        data: 'type=1',
        dataType: 'json',
        success: function (array) {
            $.each (array, function (index, obj){
				var paraCode = obj.paraCode;
				var paraValue = decodeURIComponent(obj.paraValue);
//				if(index == 0){
//					$('#component_select').append('<option value="'+paraCode+'" selected="selected">'+paraValue+'</option>');
//				}else{
					$('#component_select').append('<option value="'+paraCode+'">'+paraValue+'</option>');
//				}
            });
        }
	});
}

/*安装*/
function installSoftware(id,hostId,operationType){
	$("#install_software_loading").show();
	$.ajax({
        type: 'get',
        url: base+'host/installSoftware',
        data: {
        	hostId:hostId,
        	componentId:id,
        	operationType:operationType
        },
        dataType: 'json',
        success: function (response) {
        	showMessage(response.message);
        	$("#install_software_loading").hide();
        },
        error:function(response){
        	showMessage(response.logDetail);
        	$("#install_software_loading").hide();
        }
	});
}
function getEnvironment(id,environmentId){
	$.ajax({
		type: 'get',
		url: base+'environment/list',
		dataType: 'json',
		success: function (array) {
			$('#'+id).empty();
//			$('#'+id).append('<option value="">请选择主机环境</option>');
			$.each (array, function (index, obj){
				var environmentName = obj.eName;
				if(environmentId==obj.eId){
					var temp ='<option value="'+obj.eId+'" selected="selected">'+environmentName+'</option>';
					$('#'+id).append(temp);
				}else{
					var temp ='<option value="'+obj.eId+'">'+environmentName+'</option>';
					$('#'+id).append(temp);
				}
			});
		}
	});
}