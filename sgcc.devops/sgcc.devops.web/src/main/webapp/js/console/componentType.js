var grid_selector = "#componentType_list";
var page_selector = "#componentType_page";
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	})
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    })
	jQuery(grid_selector).jqGrid({
		url : base+'componentType/list',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['组件关系ID','组件类型名称','组件配置','连接配置','快捷操作'],
		colModel : [ 
		    {name : 'componentTypeId',index : 'componentTypeId',hidden : true},        
			{name : 'componentTypeName',index : 'componentTypeName',width : 15},
			{
		    	name : '',
				index : '',
				width : 120,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					return "<a href='#modal-wizard' data-toggle='modal' onclick=\"compConfig('"+rowObject.componentTypeId+ "')\"><b>组件配置</b></a> &nbsp;";
				}
		    },
			{
		    	name : '',
				index : '',
				width : 120,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					return "<a href=\"#modal-wizard\" data-toggle=\"modal\" onclick=\"connectConfig('"+rowObject.componentTypeId+ "')\"><b>连接配置</b></a> &nbsp;";
				}
		    },
		    {
		    	name : '',
				index : '',
				width : 120,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					return "<button class=\"btn btn-xs btn-primary\" onclick=\"showEditComponentTypeWin('"+rowObject.componentTypeId+ "','"+rowObject.componentTypeName+"')\"><b>编辑</b></button> &nbsp;";
				}
		    }
		    
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh red',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			})
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
		$(table).find('input:checkbox').addClass('ace')
		.wrap('<label />')
		.after('<span class="lbl align-top" />')
		$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
		.find('input.cbox[type=checkbox]').addClass('ace')
		.wrap('<label />').after('<span class="lbl align-top" />');
	}

});

function removeComponentType(componentTypeId){
	url = base+"componentType/delete";
	data = {componentTypeId:componentTypeId};
	bootbox.confirm("<b>你确定要删除此组件关系?</b>", function(restult) {
		if(result) {
			$.post(url,data,function(response){
				//alert(response.tostring);
				if(response.success){
					$(grid_selector).trigger("reloadGrid");
				}else{
					showMessage("删除组件关系失败！");
				}
			});
		}
	});
}

function showCreateComponentTypeWin(){
	bootbox.dialog({
		title : "<b>添加组件关系</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_component_type_form'>"+
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><b>组件类型名称：</b></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"component_type_name\" name='component_type_name' type='text' class=\"form-control\" />"+
	    	      				"</div>"+
	    	      			"</div>"+		      			
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='icon-ok'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger",
				"callback" : function() {
					    	    var componentTypeName = $('#component_type_name').val();					            
							data={
									componentTypeName: componentTypeName,								
							};
							console.log(data);
							url=base+"componentType/create";
							$.post(url,data,function(response){								
								showMessage(response.message,function(){
									$(grid_selector).trigger("reloadGrid");
								});
							});
					    }
					   
			},
			    "cancel" : {
				"label" : "<i class='icon-info'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning",
				"callback" : function() {}
			}
		}
	});
}

//编辑框
function showEditComponentTypeWin(componentTypeId,componentTypeName){
	bootbox.dialog({
		title : "<b>编辑组件关系</b>",
		message : "<div class='well ' style='margin-top:1px;'>"+
						"<form class='form-horizontal' role='form' id='create_component_type_form'>"+
							"<input id=\"component_type_id\" type=\"hidden\" name=\"component_type_id\"value='"+componentTypeId+"'>"+							
				  			"<div class='form-group'>"+
				  				"<label class='col-sm-3'><b>组件类型名称：</b></label>"+
				  				"<div class='col-sm-9'>"+
	    	      					"<input id=\"component_type_name\" name='component_type_name' type='text' value='"+componentTypeName+"' class=\"form-control\" />"+
	    	      				"</div>"+
	    	      			"</div>"+
	    	      		"</form>"+
	    	      	"</div>",
		buttons : {
			"success" : {
				"label" : "<i class='icon-ok'></i> <b>保存</b>",
				"className" : "btn-sm btn-danger",
				"callback" : function() {
								var componentTypeId = $('#component_type_id').val();
					    	    var componentTypeName = $('#component_type_name').val();
					           
							data={
									componentTypeId:componentTypeId,
									componentTypeName: componentTypeName,
							};
							console.log(data);
							url=base+"componentType/update";
							$.post(url,data,function(response){
								
								showMessage(response.message,function(){
									$(grid_selector).trigger("reloadGrid");
								});
							});
					    }
					   
			},
			    "cancel" : {
				"label" : "<i class='icon-info'></i> <b>取消</b>",
				"className" : "btn-sm btn-warning",
				"callback" : function() {}
			}
		}
	});
}

function showDeleteComponentTypes(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		
		infoList += rowData.componentTypeName+" ";
	}
	bootbox.dialog({
	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除组件关系&nbsp;' + infoList + '?</div>',
	    title: "提示",
	    buttons: {
	        main: {
	        	label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success",
	            callback: function () {
	            	var conids = new Array();
	            	var componentTypeIds="";
	            	for(var i=0; i<ids.length; i++){
	            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	            		componentTypeIds = (componentTypeIds + rowData.componentTypeId) + (((i + 1)== ids.length) ? '':',');
	            	}
	            	deleteComponentTypes(componentTypeIds);
	            }
	        },
	        cancel: {
	        	label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger",
	            callback: function () {
	            	location.reload();
	            }
	        }
	    }
	});
  }
function deleteComponentTypes(componentTypeIds){
	var data= {
     	ids : componentTypeIds
     };
	$.post(base+'componentType/deletes',data,function(){
		$(grid_selector).trigger("reloadGrid");
	});
}