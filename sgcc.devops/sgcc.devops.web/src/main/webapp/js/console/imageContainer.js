var grid_selector = "#imageContainer_list";
var page_selector = "#imageContainer_page";
jQuery(function($) {
	
	var options = {
		bootstrapMajorVersion : 3,
		currentPage : 1,
		totalPages : 1,
		numberOfPages : 0,
		onPageClicked : function(e, originalEvent, page) {
			if(page == "next"){
				page = parseInt($('#currentPage').val())+1;
			}else{
				page = parseInt($('#currentPage').val())-1;
			}
			getImageList(page,5);
		},

		shouldShowPage : function(type, page, current) {
			switch (type) {
				case "first" :
				case "last" :
					return false;
				default :
					return true;
			}
		}
	};
	$('#tplpage').bootstrapPaginator(options);
	getImageList(1,5);
	
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	var imageId=$("#imageId").val();
	jQuery(grid_selector).jqGrid({
		url : base+'container/listByimageId?imageId='+imageId,
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['ID','UUID', '名称', '依赖镜像', '','状态', '应用', '端口', '所属物理系统','容器类型','备注', '创建时间',],
		colModel : [ 
		    {name : 'conId',index : 'conId',width : 10,hidden:true},
			{name : 'conUuid',index : 'conUuid',width : 10,hidden:true,
				formatter:function(cellvalue, options, rowObject){
					return '<a href="#"> c-'+ cellvalue.substr(0, 8)+'</a>';
				}
			},
			{name : 'conTempName',index : 'conTempName',width : 15},
			{name : 'conImgid',index : 'conImgid',width : 15, hidden:true},
			{name : 'conPower',index : 'conPower',width : 10, hidden:true},
			{name : 'power',index : 'power',width : 10,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conPower){
						case 0:
							return '<i class="fa fa-stop text-danger">&nbsp; 已停止</i>';
						case 1:
							return '<i class="fa fa-play-circle text-success"><b> &nbsp;运行中<b></i>';
					}
				}
			},
			{name : 'appId',index : 'appId',width : 15, hidden:true},
			{name : 'port',index:'port',width :16,
				formatter:function(cellvalue, options, rowObject){
					return getPortInfos(rowObject.conId);
				}},
			{name : 'systemName', index : 'systemName',width : 10},
			{name : 'conType', index : 'conType',width : 10,
				formatter:function(cellvalue, options, rowObject){
					switch(rowObject.conType){
					case 0:
						return '基础镜像容器';
					case 1:
						return '应用容器';
				}
				}
			},
			{name : 'conDesc', index : 'conDesc',width : 10, hidden:true},
			{name : 'conCreatetime', index : 'conCreatetime',width : 12},
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ 
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh red',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {}
	
	function getPortInfos(conId){
		var ports = "";
		$.ajax({
	        type: 'get',
	        url: base+'container/listPort',
	        data:{
	        	conId:conId
	        },
	        async:false,
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
	            	portInfo = obj.conIp+":"+obj.pubPort+"-->"+obj.priPort + (((index + 1)== array.length) ? '':'\n');
					ports += portInfo;
	            });
	        }
		});
		return ports;
	}
	
	/**
	 * Next page, Pre page
	 * */
	currentStep =0;
	$('#modal-wizard .modal-header').ace_wizard()
	.on('change' , function(e, info){
		if(info.step == 1) {//第一步需要执行的操作
			/*var appId = $('#app_select option:selected').val();
			var imageid = $('.imagelist').find('.selected').attr("imageid");
			console.log(appId);
			console.log(imageid);
			if(appId==0){
				showMessage("请选择应用");
				currentStep == 0;
				return;
			}
			if(imageid != ""){
				showMessage("请选中依赖的镜像");
				currentStep == 0;
				return;
			}*/
		}else if(info.step == 2){//第二步需要执行的操作
			
		}
		return;
	})
	.on('finished', function(e) {//最后一步需要执行的操作
//		var appId = $('#app_select option:selected').val();
		var imageid = $('.imagelist').find('.selected').attr("imageid");
		var imageName = $('.imagelist').find('.selected').attr("imagename");
		var clusterId = $('#cluster_select option:selected').val();
		var createModel = $('#create_mode option:selected').val();
		var conNumber = $('#container_num').val();
		var conName = $('#container_name').val();
		var createParams = "";
		$.each($("#params li"), function() {
			var paramName = $(this).find("#meter option:selected").val();
			var paramValue = $(this).find("#param_value").val();
			if(paramValue != 0){
				createParams+=paramName + "="+paramValue +" ";
			}
		});
		$("#spinner").css("display","block");
		$.ajax({
	        type: 'post',
	        url: base+'container/create',
	        dataType: 'json',
	        data:{
//	        	appId:appId,
	        	imageId:imageid,
	        	imageName:imageName,
	        	clusterId:clusterId,	
	        	createModel:createModel,
	        	conNumber:conNumber,
	        	conName:conName,
	        	createParams:createParams},
	        	success: function (response) {
	        		$("#spinner").css("display","none");
	        		console.log(response);
	        	},
	        	error:function(response){
	    	    	showMessage(response.message, function(){
	            		$(grid_selector).trigger("reloadGrid");
	            	});
	    	    }
		 });
		$('#modal-wizard').hide();
		$('.modal-backdrop').hide();
		$('#modal-wizard .modal-header').ace_wizard({
			step:1
		});
		$('#modal-wizard .wizard-actions').show();
	});
	$('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');
	
	/**
	 * Get application list 
	 * 
	 */
//	getApplist();
//	function getApplist(){
//		$.ajax({
//	        type: 'get',
//	        url: base+'application/all',
//	        dataType: 'json',
//	        success: function (array) {
//	            $.each (array, function (index, obj){
//					var appid = obj.appId;
//					var appname = decodeURIComponent(obj.appName);
//					$('#app_select').append('<option value="'+appid+'">'+appname+'</option>');
//	            });
//	        }
//		});
//	}
	
	/**
	 * Get cluster list
	 * 
	 * */
	getClusters();
	function getClusters(){
		$.ajax({
	        type: 'get',
	        url: base+'cluster/all',
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
					var clusterId = obj.clusterId;
					var clustername = decodeURIComponent(obj.clusterName);
					$('#cluster_select').append('<option value="'+clusterId+'">'+clustername+'</option>');
	            });
	        }
		 });
	}
	
	/**
	 * Get parameters list
	 * 
	 */
//	getParams();
	function getParams(){
		$.ajax({
	        type: 'get',
	        url: base+'param/allValue',
	        dataType: 'json',
	        success: function (array) {
	            $.each (array, function (index, obj){
					var paramValue = obj.paramValue;
					var paramName = decodeURIComponent(obj.paramName);
					$('#meter').append('<option value="'+paramValue+'">'+paramName+'</option>');
	            });
	        }
		 });
	}
	
	/*
	 * Get selected image from imageList
	 * 
	 * */
	$('.imagelist').on('click', '.image-item', function(event) {
		event.preventDefault();
		$('div', $('#imagelist')).removeClass('selected');
		$(this).addClass('selected');
	});
	
	/**
	 * 
	 * Add container start param
	 * 
	 */
	$("#add-param").on('click', function(event) {
		event.preventDefault();
		$("#params li:first").clone(true).appendTo("#params");
		$("#params li").not(":first").find("#remove-param").show();
		$("#params li:first").find("#remove-param").hide();
		var str = $("#params li:last").find("#meter").val();
		if (str == 0 || str == 1 || str == 2) {
			$("#params li:last").find("#unit").text("%");
		} else {
			$("#params li:last").find("#unit").text("Mbps");
		}
	});
	
	/**
	 * 
	 * Remove container start param
	 * 
	 */
	$("#remove-param").on('click', function(event) {
		event.preventDefault();
		if ($("#params li").length > 1) {
			$(this).parent().remove();
		}
	});
	
	/**
	 * Start container
	 * */
	$("#start").on('click', function(event){
		var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
		var infoList = "";
		for(var i=0; i<ids.length; i++){
			var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
			if(rowData.conPower != 0){
				showMessage("容器 " + rowData.conUuid + "已经启动, 请重新选择！");
				return;
			}
			infoList += rowData.conUuid+" ";
		}
		if(infoList != ""){
			bootbox.dialog({
		        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;启动容器&nbsp;' + infoList + '?</div>',
		        title: "提示",
		        buttons: {
		            main: {
		                label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success",
		                callback: function () {
		                	var conids = new Array();
		                	for(var i=0; i<ids.length; i++){
		                		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		                		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
		                	}
//		                	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
		                	startContainer(conids);
		                }
		            },
		            cancel: {
		                label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger",
		                callback: function () {
		                }
		            }
		        }
		    });
		}else{
			showMessage("请选中要启动的容器！");
			return;
		}
	});
	
	/**
	 * stop container
	 * */
	$("#stop").on('click', function(event){
		var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
		var infoList = "";
		for(var i=0; i<ids.length; i++){
			var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
			if(rowData.conPower != 1){
				showMessage("容器 " + rowData.conUuid + "已经停止, 请重新选择！");
				return;
			}
			infoList += rowData.conUuid+" ";
		}
		if(infoList != ""){
			bootbox.dialog({
		        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;停止容器&nbsp;' + infoList + '?</div>',
		        title: "提示",
		        buttons: {
		            main: {
		            	label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success",
		                callback: function () {
		                	var conids = new Array();
		                	for(var i=0; i<ids.length; i++){
		                		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		                		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
		                	}
//		                	$('.btn-group').append('<i id="spinner" class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
		                	stopContainer(conids);
		                }
		            },
		            cancel: {
		            	label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger",
		                callback: function () {
		                }
		            }
		        }
		    });
		}else{
			showMessage("请选中要停止的容器！");
			return;
		}
	});
	
	/**
	 * remove container
	 */
	$("#trash").on('click', function(event){
		var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
		var infoList = "";
		for(var i=0; i<ids.length; i++){
			var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
			if(rowData.conPower == 1){
				showMessage("容器 " + rowData.conUuid + "没有停止, 不能删除！");
				return;
			}
			infoList += rowData.conUuid+" ";
		}
		if(infoList != ""){
			bootbox.dialog({
			    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除容器&nbsp;' + infoList + '?</div>',
			    title: "提示",
			    buttons: {
			        main: {
			        	label: "<i class='icon-info'></i><b>确定</b>",
		                className: "btn-sm btn-success",
			            callback: function () {
			            	var conids = new Array();
			            	for(var i=0; i<ids.length; i++){
			            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
			            		conids = (conids + rowData.conId) + (((i + 1)== ids.length) ? '':',');
			            	}
//			            	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
			            	trashContainer(conids);
			            }
			        },
			        cancel: {
			        	label: "<i class='icon-info'></i> <b>取消</b>",
		                className: "btn-sm btn-danger",
			            callback: function () {
			            }
			        }
			    }
			});
		}else{
			showMessage("请选中要删除的容器！");
		}
	});
});

function toMonitor(id){
	location.href = base+'monitor/container/'+id+'.html';
}
/**
 * Get image list
 */
function getImageList(page,limit){
	$('#imagelist').html("");
	var imageType = $("#imageType").val();
	var appmessage = {page:page,rows:limit,imageType:imageType};
	$.ajax({
	    type: 'get',
	    url: base+'image/listByImageType',
	    data: appmessage,
	    dataType: 'json',
	    success: function (array) {
	    	var tableStr = "";
			if (array.rows.length >= 1) {
				var totalnum = array.records;
				var totalp = 1;
				if (totalnum != 0) {
					totalp = Math.ceil(totalnum / limit);
				}
				options = {
					totalPages : totalp
				};
				$('#tplpage').bootstrapPaginator(options);
				modalPageUpdate(page, totalp);
				$('#currentPage').val(page);
				var rowData = array.rows;
				for (var i = 0; i < rowData.length; i++) {
					var obj = rowData[i];
					var imageId = obj.imageId;
					var imageName = obj.imageName;
					var tempName = obj.tempName;
					var tag = obj.imageTag;
					var imgUrl=imageName+":"+tag;
					if(tag==null||tag==""){
						imgUrl=imageName;
					}
						if (tag != null) {
							tableStr = tableStr
									+ '<div class="image-item" imageid="'+imageId+'" imagename="'
									+ imgUrl + '">' + tempName + '</div>';
						} else {
							tableStr = tableStr
									+ '<div class="image-item" imageid="'+imageId+'" imagename="'
									+ imgUrl + '">' + tempName + '</div>';
						}
					}
				$('#imagelist').html(tableStr);
			}
	    }
	});
}

/**
 * Start container 
 */
function startContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/start',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$("#spinner").css("display","none");
        	$(grid_selector).trigger("reloadGrid");
        },
        error:function(response){
        	$('#spinner').remove();
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}


/**
 * Stop single container
 * */
function stopSingleContainer(conid, conUuid){
	bootbox.dialog({
        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;停止容器&nbsp;c-' + conUuid.substr(0,8) + '?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success",
                callback: function () {
//                	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
                	stopContainer(conid);
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger",
                callback: function () {
                }
            }
        }
    });
}

/**
 * Stop container 
 */
function stopContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/stop',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$('#spinner').remove();
        	$("#spinner").css("display","none");
        	$(grid_selector).trigger("reloadGrid");
        },
        error:function(response){
        	$('#spinner').remove();
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}


/**
 * Remove single container
 */
function removeSingleContainer(conid, conUuid, conPower){
	if(conPower ==1){
		showMessage("容器 c-" + conUuid.substr(0,8) + "没有停止, 不能删除！");
		return;
	}
	bootbox.dialog({
        message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除容器&nbsp;c-' + conUuid.substr(0,8) + '?</div>',
        title: "提示",
        buttons: {
            main: {
                label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success",
                callback: function () {
//                	$('.btn-group').append('<i class="ace-icon fa fa-spinner fa-spin blue bigger-225"></i>');
                	trashContainer(conid);
                }
            },
            cancel: {
                label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger",
                callback: function () {
                }
            }
        }
    });
}

/**
 * Trash container 
 */
function trashContainer(conidArray){
	$("#spinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/trash',
        data: {
        	containerids : conidArray
        },
        dataType: 'json',
        success: function (response) {
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        },
        error: function(response){
        	$("#spinner").css("display","none");
        	showMessage(response.message, function(){
        		$(grid_selector).trigger("reloadGrid");
        	});
        }
    });
}

/**
 * Modify page status
 * @param current
 * @param total
 */
function modalPageUpdate(current, total) {
	$('#currentPtpl').html(current);
	$('#totalPtpl').html(total);
}

