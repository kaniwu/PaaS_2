//hostName 校验
function checkContainerNumber(){
	validToolObj.isNull('#arrangement_open_docker_count') || validToolObj.number('#arrangement_open_docker_count');
	checkAddContainer();
}
function checkAddContainer(){
	validToolObj.validForm('#add_containerNumber_form',"#add_containerNumber_form_save",['#arrangement_open_docker_count'])
}
function topology(){
	this.cy = undefined;
	this.options = {
		visitUrl : base + 'system/getVisitUrl',
		containerCount : 0,
		f5Position : {}, 
		nextPhyNgPosition : {}, 
		nextCompNgPosition : {}, 
		nextDockerPosition : {}, 
		nextDbPosition : {}, 
		rateX : 120, 
		rateY : 80,
		containerHtml : "<div class='well ' style='margin-top:1px;'>"
			+ "<form class='form-horizontal' id='add_containerNumber_form'>"
			+ "<div class='form-group'>"
			+ "<label class='col-sm-3'><b>容器(个)：</b></label>"
			+ "<div class='col-sm-9'>"
			+ "<input type='number' onblur='checkContainerNumber()' id='arrangement_open_docker_count' class='form-control'>"
			+ "</div>" + "</div>" + "</form>" + "</div>"
	}
	var deployId = $("#deployId").val();
	$("#spinner").show();
	var _this = this;
	$.ajax({
		type : 'post',
		url : base + 'topology/topology?deployId='+deployId,
		dataType : 'json',
		success : function(result) {
			$("#spinner").hide();
			if (result && result.success){
				if(result.data.length==0){
					return showMessage("尚未部署");
				}else{
					_this.dataAnalysisFun(result.data);
				}
				
			}else{
				return showMessage("尚未部署");
			}
		}
	});
//	var result = {
//		"success" : true,
//		"data" : [ {
//			"id" : "f5_1",
//			"name" : "f5",
//			"status" : 0
//		}, {
//			"id" : "compNginx_1",
//			"name" : "容器负载"
//		},{
//			"id" : "docker_2",
//			"name" : "容器2",
//			"status" : 0
//		}, {
//			"id" : "docker_3",
//			"name" : "容器3",
//			"status" : 0
//		}, {
//			"id" : "db_1",
//			"name" : "数据源"
//		}],
//		"systemId" : "1",
//		"systemName" : "业务系统1"
//	}
//	this.dataAnalysisFun(result.data);
}
topology.prototype = {
	addHtmlFunction : function(count) {
		var _this = this;
		bootbox.dialog({
			title : '添加容器',
			message : _this.options.containerHtml,
			buttons : {
				"success" : {
					"label" : "<i class='ace-icon fa fa-floppy-o bigger-125' id='add_containerNumber_form_save'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function(a, b, c) {
						var conCont = $('#arrangement_open_docker_count').val();
						_this.createTopologyFun(conCont);
					}
				},
				"cancel" : {
					"label" : "<i class='ace-icon fa fa-times gray bigger-125'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {
					}
				}
			}
		});
		$('#add_containerNumber_form_save').parent().attr("disabled","disabled");
	},createTopologyFun : function(containerCount) {
		var _this = this,
		params = {
			conCount : containerCount,
			deployId : $('#deployId').val()
		};
		$("#addContainer").attr("disabled","disabled");
		$("#spinner").show();
		$.ajax({
			type : 'get',
			url : base + 'topology/createConByDeployId',
			dataType : 'json',
			data : params,
			success : function(result) {
				$("#spinner").hide();
				$("#addContainer").removeAttr("disabled");
				showMessage(result.message,function(){
					if (result && result.success) {
						var parent = $('#canvasup').parent();
						_this.cy.destroy();
						parent.append('<div id="canvasup" class="col-sm-12" style="height:460px;padding-left:0px;padding-right:0px;"></div>');
						topologyObj = new topology();
					}
				});
			}
		});
	}, circulationAnalysis : function (array, node) {
		var edges = [], tempNode = undefined;
		$.each(array, function(i, v) {
			tempNode = {
				id : v + node.id,
				source : v,
				target : node.id,
			};
			edges.push({
				data : tempNode
			});
		});
		return edges;
	},dataAnalysisFun : function (data) {
		var _this = this,nodes = [], edges = [], tempEdges = undefined, tempNode = undefined, f5Array = [], compNginxArray = [], phyNginxArray = [], dockerArray = [], dbArray = [], tempId = undefined;
		$.each(data, function(index, obj) {
			tempId = obj.id;
			if(obj.status == 0)
				tempId += '_exception';
			if (obj.id.indexOf('f5_') == 0) {
				f5Array.push(tempId);
			} else if (tempId.indexOf('conNginx_') == 0) {
				compNginxArray.push(tempId);
			} else if (obj.id.indexOf('compNginx_') == 0) {
				phyNginxArray.push(tempId);
			} else if (tempId.indexOf('docker_') == 0) {
				dockerArray.push(tempId);
			} else if (tempId.indexOf('db_') == 0) {
				dbArray.push(tempId);
			}
			nodes.push({
				data : obj
			});
		});
		$.each(data, function(index, obj) {
			tempId = obj.id;
			if(obj.status == 0){
				tempId += '_exception';
				obj.id = tempId;
			}
			if (tempId.indexOf('conNginx_') == 0
					|| tempId.indexOf('compNginx_') == 0) {
				edges = edges.concat(_this.circulationAnalysis(f5Array, obj));
			} else if (tempId.indexOf('docker_') == 0) {
				if (compNginxArray.length > 0)
					edges = edges.concat(_this.circulationAnalysis(compNginxArray,
							obj));
				else if (phyNginxArray.length > 0)
					edges = edges
							.concat(_this.circulationAnalysis(phyNginxArray, obj));
				else if (f5Array.length > 0)
					edges = edges.concat(_this.circulationAnalysis(f5Array, obj));
			} else if (tempId.indexOf('db_') == 0) {
				edges = edges.concat(_this.circulationAnalysis(dockerArray, obj));
			}
		});
		_this.drawCytoscapeFun(nodes, edges);
	},drawCytoscapeFun : function (nodes, edges) {
		var _this = this;
		this.cy = cytoscape({
			container : document.getElementById('canvasup'),
			minZoom : 0.8,
			userZoomingEnabled : true,
			userPanningEnabled : false,
			style : [ {
				selector : 'node',
				css : {
					'content': 'data(name)',
					'text-valign' : 'bottom',
					'background-color' : '#f5f5f5'
//						,
//					'height' : 35,
//					'width' : 35
				}
			},{
				selector : 'node[id$="_exception"]',
				css : {
					'color' : 'red',
					'background-color' : 'red'
				}
			},{
				selector : 'edge',
				css : {
					'target-arrow-shape' : 'triangle',
					'target-arrow-color' : '#9DD6FA',
					'line-color' : '#9DD6FA'
				}
			}, {
				selector : ':selected',
				css : {
					'background-color' : 'black',
					'line-color' : 'black',
					'target-arrow-color' : 'black',
					'source-arrow-color' : 'black',

				}
			} ],
			elements : {
				nodes : nodes,
				edges : edges
			}
		});
		_this.cy.ready(function() {
			var width = _this.cy.width(), 
				f5Count = _this.cy.nodes('node[id^="f5_"]').length,
				phyNgCount = _this.cy.nodes('node[id^="compNginx_"]').length,
				compNgCount = _this.cy.nodes('node[id^="conNginx_"]').length,
				dockerCount = _this.cy.nodes('node[id^="docker_"]').length,
				dbCount = _this.cy.nodes('node[id^="db_"]').length,
				countY = 0, countX = 0;

			_this.cy.pan({
				x : 0,
				y : 0
			});

			f5Count > 0 && ++countX;
			phyNgCount > 0 && ++countX;
			compNgCount > 0 && ++countX;
			dockerCount > 0 && ++countX;
			dbCount > 0 && ++countX;

			countY = phyNgCount > countY ? phyNgCount : countY;
			countY = compNgCount > countY ? compNgCount : countY;
			countY = dbCount > countY ? dbCount : countY;

			_this.options.rateX = width / (countX + 2);
			
			/**f5**/
			_this.options.f5Position = f5Count > 0 ?  {
				x : _this.options.rateX,
				y : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2)
			} : {
				x : 0,
				y : 0
			};
			/**f5**/
			
			/**物理负载**/
			_this.options.nextPhyNgPosition = {
				x : 0,
				y : 0
			};
			_this.options.nextPhyNgPosition.y = phyNgCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
			_this.options.nextPhyNgPosition.x = f5Count > 0 ? 2 * _this.options.rateX : _this.options.rateX;
			/**物理负载**/
			
			/**组件负载**/
			_this.options.nextCompNgPosition = {
				x : 0,
				y : 0
			};
			_this.options.nextCompNgPosition.y = compNgCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
			_this.options.nextCompNgPosition.x = f5Count > 0 ? 2 * _this.options.rateX : _this.options.rateX;
			/**组件负载**/
			
			/**容器**/
			_this.options.nextDockerPosition = {
				x : 3 * _this.options.rateX,
				y : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2)
			};
			_this.options.nextDockerPosition.y = dockerCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
			_this.options.nextDockerPosition.x = f5Count == 0 ? _this.options.nextDockerPosition.x - _this.options.rateX : _this.options.nextDockerPosition.x; 
			_this.options.nextDockerPosition.x = phyNgCount == 0 && compNgCount == 0 ? _this.options.nextDockerPosition.x - _this.options.rateX : _this.options.nextDockerPosition.x; 
			/**容器**/
			
			/**数据源**/
			_this.options.nextDbPosition = {
				x : 4 * _this.options.rateX,
				y : _this.options.rateY
			};
			_this.options.nextDbPosition.y = dbCount > 1 ? _this.options.rateY : _this.options.rateY + (_this.options.rateY * (countY - 1) / 2);
			_this.options.nextDbPosition.x = f5Count == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x; 
			_this.options.nextDbPosition.x = phyNgCount == 0 && compNgCount == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x;
			_this.options.nextDbPosition.x = dockerCount == 0 ? _this.options.nextDbPosition.x - _this.options.rateX : _this.options.nextDbPosition.x; 
			
			/**数据源**/
			// 添加背景图片
			_this.cy.nodes().each(
					function(i) {
						if (this.data().id.indexOf('f5_') == 0)
							_this.addF5Bg(this, _this.options.f5Position);
						if (this.data().id.indexOf('compNginx_') == 0)
							_this.options.nextPhyNgPosition = _this.addPhysicalNginxBg(this,
									_this.options.nextPhyNgPosition);
						if (this.data().id.indexOf('conNginx_') == 0)
							_this.options.nextCompNgPosition = _this.addComponentNginxBg(this,
									_this.options.nextCompNgPosition);
						if (this.data().id.indexOf('docker_') == 0)
							_this.options.nextDockerPosition = _this.addDockerBg(this, _this.options.nextDockerPosition);
						if (this.data().id.indexOf('db_') == 0)
							_this.options.nextDbPosition = _this.addDbBg(this, _this.options.nextDbPosition);
					});
			var winH = _this.options.f5Position.y > _this.options.nextPhyNgPosition.y ? _this.options.f5Position.y : _this.options.nextPhyNgPosition.y,
				elements = $(_this.cy.container()),
				canvas = elements.find('canvas');

			winH = winH > _this.options.nextCompNgPosition.y ? winH : _this.options.nextCompNgPosition.y;
			winH = winH > _this.options.nextDockerPosition.y ? winH : _this.options.nextDockerPosition.y;
			winH = winH > _this.options.nextDbPosition.y ? winH : _this.options.nextDbPosition.y;
//			$('.page-content').height(winH);
			$('#sidebar').height(winH + 122);
			
			elements.height(winH);
			elements.find('canvas').height(winH);
			_this.cy.resize();
			
			// 添加提示
			_this.cy.nodes('node[id^="f5_"],[id^="compNginx_"],[id^="conNginx_"],[id^="docker_"],[id^="db_"]').qtip({
				content : function() {
					return this.data().name
				},
				show : {
			        event : 'mouseover',
			    },
			    hide : {
			        event : 'mouseout',
			    },
				position : {
					my : 'top right',
					at : 'top left'
				},
				style : {
					classes : 'qtip-bootstrap'
				}
			});
			// 添加右键
			_this.cy.cxtmenu({
				selector : 'node[id^="f5_"],[id^="compNginx_"],[id^="conNginx_"]',
				commands : [{
					content : '<icon class="ace-icon fa fa-home bigger-125"></icon><span class="bigger-125" style="margin-left:5px">访问</span>',
					select : function() {
						_this.visitFun(this);
					}
				}]
			});
			// 添加右键
			_this.cy.cxtmenu({
				selector : 'node[id^="docker_"]',
				commands : [{
					content : '<icon class="ace-icon fa fa-home bigger-125"></icon><span class="bigger-125" style="margin-left:5px">访问</span>',
					select : function() {
						_this.visitFun(this);
					}
				},{
					content : '<icon class="ace-icon fa fa-download bigger-125"></icon><span class="bigger-125" style="margin-left:5px" href="#modal-logs" data-toggle="modal" >日志</span>',
					select : function() {
						_this.visitLogFun(this);
					}
				},{
					content : '<icon class="ace-icon fa fa-trash-o bigger-125"></icon><span class="bigger-125" style="margin-left:5px">删除</span>',
					select : function() {
						_this.stopAndRemoveFun(this);
					}
				}]
			});
		});
	},addF5Bg : function(node, position) {
		node.css('background-image', '../img/f5.svg');
		node.position(position);
	}, addPhysicalNginxBg : function(node, position) {
		var _this = this;
		node.css('background-image', '../img/phyNginx.svg');
		node.position(position);
		return {
			x : position.x,
			y : position.y + _this.options.rateY
		}
	}, addComponentNginxBg : function(node, position) {
		var _this = this;
		node.css('background-image', '../img/compNginx.svg');
		node.position(position);
		return {
			x : position.x,
			y : position.y + _this.options.rateY
		}
	}, addDockerBg : function(node, position) {
		var _this = this;
		node.css('background-image', '../img/appservice.svg');
		node.position(position);
		return {
			x : position.x,
			y : position.y + _this.options.rateY
		}
	},addDbBg : function(node, position) {
		var _this = this;
		node.css('background-image', '../img/source.svg');
		node.position(position);
		return {
			x : position.x,
			y : position.y + _this.options.rateY
		}
	},stopAndRemoveFun : function (removeNode){
		var _this = this;
		var params = {
			containerid : removeNode.data().id.split("_")[1],
			systemId : $('#systemId').val(),
			deployId : $('#deployId').val()
		};
		$("#addContainer").attr("disabled","disabled");
		$("#spinner").show();
		$.ajax({
			type : 'get',
			url : base + 'topology/stopAndRemoveCon',
			dataType : 'json',
			data : params,
			success : function(result) {
				$("#addContainer").removeAttr("disabled");
				$("#spinner").hide();
				showMessage(result.message,function(){
					topology();
					if (result && result.success) {
						removeNode.remove();
					}
				});
			}
		});
	},visitFun : function (node){
		var _this = this,
			nodeId = node.data().id;
		
		var params = {
			nodeId : nodeId.split("_")[1],
			nodeType : node.data().id.split("_")[0],
			systemId : $('#systemId').val(),
			deployId : $('#deployId').val()
		};
		$.ajax({
			type : 'get',
			url : _this.options.visitUrl,
			dataType : 'json',
			data : params,
			success : function(result) {
				if (result && result.success) {
					window.open(result.url);
				}
			}
		});
	},visitLogFun : function (node){
		var _this = this;
	
		var conId = node.data().id.split("_")[1]
		$("#conTempName").html(node.data().name);
		$("#logspinner").css("display","block");
		$.ajax({
	        type: 'get',
	        url: base+'container/loadLogs',
	        data: {
	        	conId : conId
	        },
	        dataType: 'json',
	        success: function (response) {
	        	$("#logspinner").css("display","none");
	        	var content ='';
	        	$.each (response, function (index, obj){
	        		content +="<a href=\"javascript:scpLogs('"+conId+"','"+obj.fullpath+"','"+obj.logs+"')\">"+obj.logs+"</a><br/>";
	        	});
	        	if(content==''){
	        		content='无日志文件';
	        	}
	        	$("#logList").html(content);
			}
		});
	}
}

/**
 * @author yangqinglin
 * @datetime 2015年9月10日 17:12
 * @description 返回上一个页面
 */
function gobackpage(){
	history.go(-1);
}	
function scpLogs(conId,fullpath,logName){
	$("#logspinner").css("display","block");
	$.ajax({
        type: 'get',
        url: base+'container/scpLogs',
        data: {
        	conId : conId,
        	remotePath:fullpath
        },
        dataType: 'json',
        success: function (response) {
        	$("#logspinner").css("display","none");
        	if(!response.success){
        		showMessage(response.message);
        	}else{
        		location.href=base+'container/download?conId='+conId+'&logName='+logName;
        	}
		}
	});
}