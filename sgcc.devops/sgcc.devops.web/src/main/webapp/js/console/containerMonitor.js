$(document).ready(function() {
	var step = 2;
	//var container = '${container}';
	var container = $("#containerId").val();
	try {
		ace.settings.check('breadcrumbs', 'fixed');
	} catch (e) {
	}
	$('#container_cpu_chart').css('height', 300);
	$('#container_mem_chart').css('height', 300);
	$('#container_net_chart').css('height', 300);
	// 配置路径
	require.config({
		paths : {
			echarts : base + 'plugins/echarts'
		}
	});
	// 按需加载
	require([ 'echarts', 'echarts/chart/line' ], function(ec) {
		 echarts = ec;
	});
		var cpuChartObj = document.getElementById('container_cpu_chart');
		var memChartObj = document.getElementById('container_mem_chart');
		var netChartObj = document.getElementById('container_net_chart');
		var cpuChart = echarts.init(cpuChartObj);
		var memChart = echarts.init(memChartObj);
		var netChart = echarts.init(netChartObj);

		cpuChart.showLoading({
			text : '数据加载中...'
		});
		memChart.showLoading({
			text : '数据加载中...'
		});
		netChart.showLoading({
			text : '数据加载中...'
		});
		var dates = [];
		var cpus = [];
		var mems = [];
		var nins = [];
		var nous = [];
//		var now = new Date();
//		$.ajaxSettings.async = true;  
		$.ajax({
			url:base + 'monitor/getMonitorData?id=' + container + "&type=1",
			async: false,
			success:function(json) {
				dates = json.time;
				cpus = json.cpu;
				mems = json.mem;
				nins = json.nin;
				nous = json.nou;

				if (dates == null || dates.length == 0) {
					var now = new Date();
					var len = 20;
					while (len--) {
						dates.unshift(now.pattern("HH:mm:ss"));
						now = new Date(now - step * 1000);
						cpus.push(0);
						mems.push(0);
						nins.push(0);
						nous.push(0);
					}
				}
			}
		});

		var cpuOption = {
			title : {
				text : '容器CPU负载监控',
				subtext : '占宿主机的百分比'
			},
			tooltip : {
				show : true
			},
			legend : {
				data : [ 'CPU负载' ]
			},
			xAxis : [ {
				type : 'category',
				data : dates,
				splitNumber : 40
			} ],
			yAxis : [ {
				type : 'value',
				axisLabel : {
					formatter : '{value} %'
				}
			} ],
			series : [ {
				'name' : 'CPU负载',
				'type' : 'line',
				'data' : cpus,
				'smooth' : true
			} ]
		};
		var memOption = {
			title : {
				text : '容器内存使用监控',
				subtext : '单位：MB'
			},
			tooltip : {
				show : true
			},
			legend : {
				data : [ '内存负载' ]
			},
			xAxis : [ {
				type : 'category',
				data : dates,
				splitNumber : 40
			} ],
			yAxis : [ {
				type : 'value',
				axisLabel : {
					formatter : '{value} MB'
				}
			} ],
			series : [ {
				'name' : '内存负载',
				'type' : 'line',
				'data' : mems,
				'smooth' : true
			} ]
		};
		var netOption = {
			title : {
				text : '容器网络IO监控',
				subtext : '网络包单位：KB/S'
			},
			tooltip : {
				show : true
			},
			legend : {
				data : [ '网络收包速率', '网络发包速率' ]
			},
			xAxis : [ {
				type : 'category',
				data : dates,
				splitNumber : 40
			} ],
			yAxis : [ {
				type : 'value',
				axisLabel : {
					formatter : '{value} KB/S'
				}
			} ],
			series : [ {
				'name' : '网络收包速率',
				'type' : 'line',
				'data' : nins,
				'smooth' : true
			}, {
				'name' : '网络发包速率',
				'type' : 'line',
				'data' : nous,
				'smooth' : true
			} ]
		};
		cpuChart.setOption(cpuOption);
		cpuChart.hideLoading();
		memChart.setOption(memOption);
		memChart.hideLoading();
		netChart.setOption(netOption);
		netChart.hideLoading();
		cpu = 0;
		mem = 0;
		nin = 0;
		nou = 0;
		date = null;
		timeTicket = setInterval(function() {
			$.getJSON(base + 'monitor/container/' + container, function(json) {
				date = new Date().pattern("HH:mm:ss");
				cpu = json.cpu;
				mem = json.mem;
				nin = json.nin;
				nou = json.nou;
			});
			if(date!=null){
				// 动态数据接口 addData
				cpuChart.addData([ [ 0, // 系列索引
				cpu, // 新增数据
				false, // 新增数据是否从队列头部插入
				false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
				date ] ]);
				memChart.addData([ [ 0, // 系列索引
				mem, // 新增数据
				false, // 新增数据是否从队列头部插入
				false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
				date // 坐标轴标签
				] ]);
				netChart.addData([ [ 0, // 系列索引
				nin, // 新增数据
				false, // 新增数据是否从队列头部插入
				false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
				date ], [ 1, // 系列索引
				nou, // 新增数据
				false, // 新增数据是否从队列头部插入
				false, // 是否增加队列长度，false则自定删除原有数据，队头插入删队尾，队尾插入删队头
				date // 坐标轴标签
				] ]);
			}
		}, step * 1000);
	
	
	$("#timeSelect").change(function(){
		var model = $("#timeSelect").val();
		if (model == 0) {
			window.location.reload();
		} else if (model == 1) {
			window.clearInterval(timeTicket);
			addData(1,cpuChart,memChart,netChart);
		} else if (model == 2) {
			window.clearInterval(timeTicket);
			addData(4,cpuChart,memChart,netChart);
		} else if (model == 3) {
			window.clearInterval(timeTicket);
			addData(12,cpuChart,memChart,netChart);
		} else if (model == 4) {
			window.clearInterval(timeTicket);
			addData(24,cpuChart,memChart,netChart);
		}

	});
	// 获取并添加数据
	function addData(model,cpuChart,memChart,netChart) {
		cpuChart.clear();
		memChart.clear();
		netChart.clear();
		
		cpuChart.showLoading({
			text : '数据加载中...'
		});
		memChart.showLoading({
			text : '数据加载中...'
		});
		netChart.showLoading({
			text : '数据加载中...'
		});
		
		$.getJSON(base + 'monitor/container/getData?containerId=' + container+"&model="+model, function(json) {
			var dates = json.time;
			var cpus = json.cpu;
			var mems = json.mem;
			var nins = json.nin;
			var nous= json.nou;
		
			if(dates==null||dates.length==0){
				var now = new Date();
				var len = 5;
				var step = model * 60 / 4;
				while (len--) {
					dates.unshift(now.pattern("HH:mm:ss"));
					now = new Date(now - step * 1000 * 60);
					cpus.push(0);
					mems.push(0);
					nins.push(0);
					nous.push(0);
				}
			}
			
			var cpuOption = {
					title : {
						text : '容器CPU负载监控',
						subtext : '占宿主机的百分比'
					},
					tooltip : {
						show : true
					},
					legend : {
						data : [ 'CPU负载' ]
					},
					xAxis : [ {
						type : 'category',
						data : dates,
						splitNumber : 40
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel : {
							formatter : '{value} %'
						}
					} ],
					series : [ {
						'name' : 'CPU负载',
						'type' : 'line',
						'data' : cpus,
						'smooth' : true
					} ]
				};
				var memOption = {
					title : {
						text : '容器内存使用监控',
						subtext : '单位：MB'
					},
					tooltip : {
						show : true
					},
					legend : {
						data : [ '内存负载' ]
					},
					xAxis : [ {
						type : 'category',
						data : dates,
						splitNumber : 40
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel : {
							formatter : '{value} MB'
						}
					} ],
					series : [ {
						'name' : '内存负载',
						'type' : 'line',
						'data' : mems,
						'smooth' : true
					} ]
				};
				var netOption = {
					title : {
						text : '容器网络IO监控',
						subtext : '网络包单位：KB/S'
					},
					tooltip : {
						show : true
					},
					legend : {
						data : [ '网络收包速率', '网络发包速率' ]
					},
					xAxis : [ {
						type : 'category',
						data : dates,
						splitNumber : 40
					} ],
					yAxis : [ {
						type : 'value',
						axisLabel : {
							formatter : '{value} KB/S'
						}
					} ],
					series : [ {
						'name' : '网络收包速率',
						'type' : 'line',
						'data' : nins,
						'smooth' : true
					}, {
						'name' : '网络发包速率',
						'type' : 'line',
						'data' : nous,
						'smooth' : true
					} ]
				};
			cpuChart.setOption(cpuOption);
			cpuChart.hideLoading();
			memChart.setOption(memOption);
			memChart.hideLoading();
			netChart.setOption(netOption);
			netChart.hideLoading();
		});

	}

	//
//	function dynamicGetData(host,cpuChart,memChart,netChart) {
//		$.getJSON(base + 'monitor/host/' + host, function(json) {
//			// date = new Date().toLocaleTimeString().replace(/^\D*/,'');
//			date = new Date().pattern("HH:mm:ss");
//			cpu = json.cpu;
//			mem = json.mem;
//			nin = json.nin;
//			nou = json.nou;
//		});
//		
//	}
	
	
});