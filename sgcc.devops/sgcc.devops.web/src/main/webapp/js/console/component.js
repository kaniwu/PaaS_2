var grid_selector = "#component_list";
var page_selector = "#component_page";
jQuery(function($) {
	$(window).on('resize.jqGrid', function() {
		$(grid_selector).jqGrid('setGridWidth', $(".page-content").width());
		$(grid_selector).closest(".ui-jqgrid-bdiv").css({ 'overflow-x': 'hidden' });
	});
	var parent_column = $(grid_selector).closest('[class*="col-"]');
	$(document).on('settings.ace.jqGrid' , function(ev, event_name, collapsed) {
		if( event_name === 'sidebar_collapsed' || event_name === 'main_container_fixed' ) {
			setTimeout(function() {
				$(grid_selector).jqGrid( 'setGridWidth', parent_column.width() );
			}, 0);
		}
    });
	jQuery(grid_selector).jqGrid({
		url : base+'component/list?componentType=0',
		datatype : "json",
		height : '100%',
		autowidth: true,
		colNames : ['组件ID','组件类型','组件名称','地址','','','','','','','','','','','','','','','','','','','','','','状态','说明','快捷操作'],
		colModel : [ 
			{name : 'componentId',index : 'componentId',width : 10,hidden:true},
			{name : 'componentTypeValue',index : 'componentTypeValue',width : 7,
				formatter:function(cellvalue,options,rowObject){
					return getComType(rowObject.componentType);
			}},
			{name : 'componentName',index : 'componentName',width : 15},
			{name : 'componentIp',index : 'componentIp',width : 18},
			
			{name : 'componentType',index : 'componentType',width : 18,hidden:true},
			{name : 'componentConfigDir',index : 'componentConfigDir',width : 18,hidden:true},
			{name : 'componentDriver',index : 'componentDriver',width : 18,hidden:true},
			{name : 'componentUserName',index : 'componentUserName',width : 18,hidden:true},
			{name : 'componentHostPort',index : 'componentHostPort',width : 18,hidden:true},
			{name : 'componentPwd',index : 'componentPwd',width : 18,hidden:true},
			{name : 'componentDBType',index : 'componentDBType',width : 18,hidden:true},
			{name : 'dnsZone',index : 'dnsZone',width : 18,hidden:true},
			{name : 'key',index : 'key',width : 18,hidden:true},
			{name : 'secret',index : 'secret',width : 18,hidden:true},
			{name : 'autoConfigF5',index : 'autoConfigF5',width : 18,hidden:true},
			{name : 'bigipVersion',index : 'bigipVersion',width : 18,hidden:true},
			{name : 'driverId',index : 'driverId',width : 18,hidden:true},
			{name : 'driverDesc',index : 'driverDesc',width : 18,hidden:true,
				formatter:function(cellvalue,options,rowObject){
				return getDriverDesc(rowObject.driverId,rowObject.componentType);
			}
			},
			{name : 'initSql',index : 'initSql',width : 18,hidden:true},
			{name : 'f5ExternalIp',index : 'f5ExternalIp',width : 18,hidden:true},
			{name : 'f5ExternalPort',index : 'f5ExternalPort',width : 18,hidden:true},
			{name : 'f5CompId',index : 'f5CompId',width : 18,hidden:true},
			{name : 'backComponentIp',index : 'backComponentIp',width : 18,hidden:true},
			{name : 'keepaliveIp',index : 'keepaliveIp',width : 18,hidden:true},
			{name : 'nginxCategory',index : 'nginxCategory',width : 18,hidden:true},
			{name : 'componentStatus',index : 'componentStatus',width : 10,hidden:true,
				formatter:function(cellvalue,options,rowObject){
					switch(rowObject.componentStatus){
					case 0:
						return '删除';
					case 1:
						return '正常';
					case 2:
						return '未知';
					}
				}
			},
			{name : 'componentDescription',index : 'componentDescription',width : 15,sortable : false,
				formatter:function(cellvalue,options,rowObject){
					return stringTool.escape(rowObject.componentDescription);
				}
			},		
			{
		    	name : '',
				index : '',
				width : 200,
				align :'center',
				fixed : true,
				sortable : false,
				resize : false,
				title : false,
				formatter : function(cellvalue, options,rowObject) {
					rowObject.componentDescription = stringTool.escape(rowObject.componentDescription);
					return  "<a href='#modal-wizard' data-toggle='modal' onclick='querySystems(\""
							+rowObject.componentId+"\",\""+rowObject.componentName+"\",\""+rowObject.componentType
							+"\")' class='btn btn-xs btn-info btn-round' ><i class='ace-icon fa fa-search bigger-125'></i><b>关联物理系统</b></a> &nbsp;"
							+"<button class=\"btn btn-xs btn-success btn-round\" onclick=\"connectTest('"+rowObject.componentId+"')\"><i class=\"ace-icon fa fa-tasks bigger-125\"></i><b>测试</b></button> &nbsp;";
				}
		    }
		],
		viewrecords : true,
		rowNum : 10,
		rowList : [ 10,20,50,100,1000 ],
		pager : page_selector,
		altRows : true,
		sortname: 'componentTypeValue',
		sortname: 'componentName',
		sortname: 'componentIp',
		sortorder: "asc",
		multiselect: true,
		multiboxonly:true,
		jsonReader: {
			root: "rows",
			total: "total",
			page: "page",
			records: "records",
			repeatitems: false
		},
		loadError:function(resp){
			if(resp.responseText.indexOf("会话超时") > 0 ){
				alert('会话超时，请重新登录！');
				document.location.href='/login.html';
			}
		},
		loadComplete : function() {
			var table = this;
			setTimeout(function() {
				styleCheckbox(table);
				updateActionIcons(table);
				updatePagerIcons(table);
				enableTooltips(table);
			}, 0);
		}
	});
	$(window).triggerHandler('resize.jqGrid');// 窗口resize时重新resize表格，使其变成合适的大小
	jQuery(grid_selector).jqGrid(//分页栏按钮
			'navGrid',
			page_selector,
			{ // navbar options
				edit : false,
				add : false,
				del : false,
				search : false,
				refresh : true,
				refreshstate :'current',
				refreshicon : 'ace-icon fa fa-refresh red',
				view : false
			},{},{},{},{},{}
	);

	function updateActionIcons(table) {
	}
	function updatePagerIcons(table) {
		var replacement = {
			'ui-icon-seek-first' : 'ace-icon fa fa-angle-double-left bigger-140',
			'ui-icon-seek-prev' : 'ace-icon fa fa-angle-left bigger-140',
			'ui-icon-seek-next' : 'ace-icon fa fa-angle-right bigger-140',
			'ui-icon-seek-end' : 'ace-icon fa fa-angle-double-right bigger-140'
		};
		$('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon')
			.each(function() {
				var icon = $(this);
				var $class = $.trim(icon.attr('class').replace('ui-icon', ''));
				if ($class in replacement)
					icon.attr('class', 'ui-icon '+ replacement[$class]);
			});
	}

	function enableTooltips(table) {
		$('.navtable .ui-pg-button').tooltip({
			container : 'body'
		});
		$(table).find('.ui-pg-div').tooltip({
			container : 'body'
		});
	}
	function styleCheckbox(table) {
//		$(table).find('input:checkbox').addClass('ace')
//		.wrap('<label />')
//		.after('<span class="lbl align-top" />')
//		$('.ui-jqgrid-labels th[id*="_cb"]:first-child')
//		.find('input.cbox[type=checkbox]').addClass('ace')
//		.wrap('<label />').after('<span class="lbl align-top" />');
	}

});

function removeComponent(componentId){
	url = base+"component/delete";
	data = {componentId:componentId};
	bootbox.confirm("<b>你确定要删除此组件吗?</b>", function(result) {
		if(result) {
			$.post(url,data,function(response){
				showMessage(result.message);
				if(response.success){
					$(grid_selector).trigger("reloadGrid");
				}
			});
		}
	});
}

//组件可达性测试
function connectTest(componentId){
	url = base + 'component/connectTest?componentId=' + componentId;
	$("#spinner").css("display","block");
	$.get(url,function(response){
		$("#spinner").css("display","none");
		if(response.success||componentType==7){
			showMessage(response.message);
		}else{
			showMessage(response.message);
		}
	});
}


function showCreateComponentWin(){
	initNginxHostList("");
	var options="";
	url = base+'componentType/selectAll';
	$.get(url,function(response){
		var datas=response.params;
		for(var i=0;i<datas.length;i++){
			var componentType=datas[i];
			option="<option value='"+componentType.componentTypeId+"'>"+componentType.componentTypeName+"</option>";
			options+=option;
		}
		bootbox.dialog({
			title : "<b>添加组件</b>",
			message : "<div class='well ' style='margin-top:1px;padding-bottom:1px;'>"+
							"<form class='form-horizontal' role='form' id='create_component_form'>"+
								"<div class='form-group' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b>组件类型：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
					  					"<select id='component_type' name='component_type' class='form-control' onchange='showExtend()'>"+
						  						options+
						  					"</select>"+
					  				"</div>"+
					  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
					  			"</div>"+
					  			"<div class='form-group' id='comp_name' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='name'>组件名称：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      					"<input id=\"component_name\" name='component_name' type='text' class=\"form-control\" placeholder='请输入组件名称' onblur='checkComName();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' style='margin-bottom: 5px;display: none' id='db_type'>"+
					  				"<label class='col-sm-3'><div align='right'><b>数据库类型：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
					  				"<select id='component_db_type' name='component_db_type' class='form-control' onclick='check()' onchange='initDriverName()'>"+
					  						"<option value=''>请选择</option>"+
					  						"<option value='0'>ORACLE</option>"+
					  						"<option value='1'>MYSQL</option>"+
					  						"<option value='2'>DB2</option>"+
					  						"<option value='3'>ISOLATION</option>"+
					  						"<option value='4'>SQLSERVER</option>"+
					  						"<option value='5'>ORACLE_XA</option>"+
						  				"</select>"+
					  				"</div>"+
					  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
					  			"</div>"+
					  			
					  			
					  			"<div class='form-group' style='display: none;margin-bottom: 5px;' id='driverOrder'>"+
		  							"<label class='col-sm-3'><div align='right'><b>驱动程序：</b></div></label>"+
		  							"<div class='col-sm-9'>"+
		  							"<input id=\"component_driver_id\" name='component_driver_id' type='hidden' value='' class=\"form-control\"/>"+
		  							"<input id=\"component_driver_desc\" name='component_driver_desc' type='text' value='' class=\"form-control\" readonly=\"readonly\" />"+
		  							"</div>"+
		  							"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		  						"</div>"+
					  			
		    	      			"<div class='form-group' style='display: none;margin-bottom: 5px;' id='driver'>"+
					  				"<label class='col-sm-3'><div align='right'><b>驱动名称：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"component_driver\" name='component_driver' type='text' class=\"form-control\" readonly=\"readonly\" placeholder='请输入驱动名称'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' id='nginxCategoryDiv' style=''>"+
			  						"<label class='col-sm-3' style='padding-left: 0px;'><div align='right'><b>使用方式：</b></div></label>"+
			  						"<div class='col-sm-9' style='margin-top: 5px;'>"+
			  						"<input id=\"nginx_category_system\" name='nginx_category' type='radio' value='1' class=\"ace\" checked=\"checked\" /><span class=\"lbl\"> <b>系统负载</b></span>"+
			  						"<input id=\"nginx_category_registry\" name='nginx_category' type='radio' value='2' class=\"ace\" /><span class=\"lbl\" style='margin-left: 10px;'> <b>仓库负载</b></span>"+
			  						"</div>"+
			  						"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			  					"</div>"+
		    	      			"<div class='form-group' id='ip' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>IP地址：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"component_ip\" name='component_ip' type='text' class=\"form-control\" placeholder='请输入IP地址' onblur='checkIp();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' id='component_host_port_div' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>主机端口号：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"component_host_port\" name='component_host_port' type='text' class=\"form-control\" value=\"22\" />"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' id='back_component_ip_div' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>热备从IP地址：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"back_component_ip\" name='back_component_ip' type='text' class=\"form-control\" placeholder=''/>"+
		    	      			"</div>"+
	    	      				"</div>"+
		    	      				"<div class='form-group' id='keepalive_ip_div' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>热备keepaliveIp地址：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"keepalive_ip\" name='keepalive_ip' type='text' class=\"form-control\" placeholder=''/>"+
		    	      			"</div>"+
	    	      				"</div>"+
		    	      			"<div class='form-group' id='comconfig' style='display: none;'>"+
			  						"<label class='col-sm-3' style='padding-left: 0px;'><div align='right'><b id='isConfig'>自动配置F5：</b></div></label>"+
			  						"<div class='col-sm-9' style='margin-top: 5px;'>"+
			  						"<input id=\"auto_config_f5_yes\" name='auto_config_f5' type='radio' value='1' class=\"ace\" onclick='showInfo();'/><span class=\"lbl\"> <b>是</b></span>"+
			  						"<input id=\"auto_config_f5_no\" name='auto_config_f5' type='radio' value='0' class=\"ace\"  checked=\"checked\" onclick='hideInfo();'/><span class=\"lbl\" style='margin-left: 10px;'> <b>否</b></span>"+
			  						"</div>"+
			  						"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			  					"</div>"+
		    	      			"<div class='form-group' id='config' style='margin-bottom: 5px;'>"+
				  					"<label class='col-sm-3'><div align='right'><b>配置文件目录：</b></div></label>"+
				  					"<div class='col-sm-9'>"+
	    	      					"<input id='component_config_dir' name='component_config_dir' type='text' class=\"form-control\" placeholder='请输入配置文件目录' value='/etc/nginx' onblur='checkManage();'/>"+
	    	      					"</div>"+
	    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	    	      				"</div>"+	
	    	      				"<div class='form-group' id='dns_zone_div' style='margin-bottom: 5px;display: none;'>"+
				  					"<label class='col-sm-3'><div align='right'><b>Zone值：</b></div></label>"+
				  					"<div class='col-sm-9'>"+
	    	      					"<input id='dns_zone' name='dns_zone' type='text' class=\"form-control\" placeholder='请输入DNS的Zone值' value='' onblur='checkZone();'/>"+
	    	      					"</div>"+
	    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
    	      					"</div>"+
    	      					"<div class='form-group' id='key_div' style='margin-bottom: 5px;display: none;'>"+
				  					"<label class='col-sm-3'><div align='right'><b>key值：</b></div></label>"+
				  					"<div class='col-sm-9'>"+
	    	      					"<input id='key' name='key' type='text' class=\"form-control\" placeholder='请输入Zone的key值' value='' onblur='checkZoneKey();'/>"+
	    	      					"</div>"+
	    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		      					"</div>"+
	      						"<div class='form-group' id='secret_div' style='margin-bottom: 5px;display: none;'>"+
				  					"<label class='col-sm-3'><div align='right'><b>MD5的secret值：</b></div></label>"+
				  					"<div class='col-sm-9'>"+
				  					"<textarea id=\"secret\"  name=\"secret\" class=\"form-control\" rows=\"3\" onblur='checkZoneSecret();' placeholder='请输入Zone更新MD5的secret值'></textarea>"+
			      					"</div>"+
			      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		      					"</div>"+
		    	      			"<div class='form-group' id='username' style='margin-bottom: 5px;'>"+ 
					  				"<label class='col-sm-3'><div align='right'><b>用戶名：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"component_userName\" name='component_userName' type='text' class=\"form-control\" placeholder='请输入用户名' onblur='checkUser();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' id='password' style='margin-bottom: 5px;'>"+
					  				"<label class='col-sm-3'><div align='right'><b>密码：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"component_pwd\" name='component_pwd' type='password' class=\"form-control\" placeholder='请输入密码' onblur='checkPwd();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			
		    	      			"<div class='form-group' id='versions' style='margin-bottom: 5px;display: none;'>"+
	  			   					"<label class='col-sm-3'><div align='right'><b>BIGIP版本：</b></div></label>"+
	  			   					"<div class='col-sm-9'>"+
	  			   					"<input id=\'bigip_version\' name='bigip_version' type='text' class=\"form-control\" placeholder='请输入BIGIP版本' onblur='checkfVersions();'/>"+
	  			   					"</div>"+
	  			   					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	  			   				"</div>"+

	    	      				/*nginx组 类型 差异字段 start by yueyong*/
								"<div class='form-group' style='margin-bottom: 5px;display: none' id='f5_comp_id_div'>"+
					  				"<label class='col-sm-3'><div align='right'><b>F5组件：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
					  					"<select id='f5_comp_id' name='f5_comp_id' class='form-control' onchange='checkF5Comp();'>"+
						  					"</select>"+
					  				"</div>"+
					  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
					  			"</div>"+
								"<div class='form-group' style='margin-bottom: 5px;display: none' id='autoConfigTextDiv'>"+
					  				"<label class='col-sm-3'><div align='right'><b>自动配置F5：</b></div></label>"+
					  				"<div class='col-sm-9' style='line-height:29px;'>"+
					  				"</div>"+
					  			"</div>"+
		    	      			"<div class='form-group' style='margin-bottom: 5px;display: none' id='f5_external_ip_div'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>F5对外IP：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"f5_external_ip\" name='f5_external_ip' type='text' class=\"form-control\" placeholder='请输入F5对外IP地址' onblur='checkF5Ip();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
		    	      			"<div class='form-group' style='margin-bottom: 5px;display: none' id='f5_external_port_div'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>F5对外端口：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"f5_external_port\" name='f5_external_port' type='number' class=\"form-control\" placeholder='请输入F5对外端口' onblur='checkF5Port();'/>"+
		    	      				"</div>"+
		    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      			"</div>"+
					  			/*nginx组 类型 差异字段 end by yueyong*/
		    	      			
		    	      			"<div class='form-group hide' style='margin-bottom: 5px;display: none' id='init_sql_div'>"+
					  				"<label class='col-sm-3'><div align='right'><b id='ipName'>初始化脚本：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      				"<input id=\"init_sql\" name='init_sql' type='text' class=\"form-control\" placeholder='<init-sql>属性配置' />"+
		    	      				"</div>"+
	    	      				"</div>"+
	  			   				
				      			"<div class='form-group' style='margin-bottom: 5px;'>"+
				      				"<label class='col-sm-3'><div align='right'><b>说明：</b></div></label>"+
					  				"<div class='col-sm-9'>"+
		    	      					"<textarea id=\"component_description\" onblur=\"validToolObj.length('#component_description',200)\" name=\"component_description\" class=\"form-control\" rows=\"3\"></textarea>"+
		    	      				"</div>"+
		    	      			"</div>"+
	  			   				
				      			"<div class='form-group' style='margin-bottom: 5px;text-align: right;margin-right:4px;display:none' id='nginxHostMgr'>"+
					  				"<div class='col-sm-12'>"+
					  					"<a href='#modal-hostlist' data-toggle='modal'>"+
					  					"<button class='btn btn-sm btn-warning btn-round' type='button' onclick='nginxHostMgr(\"\");'><i class='ace-icon fa fa-pencil-square-o bigger-125'></i> <b id='countTextB'>维护Nginx主机(0)</b></button>"+
					  					"</a>"+
		    	      				"</div>"+
		    	      			"</div>"+
		    	      		"</form>"+
		    	      	"</div>",
			buttons : {
				"success" : {
					"label" : "<i class='icon-ok' id='saveButt'></i> <b>保存</b>",
					"className" : "btn-sm btn-danger btn-round",
					"callback" : function() {
						    	    var componentName = $('#component_name').val();
						            var componentIp = $('#component_ip').val();
						            var componentType = $('#component_type').val();
						            var componentDescription = $('#component_description').val();
						            var component_userName = $('#component_userName').val();
						            var component_pwd = $('#component_pwd').val();
						            var component_config_dir=$('#component_config_dir').val();
						            var component_driver=$('#component_driver').val();
						            var componentDBType = $('#component_db_type').val();
//						            var auto_config_f5 = $('#auto_config_f5').val();
						            var auto_config_f5 = $("input[name=auto_config_f5]:checked").val();
						            var bigip_version = $('#bigip_version').val();
						            var component_driver_id = $('#component_driver_id').val();
						            var init_sql = $('#init_sql').val();
						            var f5CompId = $("#f5_comp_id").val();
						            if(f5CompId!=null && f5CompId.length>0){
						            	f5CompId = f5CompId.split(":")[0];
						            }
						            var nginxCategory = $("input[name=nginx_category]:checked").val();
						            var componentHostPort = $("#component_host_port").val();
						            var f5ExternalIp = $("#f5_external_ip").val();
						            var f5ExternalPort = $("#f5_external_port").val();
						            var nginxIns ="";
						            var componentHostInfo='';
						            var dnsZone = $('#dns_zone').val();
						            var key = $('#key').val();
						            var secret = $('#secret').val();
						            
						            var backComponentIp = $("#back_component_ip").val();
						        	var keepaliveIp = $("#keepalive_ip").val();
						        	
						            if(componentType==6){
						            	if(getNginxHostCount()<=0){
						            		showMessageHard("请维护Nginx主机列表！");
							            	return false;
						            	}
						            	componentHostInfo = joinNginxHostStr();
						            }
						            
						            var data={
											componentName: componentName,
											componentIp: componentIp,
											componentType: componentType,
											componentDescription: componentDescription,
											componentPwd:component_pwd,
											componentUserName:component_userName,
											componentConfigDir:component_config_dir,
											componentDriver:component_driver,
											componentDBType:componentDBType,
											"componentExpand.autoConfigF5":auto_config_f5,
											"componentExpand.bigipVersion":bigip_version,
											"componentExpand.driverId":component_driver_id,
											"componentExpand.initSql":init_sql,
											"componentExpand.f5CompId":f5CompId,
											"componentExpand.f5ExternalIp":f5ExternalIp,
											"componentExpand.f5ExternalPort":f5ExternalPort,
											componentHostInfo:componentHostInfo,
											nginxIns:nginxIns,
											dnsZone:dnsZone,
											key:key,
											secret:secret,
											backComponentIp:backComponentIp,
											keepaliveIp : keepaliveIp,
											componentHostPort:componentHostPort,
											nginxCategory:nginxCategory
									};
						            var url = base + 'component/connectTest';
						        	$("#spinner").css("display","block");
						        	$.get(url,data,function(response){
						        		$("#spinner").css("display","none");
						        		if(response.success||componentType==7){
								       		if(componentType==1){
								       			data2={
								       					hostIp:componentIp,
								       					hostName:component_userName,
								       					hostPassword:component_pwd,
								       					type:"nginx",
								       					hostPort:componentHostPort
								       			};
								       			$("#spinner").css("display","block");
								       		  var url2 = base + 'host/getVersion';
								       			$.post(url2,data2,function(response){
								       			$("#spinner").css("display","none");
							        			bootbox.dialog({
									        	    message:"<div class='well ' style='margin-top:1px;padding-bottom:1px;'>"+
													"<form class='form-horizontal' role='form' >"+ 
													"<div class='form-group'  >"+
													"<label class='col-sm-4'><div align='right'><b>nginx当前版本：</b></div></label>"+
													 "<div class='col-sm-8'>"+
						  				         	"<input  type='text' value='"+response.nginxVersion+"' class=\"form-control\" readonly=‘readonly’/>"+
						  			        	    "</div>"+
													"</div>"+
									        	    "<div class='form-group' id='nginxIns' >"+
								  				       "<label class='col-sm-4'><div align='right'><b>nginx安装：</b></div></label>"+
								  				        "<div class='col-sm-8'>"+
								  				         	"<select id='nginxIns_select' name='nginxIns_select' class='form-control' >"+
									  					       "</select>"+
								  			        	"</div>"+
							      			         "</div>"+
								  			        "</form>"+
									    	      	"</div><script>getNginx()</script>",
									        	    title: "提示",
									        	    buttons: {
									        	    	"success" : {
									    					"label" : "<i class='icon-ok' id='saveButt'></i> <b>保存</b>",
									    					"className" : "btn-sm btn-danger btn-round",
									    					"callback" : function() {
									    						var nginxIns = $("#nginxIns_select").val();
									    						var data1={
																		componentName: componentName,
																		componentIp: componentIp,
																		componentType: componentType,
																		componentDescription: componentDescription,
																		componentPwd:component_pwd,
																		componentUserName:component_userName,
																		componentConfigDir:component_config_dir,
																		componentDriver:component_driver,
																		componentDBType:componentDBType,
																		"componentExpand.autoConfigF5":auto_config_f5,
																		"componentExpand.bigipVersion":bigip_version,
																		"componentExpand.driverId":component_driver_id,
																		"componentExpand.initSql":init_sql,
																		"componentExpand.f5CompId":f5CompId,
																		"componentExpand.f5ExternalIp":f5ExternalIp,
																		"componentExpand.f5ExternalPort":f5ExternalPort,
																		componentHostInfo:componentHostInfo,
																		nginxIns:nginxIns,
																		backComponentIp:backComponentIp,
																		keepaliveIp : keepaliveIp,
																		componentHostPort:componentHostPort,
																		nginxCategory:nginxCategory
																};
									    						var url=base+"component/create";
																$.post(url,data1,function(response){
																	showMessage(response.message,function(){
																		$(grid_selector).trigger("reloadGrid");
																	});
																});
									    					}
									        	    	},
							        			     "cancel" : {
							     					       "label" : "<i class='icon-info'></i> <b>取消</b>",
							     					         "className" : "btn-sm btn-warning btn-round",
							     					        "callback" : function() {}
							     				    }
							        		     }
							        			});
								       			});
							        		}
								       		else{
								       		var url=base+"component/create";
											$.post(url,data,function(response){
												showMessage(response.message,function(){
													$(grid_selector).trigger("reloadGrid");
												});
											});
								       		}
						        		}else{
						        			bootbox.dialog({
								        	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;连接测试失败，是否继续保存？</div>',
								        	    title: "提示",
								        	    buttons: {
								        	        main: {
								        	        	label: "<i class='icon-info'></i><b>是</b>",
								                        className: "btn-sm btn-success btn-round",
								        	            callback: function () {
															var url=base+"component/create";
															$.post(url,data,function(response){
																showMessage(response.message,function(){
																	$(grid_selector).trigger("reloadGrid");
																});
															});
								        	            }
								        	        },
								        	        cancel: {
								        	        	label: "<i class='icon-info'></i> <b>否</b>",
								                        className: "btn-sm btn-danger btn-round",
								        	            callback: function () {
								        	            }
								        	        }
								        	    }
								        	});
						        		}
						        	});
						    }
						   
				},
				    "cancel" : {
					"label" : "<i class='icon-info'></i> <b>取消</b>",
					"className" : "btn-sm btn-warning btn-round",
					"callback" : function() {}
				}
			}
		});
		$("#component_db_type").on('change',function(){
			if("0"==this.value){
				$("#component_driver").val("oracle.jdbc.driver.OracleDriver");
				$("#component_ip").attr('placeholder','jdbc:oracle:thin:@<host>:<port>:<SID>');
			}
			if("1"==this.value){
				$("#component_driver").val("com.mysql.jdbc.Driver");
				$("#component_ip").attr('placeholder','jdbc:mysql://<host>:<port>/<databasename>');
			}
			if("2"==this.value){
				$("#component_driver").val("com.ibm.db2.jcc.DB2Driver");
				$("#component_ip").attr('placeholder','jdbc:db2://<host>:<port>/<databasename>');
			}
			if("3"==this.value){
				$("#component_driver").val("sgcc.nds.jdbc.driver.NdsDriver");
				$("#component_ip").attr('placeholder','jdbc:db2://<host>:<port>/<databasename>');
			}
			if("4"==this.value){
				$("#component_driver").val("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				$("#component_ip").attr('placeholder','jdbc:sqlserver://<host>:<port>;DatabaseName=<databasename>');
			}
			if("5"==this.value){
				$("#component_driver").val("oracle.jdbc.xa.client.OracleXADataSource");
				$("#component_ip").attr('placeholder','jdbc:oracle:thin:@<host>:<port>:<SID>');
			}
			if(component_db_type==3){
				$("#component_ip").attr('placeholder','');
			}
		});
		$("#saveButt").parent().attr("disabled","disabled");
	});
}

/**数据校验 begin*/

//组件名称
function checkComName(){
	validToolObj.isNull('#component_name') || validToolObj.validName('#component_name') || validToolObj.realTimeValid(base + 'component/checkName',$('#component_id').val(),'#component_name');
	check();
}

//Ip地址
function checkIp(){
	validToolObj.isNull('#component_ip',false);
	check();
}

//文件目录
function checkManage(){
	validToolObj.isNull('#component_config_dir');
	check();
}
//DNSzone
function checkZone(){
	validToolObj.isNull('#dns_zone');
	check();
}
function checkZoneKey(){
	validToolObj.isNull('#key');
	check();
}
function checkZoneSecret(){
	validToolObj.isNull('#secret',true,2000);
	check();
}
//用户名 校验
function checkUser(){
	validToolObj.isNull('#component_userName') || validToolObj.validUserName('#component_userName');
	check();
}

//密码校验
function checkPwd(){
	validToolObj.isNull('#component_pwd',true,48);
	check();
}

//驱动校验
function checkDriver(){
	validToolObj.isNull('#component_driver',false);
	check();
}

//数据库地址
function checkDbUrl(){
	 var componentIp = $('#component_ip').val();
	 var help= $(".componentIp").text();
		if(isNull(componentIp)){
	    	if(isNull(help)){
	    		var tip="<span class='help-block componentIp' style='color:#FF0000;'>格式不正确</span>" ;
	    		$('#component_ip').parent('div').append(tip);
	    	}
	    	check();
	        return false;
	    }else{
	    	$(".componentIp").remove();
	    	check();
	    	return true;
	    }	
}

//F5对外IP校验
function checkF5Ip(){
	validToolObj.isNull('#f5_external_ip',false) || validToolObj.isIp('#f5_external_ip');
	check();
}

//F5对外端口校验
function checkF5Port(){
	validToolObj.isNull('#f5_external_port',false) || validToolObj.isPort('#f5_external_port');
	check();
}
//DB Driver_DESC获取
function getDriverDesc(driverId,componentType){
	var driverDesc="";
	if(componentType == 3 && driverId != null){
		var url = base + 'component/selectDriverDesc';
		$.ajax({
	        type: 'get',
	        url:url,
	        data:{
	        	driverId : driverId
	        },
	        async:false,
	        dataType: 'json',
	        success: function (response) {
	        	driverDesc=response.driver.driverDesc;
	        }
		});
	}
	
	return driverDesc;
}

//选择数据库后的联动
function initDriverName(){
	var componentDriverId = $('#component_driver_id');
	var componentDriverDesc = $('#component_driver_desc');
	var DBType = $('#component_db_type').val();
	$.ajax({
        type: 'get',
        url: base+'driver/getDatabaseDriver',
        dataType: 'json',
        data:{DBType:DBType},
        success: function (response) {
        	if(response.driver != null){
        		var driverId = response.driver.id;
            	var driverDesc = response.driver.driverDesc;
            	componentDriverId.val(driverId);
            	componentDriverDesc.val(driverDesc);
        	}else{
        		componentDriverId.val("");
            	componentDriverDesc.val("");
        	}
        }
	});
}

//数据提交校验
function check(){
	validToolObj.validForm('#create_component_form',"#saveButt",['#component_name','#component_ip','#component_type','#component_db_type','#component_userName','#component_pwd','#component_config_dir','#component_driver','#f5_external_ip','#f5_external_port']);
	
//	var name = $('#component_name').val();
//    var ip = $('#component_ip').val();
//    var componentType = $('#component_type').val();
//    var componentDBType = $('#component_db_type').val();
//    var userName = $('#component_userName').val();
//    var pwd = $('#component_pwd').val();
//    var config_dir=$('#component_config_dir').val();
//    var driver=$('#component_driver').val();
//    var result=false;
//    if(componentType==1||componentType==4){		
//		if(isNull(name)||isNull(config_dir)||isNull(ip)||!isIp(ip)||isNull(userName)||isNull(pwd)){
//		}else{
//			result=true;
//		}
//	}else if(componentType==2){
//		if(isNull(name)||isNull(ip)||!isIp(ip)){
//		}else{
//			result=true;
//		}	
//	}else if(componentType==3){
//		if(isNull(name)||isNull(ip)||isNull(driver)||isNull(userName)||isNull(pwd)||isNull(componentDBType)){
//		}else{
//			result=true;
//		}	
//	}else if(componentType==5){
//		if(isNull(config_dir)||isNull(ip)||!isIp(ip)||isNull(userName)||isNull(pwd)){
//		}else{
//			result=true;
//		}	
//	}
//    
//    if(result){
//    	$("#saveButt").parent().removeAttr("disabled");
//    }else{
//    	$("#saveButt").parent().attr("disabled","disabled");
//    }
}

/**数据校验 end*/

function showExtend(){
	
	var ipName=$("#ipName");
	var name=$("#name");
	var driver=$("#driver");
	var config=$("#config");
	var dbtype=$('#db_type');
	var driverOrder=$('#driverOrder');
	var username=$("#username");
	var password=$("#password");
	var configf=$("#comconfig");
	var versions=$("#versions");
	var nocheckyes=$("#auto_config_f5_no");
	var ipDiv = $("#ip");
	var f5CompDiv = $("#f5_comp_id_div");
	var f5ExternalIpDiv = $("#f5_external_ip_div");
	var f5ExternalPortDiv = $("#f5_external_port_div");
	var autoConfigTextDiv = $("#autoConfigTextDiv");
	var nginxHostMgr = $("#nginxHostMgr");
	var initSqlDiv = $("#init_sql_div");
	var nginxIns = $("#nginxIns");
	var dns_zone_div = $("#dns_zone_div");
	var key_div =$("#key_div");
	var secret_div = $("#secret_div");
	$("#component_name").attr('placeholder','请输入组件名称');
	$("#component_ip").attr('placeholder','请输入IP地址');
	var comp_name=$("#comp_name");
	var componentType=$("#component_type").val();
	var backComponentIpDiv = $("#back_component_ip_div");
	var keepaliveIpDiv = $("#keepalive_ip_div");
	var nginxCategoryDiv = $("#nginxCategoryDiv");
	if(componentType==1||componentType==4){
		$("#component_config_dir").val("/etc/nginx");
		comp_name.show();
		name.text("组件名称：");
		ipName.text("IP地址：");
		config.show();
		username.show();
		password.show();
		driver.hide();
		ipDiv.show();
		dbtype.hide();
		driverOrder.hide();
		configf.hide();
		versions.hide();
		f5CompDiv.hide();
		f5ExternalIpDiv.hide();
		f5ExternalPortDiv.hide();
		autoConfigTextDiv.hide();
		nginxHostMgr.hide();
		initSqlDiv.hide();
		nginxIns.show();
		getNginx();
		dns_zone_div.hide();
		key_div.hide();
		secret_div.hide();
		backComponentIpDiv.show();
		keepaliveIpDiv.show();
		$("#component_ip").unbind('blur');
		$("#component_ip").bind('blur',function(){
			checkIp();
		});
		nginxCategoryDiv.show();
	}else if(componentType==2){
		comp_name.show();
		$("#component_ip").attr('placeholder','请输入管理IP');
		name.text("组件名称：");
		ipName.text("管理IP：");
		username.hide();
		password.hide();
		config.hide();
		driver.hide();
		dbtype.hide();
		driverOrder.hide();
		configf.show();
		ipDiv.show();
		f5CompDiv.hide();
		f5ExternalIpDiv.hide();
		f5ExternalPortDiv.hide();
		autoConfigTextDiv.hide();
		nginxHostMgr.hide();
		initSqlDiv.hide();
		nginxIns.hide();
		dns_zone_div.hide();
		key_div.hide();
		secret_div.hide();
		backComponentIpDiv.hide();
		keepaliveIpDiv.hide();
		nginxCategoryDiv.hide();
		$("#component_ip").unbind();
		$("#component_ip").bind('blur',function(){
			checkIp();
		});
		
	}else if(componentType==3){
		config.hide();
		comp_name.show();
		configf.hide();
		versions.hide();
		ipDiv.show();
		name.text("数据库名称：");
		ipName.text("数据库地址：");
		$("#component_name").attr('placeholder','请输入数据库名称');
		var component_db_type = $("#component_db_type").val();
		//oracle
		if(component_db_type==0){
			$("#component_ip").attr('placeholder','jdbc:oracle:thin:@<host>:<port>:<SID>');
		}
		//mysql
		if(component_db_type==1){
			$("#component_ip").attr('placeholder','jdbc:mysql://<host>:<port>/<databasename>');
		}
		//db2
		if(component_db_type==2){
			$("#component_ip").attr('placeholder','jdbc:db2://<host>:<port>/<databasename>');
		}
		//
		if(component_db_type==3){
			$("#component_ip").attr('placeholder','');
		}
		if(component_db_type==4){
			$("#component_ip").attr('placeholder','jdbc:sqlserver://<host>:<port>;DatabaseName=<databasename>');
		}
		driver.show();
		username.show();
		password.show();
		
		dbtype.show();
		driverOrder.show();
		f5CompDiv.hide();
		f5ExternalIpDiv.hide();
		f5ExternalPortDiv.hide();
		autoConfigTextDiv.hide();
		nginxHostMgr.hide();
		initSqlDiv.show();
		nginxIns.hide();
		dns_zone_div.hide();
		key_div.hide();
		secret_div.hide();
		backComponentIpDiv.hide();
		keepaliveIpDiv.hide();
		nginxCategoryDiv.hide();
		$("#component_ip").unbind();
		$("#component_ip").bind('blur',function(){
			checkDbUrl();
		});
		
	}else if(componentType==5){
		$("#component_config_dir").val("");
		//$("#component_config_dir").hide();
		configf.hide();
		versions.hide();
		ipName.text("IP地址：");
		config.hide();
		username.show();
		password.show();
		dbtype.hide();
		driverOrder.hide();
		ipDiv.show();
		f5CompDiv.hide();
		f5ExternalIpDiv.hide();
		f5ExternalPortDiv.hide();
		autoConfigTextDiv.hide();
		nginxHostMgr.hide();
		initSqlDiv.hide();
		nginxIns.hide();
		dns_zone_div.show();
		key_div.show();
		secret_div.show();
		backComponentIpDiv.hide();
		keepaliveIpDiv.hide();
		nginxCategoryDiv.hide();
		$("#component_ip").unbind();
		$("#component_ip").bind('blur',function(){
			checkIp();
		});
		
	}else if(componentType==6){
		name.text("组件名称：");
		dbtype.hide();
		driver.hide();
		dbtype.hide();
		driverOrder.hide();
		ipDiv.hide();
		config.hide();
		username.hide();
		password.hide();
		configf.hide();
		versions.hide();
		f5CompDiv.show();
		f5ExternalIpDiv.show();
		f5ExternalPortDiv.show();
		autoConfigTextDiv.hide();
		nginxHostMgr.show();
		initSqlDiv.hide();
		nginxIns.hide();
		dns_zone_div.hide();
		key_div.hide();
		secret_div.hide();
		nginxCategoryDiv.hide();
		backComponentIpDiv.hide();
		keepaliveIpDiv.hide();
		//加载F5組件的列表
		$.get(base+'component/list?componentType=0&page=1&rows=65536',null,function(response){
			$('#f5_comp_id').html('');
			$.each (response.rows, function (index, obj){
				var componentId = obj.componentId;
				var componentName = obj.componentName;
				var componentType = obj.componentType;
				var autoConfigF5 = obj.autoConfigF5;
				//暂时写死 ，2代表F5
				if(componentType=='2'){
					$('#f5_comp_id').append('<option value="'+componentId+':'+autoConfigF5+'">'+componentName+'</option>');
				}
			});
			loadAutoConfig();
			$("#f5_comp_id").unbind();
			$("#f5_comp_id").bind('change',function(){
				loadAutoConfig();
			});
		});
	}else if(componentType==7){
		comp_name.show();
		name.text("组件名称：");
		ipName.text("SVN地址：");
		config.hide();
		username.show();
		password.show();
		driver.hide();
		dbtype.hide();
		$("#component_ip").attr('placeholder','请输入SVN地址');
		initSqlDiv.hide();
		versions.hide();
		nginxIns.hide();
		nginxCategoryDiv.hide();
//      取消ip校验      	
//		$("#component_ip").unbind('blur');
//		$("#component_ip").bind('blur',function(){
//			checkIp();
//		});
	}
	$(".help-block").remove();
	check();
}

/**f5点击选'是'展示*/
function showInfo(){
	var username=$("#username");
	var password=$("#password");
	var versions=$("#versions");
	username.show();
	password.show();
	versions.show();
}
//f5点击否
function hideInfo(){
	var username=$("#username");
	var password=$("#password");
	var versions=$("#versions");
	username.hide();
	password.hide();
	versions.hide();
}

//f5BIGIP版本
function checkfVersions(){
	validToolObj.isNull('#bigip_version',false);
	check();
}

//修改是否自动配置F5点击事件
function editHideInfo(){
	var username=$("#username");
	var password=$("#password");
	var versions=$("#versions");
	username.hide();
	password.hide();
	versions.hide();
/*	$("#component_userName").val("");
	$("#component_pwd").val("")
	$("#bigip_version").val("");*/
}

function editShowInfo(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var username=$("#username");
	var password=$("#password");
	var versions=$("#versions");
	username.show();
	password.show();
	versions.show();
}

function checkF5Comp(){
	validToolObj.isNull('#f5_comp_id');
	check();
}

function getComType(componentType){
	var name="";
	url = base+'componentType/selectAll';
	$.ajax({
        type: 'get',
        url:url,
        data:{
        },
        async:false,
        dataType: 'json',
        success: function (response) {
        	var datas=response.params;
    		for(var i=0;i<datas.length;i++){
    			var data=datas[i];
    			if(componentType==data.componentTypeId){
    				name = data.componentTypeName;
    				break;
    			}
    		}
        }
	});
	return name;
}


//加载F5组件 是否自动配置属性
function loadAutoConfig(){
	var autoConfigF5 = $('#f5_comp_id').val().split(":")[1];
	$("#autoConfigTextDiv").show();
	if(autoConfigF5=="0"){
		$("#autoConfigTextDiv").find(".col-sm-9").html("否");
	}else if(autoConfigF5=="1"){
		$("#autoConfigTextDiv").find(".col-sm-9").html("是");
	}else{
		$("#autoConfigTextDiv").find(".col-sm-9").html("");
	}
}

//维护nginx主机
function nginxHostMgr(componentId){
	//加载可选择作为nginx主机的 主机列表
	var freeHostHtml = "";
	$.ajax({
        type: 'get',
        url: base+'host/hostList',
        dataType: 'json',
        success: function (array) {
    		var data = new Object();
    		data.list = array;
    		freeHostHtml = template('freeHostSelectTp', data);
        	$("#freeHostDiv").html(freeHostHtml);
        }
	});
}

//初始化ngingx主机维护列表
function initNginxHostList(componentId){
	if(componentId.length>0){
		$.ajax({
	        type: 'get',
	        url: base+'host/selectComponentHost',
	        dataType: 'json',
		    data: {componentId:componentId},
	        success: function (array) {
	    		var data = new Object();
	    		data.list = array;
	    		var compHostHtml = template('host_listTp', data);
	        	$("#host_list_body").html(compHostHtml);
	        	reloadNginxCountText();
	        }
		});
	}else{
		$("#host_list_body").empty();
	}
}

//新增nginx组 主机
function addComponetHost(){
	var selectHostV = $("#componentHost").val();
	if(selectHostV==""){
		showMessageHard("请先选择需要新增的主机！");
		return;
	}
	var selectHostIp = $("#componentHost").find("option:selected").text();
	var selectHostId = selectHostV.split(":")[0];
	if(selectHostId==""||selectHostId==null){
		showMessageHard("主机信息异常，请重新添加！");
		return false;
	}
	var selectHostName = selectHostV.split(":")[1];
	var selectHostStatus = selectHostV.split(":")[2];
	
//	//是否重复新增
//	var complete = false;
//	$("#host_list_body").find("input[name='hostId']").each(function(){
//		if($(this).val()==selectHostId){
//			complete = true;
//			return false;
//		}
//	});
//	if(complete){
//		showMessageHard("请勿重复添加主机！");
//		return;
//	}
//	
//	var data = new Object();
//	var array = new Array();
//	var o = new Object();
//	o.componentHostId = "";
//	o.hostId = selectHostId;
//	o.hostIp = selectHostIp;
//	o.hostName = selectHostName;
//	o.hostStatus = selectHostStatus;
//	o.nginxStatus;
//	o.nginxPort = "5186";
//	o.delFlag = "";
//	array.push(o);
//	data.list = array;
//	var compHostHtml = template('host_listTp', data);
//	$("#host_list_body").append(compHostHtml);
//	reloadNginxCountText();
	var url = base + 'component/nginxTest?hostId=' + selectHostId;
	$("#spinner2").css("display","block");
	$.get(url,function(response){
		$("#spinner2").css("display","none");
		if(response.success){
			//是否重复新增
			var complete = false;
			$("#host_list_body").find("input[name='hostId']").each(function(){
				if($(this).val()==selectHostId){
					complete = true;
					return false;
				}
			});
			if(complete){
				showMessageHard("请勿重复添加主机！");
				return;
			}
			
			var data = new Object();
			var array = new Array();
			var o = new Object();
			o.componentHostId = "";
			o.hostId = selectHostId;
			o.hostIp = selectHostIp;
			o.hostName = selectHostName;
			o.hostStatus = selectHostStatus;
			o.nginxPort = "80";
			o.delFlag = "";
			array.push(o);
			data.list = array;
			var compHostHtml = template('host_listTp', data);
			$("#host_list_body").append(compHostHtml);
			reloadNginxCountText();
		}else{
			showMessageHard(response.message);
		}
	});
}

//删除一条nginx主机
function delNginxHost(btn){
	var componentHostId = $(btn).parents("tr").find("input[name='componentHostId']").val();
	if(componentHostId!=''){
		$(btn).parents("tr").find("input[name='delFlag']").val("1")
		$(btn).parents("tr").hide();
	}else{
		$(btn).parents("tr").remove();
	}
	reloadNginxCountText();
}

function reloadNginxCountText(){
	var count = getNginxHostCount();
	$("#countTextB").html("维护Nginx主机("+count+")");
}

//获取表格中nginx主机数
function getNginxHostCount(){
	var count = 0;
	$("#host_list_body").find("input[name='delFlag']").each(function(){
		if($(this).val()!="1"){
			count++;
		}
	});
	return count;
}

//拼接 Nginx主机 字符串
function joinNginxHostStr(){
	var str="";
	$("#host_list_body tr").each(function(){
		var componentHostId = $(this).find("[name=componentHostId]").val();
		var hostId = $(this).find("[name=hostId]").val();
//		var nginxStatus = $(this).find("[name=nginxStatus]").val();
		var nginxPort = $(this).find("[name=nginxPort]").val();
		var delFlag = $(this).find("[name=delFlag]").val();
		str+=componentHostId + "==="+hostId +"===1==="+nginxPort+"==="+delFlag+"|~~|";
	});
	return str;
}

//编辑框
function showEditComponentWin(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	if(ids.length ==0){
		showMessage("请先选择一条记录！");
	}else if(ids.length >1){
		showMessage("只能选择一条记录！");
	}else{
		var rowData = $(grid_selector).jqGrid("getRowData",ids);
		var componentId =rowData.componentId;
		initNginxHostList(componentId);
		var componentType = rowData.componentType;
		var componentName = rowData.componentName;
		var componentIp = rowData.componentIp;
		var componentDescription =rowData.componentDescription;
		var componentConfigDir = rowData.componentConfigDir;
		var componentDriver = rowData.componentDriver;
		var componentUserName = rowData.componentUserName;
		var componentPwd =rowData.componentPwd;
		var componentDBType = rowData.componentDBType;
		
		var autoConfigF5 = rowData.autoConfigF5;
		var bigipVersion = rowData.bigipVersion;
		var driverId = rowData.driverId;
		var driverDesc = rowData.driverDesc;
		var initSql = rowData.initSql;
		var f5CompId = rowData.f5CompId;
		var f5ExternalIp = rowData.f5ExternalIp;
		var f5ExternalPort = rowData.f5ExternalPort;
		var backComponentIp = rowData.backComponentIp;
		var keepaliveIp = rowData.keepaliveIp;
		var nginxCategory = rowData.nginxCategory;
		var dnsZone = rowData.dnsZone;
		var key = rowData.key;
		var secret = rowData.secret;
		var componentHostPort = rowData.componentHostPort;
		/*var autoConfigF5 ="";
		var bigipVersion ="";*/
		var input="";
		url = base+'componentType/selectAll';
		var display="style='display: none'",display0="",display1="",display2="",display3="",display4="",dispaly5="",dispaly6="",display7="",display8="",display9="",display10="",display11="",display12="",display13="",dispaly14="",display15="";
		var check="checked=\'checked\'",check1="",check2="",check3="",check4="";
		var name="";
		var ipName="";
		var select1="";
		var select2="";
		var select3="";
		var select4="";
		var select5="";
		var select6="";
		var onkey="";
		var f5comInput = "";
		var isAutoConfigText = "";
		if(componentType==1||componentType==4){
			name="组件名称：";
			ipName="IP地址";
			onkey="checkIp();";
			display0="";
			display1="";
			display2=display;
			display3="";
			display4="";
			dispaly5=display;
			dispaly6=display;
			display7=display;
			display8=display;
			display9=display;
			display10=display;
			display11=display;
			display12="";
			display13="";
			display14="";
			display15="";
			if(nginxCategory == 1){
				check3=check;
				check4="";
			}
			if(nginxCategory == 2){
				check4=check;
				check3="";
			}
		}else if(componentType==2){
			name="组件名称：";
			ipName="IP地址";
			display0="";
			display1=display;
			display2=display;
			display3=display;
			display4="";
			onkey="checkIp();";
			dispaly5=display;
			dispaly6="";
			display8=display;
			display9=display;
			display10=display;
			display11=display;
			display12=display;
			display13=display;
			display14=display;
			display15=display;
			if(autoConfigF5 == 0){
				check2=check;
				check1="";
				display7=display;
			}
			
			if(autoConfigF5 == 1){
				display3="";
				display7="";
//				$('#auto_config_f5_yes').attr("checked",'checked');
				check1=check;
				check2="";
			}
		}else if(componentType==3){
			name="数据库名称：";
			ipName="数据库地址";
			display0="";
			display1=display;
			display2="";
			display3="";
			display4="";
			onkey="checkDbUrl();";
			dispaly5="";
			dispaly6=display;
			display7=display;
			display8="";
			display9="";
			display10=display;
			display11=display;
			display12=display;
			display13=display;
			display14=display;
			display15=display;
			switch(parseInt(componentDBType)){
			case 0:
				select1="selected";
				break;
			case 1:
				select2="selected";
				break;
			case 2:
				select3="selected";
				break;
			case 3:
				select4="selected";
				break;
			}
			
			
		}else if(componentType==5){
			ipName="IP地址";
			name="组件名称：";
			display0="";
			display1=display;
			display2=display;
			display3="";
			display4="";
			onkey="checkIp();";
			dispaly5=display;
			dispaly6=display;
			display7=display;
			display8=display;
			display9=display;
			display10=display;
			display11="";
			display12=display;
			display13=display;
			display14=display;
			display15=display;
		}else if(componentType==6){
			name="组件名称：";
			display0=display;
			display1=display;
			display2=display;
			display3=display;
			display4="";
			dispaly5=display;
			dispaly6=display;
			display7=display;
			display8=display;
			display9=display;
			display10="";
			display11=display;
			display12=display;
			display13=display;
			display14=display;
			display15=display;
		}else if(componentType==7){
		      ipName="SVN地址";
			  display1=display;
			  display2=display;
			  display3="";
			  display4=display;
			  onkey=true;
			  dispaly5=display;
			  dispaly6=display;
			  display7=display;
			  display8=display;
			  display9=display;
			  display10=display;
			  display12=display;
			  display13=display;
			  display14=display;
			  display15=display;
		   }
		$.get(url,function(response){
			var datas=response.params;
			for(var i=0;i<datas.length;i++){
				//var data=datas[i];
				if(componentType==datas[i].componentTypeId){
					input="<input id=\"component_type\" readonly='true' name='component_type' type='hidden' value='"+componentType+"' class=\"form-control\"/>"
					+"<input readonly='true' name='component_type' type='text' value='"+datas[i].componentTypeName+"' class=\"form-control\"/>";
					break;
					//option="<option value='"+data.componentTypeId+"'selected='selected'>"+data.componentTypeName+"</option>";
				}
				//else{
					//option="<option value='"+data.componentTypeId+"'>"+data.componentTypeName+"</option>";}
				//options+=option;
			}
			var f5comInput = "";
			$.ajax({
		        type: 'get',
		        url:base+'component/list?componentType=0&componentId='+f5CompId,
		        data:{
		        },
		        async:false,
		        dataType: 'json',
		        success: function (response) {
		        	$.each (response.rows, function (index, obj){
						var cComponentId = obj.componentId;
						var componentName = obj.componentName;
						var componentType = obj.componentType;
						var autoConfigF5 = obj.autoConfigF5;
						//暂时写死 ，2代表F5
						if(componentType=='2' && cComponentId==f5CompId){
							$('#f5_comp_id').append('<option value="'+cComponentId+':'+autoConfigF5+'">'+componentName+'</option>');
							f5comInput="<input id=\"f5_comp_id\" readonly='true' name='f5_comp_id' type='hidden' value='"+cComponentId+':'+autoConfigF5+"' class=\"form-control\"/>"
							+"<input readonly='true' name='f5_component_name' type='text' value='"+componentName+"' class=\"form-control\"/>";

							if(autoConfigF5=='0'){
								isAutoConfigText="否";	
							}
							if(autoConfigF5=='1'){
								isAutoConfigText="是";
							}
						}
					});
		        }
			});
			
			var hostCount = getNginxHostCount();
			
			bootbox.dialog({
				title : "<b>编辑组件</b>",
				message : "<div class='well ' style='margin-top:1px;'>"+
								"<form class='form-horizontal' role='form' id='create_component_form'>"+
									"<input id=\"component_id\" type=\"hidden\" name=\"component_id\"value='"+componentId+"'>"+
									"<div class='form-group' style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>组件类型：</b></label>"+
						  				"<div class='col-sm-9'>"+
						  					input+						 
						  				"</div>"+
						  			"</div>"+
						  			"<div class='form-group'"+display4+"' style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>"+name+"</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      					"<input id=\"component_name\" name='component_name' type='text' value='"+componentName+"' class=\"form-control\" onblur='checkComName();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
			    	      			"<div class='form-group'"+dispaly5+" style='margin-bottom: 5px' id='db_type'>"+
						  				"<label class='col-sm-3'><div align='right'><b>数据库类型：</b></div></label>"+
						  				"<div class='col-sm-9'>"+
						  				"<select id='component_db_type' name='component_db_type' class='form-control' onchange='initDriverName()' >"+
						  						"<option value='0' "+select1+">ORACLE</option>"+
						  						"<option value='1' "+select2+">MYSQL</option>"+
						  						"<option value='2' "+select3+">DB2</option>"+
						  						"<option value='3' "+select4+">ISOLATION</option>"+
						  						"<option value='4' "+select5+">SQLSERVER</option>"+
						  						"<option value='5' "+select6+">ORACLE_XA</option>"+
							  				"</select>"+
						  				"</div>"+
						  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
					  			"</div>"+
					  			
					  			"<div class='form-group' "+display8+" style='margin-bottom: 5px;' id='driverOrder'>"+
									"<label class='col-sm-3'><div align='right'><b>驱动程序：</b></div></label>"+
									"<div class='col-sm-9'>"+
										"<input id=\"component_driver_id\" name='component_driver_id' type='hidden' value='"+driverId+"' class=\"form-control\"/>"+
										"<input id=\"component_driver_desc\" name='component_driver_desc' type='text' value='"+driverDesc+"' class=\"form-control\" readonly=\"readonly\" />"+
									"</div>"+
									"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
								"</div>"+
					  			
				      				"<div class='form-group' id='config' "+display1+" style='margin-bottom: 5px;'>"+
					  					"<label class='col-sm-3'><b>配置文件目录：</b></label>"+
					  					"<div class='col-sm-9'>"+
		    	      					"<input id='component_config_dir' name='component_config_dir' type='text'value='"+componentConfigDir+"'  class=\"form-control\" onblur='checkManage();' />"+
		    	      					"</div>"+
		    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
			    	      			"<div class='form-group' id='driver' "+display2+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>驱动名称：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"component_driver\"  name='component_driver' type='text' value='"+componentDriver+"' readonly=\"readonly\" class=\"form-control\" onblur='checkDriver();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
			    	      			"<div class='form-group' "+display0+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>"+ipName+"</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"component_ip\" name='component_ip' type='text' value='"+componentIp+"' class=\"form-control\" onblur='"+onkey+"'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
			    	      			"<div class='form-group' id='nginxCategoryDiv' "+display14+" style='margin-bottom: 5px;'>"+
			  							"<label class='col-sm-3' style='padding-left: 0px;'><div align='right'><b id='isConfig'>使用方式：</b></div></label>"+
			  							"<div class='col-sm-9' style='margin-top: 5px;'>"+
			  							"<input id=\"nginx_category_system\" name='nginx_category' type='radio' value='1' class=\"ace\" "+check3+" /><span class=\"lbl\"> <b>系统负载</b></span>"+
			  							
			  							
			  							"<input id=\"nginx_category_registry\" name='nginx_category' type='radio' value='2' class=\"ace\" "+check4+" /><span class=\"lbl\" style='margin-left: 10px;'> <b>仓库负载</b></span>"+
			  							"</div>"+
			  							"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			  					    "</div>"+
			    	      			"<div class='form-group' "+display12+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>从IP地址：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"back_component_ip\" name='back_component_ip' type='text' value='"+backComponentIp+"' class=\"form-control\"/>"+
			    	      				"</div>"+
			    	      			"</div>"+
			    	      			"<div class='form-group' "+display15+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>主机端口号：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"component_host_port\" name='component_host_port' type='text' value='"+componentHostPort+"' class=\"form-control\"/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
			    	      			"<div class='form-group' "+display13+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>keepalived IP地址</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"keepalive_ip\" name='keepalive_ip' type='text' value='"+keepaliveIp+"' class=\"form-control\"/>"+
			    	      				"</div>"+
			    	      			"</div>"+
			    	    			"<div class='form-group' id='dns_zone_div' "+display11+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>Zone值：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"dns_zone\"  name='dns_zone' type='text' value='"+dnsZone+"'  class=\"form-control\" onblur='checkZone();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
		    	      				"<div class='form-group' id='key_div' "+display11+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>key值：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"key\"  name='key' type='text' value='"+key+"'  class=\"form-control\" onblur='checkZoneKey();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
	    	      					"<div class='form-group' id='secret_div' "+display11+" style='margin-bottom: 5px;'>"+
						  				"<label class='col-sm-3'><b>MD5的secret值：</b></label>"+
						  				"<div class='col-sm-9'>"+
		    	      					"<textarea id=\"secret\" name=\"secret\" class=\"form-control\" onblur='checkZoneSecret();'  rows=\"3\">"+secret+"</textarea>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
			    	      			"<div class='form-group' id='comconfig' "+dispaly6+" style='margin-bottom: 5px;'>"+
			  							"<label class='col-sm-3' style='padding-left: 0px;'><div align='right'><b id='isConfig'>自动配置F5：</b></div></label>"+
			  							"<div class='col-sm-9' style='margin-top: 5px;'>"+
			  							"<input id=\"auto_config_f5_yes\" name='auto_config_f5' type='radio' value='1' class=\"ace\" "+check1+" onclick='editShowInfo();'/><span class=\"lbl\"> <b>是</b></span>"+
			  							
			  							
			  							"<input id=\"auto_config_f5_no\" name='auto_config_f5' type='radio' value='0' class=\"ace\" "+check2+" onclick='editHideInfo();'/><span class=\"lbl\" style='margin-left: 10px;'> <b>否</b></span>"+
			  							"</div>"+
			  							"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			  					    "</div>"+
			    	      			
			    	      			"<div class='form-group' id='username' "+display3+" style='margin-bottom: 5px;'>"+
					  					"<label class='col-sm-3'><b>用户名：</b></label>"+
					  					"<div class='col-sm-9'>"+
		    	      					"<input id='component_userName' name='component_userName' type='text' value='"+componentUserName+"'onblur='checkUser();' class=\"form-control\" />"+
		    	      					"</div>"+
		    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
		    	      				"<div class='form-group' id='password' "+display3+" style='margin-bottom: 5px;'>"+
					  					"<label class='col-sm-3'><b>密码：</b></label>"+
					  					"<div class='col-sm-9'>"+
		    	      					"<input id=\"component_pwd\" name='component_pwd' type='password' value='"+componentPwd+"' class=\"form-control\" onblur='checkPwd();' />"+
		    	      					"</div>"+
		    	      					"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
		    	      				"</div>"+
		    	      				
			    	      			"<div class='form-group' id='versions' "+display7+" style='margin-bottom: 5px;'>"+
	  			   						"<label class='col-sm-3'><div align='right'><b>BIGIP版本：</b></div></label>"+
	  			   						"<div class='col-sm-9'>"+
	  			   						"<input id=\'bigip_version\' name='bigip_version' type='text' value='"+bigipVersion+"' class=\"form-control\" placeholder='请输入BIGIP版本' onblur='checkfVersions();'/>"+
	  			   						"</div>"+
	  			   						"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
	  			   				   "</div>"+

		    	      				/*nginx组 类型 差异字段 start by yueyong*/
									"<div class='form-group' "+display10+" style='margin-bottom: 5px;' id='f5_comp_id_div'>"+
						  				"<label class='col-sm-3'><div align='right'><b>F5组件：</b></div></label>"+
						  				"<div class='col-sm-9'>"+
						  					f5comInput+
						  				"</div>"+
						  				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
						  			"</div>"+
									"<div class='form-group' "+display10+" style='margin-bottom: 5px;' id='autoConfigTextDiv'>"+
						  				"<label class='col-sm-3'><div align='right'><b>自动配置F5：</b></div></label>"+
						  				"<div class='col-sm-9' style='line-height:29px;'>"+
						  				isAutoConfigText+
						  				"</div>"+
						  			"</div>"+
			    	      			"<div class='form-group' "+display10+" style='margin-bottom: 5px;' id='f5_external_ip_div'>"+
						  				"<label class='col-sm-3'><div align='right'><b id='ipName'>F5对外IP：</b></div></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"f5_external_ip\" name='f5_external_ip' type='text' class=\"form-control\" value='"+f5ExternalIp+"' placeholder='请输入F5对外IP地址' onblur='checkF5Ip();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
			    	      			"<div class='form-group' "+display10+" style='margin-bottom: 5px;' id='f5_external_port_div'>"+
						  				"<label class='col-sm-3'><div align='right'><b id='ipName'>F5对外端口：</b></div></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"f5_external_port\" name='f5_external_port' type='number' class=\"form-control\" value='"+f5ExternalPort+"' placeholder='请输入F5对外端口' onblur='checkF5Port();'/>"+
			    	      				"</div>"+
			    	      				"<i class='glyphicon glyphicon-asterisk' style='font-size: 10px;color:#FF0000;top:7px'></i>"+
			    	      			"</div>"+
						  			/*nginx组 类型 差异字段 end by yueyong*/
	  			   				   
			    	      			"<div class='form-group hide' "+display9+" style='margin-bottom: 5px;' id='init_sql_div'>"+
						  				"<label class='col-sm-3'><div align='right'><b id='ipName'>初始化脚本：</b></div></label>"+
						  				"<div class='col-sm-9'>"+
			    	      				"<input id=\"init_sql\" name='init_sql' type='text' value='"+initSql+"' class=\"form-control\" placeholder='<init-sql>属性配置' />"+
			    	      				"</div>"+
		    	      				"</div>"+
		    	      				
					      			"<div class='form-group' style='margin-bottom: 5px;'>"+
					      				"<label class='col-sm-3'><b>说明：</b></label>"+
						  				"<div class='col-sm-9'>"+
			    	      					"<textarea id=\"component_description\" onblur=\"validToolObj.length('#component_description',200)\" name=\"component_description\" class=\"form-control\" rows=\"3\">"+componentDescription+"</textarea>"+
			    	      				"</div>"+
			    	      			"</div>"+
		  			   				
					      			"<div class='form-group' "+display10+" style='margin-bottom: 5px;text-align: right;margin-right:4px' id='nginxHostMgr'>"+
						  				"<div class='col-sm-12'>"+
						  					"<a href='#modal-hostlist' data-toggle='modal'>"+
						  					"<button class='btn btn-sm btn-warning btn-round' type='button' onclick='nginxHostMgr(\"\");'><i class='ace-icon fa fa-pencil-square-o bigger-125'></i> <b id='countTextB'>维护Nginx主机("+hostCount+")</b></button>"+
						  					"</a>"+
			    	      				"</div>"+
			    	      			"</div>"+
			    	      		"</form>"+
			    	      	"</div>",
				buttons : {
					"success" : {
						"label" : "<i class='icon-ok' id='saveButt'></i> <b>保存</b>",
						"className" : "btn-sm btn-danger btn-round",
						"callback" : function() {
										var componentId = $('#component_id').val();
							    	    var componentName = $('#component_name').val();
							            var componentIp = $('#component_ip').val();
							            var componentType = $('#component_type').val();
							            var componentDescription = $('#component_description').val();						       
							            var component_userName = $('#component_userName').val();
							            var component_pwd = $('#component_pwd').val();
							            var component_config_dir=$('#component_config_dir').val();
							            var component_driver=$('#component_driver').val();
							            var component_db_type=$('#component_db_type').val();
							            var backComponentIp = $("#back_component_ip").val();
							            var componentHostPort = $("#component_host_port").val();
							        	var keepaliveIp = $("#keepalive_ip").val();
							            var auto_config_f5 = $("input[name=auto_config_f5]:checked").val();
							            var bigip_version = $('#bigip_version').val();
							            var component_driver_id = $('#component_driver_id').val();
							            var init_sql = $('#init_sql').val();
							            var f5CompId = $("#f5_comp_id").val();
							            if(f5CompId!=null && f5CompId.length>0){
							            	f5CompId = f5CompId.split(":")[0];
							            }
							            var nginxCategory = $("input[name=nginx_category]:checked").val();
							            var f5ExternalIp = $("#f5_external_ip").val();
							            var f5ExternalPort = $("#f5_external_port").val();
							            var dnsZone = $("#dns_zone").val();
							            var key = $("#key").val();
							            var secret = $("#secret").val();
							            var componentHostInfo;
							            
							            if(componentType==6){
							            	if(getNginxHostCount()<=0){
							            		showMessageHard("请维护Nginx主机列表！");
								            	return false;
							            	}
							            	componentHostInfo = joinNginxHostStr();
							            }
							            
									data={
											componentId:componentId,
											componentName: componentName,
											componentIp: componentIp,
											componentType: componentType,
											componentDescription: componentDescription,
											componentPwd:component_pwd,
											componentUserName:component_userName,
											componentConfigDir:component_config_dir,
											componentDriver:component_driver,
											componentDBType:component_db_type,
											"componentExpand.autoConfigF5":auto_config_f5,
											"componentExpand.bigipVersion":bigip_version,
											"componentExpand.initSql":init_sql,
											"componentExpand.driverId":component_driver_id,
											"componentExpand.f5CompId":f5CompId,
											"componentExpand.f5ExternalIp":f5ExternalIp,
											"componentExpand.f5ExternalPort":f5ExternalPort,
											componentHostInfo:componentHostInfo,
											dnsZone:dnsZone,
											key :key,
											secret:secret,
											backComponentIp:backComponentIp,
											keepaliveIp:keepaliveIp,
											nginxCategory:nginxCategory,
											componentHostPort:componentHostPort
									};
									test_data={
											componentId:"",
											componentName: componentName,
											componentIp: componentIp,
											componentType: componentType,
											componentDescription: componentDescription,
											componentPwd:component_pwd,
											componentUserName:component_userName,
											componentConfigDir:component_config_dir,
											componentDriver:component_driver,
											componentDBType:component_db_type,
											"componentExpand.autoConfigF5":auto_config_f5,
											"componentExpand.bigipVersion":bigip_version,
											"componentExpand.initSql":init_sql,
											"componentExpand.driverId":component_driver_id,
											"componentExpand.f5CompId":f5CompId,
											"componentExpand.f5ExternalIp":f5ExternalIp,
											"componentExpand.f5ExternalPort":f5ExternalPort,
											componentHostInfo:componentHostInfo,
											dnsZone:dnsZone,
											key :key,
											secret:secret
									};
									var url = base + 'component/connectTest';
						        	$("#spinner").css("display","block");
						        	$.get(url,test_data,function(response){
						        		$("#spinner").css("display","none");
						        		if(response.success||componentType==7){
						        			var url=base+"component/update";
											$.post(url,data,function(response){
												showMessage(response.message,function(){
													$(grid_selector).trigger("reloadGrid");
												});
											});
						        		}else{
						        			bootbox.dialog({
								        	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;连接测试失败，是否继续保存？</div>',
								        	    title: "提示",
								        	    buttons: {
								        	        main: {
								        	        	label: "<i class='icon-info'></i><b>是</b>",
								                        className: "btn-sm btn-success btn-round",
								        	            callback: function () {
								        	            	var url=base+"component/update";
															$.post(url,data,function(response){						
																showMessage(response.message,function(){
																	$(grid_selector).trigger("reloadGrid");
																});
															});
								        	            }
								        	        },
								        	        cancel: {
								        	        	label: "<i class='icon-info'></i> <b>否</b>",
								                        className: "btn-sm btn-danger btn-round",
								        	            callback: function () {
								        	            }
								        	        }
								        	    }
								        	});
						        		}
						        	});
							    }
							   
					},
					    "cancel" : {
						"label" : "<i class='icon-info'></i> <b>取消</b>",
						"className" : "btn-sm btn-warning btn-round",
						"callback" : function() {}
					}
				}
			});
			$("#component_db_type").on('change',function(){
				if("0"==this.value){
					$("#component_driver").val("oracle.jdbc.driver.OracleDriver");
					$("#component_ip").attr('placeholder','jdbc:oracle:thin:@<host>:<port>:<SID>');
				}
				if("1"==this.value){
					$("#component_driver").val("com.mysql.jdbc.Driver");
					$("#component_ip").attr('placeholder','jdbc:mysql://<host>:<port>/<databasename>');
				}
				if("2"==this.value){
					$("#component_driver").val("com.ibm.db2.jcc.DB2Driver");
					$("#component_ip").attr('placeholder','jdbc:db2://<host>:<port>/<databasename>');
				}
				if("4"==this.value){
					$("#component_driver").val("com.microsoft.sqlserver.jdbc.SQLServerDriver");
					$("#component_ip").attr('placeholder','jdbc:sqlserver://<host>:<port>;DatabaseName=<databasename>');
				}
				if("5"==this.value){
					$("#component_driver").val("oracle.jdbc.xa.client.OracleXADataSource");
					$("#component_ip").attr('placeholder','jdbc:oracle:thin:@<host>:<port>:<SID>');
				}
				if(component_db_type==3){
					$("#component_ip").attr('placeholder','');
				}
				
			});
		});
	}
}

function showDeleteComponents(){
	var ids=$(grid_selector).jqGrid("getGridParam","selarrrow");
	var infoList = "";
	
	for(var i=0; i<ids.length; i++){
		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
		
		infoList += rowData.componentName+" ";
	}
	if(ids.length ==0){
		showMessage("请先选择需要删除的组件！");
		return;
	}
	bootbox.dialog({
	    message: '<div class="alert alert-info" style="margin:10px"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;删除组件&nbsp;' + infoList + '?</div>',
	    title: "提示",
	    buttons: {
	        main: {
	        	label: "<i class='icon-info'></i><b>确定</b>",
                className: "btn-sm btn-success btn-round",
	            callback: function () {
	            	var componentIds="";
	            	for(var i=0; i<ids.length; i++){
	            		var rowData = $(grid_selector).jqGrid("getRowData",ids[i]);
	            		componentIds = (componentIds + rowData.componentId) + (((i + 1)== ids.length) ? '':',');
	            	}
	            	deleteComponents(componentIds);
	            }
	        },
	        cancel: {
	        	label: "<i class='icon-info'></i> <b>取消</b>",
                className: "btn-sm btn-danger btn-round",
	            callback: function () {
	            }
	        }
	    }
	});
  }
function deleteComponents(componentIds){
	var data= {
     	ids : componentIds
     };
	$.post(base+'component/deletes',data,function(respone){
		showMessage(respone.message);
		$(grid_selector).trigger("reloadGrid");
	});
}

//查询关联物理系统
function querySystems(componentId,componentName,componentType){
	$("#componentNameLable").html(getComType(componentType)+"  :  "+componentName);
	$('#host_list').html("");
	var tableStr ='';
	if(componentType==3){
		$("#but_div").removeClass("hide");
		tableStr = '<thead> <tr> '+
			'<th style="border: 1px;border-style:inset;width:10%"><input type="checkbox" id="checkAllSystem" onclick="checkAllSystem()" /></th>'+
			'<th style="border: 1px;border-style:inset;width:60%">物理系统名称</th>'+
			'<th style="border: 1px;border-style:inset;width:30%">物理系统版本</th> </tr> </thead>';
	}else{
		$("#but_div").addClass("hide");
		tableStr = '<thead> <tr>'+
			'<th style="border: 1px;border-style:inset;width:60%">物理系统名称</th>'+
			'<th style="border: 1px;border-style:inset;width:40%">物理系统版本</th> </tr> </thead>';
	}
	$.ajax({
	    type: 'get',
	    url: base+'component/getSystem',
	    data: {componentId:componentId,componentType:componentType},
	    dataType: 'json',
	    success: function (array) {
	    	$.each(array,function(index, obj){
	    		var deployId = obj.deployId;
	    		var systemName = obj.systemName;
	    		var deployVersion=obj.deployVersion;
	    		if(componentType==3){
	    			tableStr = tableStr
					+ '<tr><td style="border: 1px;border-style:inset;"><input type="checkbox" value="'+deployId+'" name="checkName" /></td>'
					+ '<td style="border: 1px;border-style:inset;">' + systemName + '</td>'
					+ '<td style="border: 1px;border-style:inset;">' + deployVersion + '</td></tr>';
	    		}else{
	    			tableStr = tableStr+ '<tr>'
					+ '<td style="border: 1px;border-style:inset;">' + systemName + '</td>'
					+ '<td style="border: 1px;border-style:inset;">' + deployVersion + '</td></tr>';
	    		}
				
	    	});
	    	$('#system_list').html(tableStr);
	    }
	});
}

//更新数据库相关联的物理系统
function refreshSystems(){
	var chk_value =Array();
	$('input[name="checkName"]:checked').each(function(){
		chk_value.push($(this).val());
	}); 
	if(chk_value==null||chk_value==''){
		showMessage("请选择需要更新的系统版本！");
		return;
	}
	var data= {
		chk_value:chk_value.toString()
	};
	$("#modalspinner").css("display","block");
	$.post(base+'component/refresh',data,function(response){
		$("#modalspinner").css("display","none");
		showMessage(response.message);
	});
}
function checkAllSystem(){
	if(document.getElementById('checkAllSystem').checked){
		$('input[name="checkName"]').attr("checked","true");
	}else{
		$('input[name="checkName"]').removeAttr("checked");
	}
	
}

//查询条件 查询主机列表
function searchHost(){
	var componentType = $('#componentType').val();
	var componentName = $('#componentName').val();
	if(null==componentType || componentType == ''){
		componentType = '0';
	}
	jQuery(grid_selector).jqGrid('setGridParam',{url : base+'component/list?componentType='+componentType+'&componentName='+componentName}).trigger("reloadGrid");
}

$().ready(function(){
	$.ajax({
		url: base+'componentType/selectAll',
		success:function(response){
			var options=$("#componentType");
			var datas=response.params;
			for(var i=0;i<datas.length;i++){
				var componentType=datas[i];
				option="<option value='"+componentType.componentTypeId+"'>"+componentType.componentTypeName+"</option>";
				options.append(option);
			}
		}
	});
});

function showMessageHard(message){
	 showMessage(message);
	 $(".bootbox:last").css("z-index","99999");
}
function getNginx(){
	
	$('#nginxIns_select').empty();
    //
	$('#nginxIns_select').append('<option value="0">不进行安装</option>');
	$.get(base+'hostComponent/hostComponentList?type=nginx&_search=false&nd=1462867345723&rows=10&page=1&sidx=version&sord=asc',null,function(response){
		$.each (response.rows, function (index, obj){
			var id = obj.id;
			var version = obj.version;
			$('#nginxIns_select').append('<option value="'+id+'">'+version+'</option>');
        });
	},'json');
}	
//跳转到端口管理
function portManager(){
	 window.location.href=base+"port/index.html";
}
function andNginxHost(){
	$('#modal-hostlist').modal('hide');
}