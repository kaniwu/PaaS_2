$(document).ready(function(){
		var error = false;
		
		$("#oldpass").blur(function(){
			var oldpass = $("#oldpass").val();
			if(oldpass =='') {
				showError('oldpass', '密码不能为空');
				error = true;
				return;
			}else {
			error = false;
			$("#oldpass").css({"border-color":"green"});
			$("#oldpassTip").css({"display":"none"});
		}
		});
		
		$("#newpass").blur(function(){
			var newpass = $("#newpass").val();
			if(newpass == '') {
				showError('newpass', '新密码不能为空');
				error = true;
				return;
			}
			else {
				error = false;
				$("#newpass").css({"border-color":"green"});
				$("#newpassTip").css({"display":"none"});
			}
		});

		$("#newpassAgain").blur(function(){
			var newpassAgain = $("#newpassAgain").val();
			if(newpassAgain == '') {
				showError('newpassAgain', '新密码确认不能为空');
				error = true;
				return;
			}

			var newpass = $("#newpass").val();
			if(newpassAgain != newpass) {
				showError('newpassAgain', '与输入的新密码不一致');
				error = true;
				return ;
			}
			else {
				error = false;
				$("#newpassAgain").css({"border-color":"green"});
				$("#newpassAgainTip").css({"display":"none"});
			}
		});
		
		$("#submit").click(function(event){
			
			$("#newpass").blur();
			$("#newpassAgain").blur();

			if(!error) {
				var username = $("#username").val();			
				var newpass = $("#newpass").val();
				var oldpass = $("#oldpass").val();
				$.post(base+'modifyPassProcess', {oldpass:oldpass,username:username, newpass:new Base64().encode(newpass)}, function(data) {
					if(data.success){
						showMessage(data.message,function(){
							window.location.href=base + 'login.html'
						});
					}else{
						showMessage(data.message,function(){
							 $("#oldpass").val("");
							 $("#newpass").val("");
							 $("#newpassAgain").val("");
						});
					}
				});
			}

			event.preventDefault();
			return false;
		});
	});

	function showError(formSpan, errorText) {
		$("#" + formSpan).css({"border-color":"red"});
		$("#" + formSpan + "Tip").empty();
		$("#" + formSpan + "Tip").append(errorText);;
		$("#" + formSpan + "Tip").css({"display":"inline"});
	}