
var startTime;
$(document).ready(function () {
	$("#subButton").click(function () {
		var myDate = new Date();
		startTime = myDate.getTime();
		$(this).attr("disabled", true);
		$("#uploadForm").submit();
		$("#progress").show();
		window.setTimeout("getProgressBar()", 1000);
	});
	$("#close").click(function(){$("#progress").hide();});
});
function getProgressBar() {
	var timestamp = (new Date()).valueOf();
	var bytesReadToShow = 0;
	var contentLengthToShow = 0;
	var bytesReadGtMB = 0;
	var contentLengthGtMB = 0;
	$.getJSON(base+"progressContoller/progress", {"t":timestamp}, function (json) {
		var bytesRead = (json.pBytesRead / 1024).toString();
		if (bytesRead > 1024) {
			bytesReadToShow = (bytesRead / 1024).toString();
			bytesReadGtMB = 1;
		}else{
			bytesReadToShow = bytesRead.toString();
		}
		var contentLength = (json.pContentLength / 1024).toString();
		if (contentLength > 1024) {
			contentLengthToShow = (contentLength / 1024).toString();
			contentLengthGtMB = 1;
		}else{
			contentLengthToShow= contentLength.toString();
		}
		bytesReadToShow = bytesReadToShow.substring(0, bytesReadToShow.lastIndexOf(".") + 3);
		contentLengthToShow = contentLengthToShow.substring(0, contentLengthToShow.lastIndexOf(".") + 3);
		if (bytesRead == contentLength) {
			$("#close").show();
			$("#uploaded").css("width", "300px");
			if (contentLengthGtMB == 0) {
				$("div#info").html("\u4e0a\u4f20\u5b8c\u6210\uff01\u603b\u5171\u5927\u5c0f" + contentLengthToShow + "KB.\u5b8c\u6210100%");
			} else {
				$("div#info").html("\u4e0a\u4f20\u5b8c\u6210\uff01\u603b\u5171\u5927\u5c0f" + contentLengthToShow + "MB.\u5b8c\u6210100%");
			}
			window.clearTimeout(interval);
			$("#subButton").attr("disabled", false);
		} else {
			var pastTimeBySec = (new Date().getTime() - startTime) / 1000;
			var sp = (bytesRead / pastTimeBySec).toString();
			var speed = sp.substring(0, sp.lastIndexOf(".") + 3);
			var percent = Math.floor((bytesRead / contentLength) * 100) + "%";
			$("#uploaded").css("width", percent);
			if (bytesReadGtMB == 0 && contentLengthGtMB == 0) {
				$("div#info").html("\u4e0a\u4f20\u901f\u5ea6:" + speed + "KB/Sec,\u5df2\u7ecf\u8bfb\u53d6" + bytesReadToShow + "KB,\u603b\u5171\u5927\u5c0f" + contentLengthToShow + "KB.\u5b8c\u6210" + percent);
			} else {
				if (bytesReadGtMB == 0 && contentLengthGtMB == 1) {
					$("div#info").html("\u4e0a\u4f20\u901f\u5ea6:" + speed + "KB/Sec,\u5df2\u7ecf\u8bfb\u53d6" + bytesReadToShow + "KB,\u603b\u5171\u5927\u5c0f" + contentLengthToShow + "MB.\u5b8c\u6210" + percent);
				} else {
					if (bytesReadGtMB == 1 && contentLengthGtMB == 1) {
						$("div#info").html("\u4e0a\u4f20\u901f\u5ea6:" + speed + "KB/Sec,\u5df2\u7ecf\u8bfb\u53d6" + bytesReadToShow + "MB,\u603b\u5171\u5927\u5c0f" + contentLengthToShow + "MB.\u5b8c\u6210" + percent);
					}
				}
			}
		}
	});
	var interval = window.setTimeout("getProgressBar()", 500);
}

//类的构建定义，主要职责就是新建XMLHttpRequest对象
var MyXMLHttpRequest=function(){
    var xmlhttprequest;
    if(window.XMLHttpRequest){
        xmlhttprequest=new XMLHttpRequest();
        if(xmlhttprequest.overrideMimeType){
            xmlhttprequest.overrideMimeType("text/xml");
        }
    }else if(window.ActiveXObject){
        var activeName=["MSXML2.XMLHTTP","Microsoft.XMLHTTP"];
        for(var i=0;i<activeName.length;i++){
            try{
                xmlhttprequest=new ActiveXObject(activeName[i]);
                break;
            }catch(e){
                       
            }
        }
    }
    
    if(xmlhttprequest == undefined || xmlhttprequest == null){
        alert("XMLHttpRequest对象创建失败！！");
    }else{
        this.xmlhttp=xmlhttprequest;
    }
    
    //用户发送请求的方法
    MyXMLHttpRequest.prototype.send=function(method,url,data,callback,failback){
        if(this.xmlhttp!=undefined && this.xmlhttp!=null){
            method=method.toUpperCase();
            if(method!="GET" && method!="POST"){
                alert("HTTP的请求方法必须为GET或POST!!!");
                return;
            }
            if(url==null || url==undefined){
                alert("HTTP的请求地址必须设置！");
                return ;
            }
            var tempxmlhttp=this.xmlhttp;
            this.xmlhttp.onreadystatechange=function(){
                if(tempxmlhttp.readyState==4){
                    if(temxmlhttp.status==200){
                        var responseText=temxmlhttp.responseText;
                        var responseXML=temxmlhttp.reponseXML;
                        if(callback==undefined || callback==null){
                            alert("没有设置处理数据正确返回的方法");
                            alert("返回的数据：" + responseText);
                        }else{
                            callback(responseText,responseXML);
                        }
                    }else{
                        if(failback==undefined ||failback==null){
                            alert("没有设置处理数据返回失败的处理方法！");
                            alert("HTTP的响应码：" + tempxmlhttp.status + ",响应码的文本信息：" + tempxmlhttp.statusText);
                        }else{
                            failback(tempxmlhttp.status,tempxmlhttp.statusText);
                        }
                    }
                }
            }
            
            //解决缓存的转换
            if(url.indexOf("?")>=0){
                url=url + "&t=" + (new Date()).valueOf();
            }else{
                url=url+"?+="+(new Date()).valueOf();
            }
            
            //解决跨域的问题
            if(url.indexOf("http://")>=0){
                url.replace("?","&");
                url="Proxy?url=" +url;
            }
            this.xmlhttp.open(method,url,true);
            
            //如果是POST方式，需要设置请求头
            if(method=="POST"){
                this.xmlhttp.setRequestHeader("Content-type","application/x-www-four-urlencoded");
            }
            this.xmlhttp.send(data);
    }else{
        alert("XMLHttpRequest对象创建失败，无法发送数据！");
    }
    MyXMLHttpRequest.prototype.abort=function(){
        this.xmlhttp.abort();
    }
  }
}