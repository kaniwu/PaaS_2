var canvas = undefined,
	socket3 = undefined;
var grid_deploy_selector = "#system_list";
jQuery(function($) {
	canvas = $('#testProgress');
	connect3();
	$(window).unload(function() {
		disconnect3();
	});
});

function connect3() {
    if ('WebSocket' in window) {
    	var host = window.location.host+base+"/";
        if(window.location.protocol=='http:'){
        	socket3 = new WebSocket('ws://' + host + '/taskService');
        }else if(window.location.protocol=='https:'){
        	socket3 = new WebSocket('wss://' + host + '/taskService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        
        socket3.onopen = function () {};
        socket3.onclose = function () {};
        socket3.onerror = function(e) {};
        socket3.onmessage = function (event) {
            canvasHandle(event.data);
        };
    } else {
        console.log('Websocket3 not supported');
    }
}

function disconnect3() {
	socket3.close();
    console.log("Disconnected");
}
var last_process=0;
function canvasHandle(option){
	option = $.parseJSON(option);
	if(option.taskId&&option.process<100){
		$("#system1_"+option.taskId).addClass("hide");
		$("#system2_"+option.taskId).addClass("hide");
		$("#system3_"+option.taskId).addClass("hide");
		$("#system4_"+option.taskId).addClass("hide");
		$("#system5_"+option.taskId).addClass("hide");
		$("#system6_"+option.taskId).addClass("hide");
		$("#system7_"+option.taskId).addClass("hide");
	}
	if(option.messageType == 'ws_task'){
		if(option.taskId){
			if(option.process == 100){
				last_process=0;
				showMessage(option.taskname);
				$("#system1_"+option.taskId).removeClass("hide");
				$("#system2_"+option.taskId).removeClass("hide");
				$("#system3_"+option.taskId).removeClass("hide");
				$("#system4_"+option.taskId).removeClass("hide");
				$("#system5_"+option.taskId).removeClass("hide");
				$("#system6_"+option.taskId).removeClass("hide");
				$("#system7_"+option.taskId).removeClass("hide");
				$(grid_deploy_selector).trigger("reloadGrid");
			}else if(option.process <100){
				if(last_process<=option.process){
					last_process = option.process;
					$("#messageInfo").html(option.taskname);
					$("#system_"+option.taskId).html("<a href='javascript:openMessageInfo(\""+option.taskId+"\");'>部署进度"+option.process+"%</a>");
				}
			}
		}
	}
}
function openMessageInfo(systemId){
	$("#openWin").addClass("open");
}