var delpoy_socket = undefined;
var grid_selector = "#delpoy_list";
jQuery(function($) {
	deploy_connect();
	$(window).unload(function() {
		deploy_disconnect();
	});
});

function deploy_connect() {
    if ('WebSocket' in window) {
        var host = window.location.host+base+"/";
        if(window.location.protocol=='http:'){
        	delpoy_socket = new WebSocket('ws://' + host + '/taskService');
        }else if(window.location.protocol=='https:'){
        	delpoy_socket = new WebSocket('wss://' + host + '/taskService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        
        delpoy_socket.onopen = function () {};
        delpoy_socket.onclose = function () {};
        delpoy_socket.onerror = function(e) {};
        delpoy_socket.onmessage = function (event) {
        	rollbackHandle(event.data);
        };
    } else {
        console.log('Websocket3 not supported');
    }
}

function deploy_disconnect() {
	delpoy_socket.close();
    console.log("Disconnected");
}

function rollbackHandle(option){
	option = $.parseJSON(option);
	if(option.taskId&&option.process<100){
	}
	if(option.messageType == 'ws_task'){
		if(option.taskId){
			if(option.process == 100){
				showMessage(option.taskname);
				$(grid_selector).trigger("reloadGrid");
			}else if(option.process <100){
				$("#deploy_"+option.taskId).html(option.taskname);
			}
		}
	}
}