var switchImageSocket = undefined;
var grid_deploy_selector = "#system_list";
var switchImageId = "";
jQuery(function($) {
	connectSwitchImageSocket();
	$(window).unload(function() {
		disconnectSwitchImageSocket();
	});
});

function connectSwitchImageSocket() {
    if ('WebSocket' in window) {
    	var host = window.location.host+base+"/";
        if(window.location.protocol=='http:'){
        	switchImageSocket = new WebSocket('ws://' + host + '/taskService');
        }else if(window.location.protocol=='https:'){
        	switchImageSocket = new WebSocket('wss://' + host + '/taskService');
        }else{
        	showMessage("WebSocket不支持"+window.location.protocol+"协议 ！");
        }
        
        switchImageSocket.onopen = function () {};
        switchImageSocket.onclose = function () {};
        switchImageSocket.onerror = function(e) {};
        switchImageSocket.onmessage = function (event) {
            canvasHandle(event.data);
        };
    } else {
        console.log('WebswitchImageSocket not supported');
    }
}

function disconnectSwitchImageSocket() {
	switchImageSocket.close();
    console.log("Disconnected");
}
var last_process=0;
function canvasHandle(option){
	option = $.parseJSON(option);
	if(option.taskId.indexOf("imageId_")!=-1){
		if(option.taskId&&option.process<100){
			$("#switchImageButt").addClass("hide");
			$("#switchImageProcess").removeClass("hide");
		}
		if(option.messageType == 'ws_task'){
			if(option.taskId){
				if(option.process == 100){
					$("#switchImageButt").removeClass("hide");
					$("#switchImageProcess").addClass("hide");
					last_process=0;
					showMessage(option.taskname);
				}else if(option.process <100){
					if(last_process<=option.process){
						last_process = option.process;
						$("#messageInfo").html(option.taskname);
					}
				}
			}
		}
	}
}
function openMessageInfo(){
	bootbox.dialog({
        title: "<b>查看切换进度</b>",
        message:template('openProcess') ,
        buttons: {
		     "cancel" : {
			       "label" : "<i class='icon-info'></i> <b>关闭</b>",
			         "className" : "btn-sm btn-warning btn-round",
			        "callback" : function() {}
		    }
        }
    });
}