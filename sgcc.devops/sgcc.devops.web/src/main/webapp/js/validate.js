$(document).keyup(function(e){
	var inputTypes = ['text','password','textarea'],keycode = e.keyCode,
		element = $(e.target),
		type = element.get(0).type,
		isInput = false;
	if('textarea' == type && element.attr('data-no-space')){
		return;
	}
	for(var i in inputTypes){
		isInput = (type == inputTypes[i]) ? true : isInput;
		if(isInput)break;
	}
	if(isInput){
		var v = element.val();
		if(keycode == 32)
			element.val(v.trim());
		if((keycode == 190 || keycode == 188) && e.shiftKey)
			element.val(v.substring(0,v.length - 1));
	}
});

/**
 * 用户名称校验
 */
var validToolObj = {
	classNameByError : 'error_class_valid',
	showTip : function(selector,msg){
		var element = $(selector),
			parent = undefined,
			errorElement = undefined,
			msg = msg || '名称不能为空！',
			tip = undefined;
		
		if(element.length > 0){
			parent = element.parent('div');
			errorElement = $('.' + this.classNameByError,parent);
			if(errorElement.length > 0){
				errorElement.html(msg);
			}else{
				tip = "<span class='help-block " + this.classNameByError + "' style='color:#FF0000;'>" + msg + "</span>";
				parent.append(tip);
			}
		}else
			console.info('not found element by selector:' + selector);
	},
	removeTip : function(selector){
		var element = $(selector),
			parent = element.parent().find('.' + this.classNameByError).remove();
	},
	validNull : function(v){
		return v == undefined || v == null || v.trim().length == 0;
	},
	isNull : function(selector,isValid,max,maxMsg,min,minMsg,msg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			isValid = isValid == false ? false : true,
			msg = msg || '不允许为空！',
			tip = undefined;
		
		if(this.validNull(v)){
			this.showTip(selector,msg);
			return true;
	    }else if(isValid){
	    	return this.length(selector,max,maxMsg,min,minMsg);
	    }
	    else{
	    	validToolObj.removeTip(selector);	    	
	    	return false;
	    }
	    	
	},
	//onkeyup=\"validToolObj.length('#host_desc_edit',200)\"
	length : function(selector,max,maxMsg,min,minMsg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			max = max || 80,
			min = min || 0;
			minMsg = minMsg || '字符最小长度为' + min + '个字符！',
			maxMsg = maxMsg || '字符最大长度为' + max + '个字符！',
			tip = undefined;
		
		if(min > 0){
			if(!v || v.length < min){
				this.showTip(selector,minMsg);
				return true;
			}else{
				validToolObj.removeTip(selector);	    	
		    	return false;
			}
		}
		if(v && v.length > max){
			this.showTip(selector,maxMsg);
			return true;
		}else{
			validToolObj.removeTip(selector);	    	
	    	return false;
		}
	},
	//正整数
	number : function(selector,msg){
		var v = $(selector).val(),
			msg = msg || '格式不正确，只能输入整数！';
		
		if(/^[0-9]\d*$/.test(v)){
			validToolObj.removeTip(selector);
			return false;
	    }else{
	    	this.showTip(selector,msg);
	    	return true;
	    }
	},
	isUserName : function(str){
		var regExp = /^[a-zA-Z0-9_\-.]+$/;
		return !regExp.test(str);
	},
	isEnglishWordAndNumber : function(selector,isValid,max,maxMsg,min,minMsg,msg){
		var element = $(selector),
		v = element.val(),
		parent = undefined,
		errorElement = undefined,
		isValid = isValid == false ? false : true,
		msg = msg || '只能为字母和数字！',
		tip = undefined;
		var regExp = /^[a-zA-Z0-9]+$/;
		if(!regExp.test(v)){
			this.showTip(selector,msg);
			return true;
	    }else if(isValid){
	    	return this.length(selector,max,maxMsg,min,minMsg);
	    }
	    else{
	    	validToolObj.removeTip(selector);	    	
	    	return false;
	    }
	},
	validUserName : function(selector,msg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			msg = msg || '格式不正确，只能输入字母、数字、点符号、下划线、中划线！',
			tip = undefined;
		
		if(this.isUserName(v)){
			this.showTip(selector,msg);
			return true;
	    }else{
	    	validToolObj.removeTip(selector);
	    	return false;
	    }
	},
	validByRegExp : function(regexp,selector,msg){
		if(regexp){
			var element = $(selector),
				v = element.val(),
				msg = msg || '格式不正确，只能输入字母、数字、中划线、下划线！';
			
			if(!regexp.test(v)){
				this.showTip(selector,msg);
				return true;
		    }else{
		    	validToolObj.removeTip(selector);
		    	return false;
		    }
		}
	},
	isName : function(str){
		var regExp = /^[a-zA-Z0-9\u4e00-\u9fa5_\-.]+$/;
		return !regExp.test(str);
	},
	validName : function(selector,msg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			msg = msg || '格式不正确，只能输入中文、字母、数字、点符号、下划线、中划线！',
			tip = undefined;
		
		if(this.isName(v)){
			this.showTip(selector,msg);
			return true;
	    }else{
	    	validToolObj.removeTip(selector);
	    	return false;
	    }
	},
	isIp : function(selector,msg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			msg = msg || 'IP格式不正确，例：192.168.1.1',
			tip = undefined;
		
		if(!isIp(v)){
			this.showTip(selector,msg);
			return true;
	    }else{
	    	validToolObj.removeTip(selector);
	    	return false;
	    }
	},
	isPort : function(selector,msg){
		var element = $(selector),
			v = element.val(),
			parent = undefined,
			errorElement = undefined,
			msg = msg || '端口格式不正确，端口号只能在 1-65536 之间的整数！',
			tip = undefined;
		
		if(v > 0 && v < 65536 && /^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-9][0-9][0-9][0-9]$)/.test(v)){
			validToolObj.removeTip(selector);
			return false;
	    }else{
	    	this.showTip(selector,msg);
	    	return true;
	    }
	},
	isPortByMany : function(selector,msg){
		var element = $(selector),
			v = element.val(),
			prots = v.split(',')
			msg = msg || '端口格式不正确，端口号只能在 1-65536 之间的整数！',
			isOk = true;
		
		$.each(prots,function(){
			if(!(this > 0 && this < 65536 && /^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-5][0-5][0-3][0-5]$)/.test(this))){
				isOk = false;
				return;
			}
		});
		if(isOk){
			validToolObj.removeTip(selector);
			return false;
	    }else{
	    	this.showTip(selector,msg);
	    	return true;
	    }
	},
	/*重命名实时验证*/
	realTimeValid : function(url,id,selector,msg){
		var isOk = false,
			element = $(selector),
			parent = undefined,
			errorElement = undefined,
			name = undefined,
			params = undefined;
		msg = msg || '名称已存在，请重新输入！';
		if(element.length > 0){
			parent = element.parent('div');
			errorElement = $('.' + this.classNameByError,parent);
			name = element.val();
			params = {
				name : name,
				id : id
			};
			$.ajax({
		        type: 'post',
		        url: url,
		        async : false,
		        data : params,
		        dataType: 'json',
		        success: function (response) {
		        	if (response.success) {
						errorElement.remove();
					} else {
						validToolObj.showTip(selector,msg);
						isOk = true;
					}
		        }
			});
		}else
			console.info('not found element : ' + selector);
		return isOk;
	},
	validForm : function(selectorForm,selectorBtn,selectorArray,isValid){
		var element = undefined,
			v = undefined,
			isOk = true;
		if(selectorArray instanceof Array){
			for(var i in selectorArray){
				element = $(selectorArray[i]);
				if(element.length > 0 && element.is(':visible')){
					if(element.is('select')){
						v = $(selectorArray[i] + ' option:selected').val();
					}else{
						v = element.val();
					}
					if(this.validNull(v)){
						isOk = false;
						break;
					}
				}
			}
	    }
		if(isOk)
			isOk = $('.' + validToolObj.classNameByError,selectorForm).length > 0 ? false : true;
			
		if(isValid == false){
			return isOk;
		}else
			if(isOk)
				$(selectorBtn).parent().removeAttr("disabled");
			else{
				$(selectorBtn).parent().attr("disabled","disabled");
			}
		
	}
}

stringTool = {
	chartArray : ["\"","'"],//,"\n","<",">"
	encodeArray : ["&quot;","&quot;"],//,"\\n","&lt;","&gt;"
	escape : function(string,chartArray,encodeArray){
        if (!string || string.length == 0) return "";
        chartArray = chartArray || this.chartArray;
        encodeArray = encodeArray || this.encodeArray;
        
        var temp = undefined,
        	regExp = undefined;
        
        if(chartArray instanceof Array && encodeArray instanceof Array)
        	for(var i in chartArray){
            	temp = chartArray[i];
            	regExp = new RegExp(temp,'g');
            	if(regExp.test(temp)){
            		string = string.replace(regExp, encodeArray[i]);
            	}
            }
        else if(chartArray instanceof Array && encodeArray instanceof String){
        	for(var i in chartArray){
            	temp = chartArray[i];
            	regExp = new RegExp(temp,'g');
            	if(regExp.test(temp)){
            		string = string.replace(regExp, encodeArray);
            	}
            }
        }else{
        	regExp = new RegExp(chartArray,'g');
        	string = string.replace(regExp, encodeArray);
        }
        return string;
	}
};

/**
 * 判断输入内容是否为空
 */
function isNull(str){
	return str.trim().length==0;
}
/**
 * 判断日期类型是否为YYYY-MM-DD格式的类型
 */ 
function isDate(str){
	if(str.length != 0){
		var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/;     
        var r = str.match(reg); 
        return r!=null;
     } else{
    	 return false;
     }
}
/**
 * 判断日期类型是否为YYYY-MM-DD hh:mm:ss格式的类型
 */ 
function isDateTime(str){
    if(str.length!=0){    
        var reg = /^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2}) (\d{1,2}):(\d{1,2}):(\d{1,2})$/;     
        var r = str.match(reg);     
        return r!=null; 
    } else{
    	return false;
    }  
}
/**
 * 判断日期类型是否为hh:mm:ss格式的类型 
 */ 
function isTime(str){     
    if(str.length!=0){    
    	reg = /^((20|21|22|23|[0-1]\d)\:[0-5][0-9])(\:[0-5][0-9])?$/     
        return reg.test(str);
    } else{
    	return false;
    }    
} 
/**
 * 判断输入的字符是否为英文字母 
 */
function isLetter(str){     
    if(str.length!=0){    
    	reg = /^[a-zA-Z]+$/; 
    	return reg.test(str)
    } else{
    	return false;
    }   
} 
/**
 * 判断输入的字符是否为整数   
 */
function isInteger(str){       
    if(str.length != 0){    
	    reg = /^[1-9]\d*$/;     
	    return reg.test(str);
    } else{
    	return false
    }   
}
/**
 * 判断输入的字符是否为双精度   
 */
function isDouble(str){     
    if(str.length!=0){    
	    reg = /^[-\+]?\d+(\.\d+)?$/;    
	    return reg.test(str); 
    } else{
    	return false;
    }    
}
/**
 * 判断输入的字符是否为:a-z,A-Z,0-9 
 */
function isString(str){     
    if(str.length!=0){    
	    reg = /^[a-zA-Z0-9_]+$/;     
	    return reg.test(str);  
    } else{
    	return false;
    }   
}  

/**
 * 判断输入字符是否为中文 
 */
function isChinese(str){     
    if(str.length!=0){    
	    reg = /^[\u0391-\uFFE5]+$/;    
	    return reg.test(str);   
    } else{
    	return false;
    }   
}  
/**
 * 判断输入的EMAIL格式是否正确 
 */
function isEmail(str){     
    if(str.length!=0){    
	    reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;    
	    return reg.test(str);  
    } else{
    	return false;
    }   
}  

/**
 * 判断输入的Url格式是否正确 
 */
function isUrl(str){
	if(str.length!=0){    
	    reg = /^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/;    
	    return reg.test(str);   
    } else{
    	return false;
    }   
}
/**
 * 判断输入的ip格式是否正确 
 */
function isIp(str){
	if(str.length!=0){
		reg = /^([0,1]?\d{0,2}|2[0-4]\d|25[0-5])\.([0,1]?\d{0,2}|2[0-4]\d|25[0-5])\.([0,1]?\d{0,2}|2[0-4]\d|25[0-5])\.([0,1]?\d{0,2}|2[0-4]\d|25[0-5])$/;
		return reg.test(str);
	} else{
		return false;
	}
}
/**
 * 输入是否为特殊字符
 */
function isSpecialChar(str){
	if(str.length != 0){
		reg = /^[\w\u4e00-\u9fa5]+$/gi;
		return reg.test(str);
	} else{
		return false;
	}
}

/**
 * 判断输入的是否为合法的URL路径
 * */
function isHttpUrlStr(str){
	if(str.length != 0){
		reg = /^([fF][tT][pP]:\/\/|[hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/)([A-Za-z0-9-~])+([A-Za-z0-9-~\/])+$/;
		return reg.test(str);
	} else{
		return false;
	}
}

/*****************************************************************
jQuery Validate扩展验证方法  (lining)       
*****************************************************************/
$(function(){
	// 判断整数value是否等于0 
	jQuery.validator.addMethod("isIntEqZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value==0;       
	}, "整数必须为0"); 

	// 判断整数value是否大于0
	jQuery.validator.addMethod("isIntGtZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value>0;       
	}, "整数必须大于0"); 

	// 判断整数value是否大于或等于0
	jQuery.validator.addMethod("isIntGteZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value>=0;       
	}, "整数必须大于或等于0");   

	// 判断整数value是否不等于0 
	jQuery.validator.addMethod("isIntNEqZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value!=0;       
	}, "整数必须不等于0");  

	// 判断整数value是否小于0 
	jQuery.validator.addMethod("isIntLtZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value<0;       
	}, "整数必须小于0");  

	// 判断整数value是否小于或等于0 
	jQuery.validator.addMethod("isIntLteZero", function(value, element) { 
		value=parseInt(value);      
		return this.optional(element) || value<=0;       
	}, "整数必须小于或等于0");  

	// 判断浮点数value是否等于0 
	jQuery.validator.addMethod("isFloatEqZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value==0;       
	}, "浮点数必须为0"); 

	// 判断浮点数value是否大于0
	jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value>0;       
	}, "浮点数必须大于0"); 

	// 判断浮点数value是否大于或等于0
	jQuery.validator.addMethod("isFloatGteZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value>=0;       
	}, "浮点数必须大于或等于0");   

	// 判断浮点数value是否不等于0 
	jQuery.validator.addMethod("isFloatNEqZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value!=0;       
	}, "浮点数必须不等于0");  

	// 判断浮点数value是否小于0 
	jQuery.validator.addMethod("isFloatLtZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value<0;       
	}, "浮点数必须小于0");  

	// 判断浮点数value是否小于或等于0 
	jQuery.validator.addMethod("isFloatLteZero", function(value, element) { 
		value=parseFloat(value);      
		return this.optional(element) || value<=0;       
	}, "浮点数必须小于或等于0");  

	// 判断浮点型  
	jQuery.validator.addMethod("isFloat", function(value, element) {       
		return this.optional(element) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
	}, "只能包含数字、小数点等字符"); 

	// 匹配integer
	jQuery.validator.addMethod("isInteger", function(value, element) {       
		return this.optional(element) || (/^[-\+]?\d+$/.test(value) && parseInt(value)>=0);       
	}, "匹配integer");  

	// 判断数值类型，包括整数和浮点数
	jQuery.validator.addMethod("isNumber", function(value, element) {       
		return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
	}, "匹配数值类型，包括整数和浮点数");  

	// 只能输入[0-9]数字
	jQuery.validator.addMethod("isDigits", function(value, element) {       
		return this.optional(element) || /^\d+$/.test(value);       
	}, "只能输入0-9数字");  

	// 判断中文字符 
	jQuery.validator.addMethod("isChinese", function(value, element) {       
		return this.optional(element) || /^[\u0391-\uFFE5]+$/.test(value);       
	}, "只能包含中文字符。");   

	// 判断英文字符 
	jQuery.validator.addMethod("isEnglish", function(value, element) {       
		return this.optional(element) || /^[A-Za-z]+$/.test(value);       
	}, "只能包含英文字符。");   

	// 手机号码验证    
	jQuery.validator.addMethod("isMobile", function(value, element) {    
		var length = value.length;    
		return this.optional(element) || (length == 11 && /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(value));    
	}, "请正确填写您的手机号码。");

	// 电话号码验证    
	jQuery.validator.addMethod("isPhone", function(value, element) {    
		var tel = /^(\d{3,4}-?)?\d{7,9}$/g;    
		return this.optional(element) || (tel.test(value));    
	}, "请正确填写您的电话号码。");

	// 联系电话(手机/电话皆可)验证   
	jQuery.validator.addMethod("isTel", function(value,element) {   
		var length = value.length;   
		var mobile = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;   
		var tel = /^(\d{3,4}-?)?\d{7,9}$/g;       
		return this.optional(element) || tel.test(value) || (length==11 && mobile.test(value));   
	}, "请正确填写您的联系方式"); 

	// 匹配qq      
	jQuery.validator.addMethod("isQq", function(value, element) {       
		return this.optional(element) || /^[1-9]\d{4,12}$/;       
	}, "匹配QQ");   

	// 邮政编码验证    
	jQuery.validator.addMethod("isZipCode", function(value, element) {    
		var zip = /^[0-9]{6}$/;    
		return this.optional(element) || (zip.test(value));    
	}, "请正确填写您的邮政编码。");  

	// 匹配密码，以字母开头，长度在6-12之间，只能包含字符、数字和下划线。      
	jQuery.validator.addMethod("isPwd", function(value, element) {       
		return this.optional(element) || /^\w+$/.test(value);       
	}, "只能包含字符、数字和下划线。");  

	// 身份证号码验证
	jQuery.validator.addMethod("isIdCardNo", function(value, element) { 
	//var idCard = /^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$/;   
		return this.optional(element) || isIdCardNo(value);    
	}, "请输入正确的身份证号码。"); 

	// IP地址验证   
	jQuery.validator.addMethod("ip", function(value, element) {    
		return this.optional(element) || /^(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.)(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.){2}([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))$/.test(value);    
	}, "请填写正确的IP地址。");
	
	// 邮箱验证   
	jQuery.validator.addMethod("isMail", function(value, element) {    
		return this.optional(element) || /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);    
	}, "请填写正确的邮箱地址。");

	// 字符验证，只能包含中文、英文、数字、下划线等字符。    
	jQuery.validator.addMethod("stringCheck", function(value, element) {       
		return this.optional(element) || /^[a-zA-Z0-9\u4e00-\u9fa5-_]+$/.test(value);       
	}, "只能包含中文、英文、数字、下划线等字符");   

	// 匹配english  
	jQuery.validator.addMethod("isEnglish", function(value, element) {       
		return this.optional(element) || /^[A-Za-z]+$/.test(value);       
	}, "匹配english");   

	// 匹配汉字  
	jQuery.validator.addMethod("isChinese", function(value, element) {       
		return this.optional(element) || /^[\u4e00-\u9fa5]+$/.test(value);       
	}, "匹配汉字");   

	// 匹配中文(包括汉字和字符) 
	jQuery.validator.addMethod("isChineseChar", function(value, element) {       
		return this.optional(element) || /^[\u0391-\uFFE5]+$/.test(value);       
	}, "匹配中文(包括汉字和字符) "); 

	// 判断是否为合法字符(a-zA-Z0-9-_)
	jQuery.validator.addMethod("isRightfulString", function(value, element) {       
		return this.optional(element) || /^[A-Za-z0-9_-]+$/.test(value);       
	}, "只能包含(字母、数字和下划线、破折号)合法字符");   

	// 判断是否包含中英文特殊字符，除英文"-_"字符外
	jQuery.validator.addMethod("isContainsSpecialChar", function(value, element) {  
		var reg = RegExp(/[(\ )(\`)(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\|)(\{)(\})(\')(\:)(\;)(\')(',)(\[)(\])(\.)(\<)(\>)(\/)(\?)(\~)(\！)(\@)(\#)(\￥)(\%)(\…)(\&)(\*)(\（)(\）)(\—)(\+)(\|)(\{)(\})(\【)(\】)(\‘)(\；)(\：)(\”)(\“)(\’)(\。)(\，)(\、)(\？)]+/);   
		return this.optional(element) || !reg.test(value);       
	}, "含有中英文特殊字符");  
	
	// 判断是否是HttpURL地址
	jQuery.validator.addMethod("isHttpUrlString", function(value, element) { 
		var reg = /^([fF][tT][pP]:\/\/|[hH][tT]{2}[pP]:\/\/|[hH][tT]{2}[pP][sS]:\/\/|\/|\/\/)([A-Za-z0-9_\.-~])+$/;
		//var reg = RegExp(strRegex);   
		return this.optional(element) || reg.test(value);       
	}, "应用地址格式错误，请参考：http://tomcat/或者/tom_cat/"); 
	
	// 判断是否是合法端口
	jQuery.validator.addMethod("isValidServerPort", function(value, element) { 
		var reg = /^[1-9]$|(^[1-9][0-9]$)|(^[1-9][0-9][0-9]$)|(^[1-9][0-9][0-9][0-9]$)|(^[1-6][0-5][0-5][0-3][0-5]$)/;
		return this.optional(element) || reg.test(value);       
	}, "端口输入错误，请参考1~65535之间的数字"); 

	//身份证号码的验证规则
	function isIdCardNo(num){ 
		var len = num.length, re; 
		if (len == 15) 
			re = new RegExp(/^(\d{6})()?(\d{2})(\d{2})(\d{2})(\d{2})(\w)$/); 
		else if (len == 18) 
			re = new RegExp(/^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$/); 
		else {
			return false;
		} 
		var a = num.match(re); 
		if (a != null) { 
			if (len==15){ 
				var D = new Date("19"+a[3]+"/"+a[4]+"/"+a[5]); 
				var B = D.getYear()==a[3]&&(D.getMonth()+1)==a[4]&&D.getDate()==a[5]; 
			} 
			else { 
				var D = new Date(a[3]+"/"+a[4]+"/"+a[5]); 
				var B = D.getFullYear()==a[3]&&(D.getMonth()+1)==a[4]&&D.getDate()==a[5]; 
			} 
			if (!B) {
				return false;
			} 
		} 
		if(!re.test(num)){
			return false;
		}
			return true; 
	} 

});
//字符验证，只能包含中文、英文、数字、下划线等字符。    
jQuery.validator.addMethod("stringNameCheck", function(value, element) {       
	return this.optional(element) || /^[a-zA-Z0-9\u4e00-\u9fa5-_]+$/.test(value);       
}, "只能包含中文、英文、数字、下划线等字符"); 