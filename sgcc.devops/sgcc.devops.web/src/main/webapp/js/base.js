$(document).ready(function(){
	bootbox.setDefaults("locale","zh_CN"); 
});

function showMessage(message,callbackFn,id){
	var selectorHtml = id ? ' id="' + id + '"' : '';
	bootbox.dialog({
		message: "<b" + selectorHtml + ">温馨提示</b><hr/><center><b>"+message+"</b></center>", 
		buttons: {
			"success" : {
				"label" : "确定",
				"className" : "btn-sm btn-primary",
				callback: callbackFn
			}
		}
	});
}
Date.prototype.pattern=function(fmt) {         
    var o = {         
    "M+" : this.getMonth()+1, //月份         
    "d+" : this.getDate(), //日         
    "h+" : this.getHours()%12 == 0 ? 12 : this.getHours()%12, //小时         
    "H+" : this.getHours(), //小时         
    "m+" : this.getMinutes(), //分         
    "s+" : this.getSeconds(), //秒         
    "q+" : Math.floor((this.getMonth()+3)/3), //季度         
    "S" : this.getMilliseconds() //毫秒         
    };         
    var week = {         
    "0" : "/u65e5",         
    "1" : "/u4e00",         
    "2" : "/u4e8c",         
    "3" : "/u4e09",         
    "4" : "/u56db",         
    "5" : "/u4e94",         
    "6" : "/u516d"        
    };         
    if(/(y+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));         
    }         
    if(/(E+)/.test(fmt)){         
        fmt=fmt.replace(RegExp.$1, ((RegExp.$1.length>1) ? (RegExp.$1.length>2 ? "/u661f/u671f" : "/u5468") : "")+week[this.getDay()+""]);         
    }         
    for(var k in o){         
        if(new RegExp("("+ k +")").test(fmt)){         
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));         
        }         
    }         
    return fmt;         
}
function confirm(message,callbackOk,callbackCancel){
	bootbox.confirm("<b>操作提示</b><hr><center><b>"+message+"</b></center>", function(result) {
		if(result) {
			callbackOk();
		}else{
			if(null == callbackCancel || undefined == callbackCancel){
				return;
			}
			callbackCancel();
		}
	});
}
function successNotice(title,content){
	notify(title,content,'center','success');
}
function errorNotice(title,content){
	notify(title,content,'center','error');
}
function warnNotice(title,content){
	notify(title,content,'center','warn');
}
function notify(title,content,location,type){
	switch(type){
	case 'success':
		type = 'info';
		break;
	case 'warn':
		type='warning';
		break;
	case 'error':
		type='error';
		break;
	default:
		type='info';
		break;
	}
	$.gritter.add({
		title: title,
		text: '<div class="center"><b>'+content+'</b></center>',
		class_name: 'gritter-'+type+' gritter-light gritter-'+location,
		time:5000
	});
}
function stringFilter(s){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";   
    for (var i = 0; i < s.length; i++) {   
        rs = rs+s.substr(i, 1).replace(pattern, '');   
    } 
    return rs;
}

function logout(){
	confirm("您确认退出系统吗？", function(){
		location.href=base+'logout.html';
	});
}