package com.sgcc.devops.web.image.material;

import java.util.List;

import com.sgcc.devops.web.image.Material;
import com.sgcc.devops.web.image.NginxMeta;

/**
 * Nginx配置文件生成材料
 * 
 * @author dmw
 *
 */
public class NginxMaterial extends Material {
	private List<NginxMeta> metas;

	public List<NginxMeta> getMetas() {
		return metas;
	}

	public void setMetas(List<NginxMeta> metas) {
		this.metas = metas;
	}

	public NginxMaterial(List<NginxMeta> metas) {
		super();
		this.metas = metas;
	}

	public NginxMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

}
