package com.sgcc.devops.web.manager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.MessageResult;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.model.SimpleContainer;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.ObjectUtils;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.intf.ContainerCore;
import com.sgcc.devops.core.util.ContainerOperationResult;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.entity.expand.ContainerExpand;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegImageService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.daemon.PlatformDaemon;
import com.sgcc.devops.web.download.DownLoad;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.TomcatConstants;
import com.sgcc.devops.web.image.WeblogicContstants;
import com.sgcc.devops.web.message.MessagePush;

/**  
 * date：2016年2月4日 下午1:54:33
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ContainerManager.java
 * description：  容器业务流程处理类
 */
@Repository
public class ContainerManager {

	@Autowired
	private ContainerCore containerCore;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private ClusterService clusterService;
	@Autowired
	private HostService hostService;
	@Autowired
	private ConportService conportService;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private DockerHostConfig dockerHostConfig;
	@Autowired
	private DeployService deployService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private ComponentService componentService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private PlatformDaemon platformDaemon;
	@Autowired
	private HostManager hostManager;
	@Autowired
	private DownLoad downLoad;
	@Resource
	private RegImageService regImageService;
	@Resource
	private CompPortManager compPortManager;
	private static Logger logger = Logger.getLogger(ContainerManager.class);
	@Resource
	private MessagePush messagePush;
	@Resource
	private PortService portService;
	@Resource
	private SystemManager systemManager;
	@Resource
	private ComponentExpandService componentExpandService;
	@Resource
	private MonitorManager monitorManager;
	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	public Result addContainer(JSONObject jo) {
		ContainerModel model = (ContainerModel) JSONObject.toBean(jo.getJSONObject("model"), ContainerModel.class);
		if (model != null) {
			Cluster cluster = new Cluster();
			cluster.setClusterId(model.getClusterId());
			cluster = clusterService.getCluster(cluster);
			Host host = hostService.getHost(cluster.getMasteHostId());
			if (null == host) {
				return new Result(false, "创建容器【"+model.getConName()+"】失败，集群【"+cluster.getClusterName()+"】管理节点主机不存在！");
			}
			// 主机密码解密
			String hostPwd = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
			if (StringUtils.isEmpty(hostPwd)) {
				return new Result(false, "创建容器【"+model.getConName()+"】失败，获取集群【"+cluster.getClusterName()+"】管理节点主机信息异常！");
			}
			JSONArray containerArray = new JSONArray();
			int sunccessCons=-1;
			for (int i = 0; i <Integer.parseInt(model.getConNumber()); i++) {
				boolean b = monitorManager.clusterMonitor(cluster.getClusterId());
				model.setConId(KeyGenerator.uuid());
				model.setSystemCode(model.getConName());
				if(b){
					JSONObject jsonObject = containerCore.createContainer(host.getHostIp(), host.getHostUser(), hostPwd,
							cluster.getClusterPort(), model);
					if(jsonObject.getBoolean("issuccess")){
						containerArray.add(jsonObject);
					}else{
						return new Result(false, jsonObject.getString("result"));
					}
				}else{
					sunccessCons=containerArray.size();
				}
			}
			if (containerArray.size() > 0) {
				for (int i = 0; i < containerArray.size(); i++) {
					JSONObject containerJo = containerArray.getJSONObject(i);
					Container container = new Container();
					container.setConId(containerJo.getString("conId"));
					container.setConUuid(containerJo.getString("id"));
					if (i != 0) {
						container.setConName(model.getConName() + "-" + i);
					} else {
						container.setConName(model.getConName());
					}
					container.setConImgid(model.getImageId());
					if (containerJo.getString("status").contains("Up")) {
						container.setConPower((byte) Status.POWER.UP.ordinal());
					} else {
						container.setConPower((byte) Status.POWER.OFF.ordinal());
					}
					container.setConDesc("test docker");
					container.setConStatus((byte) Status.CONTAINER.UP.ordinal());
					container.setConStartCommand(containerJo.getString("command"));
					container.setConStartParam(model.getCreateParams());
					container.setClusterIp(host.getHostIp());
					container.setSysId(model.getSysId());
					container.setClusterPort(cluster.getClusterPort());
					container.setConCreator(jo.getString("userId"));
					TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
					container.setConCreatetime(new Date());
					container.setConTempName(container.getConName());
					container.setConType(model.getConType());
					container.setConEnv(model.getConEnv());
					container.setConCommand(model.getConCommand());
					container.setConPorts(model.getConPorts());
					try {
						JSONArray ports = (JSONArray) containerJo.get("ports");
						if (!ports.isEmpty()) {
							String hostIp = ((JSONObject) ports.get(0)).getString("ip");
							container.setHostId(hostService.getHostByIp(hostIp).getHostId());
							for (int j = 0; j < ports.size(); j++) {
								JSONObject port = (JSONObject) ports.get(j);
								ConPort conPort = new ConPort();
								conPort.setContainerId(container.getConId());
								conPort.setConIp(port.getString("ip"));
								conPort.setPubPort(String.valueOf(port.getInt("publicPort")));
								conPort.setPriPort(String.valueOf(port.getInt("privatePort")));
								conportService.addConports(conPort);
							}
						} else {
							logger.info("Container's port is empty!");
						}
						containerService.addContaier(container);
					} catch (Exception e) {
						logger.info("Create container error: "+e);
						return new Result(false, "创建容器【"+model.getConName()+"】失败：持久化异常:"+e);
					}
				}
				if(sunccessCons>0){
					return new Result(true,"集群【"+cluster.getClusterName()+"】资源使用已达上限！"+ "成功创建"+sunccessCons+"个容器;未创建"+(Integer.parseInt(model.getConNumber())-sunccessCons)+"个容器");
				}else{
					return new Result(true, "创建容器【"+model.getConName()+"】成功");
				}
			} else {
				logger.info("创建容器失败：服务器异常");
				if(sunccessCons>=0){
					return new Result(true,"集群【"+cluster.getClusterName()+"】资源使用已达上限！"+ "成功创建"+sunccessCons+"个容器;未创建"+(Integer.parseInt(model.getConNumber())-sunccessCons)+"个容器");
				}else{
					return new Result(false, "创建容器【"+model.getConName()+"】失败：服务器异常");
				}
			}
		} else {
			logger.info("创建容器失败:传入参数异常");
			return new Result(false, "创建容器失败:传入参数异常");
		}
	}

	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	public Result startContainer(JSONObject jo) {
		String[] containerIds = ObjectUtils.toStringArray(jo.getJSONArray("containerId").toArray());
		List<SimpleContainer> simCons = null;
		try {
			simCons = containerService.selectContainerUuid(containerIds);
		} catch (Exception e) {
			logger.info("Get container infos error", e);
			return new Result(false, "启动容器失败：获取容器详细信息失败！");
		}
		ContainerOperationResult containerResult = containerCore.startContainer(simCons);
		List<SimpleContainer> successContainers = containerResult.getsuccessContainers();
		JSONArray conArray = containerResult.getJsonArray();
		if (successContainers.size() > 0) {
			try {
				ContainerExpand ce = new ContainerExpand();
				ce.setConPower((byte) Status.POWER.UP.ordinal());
				String[] conUuis = new String[successContainers.size()];
				for (int i = 0; i < successContainers.size(); i++) {
					conUuis[i] = successContainers.get(i).getContainerUuid();
				}
				ce.setContainerArray(conUuis);
				containerService.modifyContainerPower(ce);
				for (int i = 0; i < conArray.size(); i++) {
					JSONObject jsonObject = conArray.getJSONObject(i);
					for (int j = 0; j < successContainers.size(); j++) {
						if (jsonObject.getString("id").equals(successContainers.get(j).getContainerUuid())) {
							String conId = successContainers.get(j).getContainerId();
							String[] conIds = { String.valueOf(conId) };
							conportService.removeConports(conIds);
							JSONArray ports = (JSONArray) jsonObject.get("ports");
							for (int k = 0; k < ports.size(); k++) {
								JSONObject port = (JSONObject) ports.get(k);
								ConPort conPort = new ConPort();
								conPort.setContainerId(conId);
								conPort.setConIp(port.getString("ip"));
								conPort.setPubPort(String.valueOf(port.getInt("publicPort")));
								conPort.setPriPort(String.valueOf(port.getInt("privatePort")));
								conportService.addConports(conPort);
							}
						}
					}
				}
			} catch (Exception e) {
				logger.info("Start container error: "+e.getMessage());
				ContainerOperationResult rollbackResult = containerCore.stopContainer(successContainers);
				if (rollbackResult.getsuccessContainers().size() == successContainers.size()) {
					successContainers.clear();
					return new Result(false, "启动容器失败：回滚操作成功！");
				} else {
					logger.info("启动容器失败：回滚操作失败");
					return new Result(false, "启动容器失败：回滚操作失败");
				}
			}
		}
		if (containerResult.getFlag()) {
			return new Result(true, "容器启动成功！");
		} else {
			return new Result(true, "部分容器启动成功！");
		}
	}

	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	public Result stopContainer(JSONObject jo) {
		String[] containerIds = ObjectUtils.toStringArray(jo.getJSONArray("containerId").toArray());

		List<SimpleContainer> simCons = new ArrayList<>();
		String conNames = "";
		for (int i = 0; i < containerIds.length; i++) {
			Container container = new Container();
			container.setConId(containerIds[i]);
			container = containerService.getContainer(container);
			conNames += container.getConName()+" ";
			// 如果容器为启动状态，增加到list中去停止
			SimpleContainer simpleContainer = new SimpleContainer(container.getConUuid(), container.getClusterIp(),
					container.getClusterPort());
			JSONObject joJsonObject = containerCore.containerInfo(simpleContainer);
			if (!joJsonObject.isEmpty()&&joJsonObject.size()>0&&joJsonObject.get("status").toString().toLowerCase().contains("up")) {
				simCons.add(simpleContainer);
			} else {
				ContainerExpand ce = new ContainerExpand();
				ce.setConPower((byte) Status.POWER.OFF.ordinal());
				String[] conUuis = { container.getConUuid() };
				ce.setContainerArray(conUuis);
				try {
					containerService.modifyContainerPower(ce);
				} catch (Exception e) {
					logger.info("Modify container Power state error: "+e.getMessage());
				}
			}
		}
		ContainerOperationResult containerResult = containerCore.stopContainer(simCons);
		List<SimpleContainer> successContainers = containerResult.getsuccessContainers();
		if (successContainers.size() > 0) {
			try {
				ContainerExpand ce = new ContainerExpand();
				ce.setConPower((byte) Status.POWER.OFF.ordinal());
				String[] conUuis = new String[successContainers.size()];
				for (int i = 0; i < successContainers.size(); i++) {
					conUuis[i] = successContainers.get(i).getContainerUuid();
				}
				ce.setContainerArray(conUuis);
				containerService.modifyContainerPower(ce);
			} catch (Exception e) {
				logger.error("Stop container error", e);
				ContainerOperationResult rollbackResult = containerCore.startContainer(successContainers);
				if (rollbackResult.getsuccessContainers().size() == successContainers.size()) {
					successContainers.clear();
					return new Result(false, "停止容器失败：回滚操作成功！");
				} else {
					logger.info("停止容器【"+conNames+"】失败：回滚操作失败");
					return new Result(false, "停止容器停止容器【"+conNames+"】失败：回滚操作失败");
				}
			}
		}
		return new Result(true, "容器停止容器【"+conNames+"】停止成功！");
	}

	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	public Result removeContainer(JSONObject jo) {
		String[] containerIds = ObjectUtils.toStringArray(jo.getJSONArray("containerId").toArray());
		List<SimpleContainer> simCons = null;
		try {
			simCons = containerService.selectContainerUuid(containerIds);
		} catch (Exception e) {
			logger.error("Get container infos error", e);
			return new Result(false, "删除容器失败：获取容器详细信息失败！");
		}
		// 此处没有返回已停止状态的
		ContainerOperationResult containerResult = containerCore.removeContainer(simCons);
		List<SimpleContainer> successContainers = containerResult.getsuccessContainers();
		
		String conNames = "";
		// 如果为停止状态，删除
		for (int i = 0; i < containerIds.length; i++) {
			Container container = new Container();
			container.setConId(containerIds[i]);
			container = containerService.getContainer(container);
			if (null != container && Status.POWER.UP.ordinal() == container.getConPower()
					&& Status.CONTAINER.UP.ordinal() == container.getConStatus()) {
			} else if (null != container) {
				conNames += container.getConName()+" ";
				SimpleContainer simpleContainer = new SimpleContainer(container.getConUuid(), container.getClusterIp(),
						container.getClusterPort());
				successContainers.add(simpleContainer);
			}
		}
		if (!successContainers.isEmpty()&&successContainers.size() > 0) {
			try {
				containerService.removeContainer(containerIds);
				conportService.removeConports(containerIds);
			} catch (Exception e) {
				logger.info("Delete containers error: "+e);
				successContainers.clear();
				return new Result(false, "容器【"+conNames+"】删除操作失败，可能导致数据异常:"+e);
			}
		}
		return new Result(true, "容器【"+conNames+"】删除成功！");
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年10月10日
	 */
	public JSONArray monitorContainer(String[] containerids) {
		List<Container> containers = new ArrayList<>();
		JSONArray array = new JSONArray();
		try {
			containers = containerService.selectContainerId(containerids);
			for (Container container : containers) {
				JSONObject jo = JSONUtil.parseObjectToJsonObject(container);
				String hostId = container.getHostId();
				Host host = hostService.getHost(hostId);
				jo.element("hostStatus", host.getHostStatus());
				array.add(jo);
			}
		} catch (Exception e) {
			logger.error("monitor container status fail: " + e.getMessage());
		}
		return array;
	}

	/**
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	public Result stopAndRemoveCon(JSONObject params) {
		String systemId = params.getString("systemId");
		System system = systemService.getSystemById(systemId);
		try{
			//如果此系统正在执行扩展或者收缩任务，此次任务忽略
			if(null!=system.getOperation()&&Type.OPERATION.ADD.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行扩展任务，此次任务忽略。");
			}
			if(null!=system.getOperation()&&Type.OPERATION.DEL.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行收缩任务，此次任务忽略。");
			}
			//停止守护
			platformDaemon.stopSystemDaemon(systemId);
			system.setOperation((byte)Type.OPERATION.DEL.ordinal());
			systemService.updateSystem(system);
			// 停容器
			Result result = this.stopContainer(params);
			if (result.isSuccess()) {
				// 删除容器
				result = this.removeContainer(params);
				if (result.isSuccess()) {
					Deploy deploy = deployService.getDeploy(system.getDeployId());
					String[] containerIds = ObjectUtils.toStringArray(params.getJSONArray("containerId").toArray());
					// 更新数据库
					deploy.setInstanceCount(deploy.getInstanceCount() < containerIds.length ? 0
							: deploy.getInstanceCount() - containerIds.length);
					if(deploy.getInstanceCount()<deploy.getInitCount()){
						return new Result(false, "容器实例数已达最低限度，不允许删除！");
					}
					deployService.updateDeploy(deploy);
					// 更新nginx
					try {
						// 更新负载
//						CompPort compPort = new CompPort();
//						compPort.setUsedId(deploy.getDeployId());
//						compPort.setUsedType(Type.COMPPORT_TYPE.DEPLOY.toString());
//						List<CompPort> compPorts= portService.selectAllCompPort(compPort);
						Component component = new Component();
						component.setComponentId(deploy.getLoadBalanceId());
						component = componentService.getComponent(component);
						if(component==null){
							logger.error("负载信息获取失败未更新");
							return new Result(false, "负载信息获取失败未更新");
						}
						//单个nginx组件
						if(component.getComponentType()==1){
							Result res = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.REGISTRY.toString());
							if(!res.isSuccess()){
								return res;
							}
						}
						//nginx组组件
						if(component.getComponentType()==6){
							Result res = compPortManager.modifyNginxGroup(deploy.getLoadBalanceId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
							if(!res.isSuccess()){
								return res;
							}
						}
						//f5组件
						if(component.getComponentType()==2){
							ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
							//是否自动配置
							if(componentExpand.getAutoConfigF5()==1){
								// F5测试
								F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
										Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
								boolean re = detector.normal();
								if(!re){
									return new Result(false, "F5服务器通信失败，无法自动配置！");
								}else{
									F5Model f5Model = new F5Model();
									f5Model.setHostIp(component.getComponentIp());
									f5Model.setUser(component.getComponentUserName());
									f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
									f5Model.setPort(deploy.getF5ExternalPort());
									StringBuffer members = new StringBuffer();
									for(String containerid:containerIds){
										List<ConPort> conPorts = conportService.listConPorts(containerid);
										for(ConPort conPort:conPorts){
											members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
										}
									}
									if(StringUtils.isNotEmpty(members.toString())){
										f5Model.setMembers(members.toString());
										f5Model.setPoolName("sgcc_pool");
										result = compPortManager.delF5MembersConfig(f5Model);
										if(!result.isSuccess()){
											return result;
										}
									}
								}
							}
						}
					} catch (Exception e) {
						logger.info("负载更新失败: " + e);
						return new Result(false, "负载更新失败："+e);
					}
				}
			}
			result = new Result(true, "系统【"+system.getSystemName()+"】容器删除成功!");
			return result;
		}catch(Exception e){
			logger.info("系统【"+system.getSystemName()+"】容器删除失败: " + e);
			return new Result(false, "系统【"+system.getSystemName()+"】容器删除失败："+e);
		}finally{
			system.setOperation((byte)Type.OPERATION.UNUSED.ordinal());
			systemService.updateSystem(system);
			platformDaemon.startSystemDaemon(systemId);
		}
		
	}

	/**
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	public Result createConBySysId(JSONObject params) {
		int conCount = params.getInt("conCount");
		String systemId = params.getString("systemId");
		String userId = params.getString("userId");
		System system = systemService.getSystemById(systemId);
		Result result = this.createConBySystem(system, conCount, userId);
		return result;
	}

	private Result createConBySystem(System system, int conCount, String userId) {
		User user = new User();
		user.setUserId(userId);
		try{
			//如果此系统正在执行扩展或者收缩任务，此次任务忽略
			if(null!=system.getOperation()&&Type.OPERATION.ADD.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行扩展任务，此次任务忽略。");
			}
			if(null!=system.getOperation()&&Type.OPERATION.DEL.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行收缩任务，此次任务忽略。");
			}
			system.setOperation((byte)Type.OPERATION.ADD.ordinal());
			systemService.updateSystem(system);
			
			Deploy deploy = deployService.getDeploy(system.getDeployId());
			if (deploy == null) {
				logger.error("Deploy【" + system.getDeployId() + "】 is null");
				return new Result(false, "");
			} else {
				Cluster cluster = new Cluster();
				cluster.setClusterId(deploy.getClusterId());
				cluster = clusterService.getCluster(cluster);
				Host host = hostService.getHost(cluster.getMasteHostId());
				ContainerModel containerModel = new ContainerModel();

				Image appImage = imageService.selectByPrimaryKey(deploy.getAppImageId());
				containerModel.setImageName(appImage.getImageName().toLowerCase() + ":" + appImage.getImageTag());
				containerModel.setConNumber("" + conCount);
				containerModel.setCreateModel("1");
				containerModel.setCpu(deploy.getCpu());
				containerModel.setMemery(deploy.getMemery());
				containerModel.setSystemCode(system.getSystemCode());
				String logPath = null;
				
				//应用依赖的基础镜像
				Image baseImage = imageService.selectByPrimaryKey(deploy.getBaseImageId());
				if (baseImage.getImageName().toLowerCase().contains("tomcat")) {
					logPath = TomcatConstants.LOGS_PATH;
				} else if (baseImage.getImageName().toLowerCase().contains("weblogic")) {
					logPath = WeblogicContstants.LOGS_PATH;
				}
				containerModel.setLogPath(logPath);
				containerModel.setParameter(deploy.getParameter());
				// 查询依赖镜像生成的容器的名称，作为创建容器时命名规则
				List<Container> list = containerService.selectContainersByImageId(appImage.getImageId());
				String conBaseName = "";
				int conOrder = 0;
				if (list.size() > 0) {
					conBaseName = list.get(0).getConTempName().substring(0, list.get(0).getConTempName().lastIndexOf("-"));
					for (Container container : list) {
						int conOrderTemp = Integer.valueOf(
								container.getConTempName().substring(container.getConTempName().lastIndexOf("-") + 1));
						conOrder = conOrderTemp > conOrder ? conOrderTemp : conOrder;
					}

				}else{
					conBaseName = system.getSystemName()+deploy.getDeployVersion();
				}
				// 主机密码解密
				String hostPwd = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());

				// 创建容器
				JSONArray containerArray = new JSONArray();
				int sunccessCons=-1;
				if(conCount>0){
					for (int i = 0; i < conCount&&i<20; i++) {
						String param = systemManager.containerLocation(deploy.getLocationLeavel(), i, deploy.getLocationStragegy(), deploy.getClusterId());
						containerModel.setCreateParams(param);
						containerModel.setConId(KeyGenerator.uuid());
						//检查集群主机是否有剩余资源
						boolean b = monitorManager.clusterMonitor(cluster.getClusterId());
						if(b){
							JSONObject jsonObject = containerCore.createContainer(host.getHostIp(), host.getHostUser(), hostPwd,
									cluster.getClusterPort(), containerModel);
							if(jsonObject.getBoolean("issuccess")){
								containerArray.add(jsonObject);
							}else{
								return new Result(false, jsonObject.getString("result"));
							}
						}else{
							sunccessCons=containerArray.size();
						}
					}
				}
				if (containerArray.size() > 0) {
					for (int i = 0; i < containerArray.size(); i++) {
						JSONObject containerJo = containerArray.getJSONObject(i);
						Container container = new Container();
						container.setConId(containerJo.getString("conId"));
						container.setConUuid(containerJo.getString("id"));
						container.setConName(conBaseName + "-" + (conOrder + i + 1));
						container.setConImgid(appImage.getImageId());
						if (containerJo.getString("status").contains("Up")) {
							container.setConPower((byte) Status.POWER.UP.ordinal());
						} else {
							container.setConPower((byte) Status.POWER.OFF.ordinal());
						}
						container.setConDesc("test docker");
						container.setConStatus((byte) Status.CONTAINER.UP.ordinal());
						container.setConStartCommand(containerJo.getString("command"));
						container.setSysId(deploy.getSystemId());
						container.setClusterIp(host.getHostIp());
						container.setClusterPort(cluster.getClusterPort());
						container.setConCreator(userId);
						TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
						container.setConCreatetime(new Date());
						container.setConTempName(container.getConName());
						container.setConType(Type.CON_TYPE.APP.ordinal());
						container.setConEnv(cluster.getClusterEnv());
						try {
							String hostIp = "";
							JSONArray ports = (JSONArray) containerJo.get("ports");
							if (!ports.isEmpty()) {
								hostIp = ((JSONObject) ports.get(0)).getString("ip");
								container.setHostId(hostService.getHostByIp(hostIp).getHostId());
								containerService.addContaier(container);
								for (int j = 0; j < ports.size(); j++) {
									JSONObject port = (JSONObject) ports.get(j);
									ConPort conPort = new ConPort();
									conPort.setContainerId(container.getConId());
									conPort.setConIp(port.getString("ip"));
									conPort.setPubPort(String.valueOf(port.getInt("publicPort")));
									conPort.setPriPort(String.valueOf(port.getInt("privatePort")));
									conportService.addConports(conPort);
								}
							} else {
								containerService.addContaier(container);
								logger.info("Container's port is empty!");
							}
						} catch (Exception e) {
							logger.info("Create container error "+e);
							return new Result(false, "系统【"+system.getSystemName()+"】创建容器失败：持久化异常。"+e);
						}
					}
					// 更新数据库
					deploy.setInstanceCount(deploy.getInstanceCount() + conCount);
					deployService.updateDeploy(deploy);
					// 更新nginx
					try {
						// 更新负载
//						CompPort compPort = new CompPort();
//						compPort.setUsedId(deploy.getDeployId());
//						compPort.setUsedType(Type.COMPPORT_TYPE.DEPLOY.toString());
//						List<CompPort> compPorts= portService.selectAllCompPort(compPort);
						Component component = new Component();
						component.setComponentId(deploy.getLoadBalanceId());
						component = componentService.getComponent(component);
						if(component==null){
							logger.error("负载信息获取失败未更新");
							return new Result(false, "负载信息获取失败未更新");
						}
						//单个nginx组件
						if(component.getComponentType()==1){
							Result res = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.REGISTRY.toString());
							if(!res.isSuccess()){
								return res;
							}
						}
						//nginx组组件
						if(component.getComponentType()==6){
							Result res = compPortManager.modifyNginxGroup(deploy.getLoadBalanceId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
							if(!res.isSuccess()){
								return res;
							}
						}
						//f5组件
						if(component.getComponentType()==2){
							ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
							//是否自动配置
							if(componentExpand.getAutoConfigF5()==1){
								// F5测试
								F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
										Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
								boolean re = detector.normal();
								if(!re){
									return new Result(false, "F5服务器通信失败，无法自动配置！");
								}else{
									F5Model f5Model = new F5Model();
									f5Model.setHostIp(component.getComponentIp());
									f5Model.setUser(component.getComponentUserName());
									f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
									f5Model.setPort(deploy.getF5ExternalPort());
									StringBuffer members = new StringBuffer();
									if (containerArray.size() > 0) {
										// String conIds = "";
										for (int i = 0; i < containerArray.size(); i++) {
											JSONObject containerJo = containerArray.getJSONObject(i);
											try {
												JSONArray ports = (JSONArray) containerJo.get("ports");
												if (!ports.isEmpty()) {
													String hostIp = ((JSONObject) ports.get(0)).getString("ip");
													for (int j = 0; j < ports.size(); j++) {
														JSONObject port = (JSONObject) ports.get(j);
														String conPort = String.valueOf(port.getInt("publicPort"));
														members.append(hostIp+":"+conPort+";");
													}
												}
											} catch (Exception e) {
												logger.info("F5配置异常 "+e.getMessage());
											}

										}
									}
									if(StringUtils.isNotEmpty(members.toString())){
										f5Model.setMembers(members.toString());
										f5Model.setPoolName("sgcc_pool");
										Result result = compPortManager.addF5MembersConfig(f5Model);
										if(!result.isSuccess()){
											return result;
										}
									}
								}
							}
						}					} catch (Exception e) {
						logger.error("负载更新失败" + e);
						return new Result(true, "负载更新失败");
					}
					if(sunccessCons>0){
						return new Result(true,"集群【"+cluster.getClusterName()+"】资源使用已达上限！"+ "系统【"+system.getSystemName()+"】成功创建"+sunccessCons+"个容器;未创建"+(conCount-sunccessCons)+"个容器");
					}else{
						return new Result(true, "系统【"+system.getSystemName()+"】创建容器成功");
					}
				} else {
					if(sunccessCons>=0){
						return new Result(true,"集群【"+cluster.getClusterName()+"】资源使用已达上限！"+ "系统【"+system.getSystemName()+"】成功创建"+sunccessCons+"个容器;未创建"+(conCount-sunccessCons)+"个容器");
					}else{
						return new Result(false, "系统【"+system.getSystemName()+"】创建容器失败：服务器异常");
					}
				}
			}
		}catch (Exception e) {
			logger.error("系统【"+system.getSystemName()+"】创建容器失败:"+e);
			return new Result(true, "系统【"+system.getSystemName()+"】创建容器失败:"+e);
		} finally{
			system.setOperation((byte)Type.OPERATION.UNUSED.ordinal());
			systemService.updateSystem(system);
		}
		
	}

	
	/**
	 * 更新负载 
	 * 
	 * @author zhangxin
	 * @return boolean
	 * @version 1.0 2015年11月27日
	 */
	public Boolean checkName(String id, String name) {
		Container container = new Container();
		container.setConName(name);
		container = containerService.getContainer(container);
		if (container != null) {
			if (id == null || id.equals("")) {
				// 创建时 找到containerName为name的host对象
				return false;
			} else if (container.getConId().equals(id)) {
				// 更新时，找到自身
				return true;
			} else {
				// 更新时，name重复
				return false;
			}
		} else {
			// 没有containerName为name的host对象
			return true;
		}
	}

	/**
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年12月3日
	 */
	public Result stopAndRemoveConByCount(String systemId, String containerid, String deployId) {
		String[] containerids = { containerid };
		JSONObject params = new JSONObject();
		params.put("systemId", systemId);
		params.put("containerId", containerids);
		Result result = this.stopAndRemoveCon(params);
		if (null == result) {
			logger.info("删除成功");
			return new Result(true, "删除成功");
		} else {
			logger.info("删除成功");
			return result;
		}

	}

	/**
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年12月14日
	 */
	public Result clear(String hostId, User user) {
		Host host = null;
		try {
			List<Container> containers = containerService.listContainersByHostId(hostId);
			host = hostService.getHost(hostId);
			//所属集群中的主机个数，如果除了swarm外没有其他docker节点，即目前这个集群中有2个主机，清空节点后，
			//所有部署在这个集群的应用实例个数改为0，状态改为已下线。相关物理系统的正在运行的版本值为空。
			if(null==host){
				return new Result(false, "获取主机信息异常");
			}
			List<Host> hosts = hostService.listHostByClusterId(host.getClusterId());
			if (null != containers && containers.size() > 0) {
				// 判断状态，如果启动状态，先停止
				// List<SimpleContainer> simCons = new ArrayList<>();
				String[] containerIds = new String[containers.size()];
				Set<String> systemSet = new HashSet<String>();
				for (int i = 0; i < containers.size(); i++) {
					containerIds[i] = containers.get(i).getConId();
					if (null != containers.get(i).getSysId() && !containers.get(i).getSysId().isEmpty()) {
						systemSet.add(containers.get(i).getSysId());
					}
				}
				if (null != systemSet && systemSet.size() > 0) {
					for (String systemId : systemSet) {
						platformDaemon.stopSystemDaemon(systemId);
						if(hosts.size()<=2){
							//物理系统部署置为未部署
							System system = systemService.getSystemById(systemId);
							system.setDeployId("");
							system.setSystemDeployStatus((byte)Status.SYSTEM.UNDEPLOY.ordinal());
							systemService.updateSystem(system);
						}
					}
				}
				JSONObject jo = new JSONObject();
				jo.put("containerId", containerIds);
				Result result = this.stopContainer(jo);
				if (result.isSuccess()) {
					result = this.removeContainer(jo);
					if (result.isSuccess()) {
						if (null != host && null != host.getClusterId()) {
							String[] hostIds = { host.getClusterId() + ":" + hostId };
							result = hostManager.clusterRemoveHost(hostIds, user);
						}
						if (result.isSuccess()) {
							for (String systemId : systemSet) {
								platformDaemon.startSystemDaemon(systemId);
							}
						} else {
							for (String systemId : systemSet) {
								platformDaemon.startSystemDaemon(systemId);
							}
							return result;
						}
					} else {
						for (String systemId : systemSet) {
							platformDaemon.startSystemDaemon(systemId);
						}
						return result;
					}
				} else {
					if (null != systemSet && systemSet.size() > 0) {
						for (String systemId : systemSet) {
							platformDaemon.startSystemDaemon(systemId);
						}
					}
					return new Result(false, "主机【"+host.getHostIp()+"】清除容器失败，容器【"+Arrays.toString(containerIds)+"】停止失败");
				}
			}
		} catch (Exception e) {
			logger.error("主机清空容器失败: "+e);
			return new Result(false, "主机清空容器失败："+e);
		}

		return new Result(true, "主机【"+host.getHostIp()+"】清空操作成功！");
	}

	/**
	* @author mayh
	* @return Result
	* @version 1.0
	* 2015年12月22日
	*/
	public void download(String conId,String systemCode,String logName, HttpServletRequest request,
			HttpServletResponse response,User user) {
		try {
			String localPath = localConfig.getLocalLogPath()+conId+"_"+systemCode+"/";
			downLoad.download(localPath+logName,logName, request, response);
			deleteFile(localPath);
		} catch (Exception e) {
			messagePush.pushMessage(user.getUserId(),"");
			pushMessage(user.getUserId(), new MessageResult(false, "日志文件下载失败：日志文件传输出错。", "提示"));
		}
	}
	private void pushMessage(final String userId, final MessageResult message) {
		messagePush.pushMessage(userId, JSONObject.fromObject(message).toString());
	}
	/**
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年1月6日
	*/
	public Result createConByDeployId(String deployId, int conCount,
			String userId) {
		System system = new System();
		system.setDeployId(deployId);
		system = systemService.selectSystem(system);
		return this.createConBySystem(system, conCount, userId);
	}

	/**
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年1月14日
	*/
	public JSONArray loadLogs(String conId,String systemCode) {
		JSONArray ja = new JSONArray();
		//登录到容器宿主机
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		if (ssh.connect()) {
			try {
				String logs = ssh.executeWithResult("ls "+dockerHostConfig.getHostAppLogPath()+conId+"_"+systemCode, 3000);
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(new ByteArrayInputStream(logs.getBytes())));
				try {
					String line = null;
					while ((line = reader.readLine()) != null) {
						JSONObject jo = new JSONObject();
						jo.put("logs", line);
						jo.put("fullpath", dockerHostConfig.getHostAppLogPath()+conId+"_"+systemCode+"/"+line);
						ja.add(jo);
					}
				} catch (IOException e) {
					logger.error( e);
				} finally {
					try {
						reader.close();
					} catch (IOException e) {
					}
				}
			} catch (Exception e) {
				logger.error("宿主机执行ls查询日志目录时出错！" + e);
			}finally{
				ssh.close();
			}
		} else {
			logger.error("容器宿主机登录失败！" );
		}
		return ja;
	}

	/**
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年1月14日
	*/
	public Result scpLogs(String conId,String systemCode, String remotePath) {
		//登录到容器宿主机
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		String localPath = localConfig.getLocalLogPath()+conId+"_"+systemCode;
		boolean b = true;
		try{
			if(ssh.connect()){
				//从远程docker容器主机上下载到本地目录
				localPath = FileUploadUtil.check(localPath);
				File direction = new File(localPath);
				if (!direction.exists()) {
					direction.mkdirs();
				}
				b = ssh.GetFile(remotePath, localPath);
				if(!b){
					logger.error("日志文件下载失败：日志文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
					return new Result(false, "日志文件下载失败：日志文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
				}
			}else{
				logger.error("登录容器宿主机失败。");
				return new Result(false, "登录容器宿主机失败。");
			}
		}catch(Exception e){
			logger.error("日志下载失败："+e);
			return new Result(false, "日志下载失败："+e);
		}finally{
			ssh.close();
		}
		
		return new Result(true, "操作成功");
	}
	
	/*dcoker commit制作镜像*/
	public JSONObject commitContainer(String conId,String conUuid,String imageName,String imageTag){
		JSONObject jo = new JSONObject();
		//登录到容器所在的宿主机
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		if (ssh.connect()) {
			try {
				String commandCommit = "docker commit "+ conUuid + " " + imageName+":"+imageTag;
				ssh.executeWithResult(commandCommit, 100000);
			} catch (Exception e) {
				logger.error("宿主机执行docker commit提交镜像时出错！" + e);
			}finally{
				ssh.close();
			}
		} else {
			logger.error("宿主机登录失败！" );
		}
		jo = saveContainer(conId,imageName,imageTag);
		return jo;
	}
	
	public JSONObject saveContainer(String conId, String imageName,String imageTag) {
		JSONObject jo = new JSONObject();
		jo.put("success", false);
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(),
						localConfig.getSecurityPath()));
		String loadImagePath = dockerHostConfig.getLoadImagePath();
		if (ssh.connect()) {
			String commandSave = "mkdir -p "+loadImagePath+";docker save " + imageName+":"+imageTag + " > "+loadImagePath+"/"
					+ imageName+"_"+imageTag + ".tar";
			String result;
			try {
				result = ssh.executeWithResult(commandSave, 500000);
				if (StringUtils.isEmpty(result)) {
					jo.put("success", true);
					jo.put("fullpath", loadImagePath+"/" + imageName+"_"+imageTag + ".tar");
				} else {
					logger.error("宿主机执行docker save保存镜像时出错！" + result);
					jo.put("message", "宿主机执行docker save保存镜像时出错！" + result);
				}
			} catch (Exception e) {
				logger.error("宿主机执行docker save保存镜像时出错！" + e);
				jo.put("message", "宿主机执行docker save保存镜像时出错！" + e.getCause());
			} finally {
				ssh.close();
			}
		} else {
			logger.error("容器宿主机登录失败！");
			jo.put("message", "容器宿主机登录失败！");
		}
		return jo;
	}
	
	/*scp到平台服务器*/
	public Result scpConImage(String conId,String remotePath,String imageName,String imageTag) {
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		String localPath = localConfig.getLocalImagePath();
		String loadImagePath = dockerHostConfig.getLoadImagePath();
		boolean b = true;
		try{
			if(ssh.connect()){
				//从远程仓库主机上下载到本地目录
				File direction = new File(localPath);
				if (!direction.exists()) {
					direction.mkdirs();
				}
				//下载到本地目录之前，先对本地目录进行清空
				deleteAll(direction);
				b = ssh.GetFile(remotePath, localPath);
				//scp到平台成功，则删除宿主机路径/tmp/下的tar包
				if(b){
					String commandLine = "rm -rf "+loadImagePath+"/*";
					String e = ssh.executeWithResult(commandLine, 3000);
					if(!StringUtils.isEmpty(e)){
						logger.error("宿主机删除镜像tar包时出错！" + e);
						return new Result(false,"宿主机删除镜像tar包时出错！");
					}
				}
				if(!b){
					logger.error("镜像下载失败：镜像文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
					return new Result(false, "镜像下载失败：镜像文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
				}
			}else{
				logger.error("登录仓库主机失败。");
				return new Result(false, "登录仓库主机失败。");
			}
		}catch(Exception e){
			logger.error("镜像下载失败："+e);
			return new Result(false, "镜像下载失败："+e);
		}finally{
			ssh.close();
		}
		
		return new Result(true, "操作成功");
	}
	
	/*下载到本地*/
	public void downloadConImage(String conName,HttpServletRequest request,
			HttpServletResponse response,User user) {
//		String imageName = repoName + "/" + conName;
		try {
			String localPath = localConfig.getLocalImagePath();
			downLoad.download(localPath+conName+".tar",conName+".tar", request, response);
			deleteFile(localPath);
		} catch (Exception e) {
			messagePush.pushMessage(user.getUserId(),"");
			pushMessage(user.getUserId(), new MessageResult(false, "镜像文件下载失败：镜像文件传输出错。", "提示"));
		}
	}
	
	//删除某个目录下所有的文件
	private static boolean deleteAll(File file) {
		try{
		   File[] files = file.listFiles(); 
		   if(null==files){
			   return true;
		   }
		   for (int i = 0; i < files.length; i++){    
		   files[i].delete();      
		   }
		   return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}	
	
	private static boolean deleteFile(String outputFile) {
		File file = null;
		try {
			file = new File(outputFile);
			if (file.exists())
				file.delete();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public Result restartContaner(JSONObject params) {
		String systemId = params.getString("systemId");
		platformDaemon.stopSystemDaemon(systemId);
		System system = systemService.getSystemById(systemId);
		Result result = null;
		try{
			//如果此系统正在执行扩展或者收缩任务，此次任务忽略
			if(null!=system.getOperation()&&Type.OPERATION.ADD.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行扩展任务，此次任务忽略。");
			}
			if(null!=system.getOperation()&&Type.OPERATION.DEL.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行收缩任务，此次任务忽略。");
			}
			if(null!=system.getOperation()&&Type.OPERATION.RESTART.ordinal()==system.getOperation()){
				return new Result(false, "系统【"+system.getSystemName()+"】正在执行重启任务，此次任务忽略。");
			}
			Deploy deploy = deployService.getDeploy(system.getDeployId());
			//重启容器，重启失败后再创建
			result = this.startContainer(params);
			if(!result.isSuccess()){
				//起容器，如果启动成功，删除原容器；如果失败，操作结束
				result = this.createConBySysId(params);
				if(!result.isSuccess()){
					return result;
//				}else{
//					String[] containerIds = ObjectUtils.toStringArray(params.getJSONArray("containerId").toArray());
//					// 更新数据库
//					deploy.setInstanceCount(deploy.getInstanceCount() < deploy.getInitCount() ? deploy.getInitCount()
//							: deploy.getInstanceCount() - containerIds.length);
//					deployService.updateDeploy(deploy);
//					
//					if(containerIds.length==0){
//						return result;
//					}
				}
				// 强制删除容器
				result = this.removeContainer(params);
			}
//			// 更新nginx
			try {
				// 更新负载
				Component component = new Component();
				component.setComponentId(deploy.getLoadBalanceId());
				component = componentService.getComponent(component);
				if(component==null){
					logger.error("负载信息获取失败未更新");
					return new Result(false, "负载信息获取失败未更新");
				}
				//单个nginx组件
				if(component.getComponentType()==1){
					Result res = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.REGISTRY.toString());
					if(!res.isSuccess()){
						return res;
					}
				}
				//nginx组组件
				if(component.getComponentType()==6){
					Result res = compPortManager.modifyNginxGroup(deploy.getLoadBalanceId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
					if(!res.isSuccess()){
						return res;
					}
				}
				//f5组件
				if(component.getComponentType()==2){
					ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
					//是否自动配置
					if(componentExpand.getAutoConfigF5()==1){
						// F5测试
						F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
								Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
						boolean re = detector.normal();
						if(!re){
							return new Result(false, "F5服务器通信失败，无法自动配置！");
						}else{
							F5Model f5Model = new F5Model();
							f5Model.setHostIp(component.getComponentIp());
							f5Model.setUser(component.getComponentUserName());
							f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
							f5Model.setPort(deploy.getF5ExternalPort());
							StringBuffer members = new StringBuffer();
							String[] containerIds = ObjectUtils.toStringArray(params.getJSONArray("containerId").toArray());
							for(String containerid:containerIds){
								List<ConPort> conPorts = conportService.listConPorts(containerid);
								for(ConPort conPort:conPorts){
									members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
								}
							}
							if(StringUtils.isNotEmpty(members.toString())){
								f5Model.setMembers(members.toString());
								f5Model.setPoolName("sgcc_pool");
								result = compPortManager.delF5MembersConfig(f5Model);
								if(!result.isSuccess()){
									return result;
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.error("负载更新失败" + e);
				return new Result(true, "负载更新失败");
			}
			result = new Result(true, "系统【"+system.getSystemName()+"】容器删除成功!");
			return result;
		}catch(Exception e){
			logger.info("系统【"+system.getSystemName()+"】容器删除失败: " + e);
			return new Result(false, "系统【"+system.getSystemName()+"】容器删除失败："+e);
		}finally{
			platformDaemon.startSystemDaemon(systemId);
			system.setOperation((byte)Type.OPERATION.UNUSED.ordinal());
			systemService.updateSystem(system);
		}
	}
	public Result checkHostContaniners(User user) {
		Host temp = new Host();
		temp.setHostEnv(user.getUserCompany());
		List<Host> hosts = hostService.selectAllHost(temp);
		for(Host host:hosts){
			String hostIp=host.getHostIp();
//			String hostUser = host.getHostUser();
//			String hostSecure =Encrypt.decrypt(host.getHostSecure(), deployConfig.getSecurityPath());
			HttpClient client = new HttpClient();
			String re = client.get(null, "http://"+hostIp+":2375/containers/json");
			JSONArray jsonArray = JSONUtil.parseObjectToJsonArray(re);
			for(int i = 0;i<jsonArray.size();i++){
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				if(jsonObject.isNullObject()||jsonObject.isEmpty()){
					continue;
				}
				String conid = jsonObject.getString("Id");
				Container con = new Container();
				con.setConUuid(conid);
				con = containerService.getContainer(con);
				if(con!=null&&con.getConId()!=null){
					System system = systemService.getSystemById(con.getSysId());
					if(null!=system&&null!=system.getDeployId()&&con.getConType()==Type.CON_TYPE.APP.ordinal()){
						Deploy deploy = deployService.getDeploy(system.getDeployId());
						if(!deploy.getAppImageId().equals(con.getConImgid())){
							con.setConType(Type.CON_TYPE.OTHER.ordinal());
							try {
								containerService.modifyContainer(con);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
//					}else{
//						con.setConType(Type.CON_TYPE.OTHER.ordinal());
//						try {
//							containerService.modifyContainer(con);
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
					}
					continue;
				}
				String conName = jsonObject.getString("Names");
//				String ImageName = jsonObject.getString("Image");
				String ImageID = jsonObject.getString("ImageID");
				String Command = jsonObject.getString("Command");
				String Ports = jsonObject.getString("Ports");
				String status = jsonObject.getString("Status");
				Container container = new Container();
				container.setConId(KeyGenerator.uuid());
				container.setConUuid(conid);
				container.setConName(conName);
				container.setConImgid(ImageID);
				if (status.contains("Up")) {
					container.setConPower((byte) Status.POWER.UP.ordinal());
				} else {
					container.setConPower((byte) Status.POWER.OFF.ordinal());
				}
				container.setConDesc("load");
				container.setConStatus((byte) Status.CONTAINER.UP.ordinal());
				container.setConStartCommand(Command);
				String clusterid= host.getClusterId();
				Cluster cluster = clusterService.getCluster(clusterid);
				if(cluster!=null){
					String clusterMasterHostId = cluster.getMasteHostId();
					Host clusterMasterHost = hostService.getHost(clusterMasterHostId);
					container.setClusterIp(clusterMasterHost.getHostIp());
					container.setClusterPort(cluster.getClusterPort());
				}
				container.setConCreator(user.getUserId());
				TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
				container.setConCreatetime(new Date());
				container.setConTempName(container.getConName());
				container.setConType(Type.CON_TYPE.OTHER.ordinal());
				container.setConEnv(user.getUserCompany());
				try {
					JSONArray ports = JSONUtil.parseObjectToJsonArray(Ports);
					if (!ports.isEmpty()) {
						container.setHostId(host.getHostId());
						containerService.addContaier(container);
						for (int j = 0; j < ports.size(); j++) {
							JSONObject port = (JSONObject) ports.get(j);
							ConPort conPort = new ConPort();
							conPort.setContainerId(container.getConId());
							conPort.setConIp(host.getHostIp());
							conPort.setPubPort(String.valueOf(port.getInt("PublicPort")));
							conPort.setPriPort(String.valueOf(port.getInt("PrivatePort")));
							conportService.addConports(conPort);
						}
					} else {
						containerService.addContaier(container);
						logger.info("Container's port is empty!");
					}
				} catch (Exception e) {
					logger.info("Create container error "+e);
					new Result(false, "检查执行失败："+e.getMessage());
				}
			}
		}
		return new Result(true, "检查执行成功");
	}
}
