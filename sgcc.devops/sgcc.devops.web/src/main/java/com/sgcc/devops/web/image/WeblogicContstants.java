package com.sgcc.devops.web.image;

/**
 * Weblogic常用路径常量类
 * @author dmw
 *
 */
public class WeblogicContstants {

	/**
	 * 基础目录
	 */
	public static final String BASE_PATH = "/root/Oracle/Middleware/user_projects/domains/base_domain/";
	/**
	 * WAR包放置目录
	 */
	public static final String WAR_PATH = BASE_PATH + "deploy/";
	/**
	 * 配置文件放置目录
	 */
	public static final String CONFIG_PATH = BASE_PATH + "config/";
	/**
	 * jdbc文件放置目录
	 */
	public static final String JDBC_PATH = CONFIG_PATH + "jdbc/";

	public static final String START_CMD = "[\"/run.sh\"]";

	/**
	 * 数据库驱动包位置
	 */
	public static final String DRIVER_PATH = BASE_PATH + "lib/";
	/**
	 * 日志文件存放位置位置
	 */
	public static final String LOGS_PATH = BASE_PATH + "servers/AdminServer/logs/";
	
	public static final String BIN_PATH = BASE_PATH + "bin/";
}
