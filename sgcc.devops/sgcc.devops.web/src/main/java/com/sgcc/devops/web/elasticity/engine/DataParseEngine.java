package com.sgcc.devops.web.elasticity.engine;

import java.util.List;

import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.web.elasticity.bean.LoadData;

/**
 * 数据处理引擎 抽象数据分析处理类
 * 
 * @author dmw
 *
 */
public abstract class DataParseEngine {

	public abstract LoadData parse(List<Monitor> datas);
}
