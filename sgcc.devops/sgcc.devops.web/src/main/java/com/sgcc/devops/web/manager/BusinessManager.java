package com.sgcc.devops.web.manager;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Business;
import com.sgcc.devops.dao.entity.BusinessSys;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.BusinessService;
import com.sgcc.devops.service.BusinessSysService;
import com.sgcc.devops.service.SystemService;

/**  
 * date：2016年2月4日 下午1:45:18
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessManager.java
 * description：业务系统操作流程处理类
 */
@Component
public class BusinessManager {
	private static Logger logger = Logger.getLogger(BusinessManager.class);
	@Resource
	private BusinessService businessService;
	@Resource
	private SystemService systemService;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Resource
	private BusinessSysService businessSysService;
	//业务系统数据同步
	public Result loadBusiness(User user) {
		logger.info("Synchronized Business Start!");
		try {
//			String business = AppframeRestOrgClient.getInstance().getAllSystem();
			String business = "{\"SYSTEM\":[{\"CODE\":\"tttt\",\"ID\":\"841119\",\"NAME\":\"tttt\"},{\"CODE\":\"GHXT_OLD\",\"ID\":\"841300\",\"NAME\":\"工会工作管理系统2222\"},"
					+ "{\"CODE\":\"Dlqx\",\"ID\":\"841797\",\"NAME\":\"气象灾害监测预警\"},{\"CODE\":\"GHXT1\",\"ID\":\"11000006084984\",\"NAME\":\"工会工作管理信息系统\"},"
					+ "{\"CODE\":\"SGERP-PS\",\"ID\":\"300454437\",\"NAME\":\"业务系统PS\"},{\"CODE\":\"SGERP-PM\",\"ID\":\"300454438\",\"NAME\":\"业务系统PM\"},"
					+ "{\"CODE\":\"SGERP-MM\",\"ID\":\"300454439\",\"NAME\":\"业务系统MM\"},{\"CODE\":\"SGERP-SPTR\",\"ID\":\"10000003138236\",\"NAME\":\"供应商产品技术参数库SPTR\"},"
					+ "{\"CODE\":\"SGERP-HR-test\",\"ID\":\"10000003125389\",\"NAME\":\"业务系统HR-test\"},{\"CODE\":\"PMS_APP\",\"ID\":\"10000004539571\",\"NAME\":\"PMS2.0接入APPFRAME系统\"},"
					+ "{\"CODE\":\"asasasas\",\"ID\":\"845162\",\"NAME\":\"sasasa\"},{\"CODE\":\"APP_YDZNPT13\",\"ID\":\"845179\",\"NAME\":\"移动智能平台13\"},"
					+ "{\"CODE\":\"aa\",\"ID\":\"845202\",\"NAME\":\"aa\"},{\"CODE\":\"test_common\",\"ID\":\"845288\",\"NAME\":\"测试应用1\"},"
					+ "{\"CODE\":\"test_xz\",\"ID\":\"845289\",\"NAME\":\"测试行政应用\"},{\"CODE\":\"testsystem1\",\"ID\":\"845410\",\"NAME\":\"测用业务应用1\"},"
					+ "{\"CODE\":\"testgh\",\"ID\":\"845460\",\"NAME\":\"测试工会应用\"},{\"CODE\":\"TYRWCS\",\"ID\":\"845516\",\"NAME\":\"统一任务测试\"},"
					+ "{\"CODE\":\"HRMIS2\",\"ID\":\"11000005985710\",\"NAME\":\"新版人力资源\"}]}";
			JSONObject businessJO =  JSONUtil.parseObjectToJsonObject(business);
			//业务系统
			JSONArray businessJA = JSONUtil.parseObjectToJsonArray(businessJO.get("SYSTEM"));
			this.saveBatchBusiness(businessJA);
			logger.info("Synchronized Business End!");
			
			logger.info("Synchronized PhySystem Start!");
//			String phySystem = AppframeRestOrgClient.getInstance().getPhySystem();
			String phySystem = "{\"PSYSTEM\":[{\"PSYSNAME\":\"企信公众号管理系统\",\"SYSID\":\"11000006197673\",\"PSYSID\":\"864299\",\"PSYSCODE\":\"QXGZHGL\"},"
					+ "{\"PSYSNAME\":\"移动智能中台管理\",\"SYSID\":\"11000006197673\",\"PSYSID\":\"11000006197674\",\"PSYSCODE\":\"YDZNZTGL\"},"
					+ "{\"PSYSNAME\":\"全省集中考勤数据分析管理平台\",\"SYSID\":\"11000005985710\",\"PSYSID\":\"11000006187404\",\"PSYSCODE\":\"11000006187404\"},"
					+ "{\"PSYSNAME\":\"保险系统\",\"SYSID\":\"11000005985710\",\"PSYSID\":\"11000006037531\",\"PSYSCODE\":\"HR_BX\"},"
					+ "{\"PSYSNAME\":\"葛鑫\",\"SYSID\":\"11000005985710\",\"PSYSID\":\"11000006086760\",\"PSYSCODE\":\"gex\"},"
					+ "{\"PSYSNAME\":\"刘善德\",\"SYSID\":\"11000005985710\",\"PSYSID\":\"11000006086870\",\"PSYSCODE\":\"LSD\"},"
					+ "{\"PSYSNAME\":\"全员绩效\",\"SYSID\":\"11000005985710\",\"PSYSID\":\"11000006041553\",\"PSYSCODE\":\"HR_JX\"},"
					+ "{\"PSYSNAME\":\"PS\",\"SYSID\":\"845202\",\"PSYSID\":\"40000001081691\",\"PSYSCODE\":\"SAP-PS\"},"
					+ "{\"PSYSNAME\":\"电力信息管理系统\",\"SYSID\":\"11000006086141\",\"PSYSID\":\"11000006086143\",\"PSYSCODE\":\"DLXXMIS.MAIN\"},"
					+ "{\"PSYSNAME\":\"电力信息管理\",\"SYSID\":\"11000006086142\",\"PSYSID\":\"841300\",\"PSYSCODE\":\"DLXXMIS.MAIN\"},"
					+ "{\"PSYSNAME\":\"pw_sd\",\"SYSID\":\"10000000026773\",\"PSYSID\":\"865527\",\"PSYSCODE\":\"pw_sd\"}]}";
			JSONObject phySystemJO =  JSONUtil.parseObjectToJsonObject(phySystem);
			JSONArray phySystemJA = JSONUtil.parseObjectToJsonArray(phySystemJO.get("PSYSTEM"));
			this.saveBatchSys(phySystemJA);
			logger.info("Synchronized PhySystem End!");
			
		} catch (Exception e) {
			logger.error("同步业务系统出错:"+ e);
			return new Result(true, "同步业务系统出错:"+e);
		}
		
		return new Result(true, "同步业务系统成功！");
	}
	/**
	 *
	* TODO 批量插入业务系统
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月30日
	 * @throws Exception 
	 */
	private void saveBatchBusiness(final JSONArray businessJA) throws Exception{
		StringBuffer businessSql= new StringBuffer();
		
		List<Business> batchArgs = new ArrayList<Business>();
		for(int i=0;i<businessJA.size();i++){
			
			JSONObject jsonObject=businessJA.getJSONObject(i);
//			String code = jsonObject.getString("CODE");//code值
			final String id = jsonObject.getString("ID");//id
			final String name = jsonObject.getString("NAME");//名称
			Business business2 = businessService.load(id);
			//不存在，增加；存在名称变化，修改
			if(null==business2){
				business2 =new Business();
				business2.setBusinessId(id);
				business2.setBusinessName(name);
				batchArgs.add(business2);
			}else{
				if(!name.equals(business2.getBusinessName())){
					business2.setBusinessName(name);
					businessService.update(business2);
				}
				
			}
		}
		businessSql.append(" insert into dop_business(BUSINESS_ID,BUSSINESS_NAME) values (?,?)");
		jdbcTemplate.batchUpdate(
				businessSql.toString(),
		        batchArgs,
		        batchArgs.size(),
		        new ParameterizedPreparedStatementSetter<Business>() {
		          @Override
				public void setValues(PreparedStatement ps, Business argument) throws SQLException {
		            ps.setString(1, argument.getBusinessId());
		            ps.setString(2, argument.getBusinessName());
		          }
		        });
	}
	/**
	 *
	* TODO 批量插入物理系统
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月30日
	 */
	private void saveBatchSys(JSONArray phySystemJA) throws Exception{
		//物理系统
		StringBuffer phySql= new StringBuffer();
		List<System> systems = new ArrayList<>();
		List<BusinessSys> businessSysList= new ArrayList<>();
		Set<String> psysids = new HashSet<String>();
		for(int i=0;i<phySystemJA.size();i++){
			JSONObject jsonObject=phySystemJA.getJSONObject(i);
			String psysid = jsonObject.getString("PSYSID");//物理系统ID
			String businessId = jsonObject.getString("SYSID");//对应业务系统id
			String name = jsonObject.getString("PSYSNAME");//名称
//			String code = jsonObject.getString("PSYSCODE");//code值
			System system = systemService.getSystemById(psysid);
			
			//不存在，增加；存在名称变化，修改,重复去掉
			if(null==system&&!psysids.contains(psysid)){
				system = new System();
				system.setSystemId(psysid);
				system.setSystemName(psysid);
				systems.add(system);
			}else{
				if(null!=system&&!name.equals(system.getSystemName())){
					system.setSystemName(name);
					systemService.updateSystem(system);
				}
			}
			psysids.add(psysid);
			
			BusinessSys businessSys = new BusinessSys(businessId, psysid);
			List<BusinessSys> list = businessSysService.load(businessSys);
			//如果为空，新增
			if(null==list||list.size()==0){
				businessSys.setId(KeyGenerator.uuid());
				businessSysList.add(businessSys);
			}
		}
		
		phySql.append(" insert into dop_system(SYSTEM_ID,SYSTEM_NAME,SYSTEM_DEPLOY_STATUS,SYSTEM_ELASTICITY_STATUS) values (?,?,2,0) ");
		jdbcTemplate.batchUpdate(
				phySql.toString(),
				systems,
				systems.size(),
		        new ParameterizedPreparedStatementSetter<System>() {
		          @Override
				public void setValues(PreparedStatement ps, System argument) throws SQLException {
		            ps.setString(1, argument.getSystemId());
		            ps.setString(2, argument.getSystemName());
		          }
		        });
		
		saveBatchBusinessSys(businessSysList);
	}
	/**
	* TODO 批量插入关联关系
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月30日
	 */
	private void saveBatchBusinessSys(List<BusinessSys> businessSysList) throws Exception{
		String busphySql="insert into dop_bus_sys(ID,BUSINESS_ID,SYS_ID) values (?,?,?) ";
		jdbcTemplate.batchUpdate(
				busphySql,
				businessSysList,
				businessSysList.size(),
		        new ParameterizedPreparedStatementSetter<BusinessSys>() {
		          @Override
				public void setValues(PreparedStatement ps, BusinessSys argument) throws SQLException {
		        	  ps.setString(1, argument.getId());
		        	  ps.setString(2, argument.getBusinessId());
		        	  ps.setString(3, argument.getSysId());
		          }
		        });
	}
}
