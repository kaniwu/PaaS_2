package com.sgcc.devops.web.interceptor;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Authority;
import com.sgcc.devops.dao.entity.Log;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.LogService;
import com.sgcc.devops.service.UserService;
import com.sgcc.devops.web.manager.AuthorityManager;
import com.sgcc.devops.web.util.LogConvertCache;
import com.sgcc.devops.web.util.RequestUtil;

/**
 * 访问拦截器
 * 
 * @author mingwei.dmw
 *
 */
@Repository
public class AccessInterceptor implements HandlerInterceptor {
	private static Logger logger = Logger.getLogger(AccessInterceptor.class);

	
	private static ConcurrentHashMap<String, Log> logCache = new ConcurrentHashMap<String, Log>(100, 0.9f);// 日志信息缓存
	private Map<String, String> dictionary;
	private List<String> ignoreUrls;
	@Autowired
	private LogService logService;
	@Autowired
	private LogConvertCache logConvertCache;
	@Autowired
	private UserService userService;
	@Resource
	private AuthorityManager authorityManager;

	@PostConstruct
	public void init() {
		dictionary = logConvertCache.getCache();
	}

	public void setIgnoreUrls(List<String> ignoreUrls) {
		this.ignoreUrls = ignoreUrls;
	}

	public List<String> getIgnoreUrls() {
		return ignoreUrls;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		boolean chkReferer = checkReferer(request, response);
		if(!chkReferer){
			response.setCharacterEncoding("UTF-8");//设置编码
			response.setContentType("text/html");//服务器响应类型
			PrintWriter out = response.getWriter();
			out.print("<script>alert('服务异常，存在跨站请求伪造！');document.location.href='"+request.getContextPath() +"/login.html';</script>");
			out.close();
			return false;
		}
		String requestPath = request.getServletPath();
		//集成自动化运维添加
		String token = request.getParameter("token");
		if(org.apache.commons.lang.StringUtils.isNotEmpty(token)){
			User user = new User();
			user.setUserId(token);
			user = userService.getUser(user);
			if(null==user){
				response.setCharacterEncoding("UTF-8");//设置编码
				response.setContentType("text/html");//服务器响应类型
				PrintWriter out = response.getWriter();
				out.print("<script>alert('会话超时，请重新登录！');document.location.href='"+request.getContextPath() +"/login.html';</script>");
				out.close();
				return false;
			}else{
				//获取用户第一个一级菜单权限
				String startPage=new String();
				boolean startFlag=true;
				//【三】：获取用户权限信息，存储在session中
				List<String> authlist=new ArrayList<String>();
				List<Authority> listAuths = authorityManager.getUserRoleAuths(user.getUserId());
				if(listAuths.size()==0){
					logger.warn("登录失败：用户没有相关权限！");
					return false;
				}
				//pages： 一级菜单权限、  buttons：二级按钮权限
				String pages = "";
				String buttons = "";
				for(Authority ahr:listAuths){
					if(ahr.getActionRelativeUrl()!=null){
						authlist.add(ahr.getActionRelativeUrl());
					}
					if(ahr.getActionType() == Status.AUTHTYPE.PAGE.ordinal()){
						pages += ahr.getActionRemarks()+",";
						if(startFlag){
							startPage=ahr.getActionRelativeUrl();
							startFlag=false;
						}
					}else if(ahr.getActionType() == Status.AUTHTYPE.BUTTON.ordinal()){
						buttons += ahr.getActionRemarks()+",";
					}
				}
				//用户登录成功，设置用户状态为登录状态()
				try {
					user.setUserLoginStatus(request.getSession().getId());
					userService.update(user);
				} catch (Exception e) {
					logger.error("get user by username["+user.getUserName()+"] falied!", e);
					return false;
				}
				
				//权限存储到session中（pagesAuth、buttonsAuth控制界面的权限显示效果，authlist 权限url过滤）
				request.getSession().setAttribute("pagesAuth", pages);
				request.getSession().setAttribute("buttonsAuth", buttons);
				request.getSession().setAttribute("authlist", authlist);
				
				//用户信息和项目url存储在session中
				request.getSession().setAttribute("user", user);
				String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() +  request.getContextPath()
						+ "/";
				request.getSession().setAttribute("basePath", basePath);
//				return true;
			}
		}
		
		
		User user = (User) request.getSession().getAttribute("user");
		//user为null时，返回到登录页面。
		if (null == user && !requestPath.contains("login")&& !requestPath.contains("captcha")) {
			if (null != ignoreUrls) {
				for (String url : ignoreUrls) {
					if (requestPath.contains(url)) {
						return true;
					}
				}
			}
			response.setCharacterEncoding("UTF-8");//设置编码
			response.setContentType("text/html");//服务器响应类型
			PrintWriter out = response.getWriter();
			out.print("<script>alert('会话超时，请重新登录！');document.location.href='"+request.getContextPath() +"/login.html';</script>");
			out.close();
			return false;
		}
		if (requestPath.contains(".html")||requestPath.isEmpty() || requestPath.equals("/")) {
			if (null == user && !requestPath.contains("login")) {
				response.sendRedirect(request.getContextPath() + "/login.html");
				return false;
			}
			return true;
		}
		if (null != ignoreUrls) {
			for (String url : ignoreUrls) {
				if (requestPath.contains(url)) {
					return true;
				}
			}
		}
		String accessIp = RequestUtil.getRemoteHost(request);
		Enumeration<String> params = request.getParameterNames();
		StringBuffer requestContent = new StringBuffer();
		if (null != params) {
			while (params.hasMoreElements()) {
				String param = params.nextElement();
				String value = request.getParameter(param);
				requestContent.append(param).append(":").append(value).append("|");
			}
		}
		if (requestPath.isEmpty() || requestPath.equals("/")) {
		} else {
			String logObj = requestPath.split("/")[1];
			if (dictionary.containsKey(logObj)) {
				logObj = dictionary.get(requestPath.split("/")[1]);
			}
			String logAction = requestPath;
			if (dictionary.containsKey(requestPath)) {
				logAction = dictionary.get(requestPath);
				TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
				Log log = new Log(KeyGenerator.uuid(), logObj, logAction, null, user.getUserId(), user.getUserName(),
						accessIp, new Date(), requestContent.toString());
				logCache.put(log.getLogId(), log);
				request.setAttribute("accessLog", log.getLogId());
			}
		}
		return true;

	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String logId = (String) request.getAttribute("accessLog");
		if (StringUtils.hasText(logId)) {
			Log log = logCache.get(logId);
			if (null == log) {
				return;
			}
			String logDetail = (String) request.getAttribute("logDetail");
			String success = (String) request.getAttribute("success");
			if(!StringUtils.isEmpty(logDetail)){
				log.setLogDetail(logDetail);
			}
			if(!StringUtils.isEmpty(success)){
				log.setLogResult(success);;
			}else{
				String logResult = "" + response.getStatus();
				if (dictionary.containsKey(logResult)) {
					logResult = dictionary.get(logResult);
				}
				log.setLogResult(logResult);
			}
			
			logService.save(log);
			logCache.remove(logId);
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}
	
	private boolean checkReferer(HttpServletRequest request, HttpServletResponse response){
		String referer = request.getHeader("referer");
		String server = request.getServerName();
		if(null!=referer && !referer.contains(server)){
			return false;
		}
		return true;
	}
}
