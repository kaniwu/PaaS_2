package com.sgcc.devops.web.upload;

import com.sgcc.devops.web.message.Message;
import com.sgcc.devops.web.message.MessageType;

/**
 * 上传服务消息
 * 
 * @author dmw
 *
 */
public class UploadMessage implements Message {
	private String messageType;
	private String content;
	private String appFilePath;//应用包上传相对路径
	private String appUuid;//app应用包 uuid

	private String libraryFilePath;//预加载包应用包上传相对路径
	private String libraryUuid;//预加载包 uuid
	
	private String hostCompFilePath;
	private String hostCompUUid;
	private String hostInsallPath;
	@Override
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public UploadMessage(String content) {
		this.setMessageType(MessageType.UP_FILE);
		this.setContent(content);
	}
	
	public UploadMessage(String content,String appFilePath) {
		this.setMessageType(MessageType.UP_FILE);
		this.setContent(content);
		this.setAppFilePath(appFilePath);
	}
	
	public UploadMessage(String content,String appFilePath,String appUuid) {
		this.setMessageType(MessageType.UP_FILE);
		this.setContent(content);
		this.setAppFilePath(appFilePath);
		this.setAppUuid(appUuid);
	}
	
	public String getAppFilePath() {
		return appFilePath;
	}

	public void setAppFilePath(String appFilePath) {
		this.appFilePath = appFilePath;
	}

	public String getAppUuid() {
		return appUuid;
	}

	public void setAppUuid(String appUuid) {
		this.appUuid = appUuid;
	}

	public String getLibraryFilePath() {
		return libraryFilePath;
	}

	public void setLibraryFilePath(String libraryFilePath) {
		this.libraryFilePath = libraryFilePath;
	}

	public String getLibraryUuid() {
		return libraryUuid;
	}

	public void setLibraryUuid(String libraryUuid) {
		this.libraryUuid = libraryUuid;
	}

	public String getHostCompFilePath() {
		return hostCompFilePath;
	}

	public void setHostCompFilePath(String hostCompFilePath) {
		this.hostCompFilePath = hostCompFilePath;
	}

	public String getHostCompUUid() {
		return hostCompUUid;
	}

	public void setHostCompUUid(String hostCompUUid) {
		this.hostCompUUid = hostCompUUid;
	}

	public String getHostInsallPath() {
		return hostInsallPath;
	}

	public void setHostInsallPath(String hostInsallPath) {
		this.hostInsallPath = hostInsallPath;
	}
	
}
