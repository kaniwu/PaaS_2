package com.sgcc.devops.web.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.web.monitor.MonitorDateFileTask;

/**
 * 监控数据归档调度任务
 * @author dmw
 *
 */
@Component
public class MonitorFileSchedule {

	@Autowired
	private MonitorDateFileTask monitorDateFileTask;

	/**
	 * 每天0:0:0,进行数据归档
	 */
	// 秒 分 时 天 月 周 年
	@Scheduled(cron = "${monitor.filedate:0 0 0 * * ?}")
	protected void executeFileMonitor() {
		monitorDateFileTask.monitorDateFile();
	}

}
