package com.sgcc.devops.web.manager;

import net.sf.json.JSONObject;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.service.ComponentTypeService;

/**  
 * date：2016年2月4日 下午1:53:55
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentTypeManager.java
 * description：  组件类型操作处理类
 */
@Component
public class ComponentTypeManager {
	@Resource
	private ComponentTypeService componetTypeService;

	private static Logger logger = Logger.getLogger(ComponentTypeManager.class);

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result createComponentType(JSONObject jsonObject) {
		int result = componetTypeService.createComponentType(jsonObject);
		if (result == 1) {
			logger.info("Create componentType success");
			return new Result(true, "添加组件类型成功");
		} else {
			logger.error("Create componentType error");
			return new Result(false, "添加组件类型失败");
		}
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result deleteComponentType(JSONObject jsonObject) {
		int result = componetTypeService.deleteComponentType(jsonObject);
		if (result == 1) {
			logger.info("delete componentType success");
			return new Result(true, "删除组件类型成功");
		} else {
			logger.error("delete componentType failed");
			return new Result(false, "删除组件类型失败");
		}
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result deleteComponentTypes(JSONObject jsonObject) {
		int result = componetTypeService.deleteComponentTypes(jsonObject);
		if (result == 1) {
			logger.info("delete componentTypes success");
			return new Result(true, "删除组件类型成功");
		} else {
			logger.error("delete componentTypens failed");
			return new Result(false, "删除组件类型失败");
		}
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result updateComponentType(JSONObject jsonObject) {
		int result = componetTypeService.updateComponentType(jsonObject);
		if (result == 1) {
			logger.info("update componentType success");
			return new Result(true, "更新组件类型成功");
		} else {
			logger.error("update componentType failed");
			return new Result(false, "更新组件类型失败");
		}
	}
}
