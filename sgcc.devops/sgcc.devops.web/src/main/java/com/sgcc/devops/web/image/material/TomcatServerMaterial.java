package com.sgcc.devops.web.image.material;

import java.util.List;

import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Material;

/**Tomcat Server.xml材料类
 * @author dmw
 *
 */
public class TomcatServerMaterial extends Material {

	private List<DataSource> dataSources;

	public List<DataSource> getDataSources() {
		return dataSources;
	}

	public void setDataSources(List<DataSource> dataSources) {
		this.dataSources = dataSources;
	}

	public TomcatServerMaterial(List<DataSource> dataSources) {
		super();
		this.dataSources = dataSources;
	}

	public TomcatServerMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

}
