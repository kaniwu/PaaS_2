/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.LocationModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.service.LocationService;

/**  
 * date：2016年3月16日 上午9:31:47
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ServerRoomManager.java
 * description：  
 */
@Component
public class LocationManager {
	@Resource
	private LocationService locationService;
	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年3月16日
	*/
	public JSONArray treeList() {
		JSONArray ja = new JSONArray();
		JSONObject jo = new JSONObject();
		jo.put("id", 0);
		jo.put("name", "机房管理");
		jo.put("pId", null);
		ja.add(jo);
		ja = this.getNodesByParent("0",ja);
		return ja;
	}
	
	private JSONArray getNodesByParent(String parentId,JSONArray ja){
		List<Location> locations = locationService.getLocationByParent(parentId);
		for(Location location:locations){
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("id", location.getId());
			jsonObject.put("name", location.getLocationName());
			jsonObject.put("pId", location.getLocationParent());
			ja.add(jsonObject);
			if(location.getLocationLevel()==1){//如果不是叶子节点，递归
				ja = getNodesByParent(location.getId(),ja);
			}
		}
		return ja;
	}

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	public Result create(LocationModel locationModel) {
		locationModel.setLocationLevel((byte)0);//新增为叶子节点
		Result result = locationService.create(locationModel);
		if(!result.isSuccess()){
			return result;
		}
		//修改父节点层级为非叶子节点
		if(!locationModel.getLocationParent().equals("0")){
			Location update = new Location();
			update.setId(locationModel.getLocationParent());
			update = locationService.select(update);
			update.setLocationLevel((byte)1);
			result = locationService.update(update);
		}
		if(result.isSuccess()){
			return new Result(true, "添加位置【"+locationModel.getLocationName()+"】成功！");
		}else{
			return result;
		}
		
	}

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	public Result update(LocationModel locationModel) {
		Location update = new Location();
		BeanUtils.copyProperties(locationModel, update);
		return locationService.update(update);
	}

	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月6日
	*/
	public Result delete(LocationModel locationModel) {
		return locationService.delete(locationModel);
	}

	/**
	* TODO
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年4月6日
	*/
	public JSONObject select(LocationModel locationModel) {
		Location location  = new Location();
		BeanUtils.copyProperties(locationModel, location);
		location = locationService.select(location);
		if(location==null){
			return new JSONObject();
		}
		BeanUtils.copyProperties(location, locationModel);
		if(location.getLocationParent().equals("0")){
			locationModel.setLocationParentName("机房管理");
		}else{
			Location parent = new Location();
			parent.setId(location.getLocationParent());
			parent = locationService.select(parent);
			locationModel.setLocationParentName(parent.getLocationName());
		}
		
		return JSONUtil.parseObjectToJsonObject(locationModel);
	}
}
