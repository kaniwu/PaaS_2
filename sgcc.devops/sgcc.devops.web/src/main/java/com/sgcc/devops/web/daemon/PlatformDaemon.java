package com.sgcc.devops.web.daemon;

import java.io.File;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.MonitorConfig;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.manager.ContainerManager;

/**
 * 平台守护类
 * 
 * @author dmw
 *
 */
@Repository("platformDaemon")
public class PlatformDaemon {
	private static Logger logger = Logger.getLogger(PlatformDaemon.class);
	private static ConcurrentHashMap<String, SystemDaemon> systemDaemon = new ConcurrentHashMap<String, SystemDaemon>(
			10, 0.9f);
	private static ConcurrentHashMap<String, SwarmDaemon> clusterDaemon = new ConcurrentHashMap<String, SwarmDaemon>(10,
			0.9f);
	private static ConcurrentHashMap<String, DockerDaemon> dockerDaemon = new ConcurrentHashMap<String, DockerDaemon>(10,
					0.9f);
	@Autowired
	private ClusterService clusterService;
	@Autowired
	private HostService hostService;
	@Autowired
	private HostCore hostCore;
	@Autowired
	private HostMapper hostMapper;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private MonitorConfig monitorConfig;
	@Autowired
	private ContainerManager containerManager;
	@Autowired
	private SystemService systemService;
	@Autowired
	private DeployService deployService;
	@Autowired
	private RegistryService registryService;
	@Autowired
	private ContainerService containerService;
	/**
	 * 启动时打开守护线程
	 */
	@PostConstruct
	public void onStart() {
		startSystemDaemon();
		startClusterDaemon();
		startDockerDaemon();
		//创建默认目录
		createTmpDir();
		logger.info("platform daemon start");
	}

	/**
	 * 销毁时停掉守护线程
	 */
	@PreDestroy
	public void destory() {
		stopClusterDaemon();
		stopSystemDaemon();
		stopDockerDaemon();
		logger.info("platform daemon stop");
	}

	public void startSystemDaemon(String systemId) {
		if(systemId==null){
			return;
		}
		System system = this.systemService.getSystemById(systemId);
		if (null == system) {
			logger.error("启动系统【" + systemId + "】 守护线程失败：系统不存在！");
			return;
		}
		SystemDaemon daemon = this.createSystemDaemon(system);
		if (null == daemon) {
			logger.error("启动系统【" + systemId + "】 守护线程失败：守护任务生成失败！");
			return;
		}
		daemon.start();
		systemDaemon.put(systemId, daemon);
	}

	public void stopSystemDaemon(String systemId) {
		if(systemId==null){
			return;
		}
		SystemDaemon daemon = systemDaemon.get(systemId);
		if (null != daemon) {
			daemon.stopDeamon();
			systemDaemon.remove(systemId);
		}
	}

	public void startClusterDaemon(String clusterId) {
		Cluster cluster = new Cluster();
		cluster.setClusterId(clusterId);
		cluster = this.clusterService.getCluster(cluster);
		if (null == cluster) {
			logger.error("启动集群【" + clusterId + "】 守护线程失败：集群不存在！");
			return;
		}
		SwarmDaemon daemon = this.createClusterDaemon(cluster);
		if (null == daemon) {
			logger.error("启动集群【" + clusterId + "】 守护线程失败：守护任务创建失败！");
			return;
		}
		daemon.start();
		clusterDaemon.put(clusterId, daemon);
	}

	public void stopClusterDaemon(String clusterId) {
		if(clusterId==null){
			return;
		}
		SwarmDaemon daemon = clusterDaemon.get(clusterId);
		if (null != daemon) {
			daemon.stopDeamon();
			clusterDaemon.remove(clusterId);
		}
	}

	private SystemDaemon createSystemDaemon(System system) {
		Deploy deplogy = deployService.getDeploy(system.getDeployId());
		if (null == deplogy || !StringUtils.hasText(deplogy.getClusterId())) {
			logger.error("创建系统【" + system.getSystemName() + "】 守护任务失败：部署记录不存在！");
			return null;
		}
		Cluster cluster = new Cluster();
		cluster.setClusterId(deplogy.getClusterId());
		cluster = clusterService.getCluster(cluster);
		if (null == cluster || !StringUtils.hasText(cluster.getMasteHostId())) {
			logger.error("创建系统【" + system.getSystemName() + "】 守护任务失败：系统所在集群不存在！");
			return null;
		}
		Host masterHost = this.hostService.getHost(cluster.getMasteHostId());
		if (null == masterHost) {
			logger.error("创建系统【" + system.getSystemName() + "】 守护任务失败：集群管理主机不存在！");
			return null;
		}
		SystemDaemon daemon = new SystemDaemon(false, masterHost.getHostIp(), cluster.getClusterPort(),
				system.getSystemId(), containerManager, systemService,deployService,containerService,monitorConfig);
		return daemon;
	}

	private SwarmDaemon createClusterDaemon(Cluster cluster) {
		Host host = this.hostService.getHost(cluster.getMasteHostId());
		if (null == host) {
			logger.error("创建集群【" + cluster.getClusterName() + "】守护线程失败：所在主机异常！该主机可能已被删除或断电！");
			return null;
		} else {
			String password = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
			if (StringUtils.hasText(password)) {
				String config = "/home/" + host.getHostUser() + "/" + cluster.getManagePath();
				SwarmDaemon daemon = new SwarmDaemon(host.getHostIp(), cluster.getClusterPort(), config,
						host.getHostUser(), password, host.getHostPort(),cluster.getClusterId(), localConfig.getSecurityPath(),
						hostMapper, hostService, hostCore, clusterService);
				return daemon;
			} else {
				logger.error("创建集群【" + cluster.getClusterName() + "】守护线程失败：集群主机密码解密失败！");
				return null;
			}
		}
	}

	private void startSystemDaemon() {
		List<System> systems = systemService.listDeployedSystem();// 获取所有已经部署的物理系统
		if (null == systems || systems.isEmpty()) {
			logger.warn("当前平台不存在需要守护的物理系统！");
			return;
		}
		for (System system : systems) {
			SystemDaemon daemon = this.createSystemDaemon(system);
			if (null == daemon) {
				continue;
			}
			daemon.start();
			systemDaemon.put(system.getSystemId(), daemon);
		}
	}

	private void stopSystemDaemon() {
		if (!systemDaemon.isEmpty()) {
			for (String key : systemDaemon.keySet()) {
				systemDaemon.get(key).stopDeamon();
			}
			systemDaemon.clear();
		}
	}

	private void startClusterDaemon() {
		List<Cluster> clusters = this.clusterService.getAllCluster();
		if (null == clusters || clusters.isEmpty()) {
			logger.warn("当前平台不存在需要守护的集群！");
			return;
		}
		for (Cluster cluster : clusters) {
			SwarmDaemon daemon = this.createClusterDaemon(cluster);
			if (null == daemon) {
				continue;
			}
			daemon.start();
			clusterDaemon.put(cluster.getClusterId(), daemon);
		}
	}

	private void stopClusterDaemon() {
		if (!clusterDaemon.isEmpty()) {
			for (String key : clusterDaemon.keySet()) {
				clusterDaemon.get(key).stopDeamon();
			}
			clusterDaemon.clear();
		}
	}
	
	private void startDockerDaemon(){
		List<Host> hosts = this.hostService.allOnlineHost();
		if(null==hosts||hosts.isEmpty()){
			logger.warn("当前平台不存在在线的docker主机！");
			return;
		}
		for(Host host :hosts){
			DockerDaemon daemon = this.createDockerDaemon(host);
			if (null == daemon) {
				continue;
			}
			daemon.start();
			dockerDaemon.put(host.getHostId(), daemon);
		}
	}

	/**
	* TODO
	* @author mayh
	* @return ConcurrentHashMap<String,DockerDaemon>
	* @version 1.0
	* 2016年3月15日
	*/
	private DockerDaemon createDockerDaemon(Host host) {
		String password = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		String regId = host.getRegId();
		DockerDaemon daemon = null;
		if(!StringUtils.isEmpty(regId)){
			//docker daemon指定仓库主机
			Registry registry = new Registry();
			registry.setRegistryId(regId);
			registry = registryService.selectRegistry(registry);
			if(null!=registry){
				Host regHost = hostService.getHost(registry.getHostId());
				if(null!=regHost){
					daemon = new DockerDaemon(host.getHostIp(), "2375",host.getHostUser(), password,
							localConfig.getSecurityPath(), hostService, hostCore, clusterService,regHost.getHostIp(),registry.getRegistryPort());
				}
			}
		}
		if(null==daemon){
			daemon = new DockerDaemon(host.getHostIp(), "2375",host.getHostUser(), password,
					localConfig.getSecurityPath(), hostService, hostCore, clusterService,null,null);
		}
		return daemon;
	}
	private void stopDockerDaemon() {
		if (!dockerDaemon.isEmpty()) {
			for (String key : dockerDaemon.keySet()) {
				dockerDaemon.get(key).stopDeamon();
			}
			dockerDaemon.clear();
		}
	}
	public void stopDockerDaemon(String hostId) {
		DockerDaemon daemon = dockerDaemon.get(hostId);
		if (null != daemon) {
			daemon.stopDeamon();
			dockerDaemon.remove(hostId);
		}
	}
	//平台默认创建目录
	private void createTmpDir(){
		String localBaseImagePath = localConfig.getLocalBaseImagePath();
		if(!mkdir(localBaseImagePath)){
			java.lang.System.out.println("基础镜像包上传时临时存放目录创建失败！");
		}
		String localHostInstallPath = localConfig.getLocalHostInstallPath();
		if(!mkdir(localHostInstallPath)){
			java.lang.System.out.println("主机组件安装包上传临时存放目录创建失败！");
		}
		String localLibraryPath = localConfig.getLocalLibraryPath();
		if(!mkdir(localLibraryPath)){
			java.lang.System.out.println("数据库预加载包上传临时存放目录创建失败！");
		}
		String localAppPath = localConfig.getLocalAppPath();
		if(!mkdir(localAppPath)){
			java.lang.System.out.println("应用包管理上传包临时存放目录创建失败！");
		}
		String localDeployPath = localConfig.getLocalDeployPath();
		if(!mkdir(localDeployPath)){
			java.lang.System.out.println("应用部署时产生的存放配置文件临时存放目录创建失败！");
		}
//		String localDBDriverPath = localConfig.getLocalDBDriverPath();
//		if(!mkdir(localDBDriverPath)){
//			java.lang.System.out.println("数据库驱动临时存放目录创建失败！");
//		}
		String localNginxConfPath = localConfig.getLocalNginxConfPath();
		if(!mkdir(localNginxConfPath)){
			java.lang.System.out.println("应用部署nginx配置文件临时存放目录创建失败！");
		}
		String localLogPath = localConfig.getLocalLogPath();
		if(!mkdir(localLogPath)){
			java.lang.System.out.println("应用日志scp到本地的位置存放目录创建失败！");
		}
		String localImagePath = localConfig.getLocalImagePath();
		if(!mkdir(localImagePath)){
			java.lang.System.out.println("下载的tar包scp到本地的位置临时存放目录创建失败！");
		}
	}
	private static boolean mkdir(String outputFile) {
		File file = null;
		try {
			file = new File(outputFile);
			if (!file.exists())
				file.mkdirs();

			return true;
		} catch (Exception e) {
			logger.error("make dir exception:", e);
			return false;
		}
	}
}
