package com.sgcc.devops.web.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.dao.entity.Dictionary;
import com.sgcc.devops.dao.intf.DictionaryMapper;

@Repository("logConvertCache")
public class LogConvertCache {

	private Map<String, String> cache;
	@Autowired
	private DictionaryMapper dictionaryMapper;
	
	public Map<String, String> getCache() {
		return cache;
	}

	public void setCache(Map<String, String> cache) {
		this.cache = cache;
	}

	public String getKey(String key) {
		if (null == cache) {
			return null;
		} else {
			return cache.get(key);
		}
	}

	@PostConstruct
	public void init() {
		// 这里从数据库里查到日志转义的字典信息，然后set到cache里面。
		List<Dictionary> dictionaries = dictionaryMapper.selectAllIndex(Type.DIC_TYPE.LOG.ordinal());
		Map<String, String> map = new HashMap<String, String>();
		for (Dictionary dictionary : dictionaries) {
			map.put(dictionary.getdKey(), dictionary.getdValue());
		}
		setCache(map);
	}
}
