package com.sgcc.devops.web.controller.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.ImageModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ImageManager;
import com.sgcc.devops.web.query.QueryList;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:48:07
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ImageAction.java
 * description：  镜像操作访问控制类
 */
@Controller
@RequestMapping("image")
public class ImageAction {

	@Resource
	private QueryList queryList;
	@Autowired
	private ImageManager imageManager;

	private static Logger logger=Logger.getLogger(ImageAction.class);
	
	@RequestMapping(value = "/make", method = { RequestMethod.POST })
	@ResponseBody
	public Result make(HttpServletRequest request, ImageModel image) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = imageManager.makeImage(user.getUserId(), image);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/push", method = { RequestMethod.POST })
	@ResponseBody
	public Result push(HttpServletRequest request, ImageModel image) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = imageManager.pushImage(user.getUserId(), image);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean list(HttpServletRequest request,PagerModel pagerModel, ImageModel imageModel) {
		User user = (User) request.getSession().getAttribute("user");
		if(null!=imageModel&&null!=imageModel.getImageName()){
			String imageName;
			try {
				imageName = URLDecoder.decode(imageModel.getImageName(), "UTF-8");
				imageModel.setImageName(imageName);
			} catch (UnsupportedEncodingException e) {
				logger.error("encode image name error: "+e.getMessage());
			}
		}
		return queryList.queryImageList(user,pagerModel, imageModel);
	}

	@RequestMapping(value = "/remove/{id}", method = { RequestMethod.POST })
	@ResponseBody
	public Result remove(HttpServletRequest request, @PathVariable String id) {
		List<String> ids = new ArrayList<String>();
		ids.add(id);
		Result result = imageManager.removeImage(ids);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/remove/batch", method = { RequestMethod.POST })
	@ResponseBody
	public Result batchRemove(HttpServletRequest request, String ids) {
		List<String> idArray = new ArrayList<String>();
		for (String id : ids.split(",")) {
			idArray.add(id);
		}
		Result result = imageManager.removeImage(idArray);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/listByappId", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean list(HttpServletRequest request, @RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, String appId) {
		User user = (User) request.getSession().getAttribute("user");
		return queryList.queryImageList(user.getUserId(), page, rows, appId);
	}

	@RequestMapping(value = "/listByImageType", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean listByImageType(HttpServletRequest request, @RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows,int imageType,String clusterId) {
		User user = (User) request.getSession().getAttribute("user");
		return queryList.queryImageListByImageType(user.getUserId(), page, rows,imageType,clusterId);
	}

	@RequestMapping(value = "/conlist", method = { RequestMethod.GET })
	public String containerlist(Model model, String imageId,String imageType) {
		model.addAttribute("imageId", imageId);
		Image image = imageManager.getImage(imageId);
		model.addAttribute("imageName", image.getTempName());
		model.addAttribute("imageType", imageType);
		return "/image/imageConlist";
	}

	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		// 调用接口 查重
		isOk = imageManager.checkImage(id, name);
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	
	/*导出镜像 docker save*/
	@RequestMapping(value = "/saveIamge", method = { RequestMethod.GET })
	@ResponseBody
	public String saveIamge(HttpServletRequest request,  @RequestParam(value = "imageId", required = true) String imageId,
			@RequestParam(value = "imageName", required = true) String imageName) {
		JSONObject jo = imageManager.saveImage(imageId,imageName);
		 return jo.toString();
	}
	
	/*导出镜像 scp到平台服务器*/
	@RequestMapping(value = "/scpImage", method = { RequestMethod.GET })
	@ResponseBody
	public Result scpImage(HttpServletRequest request,   @RequestParam(value = "imageId", required = true) String imageId,
			 @RequestParam(value = "imageName", required = true) String imageName,
			@RequestParam(value = "remotePath",required = true) String remotePath) {
        Result result = imageManager.scpImage(imageId,imageName,remotePath);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/*导出镜像 download到本地*/
	@RequestMapping("/downloadImage")
    public void downloadImage(String imageId,String imageName,String imageTag, HttpServletRequest request,
            HttpServletResponse response) {
		User user = (User) request.getSession().getAttribute("user");
		imageManager.download(imageId,imageName,imageTag,request,response,user);
	}
}
