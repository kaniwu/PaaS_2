package com.sgcc.devops.web.manager;

import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.ClusterModel;
import com.sgcc.devops.core.intf.ClusterCore;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.web.daemon.PlatformDaemon;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.message.MessagePush;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:51:43
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ClusterManager.java
 * description：  集群操作流程处理类
 */
@Component
public class ClusterManager {

	private static Logger logger = Logger.getLogger(ClusterManager.class);
	@Resource
	private ClusterService clusterService;
	@Resource
	private ClusterCore clusterCore;
	@Resource
	private HostCore hostCore;
	@Resource
	private HostService hostService;
	@Resource
	private MessagePush messagePush;
	@Resource
	private DeployService deployService;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private DockerHostConfig dockerHostConfig;
	@Autowired
	private PlatformDaemon platformDaemon;
	@Resource
	private ClusterManager clusterManager;
	@Resource
	private HostManager hostManager;
	@Resource
	private HostComponentService hostComponentService;
	@Resource
	private HostComponentManager hostComponentManager;
	@Resource
	private RegistryLbService registryLbService;
	/**
	 * 创建集群 1.可达测试校验主机节点 2.检查swarm以及swarm的文件是否存在 3.启动swarm
	 * 4.成功后保存信息到dop_cluster表中
	 * 
	 * @param jsonObject
	 */
	public Result createCluster(JSONObject jsonObject) {
		String masterId = jsonObject.getString("masterHost");
		String clusterName = jsonObject.getString("clusterName");
		String registryLbId = jsonObject.getString("registryLbId");
		String standByHostId = jsonObject.getString("standByHostId");
		String dockerParam = jsonObject.containsKey("dockerParam")?jsonObject.getString("dockerParam"):null;
		//TODO 
		if(org.apache.commons.lang.StringUtils.isEmpty(registryLbId)){
			logger.info("创建集群【"+clusterName+"】失败，未选择仓库");
			return new Result(false, "创建集群【"+clusterName+"】失败，，未选择仓库");
		}
		if (!StringUtils.hasText(masterId)) {
			logger.info("创建集群【"+clusterName+"】失败，未选择主机");
			return new Result(false, "创建集群【"+clusterName+"】失败，，未选择主机");
		}
		// 检测集群管理节点的主机是否存在于平台
		Host masterHost = hostService.getHost(masterId);
		Host standByHost = hostService.getHost(standByHostId);
		if (null == masterHost) {
			logger.info("创建集群【"+clusterName+"】失败，管理节点指定主机[" + masterId + "]不存在！");
			return new Result(false, "创建集群【"+clusterName+"】失败，管理节点指定主机[" + masterId + "]不存在！");
		}
		// 获取解密的host密码
		String masterHost_pwd = Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath());
		if (!StringUtils.hasText(masterHost_pwd)) {
			logger.info("创建集群【"+clusterName+"】失败，集群管理节点指定主机【"+masterHost.getHostIp()+"】密码解密失败！");
			return new Result(false, "创建集群【"+clusterName+"】失败，集群管理节点指定主机【"+masterHost.getHostIp()+"】密码解密失败！");
		}
		// 集群swarm检测
		String clusterConfFile = UUID.randomUUID().toString();
		jsonObject.put("clusterConfFile", clusterConfFile);
		String hostDockerId =jsonObject.getString("hostDocker");
		
		if(org.apache.commons.lang.StringUtils.isEmpty(hostDockerId)){
			logger.info("新增集群失败，docker版本不可识别");
			return new Result(false, "新增集群失败，docker版本不可识别！");
		}
		Result masterSwarm = this.swarmHostInstall(jsonObject, masterHost, hostDockerId, clusterConfFile, registryLbId,dockerParam);
		Result standBySwarm=null;
		if(masterSwarm.isSuccess()&&null!=standByHost){
		 standBySwarm = this.swarmHostInstall(jsonObject, standByHost, hostDockerId, clusterConfFile, registryLbId,dockerParam);
		 if(!standBySwarm.isSuccess()){
			 return standBySwarm;
		 }
		}
		String clusterId = clusterService.createCluster(jsonObject, masterHost);
		if(standByHost==null){
			platformDaemon.startClusterDaemon(clusterId);
			return new Result(true, "创建集群【" + clusterName + "】成功！"); 
		}
		if (standBySwarm!=null&&standBySwarm.isSuccess()) {
			// 添加高可用
			if (null != clusterId) {
				platformDaemon.startClusterDaemon(clusterId);
				logger.info("创建集群【" + clusterName + "】成功！");
				return new Result(true, "创建集群【" + clusterName + "】成功！");
			} else {
				logger.info("创建集群【" + clusterName + "】失败：数据库操作异常");
				return new Result(false, "创建集群【" + clusterName + "】失败：数据库操作异常");
			}
		} else {
			// swarm检查通过
			logger.info("集群【" + clusterName + "】环境检查不通过:管理节点主机【"+masterHost.getHostIp()+"】,集群配置文件名称【"+clusterConfFile+"】");
			return new Result(false, "集群【" + clusterName + "】环境检查不通过:管理节点主机【"+masterHost.getHostIp()+"】,集群配置文件名称【"+clusterConfFile+"】");
		}
	}

	/**
	 * 删除集群，需要校验集群中是否存在Node节点，如果存在Node节点，则不允许删除
	 * 
	 * @param jo
	 */
	public Result deleteCluster(JSONObject jo) {
		String clusterId = jo.getString("clusterId");
		// 查询要删除集群的信息
		Cluster cluster = new Cluster();
		cluster.setClusterId(clusterId);
		cluster = clusterService.getCluster(cluster);
		if (null == cluster) {
			logger.info("删除集群失败：主键为【" + clusterId + "】的集群可能已经被其他管理员删除！");
			return new Result(false, "删除集群失败：主键为【" + clusterId + "】的集群可能已经被其他管理员删除！");
		}
		// 查询集群内的主机
		List<Host> listHosts = hostService.listHostByClusterId(clusterId);
		if (null == listHosts) {
			logger.info("集群删除失败：集群【" + cluster.getClusterName() + "】的集群下没有任何主机！");
			return new Result(false, "集群删除失败：主键为【" + cluster.getClusterName() + "】的集群下没有任何主机！");
		}
		for (Host host : listHosts) {
			if (host.getHostId().equals(cluster.getMasteHostId())) {
				listHosts.remove(host);
				break;
			}
		}
		if (!listHosts.isEmpty()) {
			logger.info("集群删除失败：集群【" +  cluster.getClusterName() + "】的存在可用的容器宿主机！");
			return new Result(false, "集群删除失败：集群【" +  cluster.getClusterName() + "】的存在可用的容器宿主机！");
		}
		// 查询集群内的物理系统
		List<Deploy> deploys = deployService.getDeployByClusterId(cluster.getClusterId());
		if (null != deploys && !deploys.isEmpty()) {
			// logger.error("集群删除失败：主键为【"+cluster.getClusterId()+"】的集群关联正在运行的物理系统！");
			// return new Result(false, "集群删除失败：当前集群关联正在运行的物理系统！");
			for (Deploy deploy : deploys) {
				deploy.setClusterId("");
				deployService.updateDeploy(deploy);
			}
		}
		// 查询集群所在主机的信息
		Host host = hostService.getHost(cluster.getMasteHostId());
		if (null == host) {
			logger.info("集群【" +  cluster.getClusterName() + "】删除失败：集群管理主机【" + cluster.getMasteHostId() + "】不存在，可能已被其他管理员删除！");
			return new Result(false, "集群【" +  cluster.getClusterName() + "】删除失败：集群管理主机【" + cluster.getMasteHostId() + "】不存在，可能已被其他管理员删除！");
		}
		// 删除高可用
		platformDaemon.stopClusterDaemon(clusterId);
		// 解密host密码
		String hostPwd = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		// SSH连接，kill集群启动的进程
		clusterCore.stopSwarm(host.getHostIp(), host.getHostUser(), hostPwd, host.getHostPort(),cluster.getClusterPort());
		int result = clusterService.deleteCluster(jo);
		if (result > 0) {
			host.setHostType((byte) Type.HOST.UNUSED.ordinal());
			host.setClusterId(null);
			hostService.update(host);
			logger.info("集群【" +  cluster.getClusterName() + "】删除成功！");
			return new Result(true, "集群【" +  cluster.getClusterName() + "】删除成功！");
		} else {
			platformDaemon.startClusterDaemon(clusterId);
			logger.info("集群【" +  cluster.getClusterName() + "】删除失败：数据库操作异常！");
			return new Result(false, "集群【" +  cluster.getClusterName() + "】删除失败：数据库操作异常！");
		}
	}

	/**
	 * 更新集群信息
	 * 
	 * @param userId
	 * @param model
	 * @return
	 */
	public Result updateCluster(String userId, ClusterModel model) {
		try {
			Cluster cluster = new Cluster();
			BeanUtils.copyProperties(model, cluster);
			int result = clusterService.update(cluster);
			return result > 0 ? new Result(true, "修改集群【" + model.getClusterName() + "】成功！") : new Result(false, "修改集群【" + model.getClusterName() + "】失败！");
		} catch (Exception e) {
			logger.error("修改集群【" + model.getClusterName() + "】 失败: " + e);
			return new Result(false, "修改集群【" + model.getClusterName() + "】 失败: " + e);
		}
	}

	/**
	 * 集群健康检查
	 * 
	 * @param jsonObject
	 */
	public Result clusterHealthCheck(JSONObject jsonObject) {
		String clusterId = jsonObject.getString("clusterId");
		Cluster cluster = new Cluster();
		cluster.setClusterId(clusterId);
		cluster = clusterService.getCluster(cluster);
		// 通过clusterId去查找集群下主机的IP
		List<Host> lists = hostService.listHostByClusterId(clusterId);
		Host clusterHost = hostService.getHost(cluster.getMasteHostId());
		if (null == clusterHost || !lists.contains(clusterHost)) {
			logger.info("集群【"+cluster.getClusterName()+"】异常：主键为【" + cluster.getMasteHostId() + "】的管理节点已经不存在！");
			return new Result(false, "集群【"+cluster.getClusterName()+"】异常：主键为【" + cluster.getMasteHostId() + "】的管理节点已经不存在！");
		} else {
			lists.remove(clusterHost);
		}
		// 获取解密的host密码
		String clusterHostPwd = Encrypt.decrypt(clusterHost.getHostPwd(), localConfig.getSecurityPath());
		String responseString = hostCore.clusterHealthCheck(clusterHost.getHostIp(), clusterHost.getHostUser(),
				clusterHostPwd,clusterHost.getHostPort(), cluster.getClusterPort());
		String[] ips = responseString.split("\n");
		Iterator<Host> hostIterator = lists.iterator();
		Host item = null;
		while (hostIterator.hasNext()) {
			item = hostIterator.next();
			if (contains(ips, item.getHostIp())) {
				hostIterator.remove();
			}
		}
		if (lists.isEmpty()) {
			logger.info("集群【"+cluster.getClusterName()+"】健康，数据库和服务器一致");
			return new Result(true, "集群【"+cluster.getClusterName()+"】健康，数据库和服务器一致");
		} else {
			logger.info("集群【"+cluster.getClusterName()+"】不健康,数据库和服务器不一致");
			return new Result(false, "集群【"+cluster.getClusterName()+"】不健康,数据库和服务器不一致");
		}
	}

	/**
	 * 包含查询
	 * 
	 * @param ips
	 * @param ip
	 * @return
	 */
	private boolean contains(String[] ips, String ip) {
		for (String temp : ips) {
			if (ip.equals(temp.trim())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 获取指定集群下的容器宿主机列表
	 * 
	 * @param userId
	 * @param page
	 * @param rows
	 * @param clusterId
	 *            集群ID
	 * @return
	 */
	public GridBean dockerList(String userId, int page, int rows, String clusterId) {
		PageHelper.startPage(page, rows);
		Host host = new Host();
		host.setClusterId(clusterId);
		host.setHostType((byte) Type.HOST.DOCKER.ordinal());
		List<Host> hosts = hostService.selectHostListByHost(host);
		int totalpage = ((Page<?>) hosts).getPages();
		Long totalNum = ((Page<?>) hosts).getTotal();
		return new GridBean(page, totalpage, totalNum.intValue(), hosts);
	}

	/**
	 * 获取集群基本信息
	 * 
	 * @param cluster
	 * @return
	 */
	public Cluster getCluster(Cluster cluster) {
		return clusterService.getCluster(cluster);
	}

	/**
	 * 集群隔离把状态更新
	 * 
	 * @param clusterId
	 * @return
	 */
	public Result isolateCluster(String clusterId) {
		this.platformDaemon.stopClusterDaemon(clusterId);
		Cluster cluster = this.clusterService.getCluster(clusterId);
		if (null == cluster) {
			logger.info("集群隔离异常：主键为【" + clusterId + "】的集群不存在!");
			return new Result(false, "集群隔离异常：主键为【" + clusterId + "】的集群不存在!");
		}
		cluster.setClusterStatus((byte) Status.CLUSTER.ISOLATED.ordinal());
		this.clusterService.update(cluster);
		Host host = this.hostService.getHost(cluster.getMasteHostId());
		if (null == host) {
			cluster.setClusterStatus((byte) Status.CLUSTER.ABNORMAL.ordinal());
			this.clusterService.update(cluster);
			logger.info("集群【"+cluster.getClusterName()+"】隔离异常：集群管理主机数据不存在！");
			return new Result(false, "集群【"+cluster.getClusterName()+"】隔离异常：集群管理主机数据不存在！");
		}
		String password = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		if (StringUtils.isEmpty(password)) {
			cluster.setClusterStatus((byte) Status.CLUSTER.NORMAL.ordinal());
			this.clusterService.update(cluster);
			logger.info("集群【"+cluster.getClusterName()+"】隔离异常：集群管理主机密码异常！");
			return new Result(false, "集群【"+cluster.getClusterName()+"】隔离异常：集群管理主机密码异常！");
		}

		return null;
	}

	/**
	 * 集群恢复，将隔离状态的集群恢复正常
	 * 
	 * @param clusterId
	 * @return
	 */
	public Result recoveryCluster(String clusterId) {
		Cluster cluster = this.clusterService.getCluster(clusterId);
		if (null == cluster) {
			logger.info("隔离集群恢复：主键为【" + clusterId + "】的集群不存在!");
			return new Result(false, "隔离集群恢复：主键为【" + clusterId + "】的集群不存在!");
		}
		cluster.setClusterStatus((byte) Status.CLUSTER.NORMAL.ordinal());
		this.clusterService.update(cluster);
		Host host = this.hostService.getHost(cluster.getMasteHostId());
		if (null == host) {
			logger.info("隔离集群【"+cluster.getClusterName()+"】恢复：主键为【" + cluster.getMasteHostId() + "】管理主机不存在!");
			return new Result(false, "隔离集群【"+cluster.getClusterName()+"】恢复：主键为【" + cluster.getMasteHostId() + "】管理主机不存在!");
		}
		return null;
	}

	/**
	 * 集群迁移，将当前主机的swarm迁移到目前主机上。
	 * 
	 * @param clusterId
	 * @param targetHost
	 * @return
	 */
	public Result migrateCluster(String clusterId, String targetHost) {
		// 停止集群守护
		this.platformDaemon.stopClusterDaemon(clusterId);

		// 得到当前集群
		Cluster cluster = this.clusterService.getCluster(clusterId);
		if (null == cluster) {
			logger.info("集群【" + clusterId + "】迁移:集群不存在！");
			return new Result(false, "集群【" + clusterId + "】迁移:集群不存在！");
		}

		// 得到当前管理主机ID
		String oldHostId = cluster.getMasteHostId();

		// 得到目的管理主机
		Host newHost = this.hostService.getHost(targetHost);
		if (null == newHost) {
			logger.info("集群【" + cluster.getClusterName() + "】迁移:目标集群管理主机【"+targetHost+"】不存在！");
			return new Result(false, "集群【" + cluster.getClusterName() + "】迁移:目标集群管理主机【"+targetHost+"】不存在！");
		}

		// 启动目标管理主机swarm
		// 获取解密的host密码
		String password = Encrypt.decrypt(newHost.getHostPwd(), localConfig.getSecurityPath());
		if (!StringUtils.hasText(password)) {
			logger.info("集群【" + cluster.getClusterName() + "】迁移:集群管理节点所在主机密码解密失败！");
			return new Result(false, "集群【" + cluster.getClusterName() + "】迁移:集群管理节点所在主机密码解密失败！");
		}
		// 集群swarm检测
		boolean startSwarmResult = clusterCore.checkAndStartSwarm(newHost.getHostIp(), newHost.getHostUser(), password,
				newHost.getHostPort(),cluster.getClusterPort(), cluster.getManagePath());
		if (!startSwarmResult) {
			logger.info("集群【" + cluster.getClusterName() + "】迁移:目标管理主机【"+targetHost+"】启动swarm失败！");
			return new Result(false, "集群【" + cluster.getClusterName() + "】迁移:目标管理主机【"+targetHost+"】启动swarm失败！");
		}
		List<Host> list = this.hostService.listHostByClusterId(clusterId);
		if (null != list && !list.isEmpty()) {
			StringBuffer clusterConfig = new StringBuffer();
			for (Host host : list) {
				// 去除当前管理主机的信息
				if (host.getHostId().equals(oldHostId)) {
					continue;
				}
				clusterConfig.append(host.getHostIp()).append(":2375").append("\n");
			}
			// 消除最后一个换行\n
			if (StringUtils.hasText(clusterConfig.toString())) {
				clusterConfig = clusterConfig.delete(clusterConfig.length() - 1, clusterConfig.length());
			}
			boolean success = this.hostCore.updateCluster(newHost.getHostIp(), newHost.getHostUser(), password,
					newHost.getHostPort(),clusterConfig.toString(), cluster.getManagePath());
			if (!success) {
				logger.info("集群【" + cluster.getClusterName() + "】迁移:更新集群配置失败！");
				return new Result(false, "集群【" + cluster.getClusterName() + "】迁移:更新集群配置失败！");
			}
		}
		cluster.setMasteHostId(targetHost);
		this.clusterService.update(cluster);

		newHost.setClusterId(clusterId);
		newHost.setHostType((byte) Type.HOST.SWARM.ordinal());
		this.hostService.update(newHost);
		// 启动集群守护
		this.platformDaemon.startClusterDaemon(clusterId);

		// 得到当前集群的管理主机
		Host oldHost = this.hostService.getHost(oldHostId);
		if (null == oldHost) {
			logger.info("集群【" + cluster.getClusterName() + "】迁移成功，但原有集群管理主机更新失败！");
			return new Result(true, "集群【" + cluster.getClusterName() + "】迁移成功，但原有集群管理主机更新失败！");
		}
		oldHost.setClusterId(null);
		oldHost.setHostType((byte) Type.HOST.UNUSED.ordinal());

		this.hostService.update(oldHost);

		// 关闭当前管理主机swarm
		String oldpassword = Encrypt.decrypt(oldHost.getHostPwd(), localConfig.getSecurityPath());
		if (!StringUtils.hasText(oldpassword)) {
			logger.info("集群【" + cluster.getClusterName() + "】迁移成功，但是原有管理节点的主机【"+oldHost.getHostIp()+"】swarm关闭失败！");
			return new Result(true, "集群【" + cluster.getClusterName() + "】迁移成功，但是原有管理节点的主机【"+oldHost.getHostIp()+"】swarm关闭失败[主机密码异常]！");
		}
		this.clusterCore.stopSwarm(oldHost.getHostIp(), oldHost.getHostUser(), oldpassword, oldHost.getHostPort(),cluster.getClusterPort());
		logger.info("集群【" + cluster.getClusterName() + "】迁移到【"+targetHost+"】成功！");
		return new Result(true, "集群【" + cluster.getClusterName() + "】迁移到【"+targetHost+"】成功！");
	}

	/**
	 * 检查集群名称是否重复
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public Boolean checkName(String id, String name) {
		Cluster cluster = new Cluster();
		cluster.setClusterName(name);
		cluster = clusterService.selectClusterByName(cluster);
		if (null == cluster) {
			return true;
		}
		if (id == null || id.equals("")) {
			// 创建时 找到clusterName为name的cluster对象
			return false;
		} else {
			return cluster.getClusterId().equals(id);
		}
	}
	public boolean installSwarm(String ip,String username,String password,String hostport) {
		//TODO
		String file =localConfig.getLocalHostInstallPath()+"swarm-rpm/";
		String hostFileLocation=dockerHostConfig.getHostInstallPath();
		String hostFileName="swarm-rpm";
		boolean record =hostCore.installAllFile(ip, username, password,hostport, file, hostFileLocation, hostFileName);
			return record;
	}
	/**
	 * 1、安装docker并指定仓库，启动docker
	 * 2、安装swarm并启动swarm
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年7月22日
	 */
	private Result swarmHostInstall(JSONObject jsonObject,Host host,String hostDockerId,String clusterConfFile,String registryLbId,String dockerParam){
		//停掉原来的swarm进程
		if(null==host||StringUtils.isEmpty(host.getHostIp())||StringUtils.isEmpty(host.getHostUser())||StringUtils.isEmpty(host.getHostPwd())){
			return new Result(false,"swarm主机信息为空");
		}
		String hostIp = host.getHostIp();
		String hostUser = host.getHostUser();
		String hostPwd = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		String hostPort = host.getHostPort();
		hostCore.stopSwarm(hostIp, hostUser, hostPwd,hostPort,jsonObject.getString("clusterPort"));
		
		if(hostDockerId.equals("0")){		
//			String response = hostCore.dockerInfo(masterHost.getHostIp(), masterHost.getHostUser(),
//					Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//			if(null==response){
//				logger.info("Can not connect to host 【" + masterHost.getHostIp() + "】");
//				return new Result(false,"无法连接到主机【" + masterHost.getHostIp() + "】");
//			}
//			String noneFlag = "docker: command not found";
//			String deadFlag = "Cannot connect to the Docker daemon";
//			if(response.contains(noneFlag) || response.contains(deadFlag)){
//					boolean result =false;
//					if(response.contains(noneFlag)){
//						 result =hostManager.installDocker(masterHost.getHostIp(), masterHost.getHostUser(),
//									Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//						 logger.info("swarm host docker env is installing");
//					}
//					if(result||response.contains(deadFlag)){
//						hostCore.startDocker(masterHost.getHostIp(), masterHost.getHostUser(),
//								Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//						logger.info("swarm host docker env is starting");
//					}
//			response = hostCore.dockerInfo(masterHost.getHostIp(), masterHost.getHostUser(),
//						Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//			if(null==response){
//					logger.info("Can not connect to host 【" + masterHost.getHostIp() + "】");
//					return new Result(false,"无法连接到主机【" + masterHost.getHostIp() + "】");
//			}
//			 if(response.contains(noneFlag)){
//		        	logger.info("机器的环境安装docker失败");
//				     return new Result(false, "机器的环境安装docker失败");
//				}
//			if (response.contains("Cannot connect to the Docker daemon")) {
//				logger.info("机器的环境不支持docker运行");
//				return new Result(false, "机器的环境不支持docker运行");
//			}
//			}
	    }else{
			Result result =hostComponentManager.installHostComponent(host.getHostId(),hostIp, hostUser, hostPwd,hostDockerId);
			if(!result.isSuccess()){
				return result;
			}
		}
	    //给docker服务指定仓库,并启动docker
	    RegistryLb registryLb = new RegistryLb();
	    registryLb.setId(registryLbId);
	    registryLb = registryLbService.selectRegistryLb(registryLb);
		String re = hostCore.updateAndStartDocker(hostIp, hostUser, hostPwd,hostPort,
				registryLb.getDomainName()+":"+registryLb.getLbPort(),hostManager.dockerLabels(host.getHostId()),dockerParam);
		if(re.contains("0")){
			host.setHostStatus((byte)Status.DOCKER.NORMAL.ordinal());
			hostService.update(host);
		}else{
			host.setHostStatus((byte)Status.DOCKER.ABNORMAL.ordinal());
			hostService.update(host);
			logger.error("新增集群失败，主机【"+host.getHostIp()+"】docker启动参数修改失败！");
			return new Result(false, "新增集群失败，主机【"+host.getHostIp()+"】docker启动参数修改失败！");
		}
		
		String swarmIns = jsonObject.getString("swarmIns");
		if(org.apache.commons.lang.StringUtils.isEmpty(swarmIns)){
			logger.error("新增集群失败，主机【"+host.getHostIp()+"】swarm版本不可识别");
			return new Result(false, "新增集群失败，主机【"+host.getHostIp()+"】swarm版本不可识别！");
		}
	      if(swarmIns.equals("0")){		
//			boolean flag =clusterCore.checkSwarm(masterHost.getHostIp(), masterHost.getHostUser(),
//					Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//			if(!flag){
//				//安装swarm
//				logger.info("正在安装swarm");
//				clusterManager.installSwarm(masterHost.getHostIp(), masterHost.getHostUser(),
//					Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath()));
//			}
	    }else{
			Result result =hostComponentManager.installHostComponent(host.getHostId(),hostIp, hostUser, hostPwd,swarmIns);
			if(!result.isSuccess()){
				return result;
			}
	    }
	    //swarm主机添加仓库负载域名到/etc/hosts
	    re =hostCore.installedInfo(hostIp, hostUser, hostPwd,hostPort, "sudo echo "+registryLb.getLbIp()+" "+registryLb.getDomainName()+" >> /etc/hosts", 5000l);
	    
		boolean swarmResult = clusterCore.checkAndStartSwarm(hostIp, hostUser, hostPwd,hostPort,jsonObject.getString("clusterPort"), clusterConfFile);
		return new Result(swarmResult, "");
	}
}