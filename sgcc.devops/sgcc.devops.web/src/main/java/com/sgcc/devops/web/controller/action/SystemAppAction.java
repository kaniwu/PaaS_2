package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.query.QueryList;

import net.sf.json.JSONArray;

@Controller
@RequestMapping("systemApp")
public class SystemAppAction {
    
	private static Logger logger=Logger.getLogger(SystemAppAction.class);
	
	
	@Resource
	private QueryList queryList;
   /**
    * 查出所有的SystemApp
    * @param request
    * @param pagenumber
    * @return
    */
	@RequestMapping(value = "/list" , method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray list(HttpServletRequest request,String systemId) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			return queryList.systemAppsList(systemId);
		} catch (Exception e) {
			logger.error("查询systemAppsList失败: "+e.getMessage());
			return null;
		}
	}
}
