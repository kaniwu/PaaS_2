package com.sgcc.devops.web.image;

/**Nginx常量类
 * @author dmw
 *
 */
public class NginxConstants {

	public static final String BASE_PATH = "/usr/";

	public static final String CONFIG_PATH = "/etc/nginx/";

	public static final String START_CMD = "." + BASE_PATH + "sbin/nginx";
}
