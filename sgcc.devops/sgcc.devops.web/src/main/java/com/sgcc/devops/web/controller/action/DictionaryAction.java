/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.DictionaryService;

/**  
 * date：2016年2月4日 下午1:47:31
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DictionaryAction.java
 * description：  字典表访问控制类
 */
@RequestMapping("/dictionary")
@Controller
public class DictionaryAction {
	@Resource
	private DictionaryService dictionaryService;

	@RequestMapping("/listByDatabaseId")
	@ResponseBody
	public String listByDatabaseId(HttpServletRequest request, String databaseId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = dictionaryService.listByDatabaseId(databaseId, user);
		return ja.toString();
	}
}
