package com.sgcc.devops.web.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.web.task.TaskCache;
import com.sgcc.devops.web.task.TaskMessage;
import com.sgcc.devops.web.task.TaskServiceHandler;

@Component
public class TimingProcessor {
	@Autowired
	private TaskCache taskCache;
	@Autowired
	private TaskServiceHandler taskServiceHandler;

	@Scheduled(cron = "0/1 * * * * ?")
	protected void execute() {

		List<TaskMessage> taskList = taskCache.listTask();
		for (TaskMessage task : taskList) {
//			taskServiceHandler.sendMessageToUser(task.getUserId(), task);
			taskServiceHandler.sendMessageToUsers(task);
			if (task.getProcess() == 100) {
				task.setProcess(task.getProcess() + 1);
				taskCache.updateTask(task);
			} else if (task.getProcess() > 100) {
				taskCache.removeTask(task.getTaskId());
			}
		}
	}

}
