package com.sgcc.devops.web.monitor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.core.monitor.HostAgentMonitorTask;
import com.sgcc.devops.core.monitor.HostMonitorParam;
import com.sgcc.devops.core.monitor.MonitorResult;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 批量监控容器客户端
 * 
 * @author dmw
 *
 */
@Repository("hostMonitorCluster")
public class HostMonitorCluster {
	private static final Long TIME_OUT = 1000 * 60L;
	@Autowired
	private TaskCache cache;
	@Autowired
	private HostService hostService;

	public List<Monitor> batchMoniter(List<HostMonitorParam> hosts) throws Exception {
		ExecutorService executor = Executors.newCachedThreadPool();
		CompletionService<MonitorResult> completionService = new ExecutorCompletionService<>(executor);
		if (null == hosts || hosts.isEmpty()) {
			return null;
		}
		Set<String> hostIds = new HashSet<>();
		for (HostMonitorParam host : hosts) {
			HostAgentMonitorTask task = new HostAgentMonitorTask(host);
			completionService.submit(task);
			hostIds.add(host.getId());
		}
		List<Monitor> monitors = new ArrayList<Monitor>();
		int completeTask = 0;
		long begingTime = System.currentTimeMillis();
		while (completeTask < hosts.size()) {
			Future<MonitorResult> future = completionService.take();
			MonitorResult result = future.get();
			if (result.isSuccess()) {
				monitors.add(result.getData());
				hostIds.remove(result.getData().getTargetId());
				
				Host host = hostService.getHost(result.getData().getTargetId());
				//如果监控成功，但是原数据状态为监控异常，修改状态
				if(host.getHostStatus()==Status.HOST.ABNORMAL.ordinal()){
					host.setHostStatus((byte)Status.HOST.NORMAL.ordinal());
					hostService.update(host);
				}
			}
			completeTask++;
			if (System.currentTimeMillis() - begingTime > TIME_OUT) {
				break;
			}
		}
		//监控异常的数据更新到数据库
		if(hostIds.size()>0){
			for(String hostId:hostIds){
				Host host = hostService.getHost(hostId);
				if(host.getHostStatus()==Status.HOST.NORMAL.ordinal()){
					Integer times = cache.getHostMonitorCache().get(hostId);
					if(times==null){
						cache.getHostMonitorCache().put(hostId, 1);
					}else{
						cache.getHostMonitorCache().put(hostId, times+1);
					}
				}
			}
		}
		executor.shutdown();
		return monitors;
	}
}
