package com.sgcc.devops.web.elasticity.enums;

/**
 * 弹性伸缩决策枚举类
 * 
 * @author dmw
 *
 */
public enum ElasticDecision {

	SCALE_IN, // 扩展
	SCALE_OUT, // 收缩
	EXCEPTION, // 异常
	NO_ACTION;// 无动作
}
