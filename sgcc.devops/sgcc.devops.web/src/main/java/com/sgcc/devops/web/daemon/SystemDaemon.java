package com.sgcc.devops.web.daemon;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONArray;
import com.sgcc.devops.common.config.MonitorConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.core.detector.Detector;
import com.sgcc.devops.core.detector.DockerDetector;
import com.sgcc.devops.core.detector.SwarmDetector;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.manager.ContainerManager;

import net.sf.json.JSONObject;

/**
 * 集群系统线程，每隔60秒钟探测一次系统是否正常， 在swarm所在主机不可达时每隔120秒重试一次。 在发现系统中有容器状态异常时自动增加一个容器
 * 
 * @author dmw
 *
 */
public class SystemDaemon extends Thread {

	private static Logger logger = Logger.getLogger(SystemDaemon.class);
	private static HttpClient client = new HttpClient();
	private boolean stop = false;
	private String ip;// 集群IP
	private String port;// 集群端口
	private Detector detector;// 容器健康探测器
	private String system;// 系统ID
	private ContainerManager containerManager;
	private SystemService systemService;
	private DeployService deployService;
	private ContainerService containerService;
	private MonitorConfig monitorConfig;
	private JSONObject params = new JSONObject();
//	private List<SystemStore> stores = new ArrayList<>();
	private static ConcurrentHashMap<String, SystemStore> stores = new ConcurrentHashMap<String, SystemStore>(10,
			0.9f);
	public static HttpClient getClient() {
		return client;
	}

	public static void setClient(HttpClient client) {
		SystemDaemon.client = client;
	}

	public boolean isStop() {
		return stop;
	}

	public void setStop(boolean stop) {
		this.stop = stop;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public Detector getDetector() {
		return detector;
	}

	public void setDetector(Detector detector) {
		this.detector = detector;
	}

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public ContainerManager getContainerManager() {
		return containerManager;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public SystemService getSystemService() {
		return systemService;
	}

	public void setSystemService(SystemService systemService) {
		this.systemService = systemService;
	}

	public DeployService getDeployService() {
		return deployService;
	}

	public void setDeployService(DeployService deployService) {
		this.deployService = deployService;
	}

	public void stopDeamon() {
		this.stop = true;
	}

	public SystemDaemon(boolean stop, String ip, String port, String system, ContainerManager containerManager,
			SystemService systemService,DeployService deployService,ContainerService containerService,MonitorConfig monitorConfig) {
		super();
		this.stop = stop;
		this.ip = ip;
		this.port = port;
		this.system = system;
		this.containerManager = containerManager;
		this.systemService = systemService;
		this.deployService = deployService;
		this.containerService  = containerService;
		this.monitorConfig = monitorConfig;
	}

	private void init() throws Exception {
		if (!StringUtils.hasText(ip) || !StringUtils.hasText(port) || !StringUtils.hasText(system)
				|| null == containerManager || null == systemService) {
			throw new Exception("物理系统守护初始化异常！");
		}
	}

	@Override
	public void run() {
		try {
			init();
		} catch (Exception e1) {
			logger.error(e1.getMessage());
			return;
		}
		while (!stop) {
			Deploy deploy = null;
			System systementity = this.systemService.getSystemById(system);
			try {
				Thread.sleep(1000 * 10);
			} catch (InterruptedException e) {
				logger.error("system【"+systementity.getSystemName()+"】 daemon is　intercepted");
				continue;
			}
			int exceptionNum = 0;
			int startNum = 0;
			List<Container> containers = this.systemService.listContainers(system);
			
			if(null!=systementity&&org.apache.commons.lang.StringUtils.isNotEmpty(systementity.getDeployId())){
				deploy = this.deployService.getDeploy(systementity.getDeployId());
			}else{
				continue;
			}
			if (null == containers || containers.isEmpty()) {
				logger.warn("The system 【" + systementity.getSystemName() + "】  do not have any containers to daemon!");
				startNum = deploy.getInstanceCount();
			}
			if(deploy!=null&&containers.size()<deploy.getInitCount()){
				logger.warn("The system 【" + systementity.getSystemName() + "】  has less containers than init!");
				startNum = deploy.getInitCount()-containers.size();
			}
			//修正运行中的容器数量
			if(null!=deploy){
				deploy.setInstanceCount(null==containers||containers.isEmpty()?0:containers.size());
				deployService.updateDeploy(deploy);
			}
			
			HttpClient client = new HttpClient(5000);
			Detector swarmDetector = new SwarmDetector(ip, port, client);
			if(!swarmDetector.normal()){
				logger.error("The system 【" + systementity.getSystemName() + "】`s cluster is abnormal !!! wait for it recovery!!!");
				continue;
			}
			JSONArray exceptionContainers = new JSONArray();
			for (Container container : containers) {
				if (container.getConPower() != (byte) Status.POWER.UP.ordinal()) {
					detector = new DockerDetector(ip, port, container.getConUuid(), client,monitorConfig.getHttpReadTimes());
					if (detector.normal()) {
						container.setConPower((byte) Status.POWER.UP.ordinal());
						try {
							containerService.modifyContainer(container);
						} catch (Exception e) {
							e.printStackTrace();
						}
						continue;
					}
					logger.warn("The 【" + systementity.getSystemName() + "】 Container 【" + container.getConName() + "】  is crash!");
					exceptionContainers.add(container.getConId());
					exceptionNum = exceptionNum + 1;
				} else {
					detector = new DockerDetector(ip, port, container.getConUuid(), client,monitorConfig.getHttpReadTimes());
					if (detector.normal()) {
						continue;
					} else {
						long beginTime = java.lang.System.currentTimeMillis(); 
						while((java.lang.System.currentTimeMillis()-beginTime)<10*1000){
							if(detector.normal()){
								continue;
							}
							try {
								Thread.sleep(3000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						logger.warn("The 【" + systementity.getSystemName() + "】 Container 【" + container.getConName() + "】  is crash!");
						exceptionNum = exceptionNum + 1;
						exceptionContainers.add(container.getConId());
					}
				}
			}
			//重启异常容器，重启不成功创建新容器并删除异常容器
			if(exceptionNum>0&&!params.containsKey("systemId")){
				if(flag(systementity,"重新启动")){
					logger.warn("The 【" + systementity.getSystemName() + "】 restart containers!");
					
					params.put("containerId", exceptionContainers);
					params.put("systemId", system);
					params.put("conCount", exceptionNum);
					params.put("userId", "system");
					containerManager.restartContaner(params);
					params.remove("systemId");
				}
			}
			//创建容器使物理系统数据一致
			if(startNum>0&&!params.containsKey("systemId")){
				if(flag(systementity,"创建")){
					logger.warn("The 【" + systementity.getSystemName() + "】 start containers!");
					
					params.put("systemId", system);
					params.put("conCount", startNum);
					params.put("userId", "system");
					containerManager.createConBySysId(params);
					params.remove("systemId");
				}
				
			}
			//在空闲期，尝试删除物理系统不是运行版本的容器
			if(exceptionNum==0&&startNum==0&&!params.containsKey("systemId")){
				JSONArray waitContainers = new JSONArray();
				List<Container> waitCons = this.systemService.listWaitContainers(system);
				if(!waitCons.isEmpty()&&flag(systementity,"删除")){
					logger.warn("The 【" + systementity.getSystemName() + "】 delete containers!");
					for(Container container:waitCons){
						waitContainers.add(container.getConId());
					}
					params.put("systemId", system);
					params.put("userId", "system");
					params.put("containerId", waitContainers);
					containerManager.stopContainer(params);
					containerManager.removeContainer(params);
					params.remove("systemId");
				}
			}
//			Result stopResult = containerManager.stopAndRemoveCon(params);
//			if(stopResult.isSuccess()){
//				params.put("conCount", exceptionNum);
//				params.put("userId", "system");
//				Result startResult = containerManager.createConBySysId(params);
//				logger.error("System 【"+system+"】 Daemon Result is "+startResult.toString());
//			}else{
//				logger.error("Clean exception container failed:"+stopResult.toString());
//				logger.error("System 【"+system+"】 Daemon Failed!");
//			}
		}
	}
	private boolean flag(System systementity,String opration){
		boolean flag = true;
		SystemStore store = stores.get(system);
		//是否执行过
		if(store!=null){
			flag = false;
			//如果5分钟之内未发生重启的情况，重新置为空
			if((java.lang.System.currentTimeMillis()-store.getBeginTime())>5*60*1000){
				stores.remove(store);
			}
			//如果十分钟之内执行了3次或者5分钟之内又来执行一次，则认为是无法正常运行的应用容器，不再进行重启操作
			else if(store.getRestartTimes()>=3&&(java.lang.System.currentTimeMillis()-store.getBeginTime())<5*60*1000){
				logger.warn("【"+systementity.getSystemName()+"】"+opration+"过于频繁，可能此应用容器无法正常运行，请管理员注意检查！");
				return false;
			//如果5分钟之内未发生重启的情况，重新置为空
			}else{
				store.setRestartTimes(store.getRestartTimes()+1);
				store.setBeginTime(java.lang.System.currentTimeMillis());
			}
		}
		if(flag){
			SystemStore temp = new SystemStore();
			temp.setBeginTime(java.lang.System.currentTimeMillis());
			temp.setRestartTimes(1);
			temp.setSystemId(system);
			stores.put(system, temp);
		}
		return true;
	}
	public boolean clearSystemStore(String systemId){
		SystemStore store = stores.get(system);
		//是否执行过
		if(store!=null){
			stores.remove(store);
		}
		return true;
	}
}
