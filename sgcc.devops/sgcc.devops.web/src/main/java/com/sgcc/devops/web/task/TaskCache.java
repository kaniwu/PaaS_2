package com.sgcc.devops.web.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

/**
 * @author dmw
 *
 */
@Component("taskCache")
public class TaskCache {

	private ConcurrentHashMap<String, TaskMessage> cache;
	private ConcurrentHashMap<String, Integer> containerCache;
	private ConcurrentHashMap<String, Integer> hostCache;
	private ConcurrentHashMap<String, Integer> hostMonitorCache;

	public ConcurrentHashMap<String, Integer> getContainerCache() {
		return containerCache;
	}

	public ConcurrentHashMap<String, Integer> getHostCache() {
		return hostCache;
	}

	public ConcurrentHashMap<String, TaskMessage> getCache() {
		return cache;
	}

	public ConcurrentHashMap<String, Integer> getHostMonitorCache() {
		return hostMonitorCache;
	}

	public void setCache(ConcurrentHashMap<String, TaskMessage> cache) {
		this.cache = cache;
	}

	@PostConstruct
	public void init() {
		cache = new ConcurrentHashMap<String, TaskMessage>(100, 0.9f);
		containerCache = new ConcurrentHashMap<String, Integer>(100, 0.9f);
		hostCache = new ConcurrentHashMap<String, Integer>(100, 0.9f);
		hostMonitorCache = new ConcurrentHashMap<String, Integer>(100, 0.9f);
	}

	/**
	 * 添加任务
	 * 
	 * @param task
	 * @return
	 */
	public boolean addTask(TaskMessage task) {
		if (cache.isEmpty()) {
			cache = new ConcurrentHashMap<String, TaskMessage>(100, 0.9f);
		}
		cache.put(task.getTaskId(), task);
		return true;
	}

	/**
	 * 更新任务
	 * 
	 * @param task
	 * @return
	 */
	public boolean updateTask(TaskMessage task) {
		if (cache.isEmpty()) {
			cache = new ConcurrentHashMap<String, TaskMessage>(100, 0.9f);
		}
		cache.put(task.getTaskId(), task);
		return true;
	}

	/**
	 * 移除任务
	 * 
	 * @param taskname
	 * @return
	 */
	public boolean removeTask(String taskId) {
		if (cache.isEmpty()) {
			cache = new ConcurrentHashMap<String, TaskMessage>(100, 0.9f);
		}
		cache.remove(taskId);
		return true;
	}

	/**
	 * 获取当前在执行的任务
	 * 
	 * @return
	 */
	public List<TaskMessage> listTask() {
		List<TaskMessage> tasks = new ArrayList<TaskMessage>();
		if (!cache.isEmpty()) {
			for (String key : cache.keySet()) {
				tasks.add(cache.get(key));
			}
		}
		return tasks;
	}

	/**
	 * 获取指定任务名的任务进度信息
	 * 
	 * @param taskname
	 * @return
	 */
	public TaskMessage getTask(String taskId) {
		if (cache.isEmpty()) {
			return null;
		} else {
			return cache.get(taskId);
		}
	}

}
