package com.sgcc.devops.web.controller.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.component.CaptchaNumber;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.model.LoginModel;
import com.sgcc.devops.common.util.CaptchaUtil;
import com.sgcc.devops.dao.entity.Authority;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.UserService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.manager.AuthorityManager;
import com.sgcc.devops.web.manager.UserManager;
import com.sgcc.devops.web.util.Base64;

/**  
 * date：2016年2月4日 下午1:48:54
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LoginAction.java
 * description：  系统登录操作访问控制类
 */
@Controller
public class LoginAction {

	private static Logger logger = Logger.getLogger(LoginAction.class);
	@Resource
	private UserService userService;
	@Resource
	private UserManager userManager;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private AuthorityManager authorityManager;
	
	@RequestMapping(value = "/login", method = { RequestMethod.POST })
	public ModelAndView login(HttpServletRequest request, HttpServletResponse response, LoginModel loginRequestModel,RedirectAttributes redirectAttributes) {
		String userName = Base64.getFromBase64(loginRequestModel.getUserName());
		final char[] pwdArr = loginRequestModel.getPwd();
		String passwordAsString = new String(pwdArr);
		String pwd = Base64.getFromBase64(passwordAsString);
		Arrays.fill(pwdArr,Character.MAX_VALUE);
		
//		String vercode = Base64.getFromBase64(loginRequestModel.getVercode());
		String path = request.getContextPath();
		logger.info("【" + userName + "】尝试登录");
		User user=new User();
		//使用addFlashAttribute,参数不会出现在url地址栏中  
		redirectAttributes.addFlashAttribute("userName", userName);
		redirectAttributes.addFlashAttribute("password", pwdArr);
		// 会话存在过期现象
//		if (vercode == null || request.getSession().getAttribute("rand") == null) {
//			logger.warn("登录失败：验证码失效");
//			redirectAttributes.addFlashAttribute("message", "验证码失效");
//			return new ModelAndView(new RedirectView(path+"/login.html"));
//		}
		try {
			user = userManager.checkLogin(userName,pwd);
		} catch (Exception e) {
			logger.error("check user login by userName["+userName+"] falied!", e);
			redirectAttributes.addFlashAttribute("message", "用户登录失败，数据库连接异常！");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		//用户不存在
		if (user == null) {
			redirectAttributes.addFlashAttribute("message", "用户不存在！");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		//用户密码错误
		//String tpwd = Encrypt.decrypt(user.getUserPass(), localConfig.getSecurityPath());
		//String tpwd = user.getUserPass();
		//if(user != null && !StringUtils.hasText(user.getUserPass())){
		if(user != null && !pwd.equals(Encrypt.decrypt(user.getUserPass(), localConfig.getSecurityPath()))){
			redirectAttributes.addFlashAttribute("message", "用户密码错误");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		//用户被冻结
		if(user != null && user.getUserStatus() == Status.USER.DELETE.ordinal()){
			logger.warn("登录失败：该用户已被冻结！");
			redirectAttributes.addFlashAttribute("message", "该用户已被冻结,请联系管理员！");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		
		//【二】：判断验证码是否正确
		String captcha = (String) request.getSession().getAttribute("rand");
//		if (!vercode.toLowerCase(Locale.ENGLISH).equals(captcha.toLowerCase(Locale.ENGLISH))) {
//			logger.error("登录失败：验证码不正确");
//			redirectAttributes.addFlashAttribute("message", "验证码不正确！");
//			return new ModelAndView(new RedirectView(path+"/login.html"));
//		} 
		String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path
				+ "/";
		
		//获取用户第一个一级菜单权限
		String startPage=new String();
		boolean startFlag=true;
		
		//【三】：获取用户权限信息，存储在session中
		List<String> authlist=new ArrayList<String>();
		List<Authority> listAuths = authorityManager.getUserRoleAuths(user.getUserId());
		if(listAuths.size()==0){
			logger.warn("登录失败：用户没有相关权限！");
			redirectAttributes.addFlashAttribute("message", "用户没有相关权限！");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		//pages： 一级菜单权限、  buttons：二级按钮权限
		String pages = "";
		String buttons = "";
		for(Authority ahr:listAuths){
			if(ahr.getActionRelativeUrl()!=null){
				authlist.add(ahr.getActionRelativeUrl());
			}
			if(ahr.getActionType() == Status.AUTHTYPE.PAGE.ordinal()){
				pages += ahr.getActionRemarks()+",";
				if(startFlag){
					startPage=ahr.getActionRelativeUrl();
					startFlag=false;
				}
			}else if(ahr.getActionType() == Status.AUTHTYPE.BUTTON.ordinal()){
				buttons += ahr.getActionRemarks()+",";
			}
		}
		//用户登录成功，设置用户状态为登录状态()
		try {
			user.setUserLoginStatus(request.getSession().getId());
			userService.update(user);
		} catch (Exception e) {
			logger.error("get user by username["+userName+"] falied!", e);
			redirectAttributes.addFlashAttribute("message", "用户登录失败，数据库连接异常！");
			return new ModelAndView(new RedirectView(path+"/login.html"));
		}
		
		//权限存储到session中（pagesAuth、buttonsAuth控制界面的权限显示效果，authlist 权限url过滤）
		request.getSession().setAttribute("pagesAuth", pages);
		request.getSession().setAttribute("buttonsAuth", buttons);
		request.getSession().setAttribute("authlist", authlist);
		
		//用户信息和项目url存储在session中
		request.getSession().setAttribute("user", user);
		request.getSession().setAttribute("basePath", basePath);
		logger.info(userName + "登录成功");
		return new ModelAndView(new RedirectView("index.html"));
	}

	@RequestMapping("/logout.html")
	public String logout(HttpSession session) {
		session.removeAttribute("user");
		return "redirect:login.html";
	}

	@RequestMapping(value = "/captcha", method = { RequestMethod.GET })
	public ResponseEntity<byte[]> image(HttpServletRequest request) throws IOException {
		CaptchaNumber capnumber = CaptchaUtil.getCaptchaNumber();
		HttpSession session = request.getSession(true);
		session.setAttribute("rand", capnumber.getTotalNum().toString());
		return new ResponseEntity<byte[]>(CaptchaUtil.getImage(capnumber.getFirNum(), capnumber.getSecNum()),
				CaptchaUtil.getCaptchaHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/judgePass", method = { RequestMethod.GET })
	@ResponseBody
	public Result judgePass(HttpServletRequest request, String username, String oldpass) throws Exception {

		User user = userManager.checkLogin(username, oldpass);
		if (null == user || !oldpass.equals(Encrypt.decrypt(user.getUserPass(), localConfig.getSecurityPath()))) {
			logger.warn("登录失败：用户名或密码错误！");
			return new Result(false, "用户名或密码错误！");
		} else {
			return new Result(true, "用户名与密码有效");
		}
	}

	@RequestMapping(value = "/modifyPassProcess", method = { RequestMethod.POST })
	@ResponseBody
	public Result modifyPassProcess(HttpServletRequest request, String oldpass,String username, String newpass) throws IOException {
		User user = userManager.checkLogin(username, oldpass);
		if(!oldpass.equals(Encrypt.decrypt(user.getUserPass(), localConfig.getSecurityPath()))){
			return new Result(false,"原密码错误！");
		}
		newpass = Encrypt.encrypt(Base64.getFromBase64(newpass), localConfig.getSecurityPath());
		int updateResult = userService.updatePassword(username, newpass);
		if (updateResult == 1) {
			return new Result(true, "已成功修改密码,请重新登录！");
		} else {
			return new Result(false, "修改密码失败");
		}
	}
}
