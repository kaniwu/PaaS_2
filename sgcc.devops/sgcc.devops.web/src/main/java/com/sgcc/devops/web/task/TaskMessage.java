package com.sgcc.devops.web.task;

import com.sgcc.devops.web.message.Message;
import com.sgcc.devops.web.message.MessageType;

public class TaskMessage implements Message {

	private String messageType;
	private String taskId;
	private String taskname;
	private Integer process;
	private String userId;

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public Integer getProcess() {
		return process;
	}

	public void setProcess(Integer process) {
		this.process = process;
	}

	public String getTaskname() {
		return taskname;
	}

	public void setTaskname(String taskname) {
		this.taskname = taskname;
	}

	@Override
	public String getMessageType() {
		return messageType;
	}

	public TaskMessage(String taskname, Integer process, String creator, String taskId) {
		this.messageType = MessageType.TASK;
		this.taskname = taskname;
		this.process = process;
		this.userId = creator;
		this.taskId = taskId;
	}

}
