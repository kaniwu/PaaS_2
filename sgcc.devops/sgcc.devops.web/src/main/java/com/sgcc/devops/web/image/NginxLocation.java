/**
 * 
 */
package com.sgcc.devops.web.image;

/**  
 * date：2016年4月27日 下午3:20:22
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：NginxLocation.java
 * description：  
 */
public class NginxLocation {
	private String appName;
	private String dnsName;
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getDnsName() {
		return dnsName;
	}
	public void setDnsName(String dnsName) {
		this.dnsName = dnsName;
	}
	public NginxLocation(String appName, String dnsName) {
		super();
		this.appName = appName;
		this.dnsName = dnsName;
	}
	
}
