package com.sgcc.devops.web.image.builder;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.FileUploadUtil;

/**
 * 镜像制作基类
 * @author dmw
 *
 */
public class Builder {

	private static Logger logger = Logger.getLogger(Builder.class);

	boolean deleteFile(String outputFile) {
		logger.info("开始删除文件");
		File file = null;
		try {
			outputFile = FileUploadUtil.check(outputFile);
			file = new File(outputFile);
			if (file.exists())
				file.delete();
			logger.info("删除文件完成");
			return true;
		} catch (Exception e) {
			logger.error("Delete file exception:"+e);
			return false;
		}
	}

	Result parseMakeResult(String response) {
		logger.info("开始解析镜像制作结果");
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(response.getBytes())));
		String uuid = null;
		try {
			int stepNull = 0;
			while (stepNull < 2) {
				String line = reader.readLine();
				if (null == line) {
					stepNull++;
				} else if (line.contains("Successfully") && line.contains("built")) {
					uuid = line.split(" ")[2];
				} else {
					continue;
				}
			}
		} catch (IOException e) {
			logger.error("解析返回值失败:"+e);
			uuid = null;
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				logger.error("关闭数据流失败："+e);
			}
		}
		if (null == uuid) {
			logger.error("制作镜像失败：脚本执行异常");
			return new Result(false, "镜像制作脚本执行异常！");
		} else {
			logger.info("制作镜像成功");
			return new Result(true, uuid);
		}
	}
}
