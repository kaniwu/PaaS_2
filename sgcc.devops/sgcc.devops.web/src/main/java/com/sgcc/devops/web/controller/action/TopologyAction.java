/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ContainerManager;
import com.sgcc.devops.web.manager.TopologyManager;

/**  
 * date：2016年2月4日 下午1:49:35
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：TopologyAction.java
 * description：  物理系统拓扑图访问控制类
 */
@RequestMapping("/topology")
@Controller
public class TopologyAction {
	@Resource
	private TopologyManager topologyManager;
	@Autowired
	private ContainerManager containerManager;

	/**
	 * @author zhangxin
	 * @time 2015年9月23日
	 * @description
	 */
	@RequestMapping("/viewTopology")
	public String viewTopology(Model model, String deployId, String systemId) {

		model.addAttribute("deployId", deployId);
		model.addAttribute("systemName", topologyManager.getSystemName(systemId));
		model.addAttribute("systemId", systemId);
		return "system/topology";
	}

	@RequestMapping("/topology")
	@ResponseBody
	public JSONObject topology(String deployId) {
		JSONObject jo = topologyManager.selectTopologyData(deployId, "");
		return jo;
	}

	@RequestMapping(value = "/topologys")
	@ResponseBody
	public String topologys(HttpServletRequest request,
			@RequestParam(value = "businessId", required = true) String businessId,String envId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = topologyManager.getAllTopologysByBusinessId(user, businessId,envId);
		return ja.toString();
	}

	@RequestMapping("/viewTopologys")
	public String viewTopologys(Model model, String businessId) {
		model.addAttribute("businessId", businessId);
		model.addAttribute("businessName", topologyManager.getBusinessName(businessId));
		return "business/topologys";
	}

	/**
	 * 依赖镜像批量启动容器 TODO
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/createConByDeployId", method = { RequestMethod.GET })
	@ResponseBody
	public Result createConByDeployId(HttpServletRequest request, @RequestParam String deployId, int conCount) {
		User user = (User) request.getSession().getAttribute("user");
		return containerManager.createConByDeployId(deployId, conCount, user.getUserId());
	}

	/**
	 * 批量停止、删除容器 TODO
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/stopAndRemoveCon", method = { RequestMethod.GET })
	@ResponseBody
	public Result stopAndRemoveCon(HttpServletRequest request, @RequestParam String systemId,
			@RequestParam String deployId, @RequestParam String containerid) {
		return containerManager.stopAndRemoveConByCount(systemId, containerid, deployId);
	}
	/**
	 * 物理系统编排部署初始化
	* TODO
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2015年12月13日
	 */
	@RequestMapping("/arrangementInit")
	@ResponseBody
	public JSONObject arrangementInit(String deployId) {
		JSONObject jo = topologyManager.selectTopologyData(deployId, "");
		return jo;
	}
}
