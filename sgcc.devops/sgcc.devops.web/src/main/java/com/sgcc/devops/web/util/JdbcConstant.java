package com.sgcc.devops.web.util;

public class JdbcConstant {
	public static String JDBC_DATASOURCE_USERNAME_KEY = "jdbc.username";
	public static String JDBC_DATASOURCE_PASSWORD_KEY = "jdbc.userSecure";
	public static String SECURE_PATH_KEY = "security.path";
}
