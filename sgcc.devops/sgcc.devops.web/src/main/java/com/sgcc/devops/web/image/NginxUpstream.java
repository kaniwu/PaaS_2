/**
 * 
 */
package com.sgcc.devops.web.image;

import java.util.List;

/**  
 * date：2016年4月27日 下午3:23:11
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：NginxUpstream.java
 * description：  
 */
public class NginxUpstream {
	private String dnsName;
	private List<SystemServer> systemServers;
	public String getDnsName() {
		return dnsName;
	}
	public void setDnsName(String dnsName) {
		this.dnsName = dnsName;
	}
	public List<SystemServer> getSystemServers() {
		return systemServers;
	}
	public void setSystemServers(List<SystemServer> systemServers) {
		this.systemServers = systemServers;
	}
	public NginxUpstream(String dnsName, List<SystemServer> systemServers) {
		super();
		this.dnsName = dnsName;
		this.systemServers = systemServers;
	}
	
}
