/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import java.net.URLDecoder;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.SystemAppModel;
import com.sgcc.devops.common.model.SystemModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.daemon.PlatformDaemon;
import com.sgcc.devops.web.manager.SystemManager;
import com.sgcc.devops.web.manager.VersionChangeManager;
import com.sgcc.devops.web.query.QueryList;
import com.sgcc.devops.web.task.TaskCache;
import com.sgcc.devops.web.task.TaskMessage;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:49:26
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：SystemAction.java
 * description：  物理系统操作访问控制类
 */
@RequestMapping("/system")
@Controller
public class SystemAction {

	private static Logger logger = Logger.getLogger(SystemAction.class);
	@Resource
	private QueryList queryList;
	@Resource
	private SystemService systemService;
	@Resource
	private SystemManager systemManager;
	@Resource
	private VersionChangeManager versionChangeManager;
	@Autowired
	private TaskCache taskCache;
	@Resource
	private PlatformDaemon platformDaemon;
	/**
	 * @author mayh
	 * @time 2015年9月16日 10:24
	 * @description
	 */
	@RequestMapping("/listWithIP")
	@ResponseBody
	public GridBean listWithIP(HttpServletRequest request,PagerModel pagerModel, String businessId,String systemName,String envId) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			if(StringUtils.hasText(systemName)){
				systemName = URLDecoder.decode(systemName, "UTF-8");
			}
			return queryList.systemAllList(pagerModel,user,businessId,systemName,false,envId);
		} catch (Exception e) {
			logger.error("get systen all list error: "+e.getMessage());
			return null;
		}
	}
	
	/**
	 * @author yueyogn
	 * @time 2016年3月86日 10:24
	 * @description
	 */
	@RequestMapping("/listByAuth")
	@ResponseBody
	public GridBean listByAuth(HttpServletRequest request,PagerModel pagerModel, String businessId,String systemName,String envId) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			if(StringUtils.hasText(systemName)){
				systemName = URLDecoder.decode(systemName, "UTF-8");
			}
			return queryList.systemAllList(pagerModel,user,businessId,systemName,true,envId);
		} catch (Exception e) {
			logger.error("get systen all list error: "+e.getMessage());
			return null;
		}
	}

	/**
	 * @author mayh
	 * @time 2015年9月16日 10:24
	 * @description
	 */
	@RequestMapping("/create")
	@ResponseBody
	public Result create(HttpServletRequest request, SystemModel systemModel) {
		if (systemModel != null) {
			User user = (User) request.getSession().getAttribute("user");
			int re = systemService.create(systemModel, user);
			Result result = new Result(re > 0, re > 0 ? "添加系统(" + systemModel.getSystemName() + ")成功"
					: "添加系统(" + systemModel.getSystemName() + ")失败");
			request.setAttribute("logDetail", result.getMessage());
			request.setAttribute("success", result.isSuccess()?"Success":"Fail");
			return result;
		} else {
			return new Result(false, "参数输入异常!");
		}
	}

	/**
	 * @author mayh
	 * @time 2015年9月16日 10:24
	 * @description
	 */
	@RequestMapping(value = "/deploy", method = { RequestMethod.POST })
	@ResponseBody
	public Result deploy(HttpServletRequest request, DeployModel deployModel) {
		if (deployModel != null) {
			User user = (User) request.getSession().getAttribute("user");
			//停止物理系统守护
			try{
				platformDaemon.stopSystemDaemon(deployModel.getSystemId());
				TaskMessage stopSystemDaemon = new TaskMessage("守护停止成功...", 1, user.getUserId(),deployModel.getSystemId());
				taskCache.updateTask(stopSystemDaemon);
			}catch(Exception e){
				e.printStackTrace();
				return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】守护停止失败，应用发布终止！");
			}
			Result result;
			try{
				result = systemManager.deploy(deployModel, user);
			}catch(Exception e){
				e.printStackTrace();
				result = new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败："+e.getLocalizedMessage());
			}
			
			try{
				platformDaemon.startSystemDaemon(deployModel.getSystemId());
				TaskMessage stopSystemDaemon = new TaskMessage("启动守护成功...", 99, user.getUserId(),deployModel.getSystemId());
				taskCache.updateTask(stopSystemDaemon);
			}catch(Exception e){
				e.printStackTrace();
				return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】守护停止失败，应用发布终止！");
			}
			
			TaskMessage stopSystemDaemon = new TaskMessage(result.getMessage(), 100, user.getUserId(),deployModel.getSystemId());
			taskCache.updateTask(stopSystemDaemon);
			
			request.setAttribute("logDetail", result.getMessage());
			request.setAttribute("success", result.isSuccess()?"Success":"Fail");
			return result;
		} else {
			return new Result(false, "参数输入异常!");
		}
	}

	/**
	 * @author zhangs
	 * @time 2015年9月23日
	 * @description
	 */
	@RequestMapping("/arrangement")
	public String arrangement(HttpServletRequest request, ModelMap modelMap,
			@RequestParam(value = "systemId", required = true) String systemId,
			@RequestParam(value = "systemName", required = true) String systemName) {
		modelMap.addAttribute("systemId", systemId);
		com.sgcc.devops.dao.entity.System system = systemService.getSystemById(systemId);
		modelMap.addAttribute("systemName", system.getSystemName());
		modelMap.addAttribute("deployId", system.getDeployId());
		return "system/arrangement";
	}

	@RequestMapping("/deployVersion")
	@ResponseBody
	public GridBean deployVersion(HttpServletRequest request,PagerModel pagerModel, DeployModel deployModel) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			String systemId = request.getParameter("systemId");
			/* 显示正常状态的镜像内容 */
			// byte imagestatus = (byte) Status.IMAGE.NORMAL.ordinal();
			return queryList.systemDeployInfo(user.getUserId(),pagerModel, deployModel, systemId);
		} catch (Exception e) {
			logger.error("select system deploy info fail" + e);
			return null;
		}
	}

	@RequestMapping(value = "/rollBackImage", method = { RequestMethod.GET })
	@ResponseBody
	public Result rollBackImage(HttpServletRequest request, @RequestParam String deployId,@RequestParam String clusterId) {
		User user = (User) request.getSession().getAttribute("user");
		StringBuffer taskend = new StringBuffer();
		StringBuffer taskbegin = new StringBuffer();
		taskbegin.append("回滚中...");
		TaskMessage message0 = new TaskMessage(taskbegin.toString(), 0, user.getUserId(),deployId);
		taskCache.updateTask(message0);
		Result result = systemManager.rollBackImage(clusterId,deployId, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		taskend.append(result.getMessage());
		TaskMessage message1 = new TaskMessage(taskend.toString(), 100, user.getUserId(),deployId);
		taskCache.updateTask(message1);
		return result;
	}

	/**
	 * 下线
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月19日 
	 */
	@RequestMapping(value = "/killVersion", method = { RequestMethod.GET })
	@ResponseBody
	public Result killVersion(HttpServletRequest request, @RequestParam String deployId) {
		User user = (User) request.getSession().getAttribute("user");
		TaskMessage message0 = new TaskMessage("版本下线中...", 5, user.getUserId(),deployId);
		taskCache.updateTask(message0);
		Result result = systemManager.killVersion(deployId, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		TaskMessage message1 = new TaskMessage(result.getMessage(), 100, user.getUserId(),deployId);
		taskCache.updateTask(message1);
		return result;
	}

	/**
	 * 切换版本 TODO
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月19日
	 */
	@RequestMapping(value = "/versionChange", method = { RequestMethod.GET })
	@ResponseBody
	public Result versionChange(HttpServletRequest request, @RequestParam String deployId) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = versionChangeManager.versionChange(deployId, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/deleteVersion", method = { RequestMethod.GET })
	@ResponseBody
	public Result deleteVersion(HttpServletRequest request, @RequestParam String deployId) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = systemManager.deleteVersion(deployId, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	
	@RequestMapping(value = "/getBaseDeploy", method = { RequestMethod.GET })
	@ResponseBody
	public String getBaseDeploy(HttpServletRequest request, @RequestParam String systemId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject jo = systemService.getBaseDeploy(systemId, user);
		return jo.toString();
	}

	@RequestMapping(value = "/listAllByBusinessId", method = { RequestMethod.GET })
	@ResponseBody
	public String listAllByBusinessId(HttpServletRequest request, @RequestParam String businessId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.listAllByBusinessId(businessId, user);
		return ja.toString();
	}

	@RequestMapping(value = "/test", method = { RequestMethod.POST })
	@ResponseBody
	public String test(HttpServletRequest request, @RequestParam(value = "systemId", required = true) String systemId,
			@RequestParam(value = "instanceCount", required = true) String instanceCount) {

		JSONObject obj = new JSONObject();
		obj.put("success", true);
		return obj.toString();
	}

	/**
	 * @author zs
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = false;
		isOk = systemManager.checkName(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}

	/**
	 * 业务系统下的物理系统 需返回systemModel
	 */
	@RequestMapping(value = "/getListByBusinessId", method = { RequestMethod.GET })
	@ResponseBody
	public String getListByBusinessId(HttpServletRequest request, @RequestParam String businessId, @RequestParam String envId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = systemManager.getListByBusinessId(user, businessId,envId);
		return ja.toString();
	}
	
	/**
	 *访问
	 */
	@RequestMapping(value = "/getVisitUrl", method = { RequestMethod.GET })
	@ResponseBody
	public String getListByBusinessId(HttpServletRequest request, @RequestParam String nodeId,@RequestParam String nodeType,@RequestParam String systemId,@RequestParam String deployId){
//		User user = (User)request.getSession().getAttribute("user");
//		JSONArray ja = systemManager.getListByBusinessId(user, businessId);
		// nodetype :f5 compNginx conNginx docker
		JSONObject jsobj = systemManager.getNodeVisitUrl(nodeId, nodeType, systemId, deployId);
		return jsobj.toString();
	}
	
	/**
	 * @author 检查版本重名
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkVersion", method = { RequestMethod.POST })
	@ResponseBody
	public String checkVersion(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;

		isOk = systemManager.checkVersion(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	/**
	 * @author 检查域名重名
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkDomainName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkDomainName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;

		isOk = systemManager.checkDomainName(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	
	/**
	 * 获得System的AppPackage
	 * @author liyp
	 * @param request
	 * @param systemId
	 * @return GridBean
	 */
	@RequestMapping(value = "/getAppPackage")
	@ResponseBody
	public GridBean getAppPackage(HttpServletRequest request,PagerModel pagerModel, @RequestParam String systemId) {
		try {
			return queryList.sysAppPackageList(pagerModel, systemId);
		} catch (Exception e) {
			logger.error("select systemApp info fail" + e);
			return null;
		}
	}
	/**
	 * @author liyp
	 * @time 2016年3月10日16:21:37
	 * @description 新增物理系统应用包
	 */
	@RequestMapping(value = "/createSysAppPackage", method = { RequestMethod.POST })
	@ResponseBody
	public Result createSysAppPackage(HttpServletRequest request, SystemAppModel systemAppModel) {
		if (systemAppModel != null) {
			int re = systemManager.createSystemAppPackage(systemAppModel);
			Result result = new Result(re > 0, re > 0 ? "添加应用包(" + systemAppModel.getAppVersion() + ")成功"
					: "添加应用包(" + systemAppModel.getAppVersion() + ")失败");
			request.setAttribute("logDetail", result.getMessage());
			request.setAttribute("success", result.isSuccess()?"Success":"Fail");
			return result;
		} else {
			return new Result(false, "参数输入异常!");
		}
	}
	/**
	 * @author liyp
	 * @time 2016年3月10日16:21:37
	 * @description 删除物理系统应用包
	 */
	@RequestMapping("/deleteSysAppPackage")
	@ResponseBody
	public Result deleteSysAppPackage(HttpServletRequest request, SystemAppModel systemAppModel) {
		if (systemAppModel != null) {
			if(systemService.checkAppUsed(systemAppModel)){
				Result result = new Result(false,"此应用包已经被使用不可删除");
				request.setAttribute("logDetail", result.getMessage());
				request.setAttribute("success", result.isSuccess()?"Success":"Fail");
				return result;
			}
			int re = systemService.deleteSystemAppPackage(systemAppModel);
			Result result = new Result(re > 0, re > 0 ? "删除应用包成功"
					: "删除应用包失败");
			request.setAttribute("logDetail", result.getMessage());
			request.setAttribute("success", result.isSuccess()?"Success":"Fail");
			return result;
		} else {
			return new Result(false, "参数输入异常!");
		}
	}
	
	@RequestMapping(value = "/getAppPackageById", method = { RequestMethod.GET })
	@ResponseBody
	public SystemApp getSysAppPackageById(HttpServletRequest request, @RequestParam String systemAppId) {
		SystemApp systemApp = systemService.getAppPackageById(systemAppId);
		return systemApp;
	}
	
	/**@author wangjx
	 * 查询所有物理系统管理员并判断当前物理系统的管理员
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/selectAllAdmin", method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray selectAllAdmin(HttpServletRequest request,String systemId) {
		
		return systemManager.selectCheckAdmin(systemId);
	}
	
	/**@author wangjx
	 * 添加物理系统管理员
	 * @param request
	 * @param userId
	 * @param systemId
	 * @param systemName
	 * @param userName
	 * @return
	 */
	@RequestMapping(value = "/saveSystemAdmin", method = { RequestMethod.POST })
	@ResponseBody
	public Result saveSystemAdmin(HttpServletRequest request,String dataAdminInfo,String systemName){
		Result result = systemManager.saveSysAdminManage(dataAdminInfo, systemName);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	/**
	 * 查找定时版本
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月20日
	 */
	@RequestMapping(value = "/selectDeployTimer", method = { RequestMethod.GET })
	@ResponseBody
	public Deploy selectDeployTimer(HttpServletRequest request, @RequestParam String systemId) {
		User user = (User) request.getSession().getAttribute("user");
		Deploy deploy = systemManager.selectDeployTimer(systemId, user);
		return deploy;
	}
	/**
	 * 设置定时版本失效
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月20日
	 */
	@RequestMapping(value = "/setDeployTimer", method = { RequestMethod.GET })
	@ResponseBody
	public Result setDeployTimer(HttpServletRequest request, @RequestParam String systemId) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = systemManager.setDeployTimer(systemId, user);
		return result;
	}
	/**
	 * @author 检查版本重名
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/validateForm", method = { RequestMethod.GET })
	@ResponseBody
	public Result validateForm(HttpServletRequest request, DeployModel deployModel) {
		Result result = systemManager.validateForm(deployModel);
		return result;
	}
	/**
	 * 
	* TODO删除物理系统
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年8月3日
	 */
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(HttpServletRequest request, String systemId) {

		User user = (User) request.getSession().getAttribute("user");
		Result result = systemManager.deleteHost(user,systemId);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	@RequestMapping(value = "/searchRunningByEnv" , method = { RequestMethod.GET})
	@ResponseBody
	public String searchRunningByEnv(HttpServletRequest request,System system){
		User user = (User) request.getSession().getAttribute("user");
		List<System> systems = systemManager.searchByEnv(user, system);
		return JSONUtil.parseObjectToJsonArray(systems).toString(); 
	}
	@RequestMapping(value = "/switchImage", method = { RequestMethod.POST })
	@ResponseBody
	public Result switchImage(HttpServletRequest request,String baseImageId, String switchSystems) {
		Result result = new Result();
		User user = (User) request.getSession().getAttribute("user");
		TaskMessage begin = new TaskMessage("镜像切换开始", 0, user.getUserId(),"imageId_"+baseImageId);
		taskCache.updateTask(begin);
		try{
			result = systemManager.switchImage(baseImageId, switchSystems,user);
		}catch(Exception e){
			e.printStackTrace();
			result = new Result(false, "镜像切换失败："+e.getCause());
		}
		TaskMessage stop = new TaskMessage(result.getMessage(), 100, user.getUserId(),"imageId_"+baseImageId);
		taskCache.updateTask(stop);
		
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	//访问连接
	@RequestMapping(value = "/showAllAccess" , method = { RequestMethod.GET})
	@ResponseBody
	public String showAllAccess(HttpServletRequest request,String deployId){
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = systemManager.showAllAccess(user, deployId);
		return ja.toString(); 
	}
}
