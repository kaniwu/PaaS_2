package com.sgcc.devops.web.manager;

import java.io.File;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.MessageResult;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.model.ImageModel;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.ImageCore;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.web.download.DownLoad;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.message.MessagePush;

import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:57:48
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ImageManager.java
 * description： 镜像操作流程处理类 
 */
@Component
public class ImageManager {

	private static Logger logger = Logger.getLogger(ImageManager.class);
	@Resource
	private ImageService imageService;
	@Resource
	private ImageCore imageCore;
	@Resource
	private HostService hostService;
	@Resource
	private RegistryService registryService;
	@Resource
	private RegistryLbService registryLbService;
	@Resource
	private MessagePush messagePush;
	@Autowired
	private RegImageMapper regImageMapper;
	@Resource
	private ContainerService containerService;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private TmpHostConfig tmpHostConfig;
	@Autowired
	private DockerHostConfig dockerHostConfig;
	@Autowired
	private DownLoad downLoad;
	@Resource
	private ComponentService componentService;
	/**
	 * 制作镜像
	 * 
	 * @param model
	 * @return
	 */
	public Result makeImage(String userId, ImageModel model) {
		
		//转存主机
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		String port = tmpHostConfig.getTmpHostPort();
//		if (host != null) {
			// 应该需要解密
			Result coreresult = imageCore.makeImage(ip, user, pwd, port,model.getFileName(),
					model.getImageName().toLowerCase(), model.getImageTag());
			MessageResult result = new MessageResult(coreresult.isSuccess(), coreresult.getMessage(), "制作镜像");
			logger.info("make image result:" + result.toString());
			if (coreresult.isSuccess()) {
				Image image = new Image();
				image.setImageCreator(userId);
				image.setImageStatus(Status.IMAGE.MAKED.ordinal());
				image.setImageName(model.getImageName().toLowerCase());
				image.setImageTag(model.getImageTag());
				image.setImageUuid(coreresult.getMessage());
				image.setImageType(model.getImageType());
				if (model.getTempName() == null) {
					image.setTempName(model.getImageName());
				} else {
					image.setTempName(model.getTempName());
				}
				image.setImagePort(model.getImagePort());
				String imageId = imageService.create(image);
				boolean createResult = false;
				if (StringUtils.hasText(imageId)) {
					RegImage regImage = new RegImage();
					regImage.setId(KeyGenerator.uuid());
					regImage.setImageId(imageId);
					regImage.setRegistryId(model.getRegistryId());
					try {
						createResult = regImageMapper.insert(regImage) > 0;
					} catch (Exception e) {
						logger.error("保存镜像到数据库异常: "+e);
					}
				}
				if (createResult) {
					result.setSuccess(true);
					result.setMessage("制作镜像【"+model.getImageName().toLowerCase()+"】成功！");
				} else {
					result.setSuccess(false);
					result.setMessage("制作镜像【"+model.getImageName().toLowerCase()+"】失败：数据库操作异常");
				}
			}
			return result;
//		} else {
//			return new Result(false, "仓库主机未查到");
//		}
	}

	// 调用dockerAPI，打镜像的标记
	public Result tagImage(String hostId, Integer registryPort, String imageName, String tag,String nginxDns) {
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		String port = tmpHostConfig.getTmpHostPort();
//		Host host = new Host();
//		host.setHostId(hostId);
//		host = hostService.getHost(host);
//		if (host == null) {
//			logger.error("Registry host not exist!");
//			return new Result(false, "仓库主机不存在");
//		} else {
//			String password = host.getHostPwd();
//			String hostPwd = Encrypt.decrypt(password, localConfig.getSecurityPath());

			String sourceImage = imageName + ":" + tag;
			String targetImage = nginxDns+":" + registryPort + "/" + sourceImage;
			Result result = imageCore.tagImage(ip, user, pwd,port, sourceImage, targetImage);
			return result;
//		}
	}

	/**
	 * 镜像发布
	 * 
	 * @param jsonObject
	 * @return
	 */
	public Result pushImage(String userId, ImageModel model) {
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		String port = tmpHostConfig.getTmpHostPort();
//		Host host = new Host();
//		host.setHostId(model.getHostId());
		RegistryLb registryLb = new RegistryLb();
		registryLb.setId(model.getRegistryId());
		List<RegistryLb> registryLbs = this.registryLbService.getRegistryLb(registryLb);
		Integer registryPort = registryLbs.get(0).getLbPort();
		String nginxDns = registryLbs.get(0).getDomainName();
//		host = hostService.getHost(host);
//		if (null == host) {
//			logger.info("仓库主机【"+model.getHostId()+"】不存在");
//			return new Result(false, "仓库主机【"+model.getHostId()+"】不存在");
//		}
		String imageName = model.getImageName();
		String tag = model.getImageTag();
		// TODO 最终存放在数据库中镜像的名称，可以根据此名称直接从镜像仓库中拉取
		String finalName = nginxDns + ":" + registryPort + "/" + imageName;
		String pushImage = nginxDns+":" + registryPort + "/" + imageName + ":" + tag;// 推送时的镜像名称
		Result tagresult = this.tagImage(null, registryPort, imageName, tag,nginxDns);
		// 镜像打标
		MessageResult message = new MessageResult(tagresult.isSuccess(), tagresult.getMessage(), "镜像发布");
		if (tagresult.isSuccess()) {// 打标成功
			logger.info("发布镜像【"+imageName+"】,打标成功！");
			Result pushResult = imageCore.pushImage(ip, user, pwd,port, pushImage);
			if (pushResult.isSuccess()) {
				logger.info("发布镜像【"+imageName+"】上传仓库成功!");
				Image image = imageService.load(model.getImageId());
				// TODO 这里要注意的是后续要改成单纯的用户输入的镜像名，如果带有仓库的ip port则该镜像只能存放在一个仓库中。
				// 事实上同样的镜像会存在于多个仓库中。后续会对此进行修改。
				image.setImageName(finalName);
				image.setImageStatus(Status.IMAGE.NORMAL.ordinal());
				boolean result = imageService.update(image);
				if (result) {
					message.setMessage("发布镜像【"+imageName+"】成功！");
				} else {
					message.setSuccess(false);
					message.setMessage("发布镜像【"+imageName+"】失败：数据库保存异常！");
				}
			} else {
				logger.error("发布镜像【"+imageName+"】失败!!");
				message.setSuccess(false);
				message.setMessage("发布镜像【"+imageName+"】失败：" + pushResult.getMessage());
			}
		} else {
			logger.error("发布镜像【"+imageName+"】,打标失败!");
			message.setMessage("发布镜像【"+imageName+"】失败：" + tagresult.getMessage());
		}
		return new Result(message.isSuccess(), message.getMessage());
	}

	public Result removeImage(List<String> imageList) {
		if (null == imageList || imageList.isEmpty()) {
			return new Result(true, "删除成功！");
		}
		for (String imageId : imageList) {
			if (!containerService.selectContainersByImageId(imageId).isEmpty()) {
				Image image = imageService.selectByPrimaryKey(imageId);
				return new Result(false, "删除镜像失败，镜像[" + image.getImageName() + "]有在使用的容器。");
			}
		}
		try {
			for (String imageId : imageList) {
				imageService.delete(imageId);
			}
			return new Result(true, "删除镜像成功！");
		} catch (Exception e) {
			logger.error("删除镜像异常："+e.getMessage());
			return new Result(false, "删除镜像失败！");
		}
	}

	public void fastPush(String userId, String imageId) {
		Image image = this.imageService.load(imageId);
		if (null == image) {
			pushMessage(userId, new MessageResult(false, "镜像不存在！", "镜像快速发布"));
		} else {
			// imageCore.pushImage(masterIp, name, password, imageTag)
		}
	}

	private void pushMessage(final String userId, final MessageResult message) {
		messagePush.pushMessage(userId, JSONObject.fromObject(message).toString());
		logger.info("Send message :" + message + "to:" + userId);
	}

	public Image getImage(String imageId) {
		return imageService.load(imageId);
	}

	// 检查镜像别名是否重复
	public Boolean checkImage(String id, String name) {
		Image image = new Image();
		image.setTempName(name);
		image = imageService.selectImage(image);
		//
		if (image != null) {
			if (id == null || id.equals("")) {
				// 创建时 找到imageName为name的image对象
				return false;
			} else if (image.getImageId().equals(id)) {
				// 更新时，找到自身
				return true;
			} else {
				// 更新时，name重复
				return false;
			}
		} else {
			// 没有imageName为name的image对象
			return true;
		}
	}
	
	/*dcoker save将镜像保存为tar包*/
	public JSONObject saveImage(String imageId,String imageName){
		JSONObject jo = new JSONObject();
		//转存主机
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		String loadImagePath = dockerHostConfig.getLoadImagePath();
		SSH ssh = new SSH(ip, user,pwd);
		if (ssh.connect()) {
			String result;
			try {
				Image image = imageService.load(imageId);
				if(null==image||StringUtils.isEmpty(image.getImageName())||StringUtils.isEmpty(image.getImageTag())){
					logger.error("获取镜像信息异常！");
					jo.put("isOk", false);
					jo.put("message", "获取镜像信息异常！");
					return jo;
				}
				String commandLine = "mkdir -p "+loadImagePath
						+";sudo docker tag -f " + image.getImageName()+":"+image.getImageTag() + " " + imageName +  ":" + image.getImageTag() 
						+";sudo docker save " + imageName+":"+image.getImageTag() + " > "+loadImagePath+"/" + imageName +  "_" + image.getImageTag() +".tar";
				result = ssh.executeWithResult(commandLine);
				if(StringUtils.isEmpty(result)){
					jo.put("fullpath", loadImagePath+"/"+ imageName +  "_" + image.getImageTag() +".tar");
					jo.put("isOk", true);
				}else{
					logger.error("仓库主机执行docker save保存镜像时出错！" + result);
					jo.put("message", "仓库主机执行docker save保存镜像时出错！" + result);
					jo.put("isOk", false);
				}
			} catch (Exception e) {
				logger.error("仓库主机执行docker save保存镜像时出错！" + e);
				jo.put("isOk", false);
				jo.put("message", "仓库主机执行docker save保存镜像时出错！");
			}finally{
				ssh.close();
			}
		} else {
			logger.error("仓库主机连接失败！" );
			jo.put("isOk", false);
			jo.put("message", "仓库主机连接失败！");
		}
		return jo;
	}
	
	/*scp到平台服务器*/
	public Result scpImage(String imageId,String imageName,String remotePath) {
		//转存主机
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		String loadImagePath = dockerHostConfig.getLoadImagePath();
		SSH ssh = new SSH(ip,user,pwd);
		String localPath = localConfig.getLocalImagePath();
		boolean b = true;
		try{
			if(ssh.connect()){
				//从远程仓库主机上下载到本地目录
				localPath = FileUploadUtil.check(localPath);
				File direction = new File(localPath);
				if (!direction.exists()) {
					direction.mkdirs();
				}
				//下载到本地目录之前，先对本地目录进行清空
				deleteAll(direction);
				b = ssh.GetFile(remotePath, localPath);
				//scp到平台成功，则删除临时路径下的tar包
				if(b){
					String commandLine = "mkdir -p "+loadImagePath+"; rm -rf "+loadImagePath+"/*";
					String e = ssh.executeWithResult(commandLine, 3000);
					if(!StringUtils.isEmpty(e)){
						logger.error("仓库主机删除镜像tar包时出错！" + e);
						return new Result(false,"仓库主机删除镜像tar包时出错！");
					}
				}
				if(!b){
					logger.error("镜像下载失败：镜像文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
					return new Result(false, "镜像下载失败：镜像文件路径设置不正确导致远程传输出错。远程地址："+remotePath+"    本地地址："+localPath);
				}
			}else{
				logger.error("登录仓库主机失败。");
				return new Result(false, "登录仓库主机失败。");
			}
		}catch(Exception e){
			logger.error("镜像下载失败："+e);
			return new Result(false, "镜像下载失败："+e);
		}finally{
			ssh.close();
		}
		
		return new Result(true, "操作成功");
	}
	
	/*下载到本地*/
	public void download(String imageId,String imageName,String imageTag, HttpServletRequest request,
			HttpServletResponse response,User user) {
		try {
			String localPath = localConfig.getLocalImagePath();
			downLoad.download(localPath+imageName+"_"+imageTag+".tar",imageName+"_"+imageTag+".tar", request, response);
			deleteFile(localPath);
		} catch (Exception e) {
			messagePush.pushMessage(user.getUserId(),"");
			pushMessage(user.getUserId(), new MessageResult(false, "镜像文件下载失败：镜像文件传输出错。", "提示"));
		}
	}
	
	private static boolean deleteFile(String outputFile) {
		File file = null;
		try {
			file = new File(outputFile);
			if (file.exists())
				file.delete();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	//删除某个目录下所有的文件
	private static boolean deleteAll(File file) {
		try{
		   File[] files = file.listFiles();
		   if(null==files){
			   return true;
		   }
		   for (int i = 0; i < files.length; i++){    
		   files[i].delete();      
		   }
		   return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}	
	
	public Result rmImages(String[] imageIds){
		
//		imageCore.removeImages(simpleModels);
		
		return null;
	}
}
