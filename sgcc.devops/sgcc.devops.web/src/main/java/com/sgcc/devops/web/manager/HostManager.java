package com.sgcc.devops.web.manager;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.sgcc.devops.common.bean.MessageResult;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.VersionModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.DockerDaemonDetector;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.detector.HostDetector;
import com.sgcc.devops.core.intf.ClusterCore;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.CompPort;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployNgnixService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.LocationService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.daemon.PlatformDaemon;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.message.MessagePush;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:57:24
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：HostManager.java
 * description：  主机业务操作流程处理类
 */
@org.springframework.stereotype.Component
public class HostManager {

	private static Logger logger = Logger.getLogger(HostManager.class);
	@Resource
	private HostService hostService;
	@Resource
	private ClusterService clusterService;
	@Resource
	private HostCore hostCore;
	@Resource
	private MessagePush messagePush;
	@Resource
	private ContainerService containerService;
	@Resource
	private RegistryService registryService;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private DockerHostConfig dockerHostConfig;
	@Autowired
	private PlatformDaemon platformDaemon;
	@Resource
	private ContainerManager containerManager;
	@Resource
	private SystemService systemService;
	@Resource
	private DeployService deployService;
	@Resource
	private DeployNgnixService deployNgnixService;
	@Resource
	private HostManager hostManager;
	@Resource
	private ClusterManager clusterManager;
	@Resource
	private ClusterCore clusterCore;
	@Resource
	private CompPortManager compPortManager;
	@Resource
	private PortService portService;
	@Resource
	private  HostComponentService hostComponentService;
	@Resource
	private HostComponentManager hostComponentManager;
	@Resource
	private RegistryLbService registryLbService;
	@Resource
	private LocationService locationService;
	@Resource
	private ComponentService componentService;
	@Resource
	private ComponentExpandService componentExpandService;
	@Resource
	private ConportService conportService;
	@Resource
	private TmpHostConfig tmpHostConfig;
	// 创建主机，先调用API连接，成功后保持到数据库dop_host表中
	public Result createHost(JSONObject jsonObject) {
//		String userId = jsonObject.getString("userId");
		String ip = jsonObject.getString("hostIp");
		String username = jsonObject.getString("hostUser");
		String password = jsonObject.getString("hostPwd");
		String hostEnv = jsonObject.getString("hostEnv");
		String hostPort = jsonObject.getString("hostPort");
		// SWARM、DOCKER、REGISTRY、NGINX类型，NGINX不需要判断是否docker环境，其他的需要
		String responseString = hostCore.connect(ip, username, password, hostPort ,3);
		if (null == responseString) {
			logger.info("添加主机【"+ip+"】失败，无法连接到主机或者账号、密码错误！");
			return new Result(false, "添加主机【"+ip+"】失败，无法连接到主机或者账号、密码错误！");
		}
		String uuid = KeyGenerator.uuid();
		Integer cpu = null;
		Integer mem = null;
		String version = null;
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(new ByteArrayInputStream(responseString.getBytes())));
		int stepNull = 0;
		String line = null;
		while (stepNull < 4) {
			try {
				line = reader.readLine();
			} catch (IOException e) {
				logger.error("readline exception:", e);
				continue;
			}
			if (null == line) {
				stepNull++;
			} else if (line.contains("CPU")) {
				cpu = Integer.parseInt(line.split(":")[1]);
			} else if (line.contains("MEM")) {
				String memory = line.split(":")[1];
				mem = Float.valueOf(memory.substring(0, memory.length() - 3)).intValue();
			} else if (line.contains("opration")) {
				version = line.split(":")[1];
			} else {
				continue;
			}
		}
		if (version == null || cpu == null || mem == null) {
			logger.info("添加主机【"+ip+"】失败，获取主机配置信息失败！");
			return new Result(false, "添加主机【"+ip+"】失败，获取主机配置信息失败！");
		}
		jsonObject.put("hostUuid", uuid);
		jsonObject.put("hostCpu", cpu);
		jsonObject.put("hostMem", mem);
		jsonObject.put("hostKernelVersion", version);
		jsonObject.put("hostEnv", hostEnv);
		// 对密码进行加密
		jsonObject.put("password", Encrypt.encrypt(password, localConfig.getSecurityPath()));
		//docker服务状态
		boolean installed = hostCore.dockerInstalled(ip, username, password,hostPort);
		if(installed){
			DockerDaemonDetector dockerDetector = new DockerDaemonDetector(ip, "2375");
			if(dockerDetector.normal()){
				jsonObject.put("dockerStatus", Status.DOCKER.NORMAL.ordinal());
			}else{
				//如果主机docker未启动，查看是否在集群中；在集群中指定仓库启动，不在集群中只启动docker进程
				String re = hostCore.updateAndStartDocker(ip, username, password,hostPort,null,null,null);
				if("0".equals(re)){
					jsonObject.put("dockerStatus", Status.DOCKER.NORMAL.ordinal());
				}else{
					jsonObject.put("dockerStatus", Status.DOCKER.ABNORMAL.ordinal());
				}
			}
		}else{
			jsonObject.put("dockerStatus", Status.DOCKER.UNINSTAILL.ordinal());
		}
		String hostId = hostService.createHost(jsonObject);
		
		if(hostId.contains("fail")){
			return new Result(true, "添加主机【"+ip+"】失败！");
		}else{
			platformDaemon.stopDockerDaemon(hostId);
			return new Result(true, "添加主机【"+ip+"】成功！");
		}
		
	}

	/**
	 * 删除主机 根据主机类型，判断主机是否满足删除的条件 主机类型分为SWARM(0)、DOCKER(1)、Registry(2)
	 * 
	 * @param jo
	 */
	public Result deleteHost(JSONObject jo) {
		Integer hostType = jo.getInt("hostType");
		String hostId = jo.getString("hostId");
		platformDaemon.stopDockerDaemon(hostId);
		MessageResult message = null;
		Host host = hostService.getHost(hostId);
		if (null == host) {
			logger.info("删除主机失败：主机【"+hostId+"】数据异常或者主机已被删除！");
			return new Result(false,"删除主机失败：主机【"+hostId+"】数据异常或者主机已被删除！");
		}
		if (hostType == Type.HOST.SWARM.ordinal()) {
			List<Cluster> clusters = clusterService.listClustersByhostId(hostId);
			if (null == clusters || clusters.isEmpty()) {
				message = new MessageResult(true, "", "删除主机");
			} else {
				List<Host> hosts = new ArrayList<Host>();
				List<Host> temp = null;
				for (Cluster cluster : clusters) {
					temp = this.hostService.listHostByClusterId(cluster.getClusterId());
					hosts.addAll(temp);
				}
				if (hosts.isEmpty()) {
					message = new MessageResult(true, "", "删除主机");
				} else {
					message = new MessageResult(false, "主机【"+host.getHostIp()+"】为集群管理主机且存在Slave主机，不允许删除！", "删除主机");
				}
			}
		} else if (hostType == Type.HOST.DOCKER.ordinal()) {
			try {
				List<Container> containers = containerService.listContainersByHostId(hostId);
				if (null == containers || containers.isEmpty()) {
					message = new MessageResult(true, "", "删除主机");
				} else {
					message = new MessageResult(false, "主机【"+host.getHostIp()+"】上存在运行的容器，不允许删除！", "删除主机");
				}
			} catch (Exception e) {
				logger.info("获取主机【"+host.getHostIp()+"】状态信息异常，不允许删除:"+e);
				message = new MessageResult(false, "获取主机【"+host.getHostIp()+"】状态信息异常，删除失败："+e, "删除主机");
			}
		} else if (hostType == Type.HOST.REGISTRY.ordinal()) {
			logger.info("主机【"+host.getHostIp()+"】是仓库主机，删除失败！");
			message = new MessageResult(false, "主机【"+host.getHostIp()+"】是仓库主机，不能删除！", "删除主机");
		} else if (hostType == Type.HOST.UNUSED.ordinal()) {
			message = new MessageResult(true, "", "删除主机");
		} else {
			logger.info( "主机【"+host.getHostIp()+"】信息异常，暂不支持删除！");
			message = new MessageResult(false, "主机【"+host.getHostIp()+"】信息异常，暂不支持删除！", "删除主机");
		}
		// 根据获取的信息判断是否可以去删除对应的主机
		if (message.isSuccess()) {
			try {
				int result = hostService.deleteHost(jo);
				if (result == 1) {
					logger.info("删除主机【"+host.getHostIp()+"】成功！");
					message.setMessage("删除主机【"+host.getHostIp()+"】成功！");
				} else {
					logger.info("删除主机【"+host.getHostIp()+"】失败：数据库访问异常！");
					message.setMessage("删除主机【"+host.getHostIp()+"】失败：数据库访问异常！");
				}
			} catch (Exception e) {
				logger.info("删除主机【"+host.getHostIp()+"】失败：数据库访问异常:"+e);
				message.setMessage("删除主机【"+host.getHostIp()+"】失败：数据库访问异常:"+e);
			}
		}
		return new Result(message.isSuccess(), message.getMessage());
	}


	/**
	 * 主机添加到集群，先调用API连接，成功后更新数据库dop_host表
	 * 
	 * @param idArray
	 *            集群主机Id连接串数组 如：["1:2","1:3"]; ":"号前面表示集群的ID，后面表示主机的ID。
	 * @param user
	 *            操作用户
	 */
	public Result putToCluster(String[] idArray,String dockerParam, User user) {
//		String userId = user.getUserId();
		MessageResult message = null;
		if (null == idArray || idArray.length == 0) {
			logger.info("没有选择添加的主机");
			return new Result(false, "没有选择添加的主机！");
		}
		StringBuffer errorMessage = new StringBuffer();
		for (String idString : idArray) {
			if (StringUtils.isEmpty(idString)) {
				continue;
			}
			String clusterId = idString.split(":")[0];// 集群ID
			String hostId = idString.split(":")[1];
			String dockerIns = idString.split(":")[2];
			Host host = hostService.getHost(hostId);
			if (null == host) {
				logger.info("主机【" + hostId + "】的主机在数据库中不存在，或已被其他管理员删除！\n");
				errorMessage.append("主键为【" + hostId + "】的主机在数据库中不存在，或已被其他管理员删除！\n");
				continue;
			}
			StringBuffer config = new StringBuffer();
			config.append(host.getHostIp()).append(":2375");
			//
			Cluster cluster = new Cluster();
			cluster.setClusterId(clusterId);
			cluster = clusterService.getCluster(cluster);
			if(null==cluster){
				return new Result(false,"集群【" + clusterId+ "】信息为空！");
			}
			Host master = hostService.getHost(cluster.getMasteHostId());
			if (null == master) {
				logger.info("集群【"+cluster.getClusterName()+"】管理节点所在主机【"+cluster.getMasteHostId()+"】不存在！");
				errorMessage.append("集群【"+cluster.getClusterName()+"】管理节点所在主机【"+cluster.getMasteHostId()+"】不存在！");
				continue;
			}
			if(dockerIns.equals("0")){
//			String response = hostCore.dockerInfo(host.getHostIp(), host.getHostUser(),
//					Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
//			if(null==response){
//				logger.info("Can not connect to host 【" + host.getHostIp() + "】");
//				return new Result(false,"无法连接到主机【" + host.getHostIp() + "】");
//			}
//			String noneFlag = "docker: command not found";
//			String deadFlag = "Cannot connect to the Docker daemon";
//			if(response.contains(noneFlag) || response.contains(deadFlag)){
//					//TODO 未安装，先安装再启动，再检测
//					boolean result =false;
//					if(response.contains(noneFlag)){
//						 result =hostManager.installDocker(host.getHostIp(), host.getHostUser(),
//									Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
//						 logger.info("swarm node host docker env is installing");
//					}
//					if(result||response.contains(deadFlag)){
//						hostCore.startDocker(host.getHostIp(), host.getHostUser(),
//								Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
//						logger.info("swarm node host docker env is starting");
//					}
//			        response = hostCore.dockerInfo(host.getHostIp(), host.getHostUser(),
//						Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
//			        if(null==response){
//					    logger.info("Can not connect to host 【" + host.getHostIp() + "】");
//					    return new Result(false,"无法连接到主机【" + host.getHostIp() + "】");
//			        }
//			        if(response.contains(noneFlag)){
//			        	logger.info("IP为【"+host.getHostIp()+"】的主机添加到集群【"+cluster.getClusterName()+"】失败，机器的环境安装docker失败");
//					     return new Result(false, "IP为【"+host.getHostIp()+"】的主机添加到集群【"+cluster.getClusterName()+"】失败，机器的环境安装docker失败");
//					}
//			        if (response.contains("Cannot connect to the Docker daemon")) {
//				       logger.info("IP为【"+host.getHostIp()+"】的主机添加到集群【"+cluster.getClusterName()+"】失败，机器的环境不支持docker运行");
//				       return new Result(false, "IP为【"+host.getHostIp()+"】的主机添加到集群【"+cluster.getClusterName()+"】失败，机器的环境不支持docker运行");
//					}
//				}
			} else {
				String hostPassword = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
				Result result = hostComponentManager.installHostComponent(host.getHostId(),host.getHostIp(), host.getHostUser(), hostPassword, dockerIns);
				if(!result.isSuccess()){
					logger.error(result.getMessage());
					return result;
				}
			}
			String hostPassword = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
			//docker启动时指定仓库
			RegistryLb registryLb = new RegistryLb();
		    registryLb.setId(cluster.getRegistryLbId());
		    registryLb = registryLbService.selectRegistryLb(registryLb);
			String re = hostCore.updateAndStartDocker(host.getHostIp(), host.getHostUser(), 
					hostPassword,host.getHostPort(),registryLb.getDomainName()+":"+registryLb.getLbPort(),this.dockerLabels(host.getHostId()),dockerParam);
			
			if(re.contains("0")){
				host.setHostStatus((byte)Status.DOCKER.NORMAL.ordinal());
				hostService.update(host);
			}else{
				host.setHostStatus((byte)Status.DOCKER.ABNORMAL.ordinal());
				hostService.update(host);
				return new Result(false, "docker启动参数修改失败！");
			}
			//swarm主机添加仓库负载域名到/etc/hosts
		    re =hostCore.installedInfo(host.getHostIp(), host.getHostUser(),hostPassword, host.getHostPort(),
		    		"sudo echo "+registryLb.getLbIp()+" "+registryLb.getDomainName()+" >> /etc/hosts", 5000l);
			// 解密
			String masterPassword = Encrypt.decrypt(master.getHostPwd(), localConfig.getSecurityPath());
			String responseString = hostCore.connect(master.getHostIp(), master.getHostUser(), masterPassword, master.getHostPort(),0);
			if (StringUtils.isEmpty(responseString)) {
				errorMessage.append("集群【"+cluster.getClusterName()+"】管理节点所在主机【"+master.getHostIp()+"】无法连接\n");
				continue;
			}
			String[] responses = responseString.split("\n");
			if (null != responses) {
				boolean result = hostCore.putToCluster(master.getHostIp(), master.getHostUser(),
						masterPassword,master.getHostPort(), config.toString(), cluster.getManagePath());
				if (result) {
					host.setClusterId(clusterId);
					host.setHostType((byte) Type.HOST.DOCKER.ordinal());
					hostService.update(host);
				} else {
					logger.info("Connect host 【" + host.getHostIp() + "】  network error");
					return new Result(false,"主机【" + host.getHostIp() + "】 添加到集群【"+cluster.getClusterName()+"】失败！");
				}
			}
		}
		if (errorMessage.length() == 0) {
			logger.info("主机添加到集群成功");
			message = new MessageResult(true, "主机添加到集群成功", "主机添加到集群");
		} else {
			logger.info("失败消息：" + errorMessage.toString());
			message = new MessageResult(false, "主机添加到集群失败：" + errorMessage.toString(), "主机添加到集群");
		}
		return new Result(message.isSuccess(), message.getMessage());
	}

	private boolean contains(String[] ids, String id) {
		for (String temp : ids) {
			if (temp.equals(id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 主机从集群解绑
	 * 1、停止此主机上部署的物理系统守护进程
	 * 2、更新swarm配置
	 * 3、主机设置为空闲状态
	 * 4、如果主机所属集群节点大于0，删除此主机容器，重新在集群中创建容器并更新nginx，启动物理系统守护。
	 * 5、如果主机所属节点等于0，删除容器。
	 * 
	 * @param hostIds
	 * @param user
	 */
	public Result clusterRemoveHost(String[] hostIds, User user) {
		if (null == hostIds || hostIds.length == 0) {
			logger.info("没有选择解绑的主机");
			return new Result(false, "没有选择解绑的主机");
		}
		String[] temp = hostIds[0].split(":");
		String clusterId = temp[0];
		String hostId = temp[1];
		Cluster cluster = new Cluster();
		cluster.setClusterId(clusterId);
		cluster = clusterService.getCluster(cluster);
		if (null == cluster) {
			logger.info("主机解绑失败：主键为【"+clusterId+"】的集群不存在，可能已被其他管理员删除！");
			return new Result(false, "主机解绑失败：主键为【"+clusterId+"】的集群不存在，可能已被其他管理员删除！");
		}
		//所有该主机容器所属物理系统守护停止
		List<Container> containers = new ArrayList<>();
		Set<String> systemIdSet = new HashSet<>(); 
		try {
			containers = containerService.selectAllContainerOfHost(hostId);
			if(containers!=null&&containers.size()>0){
				for(Container con:containers){
					systemIdSet.add(con.getSysId());
				}
				
			}
		} catch (Exception e) {
			logger.info(e);
			return new Result(false, "主机解绑失败："+e);
		}
		if(systemIdSet.size()>0){
			for(String sysId:systemIdSet){
				platformDaemon.stopSystemDaemon(sysId);
			}
		}
		
		// 通过clusterId去查找集群下主机的IP
		List<Host> slaveList = hostService.listHostByClusterId(clusterId);
		if (null == slaveList || slaveList.isEmpty()) {
			if(systemIdSet.size()>0){
				for(String sysId:systemIdSet){
					platformDaemon.startSystemDaemon(sysId);
				}
			}
			logger.warn("主机解绑失败：集群节点主机不存在，可能已被其他管理员删除！");
			return new Result(false, "主机解绑失败：集群节点主机不存在，可能已被其他管理员删除！");
		}
		Iterator<Host> hostIterator = slaveList.iterator();
		while (hostIterator.hasNext()) {
			Host item = hostIterator.next();
			if (contains(temp, item.getHostId()) || cluster.getMasteHostId().equals(item.getHostId())) {
				hostIterator.remove();
			}
		}
		StringBuffer config = new StringBuffer();
		for (Host host : slaveList) {
			config.append(host.getHostIp()).append(":2375").append("\n");
		}
		//消除最后一个换行\n
		if(StringUtils.isNotEmpty(config.toString())){
			config = config.delete(config.length()-1, config.length());
		}
		Host clusterHost = hostService.getHost(cluster.getMasteHostId());
		String clusterHostPwd = Encrypt.decrypt(clusterHost.getHostPwd(), localConfig.getSecurityPath());

		String responseString = hostCore.connect(clusterHost.getHostIp(), clusterHost.getHostUser(), clusterHostPwd, clusterHost.getHostPort(),0);
		if (StringUtils.isEmpty(responseString)||responseString.split("\n").length == 0) {
			if(systemIdSet.size()>0){
				for(String sysId:systemIdSet){
					platformDaemon.startSystemDaemon(sysId);
				}
			}
			logger.warn("访问IP为【"+clusterHost.getHostIp()+"】的集群管理主机异常！");
//			return new Result(false, "主机解绑失败：访问IP为【"+clusterHost.getHostIp()+"】的集群管理主机异常！");
		}
		boolean result = hostCore.updateCluster(clusterHost.getHostIp(), clusterHost.getHostUser(),
				clusterHostPwd, clusterHost.getHostPort(),config.toString(), cluster.getManagePath());
		Host host = hostService.getHost(hostId);
		if (null == host) {
			if(systemIdSet.size()>0){
				for(String sysId:systemIdSet){
					platformDaemon.startSystemDaemon(sysId);
				}
			}
			logger.info("主机从集群移除异常：主键为【"+hostId+"】的主机可能已被其他管理员删除！");
			return new Result(false, "主机从集群移除异常：主键为【"+hostId+"】的主机可能已被其他管理员删除！");
		}
		if (!result) {
			logger.warn("更新IP为【"+clusterHost.getHostIp()+"】的集群配置失败！");
		}
		host.setClusterId("");
		host.setHostType((byte) Type.HOST.UNUSED.ordinal());
		hostService.update(host);
		
		//如果除了删除的docker主机外，集群中还有其他docker主机，则只移除容器。再创建容器，并更新nginx.
		//如果没有其他docker主机，将所有部署在这个集群的物理系统置为未部署，所有版本均下线处理，从系统守护中移除该物理系统。
		try {
			//停止并移除主机
			if(containers!=null&&containers.size()>0){
				StringBuffer conIdBuffer = new StringBuffer();
				String[] conIds = new String[containers.size()];
				for(int i=0;i< containers.size();i++){
					conIdBuffer.append(" ");
					conIdBuffer.append(containers.get(i).getConUuid());
					conIdBuffer.append(" ");
					conIds[i] = containers.get(i).getConId();
				}
				containerService.removeContainer(conIds);
				String stopResult = hostCore.stopFromHost( host.getHostIp(),host.getHostUser(),Encrypt.decrypt(host.getHostPwd(),localConfig.getSecurityPath()), host.getHostPort(),conIdBuffer.toString());
				String removeResult = hostCore.removeFromHost( host.getHostIp(),host.getHostUser(),Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort(),conIdBuffer.toString());
			}
			if(slaveList.size()>0){
				//在其他docker主机创建容器
//				List<CompPort> compPortsIn = new ArrayList<CompPort>();
				for(String sysId:systemIdSet){
					com.sgcc.devops.dao.entity.System system = systemService.getSystemById(sysId);
					if(null!=system&&null!=system.getDeployId()){
						Deploy deploy = deployService.getDeploy(system.getDeployId());
						if(null!=deploy){
							List<Container> list = containerService.getBySysId(sysId);
							if(null!=list&&list.size()>0&&deploy.getInstanceCount()>list.size()){
								int conCount = deploy.getInstanceCount()-list.size();
								deploy.setInstanceCount(list.size());
								deployService.updateDeploy(deploy);
								containerManager.createConByDeployId(deploy.getDeployId(),conCount , user.getUserId());
							}else{
								int conCount = deploy.getInstanceCount();
								deploy.setInstanceCount(0);
								deployService.updateDeploy(deploy);
								containerManager.createConByDeployId(deploy.getDeployId(), conCount, user.getUserId());
							}
						}
						// 更新nginx
						try {
							// 更新负载
//							CompPort compPort = new CompPort();
//							compPort.setUsedId(deploy.getDeployId());
//							compPort.setUsedType(Type.COMPPORT_TYPE.DEPLOY.toString());
//							List<CompPort> compPorts= portService.selectAllCompPort(compPort);
//							if(1==deploy.getLoadBalanceType()){
//								return new Result(false, "f5");
//							}else if(2==deploy.getLoadBalanceType()){
//								//组件nginx
//								Result res = compPortManager.updateCompPort(compPorts,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString(),deploy.getLoadBalanceType());
//								if (res != null && !res.isSuccess()) {
//									return res;
//								}
//							}
							Component component = new Component();
							if(deploy!=null&&deploy.getLoadBalanceId()!=null){
								component.setComponentId(deploy.getLoadBalanceId());
								component = componentService.getComponent(component);
							}
							if(component==null){
								logger.error("负载信息获取失败未更新");
								return new Result(false, "负载信息获取失败未更新");
							}
							//单个nginx组件
							if(component.getComponentType()==1){
								Result res = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
								if(!res.isSuccess()){
									return res;
								}
							}
							//nginx组组件
							if(component.getComponentType()==6){
								Result res = compPortManager.modifyNginxGroup(deploy.getLoadBalanceId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
								if(!res.isSuccess()){
									return res;
								}
							}
							//f5组件
							if(component.getComponentType()==2){
								ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
								//是否自动配置
								if(componentExpand.getAutoConfigF5()==1){
									// F5测试
									F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
											Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
									boolean re = detector.normal();
									if(!re){
										return new Result(false, "F5服务器通信失败，无法自动配置！");
									}else{
										F5Model f5Model = new F5Model();
										f5Model.setHostIp(component.getComponentIp());
										f5Model.setUser(component.getComponentUserName());
										f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
										f5Model.setPort(deploy.getF5ExternalPort());
										StringBuffer members = new StringBuffer();
										for(Container container:containers){
											List<ConPort> conPorts = conportService.listConPorts(container.getConId());
											for(ConPort conPort:conPorts){
												members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
											}
										}
										if(StringUtils.isNotEmpty(members.toString())){
											f5Model.setMembers(members.toString());
											f5Model.setPoolName("sgcc_pool");
											Result f5ReSult = compPortManager.delF5MembersConfig(f5Model);
											if(!f5ReSult.isSuccess()){
												return f5ReSult;
											}
										}
									}
								}
							}
						} catch (Exception e) {
							logger.info("负载更新失败: " + e);
							return new Result(false, "负载更新失败："+e);
						}
						
					}
				}
				if(systemIdSet.size()>0){
					for(String sysId:systemIdSet){
						platformDaemon.startSystemDaemon(sysId);
					}
				}
			}else{
				// 更新负载
				for(String sysId:systemIdSet){
					com.sgcc.devops.dao.entity.System system = systemService.getSystemById(sysId);
					String deployId = system.getDeployId();
					//由于所在集群中没有docker服务器器，所有的物理系统部署的版本将全部无效
					system.setDeployId("");
					system.setSystemElsasticityStatus((byte)Status.ElSASTICITYSTATUS.UNUSED.ordinal());
					systemService.updateSystem(system);
					
					if(null!=deployId){
						Deploy deploy = deployService.getDeploy(deployId);
						try {
							CompPort compPort = new CompPort();
							compPort.setUsedId(deploy.getDeployId());
							compPort.setUsedType(Type.COMPPORT_TYPE.DEPLOY.toString());
//							List<CompPort> compPorts= portService.selectAllCompPort(compPort);
//							if(1==deploy.getLoadBalanceType()){
//								return new Result(false, "f5");
//							}else if(2==deploy.getLoadBalanceType()){
//								//组件nginx
//								Result res = compPortManager.updateCompPort(compPorts,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString(),deploy.getLoadBalanceType());
//								if (res != null && !res.isSuccess()) {
//									return res;
//								}
//							}
							Component component = new Component();
							component.setComponentId(deploy.getLoadBalanceId());
							component = componentService.getComponent(component);
							if(component==null){
								logger.error("负载信息获取失败未更新");
								return new Result(false, "负载信息获取失败未更新");
							}
							//单个nginx组件
							if(component.getComponentType()==1){
								Result res = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.REGISTRY.toString());
								if(!res.isSuccess()){
									return res;
								}
							}
							//nginx组组件
							if(component.getComponentType()==6){
								Result res = compPortManager.modifyNginxGroup(deploy.getLoadBalanceId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.REGISTRY.toString());
								if(!res.isSuccess()){
									return res;
								}
							}
							//f5组件
							if(component.getComponentType()==2){
								ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
								//是否自动配置
								if(componentExpand.getAutoConfigF5()==1){
									// F5测试
									F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
											Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
									boolean re = detector.normal();
									if(!re){
										return new Result(false, "F5服务器通信失败，无法自动配置！");
									}else{
										F5Model f5Model = new F5Model();
										f5Model.setHostIp(component.getComponentIp());
										f5Model.setUser(component.getComponentUserName());
										f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
										f5Model.setPort(deploy.getF5ExternalPort());
										StringBuffer members = new StringBuffer();
										for(Container container:containers){
											List<ConPort> conPorts = conportService.listConPorts(container.getConId());
											for(ConPort conPort:conPorts){
												members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
											}
										}
										if(StringUtils.isNotEmpty(members.toString())){
											f5Model.setMembers(members.toString());
											f5Model.setPoolName("sgcc_pool");
											Result f5ReSult = compPortManager.delF5MembersConfig(f5Model);
											if(!f5ReSult.isSuccess()){
												return f5ReSult;
											}
										}
									}
								}
							}
						} catch (Exception e) {
							logger.info("负载更新失败: " + e);
							return new Result(false, "负载更新失败："+e);
						}
					}
				}
			}
		} catch (Exception e) {
			logger.info("主机【"+host.getHostIp()+"】解绑失败:"+e);
			return new Result(false, "主机【"+host.getHostIp()+"】解绑失败:"+e);
		}
		logger.info("主机【"+host.getHostIp()+"】从集群【"+cluster.getClusterName()+"】解绑成功!");
		return new Result(true, "主机从集群解绑成功");
	}

	/**
	 * 更新主机信息
	 * 
	 * @param userId
	 * @param model
	 * @return
	 */
	public Result updateHost(String userId, HostModel model) {
		try {
			Host host = new Host();
			String ip = model.getHostIp();
			String username = model.getHostUser();
			String password = model.getHostPwd();
			String hostport = model.getHostPort();
			int hosttype = (int)model.getHostType();
			//ByteArrayInputStream in = new ByteArrayInputStream(hosttype);
			//int port = Integer.parseInt(model.getHostPort());
			//如果是密码未修改，则解密后判断是否通，如果密码修改了，则拿到密码后直接判断是否通
			//System.out.println(password);
			if (!model.getHostPwd().startsWith("{AES}")){
				password = model.getHostPwd();
			}else{
				password = Encrypt.decrypt(model.getHostPwd(), localConfig.getSecurityPath());
			}
			
			//System.out.println(ip+"  "+username+"   "+password);
			String responseString = hostCore.connect(ip, username, password,hostport,hosttype);
			if (null == responseString) {
				logger.info("添加主机【"+ip+"】失败，无法连接到主机或者账号、密码、端口错误！");
				return new Result(false, "添加主机【"+ip+"】失败，无法连接到主机或者账号、密码、端口错误！");
			}
			
			//对密码进行加密
			
			model.setHostPwd(Encrypt.encrypt(password,localConfig.getSecurityPath()));
			//System.out.println(model.getHostPwd());
			
			BeanUtils.copyProperties(model, host);
			int result = hostService.update(host);
			return result > 0 ? new Result(true, "修改主机【"+model.getHostIp()+"】成功！") : new Result(false, "修改主机【"+model.getHostIp()+"】失败！");
		} catch (Exception e) {
			logger.info("Update host【"+model.getHostIp()+"】 fail:"+e.getMessage());
			return new Result(false, "修改主机【"+model.getHostIp()+"】失败：数据库保存失败！");
		}
	}
	
	/**
	 * 更新主机信息
	 * 
	 * @param userId
	 * @param model
	 * @return
	 */
	public Result checkSSH(String userId, HostModel model) {
		try {
			Host host = new Host();
			String ip = model.getHostIp();
			String username = model.getHostUser();
			String password = model.getHostPwd();
			String hostport = model.getHostPort();
			int hosttype = (int)model.getHostType();
			//ByteArrayInputStream in = new ByteArrayInputStream(hosttype);
			//int port = Integer.parseInt(model.getHostPort());
			//如果是密码未修改，则解密后判断是否通，如果密码修改了，则拿到密码后直接判断是否通
			//System.out.println(password);
			if (!model.getHostPwd().startsWith("{AES}")){
				password = model.getHostPwd();
			}else{
				password = Encrypt.decrypt(model.getHostPwd(), localConfig.getSecurityPath());
			}
			
			//System.out.println(ip+"  "+username+"   "+password);
			String responseString = hostCore.connect(ip, username, password,hostport,hosttype);
			if (null == responseString) {
				logger.info("连接主机【"+ip+"】失败，无法连接到主机或者账号、密码、端口错误！");
				return new Result(false, "无法连接到主机！");
			}else{
				return new Result(true,"连接到主机成功！");
			}
			
			//对密码进行加密
			
			
			//return result > 0 ? new Result(true, "修改主机【"+model.getHostIp()+"】成功！") : new Result(false, "修改主机【"+model.getHostIp()+"】失败！");
		} catch (Exception e) {
			logger.info("Update host【"+model.getHostIp()+"】 fail:"+e.getMessage());
			return new Result(false, "连接主机【"+model.getHostIp()+"】失败！");
		}
	}

	// 创建主机时检查IP
	public boolean queryIP(HostModel hostModel) {
		return hostService.queryIP(hostModel);
	}

	/**
	 * 检查hostIp是否存在
	 * 
	 * @param ip
	 * @return
	 */
	public Boolean checkHostIpRepeat(String ip) {
		Host host = new Host();
		host.setHostIp(ip);
		host = hostService.getHost(host);
		return null == host;
	}

	/**
	 * 检查hostName是否存在
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public Boolean checkHostNameRepeat(String id, String name) {
		Host host = new Host();
		host.setHostName(name);
		host = hostService.getHost(host);
		if (null == host) {
			return true;
		}
		if (StringUtils.isEmpty(id)) {
			return false;
		}
		return host.getHostId().equals(id);
	}

	/**
	* TODO物理系统名称
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年2月23日
	*/
	public JSONArray getSystemNames(String hostId) {
		List<com.sgcc.devops.dao.entity.System> systemNames= systemService.getSystemNames(hostId);
		JSONArray ja = JSONUtil.parseObjectToJsonArray(systemNames);
		return ja;
	}

	/**
	* TODO测试主机连接
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年3月8日
	*/
	public Result reTest(String hostId) {
		Host host = hostService.getHost(hostId);
		HostDetector detector = new HostDetector(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
		if(detector.normal()){
			host.setHostStatus((byte)Status.HOST.NORMAL.ordinal());
			hostService.update(host);
			return new Result(true, "主机【"+host.getHostIp()+"】已重新连接!");
		}else{
			return new Result(false, "主机【"+host.getHostIp()+"】重新连接失败!");
		}
		
	}

	/**
	* docker进程
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年3月8日
	*/
	public Result dockerDaemon(String hostId, String opration,User user) {
		Host host = hostService.getHost(hostId);
		if(null==host){
			return new Result(false, "主机ID【"+hostId+"】信息查询失败!");
		}
		//安装
		if("install".equals(opration)){
			return new Result(true, "主机【"+host.getHostIp()+"】暂时不支持Docker安装!");
		}
		//启动进程systemctl start docker
		if("start".equals(opration)){
			String resp = hostCore.startDocker(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
			//输出0执行状态为成功
			if("0\n".equals(resp)){
				host.setDockerStatus((byte)Status.DOCKER.NORMAL.ordinal());
				hostService.update(host);
				return new Result(false, "主机【"+host.getHostIp()+"】启动Docker进程成功!");
			}else{
				return new Result(true, "主机【"+host.getHostIp()+"】启动Docker进程失败:"+resp);
			}
		}
		//停止进程systemctl stop docker;kill docker daemon
		if("stop".equals(opration)){
			//如果在集群中，先移除
			if(!StringUtils.isEmpty(host.getClusterId())){
				String[] hostIds={host.getClusterId()+":"+host.getHostId()};
				Result result = this.clusterRemoveHost(hostIds, user);
				if(!result.isSuccess()){
					return result;
				}
			}
			//停止进程
			boolean resp = hostCore.stopDocker(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
			if(resp){
				host.setDockerStatus((byte)Status.DOCKER.ABNORMAL.ordinal());
				hostService.update(host);
				return new Result(false, "主机【"+host.getHostIp()+"】停止Docker进程成功!");
			}else{
				return new Result(true, "主机【"+host.getHostIp()+"】停止Docker进程失败!");
			}
			
		}
		if("check".equals(opration)){
			boolean installed = hostCore.dockerInstalled(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
			//是否安装
			if(installed){
				String response = hostCore.dockerInfo(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
				//是否启动
				if(null==response||response.contains("Cannot connect to the Docker daemon")){
					host.setDockerStatus((byte)Status.DOCKER.ABNORMAL.ordinal());
					hostService.update(host);
					return new Result(false, "IP为【"+host.getHostIp()+"】的主机Docker未启动");
				}else{
					host.setDockerStatus((byte)Status.DOCKER.NORMAL.ordinal());
					hostService.update(host);
					return new Result(true, "主机【"+host.getHostIp()+"】Docker运行正常!");
				}
			}else{
				host.setDockerStatus((byte)Status.DOCKER.UNINSTAILL.ordinal());
				hostService.update(host);
				return new Result(true, "主机【"+host.getHostIp()+"】未安装Docker!");
			}
		}
		return new Result(true, "主机【"+host.getHostIp()+"】操作成功!");
	}
	
	/**
	* 查询某组件关联的主机信息列表
	* @author yueyong
	* @return JSONArray
	* @version 1.0
	* 2016年3月9日
	*/
	public JSONArray selectComponentHost(String componentId) {
		List<ComponentHost> hosts = hostService.selectComponentHost(componentId);
		JSONArray ja = JSONUtil.parseObjectToJsonArray(hosts);
		return ja;
	}

	/**
	* swarm启动、停止、健康检查
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年3月23日
	*/
	public Result swarmDaemon(String clusterId, String opration, User user) {
		//停止守护
		platformDaemon.stopClusterDaemon(clusterId);
		
		Cluster cluster = clusterService.getCluster(clusterId);
		if(null == cluster){
			return new Result(false, "集群ID【"+clusterId+"】信息查询失败!");
		}
		Host host = hostService.getHost(cluster.getMasteHostId());
		if(null==host){
			return new Result(false, "集群【"+cluster.getClusterName()+"】主机信息查询失败!");
		}
		//安装
		if("install".equals(opration)){
			return new Result(true, "主机【"+host.getHostIp()+"】暂时不支持swarm安装!");
		}
		//启动进程
		if("start".equals(opration)){
			String config = "/home/" + host.getHostUser() + "/" + cluster.getManagePath();
			String resp = hostCore.startSwarm(host.getHostIp(), host.getHostUser(), 
					Encrypt.decrypt(host.getHostPwd(),localConfig.getSecurityPath()),host.getHostPort(),cluster.getClusterPort(),config);
			//输出0执行状态为成功
			if(resp.isEmpty()){
				//失败状态设置为异常
				platformDaemon.startClusterDaemon(clusterId);
				cluster.setClusterStatus((byte)Status.CLUSTER.NORMAL.ordinal());
				clusterService.update(cluster);
				return new Result(false, "主机【"+host.getHostIp()+"】启动swarm进程成功!");
			}else{
				return new Result(true, "主机【"+host.getHostIp()+"】启动swarm进程失败:"+resp);
			}
		}
		//停止进程
		if("stop".equals(opration)){
			
			//停止进程
			boolean resp = hostCore.stopSwarm(host.getHostIp(), host.getHostUser(), 
					Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort(),cluster.getClusterPort());
			if(resp){
				cluster.setClusterStatus((byte)Status.CLUSTER.ABNORMAL.ordinal());
				clusterService.update(cluster);
				return new Result(false, "主机【"+host.getHostIp()+"】停止swarm进程成功!");
			}else{
				return new Result(true, "主机【"+host.getHostIp()+"】停止swarm进程失败!");
			}
			
		}
		if("check".equals(opration)){
			Host slaveHost = new Host();
			slaveHost.setClusterId(clusterId);
			slaveHost.setHostType((byte)Type.HOST.DOCKER.ordinal());
			List<Host> slaveList = hostService.selectHostListByHost(slaveHost);
			String result = hostCore.clusterHealthCheck(host.getHostIp(), host.getHostUser(), 
					Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort(),cluster.getClusterPort());
			Iterator<Host> iterator = slaveList.iterator();
			if(StringUtils.isNotEmpty(result)){
				String[] slaves = result.split("\n");
				while(iterator.hasNext()){
					Host item = iterator.next();
					if(contains(slaves, item.getHostIp())){
						iterator.remove();
					}
				}
			}
			
			if (slaveList.size() == 0) {
				logger.info("Check cluster consistent");
				return new Result(true, "集群健康，数据库和服务器一致！");
			} else {
				logger.info("Check cluster inconsistent");
				return new Result(false, "集群不健康,数据库和服务器不一致！");
			}
		}
		return new Result(true, "集群【"+host.getHostIp()+"】操作成功!");
	}

	/**
	* 批量新增
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月7日
	*/
	public Result createGroup(HostModel hostModel,User user) {
		String beginIp = hostModel.getBeginIp();
		String endIp = hostModel.getEndIp();
		if(!beginIp.substring(0, beginIp.lastIndexOf(".")).equals(endIp.substring(0, endIp.lastIndexOf(".")))){
			return new Result(false, "开始IP:【"+beginIp+"】与结束IP:【"+endIp+"】不是同一网段！");
		}
		if(Integer.parseInt(beginIp.substring( beginIp.lastIndexOf(".")+1))>Integer.parseInt(endIp.substring(endIp.lastIndexOf(".")+1))){
			return new Result(false, "开始IP:【"+beginIp+"】不能大于结束IP:【"+endIp+"】！");
		}
		String start = beginIp.substring(0, beginIp.lastIndexOf("."));
		int startWith = Integer.parseInt(beginIp.substring( beginIp.lastIndexOf(".")+1));
		int endWith = Integer.parseInt(endIp.substring( endIp.lastIndexOf(".")+1));
		
		String result = "";
		for(int i = startWith;i<=endWith;i++){
			String hostIp = start+"."+i;
			Host host =hostService.getHostByIp(hostIp);
			if(host!=null){
				result += "IP【"+hostIp+"】已存在，忽略。</br>";
			}else{
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("hostIp", hostIp);
				jsonObject.put("hostUser", hostModel.getHostUser());
				jsonObject.put("hostPwd", hostModel.getHostPwd());
				jsonObject.put("hostName", hostIp);
				jsonObject.put("hostEnv", hostModel.getHostEnv());
				jsonObject.put("hostDesc", "批量添加");
				jsonObject.put("userId", user.getUserId());
				jsonObject.put("locationId", hostModel.getLocationId());
				Result result2 = this.createHost(jsonObject);
				if(!result2.isSuccess()){
					result += "IP【"+hostIp+"】添加异常："+result2.getMessage()+"</br>";
				}
			}
		}
		if(StringUtils.isEmpty(result)){
			return new Result(true, "批量新增主机成功，开始IP【"+beginIp+"】-结束IP【"+endIp+"】");
		}else{
			return new Result(false, "批量新增主机，开始IP【"+beginIp+"】-结束IP【"+endIp+"】。</br>"+result);
		}
	}
	public  byte[] exportHost(String locationId){
		return hostService.exportHost(locationId);
	}
	public   byte[] importHostExcel(){
		return hostService.importHostExcel();
	}

	/**
	* TODO
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年4月14日
	*/
	public HostModel getHost(String hostId) {
		Host host = hostService.getHost(hostId);
		HostModel target = new HostModel();
		BeanUtils.copyProperties(host, target);
		return target;
	}
	public boolean installDocker(String ip,String username,String password,String hostport) {
		//TODO
		String file =localConfig.getLocalHostInstallPath()+"docker-rpm/";
		String hostFileLocation=dockerHostConfig.getHostInstallPath();
		String hostFileName="docker-rpm";
		boolean result =hostCore.installAllFile(ip, username, password, hostport,file, hostFileLocation, hostFileName);
		return result;
	}
	
	/**
	 * 主机安装软件
	 * 执行结果返回
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年5月5日
	 */
	public Result installSoftWare(String hostId,String componentId,String operationType){
		String result = "";
		StringBuffer command = new StringBuffer();
		Host host = hostService.getHost(hostId);
		HostComponent hostComponent = new HostComponent();
		hostComponent.setId(componentId);
		hostComponent = hostComponentService.getHostComponent(hostComponent);
		String localFilePath = hostComponent.getLocalPath();
		String fileName = hostComponent.getFilename();
		String remoteFilePath = (dockerHostConfig.getHostInstallPath()+"/"+hostComponent.getId()).replace("\\", "/").replace("//", "/");
		if(!StringUtils.isEmpty(fileName)){
			localFilePath = (localFilePath+"/"+fileName).replace("\\", "/").replace("//", "/");
//			remoteFilePath = (remoteFilePath+"/"+hostComponent.getId()).replace("\\", "/").replace("//", "/");
		}
		if(operationType.equals("1")){
			//安装命令
			command.append(hostComponent.getInstallCommand());
		}else{
			//升级命令
			command.append(hostComponent.getUpgradeCommand());
		}
		if(command.toString().trim().endsWith(";")){
			command.append(" echo $?;");
		}else{
			command.append(";echo $?;");
		}
		String localHostPath = (localConfig.getLocalHostInstallPath()+"/"+hostComponent.getId()).replace("\\", "/").replace("//", "/");
		String hostIp = host.getHostIp();
		String hostUser = host.getHostUser();
		String decrypt = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		String tmpIp = tmpHostConfig.getTmpHostIp();
		String tmpUser = tmpHostConfig.getTmpHostUser();
		String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
		String hostPort = tmpHostConfig.getTmpHostPort();
		SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd,Integer.parseInt(hostPort));
		result = hostCore.hostInstallSoftware(ssh,hostIp, hostUser, decrypt,hostPort,localHostPath,fileName, localFilePath, remoteFilePath, command.toString(),100000L);
		if(result.trim().endsWith("0")){
			return new Result(true, "主机【"+hostIp+"】安装组件执行成功："+result);
		}else{
			return new Result(true, "主机【"+hostIp+"】安装组件执行结果："+result);
		}
		
	}
	/**
	 * 主机安装组件版本查询
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年5月5日
	 */
	public Result installedVersion(){
		String result = "";
		String hostIp="",hostUser="",decrypt="", command="",hostport="";
		result = hostCore.installedInfo(hostIp, hostUser, decrypt,hostport, command,100000L);
		return new Result(true, "主机【"+hostIp+"】已安装组件版本信息查询执行结果："+result);
	}
	/**
	 * 主机组件运行状态查询
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年5月5日
	 */
	public Result installedStatus(){
		String result = "";
		String hostIp="",hostUser="",decrypt="", command="",hostport="";
		result = hostCore.installedInfo(hostIp, hostUser, decrypt,hostport, command,100000L);
		return new Result(true, "主机【"+hostIp+"】已安装组件运行状态查询执行结果："+result);
	}
	public Result isInstalled(HostModel host){
		boolean installed = hostCore.dockerInstalled(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()),host.getHostPort());
		if(installed){
			return new Result(true, "主机【"+host.getHostIp()+"】安装Docker!");
		}
		return new Result(false, "主机【"+host.getHostIp()+"】未安装Docker!");
	}
	public VersionModel getVersion(VersionModel versionModel){
		if(org.apache.commons.lang.StringUtils.isNotEmpty(versionModel.getHostID())){
			Host host = hostService.getHost(versionModel.getHostID());
			versionModel.setHostIp(host.getHostIp());
			versionModel.setHostPassword(host.getHostPwd());
			versionModel.setHostName(host.getHostUser());
			versionModel.setHostPort(host.getHostPort());
			}
		String hostIp =versionModel.getHostIp();
		String hostUser =versionModel.getHostName();
		String decrypt =Encrypt.decrypt(versionModel.getHostPassword(), localConfig.getSecurityPath());
		String hostPort = versionModel.getHostPort();
		  if(versionModel.getType().equals("nginx")){
	        	versionModel.setNginxVersion(hostCore.getNginxVersion(hostIp, hostUser, decrypt,hostPort));
	         }
	        else if(versionModel.getType().equals("docker")){
	        	versionModel.setDockerVersion(hostCore.getDockerVersion(hostIp, hostUser, decrypt,hostPort));
	        }
	        else if(versionModel.getType().equals("swarm")){
	        	versionModel.setSwarmVersion(hostCore.getSwarmVersion(hostIp, hostUser, decrypt,hostPort));
	        	versionModel.setDockerVersion(hostCore.getDockerVersion(hostIp, hostUser, decrypt,hostPort));
	        }
	        else if(versionModel.getType().equals("registry")){
	        	versionModel.setRegistryVersion(hostCore.getRegistryVersion(hostIp, hostUser, decrypt,hostPort));
	        	versionModel.setDockerVersion(hostCore.getDockerVersion(hostIp, hostUser, decrypt,hostPort));
	        }
		return versionModel;
	}
	
	//主机的位置层级
	private Map<String, String> hostLocationMap(String hostId){
		Map<String, String> map = new HashMap<>();
		
		Host host = hostService.getHost(hostId);
		if(null==host||StringUtils.isEmpty(host.getLocationId())){
			return null;
		}
		Location location = new Location();
		location.setId(host.getLocationId());
		location = locationService.select(location);
		String parentLocationCode = location.getLocationParent();
		map.put(location.getLocationCode(), location.getLocationParamCode());
		//父节点不是最顶级节点
		while(!"0".equals(parentLocationCode)){
			Location parentLocation = new Location();
			parentLocation.setId(location.getLocationParent());
			parentLocation = locationService.select(parentLocation);
			parentLocationCode = parentLocation.getLocationParent();
			map.put(parentLocation.getLocationCode(), parentLocation.getLocationParamCode());
		}
		return map;
	}
	
	public String dockerLabels(String hostId){
		Map<String, String> map = this.hostLocationMap(hostId);
		StringBuffer dockerLabels = new StringBuffer();
		for(Map.Entry<String, String> entry:map.entrySet()){
			dockerLabels.append(" --label "+entry.getKey().toLowerCase()+"="+entry.getValue().toLowerCase());
		}
		return dockerLabels.toString();
	}
}
