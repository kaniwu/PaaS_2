package com.sgcc.devops.web.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.model.UpFileModel;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.message.Message;

@Component
public class UploadServiceWebSocketHandler implements WebSocketHandler {
	private static Logger logger = Logger.getLogger(UploadServiceWebSocketHandler.class);
	@Resource
	private HostService hostService;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private LocalConfig localConfig;
	private static List<WebSocketSession> currentUsers;

	private static List<WebSocketSession> getCurrentUsers() {
		return currentUsers;
	}

	private static void setCurrentUsers(List<WebSocketSession> currentUsers) {
		UploadServiceWebSocketHandler.currentUsers = currentUsers;
	}

	static {
		UploadServiceWebSocketHandler.setCurrentUsers(new ArrayList<WebSocketSession>());
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		UploadServiceWebSocketHandler.getCurrentUsers().add(session);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		if (message instanceof TextMessage) {
			handleTextMessage(session, (TextMessage) message);
		} else if (message instanceof BinaryMessage) {
			handleBinaryMessage(session, (BinaryMessage) message);
		} else {
			logger.error("Unexpected WebSocket message type: " + message);
			throw new IllegalStateException("Unexpected WebSocket message type: " + message);
		}
	}

	// 处理字符串
	private void handleTextMessage(WebSocketSession session, TextMessage message) {
		try {
			if (message.getPayload().contains("fileStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				String fileDir = localConfig.getLocalAppPath()+filemodel.getSystemId()+"\\"+filemodel.getTimestamp();
				fileDir = fileDir.replace("\\", "/");
				fileDir = FileUploadUtil.check(fileDir);
				File temFile = new File(fileDir);
				temFile.mkdirs();
				filemodel.setFileSrc(fileDir+"/"+filemodel.getFileStartName());
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("OK"))));
			}
			//应用包上传
			if (message.getPayload().contains("appFileStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				String fileDir = localConfig.getLocalAppPath()+filemodel.getSystemId()+"/"+filemodel.getTimestamp();
				fileDir = fileDir.replace("\\", "/").replace("//", "/");
				fileDir = FileUploadUtil.check(fileDir);
				File temFile = new File(fileDir);
				temFile.mkdirs();
				filemodel.setFileSrc(fileDir+"/"+filemodel.getAppFileStartName());
				filemodel.setUuid(filemodel.getSystemId()+"/"+filemodel.getTimestamp());
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				UploadMessage uploadMessage = new UploadMessage("OK");
				String path =tmpHostConfig.getAppPath()+"/"+filemodel.getSystemId()+"/"+filemodel.getTimestamp();
				path = path.replace("\\", "/").replace("//", "/");
				uploadMessage.setAppFilePath(path+"/"+filemodel.getAppFileStartName());
				uploadMessage.setAppUuid(filemodel.getTimestamp());
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(uploadMessage)));
			}else if (message.getPayload().contains("appFileSendover")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				//转存
				if(tmpHostConfig.isTransferNeeded()){
					String tmpIp = tmpHostConfig.getTmpHostIp();
					String tmpUser = tmpHostConfig.getTmpHostUser();
					String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
					SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
					if(ssh.connect()){
						try {
							ssh.execute("mkdir -p "+tmpHostConfig.getAppPath()+"/"+filemodel.getUuid(), 5000);
							ssh.close();
						} catch (Exception e) {
							ssh.close();
							e.printStackTrace();
						}
					}
					if(ssh.connect()){
						ssh.SCPFile(filemodel.getFileSrc(), tmpHostConfig.getAppPath()+"/"+filemodel.getUuid());
						ssh.close();
					}
				}
			} else if (message.getPayload().contains("cancel")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(
						new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("CANCEL"))));
				return;
			}
			//部署配置文件
			if(message.getPayload().contains("deployConfigStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				String fileDir = localConfig.getLocalDeployPath()+filemodel.getSystemId()+"/"+filemodel.getTimestamp();
				fileDir = fileDir.replace("\\", "/").replace("//", "/");
				File temFile = new File(fileDir);
				temFile.mkdirs();
				filemodel.setFileSrc(fileDir+"/"+filemodel.getDeployConfigStartName());
				filemodel.setUuid(filemodel.getSystemId()+"/"+filemodel.getTimestamp());
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				UploadMessage uploadMessage = new UploadMessage("OK");
				String path =tmpHostConfig.getDeployPath()+filemodel.getSystemId()+"/"+filemodel.getTimestamp()+"/"+filemodel.getDeployConfigStartName();
				path = path.replace("\\", "/").replace("//", "/");
				uploadMessage.setAppFilePath(path);
				uploadMessage.setAppUuid(filemodel.getTimestamp());
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(uploadMessage)));
			}else if (message.getPayload().contains("deployConfigSendover")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				//转存
				if(tmpHostConfig.isTransferNeeded()){
					String tmpIp = tmpHostConfig.getTmpHostIp();
					String tmpUser = tmpHostConfig.getTmpHostUser();
					String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
					SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
					if(ssh.connect()){
						try {
							ssh.execute("mkdir -p "+tmpHostConfig.getDeployPath()+"/"+filemodel.getUuid(), 5000);
							ssh.close();
						} catch (Exception e) {
							ssh.close();
							e.printStackTrace();
						}
					}
					if(ssh.connect()){
						ssh.SCPFile(filemodel.getFileSrc(), tmpHostConfig.getDeployPath()+"/"+filemodel.getUuid());
						ssh.close();
					}
				}
			} else if (message.getPayload().contains("cancel")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(
						new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("CANCEL"))));
				return;
			}
			
			//预加载包
			if (message.getPayload().contains("libraryFileStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				String uuid = KeyGenerator.uuid();
				String fileDir = localConfig.getLocalLibraryPath()+"/"+uuid;
				fileDir = fileDir.replace("\\", "/").replace("//", "/");
				File temFile = new File(fileDir);
				temFile.mkdirs();
				filemodel.setFileSrc(fileDir+"/"+filemodel.getLibraryFileStartName());
				filemodel.setUuid(uuid);
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				UploadMessage uploadMessage = new UploadMessage("OK");
				uploadMessage.setLibraryFilePath(tmpHostConfig.getLibraryPath()+"/"+uuid+"/"+filemodel.getLibraryFileStartName());
				uploadMessage.setLibraryUuid(uuid);
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(uploadMessage)));
			}else if (message.getPayload().contains("libraryFileSendover")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				//转存
				if(tmpHostConfig.isTransferNeeded()){
					String tmpIp = tmpHostConfig.getTmpHostIp();
					String tmpUser = tmpHostConfig.getTmpHostUser();
					String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
					SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
					if(ssh.connect()){
						try {
							ssh.execute("mkdir -p "+tmpHostConfig.getLibraryPath()+"/"+filemodel.getUuid(), 5000);
							ssh.close();
						} catch (Exception e) {
							ssh.close();
							e.printStackTrace();
						}
					}
					if(ssh.connect()){
						ssh.SCPFile(filemodel.getFileSrc(), tmpHostConfig.getLibraryPath()+"/"+filemodel.getUuid());
						ssh.close();
					}
				}
			} else if (message.getPayload().contains("cancel")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(
						new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("CANCEL"))));
				return;
			}
			//主机组件
			if (message.getPayload().contains("hostCompFileStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				String uuid = KeyGenerator.uuid();
				String fileDir = localConfig.getLocalHostInstallPath()+"/"+uuid;
				fileDir = fileDir.replace("\\", "/");
				File temFile = new File(fileDir);
				temFile.mkdirs();
				filemodel.setFileSrc((fileDir+"/"+filemodel.getHostCompFileStartName()).replace("\\", "/").replace("//", "/"));
				filemodel.setUuid(uuid);
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				UploadMessage uploadMessage = new UploadMessage("OK");
				uploadMessage.setHostCompFilePath(filemodel.getHostCompFileStartName());
				String hostInstallPath = tmpHostConfig.getHostInstallPath()+"/"+filemodel.getUuid();
				hostInstallPath = hostInstallPath.replace("\\", "/").replace("//", "/");
				uploadMessage.setHostInsallPath(hostInstallPath);
				uploadMessage.setHostCompUUid(uuid);
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(uploadMessage)));
			}else if (message.getPayload().contains("hostCompFileSendover")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				//转存
				if(tmpHostConfig.isTransferNeeded()){
					String tmpIp = tmpHostConfig.getTmpHostIp();
					String tmpUser = tmpHostConfig.getTmpHostUser();
					String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
					SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
					if(ssh.connect()){
						try {
							ssh.execute("mkdir -p "+tmpHostConfig.getHostInstallPath()+"/"+filemodel.getUuid(), 5000);
							ssh.close();
						} catch (Exception e) {
							ssh.close();
							e.printStackTrace();
						}
					}
					if(ssh.connect()){
						ssh.SCPFile(filemodel.getFileSrc(), tmpHostConfig.getHostInstallPath()+"/"+filemodel.getUuid());
						ssh.close();
					}
				}
			} else if (message.getPayload().contains("cancel")) {
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(
						new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("CANCEL"))));
				return;
			}
		} catch (JsonParseException e1) {
			logger.error("上传文件异常：", e1);
		} catch (JsonMappingException e1) {
			logger.error("上传文件异常：", e1);
		} catch (IOException e1) {
			logger.error("上传文件异常：", e1);
		}

	}

	// 处理二进制内容
	private void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
		if (filemodel != null) {
			try {
				if (saveFileFromBytes(message.getPayload(), filemodel.getFileSrc())) {
					session.sendMessage(
							new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("OK"))));
				} else {
					session.sendMessage(
							new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("FALSE"))));
				}
			} catch (IOException e) {
				logger.error("上传文件异常：", e);
			}
		}
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		if (session.isOpen()) {
			session.close();
		}
		UploadServiceWebSocketHandler.getCurrentUsers().remove(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
		UploadServiceWebSocketHandler.getCurrentUsers().remove(session);
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}

	public void sendMessageToUsers(Message message) {
		for (WebSocketSession user : UploadServiceWebSocketHandler.getCurrentUsers()) {
			try {
				if (user.isOpen()) {
					user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
				}
			} catch (IOException e) {
				// e.printStackTrace();
			}
		}
	}

	public void sendMessageToUser(String userId, Message message) {
		for (WebSocketSession user : UploadServiceWebSocketHandler.getCurrentUsers()) {
			User iterator = (User) user.getAttributes().get("user");
			if (iterator != null) {
				if (iterator.getUserId().equals(userId)) {
					try {
						if (user.isOpen()) {
							user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * 将二进制byte[]数组写入文件中
	 * 
	 * @param byteBuffer
	 *            byte[]数组
	 * @param outputFile
	 *            文件位置
	 * @return 成功: true 失败: false
	 */
	private static boolean saveFileFromBytes(ByteBuffer byteBuffer, String outputFile) {
		FileOutputStream fstream = null;
		File file = null;
		try {
			// System.out.print(outputFile);
			file = new File(outputFile);
			if (!file.exists())
				file.createNewFile();
			fstream = new FileOutputStream(file, true);
			fstream.write(byteBuffer.array());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (fstream != null) {
				try {
					fstream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return true;
	}

	private static boolean deleteFile(String outputFile) {
		File file = null;
		try {
			file = new File(outputFile);
			if (file.exists())
				file.delete();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
