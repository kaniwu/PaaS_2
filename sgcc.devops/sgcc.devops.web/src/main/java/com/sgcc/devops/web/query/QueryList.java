package com.sgcc.devops.web.query;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.model.ComponentModel;
import com.sgcc.devops.common.model.ComponentTypeModel;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.EnvironmentModel;
import com.sgcc.devops.common.model.HostComponentModel;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.ImageModel;
import com.sgcc.devops.common.model.LibraryModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.ParameterModel;
import com.sgcc.devops.common.model.RegIdImageTypeModel;
import com.sgcc.devops.common.model.RegistryLbModel;
import com.sgcc.devops.common.model.RegistryModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Authority;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ClusterWithIPAndUser;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Library;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.Role;
import com.sgcc.devops.dao.entity.RoleAction;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.AuthorityService;
import com.sgcc.devops.service.BusinessService;
import com.sgcc.devops.service.BusinessSysService;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ComponentTypeService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.EnvironmentService;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.LibraryService;
import com.sgcc.devops.service.ParameterService;
import com.sgcc.devops.service.RackService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.service.RoleService;
import com.sgcc.devops.service.ServerRoomService;
import com.sgcc.devops.service.SystemAppService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.service.UserService;

/**
 * @author luogan 2015年8月17日 下午3:49:55
 */
@Component
public class QueryList {

	@Autowired
	private HostService hostService;
	@Autowired
	private ClusterService clusterService;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private ConportService conportService;
	@Autowired
	private RegistryService registryService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private ComponentService componentService;
	@Autowired
	private ComponentTypeService componetTypeService;
	@Autowired
	private BusinessService businessService;
	@Autowired
	private BusinessSysService businessSysService;
	@Autowired
	private SystemAppService systemAppService;
	@Resource
	private UserService userService;
	@Resource
	private RoleService roleService;
	@Resource
	private AuthorityService authorityService;
	@Resource
	private ServerRoomService serverRoomService;
	@Resource
	private LibraryService libraryService;
	@Resource
	private RackService rackService;
	@Resource
	private ParameterService parameterService;
	@Resource
	private HostComponentService hostComponentService;
	@Resource
	private EnvironmentService environmentService;
	
	private static Logger logger = Logger.getLogger(QueryList.class);

	public GridBean queryHostList(User user,PagerModel pagerModel, HostModel hostModel) {
		return hostService.getOnePageHostList(user,pagerModel, hostModel);
	}

	public Cluster queryCluster(Host host) {
		return clusterService.getClusterByHost(host);
	}

	public GridBean queryClusterList(User user,PagerModel pagerModel,
			ClusterWithIPAndUser clusterWithIPAndUser) {
		return clusterService.getOnePageClusterList(user, pagerModel, clusterWithIPAndUser);
	}

	public JSONArray queryAllClusterList(User user, ClusterWithIPAndUser clusterWithIPAndUser) {
		return (JSONArray) clusterService.getAllClusterList(user, clusterWithIPAndUser);
	}

	public List<Host> queryAllHostList(String userId, HostModel hostModel) {
		return hostService.listHostByTypeAndCluster(hostModel.getHostType());
	}

	public JSONArray queryAllClusterHostList(String userId, HostModel hostModel) {
		JSONArray jaArray = new JSONArray();
		List<Host> hosts = hostService.listHostByClusterId(hostModel.getClusterId());
		for(Host host:hosts){
			JSONObject jo = new JSONObject();
			jo = JSONUtil.parseObjectToJsonObject(host);
			try {
				List<Container> containers = containerService.listContainersByHostId(host.getHostId());
				jo.put("conCount", containers==null?0:containers.size());
			} catch (Exception e) {
				logger.error("Get container infos failed: " + e.getMessage());
				return null;
			}
			jaArray.add(jo);
		}
		return jaArray;
	}

	public GridBean queryImageList(User user,PagerModel pagerModel, ImageModel imageModel) {
		return imageService.pagination(user,pagerModel, imageModel);
	}

	public GridBean queryImageList(String userId, int pageNum, int pageSize, String appId) {
		return imageService.getImageListByappId(userId, pageNum, pageSize, appId);
	}

//	public JSONArray queryClusterMasterListxx() {
//		return (JSONArray) clusterService.getOnePageClusterMasterList();
//	}

	public JSONArray listConPortByConId(String conId) {
		try {
			return JSONUtil.parseObjectToJsonArray(conportService.listConPorts(conId));
		} catch (Exception e) {
			logger.error("Get container port infos failed: " + e.getMessage() );
			return null;
		}
	}

	/**
	 * @author langzi
	 * @param model
	 * @return
	 * @version 1.0 2015年8月21日
	 */
	public GridBean listOnePageContainer(User user, PagerModel pagerModel, Container model) {
		try {
			return containerService.listContainerByType(user, pagerModel, model);
		} catch (Exception e) {
			logger.error("select one page containers failed: "+e.getMessage());
			return null;
		}
	}

	/**
	 * @author langzi
	 * @return
	 * @version 1.0 2015年9月11日
	 */
	public JSONArray listContainers() {
		try {
			return containerService.listAllContainersJsonArray();
		} catch (Exception e) {
			logger.error("get listcontainrt jsonarray failed: "+e.getMessage());
			return null;
		}
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 10:24
	 * @description 将原来返回字符串的方法修改为GridBean的方式
	 */
	public GridBean registryList(String userId, int pagenumber, int pagesize, RegistryModel registryModel) {
		try {
			return registryService.getOnePageRegistrys(userId, pagenumber, pagesize, registryModel);
		} catch (Exception e) {
			logger.error("get registry fail: " + e.getMessage());
		}
		return null;
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 10:24
	 * @description 将原来返回字符串的方法修改为GridBean的方式(增加IP显示替换功能)
	 */
	public GridBean registryListWithIP(String userId,PagerModel pagerModel,RegistryLbModel RegistryLbModel) {
		try {
			return registryService.getOnePageRegistrysWithIP(userId,pagerModel, RegistryLbModel);
		} catch (Exception e) {
			logger.error("get registryListWithIP failed: " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * @author yueyong
	 */
//	public GridBean registryLbList(String userId,PagerModel pagerModel,RegistryLbWithIPModel registryLbWithIPModel) {
//		try {
//			return registryService.getOnePageRegistryLbs(userId,pagerModel, registryLbWithIPModel);
//		} catch (Exception e) {
//			logger.error("get registryListWithIP failed: " + e.getMessage());
//		}
//		return null;
//	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月10日 10:24
	 * @description 增加了对于仓库下包含镜像所有
	 */
	public GridBean registrySlaveImages(String userId, PagerModel pagerModel,
			RegIdImageTypeModel regIdImageTypeModel, String registryid, byte imagestatus) {
		try {
			return registryService.getOnePageRegistrysSlaveImages(userId, pagerModel, regIdImageTypeModel,
					registryid, imagestatus);
		} catch (Exception e) {
			logger.error("select registry slave images failed: " + e.getMessage());
		}
		return null;
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年8月27日
	 */
	public JSONObject getRegistry(Registry record) {
		try {
			return registryService.getRegistry(record);
		} catch (Exception e) {
			logger.error("get registry json failed: " + e.getMessage());
		}
		return null;
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年8月27日
	 * @description 获取仓库类型的节点
	 */
	public JSONArray getRegistryMster() {
		try {
			return registryService.getRegistryMster();
		} catch (Exception e) {
			logger.error("get registry master host failed: " + e.getMessage());
		}
		return null;
	}

	/**
	 * @author langzi
	 * @param type
	 * @return
	 * @version 1.0 2015年9月11日
	 */
//	public List<Host> listHostByTypexx(int type) {
//		return hostService.listHostByType(type);
//	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年9月16日
	 */
	public GridBean systemAllList(PagerModel pagerModel,User user,String businessId,String systemName,Boolean byAuth,String envId) {
		try {
			boolean isAllAuth = true;
			if(byAuth){
				isAllAuth = false;
				//查询考虑权限 （只有超级管理员 和 业务管理员可以操作所有系统  其他用户 只有设置了物理系统的管理员才可操作对应）
				List<Role> roles = roleService.getRolesByUserId(user.getUserId());
				if(roles!=null && roles.size()>0)
					for(Role role : roles){
						if(role.getRoleId().equals("1") || role.getRoleId().equals("3")){
							isAllAuth = true;
							break;
						}
					}
			}
			return systemService.listOnePageSystems(pagerModel,user,businessId,systemName,isAllAuth,envId);
		} catch (Exception e) {
			logger.error("select all system failed: " + e);
			return null;
		}
	}

	public GridBean queryComponentTypeList(String userId, int pageNum, int pageSize,
			ComponentTypeModel componentTypeModel) {
		try {
			return componetTypeService.getOnePageComponentTypeList(userId, pageNum, pageSize, componentTypeModel);
		} catch (Exception e) {
			logger.error("select componenttype list failed: " + e.getMessage());
			return null;
		}
	}

	public GridBean queryComponentList(String userId,PagerModel pagerModel, ComponentModel componentModel) {
		try {
			return componentService.getOnePageComponentList(userId,pagerModel, componentModel);
		} catch (Exception e) {
			logger.error("get component list failed: " + e.getMessage());
			return null;
		}
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年10月12日
	 */
	public JSONArray freeHostList(User user) {
		return hostService.freeHostList(user);
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月13日
	 */
	public GridBean queryImageListByImageType(String userId, int page, int rows,int imageType,String clusterId) {
		try {
			return imageService.getImageListByImageType(userId, page, rows,imageType,clusterId);
		} catch (Exception e) {
			logger.error("select one type image list fail: " + e.getMessage());
			return null;
		}
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月15日
	 */
	public GridBean systemDeployInfo(String userId,PagerModel pagerModel, DeployModel deployModel,
			String systemId) {
		try {
			return systemService.getOnePageSystemDeployInfo(userId,pagerModel, deployModel, systemId);
		} catch (Exception e) {
			logger.error("select system deploy info failed: " + e);
		}
		return null;
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return GridBean
	 * @version 1.0 2015年10月15日
	 */
	public GridBean conlistByImage(String userId, int pagenumber, int pagesize, String imageId) {
		try {
			return containerService.selectContainersByImageId(userId, pagenumber, pagesize, imageId);
		} catch (Exception e) {
			logger.error("select container list by image fail: " + e.getMessage());
		}
		return null;
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return Object
	 * @version 1.0 2015年10月22日
	 */
	public GridBean listContainersByHostId(String hostId) {
		try {
			GridBean gridBean = containerService.gridBeanContainersByHostId(hostId);
			return gridBean;

		} catch (Exception e) {
			logger.error("get host container list failed: " + e.getMessage());
			return null;
		}
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年10月26日
	 */
	public GridBean listAllOfBusiness(User user,PagerModel pagerModel,String businessName) {
		try {
			GridBean bean = businessService.listAllOfBusiness(user,pagerModel,businessName);
			return bean;
		} catch (Exception e) {
			logger.error("get bussiness list failed: " + e.getMessage());
			return null;
		}
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年10月26日
	 */
	public JSONArray listAllByBusinessId(String businessId, User user) {
		try {
			
			return businessSysService.getSysByBusinessId(user, businessId,"");
		} catch (Exception e) {
			logger.error("get business'system list fail: " + e.getMessage());
			return new JSONArray();
		}
	}

	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2015年12月15日
	*/
	public JSONArray hostList(User user) {
		Host host = new Host();
		host.setHostEnv(user.getUserCompany());
		List<Host> hosts = hostService.selectAllHost(host);
		if(null!=hosts){
			return JSONUtil.parseObjectToJsonArray(hosts);
		}else{
			return null;
		}
		
	}
	public JSONArray systemAppsList(String systemId) {
		try {
			return systemAppService.getSystemApps(systemId);
		} catch (Exception e) {
			logger.error("get SystemApps fail: " + e.getMessage());
		}
		return null;
	}
	/**
	 * 用户列表分页
	 * 
	 * @param userId
	 * @param name
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public GridBean userList(String userId, int pageNum, int pageSize, User user) {
		try {
			return userService.list(userId, pageNum, pageSize, user);
		} catch (Exception e) {
			logger.error("get user list falied!", e);
			return null;
		}
	}

	/**
	* TODO
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年3月11日
	*/
	public GridBean roleList(String userId, int pageNum, int pageSize, Role role) {
		try {
			return roleService.list(userId, pageNum, pageSize, role);
		} catch (Exception e) {
			logger.error("get page role list falied!", e);
			return null;
		}
	}
	public JSONArray queryAllRoleList(String userId, Role role) {
		try {
			return roleService.getAllRoleList(userId, role);
		} catch (Exception e) {
			logger.error("get all role list falied!", e);
			return null;
		}
	}

	public String getRoleAuthList(String userId, RoleAction roleAction) {
		try {
			List<RoleAction> list = roleService.getRoleAuthList(userId, roleAction);
			if(null==list){
				return "[]";
			}else{
				return JSONUtil.parseObjectToJsonArray(list).toString();
			}
		} catch (Exception e) {
			logger.error("get role auth list falied!", e);
			return "[]";
		}
	}

	public JSONArray queryAllRoleListByUserId(String userId) {
		try {
			List<Role> list = roleService.getAllRoleListByUserId(userId);
			return JSONUtil.parseObjectToJsonArray(list);
		} catch (Exception e) {
			logger.error("get all role list by userid[" + userId + "] falied!", e);
			return null;
		}
	}
	/**
	 * 参数列表分页
	 * 
	 * @param userId
	 * @param name
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public GridBean authorityList(String userId, int pageNum, int pageSize, Authority authority) {
		try {
			return authorityService.list(userId, pageNum, pageSize, authority);
		} catch (Exception e) {
			logger.error("get page auth list falied!", e);
			return null;
		}
	}
	public JSONArray queryAllList(String userId, Authority authority) {
		try {
			return JSONUtil.parseObjectToJsonArray(authorityService.getAllAuthList(userId, authority));
		} catch (Exception e) {
			logger.error("get all authority list fslied!", e);
			return null;
		}
	}
	/**
	 * 通过权限的父id查找它下面的所有子节点
	 * 
	 * @param userid
	 *            [用户id] 暂时没有用到
	 * @param id
	 *            [父节点id]
	 * @return
	 */
	public List<Authority> queryListByActionParentId(String userid, String id) {
		try {
			return authorityService.ListByActionParentId(id);
		} catch (Exception e) {
			logger.error("ge auth list by parentId[" + id + "] falied!", e);
			return null;
		}
	}
	/**
	 * 获取该角色下父权限id为authId的所有权限
	 * 
	 * @param roleId
	 * @param authId
	 * @return
	 */
	public List<Authority> getAuthListByRoleId(String roleId, String authId) {
		try {
			return authorityService.getAuthListByRoleId(roleId, authId);
		} catch (Exception e) {
			logger.error("get auth list by roleid[" + roleId + "] ,authParentid[" + authId + "] failed!", e);
			return null;
		}
	}
	/**
	 * 
	 * @author liyp
	 * @return GridBean
	 * @version 1.0 2016年3月10日
	 */
	public GridBean sysAppPackageList(PagerModel pagerModel,String systemId) {
		try {
			return systemService.getAppPackage(pagerModel, systemId);
		} catch (Exception e) {
			logger.error("select system deploy info failed: " + e);
		}
		return null;
	}
	/**
	* TODO机房
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年3月16日
	*/
	public GridBean serverRoomList(PagerModel pagerModel) {
		
		return serverRoomService.serverRoomList(pagerModel);
	}
	/**
	 * 参数列表分页
	 * 
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public GridBean queryLibraryList(PagerModel pagerModel, LibraryModel libraryModel) {
		try {
			Library library = new Library();
			library.setLibraryName(libraryModel.getLibraryName());
			return libraryService.listLibrary(pagerModel, library);
		} catch (Exception e) {
			logger.error("get page Library list falied!", e);
			return null;
		}
	}


	public GridBean rackList(PagerModel pagerModel, String serverRoomId) {
		return rackService.rackList(pagerModel,serverRoomId);
	}

	public GridBean getSvnList() {
		try {
			return componentService.getSvnList();
		} catch (Exception e) {
			logger.error("get Svn  List failed: " + e.getMessage());
			return null;
		}
	}
 
	public GridBean queryParaList(PagerModel pagerModel,ParameterModel parameterModel) {
		return parameterService.getSearchParaList(pagerModel,parameterModel);
	}

    public GridBean hostComponentList(PagerModel pagerModel) {
		
		return hostComponentService.hostComponentListAll(pagerModel);
	}
    
    public GridBean hostComponentList(PagerModel pagerModel, HostComponentModel hostComponentModel) {
		return hostComponentService.hostComponentList(pagerModel,hostComponentModel);
	}
    
    public GridBean queryEnvList(PagerModel pagerModel, EnvironmentModel environmentModel) {
		return environmentService.environmentList(pagerModel,environmentModel);
	}

	public JSONArray environmentsList(User user) {
		try {
			Environment environment = new Environment();
			environment.seteId(user.getUserCompany());
			List<Environment> environments = environmentService.environmentsList(environment);
			JSONArray ja = JSONUtil.parseObjectToJsonArray(environments);
			return ja;
		} catch (Exception e) {
			logger.error("get Environments fail: " + e.getMessage());
		}
		return null;
	}

	public JSONArray grateClusterHost(User user,String clusterId) {
		return hostService.grateClusterHost(user,clusterId);
	}
}
