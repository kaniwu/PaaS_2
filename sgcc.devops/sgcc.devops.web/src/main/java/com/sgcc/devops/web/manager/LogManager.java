package com.sgcc.devops.web.manager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.IpCommonUtils;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.image.Encrypt;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**  
 * date：2017年5月5日 
 * project name：sgcc.devops.web
 * @author  zhaoy
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogManager.java
 * description：  日志处理类
 */
@Repository
public class LogManager {

	@Autowired
	private ContainerService containerService;
	@Autowired
	private HostService hostService;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private static Logger logger = Logger.getLogger(ContainerManager.class);
	
	
	
	//获取日志的最后N行，并将日志顺序翻转
	public static List<String> getLastNLine(File file, Long numRead){
		//返回内容
		List<String> result = new ArrayList<String>();
		//行数统计
		long count = 0;
		//排除不可读的状态
		if(!file.exists()||file.isDirectory()||!file.canRead()){
				return null;
		}
		//使用随机读取
		RandomAccessFile fileRead = null;
		try{
			fileRead = new RandomAccessFile(file,"r");
			//获取文件长度
			long length = fileRead.length();
			if(length == 0L){
				return result;
			}else{
				//初始化游标
				long pos = length-1;
				while(pos>0){
					pos--;
					fileRead.seek(pos);
					if(fileRead.readByte()=='\n'){
						String line = fileRead.readLine();
						result.add(line);
						//System.out.println(line);
						count++;
						if(count==numRead){
							break;
						}
					}
				}
				if (pos == 0)
                {
                    fileRead.seek(0);
                    result.add(fileRead.readLine());
                }
			}
				
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			if(fileRead!=null){
				try{
					fileRead.close();
				}catch(Exception e){
					
				}
			}
		}
		List<String> invokeResult = new ArrayList<String>();
		for(int i =0;i<result.size();i++){
			invokeResult.add(result.get(result.size()-1-i));
		}
		return invokeResult;
	}
	
	
	//获取最近10行日志。
	public JSONArray getLast10LineLog(String logPath) throws IOException{
		Long lineN = 10L;
		final File logFile = new File(logPath);
		long fileSize = logFile.length();
		List<String> ls = getLastNLine(logFile,lineN);
		JSONObject joLog = new JSONObject();
		joLog.put("content", ls);
		JSONObject joFSize = new JSONObject();
		joFSize.put("lastFileSize", fileSize);
		JSONArray ja = new JSONArray();
		ja.add(joLog);
		ja.add(joFSize);
		return ja;
	}
	//获取实时新添加的日志信息
	public JSONArray getLatestLog(String logPath,long lastFileSize) throws FileNotFoundException{
		
		List<String> ls =new ArrayList<String>();
		//返回最新文件大小
		long fileSize = 0;
		@SuppressWarnings("resource")
		final RandomAccessFile randomFile = new RandomAccessFile(logPath,"r");
		try {
			long currentFileSize = randomFile.length();
			randomFile.seek(lastFileSize);
			String tmp = "";
			while((tmp = randomFile.readLine())!=null){
				ls.add(tmp);
			}
			fileSize = currentFileSize;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject joLog = new JSONObject();
		joLog.put("content", ls);
		JSONObject joFSize = new JSONObject();
		joFSize.put("lastFileSize", fileSize);
		JSONArray ja = new JSONArray();
		ja.add(joLog);
		ja.add(joFSize);
		return ja;
	}
	
	//挂载目录到远程Docker机器
	//1.检测是否存在目录，如果存在目录则不用挂载，如果没有目录则建立目录，并且挂载目录，。
	//2.若需要挂载目录，则建立本地目录hostMountDir
	//3.修改Docker主机的/etc/exports文件，将Docker的日志目录dockerMountDir写入exports文件。主机export是文件的权限
	//4.重启Docker机器的rpcbind和nfs服务
	//5.paas主机执行挂载操作
	public Result mountLogDir(String conId,String systemCode){
		//被挂载的Docker主机目录和挂载的Host主机目录
		String dockerMountDir = localConfig.getHostLogPath()+conId+"_"+systemCode;
		String hostMountDir = localConfig.getLocalTimeLogPath()+conId+"_"+systemCode;
		//1.是否存在目录,不存在则建立目录，并且赋权777
		File hostTimeLogDir = new File(hostMountDir);
		if(hostTimeLogDir.exists()){
			return new Result(true,"目录已经挂载！");
		}else{
			hostTimeLogDir.mkdirs();
			try{
				String changemod = "sudo chmod 777 "+hostMountDir;
				Process process = Runtime.getRuntime().exec(changemod);
				//System.out.println();
			}catch(IOException e){
				e.printStackTrace();
			}
		}
		//2.不存在目录，建立目录并登录远程Docker主机进行操作
		//获取Docker主机信息
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		Host host = hostService.getHost(container.getHostId());
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
		Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		boolean b = true;
		try{
			if(ssh.connect()){
				//3.4从远程docker容器主机上下载到本地目录
				//String cmd1 = "sudo echo '"+dockerMountDir+" "+IpCommonUtils.getIpAddress()+"(ro,sync,fsid=0)"+"'"+" >> /etc/exports";
				String cmd = "sudo echo '"+dockerMountDir+" "+InetAddress.getLocalHost().getHostAddress()+"(ro,sync,fsid=0)"+"'"+" >> /etc/exports;";
				
				cmd += "sudo systemctl restart rpcbind;sudo systemctl restart nfs;";
				cmd += "sudo chmod -R 777 "+dockerMountDir+";";
				//先挂载再修改目录的权限
				b= ssh.execute(cmd);
				//5.本地执行挂载命令：
				
				if(!b){
					logger.error("docker主机重启nfs服务失败！");
					return new Result(false, "docker主机重启nfs服务失败！");
				}
			}else{
				logger.error("登录容器宿主机失败。");
				return new Result(false, "登录容器宿主机失败。");
			}
		}catch(Exception e){
			logger.error("修改docker主机挂载信息失败："+e);
			return new Result(false, "修改docker主机挂载信息失败："+e);
		}finally{
			ssh.close();
		}
		String cmd4 = "sudo mount -t nfs "+host.getHostIp()+":"+dockerMountDir+" "+hostMountDir;
		try{
			Process process = Runtime.getRuntime().exec(cmd4);
			//System.out.println(cmd4);
		}catch(IOException e){
			e.printStackTrace();
		}
		return new Result(true, "挂载成功");
		
		//Result result = new Result(true,"挂载成功");
		//return result;
	}
	
}
