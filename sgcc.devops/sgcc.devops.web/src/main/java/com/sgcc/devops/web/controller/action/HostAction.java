package com.sgcc.devops.web.controller.action;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.entity.ContentType;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.VersionModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.HostManager;
import com.sgcc.devops.web.manager.LocationManager;
import com.sgcc.devops.web.query.QueryList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:47:59
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：HostAction.java
 * description：  主机操作访问控制类
 */
@Controller
@RequestMapping("host")
public class HostAction {

	@Resource
	private QueryList queryList;
	@Resource
	private HostManager hostManager;
	@Resource
	private LocationManager locationManager;
	/**
	* 单个新增
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月7日
	 */
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, HostModel hostModel) {

		User user = (User) request.getSession().getAttribute("user");
		
		
		JSONObject params = JSONUtil.parseObjectToJsonObject(hostModel);
		params.put("userId", user.getUserId());
		Result result = hostManager.createHost(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	/**
	* 批量新增
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月7日
	 */
	@RequestMapping(value = "/createGroup", method = { RequestMethod.POST })
	@ResponseBody
	public Result createGroup(HttpServletRequest request, HostModel hostModel) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.createGroup(hostModel,user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(HttpServletRequest request, HostModel hostModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(hostModel);
		params.put("userId", user.getUserId());
		Result result = hostManager.deleteHost(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, HostModel hostModel) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.updateHost(user.getUserId(), hostModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	@RequestMapping(value = "/checkSSH", method = { RequestMethod.GET })
	@ResponseBody
	public Result checkSSH(HttpServletRequest request, HostModel hostModel) {
		User user = (User) request.getSession().getAttribute("user");
		//Result result = hostManager.updateHost(user.getUserId(), hostModel);
		Result result = hostManager.checkSSH(user.getUserId(), hostModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}


	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean hostList(HttpServletRequest request,PagerModel pagerModel, HostModel hostModel) {
		User user = (User) request.getSession().getAttribute("user");
		return queryList.queryHostList(user,pagerModel, hostModel);
	}

	@RequestMapping(value = "/queryIP", method = { RequestMethod.GET })
	@ResponseBody
	public void queryAddress(HttpServletRequest request, HostModel hostModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject jo = new JSONObject();
		JSONArray ja = new JSONArray();
		JSONObject params = new JSONObject();
		params.put("hostIP", hostModel.getHostIp());
		params.put("userId", user.getUserId());
		ja.add(0, params);
		jo.put("Params", ja);
		hostManager.queryIP(hostModel);
	}

	@RequestMapping(value = "/addAll", method = { RequestMethod.GET })
	@ResponseBody
	public String addAll(HttpServletRequest request, HostModel hostModel) {

		User user = (User) request.getSession().getAttribute("user");
		hostModel.setHostType((byte) 1);
		List<Host> hostList = queryList.queryAllHostList(user.getUserId(), hostModel);
		return JSONUtil.parseObjectToJsonArray(hostList).toString();
		// return hostList;
	}

	@RequestMapping(value = "/removeAll", method = { RequestMethod.GET })
	@ResponseBody
	public String removeAll(HttpServletRequest request, HostModel hostModel) {

		User user = (User) request.getSession().getAttribute("user");
		hostModel.setHostType((byte) 1);
		JSONArray ja = queryList.queryAllClusterHostList(user.getUserId(), hostModel);
		
		return ja.toString();
	}
	@RequestMapping(value = "/hostList", method = { RequestMethod.GET })
	@ResponseBody
	public String hostList(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.hostList(user);
		return ja.toString();

	}
	
	@RequestMapping(value = "/freeHostList", method = { RequestMethod.GET })
	@ResponseBody
	public String freeHostList(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.freeHostList(user);
		return ja.toString();

	}
	@RequestMapping(value = "/grateClusterHost", method = { RequestMethod.GET })
	@ResponseBody
	public String grateClusterHost(HttpServletRequest request,String clusterId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.grateClusterHost(user,clusterId);
		return ja.toString();

	}

	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = hostManager.checkHostNameRepeat(id, name);

		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}

	@RequestMapping(value = "/checkIp", method = { RequestMethod.POST })
	@ResponseBody
	public String checkIp(HttpServletRequest request, @RequestParam(value = "id", required = false) String id) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = hostManager.checkHostIpRepeat(id);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	/**
	* TODO
	* 主机上运行的物理系统数量
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年2月23日
	 */
	@RequestMapping(value = "/getSystemNames", method = { RequestMethod.GET })
	@ResponseBody
	public String getSystemNames(HttpServletRequest request,@RequestParam(value = "hostId", required = true) String hostId) {
		JSONArray ja = hostManager.getSystemNames(hostId);
		return ja==null?"":ja.toString();
	}
	/**
	* TODO重测主机连接
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年2月23日
	 */
	@RequestMapping(value = "/reTest", method = { RequestMethod.GET })
	@ResponseBody
	public Result reTest(HttpServletRequest request,@RequestParam(value = "hostId", required = true) String hostId) {
		Result result = hostManager.reTest(hostId);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	/**
	* TODOdocker服务安装、启动、停止、检查
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年2月23日
	 */
	@RequestMapping(value = "/dockerDaemon", method = { RequestMethod.GET })
	@ResponseBody
	public Result dockerDaemon(HttpServletRequest request,
			@RequestParam(value = "hostId", required = true) String hostId,
			@RequestParam(value = "opration", required = true) String opration) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.dockerDaemon(hostId,opration,user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	* TODO
	* 某组件关联的主机信息
	* @author yueyong
	* @return String
	* @version 1.0
	* 2016年3月9日
	 */
	@RequestMapping(value = "/selectComponentHost", method = { RequestMethod.GET })
	@ResponseBody
	public String selectComponentHost(HttpServletRequest request,@RequestParam(value = "componentId", required = true) String componentId) {
		JSONArray ja = hostManager.selectComponentHost(componentId);
		return ja==null?"":ja.toString();
	}
	
	@RequestMapping(value = "/exportHost", method = { RequestMethod.POST, RequestMethod.GET })
	@ResponseBody
	public void exportHost(HttpServletResponse response,@RequestParam(value = "locationId", required = false) String locationId) {
		byte[] bytes = hostManager.exportHost(locationId);
        response.setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
        try {
			response.setHeader("Content-Disposition", "attachment;filename=" + new String("PaaS主机列表".getBytes("gb2312"),"ISO8859-1") + ".xls");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        response.setContentLength(bytes.length);
        ServletOutputStream sos =null;
        try {
        	sos = response.getOutputStream();
        	sos.write(bytes);
        	sos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(sos !=null){
				try {
					sos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	@RequestMapping(value = "/importHost", method = { RequestMethod.POST , RequestMethod.GET })
	@ResponseBody
	public Result importHost(HttpServletRequest request,@RequestParam("file") MultipartFile file,@RequestParam(value = "locationId", required = true) String locationId) {
		User user = (User) request.getSession().getAttribute("user");
		String userId = user.getUserId();
		InputStream  inputStream =null;
		Result result = new Result();
		try {
			  inputStream = file.getInputStream();
		} catch (IOException e) {
			result.setMessage("程序错误");
			return result;
		}
		result.setMessage(importHostExcel(inputStream,userId,locationId));
		return result;
	}
	@RequestMapping(value = "/importHostExcel", method = { RequestMethod.POST ,RequestMethod.GET })
	@ResponseBody
	public void importHostExcel(HttpServletRequest request,HttpServletResponse response) {

		byte[] bytes = hostManager.importHostExcel();
        response.setContentType(ContentType.APPLICATION_OCTET_STREAM.getMimeType());
        response.setHeader("Content-Disposition", "attachment;filename=" + KeyGenerator.uuid() + ".xls");
        response.setContentLength(bytes.length);
        ServletOutputStream sos =null;
        try {
        	sos = response.getOutputStream();
        	sos.write(bytes);
        	sos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(sos !=null){
				try {
					sos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	
	}

	public  String importHostExcel(InputStream inputStream,String userId,String locationId){
		int count = 0;
		int cr  = 0;
		int hasImport  = 0;
		try{
		POIFSFileSystem fs =   
				new POIFSFileSystem(inputStream);
				HSSFWorkbook wb = new HSSFWorkbook(fs);
				HSSFSheet sheet = wb.getSheetAt(0);   
				cr = sheet.getLastRowNum();
				HSSFRow row = sheet.getRow(0); 
				HSSFCell cell0 = row.getCell((short)0);
				HSSFCell cell1 = row.getCell((short)1);  
				HSSFCell cell2 = row.getCell((short)2);  
				HSSFCell cell3 = row.getCell((short)3);
				if(!(cell0.getStringCellValue().equals("IP")&&cell1.getStringCellValue().equals("名称")&&cell2.getStringCellValue().equals("用户名（默认root)")&&cell3.getStringCellValue().equals("密码"))){
					return "与模板文件不符合" ;
				}
				for (int i = 1; i <= cr; i++) {
					HostModel host = new HostModel();
					host.setLocationId(locationId);;
					HSSFRow rowdata = sheet.getRow(i);
					 rowdata.getCell((short)0).setCellType(HSSFCell.CELL_TYPE_STRING);
					 rowdata.getCell((short)1).setCellType(HSSFCell.CELL_TYPE_STRING);
					 rowdata.getCell((short)2).setCellType(HSSFCell.CELL_TYPE_STRING);
					 rowdata.getCell((short)3).setCellType(HSSFCell.CELL_TYPE_STRING);
					HSSFCell hostIp = rowdata.getCell((short)0);
					HSSFCell hostName = rowdata.getCell((short)1);   
					HSSFCell hostUser = rowdata.getCell((short)2);   
					HSSFCell hostPwd = rowdata.getCell((short)3); 
					//如果存在就跳过
					boolean isOk = true;
					isOk = hostManager.checkHostIpRepeat(hostIp.getStringCellValue());
					if(!isOk){
						hasImport++;
						break;
					}
					host.setHostIp(hostIp.getStringCellValue());
					host.setHostName(hostName.getStringCellValue());
					host.setHostUser(hostUser.getStringCellValue());
					host.setHostPwd(hostPwd.getStringCellValue());
					JSONObject params = JSONUtil.parseObjectToJsonObject(host);
					params.put("userId", userId);
					Result result = hostManager.createHost(params);
					if(result.isSuccess()){
						count++;
					}else {
						return "已经导入成功"+count+"个，"+hasImport+"个ip地址已经存在,执行ip为"+hostIp+"时出错,原因为"+result.getMessage();
					}
					
				}
		} catch (Exception e) {
			return "导入文件失败";
		}
		return "共导入"+cr+"个主机成功";
	}
	@RequestMapping(value = "/hostById", method = { RequestMethod.GET })
	@ResponseBody
	public HostModel hostById(@RequestParam(value = "Id", required = true) String hostId) {
		HostModel host = hostManager.getHost(hostId);
		return host;

	}
	
	/*安装升级软件*/	
	@RequestMapping(value = "/installSoftware", method = {RequestMethod.GET })
	@ResponseBody
	public Result installSoftware(HttpServletRequest request,String hostId,String componentId,String operationType) {
		Result result = hostManager.installSoftWare(hostId,componentId,operationType);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/*检查重名的问题*/
	@RequestMapping(value = "/checkTreeName", method = {RequestMethod.POST  })
	@ResponseBody
	public String checkTreeName(HttpServletRequest request,@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name){
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		JSONArray data = locationManager.treeList();
		if(data.toString().indexOf(name) != -1){
			isOk = false;
		}
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	@RequestMapping(value = "/getVersion", method = {RequestMethod.POST })
	@ResponseBody
	public VersionModel getVersion(HttpServletRequest request,VersionModel versionModel){
		return hostManager.getVersion(versionModel);
	}
	@RequestMapping(value = "/getDockersVersion", method = {RequestMethod.POST })
	@ResponseBody
	public List<VersionModel> getDockersVersion(HttpServletRequest request,@RequestParam(value = "hostIds", required = false) String[] hostIds){
		List<VersionModel> dockersVersion = new ArrayList<VersionModel>();
		for (String idString : hostIds) {
			String hostId = idString.split(":")[1];
			VersionModel versionModel = new VersionModel();
			versionModel.setType("docker");
			versionModel.setHostID(hostId);
			dockersVersion.add(hostManager.getVersion(versionModel));
		}
		return dockersVersion;
	}
}
