package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.PortModel;
import com.sgcc.devops.dao.entity.Port;
import com.sgcc.devops.service.PortService;

@RequestMapping("/port")
@Controller
public class PortAction {
	@Resource
	private PortService portService;
	
	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	private GridBean list(HttpServletRequest request,PagerModel pagerModel,String type){
		
		return portService.gridBean(pagerModel,type);
	}
	
	@RequestMapping(value = "/insert", method = { RequestMethod.POST })
	@ResponseBody
	private Result insert(HttpServletRequest request,PortModel model){
		Port port = new Port();
		BeanUtils.copyProperties(model, port);
		int i = portService.insertPort(port);
		if(i==0){
			request.setAttribute("logDetail", "新增端口【"+port.getPort()+"】失败");
			request.setAttribute("success", "Fail");
			return new Result(false, "新增端口【"+port.getPort()+"】失败");
		}else{
			request.setAttribute("logDetail", "新增端口【"+port.getPort()+"】成功");
			request.setAttribute("success", "Success");
			return new Result(true, "新增端口【"+port.getPort()+"】成功");
		}
	}
	
	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	private Result delete(HttpServletRequest request,String id,String port){
		int i = portService.deletePort(id);
		if(i==0){
			request.setAttribute("logDetail", "删除端口【"+port+"】失败");
			request.setAttribute("success", "Fail");
			return new Result(false, "删除端口【"+port+"】失败");
		}else{
			request.setAttribute("logDetail", "删除端口【"+port+"】成功");
			request.setAttribute("success", "Success");
			return new Result(true, "删除端口【"+port+"】成功");
		}
	}
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	private Result update(HttpServletRequest request,PortModel portModel){
		Port port = new Port();
		BeanUtils.copyProperties(portModel, port);
		int i = portService.update(port);
		if(i==0){
			request.setAttribute("logDetail", "修改端口【"+port.getPort()+"】失败");
			request.setAttribute("success", "Fail");
			return new Result(false, "修改端口【"+port.getPort()+"】失败");
		}else{
			request.setAttribute("logDetail", "修改端口【"+port.getPort()+"】成功");
			request.setAttribute("success", "Success");
			return new Result(true, "修改端口【"+port.getPort()+"】成功");
		}
	}
	@RequestMapping(value = "/selectByKey", method = { RequestMethod.POST })
	@ResponseBody
	private String selectByKey(HttpServletRequest request,String id){
		Port port = portService.selectByPrimaryKey(id);
		return JSONObject.fromObject(port).toString();
	}
	
	@RequestMapping(value = "/getFreePort", method = { RequestMethod.GET })
	@ResponseBody
	private GridBean getFreePort(HttpServletRequest request,PagerModel pagerModel,String type){
		
		return portService.getFreePort(pagerModel,type);
	}
}
