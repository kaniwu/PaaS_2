package com.sgcc.devops.web.deployTask;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.UserService;
import com.sgcc.devops.web.manager.SystemManager;

/**
 * 定时部署
 * date：2016年5月26日 下午4:10:52
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployTimerTask.java
 * description：
 */
@Repository
public class DeployTimerTask {
	@Autowired
	private DeployService deployService;
	@Autowired
	private JdbcConfig jdbcConfig;
	@Autowired
	private UserService userService;
	@Autowired
	private SystemManager systemManager;
	private static Logger logger = Logger.getLogger(DeployTimerTask.class);
	/**
	 * 弹性伸缩处理任务 cron表达式： 秒 分 时 日 月 周几 年
	 */
	@Scheduled(cron = "${deploy.timer:0/10 0 * * * ?}")
	public void timer() {
		try {
			List<Deploy> deploys = deployService.getUndeployedByTimerTask(jdbcConfig.getDialect());
			if(null==deploys||deploys.isEmpty()){
				logger.info("无定时发布版本");
				return;
			}
			for(Deploy deploy:deploys){
				User user = new User();
				user.setUserId(deploy.getCreator());
				user = userService.getUser(user);
				Deploy deployup = new Deploy();
				deployup.setDeployId(deploy.getDeployId());
				deployup.setRegularlyPublish((byte)0);
				deployService.updateDeploy(deployup);
				Result result = systemManager.rollBackImage(deploy.getClusterId(), deploy.getDeployId(), user);
				if(!result.isSuccess()){
					logger.error(deploy.getDeployVersion()+"定时发布失败："+result.getMessage());
				}else{
					logger.info(deploy.getDeployVersion()+"定时发布成功！");
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
