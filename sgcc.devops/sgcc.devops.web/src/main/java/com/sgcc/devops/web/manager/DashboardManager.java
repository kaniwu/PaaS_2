package com.sgcc.devops.web.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.ClusterLoadModel;
import com.sgcc.devops.common.model.MonitorModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.util.LoadCompute;

/**  
 * date：2016年2月4日 下午1:55:44
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DashboardManager.java
 * description：  首页图标数据处理类
 */
@org.springframework.stereotype.Component
public class DashboardManager {
	@Resource
	private MonitorService monitorService;
	@Resource
	private LoadCompute loadCompute;
	@Resource
	private ClusterService clusterService;
	@Resource
	private HostService hostService;
	@Resource
	private ContainerService containerService;
	@Resource
	private HostComponentService hostComponentService;
	@Resource
	private ImageService imageService;
	@Resource
	private RegistryService registryService;
	@Resource
	private ComponentService componentService;
	@Resource
	private SystemService systemService;

	/** 获取监控的host 负载top10 */
	public JSONArray getHostTop(String clusterName) {
		Cluster cluster = new Cluster();
		cluster.setClusterName(clusterName);
		cluster = clusterService.selectClusterByName(cluster);
		List<Host> hosts = hostService.listHostByClusterId(cluster.getClusterId());

		List<MonitorModel> models = getMonitorModel(hosts);
		// List<MonitorModel> models =
		// loadCompute.hostLoadCompute(hostMonitors);
		if (models.size() > 10) {
			models = models.subList(0, 10);
		}
		JSONArray ja = new JSONArray();
		for (MonitorModel model : models) {
			String hostId = model.getTargetId();
			Host host = new Host();
			host.setHostId(hostId);
			host = hostService.getHost(host);
			JSONObject jo = JSONUtil.parseObjectToJsonObject(model);
			jo.put("name", host.getHostName());
			ja.element(jo);
		}
		return ja;
	}

	/** 获取集群内的host监控数据 */
	public List<MonitorModel> getMonitorModel(List<Host> hosts) {
		List<MonitorModel> monitors = new ArrayList<>();
		for (Host host : hosts) {
			Monitor monitor = new Monitor();
			monitor.setTargetId(host.getHostId());
			monitor.setTargetType((byte) Type.TARGET_TYPE.HOST.ordinal());
			Monitor monitorResult = monitorService.selectByTypeAndId(monitor);
			MonitorModel model = null;
			if (monitorResult == null) {
				model = new MonitorModel(null, (byte) Type.TARGET_TYPE.HOST.ordinal(), host.getHostId(), 0d);
			} else {
				model = loadCompute.compute(monitorResult);
			}
			monitors.add(model);
		}
		Collections.sort(monitors);
		return monitors;
	}

	/** 获取集群的top10 */
	public JSONArray getClusterTop() {
		// 获取所有cluster
		List<Cluster> clusters = clusterService.getAllCluster();
		List<ClusterLoadModel> loadModels = new ArrayList<>();

		for (Cluster cluster : clusters) {
			// 获取每个集群总的负载
			List<Host> hosts = hostService.listHostByClusterId(cluster.getClusterId());
			// 获取cluster所有的监控数据
			List<MonitorModel> models = getMonitorModel(hosts);

			Double value = loadCompute.clusterLoadCompute(models);

			ClusterLoadModel loadModel = new ClusterLoadModel();
			loadModel.setClusterId(cluster.getClusterId());
			loadModel.setName(cluster.getClusterName());
			loadModel.setValue((double)Math.round(value*100)/100);
			loadModels.add(loadModel);
		}
		// 对集群负载model排序
		Collections.sort(loadModels);
		if (loadModels.size() > 10) {
			loadModels = loadModels.subList(0, 10);
		}
		JSONArray ja = JSONUtil.parseObjectToJsonArray(loadModels);

		return ja;
	}

	public JSONObject getIndexJson() {
		JSONObject resultObject = new JSONObject();

		// 主机组件
		int hostCompNum = hostComponentService.countHostComp();
		resultObject.put("hostCompNum", hostCompNum);
		// 物理系统
		int phySystemNum = systemService.selectSystemCount(new System());
		// int phySystemNum = physicalService.allPhysicalsCount();
		resultObject.put("phySystemNum", phySystemNum);
		// 主机
		int hostNum = hostService.countAllHostList(new Host());
		resultObject.put("hostNum", hostNum);
		// 容器
		int containerNum = containerService.selectContainerCount(new Container());
		resultObject.put("containerNum", containerNum);
		// 镜像
		int imageNum = imageService.selectImageCount(new Image());
		resultObject.put("imageNum", imageNum);

		// 负载
		int conNginx = getConNginx();
		int componentNginx = getComNginx();
		int nginx = conNginx + componentNginx;
		resultObject.put("lbNum", nginx);
		// 集群
		Integer clusterNum = clusterService.countAllClusterList();
		resultObject.put("clusterNum", clusterNum);
		// 仓库
		Integer registryNum = registryService.registryCount();
		resultObject.put("registryNum", registryNum);
		// 组件
		Integer componentNum = componentService.componentCount(new Component());
		resultObject.put("componentNum", componentNum);
		return resultObject;
	}

	// 获取主机数量
	public JSONObject getTopJson() {
		JSONObject resultObject = new JSONObject();

		// 集群数量
		Integer clusterNum = clusterService.countAllClusterList();
		resultObject.put("clusterInfo", clusterNum);

		// 仓库
		Integer registryNum = registryService.registryCount();
		resultObject.put("registryInfo", registryNum);
		// 主机组件
		int hostCompNum = hostComponentService.countHostComp();
		resultObject.put("hostCompNum", hostCompNum);

		// 物理系统
		Integer systemNum = systemService.selectSystemCount(null);
		resultObject.put("systemInfo", systemNum);

		return resultObject;
	}

	// 获取主机状态
	public JSONObject getHostInfo() {
		JSONObject hostInfo = new JSONObject();
		Host host = new Host();

		host.setHostType((byte) Type.HOST.DOCKER.ordinal());
		int docker = hostService.countAllHostList(host);
		hostInfo.put("docker", docker);

		host.setHostType((byte) Type.HOST.SWARM.ordinal());
		int swarm = hostService.countAllHostList(host);
		hostInfo.put("swarm", swarm);

		host.setHostType((byte) Type.HOST.REGISTRY.ordinal());
		int registry = hostService.countAllHostList(host);
		hostInfo.put("registry", registry);

		return hostInfo;
	}

	// 获取容器状态
	public JSONObject getContainerInfo() {
		JSONObject containerInfo = new JSONObject();
		Container container = new Container();
		// 开启
		container.setConPower((byte) 1);
		int running = containerService.selectContainerCount(container);
		containerInfo.put("running", running);

		// 关闭
		container.setConPower((byte) 0);
		int stop = containerService.selectContainerCount(container);
		containerInfo.put("stop", stop);
		return containerInfo;
	}

	// 获取镜像信息
	public JSONArray getImageInfo() {
		JSONArray imageInfo = new JSONArray();
		Image image = new Image();

		image.setImageType((byte) Type.IMAGE_TYPE.NGINX.ordinal());
		int nginxImage = imageService.selectImageCount(image);

		image.setImageType((byte) Type.IMAGE_TYPE.APP.ordinal());
		int appImage = imageService.selectImageCount(image);

		image.setImageType((byte) Type.IMAGE_TYPE.BASIC.ordinal());
		int basicImage = imageService.selectImageCount(image);

		JSONObject nginxImageObj = new JSONObject();
		nginxImageObj.put("value", nginxImage);
		nginxImageObj.put("name", "NGINX");
		imageInfo.add(nginxImageObj);

		JSONObject appImageObj = new JSONObject();
		appImageObj.put("value", appImage);
		appImageObj.put("name", "APP");
		imageInfo.add(appImageObj);

		JSONObject basicImageObj = new JSONObject();
		basicImageObj.put("value", basicImage);
		basicImageObj.put("name", "BASIC");
		imageInfo.add(basicImageObj);

		return imageInfo;
	}

	// 获取负载信息
	public JSONObject getNginxInfo() {
		JSONObject nginxInfo = new JSONObject();

		// 容器负载
		int conNginx = getConNginx();
		nginxInfo.put("con", conNginx);

		// 组件负载(物理负载)
		int componentNginx = getComNginx();
		nginxInfo.put("com", componentNginx);

		return nginxInfo;
	}

	// 获取容器负载
	public Integer getConNginx() {
		Container container = new Container();
		container.setConType(2);
		return containerService.selectContainerCount(container);
	}

	// 获取组件负载（物理负载）
	public Integer getComNginx() {
		Component component = new Component();
		component.setComponentType((byte) 1);
		return componentService.componentCount(component);
	}

}
