/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.intf.ContainerCore;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegImageService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.TomcatConstants;
import com.sgcc.devops.web.image.WeblogicContstants;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午2:01:46
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：VersionChangeManager.java
 * description：  物理系统版本切换业务流程处理类
 */
@org.springframework.stereotype.Component
public class VersionChangeManager {
	private static Logger logger = Logger.getLogger(SystemManager.class);

	@Autowired
	private ContainerCore containerCore;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private ClusterService clusterService;
	@Autowired
	private HostService hostService;
	@Autowired
	private ConportService conportService;
	@Autowired
	private DeployService deployService;
	@Autowired
	private ComponentService componentService;
	@Autowired
	private ImageService imageService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private LocalConfig localConfig;
	@Resource
	private RegImageService regImageService;
	@Resource
	private CompPortManager compPortManager;
	@Resource
	private ContainerManager containerManager;
	@Resource
	private PortService portService;
	@Resource
	private SystemManager systemManager;
	@Resource
	private MonitorManager monitorManager;
	@Resource
	private ComponentExpandService componentExpandService;
	/**
	 * 切换到此版本
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月19日
	 */
	public Result versionChange(String deployId, User user) {
		Deploy deploy = deployService.getDeploy(deployId);
		System system = null;
		if (deploy == null) {
			logger.error(deployId +" Deploy is null");
			return new Result(false, deployId+" Deploy is null");
		} else {
			system = systemService.getSystemById(deploy.getSystemId());
			Cluster cluster = new Cluster();
			cluster.setClusterId(deploy.getClusterId());
			cluster = clusterService.getCluster(cluster);
			Host host = new Host();
			host.setHostId(cluster.getMasteHostId());
			host = hostService.getHost(host);
			ContainerModel containerModel = new ContainerModel();

			Image image = imageService.selectByPrimaryKey(deploy.getAppImageId());
			containerModel.setImageName(image.getImageName() + ":" + image.getImageTag());
			containerModel.setConNumber("" + deploy.getInstanceCount());
			containerModel.setCreateModel("1");
			containerModel.setCpu(deploy.getCpu());
			containerModel.setMemery(deploy.getMemery());
			List<Container> containertmep = null;
			String oldDeployID = null;
			Deploy oldDeploy = null;
			//当前运行的版本
			if (null != system.getDeployId()&&!"".equals(system.getDeployId())) {
				oldDeployID = system.getDeployId();
				oldDeploy = deployService.getDeploy(system.getDeployId());
				if (null != deploy) {
					containertmep = containerService.selectContainersByImageId(oldDeploy.getAppImageId());
				}
			}
			String hostPwd = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
			// 创建容器
			Image baseImage = imageService.selectByPrimaryKey(deploy.getBaseImageId());
			String logPath=null;
			if (baseImage.getImageName().toLowerCase().contains("tomcat")) {
				logPath = TomcatConstants.LOGS_PATH;
			} else if (baseImage.getImageName().toLowerCase().contains("weblogic")) {
				logPath = WeblogicContstants.LOGS_PATH;
			}
			containerModel.setLogPath(logPath);
			containerModel.setSystemCode(system.getSystemCode());
			JSONArray containerArray = new JSONArray();
			for(int i=0;i<Integer.parseInt(containerModel.getConNumber());i++){
				//检查集群主机是否有剩余资源
				boolean b = monitorManager.clusterMonitor(cluster.getClusterId());
				if(!b){
					return new Result(false, "创建容器失败，集群【"+cluster.getClusterName()+"】资源使用已达上限！");
				}
				containerModel.setConId(KeyGenerator.uuid());
				containerModel.setCreateParams(systemManager.containerLocation(deploy.getLocationLeavel(),i,deploy.getLocationStragegy(),deploy.getClusterId()));
				containerModel.setParameter(deploy.getParameter());
				JSONObject jo = containerCore.createContainer(host.getHostIp(), host.getHostUser(), hostPwd,
							cluster.getClusterPort(), containerModel);
				containerArray.add(jo);
			}
			// 存库
			JSONObject jo = new JSONObject();
			jo.put("systemName", system.getSystemName());
			jo.put("deployVersion", deploy.getDeployVersion());
			jo.put("imageId", deploy.getAppImageId());
			jo.put("clusterHostIp", host.getHostIp());
			jo.put("clusterPort", cluster.getClusterPort());
			jo.put("conEnv", cluster.getClusterEnv());
			this.addContainer(containerArray, jo,deploy.getSystemId(), user);
			// 更新版本切换状态
			deploy.setVersionChange((byte) 2);
			deployService.updateDeploy(deploy);
			// 更新物理系统运行版本
			system.setSystemId(deploy.getSystemId());
			system.setDeployId(deployId);
			system.setSystemDeployStatus((byte) Status.SYSTEM.DEPLOY.ordinal());
			systemService.updateSystem(system);
			// 更新nginx
			try {
				// 更新负载
				Component component = new Component();
				component.setComponentId(deploy.getLoadBalanceId());
				component = componentService.getComponent(component);
				if(component==null){
					logger.error("系统【"+system.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
					return new Result(false, "系统【"+system.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
				}
				//单个nginx组件
				if(component.getComponentType()==1){
					Result result = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
					if(!result.isSuccess()){
						return result;
					}
				}
				//nginx组组件
				if(component.getComponentType()==6){
					Result result = compPortManager.modifyNginxGroup(component.getComponentId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
					if(!result.isSuccess()){
						return result;
					}
				}
				//f5组件
				if(component.getComponentType()==2){
					ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
					//是否自动配置
					if(componentExpand.getAutoConfigF5()==1){
						// F5测试
						F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
								Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
						boolean re = detector.normal();
						if(!re){
							return new Result(false, "F5服务器通信失败，无法自动配置！");
						}else{
							F5Model f5Model = new F5Model();
							f5Model.setHostIp(component.getComponentIp());
							f5Model.setUser(component.getComponentUserName());
							f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
							f5Model.setPort(deploy.getF5ExternalPort());
							StringBuffer members = new StringBuffer();
							if (containerArray.size() > 0) {
								// String conIds = "";
								for (int i = 0; i < containerArray.size(); i++) {
									JSONObject containerJo = containerArray.getJSONObject(i);
									try {
										JSONArray ports = (JSONArray) containerJo.get("ports");
										if (!ports.isEmpty()) {
											String hostIp = ((JSONObject) ports.get(0)).getString("ip");
											for (int j = 0; j < ports.size(); j++) {
												JSONObject port = (JSONObject) ports.get(j);
												String conPort = String.valueOf(port.getInt("publicPort"));
												members.append(hostIp+":"+conPort+";");
											}
										}
									} catch (Exception e) {
										logger.info("F5配置异常 "+e.getMessage());
									}

								}
							}
							if(StringUtils.isNotEmpty(members.toString())){
								f5Model.setMembers(members.toString());
								f5Model.setPoolName("sgcc_pool");
								Result result = compPortManager.addF5MembersConfig(f5Model);
								if(!result.isSuccess()){
									return result;
								}
							}
						}
					}
				}
			} catch (Exception e) {
				logger.info("负载更新失败: "+e.getMessage());
				return new Result(false, "更新系统【"+system.getSystemName()+deploy.getDeployVersion()+"】版本失败，负载更新失败:"+e);
			}
			//原版本的容器删除
			if (containertmep != null) {
				String[] containerIds = new String[containertmep.size()];
				for (int i = 0; i < containertmep.size(); i++) {
					containerIds[i] = String.valueOf(containertmep.get(i).getConId());
				}
				if (containerIds.length > 0) {
					JSONObject old = new JSONObject();
					old.put("containerId", containerIds);
					
					containerManager.stopContainer(old);
					containerManager.removeContainer(old);
				}
				try{
					if(oldDeploy!=null){
						//f5组件
						Component oldComponent = new Component();
						oldComponent.setComponentId(oldDeploy.getLoadBalanceId());
						oldComponent = componentService.getComponent(oldComponent);
						if(oldComponent.getComponentType()==2){
							ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
							//是否自动配置
							if(componentExpand.getAutoConfigF5()==1){
								// F5测试
								F5Detector detector = new F5Detector(oldComponent.getComponentIp(), oldComponent.getComponentUserName(),
										Encrypt.decrypt(oldComponent.getComponentPwd(), localConfig.getSecurityPath()));
								boolean re = detector.normal();
								if(!re){
									return new Result(false, "F5服务器通信失败，无法自动配置！");
								}else{
									F5Model f5Model = new F5Model();
									f5Model.setHostIp(oldComponent.getComponentIp());
									f5Model.setUser(oldComponent.getComponentUserName());
									f5Model.setPwd(Encrypt.decrypt(oldComponent.getComponentPwd(), localConfig.getSecurityPath()));
									f5Model.setPort(deploy.getF5ExternalPort());
									StringBuffer members = new StringBuffer();
									for(String containerid:containerIds){
										List<ConPort> conPorts = conportService.listConPorts(containerid);
										for(ConPort conPort:conPorts){
											members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
										}
									}
									if(StringUtils.isNotEmpty(members.toString())){
										f5Model.setMembers(members.toString());
										f5Model.setPoolName("sgcc_pool");
										Result result = compPortManager.delF5MembersConfig(f5Model);
										if(!result.isSuccess()){
											logger.error(result.getMessage());
											return result;
										}
									}
								}
							}
						}
					}
				}catch(Exception e){
					logger.error("更新F5配置失败："+e.getCause());
					return new Result(false, "更新F5配置失败："+e.getCause());
				}
			}
		}
		return new Result(true, "系统【"+system.getSystemName()+deploy.getDeployVersion()+"】版本成功");
	}

	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	private List<Container> addContainer(JSONArray containerArray, JSONObject jo,String systemId, User user) {
		List<Container> containers = new ArrayList<Container>();
		if (containerArray.size() > 0) {
			for (int i = 0; i < containerArray.size(); i++) {
				JSONObject containerJo = containerArray.getJSONObject(i);
				Container container = new Container();
				container.setConId(containerJo.getString("conId"));
				container.setConUuid(containerJo.getString("id"));
				container.setConName(containerJo.getString("names"));
				String systemName = jo.getString("systemName");
				String deployVersion = jo.getString("deployVersion");
				String conTempName = systemName + deployVersion + "-" + (i + 1);
				container.setConTempName(conTempName);
				if (!jo.containsKey("imageId")) {
					continue;
				}
				container.setConImgid(jo.getString("imageId"));
				if (containerJo.getString("status").contains("Up")) {
					container.setConPower((byte) Status.POWER.UP.ordinal());
				} else {
					container.setConPower((byte) Status.POWER.OFF.ordinal());
				}
				container.setConStatus((byte) Status.CONTAINER.UP.ordinal());
				container.setConStartCommand(containerJo.getString("command"));
				container.setClusterIp(jo.getString("clusterHostIp"));
				container.setClusterPort(jo.getString("clusterPort"));
				container.setConCreator(user.getUserId());
				TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
				container.setConCreatetime(new Date());
				container.setConType(Type.CON_TYPE.APP.ordinal());
				container.setSysId(systemId);
				container.setConEnv(jo.getString("conEnv"));
				try {
					String hostIp = "";
					JSONArray ports = (JSONArray) containerJo.get("ports");
					if (!ports.isEmpty()) {
						hostIp = ((JSONObject) ports.get(0)).getString("ip");
						container.setHostId(hostService.getHostByIp(hostIp).getHostId());
						containerService.addContaier(container);
						containers.add(container);
						for (int j = 0; j < ports.size(); j++) {
							JSONObject port = (JSONObject) ports.get(j);
							ConPort conPort = new ConPort();
							conPort.setContainerId(container.getConId());
							conPort.setConIp(port.getString("ip"));
							conPort.setPubPort(String.valueOf(port.getInt("publicPort")));
							conPort.setPriPort(String.valueOf(port.getInt("privatePort")));
							conportService.addConports(conPort);
						}
					} else {
						logger.warn("Container's port is empty!");
						containerService.addContaier(container);
						containers.add(container);
					}
				} catch (Exception e) {
					logger.error("Create container error: "+e);
				}

			}
			return containers;
		} else {
			return null;
		}
	}
	
}
