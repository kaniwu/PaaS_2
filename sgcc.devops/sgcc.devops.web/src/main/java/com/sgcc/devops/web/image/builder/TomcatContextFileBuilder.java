package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.material.TomcatContextMaterial;

/**
 * Tomcat 的context.xml文件生成器
 * 
 * @author dmw
 *
 */
@Repository("tomcatContextFileBuilder")
public class TomcatContextFileBuilder implements FileBuilder {

	private static Logger logger = Logger.getLogger(TomcatContextFileBuilder.class);

	private static String FILE_NAME = "context.vm";
	private TomcatContextMaterial material;
	private LocalConfig localConfig;

	private enum ParamPosition {
		JDBC_CONFIG, SESSION_SHARE, // 是否session共享
		SESSION_SHARE_PATH;// session共享的位置

	}

	public void init(TomcatContextMaterial material, LocalConfig localConfig) {
		this.material = material;
		this.localConfig = localConfig;
	}

	public TomcatContextMaterial getMaterial() {
		return material;
	}

	public void setMaterial(TomcatContextMaterial material) {
		this.material = material;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}

	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	@Override
	public File build() {
		logger.info("开始生成context文件");
		if (null == material) {
			logger.error("Context material is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, localConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();
		// 组合配置信息
		context.put(ParamPosition.JDBC_CONFIG.name(), material.getDataSources());
		context.put(ParamPosition.SESSION_SHARE.name(), material.isSharable());
		context.put(ParamPosition.SESSION_SHARE_PATH.name(), material.getSharepath());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get context template infos error: " + e.getMessage());
			return null;
		}
		// 生成新的配置文件
		String localPath = material.getDirectory();
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = localPath.replace("\\", "/");
		File file = new File(localPath + "/context.xml");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build context exception: " + e.getMessage());
				return null;
			}
		}
		FileWriter fw= null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			writer.close();
			logger.info("制作context文件成功");
			return file;
		} catch (IOException e) {
			logger.error("制作context.xml文件异常: " + e.getMessage());
			return null;
		}finally{
			if(null!=fw){
				try {
					fw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(null!=writer){
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
