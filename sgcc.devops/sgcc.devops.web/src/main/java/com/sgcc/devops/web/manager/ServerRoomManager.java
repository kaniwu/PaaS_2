/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Rack;
import com.sgcc.devops.dao.entity.ServerRoom;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.RackService;
import com.sgcc.devops.service.ServerRoomService;

/**  
 * date：2016年3月16日 上午9:31:47
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ServerRoomManager.java
 * description：  
 */
@Component
public class ServerRoomManager {
	private static Logger logger = Logger.getLogger(ServerRoomManager.class);
	@Resource
	private ServerRoomService serverRoomService;
	@Resource
	private RackService rackService;
	@Resource
	private HostService hostService;
	
	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年3月16日
	*/
	public JSONArray treeList() {
		JSONArray ja = new JSONArray();
		List<ServerRoom> rooms = serverRoomService.selectAll();
		for(ServerRoom room:rooms){
			JSONObject jo = new JSONObject();
			jo.put("id", "room_"+room.getId());
			jo.put("name", room.getServerRoomName());
			jo.put("pId", 0);
			jo.put("type", "room");
			jo.put("nodeId", room.getId());
			ja.add(jo);
			List<Rack> racks = rackService.rackListByServerRoom(room.getId());
			for(Rack rack:racks){
				JSONObject rackjo = new JSONObject();
				rackjo.put("id", "rack_"+rack.getId());
				rackjo.put("name", rack.getRackName());
				rackjo.put("pId", "room_"+room.getId());
				rackjo.put("type", "rack");
				rackjo.put("nodeId", rack.getId());
				ja.add(rackjo);
				Host temp = new Host();
				temp.setLocationId(rack.getId());
				List<Host> hosts = hostService.hostListByRack(temp);
				for(Host host:hosts){
					JSONObject hostjo = new JSONObject();
					hostjo.put("id", "host_"+host.getHostId());
					hostjo.put("name", host.getHostIp());
					hostjo.put("pId", "rack_"+rack.getId());
					hostjo.put("type", "host");
					hostjo.put("nodeId", host.getHostId());
					ja.add(hostjo);
				}
			}
			
		}
		JSONObject jo = new JSONObject();
		jo.put("id", 0);
		jo.put("name", "机房管理");
		jo.put("pId", null);
		ja.add(jo);
		return ja;
	}
	
	
	/**
	 * 删除机房，查询被删除机房是否有机架，若有机架存在则不允许被删除
	 * @author panjing
	 */
	public Result deleteServerRoom(JSONObject jo) {
		String serverRoomId = jo.getString("serverRoomId");
		// 查询要删除机房的信息
		ServerRoom serverRoom = new ServerRoom();
		serverRoom.setId(serverRoomId);
		serverRoom = serverRoomService.getServerRoom(serverRoom);
		//若查询出来的机房为null，则机房有可能已经被其他管理员删除
		if (null == serverRoom) {
			logger.info("删除机房失败：主键为【" + serverRoomId + "】的机房可能已经被其他管理员删除！");
			return new Result(false, "删除机房失败：主键为【" + serverRoomId + "】的机房可能已经被其他管理员删除！");
		}
		// 查询机房内的机架的情况，若该机房内存在机架则不允许删除，若该机房下不存在任何机架，则可进行删除操作
		List<Rack> listRacks = rackService.rackListByServerRoom(serverRoomId);
		if (null != listRacks && listRacks.size() != 0) {
			logger.info("机房删除失败：【" + serverRoom.getServerRoomName()+ "】下存在机架！");
			return new Result(false, "机房删除失败：【" + serverRoom.getServerRoomName()+ "】下存在机架！");
		}else{
			try {
				int result = serverRoomService.deleteByPrimaryKey(serverRoomId);
				if(result == 1){
					logger.info("【" +  serverRoom.getServerRoomName() + "】删除成功！");
					return new Result(true, "【" +  serverRoom.getServerRoomName() + "】删除成功！");
				}else{
					logger.info("【" +   serverRoom.getServerRoomName() + "】删除失败：数据库操作异常！");
					return new Result(false, "【" +   serverRoom.getServerRoomName() + "】删除失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("【" +   serverRoom.getServerRoomName() + "】删除失败：数据库操作异常！");
				return new Result(false, "【" +   serverRoom.getServerRoomName() + "】删除失败：数据库操作异常！");
			}
	}
}

	/**
	 * 删除机架，查询被删除机房是否有机架，若有机架存在则不允许被删除
	 * @author panjing
	 */
	public Result deleteRack(JSONObject jo) {
		String rackId = jo.getString("rackId");
		// 查询要删除机架的信息
		Rack rack = new Rack();
		rack.setId(rackId);
		rack = rackService.getRack(rack);
		//若查询出来的机架为null，则机架有可能已经被其他管理员删除
		if (null == rack) {
			logger.info("删除机架失败：主键为【" + rackId + "】的机架可能已经被其他管理员删除！");
			return new Result(false, "删除机架失败：主键为【" + rackId + "】的机架可能已经被其他管理员删除！");
		}
		// 查询机架上主机的情况，若该机架上有主机则不允许删除，若无主机则执行删除操作
		Host temp = new Host();
		temp.setLocationId(rackId);
		List<Host> listHosts = hostService.hostListByRack(temp);
		if (null != listHosts && listHosts.size() != 0) {
			logger.info("机架删除失败：【" + rack.getRackName()+ "】上存在主机！");
			return new Result(false, "机架删除失败：【" + rack.getRackName()+ "】上存在主机！");
		}else{
			try {
				int result =rackService.deleteByPrimaryKey(rackId);
				if(result == 1){
					logger.info("【" +  rack.getRackName() + "】删除成功！");
					return new Result(true, "【" +  rack.getRackName() + "】删除成功！");
				}else{
					logger.info("【" +  rack.getRackName() + "】删除失败：数据库操作异常！");
					return new Result(false, "【" + rack.getRackName() + "】删除失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("【" + rack.getRackName() + "】删除失败：数据库操作异常！");
				return new Result(false, "【" + rack.getRackName() + "】删除失败：数据库操作异常！");
			}
	}
}
	
	/**
	 * 新建机房
	 * @author panjing
	 */
	public Result createServerRoom(JSONObject jsonObject) {
		String serverRoomName = jsonObject.getString("serverRoomName");
		String serverRoomRemark = jsonObject.getString("serverRoomRemark");
		String creator = jsonObject.getString("userId");
		ServerRoom serverRoom =new ServerRoom();
		serverRoom.setServerRoomName(serverRoomName);
		serverRoom.setServerRoomRemark(serverRoomRemark);
		serverRoom.setCreator(creator);
		serverRoom.setId(KeyGenerator.uuid());
		if(serverRoomName == null || serverRoomRemark == null){
			logger.info("添加机房失败，机房名称或机房备注为空！");
			return new Result(false, "添加机房失败，机房名称和机房备注均不能为空！");
		}
		//判断机房名称是否重复
		boolean repeat = serverRoomService.ifRepeat(serverRoomName);
		if(repeat){
			logger.info("添加机房失败，机房名称不能重复！");
			return new Result(false, "添加机房失败，机房名称不能重复！");
		}else{
			try {
				int result =serverRoomService.insert(serverRoom);
				if(result == 1){
					logger.info("【" + serverRoomName + "】创建成功！");
					return new Result(true, "【" +  serverRoomName + "】创建成功！");
				}else{
					logger.info("【" +  serverRoomName + "】创建失败：数据库操作异常！");
					return new Result(false, "【" + serverRoomName + "】创建失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("【" + serverRoomName + "】创建失败：数据库操作异常！");
				return new Result(false, "【" + serverRoomName + "】创建失败：数据库操作异常！");
			}
			
		}
	}
	

	/**
	 * 新建机架
	 * @author panjing
	 */
	public Result createRack(JSONObject jsonObject) {
		String rackName = jsonObject.getString("rackName");
		String rackRemark = jsonObject.getString("rackRemark");
		String creator = jsonObject.getString("userId");
		//获取新增机架所在机房的ID
		String serverRoomId = jsonObject.getString("serverRoomId");
		Rack record = new Rack();
		record.setCreator(creator);
		record.setRackName(rackName);
		record.setRackRemark(rackRemark);
		record.setServerRoomId(serverRoomId);
		record.setId(KeyGenerator.uuid());
		if(rackName == null || rackRemark == null){
			logger.info("添加机架失败，机架名称或机架备注为空！");
			return new Result(false, "添加机架失败，机架名称和机架备注均不能为空！");
		}
		//判断机架名称是否重复
		boolean repeat = rackService.ifRackNameRepeat(record);
		if(repeat){
			logger.info("添加机架失败，机架名称不能重复！");
			return new Result(false, "添加机架失败，机架名称不能重复！");
		}else{
			try {
 				int result =rackService.insert(record);
				if(result == 1){
					logger.info("【" + rackName + "】创建成功！");
					return new Result(true, "【" +  rackName + "】创建成功！");
				}else{
					logger.info("【" +  rackName + "】创建失败：数据库操作异常！");
					return new Result(false, "【" + rackName + "】创建失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("【" + rackName + "】创建失败：数据库操作异常！");
				return new Result(false, "【" + rackName + "】创建失败：数据库操作异常！");
			}
		}
	}
	
	/**
	 * 编辑机房
	 * @author panjing
	 */
	public Result updateServerRoom(JSONObject jsonObject) {
		String serverRoomId = jsonObject.getString("serverRoomId");
		String serverRoomName = jsonObject.getString("serverRoomName");
		String serverRoomRemark = jsonObject.getString("serverRoomRemark");
		String creator = jsonObject.getString("userId");
		ServerRoom serverRoom =new ServerRoom();
		serverRoom.setCreator(creator);
		serverRoom.setId(serverRoomId);
		serverRoom.setServerRoomName(serverRoomName);
		serverRoom.setServerRoomRemark(serverRoomRemark);
		if(serverRoomName == null || serverRoomRemark == null){
			logger.info("编辑机房失败，机房名称或机房备注为空！");
			return new Result(false, "编辑机房失败，机房名称和机房备注均不能为空！");
		}
		//判断机房名称是否重复
		/*boolean repeat = serverRoomService.ifRepeat(serverRoomName);
		if(repeat){
			logger.info("编辑机房失败，机房名称不能重复！");
			return new Result(false, "编辑机房失败，机房名称不能重复！");
		}else{*/
			try {
				int result =serverRoomService.updateByPrimaryKeySelective(serverRoom);
				if(result == 1){
					logger.info("修改机房信息成功！");
					return new Result(true, "修改机房信息成功！");
				}else{
					logger.info("修改机房信息失败：数据库操作异常！");
					return new Result(false, "修改机房信息失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("修改机房信息失败：数据库操作异常！");
				return new Result(false, "修改机房信息失败：数据库操作异常！");
			}
//		}
	}
	
	/**
	 * 编辑机架
	 * @author panjing
	 */
	public Result updateRack(JSONObject jsonObject) {
		String rackId = jsonObject.getString("rackId");
		String rackName = jsonObject.getString("rackName");
		String rackRemark = jsonObject.getString("rackRemark");
		String creator = jsonObject.getString("userId");
//		Byte serverRoomId =Byte.parseByte(jsonObject.getString("serverRoomId"));
		String serverRoomId = jsonObject.getString("serverRoomId");
		Rack record = new Rack();
		record.setCreator(creator);
		record.setRackName(rackName);
		record.setRackRemark(rackRemark);
		record.setServerRoomId(serverRoomId);
		record.setId(rackId);
		if(rackName == null || rackRemark == null){
			logger.info("编辑机架失败，机架名称或机架备注为空！");
			return new Result(false, "编辑机架失败，机架名称和机架备注均不能为空！");
		}
		//判断机架名称是否重复
		/*boolean repeat = rackService.ifRackNameRepeat(rackName);
		if(repeat){
			logger.info("编辑机架失败，机架名称不能重复！");
			return new Result(false, "编辑机架失败，机架名称不能重复！");
		}else{*/
			try {
 				int result =rackService.updateByPrimaryKeySelective(record);
				if(result == 1){
					logger.info("修改机架信息成功！");
					return new Result(true, "修改机架信息成功！");
				}else{
					logger.info("修改机架信息失败：数据库操作异常！");
					return new Result(false, "修改机架信息失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("修改机架信息失败：数据库操作异常！");
				return new Result(false, "修改机架信息失败：数据库操作异常！");
			}
//		}
	}
}
