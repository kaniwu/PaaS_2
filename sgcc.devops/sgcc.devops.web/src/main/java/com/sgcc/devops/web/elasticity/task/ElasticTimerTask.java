package com.sgcc.devops.web.elasticity.task;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.service.ElasticStrategyService;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.elasticity.cache.ElasticCache;
import com.sgcc.devops.web.manager.ContainerManager;

/**
 * 弹性伸缩定时任务,默认30分钟执行一次，可以手动配置
 * 
 * @author dmw
 *
 */
@Repository
public class ElasticTimerTask {

	private static Logger logger = Logger.getLogger(ElasticTimerTask.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private MonitorService monitorService;
	@Autowired
	private ElasticStrategyService elasticStrategyService;
	@Autowired
	private ContainerManager containerManager;
	@Autowired
	private ElasticCache elasticCache;

	/**
	 * 弹性伸缩处理任务 cron表达式： 秒 分 时 日 月 周几 年
	 */
//	@Scheduled(cron = "${elastic.timer:0 0/5 * * * ?}")
//	public void elastic() {
//		List<System> systems = systemService.listDeployedSystem();// 获取所有已经部署的物理系统
//		if (null == systems || systems.isEmpty()) {
//			logger.debug("elastic task over!");
//			return;
//		}
//		ExecutorService executor = Executors.newFixedThreadPool(10);
//		CompletionService<Result> completionService = new ExecutorCompletionService<>(executor);
//		logger.debug("create elastic task....");
//		for (System system : systems) {
//			if(null == system.getSystemElsasticityStatus() || 1!= system.getSystemElsasticityStatus().intValue()){
//				continue;
//			}
//			List<Container> containers = this.systemService.listContainers(system.getSystemId());// 获取当前物理系统下所有容器的列表
//			ElasticStrategy strategy = this.elasticStrategyService.loadBySystem(system.getSystemId());// 获取物理系统的伸缩策略
//			if (null == strategy || null == strategy.getItems() || strategy.getItems().isEmpty()) {
//				break;
//			}
//			ElasticTask task = new ElasticTask(system.getSystemId(), containers, monitorService, containerManager,
//					strategy,elasticCache);
//			completionService.submit(task);
//		}
//		executor.shutdown();
//		logger.debug("elastic task over!");
//	}
}
