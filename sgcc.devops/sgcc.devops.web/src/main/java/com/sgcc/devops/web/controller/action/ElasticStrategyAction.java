package com.sgcc.devops.web.controller.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.ElasticItem;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ElasticStrategyManager;
/**  
 * date：2016年2月4日 下午1:47:48
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticStrategyAction.java
 * description：  弹性伸缩操作控制类
 */
@Controller
@RequestMapping("elasticStrategy")
public class ElasticStrategyAction {

	@Autowired
	private ElasticStrategyManager elasticStrategyManager;

	@RequestMapping("/create")
	@ResponseBody
	public Result setElasticStrategy(HttpServletRequest request, ElasticStrategy elasticStrategy) {
		User user = (User) request.getSession().getAttribute("user");
		List<ElasticItem> items = new ArrayList<>();

		elasticStrategy.setCreator(user.getUserId());
		// 策略条目信息
		// cpu
		ElasticItem cpu = new ElasticItem();
		String cpuExpand = request.getParameter("elasticity_cpu_extended");
		String cpuShrink = request.getParameter("elasticity_cpu_contraction");
		String cpuPriority = request.getParameter("elasticity_cpu_priority");

		cpu.setItemName("cpu");
		cpu.setExpandThreshold(new BigDecimal(cpuExpand));
		cpu.setShrinkThreshold(new BigDecimal(cpuShrink));
		cpu.setPriority(new Integer(cpuPriority));

		items.add(cpu);
		// 内存
		ElasticItem mem = new ElasticItem();
		String memExpand = request.getParameter("elasticity_memory_extended");
		String memShirnk = request.getParameter("elasticity_memory_contraction");
		String memPriority = request.getParameter("elasticity_memory_priority");

		mem.setItemName("mem");
		mem.setExpandThreshold(new BigDecimal(memExpand));
		mem.setShrinkThreshold(new BigDecimal(memShirnk));
		mem.setPriority(new Integer(memPriority));

		items.add(mem);
		// 网卡
		ElasticItem net = new ElasticItem();
		String netExpand = request.getParameter("elasticity_net_extended");
		String netShirnk = request.getParameter("elasticity_net_contraction");
		String netPriority = request.getParameter("elasticity_net_priority");
		net.setItemName("net");
		net.setExpandThreshold(new BigDecimal(netExpand));
		net.setShrinkThreshold(new BigDecimal(netShirnk));
		net.setPriority(new Integer(netPriority));

		items.add(net);
		// 策略状态
		String elasticityStatus = request.getParameter("elasticity_status");

		return elasticStrategyManager.create(elasticStrategy, items, elasticityStatus);

	}

	@RequestMapping(value = "/load", method = { RequestMethod.GET })
	@ResponseBody
	public JSONObject load(HttpServletRequest request, String systemId) {
		return elasticStrategyManager.load(systemId);

	}
}
