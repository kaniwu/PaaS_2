package com.sgcc.devops.web.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.web.monitor.ContainerMonitorTask;

/**
 * 容器监控调度任务
 * @author dmw
 *
 */
@Component
public class ContainerMonitorSchedule {

	@Autowired
	private ContainerMonitorTask containerMonitorTask;

	/**
	 * 定时插入监控数据
	 */
	// 秒 分 时 天 月 周 年
	@Scheduled(cron = "${monitor.containerdate:0/30 * * * * ?}")
	protected void executeInsertMonitor() {
		containerMonitorTask.containerMonitor();
	}
}
