package com.sgcc.devops.web.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.ComponentModel;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.model.RegHost;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.DNSDetector;
import com.sgcc.devops.core.detector.DbDetector;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.detector.NginxDetector;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.entity.DeployFile;
import com.sgcc.devops.dao.entity.Driver;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.DeployDatabaseService;
import com.sgcc.devops.service.DeployFileService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.DriverService;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.image.ConfigFile;
import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.TomcatConstants;
import com.sgcc.devops.web.image.WeblogicContstants;
import com.sgcc.devops.web.image.builder.TomcatImageBuilder;
import com.sgcc.devops.web.image.builder.WeblogicImageBuilder;
import com.sgcc.devops.web.image.material.ImageMaterial;

/**  
 * date：2016年2月4日 下午1:53:11
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentManager.java
 * description：  组件配置流程处理类
 */
@Repository
public class ComponentManager {
	@Resource
	private ComponentService componentService;
	@Resource
	private ComponentExpandService componentExpandService;
	@Autowired
	private DeployDatabaseService deployDatabaseService;
	@Autowired
	private DeployService deployService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private LocalConfig localConfig;
	@Resource
	private SystemManager systemManager;
	@Resource
	private ImageService imageService;
	@Resource
	private TomcatImageBuilder tomcatImageBuilder;
	@Resource
	private WeblogicImageBuilder weblogicImageBuilder;
	@Resource
	private DriverService driverService;
	@Resource
	private RegImageMapper regImageMapper;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private DeployFileService deployFileService;
	@Resource
	private HostCore hostCore;
	@Resource
	private HostComponentService hostComponentService;
	@Resource
	private HostComponentManager hostComponentManager;
	@Resource
	private CompPortManager compPortManager;
	@Resource
	private HostService hostService;
	private static Logger logger = Logger.getLogger(ComponentManager.class);

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result createComponent(String userId,ComponentModel componentModel) {
		componentModel.setComponentPwd(Encrypt.encrypt(componentModel.getComponentPwd(), localConfig.getSecurityPath()));
		List<ComponentHost> componentHostList = getCompHostList(componentModel.getComponentHostInfo());
		if(null == componentModel.getComponentType()){
			logger.info("Create component error");
			return new Result(false, "添加【"+componentModel.getComponentName()+"】组件错误,组件类型不存在");
		}
		//单个nginx组件
		if(componentModel.getComponentType()==1){
			
			String nginxIns=componentModel.getNginxIns();
			if(org.apache.commons.lang.StringUtils.isEmpty(nginxIns)){	
				return new Result(false, "组件不存在");
			}
			if(nginxIns.equals("0")){
					//不安装
			}else{
				Result result =hostComponentManager.installHostComponent("",componentModel.getComponentIp(),componentModel.getComponentUserName(),componentModel.getComponentPwd(),nginxIns);
				if(!result.isSuccess()){
					return result;
				}
			}
		}
		//一组nginx
		if(componentModel.getComponentType()==6){
			//是否自动配置
			if(componentModel.getComponentExpand().getAutoConfigF5()==1){
				Component component = new Component();
				component.setComponentId(componentModel.getComponentExpand().getF5CompId());
				component = componentService.getComponent(component);
				// F5测试
				F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
						Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
				boolean re = detector.normal();
				if(!re){
					return new Result(false, "F5服务器通信失败，无法自动配置！");
				}else{
					F5Model f5Model = new F5Model();
					f5Model.setHostIp(component.getComponentIp());
					f5Model.setUser(component.getComponentUserName());
					f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
					f5Model.setPort(componentModel.getComponentPort()==null?"":componentModel.getComponentPort().toString());
					String componentHostInfo = componentModel.getComponentHostInfo();
					String[] nginxHosts = componentHostInfo.split("|~~|");
					StringBuffer members = new StringBuffer();
					for(String nginxhost:nginxHosts){
						String[] nginxHost = nginxhost.split("===");
						if(nginxHost.length>5){
							String hostid = nginxHost[1];
							String nginxPort = nginxHost[3];
							Host host = hostService.getHost(hostid);
							if(host!=null){
								members.append(host.getHostIp()+":"+nginxPort+";");
							}
						}
					}
					if(org.apache.commons.lang.StringUtils.isNotEmpty(members.toString())){
						f5Model.setMembers(members.toString());
//						f5Model.setMonitorInterval(monitorInterval);
//						f5Model.setMonitorTimeout(monitorTimeout);
//						f5Model.setMonitorName("sgcc_monitor");
//						f5Model.setMonitorReceiveString("js.sgcc.com.cn");
//						f5Model.setMonitorTemplateName("http");
//						f5Model.setMonitorUri("/");
						f5Model.setPoolName("sgcc_pool");
//						f5Model.setVsIp("");
//						f5Model.setVsName(vsName);
//						f5Model.setVsPort(vsPort);
						Result result = compPortManager.addF5MembersConfig(f5Model);
						if(!result.isSuccess()){
							return result;
						}
					}
				}
			}
		}
		int result = componentService.createComponent(componentModel,componentHostList);
		if (result == 1) {
			logger.info("Create component success");
			return new Result(true, "添加组件【"+componentModel.getComponentName()+"】成功");
		} else {
			logger.error("Create component error");
			return new Result(false, "添加【"+componentModel.getComponentName()+"】组件错误");
		}

	}
	
	private List<ComponentHost> getCompHostList(String componentHostInfo){
		List<ComponentHost> list = new ArrayList<ComponentHost>();
		if(!StringUtils.isEmpty(componentHostInfo)){
		String[] componentHostInfos = componentHostInfo.split("\\|~~\\|");
			for (String dataInfo : componentHostInfos) {
				String[] datas = dataInfo.split("===");
				String componentHostId = datas.length>=1?datas[0]:null;
				String hostId = datas.length>=2?datas[1]:null;
				String nginxStatus = datas.length>=3?datas[2]:null;
				String nginxPort = datas.length>=4?datas[3]:null;
				String delFlag = datas.length>=5?datas[4]:null;
				ComponentHost cHost = new ComponentHost();
				cHost.setComponentHostId(componentHostId);
				cHost.setHostId(hostId);
				cHost.setNginxStatus(StringUtils.isEmpty(nginxStatus)?null:new Byte(nginxStatus));
				cHost.setNginxPort(StringUtils.isEmpty(nginxPort)?null:Integer.parseInt(nginxPort));
				cHost.setDelFlag(delFlag);
				list.add(cHost);
			}
		}
		return list;
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result deleteComponent(JSONObject jsonObject) {
		Component component = new Component();
		component.setComponentId(jsonObject.getString("componentId"));
		component = componentService.getComponent(component);
		if(null==component){
			return new Result(false, "删除组件失败，信息获取异常");
		}
		Result result = componentService.deleteComponent(component);
		if (result.isSuccess()) {
			logger.info("delete component 【"+jsonObject.getString("componentId")+"】 success");
			return new Result(true, "删除组件【"+component.getComponentName()+"】成功");
		} else {
			logger.info(result.getMessage());
			return result;
		}
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result deleteComponents(JSONObject jsonObject) {
		String arrayString = jsonObject.getString("ids");
		String[] jaArray = arrayString.split(",");
		Result result = null;
		for (String componentId : jaArray) {
			if (componentId == null || componentId.equals("")) {

			} else {
				Component component = new Component();
				component.setComponentId(componentId);
				component = componentService.getComponent(component);
				result = componentService.deleteComponent(component);
			}
		}
		if(result==null){
			return new Result(false, "删除操作异常！");
		}
		return result;
	}

	/**
	 * @author zhangxin
	 * @param jo
	 * @return
	 * @version 1.0 2015年9月18日
	 */
	public Result updateComponent(String userId,ComponentModel componentModel) {
		List<ComponentHost> componentHostList = getCompHostList(componentModel.getComponentHostInfo());
		int result = componentService.updateComponent(componentModel,componentHostList);
		
		if (result == 1) {
			logger.info("update component【"+componentModel.getComponentName()+"】 success");
			return new Result(true, "更新组件【"+componentModel.getComponentName()+"】 成功");
		} else {
			logger.info("update component【"+componentModel.getComponentName()+"】 failed");
			return new Result(false, "更新组件【"+componentModel.getComponentName()+"】 失败");
		}
	}

	public JSONArray getSystem(String componentId,String componentType) {
		List<DeployDatabase> deployDatabases = new ArrayList<DeployDatabase>();
		JSONArray params = new JSONArray();
		if(null !=componentType && componentType.equals("6") ){
			 deployDatabases = deployDatabaseService.selectByNginxId(componentId);
		}else{
		 deployDatabases = deployDatabaseService.selectByDatabaseId(componentId);
		}
		for (DeployDatabase deployDatabase : deployDatabases) {
			JSONObject param = new JSONObject();
			Deploy deploy = deployService.getDeploy(deployDatabase.getDeployId());
			if (deploy == null) {
				continue;
			}
			System system = systemService.loadById(deploy.getSystemId());
			param.put("deployId", deploy.getDeployId());
			param.put("deployVersion", deploy.getDeployVersion());
			param.put("systemName", system.getSystemName());
			params.element(param);
		}
		return params;
	}

	public Result connectTest(ComponentModel componentModel) {
		String componentId = componentModel.getComponentId();
		Component component = new Component();
		if(StringUtils.isNotEmpty(componentId)){
			component.setComponentId(componentId);
			component = componentService.getComponent(component);
			component.setComponentPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
		}else{
			component.setComponentIp(componentModel.getComponentIp());
			component.setComponentUserName(componentModel.getComponentUserName());
			component.setComponentPwd(componentModel.getComponentPwd());
			component.setComponentName(componentModel.getComponentName());
			component.setComponentType(componentModel.getComponentType());
			component.setComponentDriver(componentModel.getComponentDriver());
			component.setComponentJdbcurl(componentModel.getComponentIp());
			component.setKey(componentModel.getKey());
			component.setSecret(componentModel.getSecret());
			component.setDnsZone(componentModel.getDnsZone());
		}
		boolean result = false;
		if (component.getComponentType() == 1) {
			// nginx测试
			NginxDetector detector = new NginxDetector(component.getComponentIp(),
					component.getComponentUserName(),
					Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
			Result res = detector.normal();
			return res;
		} else if (component.getComponentType() == 2) {
			// F5测试
			F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
					Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
			result = detector.normal();
		} else if (component.getComponentType() == 3) {
			// 数据库测试
			DbDetector detector = new DbDetector(component.getComponentJdbcurl(), component.getComponentUserName(),
					Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()), component.getComponentDriver());
			result = detector.normal();
		} else if (component.getComponentType() == 4) {
			// 反向通道测试
		} else if (component.getComponentType() == 5) {
			// DNS测试
			DNSDetector detector = new DNSDetector(component.getComponentIp(),53, component.getKey(), component.getSecret(), component.getDnsZone()+".");
			result = detector.normal();
		}if (component.getComponentType() == 6) {
			// nginx测试
			result = true;
		} 
		if (result) {
			logger.info("test component success");
			return new Result(true, "组件连接测试成功");
		} else {
			logger.error("test component failed");
			return new Result(false, "组件连接测试失败");
		}
	}

	// 组件名称查重
	public boolean checkName(String id, String name) {
		Component component = new Component();
		component.setComponentName(name);
		component = componentService.getComponent(component);
		if (component == null) {
			return true;
		}
		if(id == null || id.isEmpty()){
			return false;
		}else {
			return component.getComponentId().equals(id);
		}

	}
	
	public Result reDeploy(String comId,User user){
		Result result = null;
		
		return result;
	}

	/**
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年1月13日
	*/
	public Result refresh(String[] chk_value, User user) {
		Result result=null;
		String msg = "";
		logger.info(user.getUserName()+" refresh systems :["+Arrays.toString(chk_value)+"]");
		for(String deployId:chk_value){
			Deploy deploy = deployService.getDeploy(deployId);
			if(deploy==null){
				logger.error("Deploy is null query by deployId:"+deployId);
				continue;
			}
			/**
			 * 1、如果不是当前运行的版本，删除原来的应用镜像，重新构建。
			 * 2、如果是当前运行的版本：（1）删除原来应用镜像，重新构建。（2）删除容器，重新启动（3）nginx重新构建
			 */
			String oldAppImageId = deploy.getAppImageId();
			System system = systemService.getSystemById(deploy.getSystemId());
			
			msg +=system.getSystemName()+deploy.getDeployVersion()+" ";
			if(system.getDeployId().equals(deployId)){
				result = this.reMakeImage(system,deploy,user);
				//更新deploy表
				if(result.isSuccess()){
					String imageId = result.getMessage();
					deploy.setBaseImageId(imageId);
					deployService.updateDeploy(deploy);
				}else{
					return result;
				}
				//回滚一次
				result = systemManager.rollBackImage(deploy.getClusterId(), deployId, user);
				if(!result.isSuccess()){
					return result;
				}
			}else{
				result = this.reMakeImage(system,deploy,user);
				//更新deploy表
				if(result.isSuccess()){
					String imageId = result.getMessage();
					deploy.setBaseImageId(imageId);
					deployService.updateDeploy(deploy);
				}else{
					return result;
				}
			}
			//删除原镜像
			imageService.delete(oldAppImageId);
		}
		
		return new Result(true, "更新数据库组件成功,涉及的物理系统：【"+msg+"】！");
	}
	
	private Result reMakeImage(System system,Deploy deploy,User user){
		RegHost regHost = imageService.loadByImageId(deploy.getBaseImageId());
		//重新创建配置文件
		Result result = this.mkFile(regHost,deploy);
		if(!result.isSuccess()){
			return result;
		}else{
			//新的配置路径
			String newConfigFilePath = result.getMessage();
			deploy.setConfigRoute(tmpHostConfig.getBaseImagePath()+newConfigFilePath+"/");
			deploy.setAppRoute(tmpHostConfig.getBaseImagePath()+newConfigFilePath+"/"+deploy.getAppFilename());
		}
		//1、重建镜像
		Image image = imageService.selectByPrimaryKey(deploy.getBaseImageId());
		if(null==image){
			return new Result(false, "原基础镜像查询结构为空！");
		}
		ImageMaterial material = new ImageMaterial();

		material.setAppFileName(deploy.getAppFilename());
		String appName = deploy.getAppFilename().substring(0, deploy.getAppFilename().length() - 4);
		material.setAppName(appName);
		String appTag = deploy.getDeployVersion();
		material.setAppTag(appTag);
		material.setBaseImage(image);
		
		List<DeployDatabase> deployDatabases =deployDatabaseService.selectByDeployId(deploy.getDeployId());
		List<DataSource> dataSources = new ArrayList<>();
		for(DeployDatabase deployDatabase:deployDatabases){
			Driver driver = driverService.selectByPrimaryId(deployDatabase.getDriverId());
			Component db = new Component();
			db.setComponentId(deployDatabase.getDatabaseId());
			Component databaseInfo = componentService.getComponent(db);
			DataSource dataSource = new DataSource(databaseInfo.getComponentJdbcurl(),
					databaseInfo.getComponentUserName(), Encrypt.decrypt(databaseInfo.getComponentPwd(), localConfig.getSecurityPath()),
					databaseInfo.getComponentDriver(), deployDatabase.getDataSource(), driver.getDriverName(), driver.getDriverPath(),
					deployDatabase.getInitSql(),deployDatabase.getMaxCapacity(),deployDatabase.getMinCapacity());
			dataSources.add(dataSource);
		}
		
		material.setDataSources(dataSources);
		Host host = new Host();
		host.setHostId(regHost.getId());
		host.setHostIp(regHost.getIp());
		host.setHostUser(regHost.getUsername());
		host.setHostPwd(regHost.getPassword());

		material.setSharable(false);
//		material.setRegHost(host);
		material.setPath(deploy.getConfigRoute());
//		material.setRegPort(regHost.getPort());
		material.setSystemId(deploy.getSystemId());
		material.setUserId(user.getUserId());
		
		//上传的配置文件
		List<DeployFile> deployFiles = deployFileService.selectByDeployId(deploy.getDeployId());		
		try {
			if (image.getImageName().toLowerCase().contains("tomcat")) {
				
				List<ConfigFile> configFiles = new ArrayList<>();
				if(null!=deployFiles&&deployFiles.size()>0){
					for(DeployFile deployFile:deployFiles){
						if(null==deployFile.getFileName()||null==deployFile.getFilePath()){
							continue;
						}
						String filePath = deployFile.getFilePath();
						if(!filePath.endsWith("/")){
							filePath = filePath+"/";
						}
						//如果选择tomcat基础镜像，上传路径以/开头的从根目录开始创建路径，如果不是/开头就是放在tomcat 安装目录webapp下
						if(!filePath.startsWith("/")){
							if(filePath.startsWith("webapps")){
								filePath = TomcatConstants.BASE_PATH+filePath;
							}else{
								filePath = TomcatConstants.APP_PATH+filePath;
							}
						}
						ConfigFile configFile =new ConfigFile(deployFile.getFileName(), filePath);
						configFiles.add(configFile);
					}
				}
				material.setConfigFiles(configFiles);
				result = tomcatImageBuilder.build(material);
			} else if (image.getImageName().toLowerCase().contains("weblogic")) {
				List<ConfigFile> configFiles = new ArrayList<>();
				if(null!=deployFiles&&deployFiles.size()>0){
					for(DeployFile deployFile:deployFiles){
						if(null==deployFile.getFileName()||null==deployFile.getFilePath()){
							continue;
						}
						String filePath = deployFile.getFilePath();
						if(!filePath.endsWith("/")){
							filePath = filePath+"/";
						}
						//如果是weblogic基础镜像,上传路径以"/"开头的从根目录开始创建路径，如果不是"/"放在weblogic base_domain下
						if(!filePath.startsWith("/")){
							if(filePath.startsWith("base_domain")){
								filePath = WeblogicContstants.BASE_PATH+filePath.substring(12);
							}else{
								filePath = WeblogicContstants.BASE_PATH+filePath;
							}
						}
						ConfigFile configFile =new ConfigFile(deployFile.getFileName(), filePath);
						configFiles.add(configFile);
					}
				}
				material.setConfigFiles(configFiles);
				result = weblogicImageBuilder.build(material);
			} else {
				logger.error("[" + user.getUserName() + "]:" + "基础镜像类型[" + image.getImageName() + "]不是tomcat或者weblogic，处理失败。");
				result = new Result(false, "[" + user.getUserName() + "]:" + "基础镜像类型[" + image.getImageName() + "]不是tomcat或者weblogic，处理失败。");
			}
		} catch (Exception e) {
			logger.error("[" + user.getUserName() + "]:" + "制作镜像失败: "+e);
			result = new Result(false, "[" + user.getUserName() + "]:" + "制作镜像失败: "+e);
			return result;
		}
		String imageUUID=null;
		if (result.isSuccess()) {
			imageUUID = result.getMessage();
		} else {
			logger.error(result.getMessage());
			return result;
		}
		/* 在镜像表里添加此新增镜像的纪录 */
		String InsertImgId = null;
		try {
			Image biz_image = new Image();
			biz_image.setImageStatus((byte) Status.IMAGE.NORMAL.ordinal());
			String create_imgname = regHost.getIp() + ":" + regHost.getPort() + "/" + appName.toLowerCase();
			biz_image.setImageName(create_imgname);
			biz_image.setImageUuid(imageUUID);
			biz_image.setImageTag(appTag);
			biz_image.setDeployId(deploy.getDeployId());
			biz_image.setTempName(system.getSystemName()+deploy.getDeployVersion());;
			biz_image.setSystemName(system.getSystemName());
			biz_image.setImageCreator(user.getUserId());
			biz_image.setImageType((byte) Type.IMAGE_TYPE.APP.ordinal());
			InsertImgId = imageService.create(biz_image);
			
			RegImage regImage = regImageMapper.selectByImageId(deploy.getBaseImageId());
			RegImage reg_img = new RegImage(KeyGenerator.uuid(), regImage.getRegistryId(), InsertImgId);
			regImageMapper.insert(reg_img);
		} catch (Exception e) {
			logger.error("[" + user.getUserName() + "]:" + "镜像数据保存失败: "+e);
			result = new Result(false, "[" + user.getUserName() + "]:" + "镜像数据保存失败: "+e);
			return result;
		}
		return new Result(true,InsertImgId);
	}
	/**
	 * 创建一个新的配置路径，存放制作镜像相关文件
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年1月14日
	 */
	private Result mkFile(RegHost regHost,Deploy deploy){
		Result result = null;
		//新建配置文件路径
		String timestamp = String.valueOf(java.lang.System.currentTimeMillis());
		
		SSH ssh = new SSH(regHost.getIp(), regHost.getUsername(),
				Encrypt.decrypt(regHost.getPassword(), localConfig.getSecurityPath()));
		if (ssh.connect()) {
			String comadString = "cd " + tmpHostConfig.getBaseImagePath() + ";";
			comadString += "mkdir " + timestamp+";";
			comadString +=" cp " +deploy.getAppRoute() + " "+timestamp+";";
			try {
				boolean b = ssh.execute(comadString);
				result = new Result(b, timestamp);
			} catch (Exception e) {
				logger.info("重新制作镜像时，转存应用文件失败！" + e);
				return new Result(false, "重新制作镜像时，转存应用文件失败！" + e);
			}finally{
				ssh.close();
			}
		} else {
			return new Result(false, "重新制作镜像时，转存应用文件失败！");
		}
		return result;
	}
	
	/**
	 * 根据driverID 查询driverDesc
	 * @param driverId
	 * @return
	 */
	public JSONObject selectDriverDesc(String driverId){
		JSONObject resultObject = new JSONObject();
		Driver driver = driverService.selectByPrimaryId(driverId);
		resultObject.put("driver", driver);
		return resultObject;
	}
	public Component getComponet(Component component){
		return componentService.getComponent(component);
	}
	public Result nginxTest(String hostId) {
		Host host = hostService.getHost(hostId);
		NginxDetector detector = new NginxDetector(host.getHostIp(),
				host.getHostUser(),
				Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		Result res = detector.normal();
		return res;
	}
}
