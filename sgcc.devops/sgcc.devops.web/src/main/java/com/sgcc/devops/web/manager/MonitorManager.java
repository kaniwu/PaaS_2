package com.sgcc.devops.web.manager;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.MonitorConfig;
import com.sgcc.devops.common.constant.SshCommand;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.MonitorCore;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.web.image.Encrypt;

/**  
 * date：2016年2月4日 下午1:58:27
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：MonitorManager.java
 * description：  主机、容器监控数据处理类
 */
@Component("monitorManager")
public class MonitorManager {

	private static Logger logger = Logger.getLogger(MonitorManager.class);
	@Autowired
	private MonitorCore monitorCore;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private HostService hostService;
	@Autowired
	private MonitorService monitorService;
	@Autowired
	private MonitorConfig monitorConfig;
	@Resource
	private LocalConfig localConfig;
	public Map<String, Object> container(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		map.put("time", new SimpleDateFormat("HH:mm:ss").format(new Date()));
		try {
			Container container = new Container();
			container.setConId(id);
			container = containerService.getContainer(container);
			Monitor monitor = this.monitorCore.containerState(id, container.getClusterIp(), container.getClusterPort(),
					container.getConUuid());
			if (null == monitor) {
				emptyMap(map);
			} else {
				map.put("cpu", monitor.getCpu());
				map.put("mem", monitor.getMem());
				map.put("nin", monitor.getNetin());
				map.put("nou", monitor.getNetout());
			}
		} catch (Exception e) {
			logger.error("container monitor exception: "+e.getMessage());
			emptyMap(map);
		}
		return map;
	}

	public Map<String, Object> host(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		map.put("time", new SimpleDateFormat("HH:mm:ss").format(new Date()));
		try {
			Host host = new Host();
			host.setHostId(id);
			host = hostService.getHost(host);
			Monitor monitor = monitorCore.hostState(host.getHostIp(), "8081");
			// TODO 后续监控端口需要从配置文件获取
			if (null == monitor) {
				emptyMap(map);
			} else {
				map.put("cpu", monitor.getCpu());
				map.put("mem", monitor.getMem());
				map.put("nin", monitor.getNetin());
				map.put("nou", monitor.getNetout());
				map.put("disk", monitor.getDiskSpace());
				map.put("diskinfo", getDiskinfo(host));
			}
		} catch (Exception e) {
			logger.error("host monitor exception: "+e.getMessage());
			emptyMap(map);
			map.put("disk", 0.0);
		}
		return map;
	}

	private void emptyMap(Map<String, Object> map) {
		map.put("cpu", 0.0);
		map.put("mem", 0.0);
		map.put("nin", 0.0);
		map.put("nou", 0.0);
	}

	// 获取容器别名
	public String getContainerName(String id) {
		Container container = new Container();
		container.setConId(id);
		container = containerService.getContainer(container);
		return container.getConTempName();
	}

	// 获取主机名称
	public String getHost(String hostId) {
		Host host = hostService.getHost(hostId);
		return host.getHostName();
	}

	// 获取监控数据
	public Map<String, Object> getStaticData(String id, int model, int type) {

		Monitor monitor = new Monitor();
		monitor.setTargetId(id);
		if (type == Type.TARGET_TYPE.HOST.ordinal()) {
			monitor.setTargetType((byte) type);
		} else if (type == Type.TARGET_TYPE.CONTAINER.ordinal()) {
			monitor.setTargetType((byte) type);
		}

		// 查询timeInterval 个小时之前到现在的数据
		monitor.setMonitorTime(new Timestamp(System.currentTimeMillis() - model * 60 * 60 * 1000));

		return getMonitor(monitor);
	}

	// 获取实时监控的初始化监控数据
	public Map<String, Object> getMonitorData(String id, int type) {

		Monitor monitor = new Monitor();
		monitor.setTargetId(id);
		if (type == Type.TARGET_TYPE.HOST.ordinal()) {
			monitor.setTargetType((byte) type);
		} else if (type == Type.TARGET_TYPE.CONTAINER.ordinal()) {
			monitor.setTargetType((byte) type);
		}
		int model = 10 ;
		// 查询timeInterval 个小时之前到现在的数据
		monitor.setMonitorTime(new Timestamp(System.currentTimeMillis() - model * 60 * 1000 - 30*1000));

		return getMonitor(monitor);
	}
	
	
	//获取监控数据封装为Map返回
	public Map<String, Object> getMonitor(Monitor monitor){
		List<Monitor> monitors = monitorService.select(monitor);
		List<String> dates = new ArrayList<>();
		List<String> cpus = new ArrayList<>();
		List<String> mems = new ArrayList<>();
		List<String> nins = new ArrayList<>();
		List<String> nous = new ArrayList<>();
		List<String> disks = new ArrayList<>();
		if (monitors != null) {
			int size = monitors.size();
			if(size<20){
				Date time = monitor.getMonitorTime();
				Long frequency = 30*1000l;
				for(int i= 0 ;i<20-size;i++){
					
					time=new Date(time.getTime()+frequency);
					dates.add(new SimpleDateFormat("HH:mm:ss").format(time));
					cpus.add("0");
					mems.add("0");
					nins.add("0");
					nous.add("0");
					disks.add("0");
				}
			}
			for (Monitor monitorDemo : monitors) {
				dates.add(new SimpleDateFormat("HH:mm:ss").format(monitorDemo.getMonitorTime()));
				cpus.add(monitorDemo.getCpu());
				mems.add(monitorDemo.getMem());
				nins.add(monitorDemo.getNetin());
				nous.add(monitorDemo.getNetout());
				disks.add(monitorDemo.getDiskSpace());
			}

		}
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("time", dates);
		map.put("cpu", cpus);
		map.put("mem", mems);
		map.put("nin", nins);
		map.put("nou", nous);
		map.put("disk", disks);
		return map;
	}
	public List<Map<String, Object>> getDiskinfo(Host host){
		List<Map<String, Object>>  diskinfo  = new ArrayList<Map<String, Object>>();
		SSH ssh = new SSH(host.getHostIp(), host.getHostUser(), Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		String sshResponse = "";
		if(ssh.connect()){
			try {
				sshResponse = ssh.executeWithResult("export LC_CTYPE=zh_CN.GB18030;"+SshCommand.COMMAND_DF);
			} catch (Exception e) { 
				logger.error("ssh  execute exception: "+e.getMessage());
				return diskinfo;
			}
		}
		String [] datalist =sshResponse.split(" +|\n");
		String reStr = datalist[6];
		boolean isEnglist = true;
		if(!reStr.equalsIgnoreCase("on")){
			isEnglist = false;
		}
		//1-6(6是on)是disk的name 
		String [] disk =new String[6];
		for (int i = 0; i < 6; i++) {
             		if(i==5){
             				disk[i]="Mountedon";
             		}else{
             			if(isEnglist){
             				disk[i]=datalist[i];
             			}else{
             				if(datalist[i].equals("已用")) disk[i]="Used";
             				if(datalist[i].equals("已用%")) disk[i]="Use%";
             				if(datalist[i].equals("可用")) disk[i]="Avail";
             				if(datalist[i].equals("文件系统")) disk[i]="Filesystem";
             				if(datalist[i].equals("容量")) disk[i]="Size";
             			}	
             			
             		}
		}
		//7-最后是数据    7-12  13-18  
		Map<String, Object> diskUsageItem = new HashMap<String, Object>();
		if(isEnglist){
			for (int t =7;t<datalist.length;t++) {
				// 2-3-4需要将G变成M
				String temp = "";
				if((t-1)%6==1||(t-1)%6==2||(t-1)%6==3){
					temp =replaceMtoG(datalist[t]);
				}
				else{
					temp =datalist[t];
				}
				diskUsageItem.put(disk[(t-1)%6], temp);
				//如果是最后一个，则放入list中，并将数组清空
				if((t-1)%6==5){
					diskinfo.add(diskUsageItem);
					diskUsageItem= new HashMap<String, Object>();
				}
			}
		}else{
			for (int t =6;t<datalist.length;t++) {
				String temp = "";
				if(t%6==1||t%6==2||t%6==3){
					temp =replaceMtoG(datalist[t]);
				}
				else{
					temp =datalist[t];
				}
				diskUsageItem.put(disk[t%6], temp);
				//如果是最后一个，则放入list中，并将数组清空
				if(t%6==5){
					diskinfo.add(diskUsageItem);
					diskUsageItem= new HashMap<String, Object>();
				}
			}
		}
		return diskinfo;
		
	}
	//将M变成G并保存2位小数
	 public static String replaceMtoG(String data){
    	 String useData ="";
    		if (data.contains("G")) {
    			useData += (int)(Float.valueOf(data.substring(0, data.length() - 1))*100);
    		} else if (data.contains("M")) {
    			useData +=(int)(Float.valueOf(data.substring(0, data.length() - 1))/1000*100);
    		}else {
    			useData ="0";
    		}
		return useData;
    	 
     }
	 public boolean clusterMonitor(String clusterId){
		boolean b = false;
		//主机cpu上限
		String cpu = monitorConfig.getMonitorCpu();
		//主机内存上限
		String mem = monitorConfig.getMonitorMem();
		//磁盘上限
		String disk = monitorConfig.getHostMonitorStoreUsed();
		List<Host> hosts = hostService.listHostByClusterId(clusterId);
		for(Host host:hosts){
			if(host.getHostType()==Type.HOST.DOCKER.ordinal()){
				long begin = System.currentTimeMillis();
				long end = 0l;
				while(!b&&end-begin<30*1000){
					Map<String, Object> map =this.host(host.getHostId());
					String diskUsed = getDiskUsed(host);
					//如果cpu和内存都低于最高限制
					if(Double.parseDouble(cpu)>Double.parseDouble(map.get("cpu").toString())
							&&Double.parseDouble(mem)>Double.parseDouble(map.get("mem").toString())
							&&Double.parseDouble(disk)>Double.parseDouble(StringUtils.isEmpty(diskUsed)?"0":diskUsed)){
						b= true;
					}
					if(!b){
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					end = System.currentTimeMillis();
				}
				if(b){
					break;
				}
			}
		}
		if(!b){
			logger.warn("集群资源不足！");
		}
		return b;
	}
	public String getDiskUsed(Host host){
		String hostSecure = Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath());
		SSH ssh = new SSH(host.getHostIp(),host.getHostUser(), hostSecure);
		String sshResponse = "";
		if(ssh.connect()){
			try {
				sshResponse = ssh.executeWithResult("df -h|grep "+monitorConfig.getHostMonitorStorePath()+"|awk '{print $5}'");
			} catch (Exception e) { 
			}
		}
		sshResponse = sshResponse.replace("%", "");
		
		return sshResponse;
	}
}
