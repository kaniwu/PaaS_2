/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.sgcc.devops.f5API.bean.LocalLBBean;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.dao.entity.CompPort;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Port;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployNgnixService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegImageService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.NginxListenPort;
import com.sgcc.devops.web.image.NginxLocation;
import com.sgcc.devops.web.image.NginxMeta;
import com.sgcc.devops.web.image.NginxUpstream;
import com.sgcc.devops.web.image.SystemServer;
import com.sgcc.devops.web.image.builder.NginxFileBuilder;

/**  
 * date：2016年1月12日 上午10:40:45
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DeployNginx.java
 * description：  容器nginx/组件nginx配置管理类
 */
@Repository
public class CompPortManager {
	private static Logger logger = Logger.getLogger(CompPortManager.class);
	@Resource
	private DeployNgnixService deployNgnixService;
	@Resource
	private SystemService systemService;
	@Resource
	private ContainerService containerService;
	@Resource
	private ConportService conportService;
	@Resource
	private DeployService deployService;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private HostMapper hostMapper;
	@Resource
	private ComponentService componentService;
	@Resource
	private ImageService imageService;
	@Resource
	private RegImageService regImageService;
	@Resource
	private HostService hostService;
	@Resource
	private ClusterService clusterService;
	@Resource
	private ContainerManager containerManager;
	@Resource
	private PortService portService;
	@Resource
	private RegistryService registryService;
	@Resource
	private RegistryLbService lbService;
	/**
	 * 更新负载 
	 * @author mayh
	 * @return String
	 * @version 1.0 2015年11月17日
	 * @throws Exception 
	 */
	public Result modifyNginxGroup(String nginxGroupId,String nginxPortId,String usedId,String usedType){
		CompPort compPort = new CompPort();
		compPort.setCompId(nginxGroupId);
		compPort.setUsedId(usedId);
		List<CompPort> compPorts = portService.selectAllCompPort(compPort);
		if(null==nginxPortId&&!compPorts.isEmpty()){
			nginxPortId = compPorts.get(0).getPortId();
		}
		this.modifyNginxGroupData(nginxGroupId, nginxPortId, usedId, usedType);
		List<ComponentHost> nginxHosts = hostService.selectComponentHost(nginxGroupId);
		//nginx的upstream
		List<NginxUpstream> nginxUpstreams = new ArrayList<NginxUpstream>();
		List<NginxListenPort> nginxListenPorts = new ArrayList<NginxListenPort>();
		CompPort temp = new CompPort();
		temp.setCompId(nginxGroupId);
		List<String> ports =  portService.selectGroupByPort(temp);
		for(String portId:ports){//nginx端口遍历
			CompPort temp2 = new CompPort();
			temp2.setCompId(nginxGroupId);
			temp2.setPortId(portId);
			Port port =portService.selectByPrimaryKey(portId);
			List<CompPort> UsedComps =  portService.selectAllCompPort(temp2);
			List<NginxLocation> nginxLocations = new ArrayList<>();
			for(CompPort UsedComp:UsedComps){//nginx端口遍历部署版本
				Deploy deploy = deployService.getDeploy(UsedComp.getUsedId());
				if(null==deploy){
					continue;
				}
				System system = systemService.getSystemById(deploy.getSystemId());
				if(!deploy.getDeployId().equals(system.getDeployId())){
					continue;
				}
				if(null!=deploy){
					String dnsName = deploy.getDomainName()+"."+port.getPort();
					List<SystemServer> servers = new ArrayList<>();
					NginxLocation nginxLocation = new NginxLocation(deploy.getAppName(), dnsName);
					
					List<Container> containers = containerService.getBySysId(deploy.getSystemId());
					// 一个物理系统部署的所有容器ip：port
					for (Container container : containers) {
						List<ConPort> conPorts;
						try {
							conPorts = conportService.listConPorts(container.getConId());
							for (ConPort conPort : conPorts) {
								SystemServer server = new SystemServer();
								server.setIp(conPort.getConIp());
								server.setPort(conPort.getPubPort());
								servers.add(server);
							}
						} catch (Exception e) {
							logger.error("get container ip:port fail when update nginx: "+e.getMessage());
						}
					}
					if(servers.size()>0){
						NginxUpstream nginxUpstream = new NginxUpstream(dnsName, servers);
						nginxUpstreams.add(nginxUpstream);
						nginxLocations.add(nginxLocation);
					}
				}
			}
			if(nginxLocations.size()>0){
				NginxListenPort nginxListenPort = new NginxListenPort(port.getPort(), nginxLocations);
				nginxListenPorts.add(nginxListenPort);
			}
		}
		NginxFileBuilder builder = new NginxFileBuilder();
		NginxMeta meta = new NginxMeta(nginxUpstreams, nginxListenPorts);

		meta.setDirectory(localConfig.getLocalNginxConfPath());
		builder.setLocalConfig(localConfig);
		builder.setNginxMeta(meta);
		builder.build();
		
		StringBuffer message = new StringBuffer();
		for(ComponentHost nginxHost:nginxHosts){
			// nginx覆盖掉原来文件
			String nginxIpString = nginxHost.getHostIp();
			String nginxUserString = nginxHost.getHostUser();
			String nginxPwdString = Encrypt.decrypt(nginxHost.getHostPwd(), localConfig.getSecurityPath());

			SSH nginxSSH = new SSH(nginxIpString, nginxUserString, nginxPwdString);
			if(nginxSSH.connect()){
				boolean b = nginxSSH.SCPFile(localConfig.getLocalNginxConfPath() + "nginx.conf", "/etc/nginx/");
				if (!b) {
					logger.error("nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
							+ "nginx.conf to remote /etc/nginx/");
					message.append("\n");
					message.append("nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
							+ "nginx.conf to remote /etc/nginx/");
				}
			}else{
				message.append("\n");
				message.append("nginx config update fail : connect to the Host 【"+nginxIpString+"】 fail");
			}
			nginxSSH.close();
			if(nginxSSH.connect()){
				try {
					boolean b = nginxSSH.execute("sudo nginx -s reload");
				} catch (Exception e) {
					message.append("/n");
					message.append("nginx restart fail: "+e);
				}
			}
			nginxSSH.close();
		}
		if(StringUtils.isEmpty(message.toString())){
			return new Result(true, "Nginx更新成功！");
		}else{
			logger.error("部署过程出现异常："+message);
			return new Result(false, "部署过程出现异常："+message);
		}
		
	}
//	public static void main(String[] args) {
//		SSH nginxSSH = new SSH("10.134.161.94", "root", "sdit123!");
//		if(nginxSSH.connect()){
//			try {
//				String b = nginxSSH.executeWithResult("sudo nginx -t");
//				java.lang.System.out.println(b);
//			} catch (Exception e) {
//				 e.printStackTrace();
//			}
//		}
//		nginxSSH.close();
//	}
	/**
	 * 更新关联关系
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年10月20日
	 */
	private int modifyNginxGroupData(String compId,String compPortId,String usedId,String usedType) {
		int result = 1;
		//删除原来的的关联
		portService.deleteCompPortByUsedId(usedId);
		//重新创建
		CompPort compPort = new CompPort(compId, compPortId, usedId, usedType);
		result = portService.insertCompPort(compPort);
		return result;
	}
	/**
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年7月26日
	 */
	public Result modifyNginxConfig(String nginxId,String nginxPortId,String usedId,String usedType) throws Exception{
		CompPort compPort = new CompPort();
		compPort.setCompId(nginxId);
		compPort.setUsedId(usedId);
		List<CompPort> compPorts = portService.selectAllCompPort(compPort);
		if(null==nginxPortId&&!compPorts.isEmpty()){
			nginxPortId = compPorts.get(0).getPortId();
		}
		this.modifyNginxGroupData(nginxId, nginxPortId, usedId, usedType);
		Result result =null;
		//nginx的upstream
		List<NginxUpstream> nginxUpstreams = new ArrayList<NginxUpstream>();
		List<NginxListenPort> nginxListenPorts = new ArrayList<NginxListenPort>();
		CompPort temp = new CompPort();
		temp.setCompId(nginxId);
		List<String> ports =  portService.selectGroupByPort(temp);
		for(String portId:ports){//nginx端口遍历
			CompPort temp2 = new CompPort();
			temp2.setCompId(nginxId);
			temp2.setPortId(portId);
			Port port =portService.selectByPrimaryKey(portId);
			List<CompPort> UsedComps =  portService.selectAllCompPort(temp2);
			List<NginxLocation> nginxLocations = new ArrayList<>();
			for(CompPort UsedComp:UsedComps){//nginx端口遍历部署版本
				//应用部署还是仓库集群使用
				if(Type.COMPPORT_TYPE.DEPLOY.toString().equals(UsedComp.getUsedType())){
					Deploy deploy = deployService.getDeploy(UsedComp.getUsedId());
					if(null==deploy){
						continue;
					}
					System system = systemService.getSystemById(deploy.getSystemId());
					if(null==system){
						continue;
					}
					if(!deploy.getDeployId().equals(system.getDeployId())){
						continue;
					}
					if(null!=deploy){
						String dnsName = deploy.getDomainName()+"."+port.getPort();
						List<SystemServer> servers = new ArrayList<>();
						NginxLocation nginxLocation = new NginxLocation(deploy.getAppName(), dnsName);
						
						List<Container> containers = containerService.getBySysId(deploy.getSystemId());
						// 一个物理系统部署的所有容器ip：port
						for (Container container : containers) {
							List<ConPort> conPorts;
							try {
								conPorts = conportService.listConPorts(container.getConId());
								for (ConPort conPort : conPorts) {
									SystemServer server = new SystemServer();
									server.setIp(conPort.getConIp());
									server.setPort(conPort.getPubPort());
									servers.add(server);
								}
							} catch (Exception e) {
								logger.error("get container ip:port fail when update nginx: "+e.getMessage());
								result = new Result(false, "get container ip:port fail when update nginx");
							}
						}
						if(servers.size()>0){
							NginxUpstream nginxUpstream = new NginxUpstream(dnsName, servers);
							nginxUpstreams.add(nginxUpstream);
							nginxLocations.add(nginxLocation);
						}
					}
				//仓库集群nginx
				}else if(Type.COMPPORT_TYPE.REGISTRY.toString().equals(UsedComp.getUsedType())){
					RegistryLb registryLb = lbService.registryLbDetails(UsedComp.getUsedId());
					if(null!=registryLb){
						String dnsName = registryLb.getDomainName()+"."+port.getPort();
						List<SystemServer> servers = new ArrayList<>();
						//registryLb.getDomainName()暂时用做nginx location appName
						NginxLocation nginxLocation = new NginxLocation("", dnsName);
						
						List<Registry> registries = registryService.selectRegistryByLbId(registryLb.getId());
						// 仓库ip：port
						for (Registry registry : registries) {
							Host host = hostService.getHost(registry.getHostId());
							SystemServer server = new SystemServer();
							server.setIp(host.getHostIp());
							server.setPort(registry.getRegistryPort().toString());
							servers.add(server);
						}
						if(servers.size()>0){
							NginxUpstream nginxUpstream = new NginxUpstream(dnsName, servers);
							nginxUpstreams.add(nginxUpstream);
							nginxLocations.add(nginxLocation);
						}
					}
				}
			}
			if(nginxLocations.size()>0){
				NginxListenPort nginxListenPort = new NginxListenPort(port.getPort(), nginxLocations);
				nginxListenPorts.add(nginxListenPort);
			}
		}
		NginxFileBuilder builder = new NginxFileBuilder();
		NginxMeta meta = new NginxMeta(nginxUpstreams, nginxListenPorts);

		meta.setDirectory(localConfig.getLocalNginxConfPath());
		builder.setLocalConfig(localConfig);
		builder.setNginxMeta(meta);
		builder.build();
		
		// nginx覆盖掉原来文件
		Component component = new Component();
		component.setComponentId(nginxId);
		component = componentService.getComponent(component);
		String nginxIpString = component.getComponentIp();
		String nginxUserString = component.getComponentUserName();
		String nginxPwdString = Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath());
		String nginxConfig = component.getComponentConfigDir();
		
		SSH nginxSSH = new SSH(nginxIpString, nginxUserString, nginxPwdString);
		nginxSSH.connect();
		boolean b = nginxSSH.SCPFile(localConfig.getLocalNginxConfPath() + "nginx.conf", nginxConfig);
		if (!b) {
			logger.error("nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
					+ "/nginx.conf to remote " + nginxConfig);
			result = new Result(false, "nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
					+ "/nginx.conf to remote " + nginxConfig);
			return result;
		}
		nginxSSH.close();
		nginxSSH.connect();
		try {
			b = nginxSSH.execute("sudo nginx -s reload");
		} catch (Exception e) {
			logger.error("nginx restart fail: "+e);
			result = new Result(false, "nginx restart fail");
		}
		nginxSSH.close();
		//add by lianyd  存在备nginx地址
		if(component.getBackComponentIp()!=null && !"".equals(component.getBackComponentIp())){
			Host backHost = new Host();
			backHost.setHostIp(component.getBackComponentIp());
			backHost = hostMapper.selectHost(backHost);
			String password = "";
			if (!backHost.getHostPwd().startsWith("{AES}")){
				password = backHost.getHostPwd();
			}else{
				password = Encrypt.decrypt(backHost.getHostPwd(), localConfig.getSecurityPath());
			}
			SSH backNginxSSH = new SSH(component.getBackComponentIp(), backHost.getHostUser(), password);
			backNginxSSH.connect();
			boolean back = backNginxSSH.SCPFile(localConfig.getLocalNginxConfPath() + "nginx.conf", component.getComponentConfigDir());
			if (!back) {
				logger.error("back nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
						+ "/nginx.conf to remote " + component.getComponentConfigDir());
				result = new Result(false, "back nginx config update fail :From local " + localConfig.getLocalNginxConfPath()
						+ "/nginx.conf to remote " + component.getComponentConfigDir());
				return result;
			}
			backNginxSSH.close();
			backNginxSSH.connect();
			try {
				back = backNginxSSH.execute("sudo nginx -s reload");
			} catch (Exception e) {
				logger.error("back nginx restart fail: "+e);
				result = new Result(false, "back nginx restart fail");
			}
			backNginxSSH.close();
		}
		return new Result(true, "Nginx更新成功！");
	}
	
	/**
	 * 是否被占用
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月29日
	 */
	public Result validateNginxPort(DeployModel deployModel,CompPort compPortIn){
		System system = systemService.loadById(deployModel.getSystemId());
		//启用dns
		if(deployModel.isDnsEnable()){
			Deploy deploy = new Deploy();
			deploy.setDomainName(deployModel.getDomainName());
			List<Deploy> deploys = deployService.select(deploy);
			for(Deploy deploy2:deploys){
				if(deploy.getDnsEnable()==1&&(null==system || !system.getDeployId().equals(deploy2.getDeployId()))){
					logger.warn("域名【"+deployModel.getDomainName()+"】端口已被【"+deploy2.getAppName()+"】占用");
					return new Result(false, "域名【"+deployModel.getDomainName()+"】端口已被【"+deploy2.getAppName()+"】占用");
				}
			}
			RegistryLb lb = new RegistryLb();
			lb.setDomainName(deployModel.getDomainName());
		}else{
			CompPort compPort = new CompPort();
			compPort.setCompId(compPortIn.getCompId());
			compPort.setPortId(compPortIn.getPortId());
			List<CompPort> compPorts = portService.selectAllCompPort(compPort);
			Port port = portService.selectByPrimaryKey(compPortIn.getPortId());
			Component component = new Component();
			component.setComponentId(compPort.getCompId());
			component = componentService.getComponent(component);
			for(CompPort compPort2:compPorts){
				Deploy deploy = deployService.getDeploy(compPort2.getUsedId());
				if(deploy!=null && deploy.getDnsEnable()==0&&(null==system || !system.getSystemId().equals(deploy.getSystemId()))){
					logger.warn("Nginx组件【"+component.getComponentName()+"的"+port.getPort()+"】端口已被占用");
					return new Result(false, "Nginx组件【"+component.getComponentName()+"的"+port.getPort()+"】端口已被占用");
				}
			}
		}
		return new Result(true, "");
	}
	
	public Result addF5MembersConfig(F5Model f5Model){
		try{
			LocalLBBean localLBBean = new LocalLBBean();
			localLBBean.login("admin","jsepc01!","qilincheng.oicp.net",443);
			//创建顺序monitor、POOL、VS
			localLBBean.createMonitor("qlc_monitor",5, 16, "http", "/", "js.sgcc.com.cn");
			localLBBean.createPool("QLC_TEST_POOL3", "10.134.88.217:80;10.134.90.116:80","qlc_monitor");
			localLBBean.createVs("QLC_TEST_VS3", "172.16.85.201", 8092, "QLC_TEST_POOL3");
			//增加Member
			localLBBean.addMember("QLC_TEST_POOL3", "10.134.90.111:80");
			localLBBean.removeMember("/Common/QLC_TEST_POOL3", "10.134.90.111:80");

		}catch(Exception ex){
			logger.error(ex.getMessage());
			return new Result(true, "配置F5失败："+ex.getMessage());
		}
		logger.warn("配置F5成功！");
		return new Result(true, "配置F5成功！");
	}
	public Result delF5MembersConfig(F5Model f5Model){
		try
		{
			LocalLBBean localLBBean = new LocalLBBean();
			localLBBean.login("admin","jsepc01!","qilincheng.oicp.net",443);
			localLBBean.removeMember("/Common/QLC_TEST_POOL3", "10.134.90.111:80");
			//删除顺序VS POOL Monitor
//			localLBBean.deleteVs("QLC_TEST_VS3");
//			localLBBean.deletePool("QLC_TEST_POOL3");
//			localLBBean.deleteMonitor("qlc_monitor");
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return new Result(true, "");
	}
	public Result validateNginxPort(String systemId,String nginxGroupId,String nginxPortId,boolean dnsEnable,String dns){
		System system = systemService.loadById(systemId);
		CompPort compPort = new CompPort();
		compPort.setCompId(nginxGroupId);
		compPort.setPortId(nginxPortId);
		List<CompPort> compPorts = portService.selectAllCompPort(compPort);
		Component component = new Component();
		component.setComponentId(compPort.getCompId());
		component = componentService.getComponent(component);
		Port port = portService.selectByPrimaryKey(nginxPortId);
		for(CompPort compPort2:compPorts){
			//判断使用dns+port还是ip+port
			//如果是应用发布使用的
			if(compPort2.getUsedType().equals(Type.COMPPORT_TYPE.DEPLOY.name())){
				Deploy deploy2 = deployService.getDeploy(compPort2.getUsedId());
				//跳过本次发布的物理系统
				if(null==deploy2||deploy2.getSystemId().equals(systemId)){
					continue;
				}
				//如果是域名_PORT作为upstream,并且端口号也相同
				if(dnsEnable&&deploy2.getDnsEnable()==1&&deploy2.getDomainName().equals(dns)&&compPort2.getPortId().equals(compPort.getPortId())){
					return new Result(false, "Nginx【"+dns+":"+port.getPort()+"】端口已被【"+system.getSystemName()+"】占用");
				}
				//如果是IP_PORT作为upstream,并且端口号也相同
				if(!dnsEnable&&deploy2.getDnsEnable()==0){
					return new Result(false, "Nginx【"+component.getComponentIp()+":"+port.getPort()+"】端口已被【"+system.getSystemName()+"】占用");
				}
			}
		}
		return new Result(true, "");
	}
}
