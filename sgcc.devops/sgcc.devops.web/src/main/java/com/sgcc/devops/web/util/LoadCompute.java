package com.sgcc.devops.web.util;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.model.MonitorModel;
import com.sgcc.devops.dao.entity.Monitor;

/**
 * 计算集群负载，并排序
 */
@Repository("clusterLoad")
public class LoadCompute {

	/**
	 * @param List<Monitor>
	 *
	 * @return List<MonitorModel> 计算load ,根据load排序
	 */
	public Double clusterLoadCompute(List<MonitorModel> models) {
		Double load = 0d;
		for (MonitorModel model : models) {
			load = load + model.getValue();
		}
		return load / models.size();
	}

	/**
	 * 对监控数据进行计算，得到负载，
	 *
	 * return MonitorModel
	 */
	public MonitorModel compute(Monitor monitor) {
		Double cpu = Double.parseDouble(monitor.getCpu());
		// Double disk=Double.parseDouble(monitor.getDiskSpace());
		Double mem = Double.parseDouble(monitor.getMem());
		// Double netin=Double.parseDouble(monitor.getNetin());
		// Double netout=Double.parseDouble(monitor.getNetout());

		// Double load=(cpu+disk+mem+netin+netout)/4;
		Double load = (cpu + mem) / 2;
		MonitorModel model = new MonitorModel(monitor.getMonitorId(), monitor.getTargetType(), monitor.getTargetId(),
				(double)Math.round(load*100)/100);

		return model;
	}
}
