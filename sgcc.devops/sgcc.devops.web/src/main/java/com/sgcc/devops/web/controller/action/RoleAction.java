package com.sgcc.devops.web.controller.action;


import java.text.ParseException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.Role;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.RoleManager;
import com.sgcc.devops.web.query.QueryList;

@Controller
@RequestMapping("role")
public class RoleAction {
	Logger logger = Logger.getLogger(RoleAction.class);

	@Resource
	private QueryList queryList;
	@Resource
	private RoleManager roleManager;

	/*
	 * 角色详情列表
	 * by luogan
	 */
	@RequestMapping("/detail/{id}.html")
	public ModelAndView detail(@PathVariable String id) {
		ModelAndView mav = new ModelAndView("role/detail");
		Role role = roleManager.detail(id);
		mav.addObject("roleInfo", role);
		return mav;
	}
	
	/*
	 * 查询角色列表
	 * 包含按照角色名称进行模糊查询
	 * by luogan
	 */
	@RequestMapping(value = "/all")
	@ResponseBody
	public GridBean all(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, Role role) {
		User user = (User) request.getSession().getAttribute("user");
		if (role.getRoleName()!= null) {
			String search_name = request.getParameter("roleName").trim();
			role.setRoleName(search_name);
		}
		return queryList.roleList(user.getUserId(), page, rows, role);
	}

	/*
	 * 查询角色列表 
	 * by luogan
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public String allList(HttpServletRequest request, Role role) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.queryAllRoleList(user.getUserId(), role);
		return ja.toString();
	}

	/*
	 * 查询角色权限列表 
	 * by luogan
	 */
	@RequestMapping(value = "/roleAuth", method = { RequestMethod.GET })
	@ResponseBody
	public String roleAuthList(HttpServletRequest request, com.sgcc.devops.dao.entity.RoleAction roleAction) {
		User user = (User) request.getSession().getAttribute("user");
		String ja = queryList.getRoleAuthList(user.getUserId(), roleAction);
		return ja;
	}
	
	/*
	 * 更新角色 
	 * by luogan
	 */
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, Role role)
			throws ParseException {
		if (role == null) {
			return new Result(false, "修改角色失败：传入角色参数有误");
		} else {
			return roleManager.update(role);
		}
	}

	/*
	 * 权限授权给角色
	 * by luogan
	 */
	@RequestMapping(value = "/authToRole", method = { RequestMethod.POST })
	@ResponseBody
	public Result authToRole(HttpServletRequest request, String roles,
			String auths) {

		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = new JSONArray();
		JSONObject params = new JSONObject();
		params.put("userId", user.getUserId());
		params.put("roles", roles);
		params.put("auths", auths);
		ja.add(0, params);
		return roleManager.authToRole(params);
	}
	
	/**
	 * 多功能查询
	 * @param request
	 * @param pagenum
	 * @param pagesize
	 * @param role
	 * @return
	 */
	@RequestMapping("/advancedSearch")
	@ResponseBody
	public GridBean advancedSearch(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int pagenum,
			@RequestParam(value = "rows", required = true) int pagesize, Role role) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			String params = request.getParameter("params").trim();
			String values = request.getParameter("values").trim();
			JSONObject json_object = new JSONObject();
			json_object.put("params", params);
			json_object.put("values", values);
			return roleManager.advancedSearchRole(user.getUserId(), pagenum, pagesize, role,json_object);

		} catch (Exception e) {
			logger.error("查询角色列表失败！", e);
			return null;
		}
	}
	
	/*
	 * 查询用户角色列表 
	 * by luogan
	 */
	@RequestMapping(value = "/userRoleList", method = { RequestMethod.GET })
	@ResponseBody
	public String userRoleList(HttpServletRequest request, String userId) {
		JSONArray ja = queryList.queryAllRoleListByUserId(userId);
		return ja.toString();
	}
	
}
