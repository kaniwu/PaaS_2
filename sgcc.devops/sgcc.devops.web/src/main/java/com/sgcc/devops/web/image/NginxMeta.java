package com.sgcc.devops.web.image;

import java.util.List;

/**
 * 负载配置元数据
 * 
 * @author dmw
 *
 */
public class NginxMeta extends Material {
	private List<NginxUpstream> nginxUpstreams;
	private List<NginxListenPort> nginxListenPorts;
	public List<NginxUpstream> getNginxUpstreams() {
		return nginxUpstreams;
	}
	public void setNginxUpstreams(List<NginxUpstream> nginxUpstreams) {
		this.nginxUpstreams = nginxUpstreams;
	}
	public List<NginxListenPort> getNginxListenPorts() {
		return nginxListenPorts;
	}
	public void setNginxListenPorts(List<NginxListenPort> nginxListenPorts) {
		this.nginxListenPorts = nginxListenPorts;
	}
	public NginxMeta(List<NginxUpstream> nginxUpstreams,
			List<NginxListenPort> nginxListenPorts) {
		super();
		this.nginxUpstreams = nginxUpstreams;
		this.nginxListenPorts = nginxListenPorts;
	}
	
}
