package com.sgcc.devops.web.image;

import org.apache.log4j.Logger;

import weblogic.security.internal.SerializedSystemIni;
import weblogic.security.internal.encryption.ClearOrEncryptedService;
import weblogic.security.internal.encryption.EncryptionService;

/**
 * 加解密工具类
 * 
 * @author dmw
 *
 */
public class Encrypt {
	private static Logger logger = Logger.getLogger(Encrypt.class);
	static EncryptionService es = null;
	static ClearOrEncryptedService ces = null;

	/**
	 * 解密
	 * 
	 * @param despass
	 *            密文
	 * @param path
	 *            密钥存放路径
	 * @return 明文
	 */
	public static String decrypt(String despass, String path) {
		try {
			es = SerializedSystemIni.getEncryptionService(path);
			ces = new ClearOrEncryptedService(es);
			return ces.decrypt(despass);
		} catch (Exception e) {
			logger.error("Decrypt info exception:", e);
			return null;
		}
	}

	/**
	 * 加密
	 * 
	 * @param pass
	 *            明文
	 * @param path
	 *            私有密钥路径：security.path对应的值
	 * @return
	 */
	public static String encrypt(String pass, String path) {
		try {
			es = SerializedSystemIni.getEncryptionService(path);
			ces = new ClearOrEncryptedService(es);
			return ces.encrypt(pass);
		} catch (Exception e) {
			logger.error("Encrypt info exception:", e);
			return null;
		}
	}
}
