package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.NginxListenPort;
import com.sgcc.devops.web.image.NginxLocation;
import com.sgcc.devops.web.image.NginxMeta;
import com.sgcc.devops.web.image.NginxUpstream;
import com.sgcc.devops.web.image.SystemServer;
/**
 * Nginx配置文件生成器
 * @author dmw
 *
 */
@Repository
public class NginxFileBuilder implements FileBuilder {
	private static Logger logger = Logger.getLogger(NginxFileBuilder.class);
	private static final String FILE_NAME = "nginx.vm";
	private LocalConfig localConfig;
	private NginxMeta nginxMeta;

	private enum ParamPosition {
		UPSTREAM_LIST,LISTENPORT_LIST
	}

	public void init(NginxMeta nginxMeta, LocalConfig localConfig) {
		this.nginxMeta = nginxMeta;
		this.localConfig = localConfig;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}
	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}
	public NginxMeta getNginxMeta() {
		return nginxMeta;
	}

	public void setNginxMeta(NginxMeta nginxMeta) {
		this.nginxMeta = nginxMeta;
	}

	@Override
	public File build() {
		logger.info("开始制作nginx配置文件");
		if (null == nginxMeta) {
			logger.error("Nginx nginxMeta is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, localConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();
		// 组合配置信息
		context.put(ParamPosition.UPSTREAM_LIST.name(), nginxMeta.getNginxUpstreams());
		context.put(ParamPosition.LISTENPORT_LIST.name(), nginxMeta.getNginxListenPorts());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get nginx template infos error", e);
			return null;
		}
		// 生成新的配置文件
		String localPath = nginxMeta.getDirectory();
		localPath = FileUploadUtil.check(localPath);
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = localPath.replace("\\", "/");
		File file = new File(localPath + "/nginx.conf");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build nginx exception:", e);
				return null;
			}
		}
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			logger.info("nginx配置文件制作完成");
			return file;
		} catch (IOException e) {
			logger.error("nginx配置文件制作异常:", e);
			return null;
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void main(String[] args) {
		NginxFileBuilder builder = new NginxFileBuilder();
		
		//nginx的upstream
		List<NginxUpstream> nginxUpstreams = new ArrayList<NginxUpstream>();
		List<SystemServer> systemServers = new ArrayList<>();
		SystemServer systemServer1 = new SystemServer("19.1.1.1", "335");
		SystemServer systemServer = new SystemServer("19.1.1.", "336");
		systemServers.add(systemServer1);
		systemServers.add(systemServer);
		NginxUpstream nginxUpstream = new NginxUpstream("test1.com:80", systemServers);
		NginxUpstream nginxUpstream2 = new NginxUpstream("test2.com:80", systemServers);
		nginxUpstreams.add(nginxUpstream);
		nginxUpstreams.add(nginxUpstream2);
		//nginx的servers
		List<NginxListenPort> nginxListenPorts = new ArrayList<NginxListenPort>();
		List<NginxLocation> nginxLocations = new ArrayList<>();
		NginxLocation nginxLocation = new NginxLocation("test_jndi", "test1.com:80");
		NginxLocation nginxLocation2 = new NginxLocation("test_jndi2", "test2.com:80");
		nginxLocations.add(nginxLocation);
		nginxLocations.add(nginxLocation2);
		NginxListenPort nginxListenPort = new NginxListenPort("80", nginxLocations);
		nginxListenPorts.add(nginxListenPort);
		NginxMeta meta = new NginxMeta(nginxUpstreams, nginxListenPorts);

		meta.setDirectory("D://SGCC-TEMP/nginx/");
		LocalConfig config = new LocalConfig();
		config.setTemplatePath("D://SGCC-TEMP/vm/");
		builder.setLocalConfig(config);
		builder.setNginxMeta(meta);
		builder.build();
	}
}
