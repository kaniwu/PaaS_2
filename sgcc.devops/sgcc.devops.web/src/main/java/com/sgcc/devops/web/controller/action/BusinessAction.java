/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.BusinessManager;
import com.sgcc.devops.web.query.QueryList;


/**  
 * date：2016年2月4日 下午1:46:13
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessAction.java
 * description：  业务系统访问控制类
 */
@RequestMapping("/business")
@Controller
public class BusinessAction {
	@Resource
	private QueryList queryList;
	@Resource
	private BusinessManager businessManager;
	
	@RequestMapping(value = "/listAll", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean listAll(HttpServletRequest request,PagerModel pagerModel,String businessName) {
		User user = (User) request.getSession().getAttribute("user");
		
		return queryList.listAllOfBusiness(user,pagerModel,businessName);
	}
	@RequestMapping(value = "/loadBusiness", method = { RequestMethod.GET })
	@ResponseBody
	public Result loadBusiness(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = businessManager.loadBusiness(user);
		return result;
	}
}
