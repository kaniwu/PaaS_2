/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.Authority;
import com.sgcc.devops.service.AuthorityService;

/**
 * date：2015年8月23日 下午8:23:30 project name：sgcc-devops-web
 * 
 * @author dingmw
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ParameterManager.java description：
 */
@Component
public class AuthorityManager {
	private static Logger logger = Logger.getLogger(AuthorityManager.class);
	@Resource
	private AuthorityService authorityService;

	public Result update(Authority authority) {
		int result = authorityService.update(authority);
		if(result>0){
			logger.info("Update authority success");
			return new Result(true, "更新权限成功！");
		}else{
			logger.error("Update authority fail");
			return new Result(true, "更新权限失败！");
		}
	}

	public List<Authority> getUserRoleAuths(String userId){
		try {
			return authorityService.listAuthsByUserId(userId);
		} catch (Exception e) {
			logger.error("get auth list by userid["+userId+"] falied! ", e);
			return null;
		}
	}
	
	/**
	 * @author luogan
	 * @param advancedSearchParam
	 * @return
	 * @version 1.0
	 * 2015年10月21日
	 */
	public GridBean advancedSearchAuth(String userId, int pagenum, int pagesize, Authority authority,JSONObject json_object) {
		try {
			return authorityService.advancedSearchAuth(userId, pagenum, pagesize, authority, json_object);
		} catch (Exception e) {
			logger.info("Advanced search auth fail",e);
			return null;
		}
	}
	
}
