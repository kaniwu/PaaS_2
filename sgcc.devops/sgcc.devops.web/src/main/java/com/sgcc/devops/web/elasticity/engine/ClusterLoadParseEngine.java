package com.sgcc.devops.web.elasticity.engine;

import java.util.List;

import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.web.elasticity.bean.LoadData;

/**
 * 集群负载解析引擎
 * @author dmw
 *
 */
public class ClusterLoadParseEngine extends DataParseEngine {

	@Override
	public LoadData parse(List<Monitor> datas) {
		double cpu = 0.0;
		long mem = 0l;
		long net = 0l;
		int link = 0;
		if (null == datas || datas.isEmpty()) {
			return null;
		}
		int size = datas.size();
		for (Monitor monitor : datas) {
			cpu += Double.valueOf(monitor.getCpu());
			mem += Long.valueOf(monitor.getMem());
			net += Long.valueOf(monitor.getNetin());
		}
		cpu = cpu / size;
		mem = mem / size;
		net = net / size;
		link = link / size;
		return new LoadData(cpu, mem, net, link);
	}

}
