package com.sgcc.devops.web.upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import com.sgcc.devops.web.message.MessageHandshakeInterceptor;

@Configuration
@EnableWebMvc
@EnableWebSocket
public class UploadBasicImageWebSocketConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

	private UploadBasicImageWebSocketHandler uploadBasicImageWebSocketHandler;
	private MessageHandshakeInterceptor messageHandshakeInterceptor;

	public UploadBasicImageWebSocketHandler getUploadBasicImageWebSocketHandler() {
		return uploadBasicImageWebSocketHandler;
	}

	@Autowired
	public void setUploadBasicImageWebSocketHandler(UploadBasicImageWebSocketHandler webSocketHandler) {
		this.uploadBasicImageWebSocketHandler = webSocketHandler;
	}

	public MessageHandshakeInterceptor getMessageHandshakeInterceptor() {

		return messageHandshakeInterceptor;
	}

	@Autowired
	private void setMessageHandshakeInterceptor(MessageHandshakeInterceptor messageHandshakeInterceptor) {
		this.messageHandshakeInterceptor = messageHandshakeInterceptor;
	}

	@Bean
	public DefaultHandshakeHandler handshakeHandler() {
		return new DefaultHandshakeHandler();
	}

	@Bean
	public ServletServerContainerFactoryBean createWebSocketContainer() {
		ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
		container.setMaxTextMessageBufferSize(8192);
		container.setMaxBinaryMessageBufferSize(5 * 1024 * 1024);
		return container;
	}

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(this.getUploadBasicImageWebSocketHandler(), "uploadBasicImageService")
				.addInterceptors(this.getMessageHandshakeInterceptor()).setHandshakeHandler(handshakeHandler());
		registry.addHandler(this.getUploadBasicImageWebSocketHandler(), "/sockjs/uploadBasicImageService")
				.addInterceptors(this.getMessageHandshakeInterceptor()).setHandshakeHandler(handshakeHandler())
				.withSockJS();
	}
}
