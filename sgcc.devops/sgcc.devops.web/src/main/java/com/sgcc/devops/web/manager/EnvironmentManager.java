/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.EnvironmentModel;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.EnvironmentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.service.UserService;

import net.sf.json.JSONObject;


@Component
public class EnvironmentManager {
	private static Logger logger = Logger.getLogger(EnvironmentManager.class);
	@Resource
	private EnvironmentService environmentService;
	@Resource
	private SystemService systemService;
	@Resource
	private HostService hostService;
	@Resource
	private ClusterService clusterService;
	@Resource
	private UserService userService;
	/**
	 * 删除环境
	 * @author panjing
	 */
	public Result deleteEnvironment(JSONObject jo) {
		String eId = jo.getString("eId");
		try {
			System record = new System();
			record.setEnvId(eId);
			//环境下的物理系统
			List<System> systems = systemService.selectAllSystem(record);
			if(!systems.isEmpty()){
				Environment environment = environmentService.environmentById(eId);
				logger.info("删除环境【"+environment.geteName()+"】失败，环境下的物理系统在运行！");
				return new Result(true, "删除环境【"+environment.geteName()+"】失败，环境下的物理系统在运行！");
			}
			//环境下的主机
			Host host = new Host();
			host.setHostEnv(eId);
			List<Host> hosts = hostService.selectAllHost(host);
			if(!hosts.isEmpty()){
				Environment environment = environmentService.environmentById(eId);
				logger.info("删除环境【"+environment.geteName()+"】失败，环境下的主机在运行！");
				return new Result(true, "删除环境【"+environment.geteName()+"】失败，环境下的主机在运行！");
			}
			//环境下的集群
			Cluster cluster = new Cluster();
			cluster.setClusterEnv(eId);
			List<Cluster> clusters = clusterService.selectAllCluster(cluster);
			if(!clusters.isEmpty()){
				Environment environment = environmentService.environmentById(eId);
				logger.info("删除环境【"+environment.geteName()+"】失败，环境下的集群在运行！");
				return new Result(true, "删除环境【"+environment.geteName()+"】失败，环境下的集群在运行！");
			}
			//环境下的用户
			User user= new User();
			user.setUserCompany(eId);
			List<User> users = userService.selectAllUsers(user);
			if(!users.isEmpty()){
				Environment environment = environmentService.environmentById(eId);
				logger.info("删除环境【"+environment.geteName()+"】失败，有用户正在使用此环境！");
				return new Result(true, "删除环境【"+environment.geteName()+"】失败，有用户正在使用此环境！");
			}
			int result = environmentService.deleteByPrimaryKey(eId);
			if(result == 1){
				logger.info("删除成功！");
				return new Result(true, "删除成功！");
			}else{
				logger.info("删除失败：数据库操作异常！");
				return new Result(false, "删除失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("删除失败,数据库操作异常:"+e.getMessage());
			return new Result(false, "删除失败,数据库操作异常:"+e.getMessage());
		}
}



	
	/**
	 * 新建环境
	 * @author panjing
	 */
	public Result createParameter(JSONObject jsonObject) {
		String eName = jsonObject.getString("eName");
		String eDesc = jsonObject.getString("eDesc");
		String eCreator = jsonObject.getString("userId");
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		Date eTime = new Date();
		Environment environment = new Environment();
		environment.seteName(eName);
		environment.seteDesc(eDesc);
		environment.seteCreator(eCreator);
		environment.seteTime(eTime);
		environment.seteId(KeyGenerator.uuid());
		//判断不能为空的属性
		if(eName == null){
			logger.info("新增环境失败，环境名称为空");
			return new Result(false, "新增环境失败，环境名称为空");
		}
		try {
			int result =environmentService.insert(environment);
			if(result == 1){
				logger.info("【" + eName + "】创建成功！");
				return new Result(true, "【" +  eName + "】创建成功！");
			}else{
				logger.info("【" +  eName + "】创建失败：数据库操作异常！");
				return new Result(false, "【" + eName + "】创建失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("【" + eName + "】创建失败：数据库操作异常！");
			return new Result(false, "【" + eName + "】创建失败：数据库操作异常！");
		}
		
		
	}
	

	
	/**
	 * 编辑环境
	 * @author panjing
	 */
	public Result updateEnvironment(JSONObject jsonObject) {
		String eId = jsonObject.getString("eId");
		String eName = jsonObject.getString("eName");
		String eDesc = jsonObject.getString("eDesc");
		Environment environment = new Environment();
		environment.seteId(eId);
		environment.seteName(eName);
		environment.seteDesc(eDesc);
		if(eName == null){
			logger.info("编辑环境失败，环境名称为空！");
			return new Result(false, "编辑环境失败，环境名称为空！");
		}
		try {
			int result =environmentService.updateByPrimaryKeySelective(environment);
			if(result == 1){
				logger.info("【" + eName + "】修改成功！");
				return new Result(true, "【" +  eName + "】修改成功！");
			}else{
				logger.info("【" +  eName + "】修改失败：数据库操作异常！");
				return new Result(false, "【" + eName + "】修改失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("【" + eName + "】修改失败：数据库操作异常！");
			return new Result(false, "【" + eName + "】修改失败：数据库操作异常！");
		}
	}




	/**
	* TODO
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年8月8日
	*/
	public List<Environment> searchByEnv(User user, EnvironmentModel environmentModel) {
		Environment environment = new Environment();
		environment.seteId(user.getUserCompany());
		List<Environment> environments = environmentService.environmentsList(environment);
		return environments;
	}
	
	
	

}
