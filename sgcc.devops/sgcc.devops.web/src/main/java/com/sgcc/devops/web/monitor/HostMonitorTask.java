package com.sgcc.devops.web.monitor;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.core.monitor.HostMonitorParam;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 主机监控任务
 * @author dmw
 *
 */
@Component
public class HostMonitorTask {
	private static Logger logger = Logger.getLogger(HostMonitorTask.class);
	@Autowired
	private HostService hostService;
	@Autowired
	private HostMonitorCluster hostMonitorCluster;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private TaskCache cache;
	@Autowired
	private LocalConfig localConfig;

	public void hostMonitor() {
		logger.debug("start monitor...");
		List<Host> hosts = new ArrayList<>();
		try {
			hosts = hostService.allOnlineHost();
		} catch (Exception e) {
			logger.error("获取主机列表异常：", e);
		}
		if (null == hosts || hosts.isEmpty()) {
			logger.info("end monitor...");
			return;
		}
		List<HostMonitorParam> params = new ArrayList<HostMonitorParam>();
		HostMonitorParam param = null;
		for (Host host : hosts) {
			param = new HostMonitorParam(host.getHostId(), host.getHostIp(), "8081");
			params.add(param);
			host.setHostPwd(Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
		}

		new Thread(new HostDetectTask(cache, hosts)).start();// 启动探测主机状态的县城
		List<Monitor> monitors = null;
		try {
			monitors = hostMonitorCluster.batchMoniter(params);
		} catch (Exception e) {
			logger.error("get host monitor data exception:", e);
		}
		if (null == monitors || monitors.isEmpty()) {
			logger.info("end monitor...");
			return;
		}
		try {
			String sql = "insert into dop_monitor(MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE) values(?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.batchUpdate(
					sql.toString(),
			        monitors,
			        monitors.size(),
			        new ParameterizedPreparedStatementSetter<Monitor>() {
			          @Override
					public void setValues(PreparedStatement ps, Monitor argument) throws SQLException {
			        	  ps.setString(1, KeyGenerator.uuid());
			        	  ps.setByte(2, argument.getTargetType());
			        	  TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
			        	  ps.setTimestamp(3,new Timestamp(new Date().getTime()));
			        	  ps.setString(4, argument.getTargetId());
			        	  ps.setString(5, argument.getCpu());
			        	  ps.setString(6, argument.getMem());
			        	  ps.setString(7, argument.getNetin());
			        	  ps.setString(8, argument.getNetout());
			        	  ps.setString(9, argument.getDiskSpace());
			          }
			        });
		} catch (Exception e) {
			logger.error("save monitor data exception:", e);
		}
		logger.debug("end monitor...");
	}
}
