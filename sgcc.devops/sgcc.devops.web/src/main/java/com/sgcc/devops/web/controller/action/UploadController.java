package com.sgcc.devops.web.controller.action;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.web.image.Encrypt;
/**
 * 未使用
 * 2016-12-16
 * @author Administrator
 *
 */

@Controller
@RequestMapping(value = "/file", method = { RequestMethod.POST })
public class UploadController { 
	@Resource
	private ImageService imageService;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private HostService hostService;
	@Resource
	private RegistryLbService registryLbService;
	
	@RequestMapping(value = "/uploadFile", method = { RequestMethod.POST })
	public void uploadFile(HttpServletRequest request,HttpServletResponse response) throws IllegalStateException, IOException {
		String timestamp =  request.getParameter("timestamp");
		if(null==timestamp){
			timestamp="";
		}
		//fileType:baseImage/app/library/dbDriver/deploy
		String fileType =  request.getParameter("fileType");
		//创建一个通用的多部分解析器
		CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver(request.getSession().getServletContext());
		//判断 request 是否有文件上传,即多部分请求
		String localPath = "";
		if(multipartResolver.isMultipart(request)){
			//转换成多部分request  
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;
			//取得request中的所有文件名
			Iterator<String> iter = multiRequest.getFileNames();
			while(iter.hasNext()){
				//记录上传过程起始时的时间，用来计算上传时间
				int pre = (int) System.currentTimeMillis();
				//取得上传文件
				MultipartFile file = multiRequest.getFile(iter.next());
				String localFIlePath = "";
				if("baseImage".equals(fileType)){//基础镜像
					localFIlePath = localConfig.getLocalBaseImagePath();
				}else if("app".equals(fileType)){//应用包上传
					localFIlePath = localConfig.getLocalAppPath();
				}else if("library".equals(fileType)){//weblogic预加载包
					localFIlePath = localConfig.getLocalLibraryPath();
				}else if("dbDriver".equals(fileType)){//数据库驱动包
					localFIlePath = localConfig.getLocalDBDriverPath();
				}else if("deploy".equals(fileType)){//部署时上传文件
					localFIlePath = localConfig.getLocalDeployPath();
				}else if("hostCom".equals(fileType)){//主机组件，主机组件安装包存放在paas平台所咋服务器；安装时scp到相应的服务器
					localFIlePath = localConfig.getLocalHostInstallPath();
				}else{
//					return "FILETYPE ERROR";
				}
				if(file != null){
					//取得当前上传文件的文件名称
					String myFileName = file.getOriginalFilename();
					//如果名称不为“”,说明该文件存在，否则说明该文件不存在
					if(!StringUtils.isEmpty(myFileName.trim())){
						System.out.println(myFileName);
						//重命名上传后的文件名
						String fileName = "" + file.getOriginalFilename();
						//定义上传路径
						String source = FileUploadUtil.check(localFIlePath +timestamp);
						File temFile = new File(source);
						temFile.mkdirs();
						localPath = localFIlePath +timestamp+"/"+ fileName;
						localPath = FileUploadUtil.check(localPath);
						File localFile = new File(localPath);
						file.transferTo(localFile);
					}
				}
				//记录上传该文件后的时间
				int finaltime = (int) System.currentTimeMillis();
				System.out.println(finaltime - pre);
				//是否需要转存-----------所有包在使用的时候转存
//				if(localConfig.isTransferNeeded()&&!"hostCom".equals(fileType)){
//					sendFile(localPath,fileType,timestamp);
//				}
				
			}
			
		}
//		return "success";
	}
	
	private String sendFile(String localPath,String fileType,String remotePath){
		//fileType:baseImage/app/library/dbDriver/deploy
		localPath = localPath.replace("\\", "/");
		String registryFIlePath = "";
		if("baseImage".equals(fileType)){
			registryFIlePath = tmpHostConfig.getBaseImagePath();
		}else if("app".equals(fileType)){
			registryFIlePath = tmpHostConfig.getAppPath();
		}else if("library".equals(fileType)){
			registryFIlePath = tmpHostConfig.getLibraryPath();
		}else if("dbDriver".equals(fileType)){
//			registryFIlePath = tmpHostConfig.getDbDriverPath();
		}else if("deploy".equals(fileType)){
			registryFIlePath = tmpHostConfig.getDeployPath();
		}else{
			return "FILETYPE ERROR";
		}
		String result = "FALSE";
		List<RegistryLb> list = registryLbService.getRegistryLb(null);
		for(RegistryLb lb :list){
			Host host = hostService.getHost(lb.getLbHostId());
			SSH ssh = new SSH(host.getHostIp(), host.getHostUser(),
					Encrypt.decrypt(host.getHostPwd(), localConfig.getSecurityPath()));
			if (ssh.connect()) {
				String comadString ="mkdir -p " +registryFIlePath+ remotePath;
				try {
					ssh.execute(comadString);
					ssh.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				ssh.connect();
				boolean pushSuccess = ssh.SCPFile(localPath,registryFIlePath + remotePath + "/");
				ssh.close();
				if (pushSuccess) {
					result = "TRUE";
				}
			} else {
				result = "CONNERROR";
			}
		}
		deleteFile(localPath);
		return result;
	}

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年3月31日
	*/
	private boolean deleteFile(String localPath) {
		// TODO Auto-generated method stub
		File file = null;
		try {
			file = new File(localPath);
			if (file.exists())
				file.delete();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@RequestMapping("/toUpload"	) 
	public String toUpload() {
		
		return "/upload";
	}
	
}