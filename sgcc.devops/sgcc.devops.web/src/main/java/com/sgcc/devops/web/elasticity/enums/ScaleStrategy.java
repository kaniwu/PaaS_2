package com.sgcc.devops.web.elasticity.enums;

/**
 * 步长策略枚举
 * 
 * @author dmw
 *
 */
public enum ScaleStrategy {

	FIXED_STEP, // 固定步长
	AUTO_ADAPT;// 自动适配
}
