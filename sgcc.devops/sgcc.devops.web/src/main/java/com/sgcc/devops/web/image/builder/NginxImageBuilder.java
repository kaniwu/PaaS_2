//package com.sgcc.devops.web.image.builder;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.UUID;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Repository;
//import org.springframework.util.StringUtils;
//
//import com.sgcc.devops.common.bean.Result;
//import com.sgcc.devops.common.config.DeployConfig;
//import com.sgcc.devops.common.util.SSH;
//import com.sgcc.devops.web.image.Encrypt;
//import com.sgcc.devops.web.image.ImageBuilder;
//import com.sgcc.devops.web.image.NginxConstants;
//import com.sgcc.devops.web.image.NginxMeta;
//import com.sgcc.devops.web.image.material.DockerFileMaterial;
//import com.sgcc.devops.web.image.material.ImageMaterial;
//import com.sgcc.devops.web.image.material.NginxMaterial;
//
///**
// * nginx镜像生成器
// * @author dmw
// *
// */
//@Repository
//public class NginxImageBuilder extends Builder implements ImageBuilder {
//
//	private static Logger logger = Logger.getLogger(NginxImageBuilder.class);
//
//	@Autowired
//	private DeployConfig deployConfig;
//	@Autowired
//	private DockerfileBuilder dockerfileBuilder;
//	@Autowired
//	private NginxFileBuilder nginxFileBuilder;
//
//	@Override
//	public Result build(ImageMaterial material) {
//		logger.info("开始制作nginx镜像");
//		if (null == material) {
//			logger.error("镜像制作材料缺失");
//			return new Result(false, "镜像制作材料缺失！！！");
//		}
//		String configFileDirectory = UUID.randomUUID().toString();// 配置文件在平台主机上的临时存放位置
//		String localPath = deployConfig.getImageTempPath() + "/" + configFileDirectory;
//		// 生成 nginx.conf
//		NginxMeta meta = new NginxMeta(material.getAppName(), material.getDnsName(), material.getServers(),material.getPort());
//		List<NginxMeta> metas = new ArrayList<NginxMeta>();
//		metas.add(meta);
//		NginxMaterial nginxMaterial = new NginxMaterial(metas);
//		nginxMaterial.setDirectory(localPath);
//		nginxFileBuilder.init(nginxMaterial, deployConfig);
//		File configFile = nginxFileBuilder.build();
//		if (null == configFile) {
//			logger.error("build nginx.cnnf error!!");
//			return new Result(false, "生成nginx.conf文件失败！");
//		}
//		String exposePorts = "";
//		if (StringUtils.hasLength(material.getBaseImage().getImagePort())) {
//			exposePorts = "EXPOSE " + material.getBaseImage().getImagePort();
//		}
//		String baseImage = material.getBaseImage().getImageName() + ":" + material.getBaseImage().getImageTag();
//		// 生成Dockerfile
//		DockerFileMaterial dockerFileMaterial = new DockerFileMaterial(material.getAppTag(), baseImage, "Beyondcent",
//				"nginx.conf", NginxConstants.CONFIG_PATH, null, exposePorts,null);
//		dockerFileMaterial.setDirectory(localPath);
//		dockerFileMaterial.setCmdCommand(NginxConstants.START_CMD);
//		dockerFileMaterial.setFileAppendMap(new HashMap<String, String>());
//
//		dockerfileBuilder.init(dockerFileMaterial, deployConfig);
//		File dockerFile = dockerfileBuilder.build();
//		if (null == dockerFile) {
//			return new Result(false, "制作Dockerfile文件失败！");
//		}
//		SSH ssh = new SSH(material.getRegHost().getHostIp(), material.getRegHost().getHostUser(),
//				Encrypt.decrypt(material.getRegHost().getHostPwd(), deployConfig.getSecurityPath()));
//		if (!ssh.connect()) {// 没有链接，返回出错
//			return new Result(false, "连接主机失败！");
//		}
//		localPath = localPath.replace("\\", "/");
//		boolean pushSuccess = ssh.SCPDirectory(localPath + "/", material.getPath());
//		ssh.close();
//		deleteFile(localPath);
//		if (!pushSuccess) {// 将dockerfile等文件成功上传到仓库所在的主机上
//			logger.error("转储配置文件失败！");
//			return new Result(false, "转储配置文件失败！");
//		}
//		// 制作镜像
//		String tempImageName = "localhost:" + material.getRegPort() + "/" + (material.getAppName()+"-nginx").toLowerCase() + ":"
//				+ material.getAppTag();
//		String buildCommand = "docker build -t " + tempImageName + " " + material.getPath();
//		String uuid = null;
//		try {
//			ssh.connect();
//			String result = ssh.executeWithResult(buildCommand);
//			logger.info("Make image result:" + result);
//			Result makeResult = parseMakeResult(result);
//			if (!makeResult.isSuccess()) {
//				logger.error("镜像制作失败！");
//				return makeResult;
//			} else {
//				logger.info("镜像制作成功！");
//				uuid = makeResult.getMessage();
//			}
//		} catch (Exception e) {
//			logger.info("Make image fail");
//			return new Result(false, "制作镜像失败：脚本执行异常！");
//		} finally {
//			ssh.close();
//		}
//		// 上传镜像
//		String pushCommand = "docker push " + tempImageName;
//		ssh.connect();
//		try {
//			String result = ssh.executeWithResult(pushCommand);
//			logger.info("Push image result:" + result);
//			ssh.close();
//			ssh.connect();
//			ssh.execute("docker rmi " + uuid);
//			if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
//				logger.error("镜像发布失败！");
//				return new Result(false, "镜像发布脚本执行失败！");
//			} else {
//				logger.info("镜像发布成功！");
//				return new Result(true, uuid);
//			}
//		} catch (Exception e) {
//			logger.error("Push image fail"+ e);
//			return new Result(false, "发布镜像失败：发布镜像脚本执行异常！"+e);
//		}
//	}
//
//}
