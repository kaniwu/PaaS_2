package com.sgcc.devops.web.image.builder;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.web.image.ConfigFile;
import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.ImageBuilder;
import com.sgcc.devops.web.image.WeblogicContstants;
import com.sgcc.devops.web.image.material.ConfigFileMaterial;
import com.sgcc.devops.web.image.material.DockerFileMaterial;
import com.sgcc.devops.web.image.material.ImageMaterial;
import com.sgcc.devops.web.image.material.JdbcFileMaterial;
import com.sgcc.devops.web.task.TaskCache;
import com.sgcc.devops.web.task.TaskMessage;

/**
 * weblogic应用镜像生成器
 * 
 * @author dmw
 *
 */
@Repository("weblogicImageBuilder")
public class WeblogicImageBuilder extends Builder implements ImageBuilder {

	private static Logger logger = Logger.getLogger(WeblogicImageBuilder.class);
	@Autowired
	private TmpHostConfig tmpHostConfig;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private JdbcFileBuilder jdbcFileBuilder;
	@Autowired
	private ConfigFileBuilder configFileBuilder;
	@Autowired
	private DockerfileBuilder dockerfileBuilder;
	@Autowired
	private TaskCache taskCache;
	@Override
	public Result build(ImageMaterial material) {
		logger.info("开始制作weblogic应用镜像");
		if (null == material) {
			logger.error("镜像制作材料缺失！！！");
			return new Result(false, "镜像制作材料缺失！！！");
		}
		if (null == material.getDataSources() || material.getDataSources().isEmpty()) {
			logger.error("应用数据源信息缺失！");
			return new Result(false, "应用数据源信息缺失！");
		}
		StringBuffer taskMessage = material.getTaskMessage();
		//转存主机
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		SSH ssh = new SSH(ip,user,pwd);
		//新上传配置文件地址
		String localPath = localConfig.getLocalDeployPath()+material.getSystemId()+"/"+ material.getTimestamp() + "/";
		
		String remoteDir = material.getPath();
		StringBuffer commandStr=new StringBuffer();
		boolean unId = false;String successResult="";
		//是否需要中转
		if(material.isTransferNeeded()){
			if (ssh.connect()) {
				StringBuffer mkdirCommand = new StringBuffer("mkdir -p "+remoteDir+"; ");
				mkdirCommand.append("mkdir -p "+remoteDir+"/dbDriver"+"; ");
				mkdirCommand.append("mkdir -p "+remoteDir+"/prelib"+"; ");
				try {
					String result = ssh.executeWithResult(mkdirCommand.toString());
					ssh.close();
					if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
						logger.error("文件中转时创建目录失败！"+result);
						return new Result(false, "文件中转时创建目录失败！"+result);
					}
				}catch (Exception e) {
					e.printStackTrace();
					logger.error("文件中转时创建目录失败！"+e);
					return new Result(false, "文件中转时创建目录失败！"+e);
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
			if (ssh.connect()) {
				boolean pushSuccess = ssh.SCPDirectory(localPath, remoteDir);
//								deleteFile(localPath);
				ssh.close();
				if (!pushSuccess) {// 将dockerfile等文件成功上传到仓库所在的主机上
					logger.error("转储配置文件失败！");
					return new Result(false, "转储配置文件失败！");
				}
			} else {
				logger.error("转储配置文件时仓库主机连接失败");
				return new Result(false, "转储配置文件时仓库主机连接失败");
			}
		}else{
			remoteDir=localPath;
		}
		if(material.getAppFileName().contains(".tar.gz")||material.getAppFileName().contains(".zip")){
			commandStr.append("cd "+remoteDir+";");
			if(material.getAppFileName().contains(".tar.gz")){
				commandStr.append("tar -zxvf "+material.getAppFileName());
			}
			//处理zip包,并将meterial中的appfilename修改去掉.zip
			if(material.getAppFileName().contains(".zip")){
				String appFileName = material.getAppFileName();
				appFileName = appFileName.substring(0, appFileName.length()-4);
				commandStr.append("mkdir "+appFileName+";");
				commandStr.append("unzip "+material.getAppFileName()+" -d "+appFileName);
			}
			try {
				ssh.connect();
				unId=ssh.execute(commandStr.toString());
				ssh.close();
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("应用文件传输失败: "+e.getMessage());
			}
			if(unId){
				try {
					ssh.connect();
					successResult=ssh.executeWithResult("cd "+remoteDir+";find "+remoteDir+"  -type d -name WEB-INF;");
					ssh.close();
					if(!successResult.contains("No such file or directory")){
						material.setAppFileName(successResult.split("/")[successResult.split("/").length-3]+"/");
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.info("失败: "+e.getMessage());
				}
			}
		}
		
		// 生成jdbc配置文件
		taskMessage.append("<br/>");
		taskMessage.append("生成jdbc配置文件...");
		TaskMessage jdbcTask = new TaskMessage(taskMessage.toString(), 15, material.getUserId(),material.getSystemId());
		taskCache.updateTask(jdbcTask);
		Map<String, String> jdbcMap = new HashMap<String, String>();// 存储配置文件路径和目标路径
		Map<String, String> driverMap = new HashMap<String, String>();// 存储驱动文件路径和目标路径
		for (DataSource dataSource : material.getDataSources()) {
			// 对数据源密码进行AES加密
			dataSource.setJdbcPassword(Encrypt.encrypt(dataSource.getJdbcPassword(), localConfig.getSecurityPath()));
			String jdbcFileName = "system_ds_" + material.getDataSources().indexOf(dataSource);
			jdbcMap.put(jdbcFileName, "jdbc/" + jdbcFileName + "-jdbc.xml");

			JdbcFileMaterial jdbcFileMaterial = new JdbcFileMaterial(jdbcFileName, dataSource);
			jdbcFileMaterial.setDirectory(localPath);

			jdbcFileBuilder.init(jdbcFileMaterial, localConfig);
			File jdbcFile = jdbcFileBuilder.build();
			if (null == jdbcFile) {
				logger.error("制作应用镜像jdbc.xml文件失败！");
				return new Result(false, "制作应用镜像jdbc.xml文件失败！");
			}
			if (ssh.connect()) {
				String localDBDriverPath =(localConfig.getLocalDBDriverPath() +"/"+ dataSource.getDriverPath()).replace("\\", "/").replace("//", "/");
				boolean success = ssh.CopyDirectory(localDBDriverPath,remoteDir, "/dbDriver");
				ssh.close();
				if (!success) {
					// 转储驱动文件失败
					return new Result(false, "转储数据库驱动包失败");
				} else {
					// 记录驱动文件的名称和目标路径，用来在制作镜像的时候把驱动包打入应用镜像中
//					driverMap.put(dataSource.getDriverPath(), WeblogicContstants.DRIVER_PATH);
					driverMap.put("dbDriver",WeblogicContstants.DRIVER_PATH);
				}
			} else {
				logger.error("转储数据库驱动时连接仓库主机失败！");
				return new Result(false, "转储数据库驱动时连接仓库主机失败！");
			}
		}
		Map<String, String> configFileMap = new HashMap<String, String>();// 存储驱动文件路径和目标路径
		for (ConfigFile configFile : material.getConfigFiles()) {
			configFileMap.put(configFile.getFileName(), configFile.getFilePath());
		}
		// 生成session共享配置文件
//		 TODO
//		 JdbcFileMaterial sessionShareMaterial = new
//		 JdbcFileMaterial("session_ds", material.getSessionShareSource());
//		 sessionShareMaterial.setDirectory(localPath);
//		 jdbcFileBuilder.init(sessionShareMaterial, deployConfig);
//		 File sessionShareFile = jdbcFileBuilder.build();
//		 if (null == sessionShareFile) {
//		 logger.error("制作应用镜像session共享文件失败！");
//		 return new Result(false, "制作应用镜像session共享文件失败！");
//		 }
		// 生成config.xml配置文件
		taskMessage.append("<br/>");
		taskMessage.append("生成config.xml配置文件...");
		TaskMessage configtask = new TaskMessage(taskMessage.toString(), 20, material.getUserId(),material.getSystemId());
		taskCache.updateTask(configtask);
		ConfigFileMaterial configFileMaterial = new ConfigFileMaterial(material.getAppName(), material.getAppFileName(),
				jdbcMap, material.isSharable());
		configFileMaterial.setDirectory(localPath);
		//如果文件包名是.tar结尾，则将.tar去掉。如果不是.tar结尾，则不用处理。
		String tmpAppFile = configFileMaterial.getAppFile();
		if (tmpAppFile.endsWith(".tar")){
			tmpAppFile = tmpAppFile.substring(0, tmpAppFile.length()-4);
			configFileMaterial.setAppFile(tmpAppFile);
		}
		configFileBuilder.init(configFileMaterial, localConfig);
		File configFile = configFileBuilder.build();
		if (null == configFile) {
			logger.error("制作应用镜像weblogic配置文件失败！");
			return new Result(false, "制作应用镜像weblogic配置文件失败！");
		}
		// 生成dockerfile文件
		taskMessage.append("<br/>");
		taskMessage.append("生成Dockerfile...");
		TaskMessage Dockerfile = new TaskMessage(taskMessage.toString(), 25, material.getUserId(),material.getSystemId());
		taskCache.updateTask(Dockerfile);
		
		String exposePorts = "";
		if (StringUtils.hasLength(material.getBaseImage().getImagePort())) {
			exposePorts = "EXPOSE " + material.getBaseImage().getImagePort();
		}
		String baseImage = material.getBaseImage().getImageName() + ":" + material.getBaseImage().getImageTag();
		//此处判断基础镜像是zip包，tar.gz包。若是上述类型的包，则去除baseImage的后缀
		String zipAppFile = material.getAppFileName();
		if (zipAppFile.endsWith(".zip")){
			zipAppFile = zipAppFile.substring(0, zipAppFile.length()-4);
		}
		
		DockerFileMaterial dockerFileMaterial = new DockerFileMaterial(material.getAppTag(), baseImage, "beyondcent",
				zipAppFile, WeblogicContstants.WAR_PATH, null, exposePorts,configFileMap);
		dockerFileMaterial.setDirectory(localPath);
		// dockerFileMaterial.setCmdCommand("");
		Map<String, String> appendMap = new HashMap<String, String>();
		/**
		 * 把临时目录下的所有jdbc配置文件全部添加到weblogic的jdbc配置文件路径
		 */
		for (String key : jdbcMap.keySet()) {
			appendMap.put(key + "-jdbc.xml", WeblogicContstants.JDBC_PATH);
		}
		// 把临时目录下的所有driver配置文件全部添加到weblogic的lib路径下
		appendMap.putAll(driverMap);
		// 添加weblogic的session 共享路径
		// appendMap.put("session_ds-jdbc.xml", WeblogicContstants.JDBC_PATH);
		// 添加weblogic的config.xml文件
		appendMap.put("config.xml", WeblogicContstants.CONFIG_PATH);
		dockerFileMaterial.setFileAppendMap(appendMap);
		dockerFileMaterial.setRunCommands(material.getRunCommands());
		dockerFileMaterial.setDockerfilePart(material.getDockerfile());
		dockerfileBuilder.init(dockerFileMaterial, localConfig);
		File dockerFile = dockerfileBuilder.build();
		if (null == dockerFile) {
			logger.error("制作Dockerfile文件失败！");
			return new Result(false, "制作Dockerfile文件失败！");
		}
		//是否需要中转
		if(material.isTransferNeeded()){
			if (ssh.connect()) {
				boolean pushSuccess = ssh.SCPDirectory(localPath + "/", remoteDir);
//						deleteFile(localPath);
				ssh.close();
				if (!pushSuccess) {// 将dockerfile等文件成功上传到仓库所在的主机上
					logger.error("转储配置文件失败！");
					return new Result(false, "转储配置文件失败！");
				}
			} else {
				logger.error("转储配置文件时仓库主机连接失败");
				return new Result(false, "转储配置文件时仓库主机连接失败");
			}
		}
		
		// 制作镜像
		String tempImageName = material.getImageName();
		String buildCommand = "sudo docker build -t " + tempImageName + " " + material.getPath();
		taskMessage.append("<br/>");
		taskMessage.append("执行制作镜像脚本："+buildCommand);
		TaskMessage dockerBuild = new TaskMessage(taskMessage.toString(), 30, material.getUserId(),material.getSystemId());
		taskCache.updateTask(dockerBuild);
		String uuid = null;
		try {
			if(ssh.connect()){
				String result = ssh.executeWithResult(buildCommand);
				logger.info("Make image result:" + result);
				Result makeResult = parseMakeResult(result);
				if (!makeResult.isSuccess()) {
					logger.error("镜像制作失败:"+makeResult.getMessage()+";result="+result);
					return new Result(false, "制作镜像失败：脚本执行异常:"+result);
				} else {
					logger.info("镜像制作成功！");
					uuid = makeResult.getMessage();
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
		} catch (Exception e) {
			logger.info("Make image fail: "+e);
			return new Result(false, "制作镜像失败：脚本执行异常！"+e);
		} finally {
			ssh.close();
		}
		// 上传镜像
		String pushCommand = "sudo docker push " + tempImageName;
		taskMessage.append("<br/>");
		taskMessage.append("执行上传镜像到仓库脚本："+pushCommand);
		TaskMessage pushTask = new TaskMessage(taskMessage.toString(), 40, material.getUserId(),material.getSystemId());
		taskCache.updateTask(pushTask);
		
		material.setTaskMessage(taskMessage);
		
		ssh.connect();
		try {
			if(ssh.connect()){
				String result = ssh.executeWithResult(pushCommand);
				logger.info("Push image result:" + result);
				ssh.close();
				ssh.connect();
				ssh.execute("docker rmi " + uuid);
				if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
					logger.error("镜像推送脚本执行失败！"+result);
					return new Result(false, "镜像发布脚本执行失败！"+result);
				} else {
					// logger.info("镜像推送成功！");
					return new Result(true, uuid);
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
			
		} catch (Exception e) {
			logger.error("Push image fail" + e.getMessage());
			return new Result(false, "发布镜像失败：发布镜像脚本执行异常！");
		} finally {
			ssh.close();
		}
	}
}
