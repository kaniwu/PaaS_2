package com.sgcc.devops.web.image;

/**
 * 配置文件实体类
 * @author dmw
 *
 */
public class ConfigFile {
	private String fileName;//配置文件名称

	private String filePath;//配置文件路径

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public ConfigFile(String fileName, String filePath) {
		super();
		this.fileName = fileName;
		this.filePath = filePath;
	}
	
}