package com.sgcc.devops.web.monitor;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.config.MonitorConfig;

/**
 * 监控数据归档任务
 * @author dmw
 *
 */
@Component
public class MonitorDateFileTask {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private MonitorConfig monitorConfig;

	private static Logger logger = Logger.getLogger(MonitorDateFileTask.class);

	static long DAY = 24 * 60 * 60 * 1000;
	static long HOUR = 60 * 60 * 1000;

	/**
	 * 将1天前的监控数据转储到dop_monitor_file表中
	 */
	public void monitorDateFile() {
		String monitorFileDate = monitorConfig.getMonitorFileDate();
		String[] crons = monitorFileDate.split(" ");
		String count = null;
		if(crons[2].equals("*")){
			logger.warn("归档间隔太短");
			return;
		}
		if(crons[2].equals("0")){
			count = "24";
		}else{
			count = crons[2].split("/")[1];
		}
		Long frequency = Long.parseLong(count);
		Timestamp date = new Timestamp(System.currentTimeMillis());

		// 监控数据转储到dop_monitor_file表中
		dump(date, frequency);
		// 删除超出时间的监控数据
		deleteMonitor(date, frequency);
		// 删除dop_monitor_file中两倍数据归档时间前的数据
		deleteMonitorFile(date, frequency);

	}

	// 将超过时间的监控数据，存储在monitor_file
	public void dump(Timestamp date, long frequency) {
		final Timestamp d = new Timestamp(date.getTime() - frequency * HOUR);
		String sql = "Insert into "
				+ "dop_monitor_file(MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE) "
				+ "select MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE "
				+ "from dop_monitor where MONITOR_TIME <= ?";
		try {
			jdbcTemplate.update(sql, new PreparedStatementSetter() {
				@Override
				public void setValues(PreparedStatement ps) throws SQLException {
					ps.setTimestamp(1, d);
				}
			});
		} catch (Exception e) {
			logger.error("insert into monitor file fail: " + e.getMessage());
		}
	}

	// 删除dop_monitor中超过时间前的数据
	public void deleteMonitor(Timestamp date, long frequency) {
		final Timestamp d = new Timestamp(date.getTime() - frequency * HOUR);
		String sql = "delete from dop_monitor where MONITOR_TIME <= ?";
		try {
			jdbcTemplate.update(sql, new PreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps) throws SQLException {

					ps.setTimestamp(1, d);
				}
			});
		} catch (Exception e) {
			logger.error("delete monitor data fail: " + e.getMessage());
		}
	}

	// 删除dop_monitor_file中两倍数据归档时间前的数据
	public void deleteMonitorFile(Timestamp date, long frequency) {
		final Timestamp d = new Timestamp(date.getTime() - 2 * frequency * HOUR);
		String sqlDelete = "delete from dop_monitor_file where MONITOR_TIME <= ?";
		try {
			jdbcTemplate.update(sqlDelete, new PreparedStatementSetter() {

				@Override
				public void setValues(PreparedStatement ps) throws SQLException {

					ps.setTimestamp(1, d);
				}
			});
		} catch (Exception e) {
			logger.error("delete monitor data from monitor_file fail: " + e.getMessage());
		}
	}
}
