package com.sgcc.devops.web.scheduled;

import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.EmailCfg;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.ElasticStrategyService;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.elasticity.cache.ElasticCache;
import com.sgcc.devops.web.manager.ContainerManager;
import com.sgcc.devops.web.manager.UserManager;
import com.sgcc.devops.web.task.ElasticTask;

/**
 * 弹性伸缩定时任务,默认30分钟执行一次，可以手动配置
 * 
 * @author dmw
 *
 */
@Repository
public class ElasticScheduler {

	private static Logger logger = Logger.getLogger(ElasticScheduler.class);
	@Autowired
	private SystemService systemService;
	@Autowired
	private MonitorService monitorService;
	@Autowired
	private ElasticStrategyService elasticStrategyService;
	@Autowired
	private ElasticCache elasticCache;
	@Autowired
	private UserManager userManager;
	@Autowired
	private EmailCfg emailCfg;
	@Autowired
	private DeployService deployService;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private ContainerManager containerManager;
	
	@Scheduled(cron = "0/10 * * * * ?")
	public void elastic_10() {
		try {
			System system = new System();
			system.setIsAllAuth(true);
			List<System> systems = systemService.selectAllSystem(system);
			if (null == systems ||systems.isEmpty()) {
				return;
			}
			ExecutorService executor = Executors.newFixedThreadPool(10);
			CompletionService<Result> completionService = new ExecutorCompletionService<>(executor);
			logger.debug("create elastic task....");
			int taskNum = 0;
			for (System  system2: systems) {
				Deploy deploy = deployService.getDeploy(system2.getDeployId());
				if (null == system2.getSystemElsasticityStatus() || system2.getSystemElsasticityStatus() == 0) {
					continue;
				}
				List<Container> containers = containerService.getBySysId(system2.getSystemId());
				if (containers.isEmpty()) {
					continue;
				}
				User user = userManager.detail(deploy.getCreator());
				String mail = "";
				if (null != user) {
					mail = user.getUserMail();
				}
				ElasticStrategy strategy = elasticStrategyService.load(system2.getSystemId());
				if (strategy == null||!"10".equals(strategy.getExtendIntervals())) {
					continue;
				}
				logger.info("-------strategy step:"+strategy.getStepSize());
				ElasticTask task = new ElasticTask(system2.getSystemId(),containers, monitorService,strategy, 
						elasticCache, mail, emailCfg, systemService, containerManager);
				completionService.submit(task);
				taskNum++;
			}
			executor.shutdown();
			int index = 0;
			while (index < taskNum) {
				Future<Result> future = completionService.take();
				if (future.get().isSuccess()) {
					logger.info("Elastic Succeed!");
				} else {
					logger.warn("Elastic Failed:" + future.get().getMessage());
				}
				index++;
			}
		} catch (Exception e) {
			logger.error("The Elastic Task Occurred Exception:", e);
		}
	}
	@Scheduled(cron = "0/30 * * * * ?")
	public void elastic_30() {
		try {
			System system = new System();
			system.setIsAllAuth(true);
			List<System> systems = systemService.selectAllSystem(system);
			if (null == systems ||systems.isEmpty()) {
				return;
			}
			ExecutorService executor = Executors.newFixedThreadPool(10);
			CompletionService<Result> completionService = new ExecutorCompletionService<>(executor);
			logger.debug("create elastic task....");
			int taskNum = 0;
			for (System  system2: systems) {
				Deploy deploy = deployService.getDeploy(system2.getDeployId());
				if (null == system2.getSystemElsasticityStatus() || system2.getSystemElsasticityStatus() == 0) {
					continue;
				}
				List<Container> containers = containerService.getBySysId(system2.getSystemId());
				if (containers.isEmpty()) {
					continue;
				}
				User user = userManager.detail(deploy.getCreator());
				String mail = "";
				if (null != user) {
					mail = user.getUserMail();
				}
				ElasticStrategy strategy = elasticStrategyService.load(system2.getSystemId());
				if (strategy == null||!"30".equals(strategy.getExtendIntervals())) {
					continue;
				}
				logger.info("-------strategy step:"+strategy.getStepSize());
				ElasticTask task = new ElasticTask(system2.getSystemId(),containers, monitorService,strategy, 
						elasticCache, mail, emailCfg, systemService, containerManager);
				completionService.submit(task);
				taskNum++;
			}
			executor.shutdown();
			int index = 0;
			while (index < taskNum) {
				Future<Result> future = completionService.take();
				if (future.get().isSuccess()) {
					logger.info("Elastic Succeed!");
				} else {
					logger.warn("Elastic Failed:" + future.get().getMessage());
				}
				index++;
			}
		} catch (Exception e) {
			logger.error("The Elastic Task Occurred Exception:", e);
		}
	}
	@Scheduled(cron = "0 0/1 * * * ?")
	public void elastic_60() {
		try {
			System system = new System();
			system.setIsAllAuth(true);
			List<System> systems = systemService.selectAllSystem(system);
			if (null == systems ||systems.isEmpty()) {
				return;
			}
			ExecutorService executor = Executors.newFixedThreadPool(10);
			CompletionService<Result> completionService = new ExecutorCompletionService<>(executor);
			logger.debug("create elastic task....");
			int taskNum = 0;
			for (System  system2: systems) {
				Deploy deploy = deployService.getDeploy(system2.getDeployId());
				if (null == system2.getSystemElsasticityStatus() || system2.getSystemElsasticityStatus() == 0) {
					continue;
				}
				List<Container> containers = containerService.getBySysId(system2.getSystemId());
				if (containers.isEmpty()) {
					continue;
				}
				User user = userManager.detail(deploy.getCreator());
				String mail = "";
				if (null != user) {
					mail = user.getUserMail();
				}
				ElasticStrategy strategy = elasticStrategyService.load(system2.getSystemId());
				if (strategy == null||!"60".equals(strategy.getExtendIntervals())) {
					continue;
				}
				logger.info("-------strategy step:"+strategy.getStepSize());
				ElasticTask task = new ElasticTask(system2.getSystemId(),containers, monitorService,strategy, 
						elasticCache, mail, emailCfg, systemService, containerManager);
				completionService.submit(task);
				taskNum++;
			}
			executor.shutdown();
			int index = 0;
			while (index < taskNum) {
				Future<Result> future = completionService.take();
				if (future.get().isSuccess()) {
					logger.info("Elastic Succeed!");
				} else {
					logger.warn("Elastic Failed:" + future.get().getMessage());
				}
				index++;
			}
		} catch (Exception e) {
			logger.error("The Elastic Task Occurred Exception:", e);
		}
	}
	@Scheduled(cron = "0 0/5 * * * ?")
	public void elastic_300() {
		try {
			System system = new System();
			system.setIsAllAuth(true);
			List<System> systems = systemService.selectAllSystem(system);
			if (null == systems ||systems.isEmpty()) {
				return;
			}
			ExecutorService executor = Executors.newFixedThreadPool(10);
			CompletionService<Result> completionService = new ExecutorCompletionService<>(executor);
			logger.debug("create elastic task....");
			int taskNum = 0;
			for (System  system2: systems) {
				Deploy deploy = deployService.getDeploy(system2.getDeployId());
				if (null == system2.getSystemElsasticityStatus() || system2.getSystemElsasticityStatus() == 0) {
					continue;
				}
				List<Container> containers = containerService.getBySysId(system2.getSystemId());
				if (containers.isEmpty()) {
					continue;
				}
				User user = userManager.detail(deploy.getCreator());
				String mail = "";
				if (null != user) {
					mail = user.getUserMail();
				}
				ElasticStrategy strategy = elasticStrategyService.load(system2.getSystemId());
				if (strategy == null||!"300".equals(strategy.getExtendIntervals())) {
					continue;
				}
				logger.info("-------strategy step:"+strategy.getStepSize());
				ElasticTask task = new ElasticTask(system2.getSystemId(),containers, monitorService,strategy, 
						elasticCache, mail, emailCfg, systemService, containerManager);
				completionService.submit(task);
				taskNum++;
			}
			executor.shutdown();
			int index = 0;
			while (index < taskNum) {
				Future<Result> future = completionService.take();
				if (future.get().isSuccess()) {
					logger.info("Elastic Succeed!");
				} else {
					logger.warn("Elastic Failed:" + future.get().getMessage());
				}
				index++;
			}
		} catch (Exception e) {
			logger.error("The Elastic Task Occurred Exception:", e);
		}
	}
}
