package com.sgcc.devops.web.scheduled;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;

/**
 * 日志清理定时任务
 * @author dmw
 *
 */
@Component
public class CleanLogSchedule {

	@Autowired
	private LocalConfig localConfig;
	
	/**
	 * 每天0:0:0,进行下载日志文件删除
	 */
	// 秒 分 时 天 月 周 年
	@Scheduled(cron = "0 0 0 * * ?")
	protected void execute() {
		String logLocalPath = localConfig.getLocalLogPath();
		logLocalPath = FileUploadUtil.check(logLocalPath);
		File file = new File(logLocalPath);
		//判断文件或目录是否存在
		if(file.exists()){
			if(file.isDirectory()){
				//递归删除当前文件目录下的所有文件及目录
				deleteFile(file);
			}else{
				//删除文件
				file.delete();
			}
		}
		
		//创建目录
		file.mkdirs();
	}
	
	protected void deleteFile(File file){
		File[] files = file.listFiles();
		if (null == files) {
			return;
		}
		for (File f : files) {
			//判断是否是目录
			if(f.isDirectory()){
				//递归
				deleteFile(f);
			}else{
				//删除文件
				f.delete();
			}
		}
		//删除目录
		file.delete();
	}
}
