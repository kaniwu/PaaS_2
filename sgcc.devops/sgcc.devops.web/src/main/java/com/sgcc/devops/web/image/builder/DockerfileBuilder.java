package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.material.DockerFileMaterial;

/**
 * DockerFile创建器
 * @author dmw
 *
 */
@Repository("dockerfileBuilder")
public class DockerfileBuilder implements FileBuilder {

	private static Logger logger = Logger.getLogger(DockerfileBuilder.class);
	private static final String FILE_NAME = "Dockerfile.vm";
	private LocalConfig localConfig;
	private DockerFileMaterial material;

	private enum ParamPosition {
		VERSION, BASE_IMAGE, MAINTAINER, APP_FILE, APP_TARGET, ADD_APPEND,CONFIG_FILE, CMD_COMMAND, EXPOSE_PORT,RUN_COMMAND,DOCKERFILE_PART;
	}

	public void init(DockerFileMaterial material, LocalConfig localConfig) {
		this.material = material;
		this.localConfig = localConfig;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}
	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	public DockerFileMaterial getMaterial() {
		return material;
	}

	public void setMaterial(DockerFileMaterial material) {
		this.material = material;
	}

	@Override
	public File build() {
		if (null == material) {
			logger.error("Dockerfile material is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, localConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();

		// 组合配置信息
		context.put(ParamPosition.VERSION.name(), material.getVersion());
		context.put(ParamPosition.BASE_IMAGE.name(), material.getBaseImage());
		context.put(ParamPosition.MAINTAINER.name(), material.getMaintainer());
		context.put(ParamPosition.APP_FILE.name(), material.getAppFileName());
		context.put(ParamPosition.APP_TARGET.name(), material.getAppFileTarget());
		context.put(ParamPosition.ADD_APPEND.name(), material.getFileAppendMap());
		context.put(ParamPosition.CONFIG_FILE.name(), material.getConfigFileMap());
		context.put(ParamPosition.EXPOSE_PORT.name(), material.getExposePorts());
		context.put(ParamPosition.RUN_COMMAND.name(), material.getRunCommands());
		context.put(ParamPosition.DOCKERFILE_PART.name(), material.getDockerfilePart()==null?"":material.getDockerfilePart());
		// context.put(ParamPosition.CMD_COMMAND.name(),
		// material.getCmdCommand());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get dockerfile template infos error", e);
			return null;
		}
		// 生成新的配置文件
		String localPath = material.getDirectory();
		localPath = FileUploadUtil.check(localPath);
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = localPath.replace("\\", "/");
		File file = new File(localPath + "/Dockerfile");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build dockerfile exception:", e);
				return null;
			}
		}
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			writer.close();
			return file;
		} catch (IOException e) {
			logger.error("fild file exception:", e);
			return null;
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
}
