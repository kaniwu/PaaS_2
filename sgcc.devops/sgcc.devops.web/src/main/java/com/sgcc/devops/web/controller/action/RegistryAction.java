/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RegIdImageTypeModel;
import com.sgcc.devops.common.model.RegistryLbDetailModel;
import com.sgcc.devops.common.model.RegistryLbModel;
import com.sgcc.devops.common.model.RegistryModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.web.manager.CompPortManager;
import com.sgcc.devops.web.manager.RegistryManager;
import com.sgcc.devops.web.query.QueryList;

/**
 * date：2016年2月4日 下午1:49:16 project name：sgcc.devops.web
 * 
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegistryAction.java description：镜像仓库操作访问控制类
 */
@Controller
@RequestMapping("registry")
public class RegistryAction {

	Logger logger = Logger.getLogger(RegistryAction.class);

	@Resource
	private QueryList queryList;
	@Resource
	private RegistryService registryService;
	@Resource
	private RegistryManager registryManager;
	@Resource
	private PortService portService;
	@Resource
	private HostService hostService;
	@Resource
	private ComponentService componentService;
	@Resource
	private CompPortManager compPortManager;

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 10:24
	 * @description 将原来返回字符串的方法修改为GridBean的方式
	 */
	@RequestMapping("/list")
	@ResponseBody
	public GridBean list(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int pagenumber,
			@RequestParam(value = "rows", required = true) int pagesize,
			RegistryModel registryModel) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			return queryList.registryList(user.getUserId(), pagenumber,
					pagesize, registryModel);
		} catch (Exception e) {
			logger.error("查询仓库列表失败: " + e.getMessage());
			return null;
		}
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月6日 10:24
	 * @description 将原来返回字符串的方法修改为GridBean的方式
	 */
	@RequestMapping("/listWithIP")
	@ResponseBody
	public GridBean listWithIP(HttpServletRequest request,
			PagerModel pagerModel, RegistryLbModel RegistryLbModel) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			return queryList.registryListWithIP(user.getUserId(), pagerModel,
					RegistryLbModel);
		} catch (Exception e) {
			logger.error("查询带有物理机IP地址的仓库列表失败: " + e.getMessage());
			return null;
		}
	}

	/**
	 * @author youngtsinglin
	 * @time 2015年9月10日 11:32
	 * @description 将原来返回字符串的方法修改为GridBean的方式,列出包含在特定仓库下面所有的镜像信息
	 */
	@RequestMapping("/listslaveimages")
	@ResponseBody
	public GridBean listSlaveImages(HttpServletRequest request,
			PagerModel pagerModel, RegIdImageTypeModel regIdImageTypeModel) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			String registryid = request.getParameter("registry_id");
			/* 显示正常状态的镜像内容 */
			byte imagestatus = (byte) Status.IMAGE.NORMAL.ordinal();
			return queryList.registrySlaveImages(user.getUserId(), pagerModel,
					regIdImageTypeModel, registryid, imagestatus);
		} catch (Exception e) {
			logger.error("查询所包含镜像的列表失败: " + e.getMessage());
			return null;
		}
	}

	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, RegistryModel registryModel) {
		User user = (User) request.getSession().getAttribute("user");
		if (registryModel != null) {
			JSONObject params = JSONUtil.parseObjectToJsonObject(registryModel);
			/* 获取仓库的详细信息 */
			String registry_name = request.getParameter("registry_name");
			String registry_port = request.getParameter("registry_port");
			String registry_hostid = request.getParameter("registry_hostid");
			String registry_desc = request.getParameter("registry_desc");
			params.put("registryName", registry_name);
			params.put("registryPort", registry_port);
			params.put("hostId", registry_hostid);
			params.put("registryDesc", registry_desc);
			params.put("userId", user.getUserId());
			JSONObject jo = registryManager.createRegistry(params);
			boolean result = (boolean) jo.get("result");
			Result return_result = new Result(result, jo.getString("message"));
			request.setAttribute("logDetail", return_result.getMessage());
			request.setAttribute("success",
					return_result.isSuccess() ? "Success" : "Fail");
			return return_result;
		} else {
			Result return_result = new Result(false, "添加仓库,参数输入异常!");
			logger.info("添加仓库,参数输入异常!");
			request.setAttribute("logDetail", return_result.getMessage());
			request.setAttribute("success",
					return_result.isSuccess() ? "Success" : "Fail");
			return return_result;
		}
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, RegistryModel registryModel) {
		Result return_result = new Result();
		if (registryModel != null) {
			JSONObject params = JSONUtil.parseObjectToJsonObject(registryModel);
			/* 获取仓库的详细信息 */
			String registry_id = request.getParameter("registry_id");
			String registry_name = request.getParameter("registry_name");
			String registry_desc = request.getParameter("registry_desc");

			params.put("registryId", registry_id);
			params.put("registryName", registry_name);
			params.put("registryDesc", registry_desc);

			int result = registryService.updateRegistry(params);
			return_result = new Result(result > 0, result > 0 ? "修改仓库("
					+ registry_name + ")成功" : "修改仓库(" + registry_name + ")失败");
		} else {
			return_result = new Result(false, "修改仓库，参数输入异常!");
			logger.info("修改仓库，参数输入异常!");
		}
		request.setAttribute("logDetail", return_result.getMessage());
		request.setAttribute("success",
				return_result.isSuccess() ? "Success" : "Fail");
		return return_result;
	}

	@RequestMapping(value = "/registryHosts", method = { RequestMethod.GET })
	@ResponseBody
	public String clusterMasterList(HttpServletRequest request,
			RegistryModel registryModel) {
		JSONArray ja = queryList.getRegistryMster();
		return ja.toString();
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	public Result delete(
			HttpServletRequest request,
			@RequestParam(value = "registryIds", required = true) String registryIds) {
		String registry_names = request.getParameter("registryName");
		User user = (User) request.getSession().getAttribute("user");
		JSONObject json_object = new JSONObject();
		json_object.put("array", registryIds);
		json_object.put("name_array", registry_names);
		json_object.put("userId", user.getUserId());
		// String registry_name = request.getParameter("registryName");
		Result return_result = registryManager.deleteBatchRegistry(json_object);
		request.setAttribute("logDetail", return_result.getMessage());
		request.setAttribute("success", return_result.isSuccess() ? "Success"
				: "Fail");
		return return_result;
	}

	@RequestMapping(value = "/reachRegiHost", method = { RequestMethod.POST })
	@ResponseBody
	public Result reachRegiHost(HttpServletRequest request) {
		JSONObject params = new JSONObject();
		/* 获取仓库主机的IP地址 */
		String regihost_ip = request.getParameter("registry_ipaddr");
		/* 获取仓库主机的端口 */
		String regihost_port = request.getParameter("registry_port");
		User user = (User) request.getSession().getAttribute("user");
		params.put("userId", user.getUserId());
		params.put("registryIpaddr", regihost_ip);
		params.put("registryPort", regihost_port);

		/* 检查节点IP地址和端口的可达性 */
		Result result = registryManager.reachRegiHost(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");

		return result;
	}

	/* 增加数据库与实际注册服务器（仓库）之间的镜像同步操作 */
	@RequestMapping(value = "/syncRegiImgInfo", method = { RequestMethod.POST })
	@ResponseBody
	public Result syncRegiImgInfo(HttpServletRequest request) {
		JSONObject params = new JSONObject();
		/* 获取仓库主机的IP地址 */
		String regihost_ip = request.getParameter("registry_ipaddr");
		/* 获取仓库主机的端口 */
		String regihost_port = request.getParameter("registry_port");
		/* 获取仓库的ID值，同步时插入仓库镜像对应表中 */
		String registry_id = request.getParameter("registry_id");
		User user = (User) request.getSession().getAttribute("user");
		params.put("userId", user.getUserId());
		params.put("registryIpaddr", regihost_ip);
		params.put("registryPort", regihost_port);
		params.put("registryId", registry_id);

		/* 检查节点IP地址和端口的可达性 */
		Result result = registryManager.sycDBandRegiImgInfo(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}

	/**
	 * @author yangqinglin
	 * @time 2015年9月6日 9:38
	 * @description 增加处理查询仓库中包含的镜像列表内容
	 * 
	 */
	@RequestMapping(value = "/select", method = { RequestMethod.POST })
	@ResponseBody
	public JSONObject getRegistry(HttpServletRequest request,
			@RequestParam String registryId) {
		Registry record = new Registry();
		record.setRegistryId(registryId);
		JSONObject jo = queryList.getRegistry(record);
		return jo;

	}

	@RequestMapping(value = "/getRegistryMster", method = { RequestMethod.POST })
	@ResponseBody
	public String getRegistryMster(HttpServletRequest request) {
		JSONArray ja = queryList.getRegistryMster();
		return ja.toString();

	}

	/**
	 * 同时同步多个仓库中包含的镜像
	 * 
	 * @author youngtsinglin
	 * @param request
	 */
	@RequestMapping(value = "/sync/batch", method = { RequestMethod.POST })
	@ResponseBody
	public Result syncbatch(HttpServletRequest request) {
		String registry_ids = request.getParameter("regi_ids");
		String registry_names = request.getParameter("regi_names");
		String registry_ipaddrs = request.getParameter("regi_ipaddrs");
		String registry_ports = request.getParameter("regi_ports");

		if (null == registry_ids || registry_ids.isEmpty()) {
			Result ret_result = new Result(false, "批量同步仓库下镜像，传入参数为空！");
			request.setAttribute("logDetail", ret_result.getMessage());
			request.setAttribute("success", ret_result.isSuccess() ? "Success"
					: "Fail");
			return ret_result;
		} else {
			JSONObject json_object = new JSONObject();
			json_object.put("registry_ids", registry_ids);
			json_object.put("registry_names", registry_names);
			json_object.put("registry_ipaddrs", registry_ipaddrs);
			json_object.put("registry_ports", registry_ports);
			User user = (User) request.getSession().getAttribute("user");

			Result ret_result = registryManager
					.syncBatchRegiSlaveImg(json_object);
			logger.info(user.getUserName() + "" + ret_result.getMessage());
			request.setAttribute("logDetail", ret_result.getMessage());
			request.setAttribute("success", ret_result.isSuccess() ? "Success"
					: "Fail");
			return ret_result;
		}
	}

	@RequestMapping(value = "/remove/batch", method = { RequestMethod.POST })
	@ResponseBody
	public Result deletebatch(HttpServletRequest request) {
		String registry_ids = request.getParameter("regi_ids");
		String registry_names = request.getParameter("regi_names");
		User user = (User) request.getSession().getAttribute("user");
		JSONObject json_object = new JSONObject();
		json_object.put("array", registry_ids);
		json_object.put("name_array", registry_names);
		json_object.put("userId", user.getUserId());

		// int result = registryService.deleteRegistry(json_object);
		Result return_result = registryManager.deleteBatchRegistry(json_object);
		logger.info(user.getUserName() + ":" + return_result.getMessage());
		request.setAttribute("logDetail", return_result.getMessage());
		request.setAttribute("success", return_result.isSuccess() ? "Success"
				: "Fail");
		return return_result;
	}

	/**
	 * @author zs
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request,
			@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = registryManager.checkName(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}

	@RequestMapping(value = "/registryDaemon", method = { RequestMethod.GET })
	@ResponseBody
	public Result registryDaemon(
			HttpServletRequest request,
			@RequestParam(value = "registryId", required = true) String registryId,
			@RequestParam(value = "opration", required = true) String opration) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = registryManager.registryDaemon(registryId, opration,
				user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}

	@RequestMapping(value = "/createlb", method = { RequestMethod.POST })
	@ResponseBody
	public Result createlb(HttpServletRequest request,
			RegistryLbDetailModel registryLbDetailModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil
				.parseObjectToJsonObject(registryLbDetailModel);

		/* 获取仓库的详细信息 */
		/**
		 * reg_name: reg_name, 仓库名称 reg_loadtype: reg_loadtype,仓库负载类型 f5_Id:
		 * f5_Id, f5id nginx_Id: nginx_Id, 选择nginx的id nginx_port:nginx_port,
		 * nginx的端口 load_domain:load_domain,负载域名 load_ip:load_ip,负载主机ip
		 * load_port:load_port,负载端口 host_Id:host_Id,主机 host_port:host_port,主机端口
		 * standHost_Id:standHost_Id,备用主机ip standHost_port:standHost_port,备用主机端口
		 * reg_desc:reg_desc 仓库备注
		 */
		params.put("userId", user.getUserId());
		Result result = registryManager.createRegistrylb(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}

	@RequestMapping(value = "/registryLbDetails", method = { RequestMethod.POST })
	@ResponseBody
	public JSONObject registryLbDetails(HttpServletRequest request,
			@RequestParam(value = "id", required = true) String id) {
		RegistryLb registryLb = registryManager.registryLbDetails(id);
		JSONObject params = JSONUtil.parseObjectToJsonObject(registryLb);
		List<Registry> registrys = registryManager
				.selectRegistryByLbId(registryLb.getId());
		for (Registry registry : registrys) {
			Host host = new Host();
			host.setHostId(registry.getHostId());
			Host host2 = hostService.getHost(host);
			if (registry.getHostId().equals(registryLb.getLbHostId())) {
				params.put("host_state", registry.getRegistryStatus());
				params.put("host_port", registry.getRegistryPort());
				params.put("host_ip", host2.getHostIp());
				params.put("host_id", registry.getRegistryId());
			} else {
				params.put("stand_state", registry.getRegistryStatus());
				params.put("stand_port", registry.getRegistryPort());
				params.put("stand_ip", host2.getHostIp());
				params.put("stand_ip", host2.getHostIp());
				params.put("stand_id", registry.getRegistryId());
			}
		}
		Component component = new Component();
		component.setComponentId(registryLb.getComponentId());
		component =componentService.getComponent(component);
		params.put("component_name", component.getComponentName());
		return params;
	}

	@RequestMapping(value = "/registryLbDelete", method = { RequestMethod.POST })
	@ResponseBody
	public Result registryLbDelete(HttpServletRequest request,
			@RequestParam(value = "id", required = true) String id) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = registryManager.registryLbDelete(id, user.getUserId());
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}

	@RequestMapping(value = "/changeRegistry", method = { RequestMethod.GET })
	@ResponseBody
	public Result changeRegistry(
			HttpServletRequest request,
			@RequestParam(value = "registryId", required = true) String registryId,
			@RequestParam(value = "opration", required = true) String opration) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = registryManager.registryDaemon(registryId, opration,
				user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}

	@RequestMapping(value = "/editRegistryLb", method = { RequestMethod.POST })
	@ResponseBody
	public Result editRegistryLb(HttpServletRequest request ,RegistryLbDetailModel registryLbDetailModel) {
		Result result =new Result();
		RegistryLb registryLb = new RegistryLb();
		registryLb.setId(registryLbDetailModel.getId());
		registryLb.setDomainName(registryLbDetailModel.getLoad_domain());
		registryLb.setRegistryDesc(registryLbDetailModel.getReg_desc());
		registryLb.setRegistryName((registryLbDetailModel.getReg_name()));
		boolean isok  =registryManager.updateRegistrylb(registryLb);
		if(isok){
			result.setSuccess(true);
			result.setMessage("更新成功");
		}else{
			result.setSuccess(true);
			result.setMessage("更新失败");
		}
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess() ? "Success" : "Fail");
		return result;
	}
	@RequestMapping(value = "/changeRegistryLb", method = { RequestMethod.POST })
	@ResponseBody
	public Result changeRegistryLb(
			HttpServletRequest request,
			@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "change_id", required = true) String change_id,
			@RequestParam(value = "change_port", required = true) String change_port,
			@RequestParam(value = "registryId", required = true) String registryId,
			@RequestParam(value = "dockerIns", required = true) String dockerIns,
			@RequestParam(value = "registryIns", required = true) String registryIns
			) {
		User user = (User) request.getSession().getAttribute("user");
		//创建一个新的仓库
//		String change_id =(String)request.getAttribute("change_id");
//		String change_port =(String)request.getAttribute("change_port");
		JSONObject params = new JSONObject();
		params.put("registryName", id);
		params.put("registryPort", change_port);
		params.put("hostId", change_id);
		params.put("userId", user.getUserId());
		params.put("LbId", id);
		params.put("dockerIns", dockerIns);
		params.put("registryIns", registryIns);
		params.put("registryDesc", "备用主机");
		JSONObject json =registryManager.createRegistry(params);
		if(!(boolean)json.get("result")){
			request.setAttribute("logDetail", json.get("message"));
			request.setAttribute("success", "Fail");
		}
		//删除原来的仓库
//		String registryId =(String)request.getAttribute("registryId");
		RegistryLb lb = registryManager.registryLbDetails(id);
		if (org.apache.commons.lang.StringUtils.isEmpty(registryId)) {
			Result result = new Result();
//			CompPort record = new CompPort();
//			record.setUsedId(id);
//			List<CompPort> compPortsIn = portService.selectAllCompPort(record);
//			Result result1 =compPortManager.updateCompPort(compPortsIn, id, Type.COMPPORT_TYPE.REGISTRY.toString(),lb.getLbType());
			
			Component component = new Component();
			component.setComponentId(lb.getComponentId());
			component = componentService.getComponent(component);
			if(component==null){
				logger.error("负载信息获取失败未更新");
				return new Result(false, "负载信息获取失败未更新");
			}
			//单个nginx组件
			try {
				if(component.getComponentType()==1){
					Result res = compPortManager.modifyNginxConfig(lb.getComponentId(),lb.getLbPort().toString(),lb.getId(),Type.COMPPORT_TYPE.REGISTRY.toString());
					if(!res.isSuccess()){
						return res;
					}
				}
				//nginx组组件
				if(component.getComponentType()==6){
					Result res = compPortManager.modifyNginxConfig(lb.getComponentId(),lb.getLbPort().toString(),lb.getId(),Type.COMPPORT_TYPE.REGISTRY.toString());
					if(!res.isSuccess()){
						return res;
					}
				}
				//f5组件
				if(component.getComponentType()==2){
					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
//			if(!result.isSuccess()){
//				request.setAttribute("success", "Fail");
//				request.setAttribute("logDetail","更新nginx配置失败");
//				return result1;
//			}
			request.setAttribute("logDetail", "更换成功");
			request.setAttribute("success",   "Success");
			return result;
		}
		String registryIds = registryId;
		String registry_names = registryIds;
		JSONObject json_object = new JSONObject();
		json_object.put("array", registryIds);
		json_object.put("name_array", registry_names);
		json_object.put("userId", user.getUserId());
		Result return_result = registryManager.deleteBatchRegistry(json_object);
		if(return_result.isSuccess()){
			registryManager.insertRegistry(params);
		}
//		CompPort record = new CompPort();
//		record.setUsedId(id);
//		List<CompPort> compPortsIn = portService.selectAllCompPort(record);
//		Result result =compPortManager.updateCompPort(compPortsIn, id, Type.COMPPORT_TYPE.REGISTRY.toString(),lb.getLbType());
		
//		if(!result.isSuccess()){
//			request.setAttribute("success", "Fail");
//			request.setAttribute("logDetail","更新nginx配置失败");
//			return return_result;
//		}
		Component component = new Component();
		component.setComponentId(lb.getComponentId());
		component = componentService.getComponent(component);
		if(component==null){
			logger.error("负载信息获取失败未更新");
			return new Result(false, "负载信息获取失败未更新");
		}
		//单个nginx组件
		try {
			if(component.getComponentType()==1){
				Result res = compPortManager.modifyNginxConfig(lb.getComponentId(),lb.getLbPort().toString(),lb.getId(),Type.COMPPORT_TYPE.REGISTRY.toString());
				if(!res.isSuccess()){
					request.setAttribute("logDetail", res.getMessage());
					request.setAttribute("success", res.isSuccess() ? "Success" : "Fail");
					return res;
				}
			}
			//nginx组组件
			if(component.getComponentType()==6){
				Result res = compPortManager.modifyNginxConfig(lb.getComponentId(),lb.getLbPort().toString(),lb.getId(),Type.COMPPORT_TYPE.REGISTRY.toString());
				if(!res.isSuccess()){
					request.setAttribute("logDetail", res.getMessage());
					request.setAttribute("success", res.isSuccess() ? "Success" : "Fail");
					return res;
				}
			}
			//f5组件
			if(component.getComponentType()==2){
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return_result.setMessage("更新成功");
		return_result.setSuccess(true);
		request.setAttribute("logDetail", return_result.getMessage());
		request.setAttribute("success", return_result.isSuccess() ? "Success" : "Fail");
		return return_result;
	}
	@RequestMapping(value = "/getAllRegistryLb", method = { RequestMethod.GET })
	@ResponseBody
	public List<RegistryLb> getAllRegistryLb(HttpServletRequest request) {
		List<RegistryLb>  registryLbs= registryManager.getAllRegistryLb();
		return registryLbs;
	}
	
}
