/**
 * 
 */
package com.sgcc.devops.web.image;

import java.util.List;

/**  
 * date：2016年4月27日 下午3:19:39
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：NginxListenPort.java
 * description：  
 */
public class NginxListenPort {
	private String port;
	private List<NginxLocation> nginxLocations;
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public List<NginxLocation> getNginxLocations() {
		return nginxLocations;
	}
	public void setNginxLocations(List<NginxLocation> nginxLocations) {
		this.nginxLocations = nginxLocations;
	}
	public NginxListenPort(String port, List<NginxLocation> nginxLocations) {
		super();
		this.port = port;
		this.nginxLocations = nginxLocations;
	}
	
}
