package com.sgcc.devops.web.image.material;

import java.util.Map;

import com.sgcc.devops.web.image.Material;

/**
 * 配置文件类
 * @author dmw
 *
 */
public class ConfigFileMaterial extends Material {
	private String appName;// 应用名称
	private String appFile;// 应用包名
	private boolean sessionShare;// 是否session共享
	private Map<String, String> jdbcConfig;// jdbc数据源配置

	public boolean isSessionShare() {
		return sessionShare;
	}

	public void setSessionShare(boolean sessionShare) {
		this.sessionShare = sessionShare;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppFile() {
		return appFile;
	}

	public void setAppFile(String appFile) {
		this.appFile = appFile;
	}

	public Map<String, String> getJdbcConfig() {
		return jdbcConfig;
	}

	public void setJdbcConfig(Map<String, String> jdbcConfig) {
		this.jdbcConfig = jdbcConfig;
	}

	public ConfigFileMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigFileMaterial(String appName, String appFile, Map<String, String> configMap, boolean sessionShare) {
		super();
		this.appName = appName;
		this.appFile = appFile;
		this.jdbcConfig = configMap;
		this.sessionShare = sessionShare;
	}

	@Override
	public String toString() {
		return "ConfigFileMaterial [appName=" + appName + ", appFile=" + appFile + ", jdbcConfig=" + jdbcConfig + "]";
	}

}