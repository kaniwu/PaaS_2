package com.sgcc.devops.web.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.message.Message;

@Component
public class TaskServiceHandler implements WebSocketHandler {
	private static Logger logger = Logger.getLogger(TaskServiceHandler.class);
	private static List<WebSocketSession> currentUsers;

	private static List<WebSocketSession> getCurrentUsers() {
		return currentUsers;
	}

	private static void setCurrentUsers(List<WebSocketSession> currentUsers) {
		TaskServiceHandler.currentUsers = currentUsers;
	}

	static {
		TaskServiceHandler.setCurrentUsers(new ArrayList<WebSocketSession>());
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		TaskServiceHandler.getCurrentUsers().add(session);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		if (message instanceof TextMessage) {
			handleTextMessage(session, (TextMessage) message);
		} else {
			logger.error("Unexpected WebSocket message type: " + message);
			throw new IllegalStateException("Unexpected WebSocket message type: " + message);
		}
	}

	// 处理字符串
	private void handleTextMessage(WebSocketSession session, TextMessage message) {
		logger.error(message);
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		if (session.isOpen()) {
			session.close();
		}
		TaskServiceHandler.getCurrentUsers().remove(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
		TaskServiceHandler.getCurrentUsers().remove(session);
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}

	public void sendMessageToUsers(Message message) {
		for (WebSocketSession user : TaskServiceHandler.getCurrentUsers()) {
			try {
				if (user.isOpen()) {
					user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
				}
			} catch (IOException e) {
				logger.error("Send Message to Users exception:", e);
			}
		}
	}

	public void sendMessageToUser(String userId, Message message) {
		for (WebSocketSession user : TaskServiceHandler.getCurrentUsers()) {
			User iterator = (User) user.getAttributes().get("user");
			if (null == iterator) {
				return;
			}
			if (iterator.getUserId().equals(userId)) {
				try {
					if (user.isOpen()) {
						user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
					}
				} catch (IOException e) {
					logger.error("Send Message to User[" + userId + "] exception:", e);
				}
			}
		}
	}
}
