package com.sgcc.devops.web.image.material;

import java.util.List;
import java.util.Map;

import com.sgcc.devops.web.image.Material;

/**
 * Dockerfile信息类
 * @author dmw
 *
 */
public class DockerFileMaterial extends Material {

	private String version;// 版本

	private String baseImage;// 基础镜像

	private String maintainer;// 维护者

	private String appFileName;// 应用包名

	private String appFileTarget;// 应用宝目标路径

	private Map<String, String> fileAppendMap;// 其他配置文件名和目标路径

	private String exposePorts;// 对外暴漏的服务端口

	private String cmdCommand;// 启动时执行的命令

	private Map<String, String> configFileMap;//配置文件名称和路径
	private String DockerfilePart;
	private List<String> runCommands;
	public String getCmdCommand() {
		return cmdCommand;
	}

	public void setCmdCommand(String cmdCommand) {
		this.cmdCommand = cmdCommand;
	}

	public String getExposePorts() {
		return exposePorts;
	}

	public void setExposePorts(String exposePorts) {
		this.exposePorts = exposePorts;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBaseImage() {
		return baseImage;
	}

	public void setBaseImage(String baseImage) {
		this.baseImage = baseImage;
	}

	public String getMaintainer() {
		return maintainer;
	}

	public void setMaintainer(String maintainer) {
		this.maintainer = maintainer;
	}

	public String getAppFileName() {
		return appFileName;
	}

	public void setAppFileName(String appFileName) {
		this.appFileName = appFileName;
	}

	public String getAppFileTarget() {
		return appFileTarget;
	}

	public void setAppFileTarget(String appFileTarget) {
		this.appFileTarget = appFileTarget;
	}

	public Map<String, String> getConfigFileMap() {
		return configFileMap;
	}

	public void setConfigFileMap(Map<String, String> configFileMap) {
		this.configFileMap = configFileMap;
	}

	public List<String> getRunCommands() {
		return runCommands;
	}

	public void setRunCommands(List<String> runCommands) {
		this.runCommands = runCommands;
	}

	public String getDockerfilePart() {
		return DockerfilePart;
	}

	public void setDockerfilePart(String dockerfilePart) {
		DockerfilePart = dockerfilePart;
	}

	public DockerFileMaterial(String version, String baseImage, String maintainer, String appFileName,
			String appFileTarget, Map<String, String> fileAppendMap, String exposePorts,Map<String, String> configFileMap) {
		super();
		this.version = version;
		this.baseImage = baseImage;
		this.maintainer = maintainer;
		this.appFileName = appFileName;
		this.appFileTarget = appFileTarget;
		this.fileAppendMap = fileAppendMap;
		this.exposePorts = exposePorts;
		this.configFileMap = configFileMap;
	}

	public Map<String, String> getFileAppendMap() {
		return fileAppendMap;
	}

	public void setFileAppendMap(Map<String, String> fileAppendMap) {
		this.fileAppendMap = fileAppendMap;
	}

	public DockerFileMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "DockerFileMaterial [baseImage=" + baseImage + ", maintainer=" + maintainer + ", appFileName="
				+ appFileName + ", appFileTarget=" + appFileTarget + ", fileAppendMap=" + fileAppendMap + "]";
	}

}
