package com.sgcc.devops.web.upload;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.model.UpFileModel;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.web.message.Message;

@Component
public class UploadBasicImageWebSocketHandler implements WebSocketHandler {
	private static Logger logger = Logger.getLogger(UploadBasicImageWebSocketHandler.class);
	@Resource
	private ComponentService componentService;
//	private HostService hostService;
	
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private LocalConfig localConfig;
	private static List<WebSocketSession> currentUsers;

	private static List<WebSocketSession> getCurrentUsers() {
		return currentUsers;
	}

	private static void setCurrentUsers(List<WebSocketSession> currentUsers) {
		UploadBasicImageWebSocketHandler.currentUsers = currentUsers;
	}

	static {
		UploadBasicImageWebSocketHandler.setCurrentUsers(new ArrayList<WebSocketSession>());
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		UploadBasicImageWebSocketHandler.getCurrentUsers().add(session);
	}

	@Override
	public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception {
		if (message instanceof TextMessage) {
			handleTextMessage(session, (TextMessage) message);
		} else if (message instanceof BinaryMessage) {
			handleBinaryMessage(session, (BinaryMessage) message);
		} else {
			logger.error("Unexpected WebSocket message type: " + message);
			throw new IllegalStateException("Unexpected WebSocket message type: " + message);
		}
	}

	// 处理字符串
	private void handleTextMessage(WebSocketSession session, TextMessage message) {
		try {
			if (message.getPayload().contains("fileStartName")) {
				UpFileModel filemodel = new ObjectMapper().readValue(message.getPayload(), UpFileModel.class);
				com.sgcc.devops.dao.entity.Component component = new com.sgcc.devops.dao.entity.Component();
				component.setComponentId(filemodel.getComponentId());
				component = componentService.getComponent(component);
				if (component == null) {
					logger.warn("负载信息在数据库中不存在！");
					session.sendMessage(
							new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("NOHOST"))));
					return;
				}
				filemodel.setFileSrc(localConfig.getLocalBaseImagePath() + "\\" + filemodel.getFileStartName());
				session.getAttributes().put("filemodel", filemodel);
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("OK"))));
			} else if (message.getPayload().contains("sendover")) {
				String result = "TRUE";
				session.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage(result))));
			} else if (message.getPayload().contains("cancel")) {
				logger.warn("用户取消文件上传");
				UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
				deleteFile(filemodel.getFileSrc());
				session.sendMessage(
						new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("CANCEL"))));
				return;
			}
		} catch (JsonParseException e1) {
			logger.error("上传文件异常：", e1);
		} catch (JsonMappingException e1) {
			logger.error("上传文件异常：", e1);
		} catch (IOException e1) {
			logger.error("上传文件异常：", e1);
		}

	}

	public static void main(String[] args) {
		SSH ssh = new SSH("192.168.1.117", "sgcc", "onceas");
		if (ssh.connect()) {
			try {
				String response = ssh.executeWithResult("afdsafdsa");
				System.out.println(response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			System.out.println("can not cennect to host");
		}
	}

	// 处理二进制内容
	private void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
		UpFileModel filemodel = (UpFileModel) session.getAttributes().get("filemodel");
		if (filemodel != null) {
			try {
				if (saveFileFromBytes(message.getPayload(), filemodel.getFileSrc())) {
					session.sendMessage(
							new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("OK"))));
				} else {
					session.sendMessage(
							new TextMessage(new ObjectMapper().writeValueAsString(new UploadMessage("FALSE"))));
				}
			} catch (IOException e) {
				logger.error("上传文件异常：", e);
			}
		}
	}

	@Override
	public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
		if (session.isOpen()) {
			session.close();
		}
		UploadBasicImageWebSocketHandler.getCurrentUsers().remove(session);
	}

	@Override
	public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) throws Exception {
		UploadBasicImageWebSocketHandler.getCurrentUsers().remove(session);
	}

	@Override
	public boolean supportsPartialMessages() {
		return false;
	}

	public void sendMessageToUsers(Message message) {
		for (WebSocketSession user : UploadBasicImageWebSocketHandler.getCurrentUsers()) {
			try {
				if (user.isOpen()) {
					user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
				}
			} catch (IOException e) {
				logger.error("uploadBasicImageService Exception:", e);
			}
		}
	}

	public void sendMessageToUser(String userId, Message message) {
		for (WebSocketSession user : UploadBasicImageWebSocketHandler.getCurrentUsers()) {
			User iterator = (User) user.getAttributes().get("user");
			if (iterator != null) {
				if (iterator.getUserId().equals(userId)) {
					try {
						if (user.isOpen()) {
							user.sendMessage(new TextMessage(new ObjectMapper().writeValueAsString(message)));
						}
					} catch (IOException e) {
						logger.error("uploadBasicImageService Exception:", e);
					}
				}
			}
		}
	}

	/**
	 * 将二进制byte[]数组写入文件中
	 * 
	 * @param byteBuffer
	 *            byte[]数组
	 * @param outputFile
	 *            文件位置
	 * @return 成功: true 失败: false
	 */
	private static boolean saveFileFromBytes(ByteBuffer byteBuffer, String outputFile) {
		FileOutputStream fstream = null;
		File file = null;
		try {
			// System.out.print(outputFile);
			outputFile = FileUploadUtil.check(outputFile);
			file = new File(outputFile);
			if (!file.exists())
				file.createNewFile();
			fstream = new FileOutputStream(file, true);
			fstream.write(byteBuffer.array());
		} catch (Exception e) {
			logger.error("saveFileFromBytes Exception:", e);
			return false;
		} finally {
			if (fstream != null) {
				try {
					fstream.close();
				} catch (IOException e1) {
					logger.error("close fileimput Exception:", e1);
				}
			}
		}
		return true;
	}

	private static boolean deleteFile(String outputFile) {
		File file = null;
		try {
			file = new File(outputFile);
			if (file.exists())
				file.delete();

			return true;
		} catch (Exception e) {
			logger.error("Delete file exception:", e);
			return false;
		}
	}

}
