package com.sgcc.devops.web.daemon;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.Detector;
import com.sgcc.devops.core.detector.DockerDaemonDetector;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.image.Encrypt;

/**
 * docker守护线程，每隔5秒钟探测一次docker是否正常， 在docker所在主机不可达时每隔30秒重试一次。
 * 当docker不可达的次数超过3次时，执行docker主机从集群中移除
 * 
 * @author dmw
 *
 */
public class DockerDaemon extends Thread {

	private static Logger logger = Logger.getLogger(DockerDaemon.class);
	private static int failedNum = 0;// 获取swarm失败的次数
	private static int localDaemonFaild = 0;// swarm本机守护次数
	private boolean stop = false;

	private String ip;// 主机IP
	private String port;// 集群端口
	private String username;// 主机用户名
	private String password;// 主机密码
	private Detector dockerDetector;
	private String regIp;
	private Integer regPort;

	private HostService hostService;
	private HostCore hostCore;
	private ClusterService clusterService;
	private String keyStore;
	private Map<String, Integer> failObj = new HashMap<String, Integer>();//启动docker失败的主机Ip，key=启动失败次数
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private void init() {
		dockerDetector = new DockerDaemonDetector(ip, port);
	}

	public void stopDeamon() {
		this.stop = true;
	}

	public String getRegIp() {
		return regIp;
	}

	public void setRegIp(String regIp) {
		this.regIp = regIp;
	}


	public Integer getRegPort() {
		return regPort;
	}

	public void setRegPort(Integer regPort) {
		this.regPort = regPort;
	}

	public DockerDaemon(String ip, String port, String username, String password,
			String keyStore, HostService hostService, HostCore hostCore,
			ClusterService clusterService,String regIp, Integer regPort) {
		super();
		this.ip = ip;
		this.port = port;
		this.username = username;
		this.password = password;
		this.hostService = hostService;
		this.hostCore = hostCore;
		this.clusterService = clusterService;
		this.keyStore = keyStore;
		this.regIp = regIp;
		this.regPort = regPort;
	}

	//重启3次失败，更新数据库中主机docker服务状态为异常，并将在集群中的docker主机剔除
	private Result failReStart() {
		this.stop = true;
		Host dhost = hostService.getHostByIp(ip);
		if (null == dhost) {// 集群不存在直接返回
			logger.error("The Host 【"+ip+"】 has been remove by other person!");
			return new Result(false, "Host 【"+ip+"】 is not exist!");
		}
		
		dockerDetector = new DockerDaemonDetector(dhost.getHostIp(), "2375");
		String clusterId = dhost.getClusterId();
		if (dockerDetector.normal()) {
			this.stop = false;
			dhost.setDockerStatus((byte) Status.DOCKER.NORMAL.ordinal());
			hostService.update(dhost);
			return new Result(true, "Host 【"+ip+"】 is NORMAL!");
		}
		if(StringUtils.isEmpty(clusterId)){
			dhost.setDockerStatus((byte) Status.DOCKER.ABNORMAL.ordinal());
			hostService.update(dhost);
			return new Result(true, "Host 【"+ip+"】 is ABNORMAL!");
		}
		
		// 获取集群下的所有节点
		List<Host> slavers = hostService.listHostByClusterId(clusterId);
		if (null == slavers) {
			slavers = new ArrayList<Host>();
		}
		Iterator<Host> slaverIterator = slavers.iterator();
		Host host = null;
		Cluster cluster = clusterService.getCluster(clusterId);
		if(null==cluster){
			return new Result(false, "集群为空");
		}
		Host masterHost = hostService.getHost(cluster.getMasteHostId());
		String masterHostId = null == masterHost ? "" : masterHost.getHostId();
		// 探测集群下所有节点的Docker Daemon情况，剔除异常Docker宿主机和管理节点主机，
		while (slaverIterator.hasNext()) {
			host = slaverIterator.next();
			if (host.getHostId().equals(masterHostId)) {
				slaverIterator.remove();
				continue;
			}
			dockerDetector = new DockerDaemonDetector(host.getHostIp(), "2375");
			if (dockerDetector.normal()) {
				continue;
			} else {
				slaverIterator.remove();
				host.setDockerStatus((byte) Status.DOCKER.ABNORMAL.ordinal());
				host.setClusterId(null);
				hostService.update(host);
			}
		}
		StringBuffer clusterConfig = new StringBuffer("");
		// 生成集群配置信息，消除最后一个换行\n
		for (Host slaver : slavers) {
			clusterConfig.append(slaver.getHostIp()).append(":2375").append("\n");
		}
		if (StringUtils.hasText(clusterConfig.toString())) {
			clusterConfig = clusterConfig.delete(clusterConfig.length() - 1, clusterConfig.length());
		}
		// 更新配置文件
		String newPassword = "";
		String hostIp = "";
		String user = "";
		String hostport="";
		if(null!=masterHost){
			newPassword = Encrypt.decrypt(masterHost.getHostPwd(), keyStore);
			hostIp = masterHost.getHostIp();
			user = masterHost.getHostUser();
			hostport = masterHost.getHostPort();
		}
		boolean success = this.hostCore.updateCluster(hostIp, user, newPassword,hostport,
				clusterConfig.toString(), cluster.getManagePath());
		if (success) {
			logger.warn("Host 【"+ip+"】remove From Cluster 【"+cluster.getClusterName()+"】 success!!");
			return new Result(true, "Host 【"+ip+"】remove From Cluster 【"+cluster.getClusterName()+"】 success!!");
		} else {
			logger.error("Host 【"+ip+"】remove From Cluster 【"+cluster.getClusterName()+"】 failed!!");
			return new Result(false, "Host 【"+ip+"】remove From Cluster 【"+cluster.getClusterName()+"】 failed!!");
		}
	}

	@Override
	public void run() {
		init();
		while (!stop) {
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				logger.error("Host 【" + this.ip + "】 docker daemon is　intercepted");
			}
			if (failObj.containsKey(this.ip)&&failObj.get(this.ip) >= 3) {
				logger.error(
						"The Host【" + this.ip + "】 has self docker daemon Restart over 3 times!!!Fail Restart it!");
				Result result = this.failReStart();
				if (result.isSuccess()) {
					localDaemonFaild = 0;
					failedNum = 0;
					failObj.put(this.ip, localDaemonFaild);
					continue;
				}
			}
			if (dockerDetector.normal()) {
				Host dhost = hostService.getHostByIp(ip);
				if((byte) Status.DOCKER.ABNORMAL.ordinal()==dhost.getDockerStatus()){
					dhost.setDockerStatus((byte) Status.DOCKER.NORMAL.ordinal());
					hostService.update(dhost);
				}
				continue;
			} else {
				failedNum++;
				failObj.put(this.ip, localDaemonFaild);
				logger.error("Host 【" + this.ip + "】 docker daemon is unreachable 【" + failedNum + "】 times");
			}
			if (failedNum < 3||!failObj.containsKey(this.ip)) {
				continue;
			}
			logger.warn("Docker daemon is unreachable over 3 times !!!!! restart it !");
			SSH ssh = new SSH(ip, username, password);
			if (ssh.connect()) {
				startDocker(ssh);
			}
			localDaemonFaild++;
			failedNum = 0;
		}
	}
	private void startDocker(SSH ssh) {
		try {
			String result = ssh.executeWithResult(startCommand(regIp, regPort), 1000 * 50);
			System.out.println("-------"+result);
		} catch (Exception e) {
			logger.error("Restart swarm exception ：", e);
		} finally {
			ssh.close();
		}
	}
	private String startCommand(String regIp, Integer regPort) {
		return "systemctl start docker";
//		if(null==regIp||null==regPort){
//			return "systemctl start docker";
//		}
//		return "nohup docker daemon -H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock --insecure-registry "+regIp+":"+regPort+" > /home/logs/docker//access.log &";
	}
}
