package com.sgcc.devops.web.image;

/**
 * 数据源封装类
 * 
 * @author dmw
 *
 */
public class DataSource {

	private String jdbcUrl;// 数据源名称：JNDI的名称
	private String jdbcUser;// 数据源用户名
	private String jdbcPassword;// 数据源用户密码
	private String jdbcDriver;// jdbcdriver
	private String jndiName;// jndi名称
	private String driverFileName;// 驱动文件名称
	private String driverPath;
	private String initsql;//初始化脚本
	private String maxCapacity;//最大连接数
	private String minCapacity;//最小连接数

	public String getJdbcUrl() {
		return jdbcUrl;
	}

	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}

	public String getJdbcUser() {
		return jdbcUser;
	}

	public void setJdbcUser(String jdbcUser) {
		this.jdbcUser = jdbcUser;
	}

	public String getJdbcPassword() {
		return jdbcPassword;
	}

	public void setJdbcPassword(String jdbcPassword) {
		this.jdbcPassword = jdbcPassword;
	}

	public String getJdbcDriver() {
		return jdbcDriver;
	}

	public void setJdbcDriver(String jdbcDriver) {
		this.jdbcDriver = jdbcDriver;
	}

	public String getJndiName() {
		return jndiName;
	}

	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}

	public String getDriverFileName() {
		return driverFileName;
	}

	public void setDriverFileName(String driverFileName) {
		this.driverFileName = driverFileName;
	}

	public String getDriverPath() {
		return driverPath;
	}

	public void setDriverPath(String driverPath) {
		this.driverPath = driverPath;
	}

	public String getInitsql() {
		return initsql;
	}

	public void setInitsql(String initsql) {
		this.initsql = initsql;
	}

	public String getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(String maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public String getMinCapacity() {
		return minCapacity;
	}

	public void setMinCapacity(String minCapacity) {
		this.minCapacity = minCapacity;
	}

	public DataSource(String jdbcUrl, String jdbcUser, String jdbcPassword, String jdbcDriver, String jndiName,
			String driverFileName, String driverPath,String initsql,String maxCapacity,String minCapacity) {
		super();
		this.jdbcUrl = jdbcUrl;
		this.jdbcUser = jdbcUser;
		this.jdbcPassword = jdbcPassword;
		this.jdbcDriver = jdbcDriver;
		this.jndiName = jndiName;
		this.driverFileName = driverFileName;
		this.driverPath = driverPath;
		this.initsql = initsql;
		this.maxCapacity = maxCapacity;
		this.minCapacity = minCapacity;
	}

	public DataSource() {
		super();
	}
}
