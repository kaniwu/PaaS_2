package com.sgcc.devops.web.image;

import java.io.File;

/**
 * 文件生成器接口
 * 
 * @author dmw
 *
 */
public interface FileBuilder {

	/**
	 * 根据vm生成满足条件的配置文件
	 * 
	 * @return
	 */
	public File build();
}
