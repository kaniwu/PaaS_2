package com.sgcc.devops.web.image.material;

import java.util.List;

import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.web.image.ConfigFile;
import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.SystemServer;

/**
 * 镜像制作材料
 * 
 * @author dmw
 *
 */
public class ImageMaterial {

	private String sessionSource;// session共享数据源路径

	private List<DataSource> dataSources;// 数据源配置列表

	private String path;// 制作镜像的基本路径；

	private Image baseImage;// 依赖的基础镜像

	private String appFileName;// 应用war包名称

	private RegistryLb registryLb;
	
	private String appName;// 应用名称

	private String dnsName;// DNS名称

	private String appTag;// 应用版本
	
	private List<SystemServer> servers;// 应用服务器的Ip和port，key是IP，value是port

	private DataSource sessionShareSource;// weblogic的session共享配置

	private String sessionSharePath;// tomcat session共享的位置

	private boolean sharable;// 是否採用session共享
	
	private String systemId;//任务Id
	
	private String userId;//任务执行人
	
	private List<ConfigFile> configFiles;//配置文件
	
	private String port;
	
	private String Dockerfile;//Dockerfile填充段落
	
	private String timestamp;
	
	private boolean transferNeeded;//是否需要通过中间机器转存
	
	private String imageName;//镜像名称
	
	private List<String> runCommands;//执行语句
	StringBuffer taskMessage = new StringBuffer();
	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public DataSource getSessionShareSource() {
		return sessionShareSource;
	}

	public void setSessionShareSource(DataSource sessionShareSource) {
		this.sessionShareSource = sessionShareSource;
	}

	public String getSessionSharePath() {
		return sessionSharePath;
	}

	public void setSessionSharePath(String sessionSharePath) {
		this.sessionSharePath = sessionSharePath;
	}

	public boolean isSharable() {
		return sharable;
	}

	public void setSharable(boolean sharable) {
		this.sharable = sharable;
	}

	public List<SystemServer> getServers() {
		return servers;
	}

	public void setServers(List<SystemServer> servers) {
		this.servers = servers;
	}

	public List<DataSource> getDataSources() {
		return dataSources;
	}

	public void setDataSources(List<DataSource> dataSources) {
		this.dataSources = dataSources;
	}

	public Image getBaseImage() {
		return baseImage;
	}

	public void setBaseImage(Image baseImage) {
		this.baseImage = baseImage;
	}

	public String getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(String sessionSource) {
		this.sessionSource = sessionSource;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getAppFileName() {
		return appFileName;
	}

	public void setAppFileName(String appFileName) {
		this.appFileName = appFileName;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<ConfigFile> getConfigFiles() {
		return configFiles;
	}

	public void setConfigFiles(List<ConfigFile> configFiles) {
		this.configFiles = configFiles;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public ImageMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getDockerfile() {
		return Dockerfile;
	}

	public void setDockerfile(String dockerfile) {
		Dockerfile = dockerfile;
	}

	public boolean isTransferNeeded() {
		return transferNeeded;
	}

	public void setTransferNeeded(boolean transferNeeded) {
		this.transferNeeded = transferNeeded;
	}

	public RegistryLb getRegistryLb() {
		return registryLb;
	}

	public void setRegistryLb(RegistryLb registryLb) {
		this.registryLb = registryLb;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getDnsName() {
		return dnsName;
	}

	public void setDnsName(String dnsName) {
		this.dnsName = dnsName;
	}

	public String getAppTag() {
		return appTag;
	}

	public void setAppTag(String appTag) {
		this.appTag = appTag;
	}

	public StringBuffer getTaskMessage() {
		return taskMessage;
	}

	public void setTaskMessage(StringBuffer taskMessage) {
		this.taskMessage = taskMessage;
	}

	public List<String> getRunCommands() {
		return runCommands;
	}

	public void setRunCommands(List<String> runCommands) {
		this.runCommands = runCommands;
	}

	public ImageMaterial(String sessionSource, List<DataSource> dataSources,
			String path, Image baseImage, String appFileName, 
			String dnsName, RegistryLb registryLb,
			List<SystemServer> servers, DataSource sessionShareSource,
			String sessionSharePath, boolean sharable, String systemId,
			String userId, List<ConfigFile> configFiles,String port,String dockerfile) {
		super();
		this.sessionSource = sessionSource;
		this.dataSources = dataSources;
		this.path = path;
		this.baseImage = baseImage;
		this.appFileName = appFileName;
		this.registryLb = registryLb;
		this.servers = servers;
		this.sessionShareSource = sessionShareSource;
		this.sessionSharePath = sessionSharePath;
		this.sharable = sharable;
		this.systemId = systemId;
		this.userId = userId;
		this.configFiles = configFiles;
		this.port = port;
		this.Dockerfile = dockerfile;
	}

}
