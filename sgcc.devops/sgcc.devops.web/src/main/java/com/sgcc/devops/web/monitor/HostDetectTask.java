package com.sgcc.devops.web.monitor;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.core.detector.Detector;
import com.sgcc.devops.core.detector.HostDetector;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 主机健康状况探测任务
 * @author dmw
 *
 */
public class HostDetectTask implements Runnable {

	private TaskCache cache;
	private List<Host> hosts;

	public HostDetectTask() {
		super();
	}

	public HostDetectTask(TaskCache cache, List<Host> hosts) {
		super();
		this.cache = cache;
		this.hosts = hosts;
	}

	@Override
	public void run() {
		if (null == hosts || hosts.isEmpty()) {
			return;
		}
		ExecutorService executor = Executors.newCachedThreadPool();
		CompletionService<Result> detectService = new ExecutorCompletionService<>(executor);
		for (final Host host : hosts) {
//			int status = host.getHostStatus().intValue();
//			if (Status.HOST.DELETE.ordinal() == status || Status.HOST.ABNORMAL.ordinal() == status) {
//				continue;
//			}
			// 探测主机状态的任务改成多线程进行
			Callable<Result> task = new Callable<Result>() {
				@Override
				public Result call() throws Exception {
					Detector detector = new HostDetector(host.getHostIp(), host.getHostUser(), host.getHostPwd(),host.getHostPort());
					if (detector.normal()) {
						return new Result();
					}
					Integer times = cache.getHostCache().get(host.getHostId());
					if (null == times) {
						cache.getHostCache().put(host.getHostId(), 1);
					} else {
						cache.getHostCache().put(host.getHostId(), times + 1);
					}
					return new Result();
				}
			};
			detectService.submit(task);
		}
	}

}
