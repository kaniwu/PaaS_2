package com.sgcc.devops.web.controller.forward;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sgcc.devops.dao.entity.User;

/**  
 * date：2016年2月4日 下午1:45:51
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：IndexController.java
 * description：  系统菜单访问控制类
 */
@Controller
public class IndexController {

	private static Logger logger = Logger.getLogger(IndexController.class);

	@RequestMapping("/")
	public ModelAndView defaultIndex() {
		return new ModelAndView("index");
	}

	@RequestMapping("index.html")
	public ModelAndView index() {
		return new ModelAndView("index");
	}

	@RequestMapping("login.html")
	public ModelAndView login() {
		return new ModelAndView("account/login");
	}

	@RequestMapping("app/index.html")
	public ModelAndView app() {
		return new ModelAndView("app/index");
	}

	@RequestMapping("host/index.html")
	public ModelAndView host() {
		return new ModelAndView("host/index");
	}

	@RequestMapping("cluster/index.html")
	public ModelAndView cluster() {
		return new ModelAndView("cluster/index");
	}

	@RequestMapping("image/index.html")
	public ModelAndView image() {
		return new ModelAndView("image/index");
	}

	@RequestMapping("registry/index.html")
	public ModelAndView registry() {
		return new ModelAndView("registry/index");
	}

	@RequestMapping("registry/images.html")
	public ModelAndView registryImage(HttpServletRequest request, String registryid, String registryName,
			ModelMap modelMap) {
		modelMap.addAttribute("registryid", registryid);
		/*try {
			registryName = new String(registryName.getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("systemName encode error: " + e.getMessage());
		}*/
		modelMap.addAttribute("registryName", registryName);
		return new ModelAndView("registry/images", modelMap);
	}

	@RequestMapping("registry/hosts.html")
	public ModelAndView registryHost(HttpServletRequest request, String lbId,
			ModelMap modelMap) {
		modelMap.addAttribute("lbId", lbId);
		return new ModelAndView("registry/hosts", modelMap);
	}

	@RequestMapping("system/deploy.html")
	public ModelAndView deploy(HttpServletRequest request, String systemId, ModelMap modelMap, String systemName) {
		modelMap.addAttribute("systemId", systemId);
		try {
			systemName = URLDecoder.decode(systemName,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.error("systemName encode error: " + e.getMessage());
		}
		modelMap.addAttribute("systemName", systemName);
		return new ModelAndView("system/deploy", modelMap);
	}

	@RequestMapping("container/index.html")
	public ModelAndView container() {
		return new ModelAndView("container/index");
	}
	
	@RequestMapping("parameter/index.html")
	public ModelAndView parameter() {
		return new ModelAndView("parameter/index");
	}

	@RequestMapping("environment/index.html")
	public ModelAndView environment() {
		return new ModelAndView("environment/index");
	}
	 
	@RequestMapping("log/index.html")
	public ModelAndView log() {
		return new ModelAndView("log/index");
	}

	@RequestMapping("logs/index.html")
	public ModelAndView runLogs() {
		return new ModelAndView("logs/index");
	}

	@RequestMapping("user/index.html")
	public ModelAndView user() {
		return new ModelAndView("user/index");
	}

	@RequestMapping("role/index.html")
	public ModelAndView role() {
		return new ModelAndView("role/index");
	}

	@RequestMapping("permission/index.html")
	public ModelAndView permission() {
		return new ModelAndView("permission/index");
	}

	@RequestMapping("param/index.html")
	public ModelAndView param() {
		return new ModelAndView("param/index");
	}

	@RequestMapping("lb/index.html")
	public ModelAndView lb() {
		return new ModelAndView("lb/index");
	}

	@RequestMapping("business/index.html")
	public ModelAndView business(HttpServletRequest request, String systemId, ModelMap modelMap) {
		return new ModelAndView("business/index", modelMap);
	}

	/**
	 * page not found
	 * 
	 * @author langzi
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/404", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView pageNotFound(HttpServletRequest request) {
		return new ModelAndView("account/404");
	}

	/**
	 * page not auth
	 * 
	 * @author langzi
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/403", method = { RequestMethod.GET })
	@ResponseBody
	public ModelAndView pageNotAuth(HttpServletRequest request) {
		return new ModelAndView("account/403");
	}

	@RequestMapping("system/index.html")
	public ModelAndView system() {
		return new ModelAndView("system/index");
	}

	@RequestMapping("component/index.html")
	public ModelAndView component() {
		return new ModelAndView("component/index");
	}

	@RequestMapping("componentType/index.html")
	public ModelAndView componentType() {
		return new ModelAndView("componentType/index");
	}

	@RequestMapping("host/containerOfHost.html")
	public ModelAndView containerOfHost(HttpServletRequest request, String hostId, String hostIp,String flag, ModelMap modelMap) {
		modelMap.addAttribute("hostId", hostId);
		modelMap.addAttribute("hostIp", hostIp);
		modelMap.addAttribute("flag", flag);
		return new ModelAndView("host/containerOfHost", modelMap);
	}

	@RequestMapping("password/index.html")
	public ModelAndView password(HttpServletRequest request, ModelMap modelMap) {
		User user = (User) request.getSession().getAttribute("user");
		modelMap.addAttribute("username", user.getUserName());
		return new ModelAndView("password/index");
	}
	@RequestMapping("system/deployHistory.html")
	public ModelAndView deployHistory(HttpServletRequest request, String systemId, String systemName,ModelMap modelMap) {
		modelMap.addAttribute("systemId", systemId);
		try {
			modelMap.addAttribute("systemName", URLDecoder.decode(systemName,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error("systemName encode error: " + e.getMessage());
		}
		return new ModelAndView("system/deploy", modelMap);
	}

	@RequestMapping("library/index.html")
	public ModelAndView library() {
		return new ModelAndView("library/index");
	}
	
	@RequestMapping("auth/index.html")
	public ModelAndView auth() {
		return new ModelAndView("auth/index");
	}
	@RequestMapping("serverRoom/index.html")
	public ModelAndView serverRoom() {
		return new ModelAndView("serverRoom/index");
	}
	/**
	 * 主机组件管理
	 * @author chenzy
	 * @return
	 */
	@RequestMapping("hostComponent/index.html")
	public ModelAndView hostComponentIndex() {
		return new ModelAndView("hostComponent/index");
	}
	@RequestMapping("port/index.html")
	public ModelAndView port() {
		return new ModelAndView("port/index");
	}
}
