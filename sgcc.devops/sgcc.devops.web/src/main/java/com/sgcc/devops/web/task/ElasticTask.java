package com.sgcc.devops.web.task;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.EmailCfg;
import com.sgcc.devops.common.constant.Type.TARGET_TYPE;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.elasticity.bean.DecisionResult;
import com.sgcc.devops.web.elasticity.bean.LoadData;
import com.sgcc.devops.web.elasticity.cache.ElasticCache;
import com.sgcc.devops.web.elasticity.engine.DecideEngine;
import com.sgcc.devops.web.elasticity.engine.SystemLoadParseEngine;
import com.sgcc.devops.web.manager.ContainerManager;
import com.sgcc.devops.web.util.EmailSendUtil;

import net.sf.json.JSONObject;


/**
 * 弹性伸缩任务
 * 
 * @author dmw
 *
 */
public class ElasticTask implements Callable<Result> {

	private static Logger logger = Logger.getLogger(ElasticTask.class);

	private String systemId;// 应用ID
	private List<Container> containers;// 当前应用在该环境下的容器列表
	private MonitorService monitorService;// 监控服务，用来获取监控数据
	private ElasticStrategy strategy;// 监控策略
	private ElasticCache cache;// 弹性伸缩缓存，用来处理重复弹性的问题
	private String email;// 需要通知的邮件地址
	private EmailCfg emailCfg;
	private SystemService systemService;
	private ContainerManager containerManager;
	//是否可以发送邮件（防止发送过多邮件）
	private static String emailAppId;//验证已发送邮件的appId
	private static String emailResult;//验证已发送邮件的结果

	/**
	 * @param appId
	 * @param envId
	 * @param containers
	 * @param monitorService
	 * @param appManager
	 * @param strategy
	 * @param cache
	 */
	public ElasticTask(String systemId,List<Container> containers, MonitorService monitorService,
			ElasticStrategy strategy, ElasticCache cache, String email,
			EmailCfg emailCfg,SystemService systemService,ContainerManager containerManager) {
		super();
		this.systemId = systemId;
		this.containers = containers;
		this.monitorService = monitorService;
		this.strategy = strategy;
		this.cache = cache;
		this.email = email;
		this.emailCfg = emailCfg;
		this.systemService = systemService;
		this.containerManager = containerManager;
	}

	@Override
	public Result call() throws Exception {
		logger.info("start to do elastic task");
		if (null == monitorService) {
			logger.warn("TASK INIT FAILED!");
			return new Result(false, "TASK INIT FAILED!");
		}
		if (null == systemId) {
			logger.warn("NO Application ID!!");
			return new Result(false, "NO Application ID!!");
		}
		if (null == containers || containers.isEmpty()) {
			logger.warn("No container is in the Application!");
			return new Result(true, "success");
		}
		if (null == cache) {
			logger.warn("No cache inited!");
			return new Result(false, "No cache inited!");
		}
		// 看看当前系统中是否有正在处理的伸缩任务
		if (cache.taskExist(systemId)) {
			logger.warn("Another task is running");
			return new Result(false, "Another task is running");
		}
		try {
			List<Monitor> monitorDatas = new ArrayList<Monitor>();
			logger.info("start to get load data");
			for (Container container : containers) {
				List<Monitor> monitors = monitorService.selectLastMonitorData((byte) TARGET_TYPE.CONTAINER.ordinal(),
						container.getConId(), 60l);
				if (null != monitors) {
					monitorDatas.addAll(monitors);
				}
			}
			logger.info("get load data over!");
			SystemLoadParseEngine loadParsor = new SystemLoadParseEngine();
			//计算平均使用率
			LoadData loadData = loadParsor.parse(monitorDatas);
			logger.info("parse load data over!");
			// 判断是否满足扩展条件
			DecideEngine decideEngine = new DecideEngine(loadData, strategy, containers.size());
			DecisionResult decision = decideEngine.decide();
			Result result = null;
			// 判断是否满足收缩条件
			logger.info("Elastic decision is :" + decision.toString());
			com.sgcc.devops.dao.entity.System system = systemService.getSystemById(systemId);
			JSONObject params = new JSONObject();
			params.put("systemId", systemId);
			params.put("conCount", decision.getStep());
			switch (decision.getDecision()) {
			case SCALE_IN: {// 满足收缩条件
				logger.warn("当前系统需要收缩，接下来会进行收缩操作");
				List<String> containers = new ArrayList<>();
				List<String> scaleInContainers = this.judgeScaleInContainer(decision.getStep(), strategy);
				if (scaleInContainers.isEmpty()) {
					logger.warn("当前系统已经达到最小节点数，不能继续收缩节点");
					result = new Result(false, "当前系统已经达到最小节点数，不能继续收缩节点");
					break;
				}
				containers.addAll(scaleInContainers);
				params.put("containerId", containers);
				logger.info("Scale in  :" + containers.size());
				// 看看当前系统中是否有正在处理的伸缩任务
				if (cache.taskExist(systemId)) {
					logger.warn("The System already have elastic task runing!!! Drop this task!");
					result = new Result(false, "The System already have elastic task runing!!! Drop this task!");
				} else {
					cache.addTask(systemId);
					result = this.containerManager.stopAndRemoveCon(params);
					cache.removeTask(systemId);
				}
				//如果收缩出现同样的错误只能发送一次邮件，成功不限制
				if ((!systemId.equals(emailAppId) && !result.getMessage().equals(emailResult)) || result.isSuccess()) {
					notice("应用【" + system.getSystemName() + "】缩容",
							"应用【" + system.getSystemName() + "】收缩服务实例【" + containers.size() + "】 结果:" + result.getMessage());
				}
				emailAppId = systemId;
				emailResult = result.getMessage();
				cache.removeTask(systemId);
				break;
			}
			case SCALE_OUT: {// 满足扩展条件
				logger.warn("当前系统需要扩展！接下来进行节点扩展");
				if (containers.size() + decision.getStep() > strategy.getMaxNode()) {
					int tempstep = strategy.getMaxNode() - containers.size();
					if (tempstep < 0) {
						tempstep = 0;
					}
					decision.setStep(tempstep);
				}
				if (decision.getStep() <= 0) {
					logger.warn("系统已经到达最大节点数，无法继续扩展!");
					result = new Result(false, "系统已经到达最大节点数，无法继续扩展!");
					break;
				}
				logger.info("Scaleout instance:" + decision.getStep());
				
				// 看看当前系统中是否有正在处理的伸缩任务
				if (cache.taskExist(systemId)) {
					logger.warn("The System already have elastic task runing!!! Drop this task!");
					result = new Result(false, "The System already have elastic task runing!!! Drop this task!");
				} else {
					params.put("userId", "system");
					logger.info("Elastic param is :" + params.toString());
					cache.addTask(systemId);
					result = this.containerManager.createConBySysId(params);
					cache.removeTask(systemId);
				}
				
				
				//如果扩展出现同样的错误只能发送一次邮件，成功不限制
				if ((!systemId.equals(emailAppId) && !result.getMessage().equals(emailResult)) || result.isSuccess()) {
					notice("应用【" + system.getSystemName() + "】扩容",
							"应用【" + system.getSystemName() + "】扩展服务实例【" + decision.getStep() + "】 结果:" + result.getMessage());
				}
				emailAppId = systemId;
				emailResult = result.getMessage();
				cache.removeTask(systemId);
				break;
			}
			case NO_ACTION: {// 没有任何动作
				logger.info("Nothing to do!");
				result = new Result(true, "nothing to do!");
				break;
			}
			case EXCEPTION: {// 数据提供异常
				logger.warn("Data exception");
				result = new Result(false, "Data exception");
				//如果数据提供异常出现同样的错误只能发送一次邮件
				if ((!systemId.equals(emailAppId) && !result.getMessage().equals(emailResult))) {
					notice("应用【" + system.getSystemName() + "】弹性伸缩",
							"应用【" + system.getSystemName() + "】弹性伸缩执行失败：数据异常！");
				}
				emailAppId = systemId;
				emailResult = result.getMessage();
				break;
			}
			default: {// 未知结果
				logger.warn("Unexcepted thing occured!");
				result = new Result(false, "Unexcepted thing occured!");
				//如果未知结果出现同样的错误只能发送一次邮件
				if ((!systemId.equals(emailAppId) && !result.getMessage().equals(emailResult))) {
					notice("应用【" + system.getSystemName() + "】弹性伸缩",
							"应用【" + system.getSystemName() + "】弹性伸缩执行【" + decision.getStep() + "】 失败：未知错误！");
				}
				emailAppId = systemId;
				emailResult = result.getMessage();
				break;
			}
			}
			logger.info("Application[" + system.getSystemName() + "]Elastic Task result is " + result.toString());
			return result;
		} finally {
			cache.removeTask(systemId);
		}
	}

	/**
	 * 判断需要进行收缩的容器ID列表， 收缩后的容器数量不能小于系统伸缩策略的最小节点数。
	 * 如果当前容器数量减去计划缩减的容器数量小于最小节点数，那么将减少计划缩减的容器数量。
	 * 筛选容器是否删除的策略是随机方式。即如果当前需要从10个容器节点中删除3个容器，则取0-9之间的3个不同的随机数。然后把
	 * 
	 * @param step
	 * @param strategy
	 * @return
	 */
	private List<String> judgeScaleInContainer(Integer step, ElasticStrategy strategy) {
		logger.info("start to judge the sacle in container...");
		int containersSize = containers.size();
		logger.info("container size-----:"+containersSize);
		if (null == step || containersSize <= strategy.getMinNode()) {
			// 没有收缩或者当前系统的容器数本身就小于伸缩策略规定的最小节点数，则不处理
			logger.warn("no step or container");
			return new ArrayList<String>();
		}
		int finalNum = 0;
		if (containersSize - step <= strategy.getMinNode()) {
			// 当前系统节点数减去计划缩减数小于系统规定的最小节点数，取当前节点数减去最小节点数作为最终需要缩减的容器数量。
			logger.warn("容器数量不足，无法收缩至最小节点以下");
			finalNum = containers.size() - strategy.getMinNode();
		} else {
			finalNum = step;
		}
		if (finalNum <= 0) {
			return new ArrayList<String>();
		}
		Random random = new Random();
		List<Integer> indexList = new ArrayList<Integer>();
		logger.info("start to get random index of container list");
		while (indexList.size() < finalNum) {
			int number = Math.abs(random.nextInt() % containersSize);
			if (indexList.contains(number)) {
				continue;
			} else {
				indexList.add(number);
			}
		}
		List<String> idList = new ArrayList<String>(finalNum);
		for (Integer index : indexList) {
			idList.add(containers.get(index).getConId());
		}
		logger.info("final sacle container is list is " + idList.toString());
		return idList;
	}

	private void notice(String subject, String content) {
		try {
			if (StringUtils.hasText(email)) {
				EmailSendUtil.sendMail(emailCfg, subject, content, email);
			}
		} catch (Exception e) {
			logger.error("notice create exception:", e);
		}
	}
}
