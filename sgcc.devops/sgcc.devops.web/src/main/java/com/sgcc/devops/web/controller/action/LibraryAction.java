package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.LibraryModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.LibraryService;
import com.sgcc.devops.web.query.QueryList;

/**  
 * date：2016年3月14日10:53:57
 * project name：sgcc.devops.web
 * @author  liyp
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LibraryAction.java
 * description：  库文件操作访问控制类
 */
@Controller
@RequestMapping("library")
public class LibraryAction {
	Logger logger = Logger.getLogger(LibraryAction.class);
	@Resource
	private QueryList queryList;
	@Resource
	private LibraryService libraryService;
	
	@RequestMapping(value = "/create")
	@ResponseBody
	public Result create(HttpServletRequest request, LibraryModel libraryModel) {
		User user = (User) request.getSession().getAttribute("user");
		if (libraryModel != null) {
			try {
				if(libraryModel.getLibraryName()!=null){
					libraryModel.setLibraryName(new String(libraryModel.getLibraryName().getBytes("ISO-8859-1"), "UTF-8"));
				}
				if(libraryModel.getLibraryDescription()!=null){
					libraryModel.setLibraryDescription(libraryModel.getLibraryDescription());
				}
				libraryModel.setLibraryCreator(user.getUserId());
				int re = libraryService.createLibrary(libraryModel);
				Result result = new Result(re > 0, re > 0 ? "添加预加载包(" + libraryModel.getLibraryName() + ")成功"
						: "添加预加载包(" + libraryModel.getLibraryName() + ")失败");
				request.setAttribute("logDetail", result.getMessage());
				request.setAttribute("success", result.isSuccess()?"Success":"Fail");
				return result;
			} catch (Exception e) {
				logger.error("添加预加载包失败: "+e.getMessage());
				return null;
			}
		} else {
			return new Result(false, "参数输入异常!");
		}
	}

	@RequestMapping(value = "/list")
	@ResponseBody
	public GridBean libraryList(HttpServletRequest request,PagerModel pagerModel, LibraryModel libraryModel) {
		try {
			if(libraryModel!=null && libraryModel.getLibraryName()!=null){
				libraryModel.setLibraryName(new String(libraryModel.getLibraryName().getBytes("ISO-8859-1"), "UTF-8"));
			}
			return queryList.queryLibraryList(pagerModel, libraryModel);
		} catch (Exception e) {
			logger.error("查询预加载包失败: "+e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "/update")
	@ResponseBody
	public Result update(HttpServletRequest request, LibraryModel libraryModel) {
		if (libraryModel != null) {
			try {
				int re = libraryService.updateLibrary(libraryModel);
				Result result = new Result(re > 0, re > 0 ? "编辑预加载包成功": "编辑预加载包失败");
				request.setAttribute("logDetail", result.getMessage());
				request.setAttribute("success", result.isSuccess()?"Success":"Fail");
				return result;
			} catch (Exception e) {
				logger.error("编辑预加载包失败: "+e.getMessage());
				return null;
			}
		} else {
			return new Result(false, "参数输入异常!");
		}
	}
	
	/**
	 * @author liyp
	 * @time 2016年3月16日
	 * @description 删除预加载包记录
	 */
	@RequestMapping("/deleteLibrary")
	@ResponseBody
	public Result deleteLibrary(HttpServletRequest request, LibraryModel libraryModel) {
		if (libraryModel != null) {
			if(libraryService.checkLibraryUsed(libraryModel.getLibraryId())){
				Result result = new Result(false,"此预加载包已经被使用不可删除");
				request.setAttribute("logDetail", result.getMessage());
				request.setAttribute("success", result.isSuccess()?"Success":"Fail");
				return result;
			}else{
			int re = libraryService.deleteLibrary(libraryModel);
			Result result = new Result(re > 0, re > 0 ? "删除预加载包记录成功": "删除预加载包记录失败");
			request.setAttribute("logDetail", result.getMessage());
			request.setAttribute("success", result.isSuccess()?"Success":"Fail");
			return result;
			}
		} else {
			return new Result(false, "参数输入异常!");
		}
	}
}
