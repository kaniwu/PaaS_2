package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.ComponentTypeModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ComponentTypeService;
import com.sgcc.devops.web.manager.ComponentTypeManager;
import com.sgcc.devops.web.query.QueryList;

/**  
 * date：2016年2月4日 下午1:47:01
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentTypeAction.java
 * description：  组件类型操作访问控制类
 */
@Controller
@RequestMapping("componentType")
public class ComponentTypeAction {

	@Resource
	private QueryList queryList;
	@Resource
	private ComponentTypeManager componentTypeManager;
	@Resource
	private ComponentTypeService componentTypeService;

	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, ComponentTypeModel componentTypeModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(componentTypeModel);
		params.put("componentTypeName", componentTypeModel.getComponentTypeName());
		params.put("userId", user.getUserId());
		return componentTypeManager.createComponentType(params);

	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	public Result delete(HttpServletRequest request, int componentTypeId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("componentTypeId", componentTypeId);
		params.put("userId", user.getUserId());
		// ja.add(0, params);
		// jo.put("Params", ja);
		return componentTypeManager.deleteComponentType(params);
		// dispatch.distributor(jo);
	}

	@RequestMapping(value = "/deletes", method = { RequestMethod.POST })
	@ResponseBody
	public Result deletes(HttpServletRequest request, String ids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("userId", user.getUserId());
		params.put("ids", ids);
		// dispatch.distributor(jo);
		return componentTypeManager.deleteComponentTypes(params);
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, ComponentTypeModel componentTypeModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("componentTypeId", componentTypeModel.getComponentTypeId());
		params.put("componentTypeName", componentTypeModel.getComponentTypeName());
		params.put("userId", user.getUserId());
		return componentTypeManager.updateComponentType(params);
	}

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean componentTypeList(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, ComponentTypeModel componentTypeModel) {
		User user = (User) request.getSession().getAttribute("user");
		return queryList.queryComponentTypeList(user.getUserId(), page, rows, componentTypeModel);
	}

	@RequestMapping(value = "/selectAll", method = { RequestMethod.GET })
	@ResponseBody
	public JSONObject allComponentTypeList(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		return componentTypeService.selectAll(user.getUserId());
	}

}
