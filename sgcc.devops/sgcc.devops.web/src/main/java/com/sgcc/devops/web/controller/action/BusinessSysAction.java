/**
 * 
 */
package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.BusinessSysService;

/**  
 * date：2016年2月4日 下午1:46:26
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessSysAction.java
 * description：  业务系统与物理系统关联关系访问控制类
 */
@RequestMapping("/businessSys")
@Controller
public class BusinessSysAction {
	@Resource
	private BusinessSysService businessSysService;

	@RequestMapping(value = "/getSysByBusinessId", method = { RequestMethod.POST })
	@ResponseBody
	public String getSysByBusinessId(HttpServletRequest request,
			@RequestParam(value = "businessId", required = true) String businessId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = businessSysService.getSysByBusinessId(user, businessId,"");
		return ja.toString();
	}

}
