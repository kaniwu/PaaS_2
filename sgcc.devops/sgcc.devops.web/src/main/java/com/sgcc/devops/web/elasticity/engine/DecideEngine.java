package com.sgcc.devops.web.elasticity.engine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.log4j.Logger;

import com.sgcc.devops.dao.entity.ElasticItem;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.web.elasticity.bean.DecisionResult;
import com.sgcc.devops.web.elasticity.bean.LoadData;
import com.sgcc.devops.web.elasticity.enums.ConflictStrategy;
import com.sgcc.devops.web.elasticity.enums.ElasticDecision;
import com.sgcc.devops.web.elasticity.enums.ElasticItemName;
import com.sgcc.devops.web.elasticity.enums.ScaleStrategy;

/**
 * 决策引擎类
 * 
 * @author dmw
 *
 */
public class DecideEngine {

	private static Logger logger = Logger.getLogger(DecideEngine.class);
	private int containerNum;
	private LoadData load;
	private ElasticStrategy strategy;

	private ElasticItemName coreFactor;// 引起伸缩的关键策略项。

	public int getContainerNum() {
		return containerNum;
	}

	public void setContainerNum(int containerNum) {
		this.containerNum = containerNum;
	}

	public LoadData getLoad() {
		return load;
	}

	public void setLoad(LoadData load) {
		this.load = load;
	}

	public ElasticStrategy getStrategy() {
		return strategy;
	}

	public void setStrategy(ElasticStrategy strategy) {
		this.strategy = strategy;
	}

	public DecideEngine(LoadData load, ElasticStrategy strategy, int containerNum) {
		super();
		this.load = load;
		this.strategy = strategy;
		this.containerNum = containerNum;
	}

	public DecideEngine() {
		super();
	}

	/**
	 * 解析弹性伸缩策略项枚举
	 * 
	 * @param name
	 * @return
	 */
	private ElasticItemName getName(String name) {
		ElasticItemName itemName = ElasticItemName.NULL;
		try {
			itemName = ElasticItemName.valueOf(name.toUpperCase());
		} catch (Exception e) {
			logger.error("parse item name exception:", e);
		}
		return itemName;
	}

	/**
	 * 获取冲突策略枚举，异常时默认扩展优先
	 * 
	 * @param string
	 * @return
	 */
	private ConflictStrategy getConflictStrategy(String string) {
		ConflictStrategy strategy = null;
		try {
			strategy = ConflictStrategy.valueOf(string);
		} catch (Exception e) {
			strategy = ConflictStrategy.SCALE_OUT_FIRST;
			//logger.error(e);
		}
		return strategy;
	}

	/**
	 * 获取伸缩策略，异常时默认固定步长
	 * 
	 * @param string
	 * @return
	 */
	private ScaleStrategy getScaleStrategy(String string) {
		ScaleStrategy strategy = null;
		try {
			logger.info("-----"+string);
			strategy = ScaleStrategy.valueOf(string);
		} catch (Exception e) {
			strategy = ScaleStrategy.FIXED_STEP;
			//logger.error("--------"+e);
		}
		return strategy;
	}

	/**
	 * 判断弹性甚多的决策
	 * 
	 * @param old
	 *            当前决策
	 * @param now
	 *            最新决策
	 * @param strategy
	 *            系统弹性伸缩策略
	 * @param name
	 *            监控项名称
	 * @return
	 */
	private ElasticDecision judge(ElasticDecision old, ElasticDecision now, ConflictStrategy strategy,
			ElasticItemName name) {
		logger.info("begion to judge decision");
		if (null == old || now == null) {
			logger.info("old decision or new decision is null");
			coreFactor = name;
			return now;
		}
		ElasticDecision decision = null;
		if (old.name().equals(now.name())) {
			decision = now;
		}else if(!old.name().equals(ElasticDecision.SCALE_OUT.name())&&!now.name().equals(ElasticDecision.SCALE_OUT.name())){
			return ElasticDecision.NO_ACTION;
		}else {
			switch (strategy) {
			case SCALE_IN_FIRST:
				logger.info("sacle in first");
				decision = ElasticDecision.SCALE_IN;
				break;
			case SCALE_OUT_FIRST:
				logger.info("scale out first");
				decision = ElasticDecision.SCALE_OUT;
				break;
			default:
				break;
			}
		}
		if (null != decision) {
			if (!old.equals(ElasticDecision.SCALE_OUT) && now.equals(ElasticDecision.SCALE_OUT)) {
				logger.info("name[" + name + "] begin to main factor");
				coreFactor = name;
			}
		}
		return decision;
	}

	/**
	 * 获取决定伸缩的策略项
	 * 
	 * @return
	 */
	private ElasticItem getCoreFactor() {
		logger.info("start to get core factor");
		if (null == strategy) {
			logger.error("Strategy is null!");
			return null;
		}
		List<ElasticItem> items = strategy.getItems();
		if (null == strategy || null == items || items.isEmpty()) {
			logger.warn("strategy or elastic item is empty");
			return null;
		}
		if (null == coreFactor) {
			logger.warn("no core factor is found");
			return null;
		}
		for (ElasticItem item : items) {
			if (item.getItemName().equalsIgnoreCase(coreFactor.name())) {
				logger.info("Find core factor elastic item :" + coreFactor.name());
				return item;
			}
		}
		logger.info("Do not find core factor elastic item");
		return null;
	}

	/**
	 * 计算需要的增加或者减少的数量公式为num -2*max*min/(max+min)或2*max*min/(max+min)-num
	 * 
	 * @param scalein
	 *            是否为收缩
	 * @param num
	 *            当前容器数量
	 * @param max
	 *            扩展阈值
	 * @param min
	 *            收缩阈值
	 * @param load
	 *            当前负载
	 * @return
	 */
	private int calc(boolean scalein, BigDecimal num, BigDecimal max, BigDecimal min, BigDecimal load) {
		logger.info("begin to calc step...");
		int step = 0;
		logger.info("------num:"+num+"-----load:"+load+"-----max:"+max+"-----min:"+min);
		if (scalein) {
			step = (num.subtract(
					(new BigDecimal(2).multiply(num).multiply(load)).divide(max.add(min), RoundingMode.CEILING)))
							.intValue();
		} else {
			step = ((new BigDecimal(2).multiply(num).multiply(load)).divide(max.add(min), RoundingMode.CEILING)
					.subtract(num)).intValue();
		}
		logger.info("step is :" + step);
		return step;
	}

	/**
	 * 获取引起弹性伸缩的负载项
	 * 
	 * @return
	 */
	private BigDecimal getCoreLoad() {
		logger.info("start to get core load");
		switch (coreFactor) {
		case CPU:
			logger.info("core load is cpu");
			return new BigDecimal(load.getCpu());
		case MEM:
			logger.info("core load is mem");
			return new BigDecimal(load.getMem());
		case NET:
			logger.info("core load is network");
			return new BigDecimal(load.getNet());
		default:
			logger.info("default load is link");
			return new BigDecimal(load.getCpu());
		}
	}

	/**
	 * 根据决策结果计算步长
	 * 
	 * @param decision
	 * @return
	 */
	private int calcStep(ElasticDecision decision) {
		logger.info("start to calc step by decision");
		int step = 0;
		ElasticItem item = getCoreFactor();
		if (null == item) {
			logger.warn("no core factor elastic item found");
			return 0;
		}
		switch (decision) {
		case SCALE_IN:
			logger.info("calc scale in step");
			step = calc(true, new BigDecimal(containerNum), item.getExpandThreshold(), item.getShrinkThreshold(),
					getCoreLoad());
			break;
		case SCALE_OUT:
			logger.info("calc scale-out step");
			step = calc(false, new BigDecimal(containerNum), item.getExpandThreshold(), item.getShrinkThreshold(),
					getCoreLoad());
			break;
		default:
			break;
		}
		logger.info("elastic step is " + step);
		return step;
	}

	public DecisionResult decide() {
		logger.info("begin to decide...");
		if (null == load || null == strategy) {// 数据不全
			logger.error("No LoadData or No Strategy!");
			return new DecisionResult(ElasticDecision.EXCEPTION, null);
		}
		List<ElasticItem> items = strategy.getItems();
		logger.info("sort the elastic item");
		Collections.sort(items, new Comparator<ElasticItem>() {
			@Override
			public int compare(ElasticItem item1, ElasticItem item2) {
				return item1.getPriority().compareTo(item2.getPriority());
			}
		});
		logger.info("sort the elastic item over!");
		ElasticDecision decision = null;
		ConflictStrategy conflictStrategy = getConflictStrategy(strategy.getConflictStrategy());
		ScaleStrategy scaleStrategy = getScaleStrategy(strategy.getElasticStrategy());
		logger.info("loop the elastic item and decide...");
		for (ElasticItem item : items) {
			switch (getName(item.getItemName())) {
			case CPU: {
				Double expandThreshold = item.getExpandThreshold().doubleValue();
				Double shrinkThreshold = item.getShrinkThreshold().doubleValue();
				if (load.getCpu() > expandThreshold) {// 触发系统扩展阈值
					decision = judge(decision, ElasticDecision.SCALE_OUT, conflictStrategy, ElasticItemName.CPU);
				} else if (load.getCpu() < shrinkThreshold) {// 触发系统收缩阈值
					decision = judge(decision, ElasticDecision.SCALE_IN, conflictStrategy, ElasticItemName.CPU);
				}else{
					decision = ElasticDecision.NO_ACTION;
				}
				break;
			}
			case MEM: {
				Long expandThreshold = item.getExpandThreshold().longValue();
				Long shrinkThreshold = item.getShrinkThreshold().longValue();
				if (load.getMem() > expandThreshold) {
					decision = judge(decision, ElasticDecision.SCALE_OUT, conflictStrategy, ElasticItemName.MEM);
				} else if (load.getMem() < shrinkThreshold) {
					decision = judge(decision, ElasticDecision.SCALE_IN, conflictStrategy, ElasticItemName.MEM);
				}else{
					decision = ElasticDecision.NO_ACTION;
				}
				break;
			}
			case NET: {
				Long expandThreshold = item.getExpandThreshold().longValue();
				Long shrinkThreshold = item.getShrinkThreshold().longValue();
				if (load.getNet() > expandThreshold) {
					decision = judge(decision, ElasticDecision.SCALE_OUT, conflictStrategy, ElasticItemName.NET);
				} else if (load.getNet() < shrinkThreshold) {
					decision = judge(decision, ElasticDecision.SCALE_IN, conflictStrategy, ElasticItemName.NET);
				}else{
					decision = ElasticDecision.NO_ACTION;
				}
				break;
			}
			default:
				break;
			}
		}
		if (null == decision) {
			decision = ElasticDecision.NO_ACTION;
		}
		int step = 0;
		logger.info("start to calc step with decision [" + decision.name() + "] and scale strategy:"
				+ scaleStrategy.name());

		switch (scaleStrategy) {

		case FIXED_STEP:
			logger.info("--------："+strategy.getStepSize());
			step = strategy.getStepSize()==null ? 0 : strategy.getStepSize();
			break;
		case AUTO_ADAPT:
			step = calcStep(decision);
			break;
		default:
			step = strategy.getStepSize();
			break;
		}
		if (0 == step) {
			decision = ElasticDecision.NO_ACTION;
		}
		logger.info("final decision is :" + decision.name() + "|step =" + step);
		return new DecisionResult(decision, step);
	}
}
