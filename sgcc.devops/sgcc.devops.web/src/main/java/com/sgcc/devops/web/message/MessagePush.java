package com.sgcc.devops.web.message;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

/**
 * date：2015年8月17日 下午3:41:13 project name：sgcc-devops-web
 * 
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：MessagePush.java description：
 */
@Component
public class MessagePush {

	@Resource
	private MessageWebSocketHandler messageWebSocketHandler;

	public void pushMessage(final String userId, final String content) {
		messageWebSocketHandler.sendMessageToUser(userId, new StickyMessage(content));
	}

	public void pushMessage(final String content) {
		messageWebSocketHandler.sendMessageToUsers(new StickyMessage(content));
	}

	public MessagePush() {
		super();
	}
}
