package com.sgcc.devops.web.controller.action;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.EnvironmentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Environment;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.EnvironmentManager;
import com.sgcc.devops.web.query.QueryList;


@Controller
@RequestMapping("environment")
public class EnvironmentAction {
	Logger logger = Logger.getLogger(EnvironmentAction.class);
	
	@Resource 
	private QueryList queryList;
	@Resource
	private EnvironmentManager environmentManager;
	
	/*环境管理表分页获取*/
	@RequestMapping(value = "/searchlist" , method = { RequestMethod.GET})
	@ResponseBody
	public GridBean searchList(HttpServletRequest request,PagerModel pagerModel,EnvironmentModel environmentModel){
		return queryList.queryEnvList(pagerModel, environmentModel);
	}
	/*环境管理表分页获取*/
	@RequestMapping(value = "/searchByEnv" , method = { RequestMethod.GET})
	@ResponseBody
	public String searchByEnv(HttpServletRequest request,EnvironmentModel environmentModel){
		User user = (User) request.getSession().getAttribute("user");
		List<Environment> environments = environmentManager.searchByEnv(user, environmentModel);
		return JSONUtil.parseObjectToJsonArray(environments).toString(); 
	}
	/*新增环境*/
	@RequestMapping(value = "/createEnvironment" , method = {RequestMethod.POST})
	@ResponseBody
	public Result createEnvironment(HttpServletRequest request , EnvironmentModel environmentModel){
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(environmentModel);
		params.put("userId", user.getUserId());
		Result result = environmentManager.createParameter(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/*编辑环境*/
	@RequestMapping(value = "/updateEnvironment", method = RequestMethod.POST)
	@ResponseBody
	public Result updateEnvironment(HttpServletRequest request,  EnvironmentModel environmentModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(environmentModel);
		params.put("userId", user.getUserId());
		Result result = environmentManager.updateEnvironment(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/*删除环境*/
	@RequestMapping(value = "/deleteEnvironment", method = RequestMethod.POST)
	@ResponseBody
	public Result deleteEnvironment(HttpServletRequest request,  EnvironmentModel environmentModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(environmentModel);
		params.put("userId", user.getUserId());
		Result result = environmentManager.deleteEnvironment(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/list" , method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray list(HttpServletRequest request,String environmentId) {
		try {
			User user = (User) request.getSession().getAttribute("user");
			return queryList.environmentsList(user);
		} catch (Exception e) {
			logger.error("查询environmentsList失败: "+e.getMessage());
			return null;
		}
	}
}
