package com.sgcc.devops.web.image.material;

import java.util.List;

import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Material;

/**
 * Tomcat上下文材料
 * @author dmw
 *
 */
public class TomcatContextMaterial extends Material {

	private List<DataSource> dataSources;

	private boolean sharable;// 是否session共享

	private String sharepath;// session共享的位置

	public boolean isSharable() {
		return sharable;
	}

	public void setSharable(boolean sharable) {
		this.sharable = sharable;
	}

	public String getSharepath() {
		return sharepath;
	}

	public void setSharepath(String sharepath) {
		this.sharepath = sharepath;
	}
	
	public List<DataSource> getDataSources() {
		return dataSources;
	}

	public void setDataSources(List<DataSource> dataSources) {
		this.dataSources = dataSources;
	}

	public TomcatContextMaterial(List<DataSource> dataSources, boolean sharable, String sharepath) {
		super();
		this.dataSources = dataSources;
		this.sharable = sharable;
		this.sharepath = sharepath;
	}

	public TomcatContextMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}
}
