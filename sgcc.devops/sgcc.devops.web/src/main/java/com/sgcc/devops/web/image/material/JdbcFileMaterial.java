package com.sgcc.devops.web.image.material;

import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Material;

/**
 * JDBC文件材料
 * @author dmw
 *
 */
public class JdbcFileMaterial extends Material {

	private String sourceName;// 数据源名称
	private DataSource dataSource;// 数据源封装类

	public String getSourceName() {
		return sourceName;
	}

	public void setSourceName(String sourceName) {
		this.sourceName = sourceName;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public JdbcFileMaterial(String sourceName, DataSource dataSource) {
		super();
		this.sourceName = sourceName;
		this.dataSource = dataSource;
	}

	public JdbcFileMaterial() {
		super();
		// TODO Auto-generated constructor stub
	}

}
