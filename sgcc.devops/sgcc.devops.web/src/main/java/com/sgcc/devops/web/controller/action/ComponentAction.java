package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.ComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ComponentManager;
import com.sgcc.devops.web.query.QueryList;


/**  
 * date：2016年2月4日 下午1:46:52
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ComponentAction.java
 * description：  组件操作访问控制类
 */
@Controller
@RequestMapping("component")
public class ComponentAction {

	@Resource
	private QueryList queryList;
	@Resource
	private ComponentManager componentManager;

	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, ComponentModel componentModel) {

		User user = (User) request.getSession().getAttribute("user");
		Result result = componentManager.createComponent(user.getUserId(),componentModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result; 
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	public Result delete(HttpServletRequest request, int componentId) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("componentId", componentId);
		params.put("userId", user.getUserId());
		Result result = componentManager.deleteComponent(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/deletes", method = { RequestMethod.POST })
	@ResponseBody
	public Result deletes(HttpServletRequest request, String ids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("userId", user.getUserId());
		params.put("ids", ids);
		Result result = componentManager.deleteComponents(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, ComponentModel componentModel) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = componentManager.updateComponent(user.getUserId(),componentModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean componentList(HttpServletRequest request, PagerModel pagerModel, ComponentModel componentModel) {
		User user = (User) request.getSession().getAttribute("user");
		return queryList.queryComponentList(user.getUserId(),pagerModel, componentModel);
	}

	@RequestMapping(value = "/getSystem", method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray getSystem(HttpServletRequest request, String componentId,String componentType) {
		return componentManager.getSystem(componentId,componentType);
	}

	/** 组件的可达性测试 */
	@RequestMapping(value = "/connectTest", method = { RequestMethod.GET })
	@ResponseBody
	public Result connectTest(HttpServletRequest request, ComponentModel componentModel) {
		return componentManager.connectTest(componentModel);
	}
	/**
	 * @author zs
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;

		isOk = componentManager.checkName(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	/**
	 * 更新数据库相关的物理系统
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年1月13日
	 */
	@RequestMapping(value = "/refresh", method = { RequestMethod.POST })
	@ResponseBody
	public Result refresh(HttpServletRequest request, @RequestParam(value = "chk_value", required = false) String[] chk_value) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = componentManager.refresh(chk_value,user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	
	/**
	 * 根据driverID 查询driverDesc
	 * @param request
	 * @param DBType
	 * @return
	 */
	@RequestMapping(value = "/selectDriverDesc", method = { RequestMethod.GET })
	@ResponseBody
	public JSONObject selectDriverDesc(HttpServletRequest request, String driverId){
		return componentManager.selectDriverDesc(driverId);
	}
	/**
	 */
	@RequestMapping(value = "/listSvn", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean getSvnList() {
		return queryList.getSvnList();
	}
	/** nginx测试 */
	@RequestMapping(value = "/nginxTest", method = { RequestMethod.GET })
	@ResponseBody
	public Result nginxTest(HttpServletRequest request, String hostId) {
		return componentManager.nginxTest(hostId);
	}
}
