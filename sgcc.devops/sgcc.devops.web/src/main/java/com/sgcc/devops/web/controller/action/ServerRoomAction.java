package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.RackModel;
import com.sgcc.devops.common.model.ServerRoomModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ServerRoomManager;
import com.sgcc.devops.web.query.QueryList;


@Controller
@RequestMapping("serverRoom")
public class ServerRoomAction {
	Logger logger = Logger.getLogger(ServerRoomAction.class);
	
	@Resource
	private QueryList queryList;
	@Resource
	private ServerRoomManager serverRoomManager;

	/**
	 * 获取所有机房机架并拼装成json
	 * @return
	 */
	@RequestMapping(value = "/tree", method = { RequestMethod.GET })
	@ResponseBody
	public String treeList(HttpServletRequest request) {
		JSONObject result = new JSONObject();
		result.put("success", true);
		JSONArray data = serverRoomManager.treeList();
		result.put("data", data);
		return result.toString();
	}
	/**
	 * @param authority
	 * 机房分页列表
	 * @return
	 */
	@RequestMapping(value = "/serverRoomList", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean serverRoomList(HttpServletRequest request,PagerModel pagerModel) {
		return queryList.serverRoomList(pagerModel);
	}
	
	
	/**
	 * 获取所有机架
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/rackList", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean rackList(HttpServletRequest request,PagerModel pagerModel,String serverRoomId) {
		return queryList.rackList(pagerModel,serverRoomId);
	}
	
	/**
	 * 删除机房
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/deleteServerRoom", method = RequestMethod.POST)
	@ResponseBody
	public Result deleteServerRoom(HttpServletRequest request, ServerRoomModel serverRoomModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(serverRoomModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.deleteServerRoom(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	

	/**
	 * 删除机架
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/deleteRack", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(HttpServletRequest request, RackModel rackModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(rackModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.deleteRack(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	 * 新增机房
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/createServerRoom", method = RequestMethod.POST)
	@ResponseBody
	public Result createServerRoom(HttpServletRequest request, ServerRoomModel serverRoomModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(serverRoomModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.createServerRoom(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	 * 新增机架
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/createRack", method = RequestMethod.POST)
	@ResponseBody
	public Result createRack(HttpServletRequest request, RackModel rackModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(rackModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.createRack(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	

	/**
	 * 编辑机房
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/updateServerRoom", method = RequestMethod.POST)
	@ResponseBody
	public Result updateServerRoom(HttpServletRequest request, ServerRoomModel serverRoomModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(serverRoomModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.updateServerRoom(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	 * 编辑机架
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/updateRack", method = RequestMethod.POST)
	@ResponseBody
	public Result updateRack(HttpServletRequest request, RackModel rackModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(rackModel);
		params.put("userId", user.getUserId());
		Result result = serverRoomManager.updateRack(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
}
