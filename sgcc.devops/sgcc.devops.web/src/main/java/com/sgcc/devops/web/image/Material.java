package com.sgcc.devops.web.image;

/**
 * 信息材料基类
 * @author dmw
 *
 */
public class Material {

	private String directory;

	public String getDirectory() {
		return directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

}
