/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.JsonNode;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.HttpClientConstant;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.core.intf.RegistryCore;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.Port;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.RegistryService;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.message.MessagePush;

import net.sf.json.JSONObject;

/**
 * date：2016年2月4日 下午1:58:57 project name：sgcc.devops.web
 * 
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegistryManager.java description： 镜像仓库操作流程处理类
 */
@org.springframework.stereotype.Component
public class RegistryManager {
	private static Logger logger = Logger.getLogger(RegistryManager.class);
	@Resource
	private RegistryService registryService;
	@Resource
	private RegistryLbService registryLbService;
	@Resource
	private ImageService imageService;
	@Resource
	private HttpClient httpClient;
	@Resource
	private RegImageMapper regImageMapper;
	@Resource
	private HostService hostService;
	@Resource
	private MessagePush messagePush;
	@Resource
	private HostCore hostCore;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private DockerHostConfig dockerHostConfig;
	@Resource
	private RegistryCore registryCore;
	@Resource
	private PortService portService;
	@Resource
	private HostManager hostManager;
	@Resource
	private CompPortManager compPortManager;
	@Resource
	private HostComponentManager hostComponentManager;
	@Resource
	private ComponentService componentService;
	@Resource
	private ComponentExpandService componentExpandService;
	public Result reachRegiHost(JSONObject json_object) {
		HttpClient httpClient = new HttpClient();
		String regihost_ip = json_object.getString("registryIpaddr");
		String regihost_port = json_object.getString("registryPort");

		HashMap<String, Object> return_map = (HashMap<String, Object>) httpClient
				.doGet_DOCKER(null, HttpClientConstant.GET_QUERY_REGISTRY(
						regihost_ip, regihost_port));
		/* 判断结果返回是否为成功 */
		if (return_map.get("success").equals(true)) {
			return new Result(true,
					"<i class=\"fa fa-link\"></i>仓库[<font color=\"blue\"><b>"
							+ regihost_ip
							+ "</b></font>:<font color=\"blue\"><b>"
							+ regihost_port + "]</b></font>可以使用。");
		}
		return new Result(false,
				"<i class=\"fa fa-chain-broken\"></i>仓库[<font color=\"red\"><b>"
						+ regihost_ip + "</b></font>:<font color=\"red\"><b>"
						+ regihost_port + "</b></font>]连接无效。");

	}

	/**
	 * @author yangqinglin
	 * @description 将仓库服务器的镜像数据同步到数据库中
	 */
	public Result syncBatchRegiSlaveImg(JSONObject json_object) {
		/* 从调用端获取仓库的ID、名称、地址、端口等数组 */
		String[] registry_ids = json_object.getString("registry_ids")
				.split(",");
		String[] registry_names = json_object.getString("registry_names")
				.split(",");
		String[] registry_ipaddrs = json_object.getString("registry_ipaddrs")
				.split(",");
		String[] registry_ports = json_object.getString("registry_ports")
				.split(",");

		for (int count = 0, length = registry_ids.length; count < length; count++) {
			/* 初始化对于镜像数据表处理的HashMap对象，String */
			HashMap<String, Image> DB_OperHashMap = new HashMap<String, Image>();

			/* 获取仓库中镜像的列表 */
			ArrayList<String> slave_img_list = getRegiSlaveImages(
					registry_ipaddrs[count], registry_ports[count]);
			/* 分别获取每个镜像的版本数据 */
			HashMap<String, HashMap<String, String>> regi_imgs_info = getRegiMappingImgs(
					registry_ipaddrs[count], registry_ports[count],
					slave_img_list);

			/* 定义两个链表分别保存数据库和在线信息 */
			ArrayList<Image> online_image_list = new ArrayList<Image>();
			ArrayList<Image> database_image_list = new ArrayList<Image>();

			for (Entry<String, HashMap<String, String>> img_entry : regi_imgs_info
					.entrySet()) {

				String img_name = img_entry.getKey();
				HashMap<String, String> img_tags = img_entry.getValue();
				/* dop_image表中的IMAGE_NAME字段，格式如下：192.168.1.117:5000/registry */
				String IMAGE_NAME = registry_ipaddrs[count] + ":"
						+ registry_ports[count] + "/" + img_name;

				/* 从数据库中获取某个镜像所有的版本列表信息 */
				Image database_image = new Image();
				database_image.setImageName(IMAGE_NAME);
				/* 向数据库镜像数据链表中插入新查询的数据库结果 */
				database_image_list.addAll(imageService
						.selectActiveAllImages(database_image));

				for (Entry<String, String> tag_entry : img_tags.entrySet()) {
					Image online_image = new Image();
					String tag_name = tag_entry.getKey();
					/* 写入镜像的标签内容 */
					String IMAGE_TAG = tag_name;
					online_image.setImageTag(IMAGE_TAG);

					String tag_img_id = tag_entry.getValue();
					/* 取整个UUID的前12为作为IMAGE_UUID保存入数据库 */
					String IMAGE_UUID = tag_img_id.substring(0, 12);
					online_image.setImageUuid(IMAGE_UUID);
					online_image.setImageName(IMAGE_NAME);
					online_image.setImageStatus(Status.IMAGE.NORMAL.ordinal());
					// online_image.setAppId(null);

					/* 向链表中插入网络查询的镜像版本对象 */
					online_image_list.add(online_image);
				}
			}
			/* 比较连个链表，生成对于数据库操作的映射表 */
			DB_OperHashMap = CompareOnlineDBImageList(online_image_list,
					database_image_list);

			/* 根据多次循环遍历的结果，确定对于数据库的操作 */
			Iterator<String> img_iter = DB_OperHashMap.keySet().iterator();
			while (img_iter.hasNext()) {
				/* 获取哈希表的key值 */
				String key = img_iter.next();

				if (key.contains("INSERT")) {
					/* 当类型为插入时，对数据库进行插入操作 */
					String insert_imgid = imageService.create(DB_OperHashMap
							.get(key));
					/* 插入仓库与镜像的对应表 */
					RegImage reg_img = new RegImage(KeyGenerator.uuid(),
							registry_ids[count], insert_imgid);
					regImageMapper.insert(reg_img);

				} else if (key.contains("DELETE")) {
					/* 当类型为删除时，对数据库进行删除操作 */
					imageService.delete(DB_OperHashMap.get(key).getImageId());
					/* 在仓库与镜像对应表中删除对应元素 */
				}
			}
		}

		/* 根据HTTP获取的内容与数据库进行同步 */
		return new Result(true, "仓库[" + Arrays.toString(registry_names) + "]同步数据已经完成！");

	}

	/**
	 * @author yangqinglin
	 * @description 将仓库服务器的镜像数据同步到数据库中
	 */
	public Result sycDBandRegiImgInfo(JSONObject json_object) {
		String regihost_ip = json_object.getString("registryIpaddr");
		String regihost_port = json_object.getString("registryPort");
		String registry_id = json_object.getString("registryId");
		// String USER_ID = json_object.getString("userId");
		/* 初始化对于镜像数据表处理的HashMap对象，String */
		HashMap<String, Image> DB_OperHashMap = new HashMap<String, Image>();

		/* 获取仓库中镜像的列表 */
		ArrayList<String> slave_img_list = getRegiSlaveImages(regihost_ip,
				regihost_port);
		/* 分别获取每个镜像的版本数据 */
		HashMap<String, HashMap<String, String>> regi_imgs_info = getRegiMappingImgs(
				regihost_ip, regihost_port, slave_img_list);

		/* 定义两个链表分别保存数据库和在线信息 */
		ArrayList<Image> online_image_list = new ArrayList<Image>();
		ArrayList<Image> database_image_list = new ArrayList<Image>();

		for (Entry<String, HashMap<String, String>> img_entry : regi_imgs_info
				.entrySet()) {

			String img_name = img_entry.getKey();
			HashMap<String, String> img_tags = img_entry.getValue();
			/* dop_image表中的IMAGE_NAME字段，格式如下：192.168.1.117:5000/registry */
			String IMAGE_NAME = regihost_ip + ":" + regihost_port + "/"
					+ img_name;

			/* 从数据库中获取某个镜像所有的版本列表信息 */
			Image database_image = new Image();
			database_image.setImageName(IMAGE_NAME);
			/* 向数据库镜像数据链表中插入新查询的数据库结果 */
			database_image_list.addAll(imageService
					.selectActiveAllImages(database_image));

			for (Entry<String, String> tag_entry : img_tags.entrySet()) {
				Image online_image = new Image();
				String tag_name = tag_entry.getKey();
				/* 写入镜像的标签内容 */
				String IMAGE_TAG = tag_name;
				online_image.setImageTag(IMAGE_TAG);

				String tag_img_id = tag_entry.getValue();
				/* 取整个UUID的前12为作为IMAGE_UUID保存入数据库 */
				String IMAGE_UUID = tag_img_id.substring(0, 12);
				online_image.setImageUuid(IMAGE_UUID);
				online_image.setImageName(IMAGE_NAME);
				online_image.setImageStatus(Status.IMAGE.NORMAL.ordinal());
				// online_image.setAppId(null);

				/* 向链表中插入网络查询的镜像版本对象 */
				online_image_list.add(online_image);
			}
		}
		/* 比较连个链表，生成对于数据库操作的映射表 */
		DB_OperHashMap = CompareOnlineDBImageList(online_image_list,
				database_image_list);

		/* 根据多次循环遍历的结果，确定对于数据库的操作 */
		Iterator<String> img_iter = DB_OperHashMap.keySet().iterator();
		while (img_iter.hasNext()) {
			/* 获取哈希表的key值 */
			String key = img_iter.next();

			if (key.contains("INSERT")) {
				/* 当类型为插入时，对数据库进行插入操作 */
				String insert_imgid = imageService.create(DB_OperHashMap
						.get(key));
				/* 插入仓库与镜像的对应表 */
				RegImage reg_img = new RegImage(KeyGenerator.uuid(),
						registry_id, insert_imgid);
				regImageMapper.insert(reg_img);

			} else if (key.contains("DELETE")) {
				/* 当类型为删除时，对数据库进行删除操作 */
				imageService.delete(DB_OperHashMap.get(key).getImageId());
				/* 在仓库与镜像对应表中删除对应元素 */
			}
		}

		/* 根据HTTP获取的内容与数据库进行同步 */
		return new Result(true, "仓库【" + registry_id + "】同步数据已经完成！");
	}

	/**
	 * @author youngtsinglin
	 * @description 增加比较两个查询结果链表的处理方法
	 */
	private HashMap<String, Image> CompareOnlineDBImageList(
			ArrayList<Image> online_list, ArrayList<Image> database_list) {
		int counter = 0;
		/* 保存两个链表之间的相对差集，以便后续数据库操作。 */
		HashMap<String, Image> oper_subtract_map = new HashMap<String, Image>();
		boolean match_sign = false;

		/* 如果http查询的链表比数据库多的情况下，则考虑插入数据库操作 */
		for (Image online_img : online_list) {
			match_sign = false;
			/* 比对镜像UUID、镜像名称、镜像标签是否一致 */
			for (Image db_img : database_list) {
				if ((db_img.getImageUuid().trim().equalsIgnoreCase(online_img
						.getImageUuid().trim()))
						&& (db_img.getImageName().trim()
								.equalsIgnoreCase(online_img.getImageName()
										.trim()))
						&& (db_img.getImageTag().trim()
								.equalsIgnoreCase(online_img.getImageTag()
										.trim()))) {
					match_sign = true;
				}
			}
			/* 遍历查询之后，没有在数据库表里找到匹配记录 */
			if (!match_sign) {
				oper_subtract_map.put("INSERT_" + counter, online_img);
				counter++;
			}
		}
		/* 如果数据库http比查询的链表多的情况下，则考虑删除数据库操作 */
		for (Image db_img : database_list) {
			match_sign = false;
			for (Image online_img : online_list) {
				if ((online_img.getImageUuid().trim().equalsIgnoreCase(db_img
						.getImageUuid().trim()))
						&& (online_img.getImageName().trim()
								.equalsIgnoreCase(db_img.getImageName().trim()))
						&& (online_img.getImageTag().trim()
								.equalsIgnoreCase(db_img.getImageTag().trim()))) {
					match_sign = true;
				}
			}
			/* 遍历查询之后，没有在在线查询结果里找到匹配记录 */
			if (!match_sign) {
				oper_subtract_map.put("DELETE_" + counter, db_img);
			}
		}

		return oper_subtract_map;
	}

	public static HashMap<String, HashMap<String, String>> getRegiMappingImgs(
			String IP, String Port, ArrayList<String> SlaveImgList) {
		HashMap<String, HashMap<String, String>> return_mapping = new HashMap<String, HashMap<String, String>>();
		HttpClient httpClient = new HttpClient();

		for (String ImgName : SlaveImgList) {
			HashMap<String, Object> return_image_map = (HashMap<String, Object>) httpClient
					.doGet_DOCKER(null, HttpClientConstant
							.GET_REGISTRY_IMAGE_TAGS(IP, Port, ImgName));
			/* 判断结果返回是否为成功 */
			if (return_image_map.get("success").equals(true)) {
				HashMap<String, String> imgs_hashmap = new HashMap<String, String>();
				JsonNode json_image_node = (JsonNode) return_image_map
						.get(HttpClientConstant.constant_hashmap
								.get("RESULT_KEY"));
				Iterator<Entry<String, JsonNode>> iter_json = json_image_node
						.fields();
				while (iter_json.hasNext()) {
					Entry<String, JsonNode> temp_node = iter_json.next();
					imgs_hashmap.put(temp_node.getKey(), temp_node.getValue()
							.asText());
				}
				return_mapping.put(ImgName, imgs_hashmap);
			}
		}

		return return_mapping;
	}

	public static ArrayList<String> getRegiSlaveImages(String IP, String port) {
		ArrayList<String> RegiSlaveImgList = new ArrayList<String>();
		HttpClient httpClient = new HttpClient();

		/* 执行HTTP查询仓库下镜像的请求 */
		HashMap<String, Object> return_map = (HashMap<String, Object>) httpClient
				.doGet_DOCKER(null,
						HttpClientConstant.GET_QUERY_REGISTRY(IP, port));
		/* 判断结果返回是否为成功 */
		if (return_map.get("success").equals(true)) {
			JsonNode json_node = (JsonNode) return_map
					.get(HttpClientConstant.constant_hashmap.get("RESULT_KEY"));
			JsonNode results_node = json_node.path("results");
			Iterator<JsonNode> registry_nodes = results_node.elements();
			while (registry_nodes.hasNext()) {
				JsonNode single_node = registry_nodes.next();
				JsonNode name_node = single_node.path("name");
				String name_content = name_node.asText();
				if (name_content.contains("/")) {
					/* 取得类似于dingmw、sgcc、dmbc等字段 ，例如：library/dingmw */
					String image_name = name_content.split("/")[1];
					RegiSlaveImgList.add(image_name);
				} else {
					logger.error("Query Registry Slave Images Info error:("
							+ name_content + "),Not Contain /.");
				}
			}
		} else {
			logger.error("Query Registry Slave Images Info error:(" + IP + ":"
					+ port + "), query failed.");
		}
		return RegiSlaveImgList;
	}
	/**
	 * 测试安装仓库
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年8月4日
	 */
	public JSONObject createRegistry(JSONObject jo) {
		// MessageResult message = null;
		// String userId = jo.getString("userId");
		String registry_name = jo.getString("registryName");
		String registryPort = jo.getString("registryPort");
		String nginx_Id=jo.getString("nginx_Id");
		try {

			Host host = new Host();
			host.setHostId(jo.getString("hostId"));
			host = hostService.getHost(host);
			if (host == null) {
				logger.info("主机【" + jo.getString("hostId") + "】不存在！");
				jo.put("result", false);
				jo.put("message", "主机【" + jo.getString("hostId") + "】不存在！");
				return jo;
			}
			/* 判断远程的服务器是否已经安装了docker程序 */
			String dockerIns = jo.getString("dockerIns");
			String ip = host.getHostIp();
			String account = host.getHostUser();
			// 主机密码解密
			String password = Encrypt.decrypt(host.getHostPwd(),
					localConfig.getSecurityPath());
			//校验主机的nginx端口是否被占用
			Result result = this.validateNginx(nginx_Id,registryPort);
			if(!result.isSuccess()){
				jo.put("result", result.isSuccess());
				jo.put("message", result.getMessage());
				return jo;
			}
//			String info = "";
			if (dockerIns.equals("0")) {
//				info = hostCore.dockerInfo(ip, account, password);
//				String noneFlag = "docker: command not found";
//				String deadFlag = "Cannot connect to the Docker daemon";
//				if (null == info || info.contains(noneFlag)
//						|| info.contains(deadFlag)) {
//					if (info.contains(noneFlag) || info.contains(deadFlag)) {
//						// 未安装，先安装再启动
//						boolean result = false;
//						if (info.contains(noneFlag)) {
//							logger.info("Registry host docker env is installing");
//							result = hostManager.installDocker(ip, account,
//									password);
//						}
//						if (result || info.contains(deadFlag)) {
//							logger.info("Registry host docker env is starting");
//							hostCore.startDocker(ip, account, password);
//							info = hostCore.dockerInfo(ip, account, password);
//							if (null == info || info.contains(noneFlag)
//									|| info.contains(deadFlag)) {
//								logger.info("Registry host docker start is not ok");
//								jo.put("result", false);
//								jo.put("message",
//										"Registry host docker start is not ok");
//								return jo;
//							} else {
//								logger.info("Registry host docker start is ok");
//							}
//						} else {
//							jo.put("result", false);
//							jo.put("message",
//									"Registry host docker start is not ok");
//							return jo;
//						}
//					} else {
//						logger.info("Registry host docker env is not ok");
//						jo.put("result", false);
//						jo.put("message", "Registry host docker env is not ok");
//						return jo;
//					}
//				} else {
//					logger.info("Registry host docker env is ok");
//				}
			} else {
				result = hostComponentManager.installHostComponent(host.getHostId(),ip,
						account, password, dockerIns);
				if (!result.isSuccess()) {
					jo.put("result", false);
					jo.put("message", result.getMessage());
					return jo;
				}
			}
			// 安装registry
			if (jo.get("registryIns").equals("0")) {
//				String infoReg = registryCore.registryInfo(ip, account,
//						password);
//				String noneFlagReg = "not-found";
//				String deadFlagReg1 = "loaded";
//				String deadFlagReg2 = "inactive";
//				if (null == infoReg
//						|| infoReg.contains(noneFlagReg)
//						|| (infoReg.contains(deadFlagReg1) && infoReg
//								.contains(deadFlagReg2))) {
//					if (infoReg.contains(noneFlagReg)
//							|| (infoReg.contains(deadFlagReg1) && infoReg
//									.contains(deadFlagReg2))) {
//						// 未安装，先安装再启动
//						boolean result = false;
//						if (infoReg.contains(noneFlagReg)) {
//							result = installRegistry(ip, account, password);
//							logger.info("Registry host registry env is installing");
//						}
//						if (result
//								|| (infoReg.contains(deadFlagReg1) && infoReg
//										.contains(deadFlagReg2))) {
//							registryCore.startRegistry(ip, account, password);
//							logger.info("Registry host registry env is starting");
//							info = registryCore.registryInfo(ip, account,
//									password);
//							if (null == info
//									|| info.contains(noneFlagReg)
//									|| (info.contains(deadFlagReg1) && info
//											.contains(deadFlagReg2))) {
//								logger.info("Registry host registry start is not ok");
//								jo.put("result", false);
//								jo.put("message",
//										"Registry host registry start is not ok");
//								return jo;
//							} else {
//								logger.info("Registry host registry start is ok");
//							}
//						}
//					} else {
//						logger.info("Registry host registry env is not ok");
//						jo.put("result", false);
//						jo.put("message",
//								"Registry host registry env is not ok");
//						return jo;
//					}
//				} else {
//					logger.info("Registry host registry env is ok");
//				}
			} else {
				String registryIns = jo.getString("registryIns");
				if (org.apache.commons.lang.StringUtils.isEmpty(registryIns)) {
					jo.put("result", false);
					jo.put("message", "组件不存在");
					return jo;
				}
				result = hostComponentManager.installHostComponent(host.getHostId(),ip,
						account, password, registryIns);
				if (!result.isSuccess()) {
					jo.put("result", false);
					jo.put("message",result.getMessage());
					return jo;
				}

			}
			JSONObject params = new JSONObject();
			/* 获取仓库主机的IP地址 */
			/* 获取仓库主机的端口 */
			params.put("registryIpaddr", ip);
			params.put("registryPort", registryPort);

			/* 检查节点IP地址和端口的可达性 */
			Result record = this.reachRegiHost(params);
			if (record.isSuccess()) {
				logger.info("Registry 测试成功");
			} else {
				jo.put("result", false);
				jo.put("message", "Registry 测试失败");
				logger.info("Registry 测试失败");
				return jo;
			}
		} catch (Exception e) {
			jo.put("result", false);
			jo.put("message",
					"添加仓库【" + registry_name + "】 失败: " + e.getMessage());
			logger.error("添加仓库【" + registry_name + "】 失败: " + e.getMessage());
		}
		jo.put("result", true);
		return jo;
	}

	public JSONObject insertRegistry(JSONObject jo) {
		String registry_name = jo.getString("registryName");
		try {
			Host host = new Host();
			host.setHostId(jo.getString("hostId"));
			host = hostService.getHost(host);
			int result = registryService.createRegistry(jo);
			if (result == 1) {
				host.setHostType((byte) Type.HOST.REGISTRY.ordinal());
				hostService.update(host);
				jo.put("result", true);
				jo.put("message", "添加仓库【" + registry_name + "】 成功！");
				logger.info("添加仓库【" + registry_name + "】 成功！");
			} else {
				jo.put("result", false);
				jo.put("message", "添加仓库【" + registry_name + "】 失败！");
				logger.info("添加仓库【" + registry_name + "】 失败！");
			}
		} catch (Exception e) {
			jo.put("result", false);
			jo.put("message",
					"添加仓库【" + registry_name + "】 失败: " + e.getMessage());
			logger.error("添加仓库【" + registry_name + "】 失败: " + e.getMessage());
		}
		return jo;
	}

	public JSONObject updateRegistry(JSONObject jo) {
		try {
			int result = registryService.updateRegistry(jo);
			if (result == 1) {
				jo.put("result", true);
				logger.info("Update registry【" + jo.getString("registryName")
						+ "】 success");
			} else {
				jo.put("result", false);
				logger.info("Update registry【" + jo.getString("registryName")
						+ "】 fail");
			}
		} catch (Exception e) {
			logger.error("Update registry【" + jo.getString("registryName")
					+ "】 fail: " + e.getMessage());
		}
		return jo;
	}

	public Result deleteBatchRegistry(JSONObject json_object) {
		String exist = null;
		String name_array = json_object.getString("name_array");

		try {
			exist = registryService.checkImageIsExist(json_object);
		} catch (Exception e) {
			logger.error("检查仓库【" + name_array + "】镜像是否存在失败: " + e.getMessage());
			return new Result(false, "检查仓库(" + name_array + ")镜像是否存在失败."
					+ e.getMessage());
		}
		if (exist != null && !"".equals(exist)) {
			// json_object.put("result", 2);
			// json_object.put("exist", exist);
			logger.info("仓库【" + name_array + "】中存在镜像，删除仓库失败！");
			return new Result(false, "仓库【" + name_array + "】中存在镜像，删除仓库失败！");
		} else {
			try {
				int result = registryService.deleteRegistry(json_object);
				if (result == 1) {
					// json_object.put("result", result);
					logger.info("仓库【" + name_array + "】删除成功。");
					return new Result(true, "仓库【" + name_array + "】删除成功。");
				} else {
					// json_object.put("result", result);
					logger.info("仓库【" + name_array + "】中无镜像，数据库操作失败！");
					return new Result(false, "仓库【" + name_array
							+ "】中无镜像，数据库操作失败！");
				}
			} catch (Exception e) {
				logger.error("删除仓库(" + name_array + ")失败！" + e.getMessage());
				return new Result(false, "删除仓库(" + name_array + ")失败！"
						+ e.getMessage());
			}
		}
	}

	public JSONObject deleteRegistry(JSONObject json_object) {
		String exist = null;
		try {
			exist = registryService.checkImageIsExist(json_object);
		} catch (Exception e) {
			logger.info("检查仓库镜像是否存在失败: " + e.getMessage());
		}
		if (exist != null && !"".equals(exist)) {
			json_object.put("result", 2);
			json_object.put("exist", exist);
			logger.info("Delete registry fail,The registry has images.");
		} else {
			try {
				int result = registryService.deleteRegistry(json_object);
				if (result == 1) {
					json_object.put("result", result);
					logger.info("Delete registry success");
				} else {
					json_object.put("result", result);
					logger.info("Delete registry fail");
				}
			} catch (Exception e) {
				logger.error("Delete registry fail: " + e.getMessage());
			}
		}
		return json_object;
	}

	// 检查仓库名称是否重复
	public Boolean checkName(String id, String name) {
		Registry registry = new Registry();
		registry.setRegistryName(name);
		registry = registryService.selectRegistry(registry);
		//
		if (registry != null) {
			if (id == null || id.equals("")) {
				// 创建时 找到registryName为name的cluster对象
				return false;
			} else if (registry.getRegistryId().endsWith(id)) {
				// 更新时，找到自身
				return true;
			} else {
				// 更新时，name重复
				return false;
			}
		} else {
			// 没有registryName为name的registry对象
			return true;
		}
	}

	public Result registryDaemon(String registryId, String opration, User user) {
		Registry record = new Registry();
		record.setRegistryId(registryId);
		Registry registry = registryService.selectRegistry(record);
		Result result = null;
		if (null == registry) {
			return new Result(false, "仓库ID【" + registryId + "】信息查询失败!");
		}
		if ("start".equals(opration)) {
			if (registry.getRegistryStatus() != 3) {
				return new Result(false, "仓库ID【" + registry.getRegistryId()
						+ "】状态必须为停止!");
			}
			String hostid = registry.getHostId();
			Host host = hostService.getHost(hostid);
			// 调用接口启动或停止
			result = registryCore.startRegistry(
					host.getHostIp(),
					host.getHostUser(),
					Encrypt.decrypt(host.getHostPwd(),
					localConfig.getSecurityPath()),
					host.getHostPort());
			if (!result.isSuccess()) {
				return result;
			}
			record.setRegistryStatus((byte) Status.REGISTRY.NORMAL.ordinal());
			JSONObject params = JSONUtil.parseObjectToJsonObject(record);
			int statusResult = registryService.updateRegistrySta(params);
			Result return_result = new Result(statusResult > 0,
					statusResult > 0 ? "开启仓库成功" : "数据库出错");
			// 改变数据库状态
			return return_result;
		}
		if ("stop".equals(opration)) {
			// 校验状态是否与数据库中一致,状态为2时可以操作
			if (registry.getRegistryStatus() != 1) {
				return new Result(false, "仓库状态必须为正常!");
			}
			String hostid = registry.getHostId();
			Host host = hostService.getHost(hostid);
			// 调用接口启动或停止
			result = registryCore.stopRegistry(
					host.getHostIp(),
					host.getHostUser(),
					Encrypt.decrypt(host.getHostPwd(),
					localConfig.getSecurityPath()),
					host.getHostPort());
			if (!result.isSuccess()) {
				return result;
			}
			record.setRegistryStatus((byte) Status.REGISTRY.STOP.ordinal());
			JSONObject params = JSONUtil.parseObjectToJsonObject(record);
			int statusResult = registryService.updateRegistrySta(params);
			Result return_result = new Result(statusResult > 0,
					statusResult > 0 ? "停止仓库成功" : "数据库出错");
			// 改变数据库状态
			return return_result;
		}
		return new Result(false, "传入参数有误，操作符问题");
	}

	public Result createRegistrylb(JSONObject jo) {
		// 创建仓库，高可用，
		// 存3次 一个lb表，两个registry表
		RegistryLb registryLb = new RegistryLb();
		String id = KeyGenerator.uuid();
		registryLb.setId(id);
		registryLb.setRegistryName(jo.getString("reg_name"));
		registryLb.setRegistryDesc(jo.getString("reg_desc"));
		registryLb.setDomainName(jo.getString("load_domain"));
		String Lbtype = jo.getString("reg_loadtype");
		if (jo.containsKey("nginx_port")) {
			Port prot = portService.selectByPrimaryKey(jo.getString("nginx_port"));
			if (prot == null || StringUtils.isEmpty(prot.getPort())) {
				return new Result(false, "取得nginx端口失败！");
			}
			registryLb.setLbPort(Integer.parseInt(prot.getPort()));
		}
		registryLb.setLbHostId(jo.getString("host_Id"));
		registryLb.setStatus((byte) Status.REGISTRY.NORMAL.ordinal());
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		registryLb.setRegistryCreatetime(new Date());
		registryLb.setRegistryCreator(jo.getString("userId"));
		registryLb.setLbIp(jo.getString("load_ip"));

		String dockerIns = jo.getString("dockerIns");
		String registryIns = jo.getString("registryIns");
		// 之前仓库的逻辑添加一个仓库
		if (org.apache.commons.lang.StringUtils.isEmpty(registryIns)
				|| org.apache.commons.lang.StringUtils.isEmpty(dockerIns)) {
			return new Result(false, "仓库创建失败，未找到组件信息");
		}
		JSONObject params = new JSONObject();
		params.put("registryName", jo.getString("reg_name"));
		params.put("registryPort", jo.getString("host_port"));
		params.put("hostId", jo.getString("host_Id"));
		params.put("userId", jo.getString("userId"));
		params.put("registryDesc", "主机");
		params.put("registryIns", registryIns);
		params.put("dockerIns", dockerIns);
		params.put("LbId", id);
		params.put("nginx_Id",jo.getString("nginx_Id"));
		JSONObject json = this.createRegistry(params);
		if (!(boolean) json.get("result")) {
			return new Result(false, "主机创建失败：" + json.get("message"));
		}
		JSONObject param2 = null;
		if (!"0".equals(jo.getString("standHost_Id"))) { 
			param2 = new JSONObject();
			param2.put("registryName", jo.getString("reg_name"));
			param2.put("registryPort", jo.getString("standHost_port"));
			param2.put("hostId", jo.getString("standHost_Id"));
			param2.put("userId", jo.getString("userId"));
			param2.put("registryDesc", "备用主机");
			param2.put("registryIns", registryIns);
			param2.put("dockerIns", dockerIns);
			param2.put("LbId", id);
			param2.put("nginx_Id",jo.getString("nginx_Id"));
			JSONObject json2 = this.createRegistry(param2);
			if (!(boolean) json2.get("result")) {
				return new Result(false, json2.getString("message"));
			}
		}
		//给主用仓库主机docker服务指定仓库
		JSONObject resultJS = this.updateAndStartDocker(jo.getString("host_Id"),registryLb.getDomainName(),registryLb.getLbPort(),jo.getString("dockerParam"));
		if(!resultJS.getBoolean("result")){
			return new Result(false, resultJS.getString("message"));
		}
		if (!"0".equals(jo.getString("standHost_Id"))) { 
			//给备用仓库主机docker服务指定仓库
			resultJS = this.updateAndStartDocker(jo.getString("standHost_Id"),registryLb.getDomainName(),registryLb.getLbPort(),jo.getString("dockerParam"));
			if(!resultJS.getBoolean("result")){
				return new Result(false, resultJS.getString("message"));
			}
		}
		//给仓库集群主机docker服务指定仓库
//		resultJS = this.updateAndStartDocker(jo.getString("standHost_Id"),registryLb.getDomainName(),registryLb.getLbPort());
//		if(!resultJS.getBoolean("result")){
//			return resultJS;
//		}
		
		if (registryLbService.createRegistryLb(registryLb) <= 0) {
			return new Result(false, "仓库创建失败，数据库操作失败");
		}
		this.insertRegistry(params);
		if (param2 != null) {
			this.insertRegistry(param2);
		}
		try {
	//			CompPort comport = new CompPort();
	//			comport.setCompId(jo.getString("nginx_Id"));
	//			comport.setPortId(jo.getString("nginx_port"));
	//			comport.setUsedId(id);
	//			comport.setUsedType(Type.COMPPORT_TYPE.REGISTRY.toString());
	//			comport.setId(KeyGenerator.uuid());
	//			int i = portService.insertCompPort(comport);
	//			if (i == 0) {
	//				jo.put("result", false);
	//				jo.put("message", "comport表插入失败");
	//				return jo;
	//			}
	//			List<CompPort> compPortsIn = new ArrayList<CompPort>();
	//			compPortsIn.add(comport);
	//			Result result = compPortManager.updateCompPort(compPortsIn, id,
	//					Type.COMPPORT_TYPE.REGISTRY.toString(),(byte)1);
				
			// 更新负载
			Component component = new Component();
			// 两种类型nginx
			if (Lbtype.equals("1")) {
				registryLb.setComponentId(jo.getString("nginx_Id"));
				registryLb.setLbType((byte) 1);
				component.setComponentId(jo.getString("nginx_Id"));
				component = componentService.getComponent(component);
				registryLbService.updateRegistryLb(registryLb);
			}
			if (Lbtype.equals("2")) {
				// F5
				registryLb.setComponentId(jo.getString("f5_Id"));
				registryLb.setLbType((byte) 2);
				component.setComponentId(jo.getString("f5_Id"));
				component = componentService.getComponent(component);
				registryLbService.updateRegistryLb(registryLb);
			}
			if(component==null){
				logger.error("创建仓库【"+jo.getString("reg_name")+"】集群失败,负载信息获取失败未更新");
				return new Result(false, "创建仓库【"+jo.getString("reg_name")+"】集群失败,负载信息获取失败未更新");
			}
			//单个nginx组件
			if(component.getComponentType()==1){
				Result result = compPortManager.modifyNginxConfig(jo.getString("nginx_Id"),jo.getString("nginx_port"),id,Type.COMPPORT_TYPE.REGISTRY.toString());
				if(!result.isSuccess()){
					return result ;
				}
			}
			//nginx组组件
			if(component.getComponentType()==6){
				Result result = compPortManager.modifyNginxGroup(jo.getString("nginx_Id"),jo.getString("nginx_port"),id,Type.COMPPORT_TYPE.REGISTRY.toString());
				if(!result.isSuccess()){
					return result;
				}
			}
			//f5组件
			if(component.getComponentType()==2){
				ComponentExpand componentExpand = componentExpandService.selectByCompId(jo.getString("f5_Id"));
				//是否自动配置
				if(componentExpand.getAutoConfigF5()==1){
					// F5测试
					F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
							Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
					boolean re = detector.normal();
					if(!re){
						return new Result(false, "F5服务器通信失败，无法自动配置！");
					}else{
						F5Model f5Model = new F5Model();
						f5Model.setHostIp(componentExpand.getF5ExternalIp());
						f5Model.setUser(component.getComponentUserName());
						f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
						f5Model.setPort(componentExpand.getF5ExternalPort()==null?"":componentExpand.getF5ExternalPort().toString());
						StringBuffer members = new StringBuffer();
						if(StringUtils.isNotEmpty(jo.getString("host_Id"))&&StringUtils.isNotEmpty(jo.getString("host_port"))){
							Host host = hostService.getHost(jo.getString("host_Id"));
							members.append(host.getHostIp()+":"+jo.getString("host_port")+";");
						}
						if(StringUtils.isNotEmpty(jo.getString("standHost_Id"))&&StringUtils.isNotEmpty(jo.getString("standHost_port"))){
							Host host = hostService.getHost(jo.getString("standHost_Id"));
							members.append(host.getHostIp()+":"+jo.getString("standHost_port")+";");
						}
						if(StringUtils.isNotEmpty(members.toString())){
							f5Model.setMembers(members.toString());
							f5Model.setPoolName("sgcc_pool");
							Result result = compPortManager.addF5MembersConfig(f5Model);
							if(!result.isSuccess()){
								return result;
							}
						}
					}
				}
			}
				
		} catch (Exception e) {
			logger.error("仓库创建异常："+e.getMessage());
			return new Result(false, "仓库创建异常："+e.getMessage());
		}
		return new Result(true, "仓库创建成功");

	}

	public RegistryLb registryLbDetails(String id) {
		return registryLbService.registryLbDetails(id);
	}

	public List<Registry> selectRegistryByLbId(String LbId) {
		return registryService.selectRegistryByLbId(LbId);
	}

	public int deleteRegistryLb(String LbId) {
		return registryLbService.deleteRegistryLb(LbId);
	}

	public List<RegistryLb> getAllRegistryLb() {
		return registryLbService.getAllRegistryLb();
	}

	public boolean updateRegistrylb(RegistryLb registryLb) {
		int t = registryLbService.updateRegistryLb(registryLb);

		return t > 0 ? true : false;

	}

	/**
	 * 上传并安装并启动registry
	 * 
	 * @param ip
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean installRegistry(String ip, String username, String password,String hostPort) {
		String file = localConfig.getLocalHostInstallPath() + "registry-rpm/";
		String hostFileLocation = dockerHostConfig.getHostInstallPath();
		String hostFileName = "registry-rpm";
		boolean record = hostCore.installAllFile(ip, username, password,hostPort, file,
				hostFileLocation, hostFileName);
		return record;
	}

//	public Result installDocker(String ip, String username, String password,
//			String dockerIns) {
//		if (dockerIns.equals("0")) {
//			String response = hostCore.dockerInfo(ip, username,
//					Encrypt.decrypt(password, localConfig.getSecurityPath()));
//			if (null == response) {
//				logger.info("Can not connect to host 【" + ip + "】");
//				return new Result(false, "无法连接到主机【" + ip + "】");
//			}
//			String noneFlag = "docker: command not found";
//			String deadFlag = "Cannot connect to the Docker daemon";
//			if (response.contains(noneFlag) || response.contains(deadFlag)) {
//				boolean result = false;
//				if (response.contains(noneFlag)) {
//					result = hostManager.installDocker(
//							ip,
//							username,
//							Encrypt.decrypt(password,
//									localConfig.getSecurityPath()));
//					logger.info("swarm host docker env is installing");
//				}
//				if (result || response.contains(deadFlag)) {
//					hostCore.startDocker(
//							ip,
//							username,
//							Encrypt.decrypt(password,
//									localConfig.getSecurityPath()));
//					logger.info("swarm host docker env is starting");
//				}
//				response = hostCore.dockerInfo(
//						ip,
//						username,
//						Encrypt.decrypt(password,
//								localConfig.getSecurityPath()));
//				if (null == response) {
//					logger.info("Can not connect to host 【" + ip + "】");
//					return new Result(false, "无法连接到主机【" + ip + "】");
//				}
//				if (response.contains(noneFlag)) {
//					logger.info("机器的环境安装docker失败");
//					return new Result(false, "机器的环境安装docker失败");
//				}
//				if (response.contains("Cannot connect to the Docker daemon")) {
//					logger.info("机器的环境不支持docker运行");
//					return new Result(false, "机器的环境不支持docker运行");
//				}
//			}
//		} else {
//			Result result = hostComponentManager.installHostComponent(ip,
//					username,
//					Encrypt.decrypt(password, localConfig.getSecurityPath()),
//					dockerIns);
//			if (!result.isSuccess()) {
//				logger.error(result.getMessage());
//				return new Result(false, result.getMessage());
//			}
//		}
//		logger.info("安装docker成功");
//		return new Result(true, "安装docker成功");
//	}
	public Result registryLbDelete(String id,String userid){
		Result result = new Result();
		Result return_result = registryLbService.registryLbDelete(id);
		if (!return_result.isSuccess()) {
			return return_result;
		}
		portService.deleteCompPortByUsedId(id);
		result.setSuccess(true);
		result.setMessage("删除仓库成功");
		return result;
	}
	public JSONObject updateAndStartDocker(String host_Id, String registryLb_Ip, Integer registryLb_Port,String dockerParam){
		JSONObject jo = new JSONObject();
		Host masterHost = new Host();
		masterHost.setHostId(host_Id);
		masterHost = hostService.getHost(masterHost);
		String password = Encrypt.decrypt(masterHost.getHostPwd(), localConfig.getSecurityPath());
		String re = hostCore.updateAndStartDocker(masterHost.getHostIp(), masterHost.getHostUser(), password,masterHost.getHostPort(),
				registryLb_Ip+":"+registryLb_Port,hostManager.dockerLabels(masterHost.getHostId()),dockerParam);
		if(re.contains("0")){
			masterHost.setHostStatus((byte)Status.DOCKER.NORMAL.ordinal());
			hostService.update(masterHost);
			jo.put("result", true);
			jo.put("message","");
			return jo;
		}else{
			masterHost.setHostStatus((byte)Status.DOCKER.ABNORMAL.ordinal());
			hostService.update(masterHost);
			jo.put("result", false);
			jo.put("message", "主机【"+masterHost.getHostIp()+"】docker启动参数修改失败！");
			return jo;
		}
	}
	public Result validateNginx(String nginxId,String nginxPort){
		Component component = new Component();
		component.setComponentId(nginxId);
		component = componentService.getComponent(component);
		
		//统一nginx中的appName不能重名
		Result result = new Result(true, "");
		String nginxIpString = component.getComponentIp();
		String nginxUserString = component.getComponentUserName();
		String nginxPwdString = Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath());

		SSH nginxSSH = new SSH(nginxIpString, nginxUserString, nginxPwdString);
		String message = null;
		if(nginxSSH.connect()){
			try {
				//排除掉nginx,端口是否被其他进程占用
				String re = nginxSSH.executeWithResult("sudo lsof -i:"+nginxPort+"|grep -a -L nginx", 3000);
				if(org.apache.commons.lang.StringUtils.isNotEmpty(re)&&!"(standard input)\n".equals(re)&&!"(标准输入)\n".equals(re)&&!"(鏍囧噯杈撳叆)\n".equals(re)){
					message = "Nginx主机【"+component.getComponentIp()+"】端口验证异常："+re;
					result = new Result(false, message);
					return result;
				}
				if(re.contains("command not found")){
					message = "Nginx主机【"+component.getComponentIp()+"】检查端口失败："+re;
					result = new Result(false, message);
					return result;
				}
			} catch (Exception e) {
				message = "Nginx主机【"+component.getComponentIp()+"】检查端口失败："+e.getMessage();
				result = new Result(false, message);
				e.printStackTrace();
				return result;
			}
		}else{
			message = "Nginx主机【"+component.getComponentIp()+"】连接不成功！";
			result = new Result(false, message);
			return result;
		}
		return result;
	}
}