/**
 * 
 */
package com.sgcc.devops.web.image;

/**  
 * date：2016年5月18日 下午5:08:22
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：JvmConfig.java
 * description：  
 */
public class JvmConfig {
	private Long Xms = 0l;
	private Long Xmx = 0l;
	private static Long MaxNewSize=128l;
	private static Long MaxPermSize=128l;
	public Long getXms() {
		return Xms;
	}
	public void setXms(Long xms) {
		Xms = xms;
	}
	public Long getXmx() {
		return Xmx;
	}
	public void setXmx(Long xmx) {
		Xmx = xmx;
	}
	public JvmConfig(Long xms, Long xmx) {
		super();
		Xms = xms;
		Xmx = xmx;
	}
	public String getWeblogicJvmConfig(){
		if(Xms>=0&&Xmx>0){
			return "sed -i '2i MEM_ARGS=\"-Xms"+Xms+"m -Xmx"+Xmx+"m -XX:MaxNewSize="+MaxNewSize+"m -XX:MaxPermSize="+MaxPermSize+"m\"' ";
		}
		return "sed -i '2i MEM_ARGS=\"-Xms256m -Xmx1024m -XX:MaxNewSize="+MaxNewSize+"m -XX:MaxPermSize="+MaxPermSize+"m\"' ";
	}
	public String getTomcatJvmConfig(){
		if(Xms>=0&&Xmx>0){
			return "sed -i '2i JAVA_OPTS=\"-Xms"+Xms+"m -Xmx"+Xmx+"m -XX:MaxNewSize="+MaxNewSize+"m -XX:MaxPermSize="+MaxPermSize+"m\"' ";
		}
		return "sed -i '2i JAVA_OPTS=\"-Xms256m -Xmx512m -XX:MaxNewSize="+MaxNewSize+"m -XX:MaxPermSize="+MaxPermSize+"m\"' ";
	}
}
