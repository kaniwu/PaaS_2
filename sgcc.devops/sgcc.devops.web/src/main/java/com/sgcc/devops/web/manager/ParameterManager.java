/**
 * 
 */
package com.sgcc.devops.web.manager;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Parameter;
import com.sgcc.devops.service.ParameterService;

/**  
 * date：2016年4月1日 上午9:31:47
 * project name：sgcc.devops.web
 * @author  panjing
 * @version 1.0   
 * @since JDK 1.7.0_21  
 */
@Component
public class ParameterManager {
	private static Logger logger = Logger.getLogger(SecurityManager.class);
	@Resource
	private ParameterService parameterService;
	
	
	/**
	 * 删除字典
	 * @author panjing
	 */
	public Result deleteParameter(JSONObject jo) {
		String paraId = jo.getString("paraId");
		// 查询要删除字典的信息
		Parameter parameter = new Parameter();
		parameter.setId(paraId);
		parameter = parameterService.getParameter(parameter);
		if (null == parameter) {
			logger.info("删除字典失败：主键为【" + paraId + "】的字典可能已经被其他管理员删除！");
			return new Result(false, "删除字典失败：主键为【" + paraId + "】的字典可能已经被其他管理员删除！");
		}
		try {
			int result = parameterService.deleteByPrimaryKey(paraId);
			if(result == 1){
				logger.info("【" +  parameter.getParaValue() + "】删除成功！");
				return new Result(true, "【" +  parameter.getParaValue() + "】删除成功！");
			}else{
				logger.info("【" +   parameter.getParaValue() + "】删除失败：数据库操作异常！");
				return new Result(false, "【" +   parameter.getParaValue() + "】删除失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("【" +   parameter.getParaValue() + "】删除失败：数据库操作异常！");
			return new Result(false, "【" +  parameter.getParaValue() + "】删除失败：数据库操作异常！");
		}
}



	
	/**
	 * 新建字典
	 * @author panjing
	 */
	public Result createParameter(JSONObject jsonObject) {
		String paraCode = jsonObject.getString("paraCode");
		String paraName = jsonObject.getString("paraName");
		String paraValue = jsonObject.getString("paraValue");
		String paraType = jsonObject.getString("paraType");
		String parentId = jsonObject.getString("parentId");
		String paraDesc = jsonObject.getString("paraDesc");
		Parameter parameter = new Parameter();
		parameter.setParaCode(paraCode);
		parameter.setParaName(paraName);
		parameter.setParaValue(paraValue);
		parameter.setParaType(paraType);
		parameter.setParentId(parentId);
		parameter.setParaDesc(paraDesc);
		parameter.setId(KeyGenerator.uuid());
		//判断不能为空的属性
		if(paraCode == null || paraName == null || paraValue ==null || paraType == null){
			logger.info("编辑字典失败，字典编码或字典名称为空！");
			return new Result(false, "编辑字典失败，字典编码或字典名称为空！");
		}
		try {
			int result =parameterService.insert(parameter);
			if(result == 1){
				logger.info("【" + paraName + "】创建成功！");
				return new Result(true, "【" +  paraName + "】创建成功！");
			}else{
				logger.info("【" +  paraName + "】创建失败：数据库操作异常！");
				return new Result(false, "【" + paraName + "】创建失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("【" + paraName + "】创建失败：数据库操作异常！");
			return new Result(false, "【" + paraName + "】创建失败：数据库操作异常！");
		}
		
		
	}
	

	
	/**
	 * 编辑字典
	 * @author panjing
	 */
	public Result updateParameter(JSONObject jsonObject) {
		String paraId = jsonObject.getString("paraId");
		String paraCode = jsonObject.getString("paraCode");
		String paraName = jsonObject.getString("paraName");
		String paraValue = jsonObject.getString("paraValue");
		String paraType = jsonObject.getString("paraType");
		String parentId = jsonObject.getString("parentId");
		String paraDesc = jsonObject.getString("paraDesc");
		Parameter parameter = new Parameter();
		parameter.setId(paraId);
		parameter.setParaCode(paraCode);
		parameter.setParaName(paraName);
		parameter.setParaValue(paraValue);
		parameter.setParaType(paraType);
		parameter.setParentId(parentId);
		parameter.setParaDesc(paraDesc);
		if(paraId == null || paraCode == null || paraName == null || paraValue ==null || paraType == null){
			logger.info("编辑字典失败，字典编码或字典名称为空！");
			return new Result(false, "编辑字典失败，字典编码或字典名称为空！");
		}
		try {
			int result =parameterService.updateByPrimaryKeySelective(parameter);
			if(result == 1){
				logger.info("【" + paraName + "】修改成功！");
				return new Result(true, "【" +  paraName + "】修改成功！");
			}else{
				logger.info("【" +  paraName + "】修改失败：数据库操作异常！");
				return new Result(false, "【" + paraName + "】修改失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("【" + paraName + "】修改失败：数据库操作异常！");
			return new Result(false, "【" + paraName + "】修改失败：数据库操作异常！");
		}
	}
	
	
	/**
	 * 检查编码是否重复
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public Boolean checkParaCode(String id, String name) {
		Parameter parameter = new Parameter();
		parameter.setParaCode(name);
		parameter = parameterService.selectParameterByCode(parameter);
		if (null == parameter) {
			return true;
		}
		if (id == null || id.equals("")) {
			return false;
		} else {
			return parameter.getId().equals(id);
		}
	}




	/**
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2016年6月20日
	*/
	public boolean hasChild(String treeId) {
		JSONArray ja = parameterService.selectByLocation(treeId);
		if(null!=ja&&ja.size()>0){
			return true;
		}
		return false;
	}

}
