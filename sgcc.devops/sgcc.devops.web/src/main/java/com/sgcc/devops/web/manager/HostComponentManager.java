/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.DockerHostConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.model.HostComponentModel;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.HostInstall;
import com.sgcc.devops.service.HostComponentService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.RackService;
import com.sgcc.devops.service.ServerRoomService;
import com.sgcc.devops.web.image.Encrypt;

import net.sf.json.JSONObject;

@Component
public class HostComponentManager {
	private static Logger logger = Logger.getLogger(SecurityManager.class);
	@Resource
	private ServerRoomService serverRoomService;
	@Resource
	private HostComponentService hostComponentService;
	@Resource
	private RackService rackService;
	@Resource
	private HostService hostService;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private TmpHostConfig tmpHostConfig ;
	@Autowired
	private DockerHostConfig dockerHostConfig;
	@Resource
	private HostCore hostCore;

	public Result deleteHostComponent(JSONObject jo) {
		String hostComponentId = jo.getString("id");
		// 查询要删除机架的信息
		HostComponent hostComponent = new HostComponent();
		hostComponent.setId(hostComponentId);
		hostComponent = hostComponentService.getHostComponent(hostComponent);
		//若查询出来的机架为null，则机架有可能已经被其他管理员删除
		if (null == hostComponent) {
			logger.info("删除失败：主键为【" + hostComponentId + "】的可能已经被其他管理员删除！");
			return new Result(false, "删除机架失败：主键为【" + hostComponentId + "】可能已经被其他管理员删除！");
		}
		try {
			if(tmpHostConfig.isTransferNeeded()){
				String tmpIp = tmpHostConfig.getTmpHostIp();
				String tmpUser = tmpHostConfig.getTmpHostUser();
				String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
				SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
				if(ssh.connect()){
					try {
						ssh.rmFile(hostComponent.getLocalPath());
						ssh.close();
					} catch (Exception e) {
						ssh.close();
						e.printStackTrace();
					}
				}
			}
			int result =hostComponentService.deleteByPrimaryKey(hostComponentId);
			if(result == 1){
				logger.info("删除成功！");
				return new Result(true, "删除成功！");
			}else{
				logger.info("删除失败：数据库操作异常！");
				return new Result(false, "删除失败：数据库操作异常！");
			}
		} catch (Exception e) {
			logger.info("删除失败：数据库操作异常！");
			return new Result(false, "删除失败：数据库操作异常！");
		}
	
}
	public HostComponent selectById(String id) {
		return  hostComponentService.selectById(id);
	}
	public Result createHostComponent(HostComponentModel hostComponentModel) {
		String version = hostComponentModel.getVersion();
		if(version == null){
			logger.info("添加主机组件失败，版本为空！");
			return new Result(false, "添加主机主机失败，版本为空");
		}
		//判断机房名称是否重复
		boolean repeat = hostComponentService.ifRepeat(hostComponentModel);
		if(repeat){
			logger.info("添加主机组件失败，名称不能重复！");
			return new Result(false, "添加主机组件失败，名称不能重复！");
		}else{
			try {
//				hostComponentModel.setLocalPath(localConfig.getLocalHostInstallPath());
				int result =hostComponentService.insert(hostComponentModel);
				if(result == 1){
					logger.info( "创建成功！");
					return new Result(true,"创建成功！");
				}else{
					logger.info( "创建失败：数据库操作异常！");
					return new Result(false, "创建失败：数据库操作异常！未能创建");
				}
			} catch (Exception e) {
				logger.info("创建失败：数据库操作异常！");
				return new Result(false, "创建失败：数据库操作异常！");
			}
			
		}
	}
	

	public Result updateHostComponent(HostComponentModel hostComponentModel) {
			try {
				HostComponent hostComponent = new HostComponent();
				hostComponent.setId(hostComponentModel.getId());
				hostComponent.setFilename(hostComponentModel.getFilename());
				hostComponent.setVersion(hostComponentModel.getVersion());
				hostComponent.setInstallCommand(hostComponentModel.getInstallCommand());
				hostComponent.setStatusCommand(hostComponentModel.getStatusCommand());
				hostComponent.setVersionCommand(hostComponentModel.getVersionCommand());
				hostComponent.setUpgradeCommand(hostComponentModel.getUpgradeCommand());
				hostComponent.setTransfer(hostComponentModel.getTransfer());
				int result =hostComponentService.updateByPrimaryKeySelective(hostComponent);
				
				if(result == 1){
					logger.info("修改成功！");
					return new Result(true, "修改成功！");
				}else{
					logger.info("修改失败：数据库操作异常！");
					return new Result(false, "修改失败：数据库操作异常！");
				}
			} catch (Exception e) {
				logger.info("修改失败：数据库操作异常！");
				return new Result(false, "修改失败：数据库操作异常！");
			}
//		}
	}
	/**
	* 主机安装组件
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年4月14日
	*/
	public List<HostInstall> selectHostInstallByHostId(String hostId) {
		List<HostInstall> hostInstalls = hostComponentService.selectHostInstallByHostId(hostId);
		return hostInstalls;
	}
	/**
	 * 根据主机组件安装
	 * @param hostIp
	 * @param hostUser
	 * @param decrypt
	 * @param hostComponentId
	 * @return
	 */
	public Result installHostComponent(String hostId,String hostIp, String hostUser, String decrypt,String hostComponentId) {
		HostComponent hostComponent = hostComponentService.selectById(hostComponentId);
		String localFilePath = hostComponent.getLocalPath();
		String fileName = hostComponent.getFilename();
		if(!StringUtils.isEmpty(fileName)){
			localFilePath = (localFilePath+"/"+fileName).replace("\\", "/").replace("//", "/");
		}
		String hostFileLocation=(dockerHostConfig.getHostInstallPath()+"/"+hostComponent.getId()).replace("\\", "/").replace("//", "/");
		StringBuffer command = new StringBuffer();
		command.append(hostComponent.getInstallCommand());
		if(hostComponent.getInstallCommand().toString().trim().endsWith(";")){
			command.append(" echo $?;");
		}else{
			command.append(" ; echo $?;");
		}
		String tmpIp = tmpHostConfig.getTmpHostIp();
		String tmpUser = tmpHostConfig.getTmpHostUser();
		String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
		String tmpPort = tmpHostConfig.getTmpHostPort();
		SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd,Integer.parseInt(tmpPort));
		String localHostPath = (localConfig.getLocalHostInstallPath()+"/"+hostComponent.getId()).replace("\\", "/").replace("//", "/");
		
		String isok = hostCore.hostInstallSoftware(ssh,hostIp, hostUser, decrypt,tmpPort,localHostPath,fileName,localFilePath, hostFileLocation, command.toString(), 100000L);
		if (null == isok || isok.contains(hostIp) || isok.contains("error")) {
			return new Result(false, "主机【"+hostIp+"】安装组件执行结果："+isok);
		}
		if(isok.trim().endsWith("0")){
			HostInstall hostInstall = new HostInstall();
			if(StringUtils.isNotEmpty(hostId)&&StringUtils.isNotEmpty(hostComponentId)){
				hostInstall.setHostId(hostId);
				hostInstall.setHostComId(hostComponentId);
				hostService.insert(hostInstall);
			}
			return new Result(true, "主机【"+hostIp+"】安装组件执行成功："+isok);
		}else{
			return new Result(false, "主机【"+hostIp+"】安装组件执行结果："+isok);
		}
	}
}
