package com.sgcc.devops.web.elasticity.bean;

import com.sgcc.devops.web.elasticity.enums.ElasticDecision;

/**
 * 决策结果类
 * 
 * @author dmw
 *
 */
public class DecisionResult {

	private ElasticDecision decision;// 弹性决定
	private Integer step;// 弹性步长

	public ElasticDecision getDecision() {
		return decision;
	}

	public void setDecision(ElasticDecision decision) {
		this.decision = decision;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public DecisionResult(ElasticDecision decision, Integer step) {
		super();
		this.decision = decision;
		this.step = step;
	}

	@Override
	public String toString() {
		return "DecisionResult [decision=" + decision + ", step=" + step + "]";
	}

	public DecisionResult() {
		super();
		// TODO Auto-generated constructor stub
	}

}
