package com.sgcc.devops.web.image;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.web.image.material.ImageMaterial;

/**
 * 镜像制作接口
 * 
 * @author dmw
 *
 */
public interface ImageBuilder {

	/**
	 * 制作镜像
	 * 
	 * @param material
	 *            镜像制作材料
	 * @return 镜像制作结果 {success:true|false,message:"reason|imageId"}
	 */
	public Result build(ImageMaterial material);
}
