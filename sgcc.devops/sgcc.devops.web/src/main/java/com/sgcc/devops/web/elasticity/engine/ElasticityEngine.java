package com.sgcc.devops.web.elasticity.engine;

import com.sgcc.devops.common.bean.Result;

/**
 * 弹性伸缩引擎 对外提供系统的收缩和扩展服务接口
 * 
 * @author dmw
 *
 */
public interface ElasticityEngine {

	/**
	 * 系统收缩
	 * 
	 * @return
	 */
	public Result shrink();

	/**
	 * 系统扩展
	 * 
	 * @return
	 */
	public Result expand();
}
