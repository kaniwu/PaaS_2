/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.xbill.DNS.Message;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.constant.Weights;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.DeployModel;
import com.sgcc.devops.common.model.F5Model;
import com.sgcc.devops.common.model.SystemAppModel;
import com.sgcc.devops.common.model.SystemModel;
import com.sgcc.devops.common.util.DnsZone;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.intf.ContainerCore;
import com.sgcc.devops.core.intf.SystemCore;
import com.sgcc.devops.dao.entity.AppPrelib;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.CompPort;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.ConPort;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.entity.DeployFile;
import com.sgcc.devops.dao.entity.Driver;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.Image;
import com.sgcc.devops.dao.entity.Location;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.dao.entity.Port;
import com.sgcc.devops.dao.entity.RegImage;
import com.sgcc.devops.dao.entity.RegistryLb;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.RegImageMapper;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ConportService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployDatabaseService;
import com.sgcc.devops.service.DeployFileService;
import com.sgcc.devops.service.DeployNgnixService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.DriverService;
import com.sgcc.devops.service.ElasticStrategyService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.ImageService;
import com.sgcc.devops.service.LocationService;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.RegImageService;
import com.sgcc.devops.service.RegistryLbService;
import com.sgcc.devops.service.SystemAppService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.daemon.PlatformDaemon;
import com.sgcc.devops.web.image.ConfigFile;
import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.JvmConfig;
import com.sgcc.devops.web.image.TomcatConstants;
import com.sgcc.devops.web.image.WeblogicContstants;
import com.sgcc.devops.web.image.builder.JavaImageBuilder;
import com.sgcc.devops.web.image.builder.TomcatImageBuilder;
import com.sgcc.devops.web.image.builder.WeblogicImageBuilder;
import com.sgcc.devops.web.image.material.ImageMaterial;
import com.sgcc.devops.web.task.TaskCache;
import com.sgcc.devops.web.task.TaskMessage;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:59:53
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：SystemManager.java
 * description：  物理系统版本发布 、版本更新、版本下线、版本删除操作流程处理类
 */
@org.springframework.stereotype.Component
public class SystemManager {
	@Resource
	private RegImageService regImageService;
	@Resource
	private RegistryLbService registryLbService;
	@Resource
	private ComponentService componentService;
	@Resource
	private ComponentExpandService componentExpandService;
	@Resource
	private ClusterService clusterService;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private HostService hostService;
	@Resource
	private ImageService imageService;
	@Resource
	private SystemAppService systemAppService;
	@Resource
	private ContainerService containerService;
	@Resource
	private DeployService deployService;
	@Resource
	private SystemService systemService;
	@Resource
	private ConportService conportService;
	@Autowired
	private TaskCache taskCache;
	@Resource
	private RegImageMapper regImageMapper;
	@Autowired
	private ContainerCore containerCore;
	@Resource
	private DeployNgnixService deployNgnixService;
	@Resource
	private TomcatImageBuilder tomcatImageBuilder;
	@Resource
	private WeblogicImageBuilder weblogicImageBuilder;
	@Resource
	private JavaImageBuilder javaImageBuilder;
	@Resource
	private DeployDatabaseService deployDatabaseService;
	@Resource
	private LocalConfig localConfig;
	@Resource
	private DriverService driverService;
	@Resource
	private CompPortManager compPortManager;
	@Resource
	private ContainerManager containerManager;
	@Resource
	private ElasticStrategyService elasticStrategyService;
	@Resource
	private DeployFileService deployFileService;
	@Resource
	private PortService portService;
	@Resource
	private SystemAppService appService;
	@Resource
	private SystemCore systemCore;
	@Resource
	private PlatformDaemon platformDaemon;
	@Resource
	private LocationService locationService;
	@Resource
	private MonitorService monitorService;
	@Resource
	private MonitorManager monitorManager;
	private StringBuffer taskMessage ;
	private static Logger logger = Logger.getLogger(SystemManager.class);
	
	/**
	 *  手动切换时，1、容器不启动，2、nginx不更新，3、上一版本的容器不用停，4、物理系统在运行的版本不变
	 * 
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年9月23日
	 */
	public Result deploy(DeployModel deployModel, User user) {
		try{
			deployModel.setCreator(user.getUserId());
			taskMessage = new StringBuffer();
			taskMessage.append("应用发布开始...");
			TaskMessage begin = new TaskMessage(taskMessage.toString(), 1, user.getUserId(),deployModel.getSystemId());
			taskCache.updateTask(begin);
			System system = systemService.getSystemById(deployModel.getSystemId());
			Deploy oldDeploy = null;
			//停止当前运行的物理系统弹性
			if(null!= system&&Status.ElSASTICITYSTATUS.USED.ordinal()==system.getSystemElsasticityStatus()){
				system.setSystemElsasticityStatus((byte)Status.ElSASTICITYSTATUS.UNUSED.ordinal());
				systemService.updateSystem(system);
			}
			//当前运行的版本
			List<Container> containertmep = containerService.getBySysId(deployModel.getSystemId());
			if (system!=null&&system.getDeployId() != null) {
				oldDeploy = deployService.getDeploy(system.getDeployId());
//				if (null != oldDeploy) {
//					containertmep = containerService.selectContainersByImageId(oldDeploy.getAppImageId());
//				}
			}
			// 数据库连接信息
			String databaseInfosJsonStr = deployModel.getDatabaseInfo();
			JSONArray dataInfoArray = JSONArray.fromObject(databaseInfosJsonStr);
			List<DeployDatabase> deployDatabases =this.databaseMeterial(dataInfoArray);
			//负载信息校验
			String loadBalanceId =deployModel.getLoadBalanceId();
			Component component = new Component();
			component.setComponentId(loadBalanceId);
			component = componentService.getComponent(component);
			//如果是nginx组或者单个nginx组件，判断是否端口可用
			if(component.getComponentType()==1||component.getComponentType()==6){
				CompPort compPort = new CompPort();
				compPort.setCompId(loadBalanceId);
				compPort.setPortId(deployModel.getNginxPortId());
				compPort.setUsedId(deployModel.getDeployId());
				compPort.setUsedType(Type.COMPPORT_TYPE.DEPLOY.toString());
				//判断dns或者Ip端口是否重名
				Result validate = compPortManager.validateNginxPort(deployModel,compPort);
				if(!validate.isSuccess()){
					return validate;
				}
			}
			
			//远程配置文件地址
			String remoteConfigPath = (tmpHostConfig.getDeployPath()+deployModel.getSystemId()+"/"+ deployModel.getTimestamp() + "/").trim().replace("\\", "/").replace("//", "/");
			deployModel.setAppRoute(remoteConfigPath);
			deployModel.setConfigRoute(remoteConfigPath);
			/**
			 * 新版本是否使用了当前版本相关的应用包与配置文件，如果使用拷贝到新版本目录下
			 */
			String systemAppId = deployModel.getSystemAppId();
			SystemApp systemApp = systemService.getAppPackageById(systemAppId);
			deployModel.setAppCategory(systemApp.getAppCategory());
			if("1".equals(systemApp.getAppCategory())){//非自定义镜像
				Result copyfileResult = this.copyFile(deployModel,system,deployDatabases);
				if(!copyfileResult.isSuccess()){
					return copyfileResult;
				}
			}
			JSONArray containerArray = null;
			//判断是否是新发布版本，如果不是新发布版本，不需要重新制作镜像
			String newDeployId =deployModel.getDeployId();
			if(null!=oldDeploy&&oldDeploy.getDeployVersion().equals(deployModel.getDeployVersion())){
				taskMessage.append("<br/>");
				taskMessage.append("应用配置未修改，不需要重新制作应用镜像...");
				TaskMessage overSystemDaemon = new TaskMessage(taskMessage.toString(), 2, user.getUserId(),deployModel.getSystemId());
				taskCache.updateTask(overSystemDaemon);
				//容器数量是否有增减
				if(deployModel.getInstanceCount()>oldDeploy.getInstanceCount()){
					taskMessage.append("<br/>");
					taskMessage.append("新增应用实例容器...");
					TaskMessage addContainer = new TaskMessage(taskMessage.toString(), 50, user.getUserId(),deployModel.getSystemId());
					taskCache.updateTask(addContainer);
					Result result = containerManager.createConByDeployId(oldDeploy.getDeployId(), deployModel.getInstanceCount()-oldDeploy.getInstanceCount(), user.getUserId());
					if(!result.isSuccess()){
						logger.error( "新增应用实例失败："+result.getMessage());
						return new Result(false, "新增应用实例失败："+result.getMessage());
					}
				}else if(deployModel.getInstanceCount()<oldDeploy.getInstanceCount()){
					SecureRandom secureRandom = new SecureRandom();
					List<Integer> indexList = new ArrayList<Integer>();
					logger.info("start to get random index of container list");
					while (indexList.size() < oldDeploy.getInstanceCount()-deployModel.getInstanceCount()) {
						int number = Math.abs(secureRandom.nextInt() % oldDeploy.getInstanceCount());
						if (indexList.contains(number)) {
							continue;
						} else {
							indexList.add(number);
						}
					}
					List<String> idList = new ArrayList<String>(oldDeploy.getInstanceCount()-deployModel.getInstanceCount());
					for (Integer index : indexList) {
						idList.add(containertmep.get(index).getConId());
					}
					JSONArray rmcontainers = new JSONArray();
					rmcontainers.addAll(idList);
					JSONObject params = new JSONObject();
					params.put("systemId", deployModel.getSystemId());
					params.put("containerId", rmcontainers);
					taskMessage.append("<br/>");
					taskMessage.append("删除应用实例容器...");
					TaskMessage rmContainer = new TaskMessage(taskMessage.toString(), 50, user.getUserId(),deployModel.getSystemId());
					taskCache.updateTask(rmContainer);
					Result result = containerManager.removeContainer(params);
					if(!result.isSuccess()){
						logger.error( "删除应用实例失败："+result.getMessage());
						return new Result(false, "删除应用实例失败："+result.getMessage());
					}
				}
				List<Container> containers = containerService.getBySysId(oldDeploy.getSystemId());
				containerArray = JSONUtil.parseObjectToJsonArray(containers);
			}else{
				try{
					//制作镜像-----------开始
					String newAppImageId = "";
					boolean issuccess = false;
					if("2".equals(systemApp.getAppCategory())){//自定义镜像
						taskMessage.append("<br/>");
						taskMessage.append("自定义镜像...");
						newAppImageId = systemApp.getAppRoute();
					}else{
						JSONObject makeImageJo = this.makeAppImage(deployModel, user,system);
						issuccess = makeImageJo.getBoolean("issuccess");
						if(!issuccess){
							String makeImageResult = makeImageJo.getString("result");
							return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败："+makeImageResult);
						}
						newAppImageId = makeImageJo.getString("InsertImgId");
					}
					//制作镜像-----------结束
					//启动容器-----------开始 
					taskMessage.append("<br/>");
					taskMessage.append("启动容器...");
					TaskMessage startConTask = new TaskMessage(taskMessage.toString(), 60, user.getUserId(),deployModel.getSystemId());
					taskCache.updateTask(startConTask);
					JSONObject startConJo = this.startContainersByImage(deployModel,user,newAppImageId);
					issuccess = startConJo.getBoolean("issuccess");
					if(!issuccess){
						String startConResult = startConJo.getString("result");
						logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败："+startConResult);
						return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败："+startConResult);
					}
					containerArray = startConJo.getJSONArray("containerArray");
					
					taskMessage.append("<br/>");
					taskMessage.append("容器启动成功...");
					TaskMessage startConOk = new TaskMessage(taskMessage.toString(), 80, user.getUserId(),deployModel.getSystemId());
					taskCache.updateTask(startConOk);
					//启动容器-----------结束
					
					//1、保存容器
					try{
						this.addContainer(containerArray, deployModel.getSystemId(), user);
					}catch(Exception e){
						logger.error("容器数据存储失败："+e);
						return new Result(false, "容器数据存储失败："+e);
					}
					//2、保存deploy
					try{
						newDeployId = this.saveDeployInfo(deployModel, newAppImageId);
						if (StringUtils.isEmpty(newDeployId)) {
							logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,基本部署数据保存失败！");
							return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,基本部署数据保存失败！");
						}
					}catch(Exception e){
						logger.error("容器数据存储失败："+e);
						return new Result(false, "容器数据存储失败："+e);
					}
					// 3、保存deploy_database,创建deployDatabase关联关系
					int databaseResult = this.createDeployDatabase(newDeployId, deployDatabases);
					if (databaseResult == 0 && deployDatabases!=null && deployDatabases.size()>0) {
						logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存应用数据库信息失败！");
						return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存应用数据库信息失败！");
					}
					//4、更新应用用镜像dop_image，dop_reg_img
					int imageResult = this.updateImage(newAppImageId, newDeployId, deployModel);
					if (imageResult == 0) {
						logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存应用镜像信息失败！");
						return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存应用镜像信息失败！");
					}
					//5、上传配置文件dop_deploy_file
					String deployFileStr = deployModel.getDeployFiles();
					int fileResult = this.saveDeployFile(deployFileStr,newDeployId);
					if (fileResult == 0) {
						logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存配置文件信息失败!");
						return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,保存配置文件信息失败!");
					}
					//如果自动切换版本
					if (2==deployModel.getAutoDeploy()) {
						// 物理系统更新运行的版本,自动切换的时候执行
						system.setSystemDeployStatus((byte) Status.SYSTEM.DEPLOY.ordinal());
						system.setDeployId(newDeployId);
						int systemResult = systemService.updateSystem(system);
						if (systemResult == 0) {
							logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,物理系统更新运行的版本信息失败!");
							return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署失败,物理系统更新运行的版本信息失败!");
						}
						taskMessage.append("<br/>");
						taskMessage.append("删除上一版本的容器...");
						TaskMessage rmLastConTask = new TaskMessage(taskMessage.toString(), 80, user.getUserId(),deployModel.getSystemId());
						taskCache.updateTask(rmLastConTask);
						Result removeContaineresult = this.removeLastVersionContainers(containertmep);
						if(!removeContaineresult.isSuccess()){
							logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署完成，删除上一版本容器失败!");
							return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署完成，删除上一版本容器失败!");
						}
						//如果上一版本的负载与新部署使用的负载不是同一个，需要把原负载配置更新
						if(null!=oldDeploy&&!oldDeploy.getLoadBalanceId().equals(loadBalanceId)){
							Component oldComponent = new Component();
							oldComponent.setComponentId(oldDeploy.getLoadBalanceId());
							oldComponent = componentService.getComponent(oldComponent);
							
							//单个nginx组件删除容器，重新配置
							if(oldComponent.getComponentType()==1){
								Result result = compPortManager.modifyNginxConfig(oldComponent.getComponentId(),null,oldDeploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
								if(!result.isSuccess()){
									logger.error(result.getMessage());
									return result;
								}
							}
							//nginx组组件删除容器，重新配置
							if(oldComponent.getComponentType()==6){
								Result result = compPortManager.modifyNginxGroup(oldComponent.getComponentId(),null,oldDeploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
								if(!result.isSuccess()){
									logger.error(result.getMessage());
									return result;
								}
							}
							//f5组件删除容器member
							if(oldComponent.getComponentType()==2){
								ComponentExpand componentExpand = componentExpandService.selectByCompId(oldDeploy.getLoadBalanceId());
								//是否自动配置
								if(componentExpand.getAutoConfigF5()==1){
									// F5测试
									F5Detector detector = new F5Detector(oldComponent.getComponentIp(), oldComponent.getComponentUserName(),
											Encrypt.decrypt(oldComponent.getComponentPwd(), localConfig.getSecurityPath()));
									boolean re = detector.normal();
									if(!re){
										return new Result(false, "F5服务器通信失败，无法自动配置！");
									}else{
										F5Model f5Model = new F5Model();
										f5Model.setHostIp(oldComponent.getComponentIp());
										f5Model.setUser(oldComponent.getComponentUserName());
										f5Model.setPwd(Encrypt.decrypt(oldComponent.getComponentPwd(), localConfig.getSecurityPath()));
										f5Model.setPort(oldDeploy.getF5ExternalPort());
										StringBuffer members = new StringBuffer();
										for(Container container:containertmep){
											List<ConPort> conPorts = conportService.listConPorts(container.getConId());
											for(ConPort conPort:conPorts){
												members.append(conPort.getConIp()+":"+conPort.getPubPort()+";");
											}
										}
										if(StringUtils.isNotEmpty(members.toString())){
											f5Model.setMembers(members.toString());
											f5Model.setPoolName("sgcc_pool");
											Result result = compPortManager.delF5MembersConfig(f5Model);
											if(!result.isSuccess()){
												return result;
											}
										}
									}
								}
							}
						}
					}
				}catch(Exception e){
					logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署过程异常："+e);
					return new Result(false, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署过程异常："+e);
				}
			}
			//更新负载-----------开始 
			taskMessage.append("<br/>");
			taskMessage.append("更新负载...");
			TaskMessage loadBalanceTask = new TaskMessage(taskMessage.toString(), 90, user.getUserId(),deployModel.getSystemId());
			taskCache.updateTask(loadBalanceTask);
			try{
				byte loadBalanceType = 0;
				String nginxPort="";
				String f5Port="";
				String f5Ip="";
				boolean flag = false;
				if (null != oldDeploy) {
					//判断是否需要更新
					loadBalanceType = oldDeploy.getLoadBalanceType();
					loadBalanceId = oldDeploy.getLoadBalanceId();
					nginxPort = oldDeploy.getNginxPort();
					f5Port = oldDeploy.getF5ExternalPort();
					f5Ip = oldDeploy.getF5ExternalIp();
					
					//nginx组或则f5
					if(2==deployModel.getLoadBalanceType()&&loadBalanceType==deployModel.getLoadBalanceType()
							&&loadBalanceId.equals(deployModel.getLoadBalanceId())
							&&nginxPort.equals(deployModel.getNginxPort())){
						flag = true;
					}else if(1==deployModel.getLoadBalanceType()&&loadBalanceType==deployModel.getLoadBalanceType()
							&&loadBalanceId.equals(deployModel.getLoadBalanceId())
							&&f5Ip.equals(deployModel.getF5ExternalIp())&&f5Port.equals(deployModel.getF5ExternalPort())){
						flag = true;
					}
				}
				//如果没有新容器创建，并且负载也未改变，则不需要变更负载配置
				if(flag&&(null==containerArray||containerArray.size()==0)){
				}else{
					Deploy deployUpdate = deployService.getDeploy(newDeployId);
					deployUpdate.setDomainName(deployModel.getDomainName());
					deployUpdate.setHttp(deployModel.getHttp());
					deployUpdate.setPort(deployModel.getPort());
					deployUpdate.setF5ExternalIp(deployModel.getF5ExternalIp());
					deployUpdate.setF5ExternalPort(deployModel.getF5ExternalPort());
					deployUpdate.setNginxPort(deployModel.getNginxPort());
					deployUpdate.setLoadBalanceId(deployModel.getLoadBalanceId());
					deployUpdate.setLocationLeavel(deployModel.getLocationLeavel());
					deployUpdate.setRegularlyPublish(deployModel.getRegularlyPublish());
					deployUpdate.setPublishTime(deployModel.getPublishTime());
					deployUpdate.setLoadBalanceType(deployModel.getLoadBalanceType());
					deployUpdate.setUrl(deployModel.getUrl());
					deployUpdate.setLocationStragegy(deployModel.getLocationStragegy());
					
					if(deployModel.isDnsEnable()){
						deployUpdate.setDnsEnable((byte)1);
					}else{
						deployUpdate.setDnsEnable((byte)0);
					}
					if(deployModel.isSyncDnsEnable()){
						deployUpdate.setSyncDnsEnable((byte)1);
					}else{
						deployUpdate.setSyncDnsEnable((byte)0);
					}
					deployUpdate.setDnsComponentId(deployModel.getDnsComponentId());
					deployUpdate.setSonZone(deployModel.getSonZone());
					
					deployService.updateDeploy(deployUpdate);
					//单个nginx组件
					if(component.getComponentType()==1){
						Result result = compPortManager.modifyNginxConfig(component.getComponentId(),deployModel.getNginxPortId(),newDeployId,Type.COMPPORT_TYPE.DEPLOY.toString());
						if(!result.isSuccess()){
							logger.error(result.getMessage());
							return result;
						}
					}
					//nginx组组件
					if(component.getComponentType()==6){
						Result result = compPortManager.modifyNginxGroup(component.getComponentId(),deployModel.getNginxPortId(),newDeployId,Type.COMPPORT_TYPE.DEPLOY.toString());
						if(!result.isSuccess()){
							logger.error(result.getMessage());
							return result;
						}
					}
					//f5组件
					if(component.getComponentType()==2){
						ComponentExpand componentExpand = componentExpandService.selectByCompId(deployModel.getLoadBalanceId());
						//是否自动配置
						if(componentExpand.getAutoConfigF5()==1){
							// F5测试
							F5Detector detector = new F5Detector(component.getComponentIp(), component.getComponentUserName(),
									Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
							boolean re = detector.normal();
							if(!re){
								return new Result(false, "F5服务器通信失败，无法自动配置！");
							}else{
								F5Model f5Model = new F5Model();
								f5Model.setHostIp(component.getComponentIp());
								f5Model.setUser(component.getComponentUserName());
								f5Model.setPwd(Encrypt.decrypt(component.getComponentPwd(), localConfig.getSecurityPath()));
								f5Model.setPort(deployModel.getF5ExternalPort());
								StringBuffer members = new StringBuffer();
								if (containerArray.size() > 0) {
									// String conIds = "";
									for (int i = 0; i < containerArray.size(); i++) {
										JSONObject containerJo = containerArray.getJSONObject(i);
										try {
											JSONArray ports = (JSONArray) containerJo.get("ports");
											if (!ports.isEmpty()) {
												String hostIp = ((JSONObject) ports.get(0)).getString("ip");
												for (int j = 0; j < ports.size(); j++) {
													JSONObject port = (JSONObject) ports.get(j);
													String conPort = String.valueOf(port.getInt("publicPort"));
													members.append(hostIp+":"+conPort+";");
												}
											}
										} catch (Exception e) {
											logger.info("F5配置异常 "+e.getMessage());
										}

									}
								}
								if(StringUtils.isNotEmpty(members.toString())){
									f5Model.setMembers(members.toString());
									f5Model.setPoolName("sgcc_pool");
									Result result = compPortManager.addF5MembersConfig(f5Model);
									if(!result.isSuccess()){
										return result;
									}
								}
							}
						}
					}
					//同步DNSzone
					if(deployModel.isSyncDnsEnable()){
						Component c = new Component();
						c.setComponentId(deployModel.getDnsComponentId());
						c = componentService.getComponent(c);
						DnsZone dnsZone = new DnsZone(c.getComponentIp(), c.getKey(), c.getSecret(), c.getDnsZone()+".");
						String ip = dnsZone.queryHostIpByDns(deployModel.getDomainName());
						if(component.getComponentType()==6){//启用负载是nginx组 ，暂实现nginx
							String syncIp = "";
							List<ComponentHost> nginxHosts = hostService.selectComponentHost(component.getComponentId());
							if(nginxHosts.size() > 0 ){
								syncIp = nginxHosts.get(0).getHostIp();
							}
							if(ip == null ){
								Message dnsMessage = dnsZone.addHostIpAndDNSToZone(deployModel.getSonZone(),syncIp);
								java.lang.System.out.println(dnsMessage);
							}else {
								dnsZone.deleteHostIpAndDNSToZone(deployModel.getSonZone(),ip);
								Message dnsMessage = dnsZone.addHostIpAndDNSToZone(deployModel.getSonZone(),syncIp);
								java.lang.System.out.println(dnsMessage);
							}
						}
					}
					
				}
			}catch(Exception e){
				logger.error("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署负载更新失败："+e.getMessage());
				return new Result(true, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署负载更新失败："+e.getMessage());
			}
			//更新负载-----------结束
			//发布成功添加守护
			return new Result(true, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署成功");
		}catch(Exception e){
			e.printStackTrace();
			taskMessage.append("<br/>");
			taskMessage.append("执行异常："+e.getCause());
			TaskMessage loadBalanceTask = new TaskMessage(taskMessage.toString(), 100, user.getUserId(),deployModel.getSystemId());
			taskCache.updateTask(loadBalanceTask);
			//发布成功添加守护
			return new Result(true, "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署异常："+e.getCause());
		}finally{
			taskMessage.append("<br/>");
			taskMessage.append("执行结束。");
			TaskMessage loadBalanceTask = new TaskMessage(taskMessage.toString(), 100, user.getUserId(),deployModel.getSystemId());
			taskCache.updateTask(loadBalanceTask);
		}
	}
	/**
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年5月12日
	*/
	private Result removeLastVersionContainers(List<Container> containertmep) {
		Result result = new Result(true, "");
		if (containertmep != null) {
			String[] containerIds = new String[containertmep.size()];
			for (int i = 0; i < containertmep.size(); i++) {
				containerIds[i] = String.valueOf(containertmep.get(i).getConId());
			}
			if (containerIds.length > 0) {
				JSONObject jo = new JSONObject();
				jo.put("containerId", containerIds);
				
				result = containerManager.stopContainer(jo);
				if(!result.isSuccess()){
					logger.error(result.getMessage());
					return result;
				}
				result = containerManager.removeContainer(jo);
			}
		}
		return result;
	}
	/**
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年5月12日
	*/
	private int saveDeployFile(String deployFileStr,String newDeployId) {
		int result = 1;
		if(null!=deployFileStr&&!"".equals(deployFileStr)){
			String[] deployFiles = deployFileStr.split(" ");
			for(String deployFile:deployFiles){
				String[] deployFileInfo = deployFile.split("===");
				String fileName = deployFileInfo[0];
				String filePath = deployFileInfo[1];
				String id =KeyGenerator.uuid();
				DeployFile insertDeployFile = new DeployFile(id, newDeployId, fileName, filePath);
				result = deployFileService.insert(insertDeployFile);
			}
		}
		return result;
	}
	private List<DeployDatabase> databaseMeterial(JSONArray dataInfoArray){
		// 数据库信息，待保存
		List<DeployDatabase> deployDatabases = new ArrayList<>();
		List<DataSource> dataSources = new ArrayList<>();
		for(int d=0;d<dataInfoArray.size();d++){
			JSONObject dataInfo = dataInfoArray.getJSONObject(d);
			String databaseId = dataInfo.getString("databaseId");
			String ds = dataInfo.getString("dataSource");
			String initSql = dataInfo.getString("initSql");
			String maxCapacity = dataInfo.getString("maxCapacity");
			String minCapacity = dataInfo.getString("minCapacity");
			Component db = new Component();
			db.setComponentId(databaseId);
			Component databaseInfo = componentService.getComponent(db);
            //compenentexpend 查询,并将compenentexpend放入databaseInfo
			ComponentExpand componentExpand=componentExpandService.selectByCompId(databaseId);
			databaseInfo.setDriverId(componentExpand.getDriverId());
			Driver driver = driverService.selectByPrimaryId(databaseInfo.getDriverId());
//			String initSql = databaseInfo.getInitSql();
			DataSource dataSource = new DataSource(databaseInfo.getComponentJdbcurl(),
					databaseInfo.getComponentUserName(), Encrypt.decrypt(databaseInfo.getComponentPwd(), localConfig.getSecurityPath()),
					databaseInfo.getComponentDriver(), ds, driver.getDriverName(), driver.getDriverPath(), initSql,maxCapacity,minCapacity);
			//driver.getDriverName(), driver.getDriverPath()暂不确定是否从databaseInfo—>driver中获取
			dataSources.add(dataSource);
			
			DeployDatabase deployDatabase = new DeployDatabase();
			deployDatabase.setDatabaseId(databaseId);
			deployDatabase.setDataSource(ds);
			//部署完成后生成
			//deployDatabase.setDeployId(null);
			deployDatabase.setDriverId(driver.getId());
			deployDatabase.setInitSql(initSql);
			deployDatabase.setMaxCapacity(maxCapacity);
			deployDatabase.setMinCapacity(minCapacity);
			deployDatabases.add(deployDatabase);
		}
		return deployDatabases;
	}
	/**
	 * 需要转存的文件
	*  开发环境需要转存到临时服务器
	*  平台运行在linux上的需要把文件转存到指定位置
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年5月12日
	 */
	private Result copyFile(DeployModel deployModel,System system,List<DeployDatabase> deployDatabases) {
		//本地应用文件目录
		SystemApp systemApp = systemAppService.getSystemAppById(deployModel.getSystemAppId());
		String appLocalPath = systemApp.getAppRoute().trim();
		String appLocalfile = (systemApp.getAppRoute().trim()+"/"+systemApp.getAppName()).replace("\\", "/").replace("//", "/");
		//新上传配置文件地址
		String localConfigPath = deployModel.getConfigRoute();
		String localHostConfigPath = localConfigPath.replace(tmpHostConfig.getDeployPath(), localConfig.getLocalDeployPath());
		String localHostPath = appLocalPath.replace(tmpHostConfig.getAppPath(), localConfig.getLocalAppPath());
		try {
			//复制应用文件
			appLocalPath = FileUploadUtil.check(appLocalPath);
			localHostConfigPath = FileUploadUtil.check(localHostConfigPath);
			appLocalfile = appLocalfile.replace(tmpHostConfig.getAppPath(), localConfig.getLocalAppPath());
			File isfile = new File(appLocalfile);
			//如果本地有，从本地拷贝。没有从中转机器下载
			if(isfile.exists()&&isfile.isFile()){
				File from = new File(localHostPath);
				File to = new File(localHostConfigPath);
				if(!to.exists()){
					to.mkdirs();
    			}
				FileUtils.copyDirectory(from, to);
			}else{
				if(tmpHostConfig.isTransferNeeded()){
		    		try {
		    			//拷贝到本地
		    			File temFile = new File(localHostPath);
		    			if(!temFile.exists()){
		    				temFile.mkdirs();
		    			}
		    			String tmpIp = tmpHostConfig.getTmpHostIp();
		    			String tmpUser = tmpHostConfig.getTmpHostUser();
		    			String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
		    			SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
		    			if(ssh.connect()){
		    				boolean b = ssh.GetFile((appLocalPath+"/"+systemApp.getAppName()).replace("\\", "/").replace("//", "/")
		    						, localHostPath);
		    				ssh.close();
		        			if(!b){
		        				return new Result(false, "文件远程拷贝到本地失败");
		        			}
		        			File from2 = new File(localHostPath);
		        			File to2 = new File(localHostConfigPath);
		    				FileUtils.copyDirectory(from2, to2);
						}else{
			        		return new Result(false, "中转主机连接失败！");
			        	}
					} catch (Exception e) {
						e.printStackTrace();
						return new Result(false, "文件远程拷贝到本地失败！");
					}
		        }
			}
		} catch (IOException e1) {
			logger.error("转储应用文件失败:"+e1.getMessage());
			return new Result(false, "转储应用文件失败:"+e1);
		}
		try {
			//配置文件，如果有上一版本，把上一版本保留的文件上传到本次部署版本目录
			Deploy deploy = deployService.getDeploy(system.getDeployId());
			String allConfigFiles = deployModel.getDeployFiles();
			if(StringUtils.isNotEmpty(allConfigFiles)){
				String[] allConfigArray = allConfigFiles.split(" ");
				if(null!=deploy&&StringUtils.isNotEmpty(deploy.getConfigRoute())){
					for(int d=0;d<allConfigArray.length;d++){
						String configFile = allConfigArray[d];
						String[] cfg = configFile.split("===");
						if(null==deployModel.getDeployConfiFile()||!deployModel.getDeployConfiFile().contains(configFile)){
							if(StringUtils.isNotEmpty(configFile)&&StringUtils.isNotEmpty(cfg[0])){
								String lastConfigLocalPath = deploy.getConfigRoute().replace(tmpHostConfig.getDeployPath(), localConfig.getLocalDeployPath());
								File isfile = new File((lastConfigLocalPath+'/'+cfg[0]).replace("\\", "/").replace("//", "/"));
								File thisfile = new File((localHostConfigPath+'/'+cfg[0]).replace("\\", "/").replace("//", "/"));
								if(thisfile.exists()&&thisfile.isFile()){
								}else if(isfile.exists()&&isfile.isFile()){//如果本地有，从本地拷贝。没有从中转机器下载
									//拷贝到本地
					    			File temFile = new File(localHostConfigPath);
					    			if(!temFile.exists()){
					    				temFile.mkdirs();
					    			}
									FileUtils.copyFileToDirectory(isfile, temFile);
								}else{
									if(tmpHostConfig.isTransferNeeded()){
							    		try {
							    			//拷贝到本地
							    			File temFile = new File(localHostConfigPath);
							    			if(!temFile.exists()){
							    				temFile.mkdirs();
							    			}
							    			String tmpIp = tmpHostConfig.getTmpHostIp();
							    			String tmpUser = tmpHostConfig.getTmpHostUser();
							    			String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
							    			SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
							    			if(ssh.connect()){
							    				boolean b = ssh.GetFile((deploy.getConfigRoute()+'/'+cfg[0]).replace("\\", "/").replace("//", "/")
							    						, localHostConfigPath);
							    				ssh.close();
							        			if(!b){
							        				return new Result(false, "文件远程拷贝到本地失败");
							        			}
											}else{
								        		return new Result(false, "中转主机连接失败！");
								        	}
										} catch (Exception e) {
											e.printStackTrace();
											return new Result(false, "文件远程拷贝到本地失败！");
										}
							        }
								}
								
							}
						}
					}
				}
			}
			//复制配置文件
			if(StringUtils.isNotEmpty(deployModel.getDeployConfiFile())){
				String[] configArray = deployModel.getDeployConfiFile().split(" ");
				if(null!=deploy&&StringUtils.isNotEmpty(deploy.getConfigRoute())){
					for(int d=0;d<configArray.length;d++){
						String configFile = configArray[d];
						String[] cfg = configFile.split("===");
						if(StringUtils.isNotEmpty(configFile)&&StringUtils.isNotEmpty(cfg[0])){
							File isfile = new File((localHostConfigPath+'/'+cfg[0]).replace("\\", "/").replace("//", "/"));
							//如果本地有，从本地拷贝。没有从中转机器下载
							if(isfile.exists()&&isfile.isFile()){
							}else{
								if(tmpHostConfig.isTransferNeeded()){
						    		try {
						    			//拷贝到本地
						    			File temFile = new File(localHostConfigPath);
						    			if(!temFile.exists()){
						    				temFile.mkdirs();
						    			}
						    			String tmpIp = tmpHostConfig.getTmpHostIp();
						    			String tmpUser = tmpHostConfig.getTmpHostUser();
						    			String tmpPwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(),localConfig.getSecurityPath());
						    			SSH ssh = new SSH(tmpIp, tmpUser, tmpPwd);
						    			if(ssh.connect()){
						    				boolean b = ssh.GetFile((localConfigPath+'/'+cfg[0]).replace("\\", "/").replace("//", "/")
						    						, localHostConfigPath);
						    				ssh.close();
						        			if(!b){
						        				return new Result(false, "文件远程拷贝到本地失败");
						        			}
										}else{
							        		return new Result(false, "中转主机连接失败！");
							        	}
									} catch (Exception e) {
										e.printStackTrace();
										return new Result(false, "文件远程拷贝到本地失败！");
									}
						        }
							}
							
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("转储上一版本保留的文件上传到本次部署版本目录失败:"+e.getMessage());
			return new Result(false, "转储上一版本保留的文件上传到本次部署版本目录失败:"+e);
		}
		//驱动包
		for(DeployDatabase dataSource :deployDatabases){
			Driver driver = driverService.selectByPrimaryId(dataSource.getDriverId());
			try {
				File from = new File(localConfig.getLocalDBDriverPath() + driver.getDriverPath());
				String toPath = localConfigPath.trim()+"/dbDriver/";
				File to = new File(toPath);
//				FileUtils.forceMkdir(arg0);
				FileUtils.copyDirectory(from, to);
			} catch (IOException e) {
				logger.error("转储数据库驱动包失败:"+e.getMessage());
				return new Result(false, "转储数据库驱动包失败:"+e);
			}
		}
		return new Result(true, "");
	}
	/**
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年10月30日
	 */
	private int createDeployDatabase(String deployId, List<DeployDatabase> newDeployDatabases) {
		int result = 0;
		try {
			for (DeployDatabase deployDatabase : newDeployDatabases) {
				deployDatabase.setDeployId(deployId);
				result = deployDatabaseService.create(deployDatabase);
			}
		} catch (Exception e) {
			logger.error("create deploy database fail: " + e.getMessage());
			result = 0;
		}
		return result;
	}
	/**
	 * @author youngtsinglin
	 * @2015年9月29日 基础部署模块，完成镜像的生成创建，标注和发布，并创建多个容器
	 */
	public JSONObject makeAppImage(DeployModel deployModel,User user,System system) {
		taskMessage.append("<br/>");
		taskMessage.append("准备制作镜像材料...");
		TaskMessage makeImagePre = new TaskMessage(taskMessage.toString(), 5, user.getUserId(),deployModel.getSystemId());
		taskCache.updateTask(makeImagePre);
		
		JSONObject deployObject = new JSONObject();
		//registry负载集群Ip
		List<RegImage> regImages = regImageService.selectAll(deployModel.getBaseImageId());
		RegistryLb registryLb = new RegistryLb();
		registryLb.setId(regImages.get(0).getRegistryId());
		registryLb = registryLbService.selectRegistryLb(registryLb);
		String appName = deployModel.getAppName();
		String appTag = deployModel.getDeployVersion();
		String appImageName = registryLb.getDomainName()+":"+registryLb.getLbPort()+"/"+appName.toLowerCase();
		String appImageFullName = appImageName+":"+appTag;
		//应用文件
		SystemApp systemApp = appService.getSystemAppById(deployModel.getSystemAppId());
		ImageMaterial material = new ImageMaterial();
		material.setAppFileName(systemApp.getAppName());
		Image image = imageService.load(deployModel.getBaseImageId());
		material.setBaseImage(image);
		material.setAppName(appName);
		material.setAppTag(appTag);
		material.setTimestamp(deployModel.getTimestamp());
		material.setImageName(appImageFullName);
		List<DataSource> dataSources = new ArrayList<>();
		Result result = null;
		// 数据库连接信息
		String databaseInfosJsonStr = deployModel.getDatabaseInfo();
		JSONArray dataInfoArray = JSONArray.fromObject(databaseInfosJsonStr);
		for(int d=0;d<dataInfoArray.size();d++){
			JSONObject dataInfo = dataInfoArray.getJSONObject(d);
			String databaseId = dataInfo.getString("databaseId");
			String ds = dataInfo.getString("dataSource");
			String initSql = dataInfo.getString("initSql");
			String maxCapacity = dataInfo.getString("maxCapacity");
			String minCapacity = dataInfo.getString("minCapacity");
			Component db = new Component();
			db.setComponentId(databaseId);
			Component databaseInfo = componentService.getComponent(db);
            //compenentexpend 查询,并将compenentexpend放入databaseInfo
			ComponentExpand componentExpand=componentExpandService.selectByCompId(databaseId);
			databaseInfo.setDriverId(componentExpand.getDriverId());
			Driver driver = driverService.selectByPrimaryId(databaseInfo.getDriverId());
			DataSource dataSource = new DataSource(databaseInfo.getComponentJdbcurl(),
					databaseInfo.getComponentUserName(), Encrypt.decrypt(databaseInfo.getComponentPwd(), localConfig.getSecurityPath()),
					databaseInfo.getComponentDriver(), ds, driver.getDriverName(), driver.getDriverPath(), initSql,maxCapacity,minCapacity);
			//driver.getDriverName(), driver.getDriverPath()暂不确定是否从databaseInfo—>driver中获取
			dataSources.add(dataSource);
		}
		material.setDataSources(dataSources);
		//是否session共享
		material.setSharable(false);
		material.setRegistryLb(registryLb);
//		if(tmpHostConfig.isTransferNeeded()){
//			material.setPath(deployModel.getAppRoute());
//		}else{
			material.setPath(deployModel.getConfigRoute());
//		}
		
		material.setSystemId(deployModel.getSystemId());
		material.setUserId(user.getUserId());
		//Dockerfile填充段落
		material.setDockerfile(deployModel.getDockerfile());
		//是否需要中转
		material.setTransferNeeded(tmpHostConfig.isTransferNeeded());
		//设置JVM内存
		List<String> runCommands = new ArrayList<>();
		taskMessage.append("<br/>");
		taskMessage.append("开始制作应用镜像...");
		TaskMessage makeImageing = new TaskMessage(taskMessage.toString(), 10, user.getUserId(),deployModel.getSystemId());
		taskCache.updateTask(makeImageing);
		material.setTaskMessage(taskMessage);
		JvmConfig jvmConfig = new JvmConfig(null==deployModel.getXmxmin()?0:(long)deployModel.getXmxmin(), 
				null==deployModel.getXmxmax()?0:(long)deployModel.getXmxmax());
		// 制作镜像
		try {
			if (image.getImageName().toLowerCase().contains("tomcat")) {
				String configPath = TomcatConstants.BIN_PATH+"catalina.sh";
				String runCommand = jvmConfig.getTomcatJvmConfig()+" "+configPath;
				runCommands.add(runCommand);
				material.setRunCommands(runCommands);
				//配置文件
				List<ConfigFile> configFiles = new ArrayList<>();
				String deployFileStr = deployModel.getDeployFiles();
				if(null!=deployFileStr&&!"".equals(deployFileStr)){
					String[] deployFiles = deployFileStr.split(" ");
					for(String deployFile:deployFiles){
						String[] deployFileInfo = deployFile.split("===");
						String fileName = deployFileInfo[0];
						String filePath = deployFileInfo[1];
						if(!filePath.endsWith("/")){
							filePath = filePath+"/";
						}
						//如果选择tomcat基础镜像，上传路径以/开头的从根目录开始创建路径，如果不是/开头并且以“webapps”开头放在tomcat 安装目录webapp下
						if(!filePath.startsWith("/")){
							if(filePath.startsWith("webapps")){
								filePath = TomcatConstants.BASE_PATH+filePath.substring(8);
							}else{
								filePath = TomcatConstants.APP_PATH+filePath;
							}
						}
						ConfigFile configFile =new ConfigFile(fileName, filePath);
						configFiles.add(configFile);
					}
				}
				material.setConfigFiles(configFiles);
				
				result = tomcatImageBuilder.build(material);
				taskMessage = material.getTaskMessage();
			} else if (image.getImageName().toLowerCase().contains("weblogic")) {
				String configPath = WeblogicContstants.BIN_PATH+"setDomainEnv.sh";
				String runCommand =jvmConfig.getWeblogicJvmConfig()+" "+configPath;
				runCommands.add(runCommand);
				material.setRunCommands(runCommands);
				//配置文件
				List<ConfigFile> configFiles = new ArrayList<>();
				String deployFileStr = deployModel.getDeployFiles();
				if(null!=deployFileStr&&!"".equals(deployFileStr)){
					String[] deployFiles = deployFileStr.split(" ");
					for(String deployFile:deployFiles){
						String[] deployFileInfo = deployFile.split("===");
						String fileName = deployFileInfo[0];
						String filePath = deployFileInfo[1];
						if(!filePath.endsWith("/")){
							filePath = filePath+"/";
						}
						//如果是weblogic基础镜像,上传路径以"/"开头的从根目录开始创建路径，如果不是"/"放在weblogic base_domain下
						if(!filePath.startsWith("/")){
							if(filePath.startsWith("base_domain")){
								filePath = WeblogicContstants.BASE_PATH+filePath.substring(12);
							}else{
								filePath = WeblogicContstants.BASE_PATH+filePath;
							}
						}
						ConfigFile configFile =new ConfigFile(fileName, filePath);
						configFiles.add(configFile);
					}
				}
				material.setConfigFiles(configFiles);
				
				result = weblogicImageBuilder.build(material);
				taskMessage = material.getTaskMessage();
			}else if (image.getImageName().toLowerCase().contains("java")) {
				material.setRunCommands(runCommands);
				//配置文件
				List<ConfigFile> configFiles = new ArrayList<>();
				String deployFileStr = deployModel.getDeployFiles();
				if(null!=deployFileStr&&!"".equals(deployFileStr)){
					String[] deployFiles = deployFileStr.split(" ");
					for(String deployFile:deployFiles){
						String[] deployFileInfo = deployFile.split("===");
						String fileName = deployFileInfo[0];
						String filePath = deployFileInfo[1];
						if(!filePath.endsWith("/")){
							filePath = filePath+"/";
						}
//						//如果是weblogic基础镜像,上传路径以"/"开头的从根目录开始创建路径，如果不是"/"放在weblogic base_domain下
//						if(!filePath.startsWith("/")){
//							if(filePath.startsWith("base_domain")){
//								filePath = WeblogicContstants.BASE_PATH+filePath.substring(12);
//							}else{
//								filePath = WeblogicContstants.BASE_PATH+filePath;
//							}
//						}
						ConfigFile configFile =new ConfigFile(fileName, filePath);
						configFiles.add(configFile);
					}
				}
				material.setConfigFiles(configFiles);
				
				result = javaImageBuilder.build(material);
				taskMessage = material.getTaskMessage();
			} else {
				deployObject.put("issuccess",false);
				logger.error("[" + user.getUserName() + "]:" + "基础镜像类型[" + image.getImageName() + "]不是tomcat或者weblogic，处理失败。");
				deployObject.put("result", "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署,基础镜像类型[" + image.getImageName() + "]不是tomcat或者weblogic，处理失败。");
				return deployObject;
			}
		} catch (Exception e) {
			e.printStackTrace();
			deployObject.put("issuccess",false);
			logger.error("[" + user.getUserName() + "]:" + "制作镜像失败: "+e.getMessage());
			deployObject.put("result", "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署制作镜像失败");
			return deployObject;
		}
		String imageUUID = null;
		if (result.isSuccess()) {
			imageUUID = result.getMessage();
			taskMessage.append("<br/>");
			taskMessage.append("制作镜像已完成...");
			TaskMessage makeImageOk = new TaskMessage(taskMessage.toString(), 50, user.getUserId(),
					deployModel.getSystemId());
			taskCache.updateTask(makeImageOk);
		} else {
			logger.error(result.getMessage());
			deployObject.put("issuccess",false);
			deployObject.put("result", result.getMessage());
			return deployObject;
		}
		/* 在镜像表里添加此新增镜像的纪录 */
		String InsertImgId = null;
		try {
			Image biz_image = new Image();
			biz_image.setImageStatus((byte) Status.IMAGE.NORMAL.ordinal());
			biz_image.setImageName(appImageName);
			biz_image.setImageUuid(imageUUID);
			biz_image.setImageTag(appTag);
			biz_image.setTempName(deployModel.getSystemName()+deployModel.getDeployVersion());
			biz_image.setSystemName(deployModel.getSystemName());
			biz_image.setImageCreator(user.getUserId());
			biz_image.setImageType((byte) Type.IMAGE_TYPE.APP.ordinal());
			biz_image.setEnvId(system.getEnvId());
			InsertImgId = imageService.create(biz_image);
		} catch (Exception e) {
			logger.error("[" + user.getUserName() + "]:" + "镜像数据保存失败: "+e.getMessage());
			deployObject.put("issuccess",false);
			deployObject.put("result", "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署镜像数据保存失败:"+e.getMessage());
			return deployObject;
		}

		deployObject.put("imageName", systemApp.getAppName());
		deployObject.put("imageTag", deployModel.getDeployVersion());
		deployObject.put("InsertImgId", InsertImgId);
		deployObject.put("issuccess",true);
		return deployObject;
	}
	
	public JSONObject startContainersByImage(DeployModel deployModel,User user,String imageId){
		JSONObject jsonObject = new JSONObject();
		Cluster cluster = clusterService.getCluster(deployModel.getClusterId());
		Host clusterHost = hostService.getHost(cluster.getMasteHostId());
		// 创建多个容器
		JSONArray containerArray = new JSONArray();
		
		//应用镜像
		Image image = imageService.selectByPrimaryKey(imageId);
		Image baseImage = imageService.selectByPrimaryKey(deployModel.getBaseImageId());
		String finalImageName = image.getImageName()+":"+image.getImageTag();
		
//		SSH ssh = new SSH(clusterHost.getHostIp(), clusterHost.getHostUser(),
//				Encrypt.decrypt(clusterHost.getHostPwd(), localConfig.getSecurityPath()));
		// 自动切换，启动容器。手动切换不启动
		if (2==deployModel.getAutoDeploy()) {
			try {
				for (int i = 0; i < deployModel.getInstanceCount(); i++) {
					String conId = KeyGenerator.uuid();
					
					ContainerModel model = new ContainerModel();
					model.setCreateModel("1");
					model.setCpu(deployModel.getCpu());
					model.setMemery(deployModel.getMemery());
					//设置容器日志文件目录外挂到宿主机host.app.log.path /conId下
					String logPath = null;
					if (baseImage.getImageName().toLowerCase().contains("tomcat")) {
						logPath = TomcatConstants.LOGS_PATH;
					} else if (baseImage.getImageName().toLowerCase().contains("weblogic")) {
						logPath = WeblogicContstants.LOGS_PATH;
					}
					model.setLogPath(logPath);
					model.setImageName(finalImageName);
					String param = this.containerLocation(deployModel.getLocationLeavel(),i,deployModel.getLocationStragegy(),deployModel.getClusterId());
					model.setCreateParams(param);
					model.setConId(conId);
					model.setParameter(deployModel.getParameter());
					model.setSystemCode(deployModel.getSystemCode());
					JSONObject containerObject = new JSONObject();
					boolean b = monitorManager.clusterMonitor(cluster.getClusterId());
					if(!b){
						containerObject.put("containerArray", containerArray);
						if(i>0){
							containerObject.put("issuccess", true);
						}else{
							containerObject.put("issuccess", false);
						}
						
						containerObject.put("result", "已创建"+i+"个应用容器，集群【"+cluster.getClusterName()+"】资源使用已达上限！");
						return containerObject;
					}
					containerObject = containerCore.createContainer(clusterHost.getHostIp(), clusterHost.getHostUser(),
							Encrypt.decrypt(clusterHost.getHostPwd(), localConfig.getSecurityPath()), cluster.getClusterPort(), model);
					if(!containerObject.getBoolean("issuccess")){
						return containerObject;
					}
					containerObject.element("conId", conId);
					containerObject.element("clusterHostIp", clusterHost.getHostIp());
					containerObject.element("clusterPort", cluster.getClusterPort());
					containerObject.element("conEnv", cluster.getClusterEnv());
					containerObject.element("imageId", imageId);
					containerObject.element("systemName", deployModel.getSystemName());
					containerObject.element("deployVersion", deployModel.getDeployVersion());
					containerArray.add(containerObject);
	
				}
			} catch (Exception e) {
				logger.info("系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署容器启动失败:"+e.getMessage());
				jsonObject.put("issuccess", false);
				jsonObject.put("result", "系统【"+deployModel.getSystemName()+deployModel.getDeployVersion()+"】部署容器启动失败:"+e);
				return jsonObject;
			}
		}
		jsonObject.put("containerArray", containerArray);
		jsonObject.put("issuccess", true);
		return jsonObject;
	}
	/**
	 * @author langzi
	 * @param jo
	 * @return
	 * @version 1.0 2015年8月27日
	 */
	private List<Container> addContainer(JSONArray containerArray, String systemId, User user) {
		List<Container> containers = new ArrayList<Container>();
		if (containerArray.size() > 0) {
			// String conIds = "";
			for (int i = 0; i < containerArray.size(); i++) {
				JSONObject containerJo = containerArray.getJSONObject(i);
				Container container = new Container();
				container.setConId(containerJo.getString("conId"));
				container.setConUuid(containerJo.getString("id"));
				container.setConName(containerJo.getString("names"));
				String systemName = containerJo.getString("systemName");
				String deployVersion = containerJo.getString("deployVersion");
				String conTempName = systemName + deployVersion + "-" + (i + 1);
				container.setConTempName(conTempName);
				if (!containerJo.containsKey("imageId")) {
					continue;
				}
				container.setConImgid(containerJo.getString("imageId"));
				if (containerJo.getString("status").contains("Up")) {
					container.setConPower((byte) Status.POWER.UP.ordinal());
				} else {
					container.setConPower((byte) Status.POWER.OFF.ordinal());
				}
				container.setConDesc("test docker");
				container.setConStatus((byte) Status.CONTAINER.UP.ordinal());
				container.setConStartCommand(containerJo.getString("command"));
				container.setClusterIp(containerJo.getString("clusterHostIp"));
				container.setClusterPort(containerJo.getString("clusterPort"));
				container.setConCreator(user.getUserId());
				TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
				container.setConCreatetime(new Date());
				container.setConType(Type.CON_TYPE.APP.ordinal());
				container.setConEnv(containerJo.getString("conEnv"));
				container.setSysId(systemId);
				try {
					String hostIp = "";
					JSONArray ports = (JSONArray) containerJo.get("ports");
					if (!ports.isEmpty()) {
						hostIp = ((JSONObject) ports.get(0)).getString("ip");
						container.setHostId(hostService.getHostByIp(hostIp).getHostId());
						containerService.addContaier(container);
						containers.add(container);
						for (int j = 0; j < ports.size(); j++) {
							JSONObject port = (JSONObject) ports.get(j);
							ConPort conPort = new ConPort();
							conPort.setContainerId(container.getConId());
							conPort.setConIp(port.getString("ip"));
							conPort.setPubPort(String.valueOf(port.getInt("publicPort")));
							conPort.setPriPort(String.valueOf(port.getInt("privatePort")));
							conportService.addConports(conPort);
						}
					} else {
						logger.info("Container's port is empty!");
						containerService.addContaier(container);
						containers.add(container);
					}
				} catch (Exception e) {
					logger.info("Create container error: "+e);
				}

			}
			return containers;
		} else {
			return null;
		}
	}

	/**
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年10月20日
	 * 保存发布版本数据
	 */
	private String saveDeployInfo(DeployModel deployModel, String appImageId)
			throws Exception {
		Deploy deploy = new Deploy();
		BeanUtils.copyProperties(deployModel, deploy);
		deploy.setVersionChange(deployModel.getAutoDeploy());
		deploy.setAppImageId(appImageId);
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		deploy.setCreateTime(new Date());
		if(deployModel.isDnsEnable()){
			deploy.setDnsEnable((byte)1);
		}else{
			deploy.setDnsEnable((byte)0);
		}
		if(deployModel.isSyncDnsEnable()){
			deploy.setSyncDnsEnable((byte)1);
		}else{
			deploy.setSyncDnsEnable((byte)0);
		}
		deploy.setDeployId(null);
		deploy.setXmx(deployModel.getXmxmax());
		deploy.setXms(deployModel.getXmxmin());
		deploy.setInitCount(deployModel.getInstanceCount());
		String deployId = deployService.createDeploy(deploy);
		return deployId;
	}
	/**
	 * @author mayh
	 * @return void
	 * @version 1.0 2015年10月20日
	 * 更新镜像数据
	 */
	private int updateImage(String appImageId, String deployId, DeployModel deployModel) {
		Image newImage = new Image();
		newImage.setImageId(appImageId);
		newImage.setDeployId(deployId);
		newImage.setImageStatus(Status.IMAGE.NORMAL.ordinal());
		newImage.setTempName(deployModel.getSystemName()+deployModel.getDeployVersion());
		boolean b = imageService.update(newImage);
		if(b&&deployModel.getAppCategory().equals("1")){//非自定义镜像
			RegImage regImage = regImageMapper.selectByImageId(deployModel.getBaseImageId());
			RegImage reg_img = new RegImage(KeyGenerator.uuid(), regImage.getRegistryId(), appImageId);
			int re = regImageMapper.insert(reg_img);
			return re;
		}else if(b&&deployModel.getAppCategory().equals("2")){
			return 1;
		}
		else{
			return 0;
		}
		
	}

	/**
	 * 版本回滚
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年10月15日
	 */
	public Result rollBackImage(String clusterId,String deployId, User user) {
		
		// 1、获取镜像及回滚到的版本信息
		Deploy deploy = deployService.getDeploy(deployId);
		if (deploy == null) {
			logger.error("回滚操作失败，版本信息不存在");
			return new Result(false, "回滚操作失败，版本信息不存在");
		}
		Image image = new Image();
		image = imageService.load(deploy.getAppImageId());
		if (image == null) {
			logger.error("回滚操作失败，系统应用依赖镜像不存在，可能已被删除");
			return new Result(false, "回滚操作失败，系统应用依赖镜像不存在，可能已被删除");
		}
		// 集群
		Cluster cluster = new Cluster();
		cluster.setClusterId(clusterId);
		cluster = clusterService.getCluster(cluster);
		if (cluster == null) {
			logger.error("原集群不存在，可能已被删除");
			return new Result(false, "原集群不存在，可能已被删除");
		}
		Host clusterhost = new Host();
		clusterhost.setHostId(cluster.getMasteHostId());
		clusterhost = hostService.getHost(clusterhost);
		if (clusterhost == null) {
			logger.error("集群【"+cluster.getClusterName()+"】所在主机不存在，可能已被删除");
			return new Result(false, "集群【"+cluster.getClusterName()+"】所在主机不存在");
		}
		
		SSH ssh = new SSH(clusterhost.getHostIp(), clusterhost.getHostUser(),
				Encrypt.decrypt(clusterhost.getHostPwd(), localConfig.getSecurityPath()));
		JSONArray containerArray = new JSONArray();
		// 查出当前版本的容器，待新增版本容器成功后，上一版本的容器删除
		System systemtemp = systemService.loadById(deploy.getSystemId());
		//停止当前运行的物理系统弹性
		if(null!= systemtemp&&Status.ElSASTICITYSTATUS.USED.ordinal()==systemtemp.getSystemElsasticityStatus().intValue()){
			systemtemp.setSystemElsasticityStatus((byte)Status.ElSASTICITYSTATUS.UNUSED.ordinal());
			systemService.updateSystem(systemtemp);
		}
		if(systemtemp==null){
			return new Result(false, "物理系统信息处理异常"); 
		}
		Deploy oldDeploy = null;
		if(StringUtils.isNotEmpty(systemtemp.getDeployId())){
			oldDeploy = deployService.getDeploy(systemtemp.getDeployId());
		}
		List<Container> containertmep = containerService
				.selectContainersByImageId(oldDeploy == null ? null : oldDeploy.getAppImageId());
//		Image baseImage = new Image();
//		baseImage = imageService.load(deploy.getBaseImageId());
		//关闭守护
		platformDaemon.stopSystemDaemon(deploy.getSystemId());
		// 2、启动容器
		try {
			for (int i = 0; i < deploy.getInitCount(); i++) {
				String conId = KeyGenerator.uuid();
				
				ContainerModel model = new ContainerModel();
				model.setCreateModel("1");
				model.setCpu(deploy.getCpu());
				model.setMemery(deploy.getMemery());
				//设置容器日志文件目录外挂到宿主机/container/logs/conId下
				String logPath = null;
				if (image.getImageName().toLowerCase().contains("tomcat")) {
					logPath = TomcatConstants.LOGS_PATH;
				} else if (image.getImageName().toLowerCase().contains("weblogic")) {
					logPath = WeblogicContstants.LOGS_PATH;
				}
				model.setLogPath(logPath);
				String finalImageName = image.getImageName().toLowerCase() + ":" + image.getImageTag();
				model.setImageName(finalImageName);
				String param = this.containerLocation(deploy.getLocationLeavel(),i,deploy.getLocationStragegy(),deploy.getClusterId());
				model.setCreateParams(param);
				model.setConId(conId);
				model.setSystemCode(systemtemp.getSystemCode());
				//检查集群主机是否有剩余资源
				boolean b = monitorManager.clusterMonitor(cluster.getClusterId());
				if(!b){
					if(!b){
						if(i==0){
							return new Result(false, "创建容器失败，集群【"+cluster.getClusterName()+"】资源使用已达上限！");
						}
						logger.warn("集群资源使用已达上限！");
					}
				}
				model.setParameter(deploy.getParameter());
				JSONObject containerObject = containerCore.createContainer(clusterhost.getHostIp(), clusterhost.getHostUser(),
						Encrypt.decrypt(clusterhost.getHostPwd(), localConfig.getSecurityPath()), cluster.getClusterPort(), model);
				if(!containerObject.getBoolean("issuccess")){
					logger.error(containerObject.getString("result"));
					return new Result(containerObject.getBoolean("issuccess"), containerObject.getString("result"));
				}

				containerObject.element("conId", conId);
				containerObject.element("clusterHostIp", clusterhost.getHostIp());
				containerObject.element("clusterPort", cluster.getClusterPort());
				containerObject.element("conEnv", cluster.getClusterEnv());
				containerObject.element("imageId", image.getImageId());
				containerObject.element("systemName", image.getSystemName());
				containerObject.element("deployVersion", deploy.getDeployVersion());
				containerArray.add(containerObject);
			}
			// 将容器信息保存到库表
			this.addContainer(containerArray, deploy.getSystemId(), user);
			// 停止当前运行的容器，并更改物理系统当前运行的版本信息
			// 物理系统更新运行的版本
			System system = systemService.loadById(deploy.getSystemId());
			system.setSystemDeployStatus((byte) Status.SYSTEM.DEPLOY.ordinal());
			system.setDeployId(deployId);
			systemService.updateSystem(system);
			//如果更换了集群，需要更新版本信息的集群信息
			deploy.setClusterId(clusterId);
			deployService.updateDeploy(deploy);
			//更新此版本的clusterID
			if(null!=oldDeploy){
				oldDeploy.setClusterId(clusterId);
				deployService.updateDeploy(oldDeploy);
			}
			// 删除上一版本的容器
			if (containertmep != null && containertmep.size() > 0) {
				String[] containerIds = new String[containertmep.size()];
				for (int i = 0; i < containertmep.size(); i++) {
					containerIds[i] = String.valueOf(containertmep.get(i).getConId());
				}
				JSONObject jo = new JSONObject();
				jo.put("containerId", containerIds);
				
				containerManager.stopContainer(jo);
				containerManager.removeContainer(jo);
				//如果上一版本的负载与新部署使用的负载不是同一个，需要把原负载配置更新
				if(null!=oldDeploy&&!oldDeploy.getLoadBalanceId().equals(deploy.getLoadBalanceId())){
					Component oldComponent = new Component();
					oldComponent.setComponentId(oldDeploy.getLoadBalanceId());
					oldComponent = componentService.getComponent(oldComponent);
					
					//单个nginx组件删除容器，重新配置
					if(oldComponent.getComponentType()==1){
						Result result = compPortManager.modifyNginxConfig(oldComponent.getComponentId(),null,oldDeploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
						if(!result.isSuccess()){
							logger.error(result.getMessage());
							return result;
						}
					}
					//nginx组组件删除容器，重新配置
					if(oldComponent.getComponentType()==6){
						Result result = compPortManager.modifyNginxGroup(oldComponent.getComponentId(),null,oldDeploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
						if(!result.isSuccess()){
							logger.error(result.getMessage());
							return result;
						}
					}
					//f5组件删除容器member
					if(oldComponent.getComponentType()==2){
						
					}
				}
			}
			// 更新负载关联关系
			Component component = new Component();
			component.setComponentId(deploy.getLoadBalanceId());
			component = componentService.getComponent(component);
			if(component==null){
				logger.error("系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
				return new Result(false, "系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
			}
			//单个nginx组件
			if(component.getComponentType()==1){
				Result result = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
				if(!result.isSuccess()){
					logger.error(result.getMessage());
					return result;
				}
			}
			//nginx组组件
			if(component.getComponentType()==6){
				Result result = compPortManager.modifyNginxGroup(component.getComponentId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
				if(!result.isSuccess()){
					logger.error(result.getMessage());
					return result;
				}
			}
			//f5组件
			if(component.getComponentType()==2){
				
			}
		} catch (Exception e) {
			logger.error("系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本失败:"+e.getMessage());
			return new Result(true, "系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本失败:"+e);
		} finally {
			if (ssh.connect()) {
				ssh.close();
			}
		}
		// 打开守护
		platformDaemon.startSystemDaemon(deploy.getSystemId());
		if(null==oldDeploy){
			return new Result(true, "系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】成功！");
		}else{
			return new Result(true, "系统【"+systemtemp.getSystemName()+"】从【"+oldDeploy.getDeployVersion()+"】回滚到【"+deploy.getDeployVersion()+"】成功！");
		}
		
	}
	/**
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年10月20日
	 */
	public Result deleteVersion(String deployId, User user) {
		TaskMessage message0 = new TaskMessage("版本删除中...", 0, user.getUserId(),deployId);
		taskCache.updateTask(message0);
		// 获取镜像及回滚到的版本信息
		Deploy deploy = deployService.getDeploy(deployId);
		// 1、删除应用镜像
		Image image = imageService.load(deploy.getAppImageId());
		String imageUuid = image.getImageUuid();
		imageService.delete(deploy.getAppImageId());
		// 3、删除nginx关联关系
		portService.deleteCompPortByUsedId(deployId);
		// 4、删除database关联关系
		deployDatabaseService.deleteByDeployID(deployId);
		// 5、删除配置文件信息
		deployFileService.deleteByDeployId(deployId);
		// 6、版本信息删除
		deployService.deleteDeploy(deployId);
		//7、清楚服务器上的镜像
		Cluster cluster = clusterService.getCluster(deploy.getClusterId());
		Host clusterHost = hostService.getHost(cluster.getMasteHostId());
		SSH ssh = new SSH(clusterHost.getHostIp(), clusterHost.getHostUser(), Encrypt.decrypt(clusterHost.getHostPwd(), localConfig.getSecurityPath()));
		if(ssh.connect()){
			String commandLine = "sudo docker -H tcp://"+clusterHost.getHostIp()+":"+cluster.getClusterPort()+" rmi -f "+imageUuid;
			try {
				String result = ssh.executeWithResult(commandLine, 3000);
				if(result.toLowerCase().contains("error")){
					TaskMessage message1 = new TaskMessage("系统【"+deploy.getDeployVersion()+"】版本删除成功，服务器镜像清除失败："+result,100, user.getUserId(),deployId);
					taskCache.updateTask(message1);
					return new Result(true, "系统【"+deploy.getDeployVersion()+"】版本删除成功，服务器镜像清除失败："+result);
				}
			} catch (Exception e) {
				TaskMessage message1 = new TaskMessage("系统【"+deploy.getDeployVersion()+"】版本删除成功，服务器镜像清除失败："+e,100, user.getUserId(),deployId);
				taskCache.updateTask(message1);
				return new Result(true, "系统【"+deploy.getDeployVersion()+"】版本删除成功，服务器镜像清除失败："+e);
			} finally {
				ssh.close();
			}
		}
		TaskMessage message1 = new TaskMessage("系统【"+deploy.getDeployVersion()+"】版本删除成功！",100, user.getUserId(),deployId);
		taskCache.updateTask(message1);
		return new Result(true, "系统【"+deploy.getDeployVersion()+"】版本删除成功！");
	}
	// 检查物理系统名称是否重复
	public Boolean checkName(String id, String name) {
		System system = new System();
		system.setSystemName(name);
		system = systemService.selectSystem(system);
		if(null == system){
			return true;
		}
		if (StringUtils.isEmpty(id)) {
			// 创建时 找到systemName为name的system对象
			return false;
		} else if (system.getSystemId().equals(id)) {
			// 更新时，找到自身
			return true;
		} else {
			// 更新时，name重复
			return false;
		}
	}
	// 业务系统下的物理系统（运行版本、最新版本）
	public JSONArray getListByBusinessId(User user, String businessId,String envId) {
		List<SystemModel> models = systemService.getListByBusinessId(businessId, user,envId);

		return JSONUtil.parseObjectToJsonArray(models);

	}

	/**
	 * 版本下线
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年12月6日
	 */
	public Result killVersion(String deployId, User user) {
		// 1、获取镜像及回滚到的版本信息
		Deploy deploy = deployService.getDeploy(deployId);
		if (deploy == null) {
			return new Result(false, "版本下线失败，未查到相关版本信息");
		}
		Image image = new Image();
		image = imageService.load(deploy.getAppImageId());
		if (image == null) {
			return new Result(false, "版本下线【"+deploy.getDeployVersion()+"】失败，依赖镜像不存在");
		}
//		RegHost regHost = imageService.loadByImageId(deploy.getAppImageId());
//		if (regHost == null) {
//			return new Result(false, "版本下线【"+deploy.getDeployVersion()+"】失败，仓库主机不存在");
//		}
		// 集群
		Cluster cluster = new Cluster();
		cluster.setClusterId(deploy.getClusterId());
		cluster = clusterService.getCluster(cluster);
		if (cluster == null) {
			return new Result(false, "版本下线【"+deploy.getDeployVersion()+"】失败，依赖集群不存在");
		}
		Host clusterhost = new Host();
		clusterhost.setHostId(cluster.getMasteHostId());
		clusterhost = hostService.getHost(clusterhost);
		if (clusterhost == null) {
			return new Result(false, "版本下线【"+deploy.getDeployVersion()+"】失败,集群【"+cluster.getClusterName()+"】管理主机不存在");
		}
		
		//关闭守护
		platformDaemon.stopSystemDaemon(deploy.getSystemId());
		
//		SSH ssh = new SSH(regHost.getIp(), regHost.getUsername(),
//				Encrypt.decrypt(regHost.getPassword(), localConfig.getSecurityPath()));
		// 查出当前版本的容器
		System systemtemp = systemService.loadById(deploy.getSystemId());
		Deploy deploytemp = deployService.getDeploy(systemtemp.getDeployId());
		List<Container> containertmep = containerService.selectContainersByImageId(deploytemp.getAppImageId());

		try {
			Result result = null;
			// 物理系统更新运行的版本
			System system = systemService.loadById(deploy.getSystemId());
			system.setSystemDeployStatus((byte) Status.SYSTEM.UNDEPLOY.ordinal());
			system.setSystemElsasticityStatus((byte)Status.ElSASTICITYSTATUS.UNUSED.ordinal());
			system.setDeployId("");
			systemService.updateSystem(system);

			// 删除当前版本的容器
			String[] containerIds = new String[containertmep.size()];
			for (int i = 0; i < containertmep.size(); i++) {
				containerIds[i] = String.valueOf(containertmep.get(i).getConId());
			}
			JSONObject jo = new JSONObject();
			jo.put("containerId", containerIds);
			
			result =containerManager.stopContainer(jo);
			result =containerManager.removeContainer(jo);
			// 更新负载
			Component component = new Component();
			component.setComponentId(deploy.getLoadBalanceId());
			component = componentService.getComponent(component);
			if(component==null){
				logger.error("系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
				return new Result(false, "系统【"+systemtemp.getSystemName()+"】回滚到【"+deploy.getDeployVersion()+"】版本,负载信息获取失败未更新");
			}
			//单个nginx组件
			if(component.getComponentType()==1){
				result = compPortManager.modifyNginxConfig(deploy.getLoadBalanceId(),deploy.getNginxPort(),deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
				if(!result.isSuccess()){
					return result;
				}
			}
			//nginx组组件
			if(component.getComponentType()==6){
				result = compPortManager.modifyNginxGroup(component.getComponentId(),null,deploy.getDeployId(),Type.COMPPORT_TYPE.DEPLOY.toString());
				if(!result.isSuccess()){
					return result;
				}
			}
			//f5组件
			if(component.getComponentType()==2){
				
			}
		} catch (Exception e) {
			logger.info("start container error: "+e);
			return new Result(false, "版本下线【"+deploy.getDeployVersion()+"】失败："+e);
		} 
		return new Result(true, "版本下线【"+deploy.getDeployVersion()+"】成功！");
	}
	
	// 版本名称重复检查
	public boolean checkVersion(String id, String deployVersion) {
		if(id == null || id.isEmpty()){
			return false;
		}
		Deploy deploy = new Deploy();
		deploy.setSystemId(id);
		deploy.setDeployVersion(deployVersion);
		List<Deploy> deploys = deployService.select(deploy);
		if (deploys == null||deploys.size()==0) {
			return true;
		}
		else {
			return false;
		}
	}

	/**
	* @author mayh
	* @return boolean
	* @version 1.0
	* 2015年12月29日
	*/
	public boolean checkDomainName(String deployId, String domainName) {
		if(deployId == null || deployId.isEmpty()){
			return true;
		}
		Deploy deploy = new Deploy();
		deploy.setDomainName(domainName);
		List<Deploy> deploys = deployService.select(deploy);
		if (deploys == null||deploys.size()==0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * @author yueyong
	 * @time 2016年2月24日下午3:46:55
	 * @description
	 */
	public JSONObject getNodeVisitUrl(String nodeId, String nodeType,String systemId, String deployId) {
		JSONObject resultobj = new JSONObject();
		
		Deploy deploy = deployService.getDeploy(deployId);
		if (null == deploy) {
			resultobj.put("success", false);
			return resultobj;
		}
		
		String url = deploy.getHttp()+"://"+deploy.getDomainName()
				+(StringUtils.isNotEmpty(deploy.getNginxPort())?(":"+deploy.getNginxPort()):"")
				+"/"+deploy.getAppName();
		if(nodeType.equals("docker")){
			try {
				JSONArray conportArray = JSONUtil.parseObjectToJsonArray(conportService.listConPorts(nodeId));
				if(conportArray!=null && conportArray.size() > 0){
					JSONObject conPort = (JSONObject)conportArray.get(0);
					url = deploy.getHttp()+"://"+conPort.getString("conIp")+":"+conPort.getString("pubPort")+"/"+deploy.getAppName();
					resultobj.put("success", true);
					resultobj.put("url", url);
				}else{
					resultobj.put("success", false);
					return resultobj;
				}
				
			} catch (Exception e) {
				resultobj.put("success", false);
				return resultobj;
			}
		}else{
			resultobj.put("success", true);
			resultobj.put("url", url);
		}
		
		return resultobj;
	}

	private Result checkContentPath(String systemId,String appName,String loadbalanceId) {
		CompPort compPort = new CompPort();
		compPort.setCompId(loadbalanceId);;
		List<CompPort> compPorts = portService.selectAllCompPort(compPort);
		for(CompPort compPort2:compPorts){
			Deploy deploy = deployService.getDeploy(compPort2.getUsedId());
			//挑过本物理系统
			if(null==deploy||deploy.getSystemId().equals(systemId)){
				continue;
			}
			if(deploy.getAppName().equals(appName)){
				System system = systemService.getSystemById(deploy.getSystemId());
				return  new Result(false, "ContentPath【"+appName+"】与物理系统【"+system.getSystemName()+"-"+deploy.getDeployVersion()+"】冲突");
			}
		}
		return new Result(true, "");
	}

	
	
	/**
	 * @author wangjx
	 * 查询所有管理员并判断所属当前物理系统的管理员
	 * @param systemId
	 * @return
	 */
	public JSONArray selectCheckAdmin(String systemId){
		List<User> allSysAdmins =  systemService.selectAllAdminService();
		List<User> userSysAdmins = systemService.selectAdminsBySystemId(systemId);
		int has;
		JSONArray params = new JSONArray();
		for (User allUser : allSysAdmins) {
			has = 0; //0代表该用户不是当前的物理系统用户
			JSONObject param = new JSONObject();
			param.put("userId", allUser.getUserId());
			param.put("userName", allUser.getUserName());
			for (User user : userSysAdmins) {
				if(user.getUserId().equals(allUser.getUserId())){
					has = 1;
				}
			}
			param.put("has", has);
			params.element(param);
		}
		return params;
	}
	
	/**
	 * 保存当前系统管理员信息
	 * @author wangjx
	 * @param dataAdminInfo
	 * @param systemName
	 * @return
	 */
	public Result saveSysAdminManage(String dataAdminInfo,String systemName){
		int addResult = 1;  //代表添加成功
		int delResult = 1;
		JSONArray dataInfoArray = JSONArray.fromObject(dataAdminInfo);
		List<Map<String,Object>> addSysAdmin = new ArrayList<>();
		List<Map<String,Object>> delSysAdmin = new ArrayList<>();
		
		for(int d=0;d<dataInfoArray.size();d++){
			Map<String,Object> map = new HashMap<>();
			JSONObject dataInfo = dataInfoArray.getJSONObject(d);
			String userId = dataInfo.getString("userId");
			String systemId = dataInfo.getString("systemId");
			String beiFly = dataInfo.getString("beiFly");

			if(beiFly.equals("shouldAdd")){
				String systemUserKey = KeyGenerator.uuid();
				map.put("id", systemUserKey);
				map.put("userId", userId);
				map.put("systemId", systemId);
				addSysAdmin.add(map);
			}
			if(beiFly.equals("shouldDel")){
				map.put("userId", userId);
				map.put("systemId", systemId);
				delSysAdmin.add(map);
			}
			
		}

		if(addSysAdmin.size() > 0){
			addResult = systemService.addSysAdminService(addSysAdmin);
		}
		
		if(delSysAdmin.size() > 0){
			delResult = systemService.delSysAdminsService(delSysAdmin);
		}
		
		if(addResult == 1 && delResult == 1){
			logger.info("Save systemAdmin success");
			return new Result(true, "保存【"+systemName+"】物理系统管理员成功");
		}else{
			logger.info("Save systemAdmin error");
			return new Result(false, "保存【"+systemName+"】物理系统管理员失败");
		}

	}
	/**
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年4月22日
	*/
	public int createSystemAppPackage(SystemAppModel systemAppModel) {
		//保存应用包
		SystemApp systemApp = new SystemApp();
		BeanUtils.copyProperties(systemAppModel, systemApp);
		if("1".equals(systemApp.getAppCategory())){
			systemApp.setAppRoute(tmpHostConfig.getAppPath()+systemAppModel.getAppRoute());
		}
		int re = systemService.createSystemAppPackage(systemApp);
		//保存应用预加载包关联
		String prelibStr = systemAppModel.getPrelibInfo();
		if(org.apache.commons.lang.StringUtils.isNotEmpty(prelibStr)){
			String[] prelibs = prelibStr.split(",");
			String appId = systemApp.getId();
			for(int i=0;i<prelibs.length;i+=2){
				String libraryId = prelibs[i];
				String path = prelibs[i+1];
				AppPrelib appPrelib = new AppPrelib();
				
				appPrelib.setAppId(appId);
				appPrelib.setLibraryId(libraryId);
				appPrelib.setPath(path);
				systemService.createAppPrelib(appPrelib);
			}
		}
		return re;
	}
	/**
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年5月20日
	*/
	public Deploy selectDeployTimer(String systemId, User user) {
		Deploy deploy = new Deploy();
		deploy.setSystemId(systemId);
		deploy.setRegularlyPublish((byte)Status.YESNO.YES.ordinal());
		List<Deploy> deploys = deployService.select(deploy);
		if(deploys.size()>0){
			return deploys.get(0);
		}
		return null;
	}
	/**
	* @author mayh
	* @return JSONObject
	* @version 1.0
	* 2016年5月20日
	*/
	public Result setDeployTimer(String systemId, User user) {
		Deploy deploy = new Deploy();
		deploy.setSystemId(systemId);
		List<Deploy> deploys = deployService.select(deploy);
		int re = 1;
		for(Deploy deploy2:deploys){
			deploy2.setRegularlyPublish((byte)Status.YESNO.NO.ordinal());
			re = deployService.updateDeploy(deploy2);
		}
		if(re==1){
			return new Result(true, "");
		}else{
			return new Result(false, "定时任务设置失效失败!");
		}
	}
	//分布级别位置选择，locationStrategy两种方式：1、根据资源选择最优，2、随机选择符合条件的（采用这种）
	//model模式，1资源最优，2随机
	public String containerLocation(String locationParamCode,int order,String model,String clusterId){
		if(StringUtils.isEmpty(locationParamCode)){
			return null;
		}
		String labels = null;
		List<Location> list = locationService.selectByParamCode(locationParamCode);
		if(null==list){
			return null;
		}
		//移除没有主机的位置层级
		Iterator<Location> it = list.iterator();  
        while(it.hasNext()){  
        	Location location = it.next();  
        	List<Host> hosts = this.locationRecursion(location,clusterId);
        	if(hosts==null||hosts.size()==0){
                //移除当前的对象  
                it.remove();  
            }  
        }
        if(list==null||list.size()==0){
        	return null;
        }
		if(null!=model&&"1".equals(model)){
			if(list.size()==1){
				order = 0;
			}else if(list.size()-1<order&&list.size()>1){
				order = order%(list.size()-1);
			}
			//分别计算资源,资源利用从低到高排序
			List<String> labels_list = new ArrayList<>();
			double highest_resource = 0;
			for(Location location:list){
				List<Host> hosts = this.locationRecursion(location,clusterId);
				//计算主机资源
				labels = " constraint:"+location.getLocationCode().trim().toLowerCase()+"=="+locationParamCode.toLowerCase()+" ";
				double AllSource = 0;
				for(Host host2:hosts){
					double hostResource = 0;
					//model为几个小时之前到现在的数据，此处一个小时
					Monitor monitor = new Monitor();
					monitor.setTargetId(host2.getHostId());
					monitor.setMonitorTime(new Timestamp(java.lang.System.currentTimeMillis() - 24*60 * 60 * 1000));
					Monitor monitors = monitorService.getAvgData(monitor);
					double cpu = monitors.getCpu()==null?0:Double.parseDouble(monitors.getCpu());
					double mem = monitors.getMem()==null?0:Double.parseDouble(monitors.getMem());
					double netin = monitors.getNetin()==null?0:Double.parseDouble(monitors.getNetin());
					double netout = monitors.getNetout()==null?0:Double.parseDouble(monitors.getNetout());
					double disk = monitors.getDiskSpace()==null?0:Double.parseDouble(monitors.getDiskSpace());
					hostResource = cpu*Weights.CPU_WEIGHT+mem*Weights.MEM_WEIGHT
							+netin*Weights.NETIN_WEIGHT+netout*Weights.NETOUT_WEIGHT+disk*Weights.DISK_WEIGHT;
					AllSource +=hostResource; 
					
				}
				if(highest_resource<=AllSource){
					highest_resource = AllSource;
					labels_list.add(labels);
				}else{
					labels_list.add(labels_list.get(labels_list.size()));
					labels_list.set(labels_list.size(), labels);
				}
			}
			return labels_list.get(order);
		}else{
			if(null!=list&&list.size()>0){
				SecureRandom secureRandom = new SecureRandom();
				int order_ = secureRandom.nextInt(list.size());
				if(StringUtils.isEmpty(list.get(order_).getLocationCode())){
					return null;
				}
				labels = " constraint:"+list.get(order_).getLocationCode().trim().toLowerCase()+"=="+locationParamCode.toLowerCase()+" ";
				return labels;
			}else{
				return null;
			}
		}
	}
	//递归查询所选位置层级下的主机
	private List<Host> locationRecursion(Location location,String clusterId){
		Host host = new Host();
		host.setClusterId(clusterId);
		host.setLocationId(location.getId());
		List<Host> hosts = hostService.hostListByRack(host);
		Iterator<Host> it = hosts.iterator(); 
		while(it.hasNext()){  
			Host host2 = it.next();  
        	if(!clusterId.equals(host2.getClusterId())||host2.getHostType()!=1){
                //移除当前的对象  
                it.remove();  
            }  
        }
		List<Location> list = locationService.getLocationByParent(location.getId());
		for(Location location2:list){
			List<Host> hosts2 = locationRecursion(location2,clusterId);
			hosts.addAll(hosts2);
		}
		return hosts;
	}
	//提交前校验表单
	public Result validateForm(DeployModel deployModel){
		if(deployModel.getLoadBalanceType()!=2){
			return null;
		}
		//一、校验nginx配置：1、nginx端口是否可用2、nginx域名端口是否重复
		Result result = new Result(true, "");
		String message = "";
		//端口是否被占用
		String nginxGroupId = deployModel.getLoadBalanceId();
		if(StringUtils.isEmpty(nginxGroupId)){
			return new Result(false, "请选择负载！");
		}
		List<ComponentHost> nginxHosts = hostService.selectComponentHost(nginxGroupId);
		for(ComponentHost nginxHost:nginxHosts){
			//统一nginx中的appName不能重名
			result = checkContentPath(deployModel.getSystemId(), deployModel.getAppName(),nginxGroupId);
			if(result != null && !result.isSuccess()){
				return result;
			}
			String nginxIpString = nginxHost.getHostIp();
			String nginxUserString = nginxHost.getHostUser();
			String nginxPwdString = Encrypt.decrypt(nginxHost.getHostPwd(), localConfig.getSecurityPath());

			SSH nginxSSH = new SSH(nginxIpString, nginxUserString, nginxPwdString);
			Port nginxPort = portService.selectByPrimaryKey(deployModel.getNginxPortId());
			if(nginxSSH.connect()){
				try {
					//排除掉nginx,端口是否被其他进程占用
					String re = nginxSSH.executeWithResult("sudo lsof -i:"+nginxPort.getPort()+"|grep -a -L nginx", 3000);
					if(org.apache.commons.lang.StringUtils.isNotEmpty(re)&&!"(standard input)\n".equals(re)&&!"(标准输入)\n".equals(re)){
						message = "Nginx主机【"+nginxHost.getHostIp()+"】端口已被占用："+re;
						result = new Result(false, message);
						break;
					}
					if(re.contains("command not found")){
						message = "Nginx主机【"+nginxHost.getHostIp()+"】检查端口失败："+re;
						result = new Result(false, message);
						break;
					}
				} catch (Exception e) {
					message = "Nginx主机【"+nginxHost.getHostIp()+"】检查端口失败："+e.getMessage();
					result = new Result(false, message);
					e.printStackTrace();
					break;
				}
			}else{
				message = "Nginx主机【"+nginxHost.getHostIp()+"】连接不成功！";
				result = new Result(false, message);
				break;
			}
//				//检查是否重名
//				List<System> list = systemService.listDeployedSystem();
//				for(System system:list){
//					if(!deployModel.getSystemId().equals(system.getSystemId())){
//						CompPort compPort = new CompPort(nginxId, nginxPortId, system.getDeployId(), Type.COMPPORT_TYPE.DEPLOY.name());
//						List<CompPort> compPorts = portService.selectAllCompPort(compPort);
//						if(null!=compPorts&&compPorts.size()>0){
//							message = "Nginx主机【"+nginx.getComponentIp()+"】端口号【"+nginxPort.getPort()+"】已被物理系统【"+system.getSystemName()+"】占用！";
//							result = new Result(false, message);
//							break;
//						}
//					}
//				}
		}
		//判断dns或者Ip端口是否重名
		Result validate = compPortManager.validateNginxPort(deployModel.getSystemId(),nginxGroupId,deployModel.getNginxPortId(), deployModel.isDnsEnable(), deployModel.getDomainName());
		if(!validate.isSuccess()){
			return validate;
		}
		return result;
	}
	/**
	* TODO
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年8月3日
	*/
	public Result deleteHost(User user, String systemId) {
		System system = systemService.getSystemById(systemId);
		if(null==system){
			logger.error("删除物理系统【"+systemId+"】失败，可能已被其他用户删除！");
			return new Result(false, "删除物理系统【"+systemId+"】失败，可能已被其他用户删除！");
		}
		if(system.getSystemDeployStatus()==1){
			logger.error("删除物理系统【"+system.getSystemName()+"】失败，此物理系统已部署运行！");
			return new Result(false, "删除物理系统【"+system.getSystemName()+"】失败，此物理系统已部署运行！");
		}
		if(StringUtils.isNotEmpty(user.getUserCompany())&&StringUtils.isNotEmpty(system.getEnvId())
				&&user.getUserCompany().equals(system.getEnvId())){
			logger.error("删除物理系统【"+system.getSystemName()+"】失败，用户没有权限！");
			return new Result(false, "删除物理系统【"+system.getSystemName()+"】失败，用户没有权限！");
		}
		Deploy deploy = new Deploy();
		deploy.setSystemId(systemId);
		List<Deploy> deploys = deployService.select(deploy);
		if(!deploys.isEmpty()){
			logger.error("删除物理系统【"+system.getSystemName()+"】失败，此物理系统存在历史部署信息！");
			return new Result(false, "删除物理系统【"+system.getSystemName()+"】失败，此物理系统存在历史部署信息！");
		}
		try{
			boolean re = systemService.delete(systemId);
			if(re){
				logger.warn("删除物理系统【"+system.getSystemName()+"】成功！");
				return new Result(true, "删除物理系统【"+system.getSystemName()+"】成功！");
			}else{
				logger.error("删除物理系统【"+system.getSystemName()+"】失败！");
				return new Result(false, "删除物理系统【"+system.getSystemName()+"】失败！");
			}
		}catch(Exception exception){
			logger.error(exception.getMessage());
			return new Result(false, "删除物理系统【"+system.getSystemName()+"】失败:"+exception.getMessage());
		}
		
	}
	/**
	* TODO
	* @author mayh
	* @return List<System>
	* @version 1.0
	* 2016年8月8日
	*/
	public List<System> searchByEnv(User user, System system) {
//		system.setEnvId(user.getUserCompany());
		system.setSystemDeployStatus((byte)Status.SYSTEM.DEPLOY.ordinal());
		List<System> systems = systemService.selectAllSystem(system);
		return systems;
	}
	/**
	* TODO切换镜像
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年8月8日
	*/
	public Result switchImage(String baseImageId, String switchSystems,User user) {
		String[] systemIds = switchSystems.split("===");
		int all = systemIds.length;
		int success = 0;
		int fail = 0;
		int left = systemIds.length;
		for(int i=0;i<systemIds.length;i++){
			String systemId = systemIds[i];
			System system = systemService.getSystemById(systemId);
			if(null==system){
				continue;
			}
			Deploy deploy = deployService.getDeploy(system.getDeployId());
			if(system==null||deploy==null){
				logger.error("系统守护停止失败！");
				fail+=1;
				left = all-i+1;
				TaskMessage begin = new TaskMessage("共切换"+all+"个物理系统，已成功执行切换"+success+"个，失败"+fail+"个。</br>还有"+left+"个物理系统待执行。", i, user.getUserId(),"imageId_"+baseImageId);
				taskCache.updateTask(begin);
				continue;
			}
			//停止物理系统守护
			try{
				platformDaemon.stopSystemDaemon(systemId);
			}catch(Exception e){
				logger.error("系统【"+system.getSystemName()+deploy.getDeployVersion()+"】守护停止失败！");
				fail+=1;
				left = all-i+1;
				TaskMessage begin = new TaskMessage("共切换"+all+"个物理系统，已成功执行切换"+success+"个，失败"+fail+"个。</br>还有"+left+"个物理系统待执行。", i, user.getUserId(),"imageId_"+baseImageId);
				taskCache.updateTask(begin);
				continue;
			}
			//停止弹性伸缩
			system.setSystemElsasticityStatus((byte)Status.ElSASTICITYSTATUS.UNUSED.ordinal());
			systemService.updateSystem(system);
			
			DeployModel deployModel = new DeployModel();
			BeanUtils.copyProperties(deploy, deployModel);
			long timestamp=java.lang.System.currentTimeMillis();//时间戳，用于创建唯一文件夹目录
			String newVersion = deploy.getDeployVersion().substring(0, deploy.getDeployVersion().lastIndexOf("_"))+"_"+timestamp;
			deployModel.setTimestamp(""+timestamp);
			deployModel.setDeployVersion(newVersion);//部署版本，如果版本不变，无需重新生成镜像
			deployModel.setAutoDeploy((byte)2);//版本切换
			deployModel.setInstanceCount(deploy.getInitCount());//实例数量
			deployModel.setSystemName(system.getSystemName());
			CompPort compPort = new CompPort();
			compPort.setUsedId(deploy.getDeployId());
			List<CompPort> compPorts = portService.selectAllCompPort(compPort);
			if(!compPorts.isEmpty()){
				deployModel.setNginxPortId(compPorts.get(0).getPortId());
			}
			List<DeployFile> files = deployFileService.selectByDeployId(deploy.getDeployId());
			String fileStr = "";
			//配置文件
			for(DeployFile file:files){
				fileStr+=file.getFileName() + "==="+file.getFilePath()+" ";
			}
			deployModel.setDeployFiles(fileStr);//上传配置文件及上传路径
			//数据库
			List<DeployDatabase> databaseInfos = deployDatabaseService.selectByDeployId(deploy.getDeployId());
			JSONArray dataArray = new JSONArray();
			for(DeployDatabase databaseInfo:databaseInfos){
				String databaseId = databaseInfo.getDatabaseId();
				String dataSource = databaseInfo.getDataSource();
				if(StringUtils.isNotEmpty(databaseId)&&StringUtils.isNotEmpty(dataSource)){
					JSONObject dataObject = new JSONObject();
					dataObject.put("databaseId",databaseId);
					dataObject.put("dataSource",dataSource);
					dataArray.add(dataObject);
				}
			}
			deployModel.setDatabaseInfo(dataArray.toString());//数据库
			TaskMessage begin = new TaskMessage("共切换"+all+"个物理系统，已成功执行切换"+success+"个，失败"+fail+"个。</br>还有"+left+"个物理系统待执行。</br>正在执行第"+(i+1)+"个。", i, user.getUserId(),"imageId_"+baseImageId);
			taskCache.updateTask(begin);
			Result result = this.deploy(deployModel, user);
			if(result.isSuccess()){
				success +=1;
			}else{
				fail +=1;
			}
			left = all-i-1;
			TaskMessage end = new TaskMessage("共切换"+all+"个物理系统，已成功执行切换"+success+"个，失败"+fail+"个。</br>还有"+left+"个物理系统待执行。", i, user.getUserId(),"imageId_"+baseImageId);
			taskCache.updateTask(end);
			//启动守护
			platformDaemon.startSystemDaemon(systemId);
		}
		return new Result(true, "共切换"+all+"个物理系统，已成功执行切换"+success+"个，失败"+fail+"个。");
	}
	/**
	* TODO
	* @author mayh
	* @return JSONArray
	* @version 1.0
	* 2016年8月10日
	*/
	public JSONArray showAllAccess(User user, String deployId) {
		JSONArray ja = new JSONArray();
		Deploy deploy = deployService.getDeploy(deployId);
		if(null==deploy){
			return new JSONArray();
		}
		String url = StringUtils.isEmpty(deploy.getUrl())?"":deploy.getUrl();
		if(deploy.getLoadBalanceType()==1){
			//f5类型
			Component component = new Component();
			component.setComponentId(deploy.getLoadBalanceId());
			component = componentService.getComponent(component);
			JSONObject jo = new JSONObject();
			//1、f5
			jo.put("name", component.getComponentName());
			jo.put("url", deploy.getHttp()+"://"+deploy.getF5ExternalIp()+":"+deploy.getF5ExternalPort()+"/"+deploy.getContentPath()+"/"+url);
			jo.put("type", "f5");
			ja.add(jo);
			//2、容器
			List<Container> containers = containerService.getBySysId(deploy.getSystemId());
			for(Container container:containers){
				Host host = hostService.getHost(container.getHostId());
				jo.put("name", container.getConName());
				jo.put("url", deploy.getHttp()+"://"+host.getHostIp()+":"+deploy.getF5ExternalPort()+"/"+deploy.getContentPath()+"/"+url);
				jo.put("type", "container");
				ja.add(jo);
			}
			
		}else if(deploy.getLoadBalanceType()==2){
			//nginx组类型
			ComponentExpand componentExpand = new ComponentExpand();
			componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
			
			JSONObject jo = new JSONObject();
			//1、f5
			Component f5 = new Component();
			f5.setComponentId(componentExpand.getF5CompId());
			f5 = componentService.getComponent(f5);
			jo.put("name",f5.getComponentName());
			jo.put("url", deploy.getHttp()+"://"+componentExpand.getF5ExternalIp()+":"+componentExpand.getF5ExternalPort()+"/"+deploy.getContentPath()+"/"+url);
			jo.put("type", "f5");
			ja.add(jo);
			//2.nginx
			Component nginx = new Component();
			nginx.setComponentId(deploy.getLoadBalanceId());
			nginx = componentService.getComponent(nginx);
			List<ComponentHost> componentHosts = hostService.selectComponentHost(deploy.getLoadBalanceId());
			for(ComponentHost componentHost:componentHosts){
				Host host = hostService.getHost(componentHost.getHostId());
				jo.put("name",nginx.getComponentName());
				jo.put("url", deploy.getHttp()+"://"+host.getHostIp()+":"+deploy.getNginxPort()+"/"+deploy.getContentPath()+"/"+url);
				jo.put("type", "nginx");
				ja.add(jo);
			}
			//3、容器
			List<Container> containers = containerService.getBySysId(deploy.getSystemId());
			for(Container container:containers){
				List<ConPort> conPorts;
				jo.put("name", container.getConTempName());
				try {
					conPorts = conportService.listConPorts(container.getConId());
					for(ConPort conPort:conPorts){
						jo.put("http", deploy.getHttp());
						jo.put("url", deploy.getHttp()+"://"+conPort.getConIp()+":"+conPort.getPubPort()+"/"+deploy.getContentPath()+"/"+url);
						jo.put("type", "container");
						ja.add(jo);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}else if(deploy.getLoadBalanceType()==3){
			JSONObject jo = new JSONObject();
			//3 nginx热备
			Component nginx = new Component();
			nginx.setComponentId(deploy.getLoadBalanceId());
			nginx = componentService.getComponent(nginx);
			if(nginx.getKeepaliveIp() != null && !"".equals(nginx.getKeepaliveIp())){
				//keepalive虚拟地址
				jo.put("name","keepalive虚拟地址");
				jo.put("url", deploy.getHttp()+"://"+nginx.getKeepaliveIp()+":"+deploy.getNginxPort()+"/"+deploy.getContentPath()+"/"+url);
				jo.put("type", "nginxHot");
				ja.add(jo);
			}
			//主nginx
			jo.put("name","主Nginx");
			jo.put("url", deploy.getHttp()+"://"+nginx.getComponentIp()+":"+deploy.getNginxPort()+"/"+deploy.getContentPath()+"/"+url);
			jo.put("type", "nginxHot");
			ja.add(jo);
			
			//备nginx
			if(nginx.getBackComponentIp() != null && !"".equals(nginx.getBackComponentIp())){
				//keepalive虚拟地址
				jo.put("name","备Nginx");
				jo.put("url", deploy.getHttp()+"://"+nginx.getBackComponentIp()+":"+deploy.getNginxPort()+"/"+deploy.getContentPath()+"/"+url);
				jo.put("type", "nginxHot");
				ja.add(jo);
			}
			//3、容器
			List<Container> containers = containerService.getBySysId(deploy.getSystemId());
			for(Container container:containers){
				List<ConPort> conPorts;
				jo.put("name", container.getConTempName());
				try {
					conPorts = conportService.listConPorts(container.getConId());
					for(ConPort conPort:conPorts){
						jo.put("http", deploy.getHttp());
						jo.put("url", deploy.getHttp()+"://"+conPort.getConIp()+":"+conPort.getPubPort()+"/"+deploy.getContentPath()+"/"+url);
						jo.put("type", "container");
						ja.add(jo);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return ja;
	}
}