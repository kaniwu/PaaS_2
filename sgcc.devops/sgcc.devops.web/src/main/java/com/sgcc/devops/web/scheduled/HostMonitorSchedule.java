package com.sgcc.devops.web.scheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.web.monitor.HostMonitorTask;

/**
 * 主机监控调度任务
 * @author dmw
 *
 */
@Component
public class HostMonitorSchedule {

	@Autowired
	private HostMonitorTask hostMonitorTask;

	// 秒 分 时 天 月 周 年
	@Scheduled(cron = "${monitor.hostdate:0/30 * * * * ?}")
	protected void executeInsertMonitor() {
		hostMonitorTask.hostMonitor();
	}

}
