package com.sgcc.devops.web.upload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import com.sgcc.devops.web.message.MessageHandshakeInterceptor;

@Configuration
@EnableWebMvc
@EnableWebSocket
public class UploadServiceConfig extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

	@Autowired
	private UploadServiceWebSocketHandler uploadServiceWebSocketHandler;
	@Autowired
	private MessageHandshakeInterceptor messageHandshakeInterceptor;

	@Bean
	public DefaultHandshakeHandler handshakeHandler() {
		return new DefaultHandshakeHandler();
	}

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		registry.addHandler(this.uploadServiceWebSocketHandler, "uploadService")
				.addInterceptors(this.messageHandshakeInterceptor).setHandshakeHandler(handshakeHandler());
		registry.addHandler(this.uploadServiceWebSocketHandler, "/sockjs/uploadService")
				.addInterceptors(this.messageHandshakeInterceptor).setHandshakeHandler(handshakeHandler()).withSockJS();
	}
}
