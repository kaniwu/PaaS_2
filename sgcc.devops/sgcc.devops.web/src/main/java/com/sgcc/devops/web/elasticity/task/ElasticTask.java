package com.sgcc.devops.web.elasticity.task;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSONArray;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.constant.Type.TARGET_TYPE;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.MonitorService;
import com.sgcc.devops.web.elasticity.bean.DecisionResult;
import com.sgcc.devops.web.elasticity.bean.LoadData;
import com.sgcc.devops.web.elasticity.cache.ElasticCache;
import com.sgcc.devops.web.elasticity.engine.DecideEngine;
import com.sgcc.devops.web.elasticity.engine.SystemLoadParseEngine;
import com.sgcc.devops.web.manager.ContainerManager;

import net.sf.json.JSONObject;

/**
 * 弹性伸缩任务
 * 
 * @author dmw
 *
 */
public class ElasticTask implements Callable<Result> {

	private static Logger logger = Logger.getLogger(ElasticTask.class);

	private String systemId;
	private List<Container> containerList;
	private MonitorService monitorService;
	private ContainerManager containerManager;
	private ElasticStrategy strategy;
	private ElasticCache cache;

	public ElasticTask(String systemId, List<Container> containerList, MonitorService monitorService,
			ContainerManager containerManager, ElasticStrategy strategy, ElasticCache cache) {
		super();
		this.systemId = systemId;
		this.containerList = containerList;
		this.monitorService = monitorService;
		this.containerManager = containerManager;
		this.strategy = strategy;
		this.cache = cache;
	}

	@Override
	public Result call() throws Exception {
		logger.info("start to do elastic task");
		if (null == containerManager || null == monitorService) {
			logger.warn("TASK INIT FAILED!");
			return new Result(false, "TASK INIT FAILED!");
		}
		if (null == systemId) {
			logger.warn("NO SYSTEM ID!!");
			return new Result(false, "NO SYSTEM ID!!");
		}
		if (null == containerList || containerList.isEmpty()) {
			logger.warn("No container is in the system!");
			return new Result(true, "success");
		}
		if (null == cache) {
			logger.warn("No cache inited!");
			return new Result(false, "No cache inited!");
		}
		List<Monitor> loadDatas = new ArrayList<Monitor>();
		List<Monitor> temp = new ArrayList<Monitor>();
		logger.info("start to get system monitor data");
		for (Container container : containerList) {
			temp = this.monitorService.selectLastMonitorData((byte) TARGET_TYPE.CONTAINER.ordinal(),
					container.getConId(), 300000l);
			if (null != temp) {
				loadDatas.addAll(temp);
			}
		}
		logger.info("get system monitor data over!");
		SystemLoadParseEngine loadParsor = new SystemLoadParseEngine();
		LoadData loadData = loadParsor.parse(loadDatas);
		logger.info("parse load data over!");
		// 判断是否满足扩展条件
		DecideEngine decideEngine = new DecideEngine(loadData, strategy, 0);
		DecisionResult decision = decideEngine.decide();
		JSONObject params = new JSONObject();
		params.put("systemId", systemId);
		Result result = null;
		// 判断是否满足收缩条件
		logger.info("Elastic decision is :" + decision.toString());
		switch (decision.getDecision()) {
		case SCALE_IN: {// 满足收缩条件
			logger.warn("当前系统需要收缩，接下来会进行收缩操作");
			JSONArray containers = new JSONArray();
			List<String> scaleInContainers = this.judgeScaleInContainer(decision.getStep(), strategy);
			if (scaleInContainers.isEmpty()) {
				logger.warn("当前系统已经达到最小节点数，不能继续收缩节点");
				result = new Result(false, "当前系统已经达到最小节点数，不能继续收缩节点");
				break;
			}
			containers.addAll(scaleInContainers);
			params.put("containerId", containers);
			logger.info("Elastic param is :" + params.toString());
			// 看看当前系统中是否有正在处理的伸缩任务
			if (cache.taskExist(systemId)) {
				logger.warn("The System already have elastic task runing!!! Drop this task!");
				result = new Result(false, "The System already have elastic task runing!!! Drop this task!");
			} else {
				cache.addTask(systemId);
				result = this.containerManager.stopAndRemoveCon(params);
				cache.removeTask(systemId);
			}
			break;
		}
		case SCALE_OUT: {// 满足扩展条件
			logger.warn("当前系统需要扩展！接下来进行节点扩展");
			if (containerList.size() + decision.getStep() > strategy.getMaxNode()) {
				int tempstep = strategy.getMaxNode() - containerList.size();
				if (tempstep < 0) {
					tempstep = 0;
				}
				decision.setStep(tempstep);
			}
			if (decision.getStep() <= 0) {
				logger.warn("系统已经到达最大节点数，无法继续扩展!");
				result = new Result(false, "系统已经到达最大节点数，无法继续扩展!");
				break;
			}
			params.put("conCount", decision.getStep());
			// 看看当前系统中是否有正在处理的伸缩任务
			if (cache.taskExist(systemId)) {
				logger.warn("The System already have elastic task runing!!! Drop this task!");
				result = new Result(false, "The System already have elastic task runing!!! Drop this task!");
			} else {
				params.put("userId", "system");
				logger.info("Elastic param is :" + params.toString());
				cache.addTask(systemId);
				result = this.containerManager.createConBySysId(params);
				cache.removeTask(systemId);
			}
			break;
		}
		case NO_ACTION: {// 没有任何动作
			logger.info("The System need to do nothing!");
			result = new Result(true, "The System need to do nothing!");
			break;
		}
		case EXCEPTION: {// 数据提供异常
			logger.warn("The system do not have any elastic strategy or load data");
			result = new Result(false, "The system do not have any elastic strategy or load data");
			break;
		}
		default: {// 位置结果
			logger.warn("Unexcepted thing occured!");
			result = new Result(false, "Unexcepted thing occured!");
			break;
		}
		}
		logger.info("System["+systemId+"]Elastic Task result is " + result.toString());
		return result;
	}

	/**
	 * 判断需要进行收缩的容器ID列表， 收缩后的容器数量不能小于系统伸缩策略的最小节点数。
	 * 如果当前容器数量减去计划缩减的容器数量小于最小节点数，那么将减少计划缩减的容器数量。
	 * 筛选容器是否删除的策略是随机方式。即如果当前需要从10个容器节点中删除3个容器，则取0-9之间的3个不同的随机数。然后把
	 * 
	 * @param step
	 * @param strategy
	 * @return
	 */
	private List<String> judgeScaleInContainer(Integer step, ElasticStrategy strategy) {
		logger.info("start to judge the sacle in container...");
		int containerListSize = containerList.size();
		if (null == step || containerListSize <= strategy.getMinNode()) {
			// 没有收缩或者当前系统的容器数本身就小于伸缩策略规定的最小节点数，则不处理
			logger.warn("no step or container");
			return new ArrayList<String>();
		}
		int finalNum = 0;
		if (containerListSize - step <= strategy.getMinNode()) {
			// 当前系统节点数减去计划缩减数小于系统规定的最小节点数，取当前节点数减去最小节点数作为最终需要缩减的容器数量。
			logger.warn("容器数量不足，无法收缩至最小节点以下");
			finalNum = containerList.size() - strategy.getMinNode();
		} else {
			finalNum = step;
		}
		if (finalNum == 0) {
			return new ArrayList<String>();
		}
		SecureRandom secureRandom = new SecureRandom();
		List<Integer> indexList = new ArrayList<Integer>();
		logger.info("start to get random index of container list");
		while (indexList.size() < finalNum) {
			int number = Math.abs(secureRandom.nextInt() % containerListSize);
			if (indexList.contains(number)) {
				continue;
			} else {
				indexList.add(number);
			}
		}
		List<String> idList = new ArrayList<String>(finalNum);
		for (Integer index : indexList) {
			idList.add(containerList.get(index).getConId());
		}
		logger.info("final sacle container is list is " + idList.toString());
		return idList;
	}
}
