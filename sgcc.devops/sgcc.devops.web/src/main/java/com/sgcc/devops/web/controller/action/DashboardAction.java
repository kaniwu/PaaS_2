package com.sgcc.devops.web.controller.action;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.web.manager.DashboardManager;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午1:47:21
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：DashboardAction.java
 * description：首页图标数据访问控制类  
 */
@Controller
@RequestMapping("dashboard")
public class DashboardAction {

	@Autowired
	private DashboardManager dashboardManager;

	/**
	 * @author zhangs
	 * @time 2015年11月16日
	 * @description
	 */
	@RequestMapping(value = "/getIndexJson", method = { RequestMethod.POST })
	@ResponseBody
	public String getIndexJson(HttpServletRequest request) {

		// 首页数据
		JSONObject resultObject = dashboardManager.getIndexJson();

		// 主机
		JSONObject hostInfo = dashboardManager.getHostInfo();
		resultObject.put("hostInfo", hostInfo);
		// 主机

		// 容器
		JSONObject containerInfo = dashboardManager.getContainerInfo();
		resultObject.put("containerInfo", containerInfo);

		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("data", resultObject);
		return result.toString();
	}

	/**
	 * @author zhangs
	 * @time 2015年11月16日
	 * @description
	 */
	@RequestMapping(value = "/getJson", method = { RequestMethod.POST })
	@ResponseBody
	public String getJson(HttpServletRequest request) {
		// cluster registry bussiness system
		JSONObject resultObject = dashboardManager.getTopJson();

		// 主机
		JSONObject hostInfo = dashboardManager.getHostInfo();
		resultObject.put("hostInfo", hostInfo);
		// 主机

		// 容器
		JSONObject containerInfo = dashboardManager.getContainerInfo();
		resultObject.put("containerInfo", containerInfo);
		// 容器

		// 负载
		JSONObject nginxInfo = dashboardManager.getNginxInfo();
		resultObject.put("nginxInfo", nginxInfo);

		// 负载

		// 镜像
		JSONArray imageInfo = dashboardManager.getImageInfo();

		resultObject.put("imageInfo", imageInfo);
		// 镜像

		// 主机top10

		// 集群top10
		JSONArray clusterTopInfo = dashboardManager.getClusterTop();

		// 主机top10
		resultObject.put("hostTopInfo", clusterTopInfo);

		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("data", resultObject);
		return result.toString();
	}

	/**
	 * @author zhangs
	 * @time 2015年11月16日
	 * @description
	 */
	@RequestMapping(value = "/getTop10Json", method = { RequestMethod.POST })
	@ResponseBody
	public String getTop10Json(HttpServletRequest request) {
		JSONArray tempArray = new JSONArray();
		// 集群下主机top10
		String clusterName = request.getParameter("name");
		// 主机top10
		tempArray = dashboardManager.getHostTop(clusterName);

		JSONObject result = new JSONObject();
		result.put("success", true);
		result.put("data", tempArray);
		return result.toString();
	}

}
