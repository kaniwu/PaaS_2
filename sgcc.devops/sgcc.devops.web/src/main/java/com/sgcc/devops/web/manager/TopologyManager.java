/**
 * 
 */
package com.sgcc.devops.web.manager;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.core.detector.DbDetector;
import com.sgcc.devops.core.detector.F5Detector;
import com.sgcc.devops.core.detector.NginxDetector;
import com.sgcc.devops.dao.entity.Business;
import com.sgcc.devops.dao.entity.CompPort;
import com.sgcc.devops.dao.entity.Component;
import com.sgcc.devops.dao.entity.ComponentExpand;
import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Deploy;
import com.sgcc.devops.dao.entity.DeployDatabase;
import com.sgcc.devops.dao.entity.DeployFile;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.dao.intf.DeployFileMapper;
import com.sgcc.devops.service.BusinessService;
import com.sgcc.devops.service.BusinessSysService;
import com.sgcc.devops.service.ComponentExpandService;
import com.sgcc.devops.service.ComponentService;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.DeployDatabaseService;
import com.sgcc.devops.service.DeployNgnixService;
import com.sgcc.devops.service.DeployService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.service.PortService;
import com.sgcc.devops.service.SystemService;
import com.sgcc.devops.web.image.Encrypt;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**  
 * date：2016年2月4日 下午2:00:53
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：TopologyManager.java
 * description：  拓扑图形获取数据处理类
 */
@org.springframework.stereotype.Component
public class TopologyManager {
	@Resource
	private DeployService deployService;
	@Resource
	private ComponentService componentService;
	@Resource
	private PortService portService;
	@Resource
	private DeployNgnixService deployNgnixService;
	@Resource
	private DeployDatabaseService deployDatabaseService;
	@Resource
	private SystemService systemService;
	@Resource
	private BusinessService businessService;
	@Resource
	private ContainerService containerService;
	@Resource
	private BusinessSysService businessSysService;
	@Autowired
	private LocalConfig localConfig;
	@Resource
	private DeployFileMapper deployFileMapper;
	@Resource
	private HostService hostService;
	@Resource
	private ComponentExpandService componentExpandService;
	private static Logger logger = Logger.getLogger(MonitorManager.class);

	/**
	 * 根据业务系统查询本业务系统相关的物理系统部署版本
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年11月25日
	 */
	public JSONArray getAllTopologysByBusinessId(User user, String businessId,String envId) {
		JSONArray jsonArray = new JSONArray();
		JSONArray ja = businessSysService.getSysByBusinessId(user,businessId,envId);
		if(ja==null){
			return new JSONArray();
		}
		for (int i = 0; i < ja.size(); i++) {
			if (null != ja && ja.getJSONObject(i).containsKey("deployId")) {
				jsonArray.add(this.selectTopologyData(ja.getJSONObject(i).getString("deployId"),
						ja.getJSONObject(i).getString("systemName")));
			}

		}
		return jsonArray;
	}

	/**
	 * 查询物理系统发布版本信息
	 * @author mayh
	 * @return JSONObject
	 * @version 1.0 2015年11月25日
	 */
	public JSONObject selectTopologyData(String deployId, String systemName) {
		JSONObject resultobj = new JSONObject();
		JSONArray resultArray = new JSONArray();
		// 根据sysId查询deploy
		Deploy deploy = deployService.getDeploy(deployId);
		if (null == deploy) {
			resultobj.put("success", false);
			return resultobj;
		}
		// F5==============================
		if (1==deploy.getLoadBalanceType()) {
			Component f5 = new Component();
			f5.setComponentId(deploy.getLoadBalanceId());
			f5 = componentService.getComponent(f5);
			if (null != f5) {
				JSONObject obj = new JSONObject();
				obj.put("id", "f5_" + f5.getComponentId());
				obj.put("name", f5.getComponentName());

				boolean result = false;
				// F5测试
				F5Detector detector = new F5Detector(f5.getComponentIp(), f5.getComponentUserName(),
						Encrypt.decrypt(f5.getComponentPwd(), localConfig.getSecurityPath()));
				result = detector.normal();
				obj.put("status", result ? 1 : 0);
				resultArray.add(obj);
			}
		}
		if (2==deploy.getLoadBalanceType()) {
			// 组件nginx=============================
			Component nginxComp = new Component();
			nginxComp.setComponentId(deploy.getLoadBalanceId());
			nginxComp = componentService.getComponent(nginxComp);
			if(null !=nginxComp){
				//如果是nginx组
				if(nginxComp.getComponentType()==6){
					JSONObject nginxobj = new JSONObject();
					nginxobj.put("id", "compNginx_" + nginxComp.getComponentId());
					nginxobj.put("name", nginxComp.getComponentName());
					nginxobj.put("nginxId", nginxComp.getComponentId());
					nginxobj.put("nginxPort", deploy.getNginxPort());
					CompPort compPort=new CompPort();
					compPort.setUsedId(deployId);
					compPort.setCompId(deploy.getLoadBalanceId());
					List<CompPort> compPorts = portService.selectAllCompPort(compPort);
					for (CompPort compPort2 : compPorts) {
						nginxobj.put("nginxPortId", compPort2.getPortId());
					}
					Result result = new Result();
					List<ComponentHost> nginxHosts = hostService.selectComponentHost(nginxComp.getComponentId());
					for(ComponentHost componentHost:nginxHosts){
						// nginx测试
						NginxDetector detector = new NginxDetector(componentHost.getHostIp(), componentHost.getHostUser(),
								Encrypt.decrypt(componentHost.getHostPwd(), localConfig.getSecurityPath()));
						result = detector.normal();
						if(result.isSuccess()){
							break;
						}
					}
					nginxobj.put("status", result.isSuccess() ? 1 : 0);
					resultArray.add(nginxobj);
					//nginx组件是否使用F5
					ComponentExpand componentExpand = componentExpandService.selectByCompId(deploy.getLoadBalanceId());
					if(null!=componentExpand&&null!=componentExpand.getF5CompId()){
						Component f5Comp = new Component();
						f5Comp.setComponentId(componentExpand.getF5CompId());
						f5Comp = componentService.getComponent(f5Comp);
						if(null!=f5Comp){
							JSONObject obj = new JSONObject();
							obj.put("id", "f5_" + f5Comp.getComponentId());
							obj.put("name", f5Comp.getComponentName());

							boolean resultf = false;
							// F5测试
							F5Detector detector = new F5Detector(f5Comp.getComponentIp(), f5Comp.getComponentUserName(),
									Encrypt.decrypt(f5Comp.getComponentPwd(), localConfig.getSecurityPath()));
							resultf = detector.normal();
							obj.put("status", resultf ? 1 : 0);
							resultArray.add(obj);
						}
					}
				}else{
					JSONObject obj = new JSONObject();
					obj.put("id", "compNginx_" + nginxComp.getComponentId());
					obj.put("name", nginxComp.getComponentName());
					obj.put("nginxId", nginxComp.getComponentId());
					obj.put("nginxPort", deploy.getNginxPort());
					//组件nginx端口
					CompPort compPort=new CompPort();
					compPort.setUsedId(deployId);
					compPort.setCompId(deploy.getLoadBalanceId());
					List<CompPort> compPorts = portService.selectAllCompPort(compPort);
					for (CompPort compPort2 : compPorts) {
						obj.put("nginxPortId", compPort2.getPortId());
					}
					resultArray.add(obj);
					Result result = new Result();
					// nginx测试
					NginxDetector detector = new NginxDetector(nginxComp.getComponentIp(), nginxComp.getComponentUserName(),
							Encrypt.decrypt(nginxComp.getComponentPwd(), localConfig.getSecurityPath()));
					result = detector.normal();
					obj.put("status", result.isSuccess() ? 1 : 0);
					resultArray.add(obj);
				}
			}
		}
		// 容器==============================
		// 所有容器
		List<Container> containers = containerService.getBySysId(deploy.getSystemId());
		for (Container container : containers) {
			if(container.getConType()!=Type.CON_TYPE.APP.ordinal()){
				continue;
			}
			JSONObject obj = new JSONObject();
			obj = new JSONObject();
			obj.put("id", "docker_" + container.getConId());
			// 关机
			if (Status.CONTAINER.DELETE.ordinal() == container.getConStatus()) {
				obj.put("name", "【" + container.getConTempName() + "】已删除");
				obj.put("status", 0);
			} else if (Status.CONTAINER.EXIT.ordinal() == container.getConStatus()) {
				obj.put("name", "【" + container.getConTempName() + "】已停止");
				obj.put("status", 0);
			} else if (Status.CONTAINER.HALT.ordinal() == container.getConStatus()) {
				obj.put("name", "【" + container.getConTempName() + "】已挂起");
				obj.put("status", 0);
			} else if (Status.CONTAINER.UP.ordinal() == container.getConStatus()) {
				if (Status.POWER.OFF.ordinal() == container.getConPower()) {
					obj.put("name", "【" + container.getConTempName() + "】已停止");
					obj.put("status", 0);
				} else if (Status.POWER.UP.ordinal() == container.getConPower()) {
					obj.put("name", container.getConTempName());
					obj.put("status", 1);
				}
			} else {
				obj.put("name", "【" + container.getConTempName() + "】异常");
				obj.put("status", 0);
			}
			resultArray.add(obj);
		}
		// 数据库========================================
		List<DeployDatabase> deployDatabases = deployDatabaseService.selectByDeployId(deployId);
		for (DeployDatabase dataInfo : deployDatabases) {
			JSONObject obj = new JSONObject();
			Component db = new Component();
			db.setComponentId(dataInfo.getDatabaseId());
			Component databaseInfo = componentService.getComponent(db);
			obj = new JSONObject();
			obj.put("id", "db_" + databaseInfo.getComponentId());
			obj.put("name", databaseInfo.getComponentName());
			obj.put("database",databaseInfo.getComponentId());
			obj.put("databaseName", databaseInfo.getComponentName());
			obj.put("driver",dataInfo.getDriverId());
			obj.put("initSql",dataInfo.getInitSql());
			obj.put("driverName", "");
			obj.put("jndi", dataInfo.getDataSource());
			boolean result = false;
			// 数据库测试
			DbDetector detector = new DbDetector(databaseInfo.getComponentJdbcurl(),
					databaseInfo.getComponentUserName(), Encrypt.decrypt(databaseInfo.getComponentPwd().toString(), localConfig.getSecurityPath()),
					databaseInfo.getComponentDriver());
			result = detector.normal();
			obj.put("status", result ? 1 : 0);
			resultArray.add(obj);
		}
		resultobj.put("appFilename", deploy.getAppFilename());
		resultobj.put("appImageId", deploy.getAppImageId());
		resultobj.put("appName", deploy.getAppName());
		resultobj.put("appRoute", deploy.getAppRoute());
		resultobj.put("baseImageId", deploy.getBaseImageId());
		resultobj.put("clusterId", deploy.getClusterId());
		resultobj.put("configRoute", deploy.getConfigRoute());
		resultobj.put("contentPath", deploy.getContentPath());
		resultobj.put("locationLeavel", deploy.getLocationLeavel());
		resultobj.put("cpu", deploy.getCpu());
		resultobj.put("deployId", deploy.getDeployId());
		resultobj.put("appFilename", deploy.getAppFilename());
		resultobj.put("version", deploy.getDeployVersion());
		resultobj.put("dnsEnable", deploy.getDnsEnable());
		resultobj.put("domainName", deploy.getDomainName());
		resultobj.put("dockerfile", deploy.getDockerfile());
		resultobj.put("loadBalanceType", deploy.getLoadBalanceType());
		resultobj.put("loadBalanceTId", deploy.getLoadBalanceId());
		
		resultobj.put("f5ExternalIp", deploy.getF5ExternalIp());
		resultobj.put("f5ExternalPort", deploy.getF5ExternalPort());
		resultobj.put("http", deploy.getHttp());
		resultobj.put("memery", deploy.getMemery());
		resultobj.put("nginxPort", deploy.getNginxPort());
		resultobj.put("port", deploy.getPort());
		resultobj.put("regularlyPublish", deploy.getRegularlyPublish());
		resultobj.put("publishTime",  deploy.getPublishTime());
		resultobj.put("systemName", systemName);
		resultobj.put("url", deploy.getUrl());
		resultobj.put("systemId", deploy.getSystemId());
		resultobj.put("systemAppId", deploy.getSystemAppId());
		resultobj.put("versionChange", deploy.getVersionChange());
		resultobj.put("locationStragegy", deploy.getLocationStragegy());
		
		resultobj.put("xms", deploy.getXms());
		resultobj.put("xmx", deploy.getXmx());
		
		resultobj.put("success", true);
		
		
		resultobj.put("data", resultArray);
		//配置文件
		List<DeployFile> deployFiles = deployFileMapper.selectByDeployId(deploy.getDeployId());
		resultobj.put("deployFiles", deployFiles);
		return resultobj;
	}

	// 获取系统名称
	public String getSystemName(String systemId) {
		System system = systemService.loadById(systemId);
		if (system != null) {
			return system.getSystemName();
		} else {
			logger.warn("system not exist");
			return "";
		}
	}

	// 获取业务系统名称
	public String getBusinessName(String businessId) {
		Business business = businessService.load(businessId);
		if (business != null) {
			return business.getBusinessName();
		} else {
			logger.warn("business not exist");
			return "";
		}
	}
}
