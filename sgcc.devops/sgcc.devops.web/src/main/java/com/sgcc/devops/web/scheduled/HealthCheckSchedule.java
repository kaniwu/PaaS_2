package com.sgcc.devops.web.scheduled;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 健康检测调度任务
 * @author dmw
 *
 */
@Component
public class HealthCheckSchedule {

	@Autowired
	private ContainerService containerService;
	@Autowired
	private HostService hostService;
	@Autowired
	private TaskCache cache;

	/**
	 * 定时插入监控数据
	 */
	// 秒 分 时 天 月 周 年
	@Scheduled(cron = "${monitor.health:0/30 * * * * ?}")
	protected void execute() {
		Map<String, Integer> hostMap = cache.getHostCache();
		Map<String, Integer> contMap = cache.getContainerCache();
		Map<String, Integer> hostMonitorCache = cache.getHostMonitorCache();
		Host host = null;
		Container container = null;
		for (String id : hostMap.keySet()) {
			if (hostMap.get(id) > 5) {
				host = hostService.getHost(id);
				if (null == host) {
					continue;
				}
				//下线
				host.setHostStatus((byte) Status.HOST.OFFLINE.ordinal());
				hostService.update(host);
				hostMap.remove(id);
				hostMonitorCache.remove(id);
			} else {
				continue;
			}
		}
		for (String id : hostMonitorCache.keySet()) {
			//主机监控程序
			if(hostMonitorCache.get(id)>5){
				host = hostService.getHost(id);
				if (null == host) {
					continue;
				}
				//监控程序异常
				host.setHostStatus((byte) Status.HOST.ABNORMAL.ordinal());
				hostService.update(host);
				hostMonitorCache.remove(id);
			}
		}
		for (String id : contMap.keySet()) {
			if (contMap.get(id) < 5) {
				continue;
			}
			container = new Container();
			container.setConId(id);
			container = containerService.getContainer(container);
			if (null == container) {
				continue;
			}
			container.setConPower((byte) Status.POWER.OFF.ordinal());
			try {
				containerService.modifyContainer(container);
				cache.getContainerCache().remove(id);
			} catch (Exception e) {
				continue;
			}
		}
//		cache.getContainerCache().clear();
//		cache.getHostCache().clear();
	}
}
