package com.sgcc.devops.web.elasticity.enums;

/**
 * 冲突策略
 * 
 * @author dmw
 *
 */
public enum ConflictStrategy {

	SCALE_IN_FIRST, // 收缩优先
	SCALE_OUT_FIRST;// 扩展优先
}
