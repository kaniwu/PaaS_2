package com.sgcc.devops.web.elasticity.enums;

/**
 * 弹性决策条目枚举类
 * @author dmw
 *
 */
public enum ElasticItemName {

	CPU, MEM, NET, DISK, LINK, NULL;
}
