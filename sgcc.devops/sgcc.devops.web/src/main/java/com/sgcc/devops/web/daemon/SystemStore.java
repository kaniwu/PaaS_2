package com.sgcc.devops.web.daemon;

public class SystemStore {
	private String systemId;
	private long beginTime=0;
	private int restartTimes=0;
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public long getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(long beginTime) {
		this.beginTime = beginTime;
	}
	public int getRestartTimes() {
		return restartTimes;
	}
	public void setRestartTimes(int restartTimes) {
		this.restartTimes = restartTimes;
	}
	
}
