package com.sgcc.devops.web.controller.action;


import java.util.Date;
import java.util.TimeZone;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.EmailCfg;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.manager.UserManager;
import com.sgcc.devops.web.query.QueryList;
import com.sgcc.devops.web.util.Base64;
import com.sgcc.devops.web.util.EmailSendUtil;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("user")
public class UserAction {
	Logger logger = Logger.getLogger(UserAction.class);

	@Resource
	private QueryList queryList;
	@Resource
	private UserManager userManager;
	@Autowired
	private EmailCfg config;
	@Autowired
	private LocalConfig localConfig;
	/*
	 * 用户详情列表
	 * by luogan
	 */
	@RequestMapping("/detail/{id}.html")
	public ModelAndView detail(@PathVariable String id) {
		ModelAndView mav = new ModelAndView();
		User user = userManager.detail(id);
		if(user!=null){
			mav.addObject("userInfo", user);
			mav.setViewName("user/detail");
		}else{
			mav.setViewName("/404");
		}
		return mav;
	}
	
	/*
	 * 查询用户列表 
	 * 包含按照用户名称进行模糊查询
	 * by luogan
	 */
	@RequestMapping(value = "/all")
	@ResponseBody
	public GridBean all(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, User user)
			throws Exception, Exception {
		if (user.getUserName() != null) {
			String search_name = request.getParameter("userName").trim();
			user.setUserName(search_name);
		}
		User creator = (User) request.getSession().getAttribute("user");
		return queryList.userList(creator.getUserId(), page, rows, user);
	}

	/*
	 * 创建用户
	 * by luogan
	 */
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, User user)
			throws Exception {
		if (user != null) {
			User creator = (User) request.getSession().getAttribute("user");
			user.setUserCreator(creator.getUserId());
			user.setUserStatus((byte) Status.USER.NORMAL.ordinal());
			user.setUserLevel(1);
			//创建用户时默认初始化密码是123456
			user.setUserPass(Encrypt.encrypt("123456", localConfig.getSecurityPath()));
			TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
			user.setUserCreatedate(new Date());
			String path = request.getContextPath();
			String bath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
			return userManager.create(user,bath);
		} else {
			return new Result(false, "用户添加失败：用户参数传入异常！");
		}
	}

	/*
	 * 更新用户
	 * by luogan
	 */
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, User user){
		if (user == null) {
			return new Result(false, "修改用户失败：传入用户参数有误");
		} else {
			return userManager.update(user);
		}
	}

	/*
	 * 删除用户 
	 * by luogan
	 */
	@RequestMapping(value = "/delete/{id}")
	@ResponseBody
	public Result delete(HttpServletRequest request, @PathVariable String id) {
		if (null == id) {
			return new Result(false, "传入参数为空！");
		} else {
			return userManager.delete(id);
		}
	}
	
	/*
	 * 激活用户 
	 * by luogan
	 */
	@RequestMapping(value = "/active/{id}")
	@ResponseBody
	public Result active(HttpServletRequest request, @PathVariable String id) {
		if (null == id) {
			return new Result(false, "传入参数为空！");
		} else {
			return userManager.active(id);
		}
	}

	/*
	 * 角色授权给用户 
	 * by luogan
	 */
	@RequestMapping(value = "/authToUser", method = { RequestMethod.POST })
	@ResponseBody
	public Result authToUser(HttpServletRequest request, String users,
			String roles) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("userId", user.getUserId());
		params.put("users", users);
		params.put("roles", roles);
		return userManager.authToUser(params);
	}
	
	/*
	 * 用户名唯一性验证
	 * by luogan
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public Boolean checkName(HttpServletRequest request, String userName) {
		return userManager.checkUserName(userName);
	}
	
	/**
	 * 多条件查询用户列表
	 * @param request
	 * @param pagenum
	 * @param pagesize
	 * @param user
	 * @return
	 */
	@RequestMapping("/advancedSearch")
	@ResponseBody
	public GridBean advancedSearch(HttpServletRequest request,
			@RequestParam(value = "page", required = true) int pagenum,
			@RequestParam(value = "rows", required = true) int pagesize, User user) {
		try {
			User userSession = (User) request.getSession().getAttribute("user");
			String params = request.getParameter("params").trim();
			String values = request.getParameter("values").trim();
			JSONObject json_object = new JSONObject();
			json_object.put("params", params);
			json_object.put("values", values);
			return userManager.advancedSearchUser(userSession.getUserId(), pagenum, pagesize, user,json_object);

		} catch (Exception e) {
			logger.error("查询用户列表失败！", e);
			return null;
		}
	}

	/**
	 * 用户登出系统
	 * @param request
	 * @param userId
	 */
	@RequestMapping(value = "/logout", method = { RequestMethod.POST })
	public void logout(HttpServletRequest request, @RequestParam int userId){
		request.getSession().invalidate();
	}
	
	/*
	 * 忘记密码
	 * by luogan
	 */
	@RequestMapping(value = "/forgetPass", method = { RequestMethod.POST })
	@ResponseBody
	public Result forgetPass(HttpServletRequest request, String userName) { 
		User user = userManager.getUserByName(userName);
		if (user == null) {
			return new Result(false,"该用户不存在,请输入正确的用户名！");
		} else {
			if(user.getUserStatus()==Status.USER.DELETE.ordinal()){
				return new Result(false,"该用户已被冻结，请联系管理员！");
			}
			try {
				String path = request.getContextPath();
				String bath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
						+ path + "/";
				EmailSendUtil.sendMail(config, "cmbc账户密码修改", user.getUserName()+",您好:</br> 请您<a href='"+bath+"user/updateUser/"+Base64.getBase64(String.valueOf(user.getUserId()))+".html'>修改密码</a>。", user.getUserMail());
			} catch (Exception e) {
				logger.error("send user Email failed",e);
				return new Result(false,"邮件发送失败,请联系邮箱管理员！");
			}
			return new Result(true,"请您查收邮件并重置密码！");
		}
	}
	
	/**
	 * 校验用户是否在别处登陆
	 * @param request
	 * @return Result
	 */
	@RequestMapping(value = "/checkuser", method = { RequestMethod.GET })
	@ResponseBody
	public Result checkuser(HttpServletRequest request){
		User user=(User) request.getSession().getAttribute("user");
		if(user!=null){
			//获取session中的userLoginStatus
			String old_userFlag=user.getUserLoginStatus();
			User info=userManager.detail(user.getUserId());
			//如果session中的userLoginStatus与数据库中的不一致则用户退出登陆
			if(info.getUserLoginStatus()!=null&&(!info.getUserLoginStatus().equals(old_userFlag))){
				return new Result(true, "该用户已在别处登录！");
			}else {
				return new Result(false,"");
			}
		}else{
			return new Result(false,"");
		}
	}
}
