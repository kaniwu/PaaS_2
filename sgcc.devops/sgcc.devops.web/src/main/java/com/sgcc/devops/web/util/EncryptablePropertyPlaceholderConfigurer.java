package com.sgcc.devops.web.util;

import java.util.Properties;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.web.image.Encrypt;
@Repository
public class EncryptablePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
	private static final String SECURE_PATH_KEY = JdbcConstant.SECURE_PATH_KEY;
	
    @Override
	protected void processProperties(ConfigurableListableBeanFactory beanFactory, Properties props)
        throws BeansException {
            try {
            	String securityPath = props.getProperty(SECURE_PATH_KEY);
            	
                String username = props.getProperty(JdbcConstant.JDBC_DATASOURCE_USERNAME_KEY);
                if (username != null) {
                    props.setProperty(JdbcConstant.JDBC_DATASOURCE_USERNAME_KEY, Encrypt.decrypt(username, securityPath));
                }
                
                String secure = props.getProperty(JdbcConstant.JDBC_DATASOURCE_PASSWORD_KEY);
                if (secure != null) {
                    props.setProperty(JdbcConstant.JDBC_DATASOURCE_PASSWORD_KEY, Encrypt.decrypt(secure, securityPath));
                }
                super.processProperties(beanFactory, props);
            } catch (Exception e) {
                e.printStackTrace();
                throw new BeanInitializationException(e.getMessage());
            }
        }
    }
