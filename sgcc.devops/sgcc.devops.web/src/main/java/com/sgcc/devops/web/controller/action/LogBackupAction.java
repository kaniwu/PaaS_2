package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sgcc.devops.dao.intf.LogBackupMapper;
import com.sgcc.devops.service.LogBackupService;

/**  
 * date：2016年2月4日 下午1:48:45
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogBackupAction.java
 * description：   日志备份操作访问控制类
 */
@Controller
@RequestMapping("logBackup")
public class LogBackupAction {

	@Resource
	private LogBackupService logBackupService;
	@Resource
	private LogBackupMapper LogBackupMapper;
}
