package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.model.ParameterModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ParameterService;
import com.sgcc.devops.web.manager.ParameterManager;
import com.sgcc.devops.web.query.QueryList;



@Controller
@RequestMapping("parameter")
public class ParameterAction {
	Logger logger = Logger.getLogger(ParameterAction.class);
	
	@Resource
	private QueryList queryList;
	@Resource
	private ParameterService parameterService;
	@Resource
	private ParameterManager parameterManager;
	
	/**
	 * 字典表列表分页查询
	 * @return
	 */
	@RequestMapping(value = "/searchlist", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean searchList(HttpServletRequest request,PagerModel pagerModel,ParameterModel parameterModel) {
		return queryList.queryParaList(pagerModel,parameterModel);
	}
	/**
	 * 根据类型查询参数
	* TODO
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年4月6日
	 */
	@RequestMapping(value = "/selectByType", method = { RequestMethod.GET })
	@ResponseBody
	public String selectByType(HttpServletRequest request, String type) {
		JSONArray ja = parameterService.selectByType(type);
		return ja.toString();
	}
	/**
	 * 根据类型查询参数
	* TODO
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年4月6日
	 */
	@RequestMapping(value = "/selectByLocation", method = { RequestMethod.GET })
	@ResponseBody
	public String selectByLocation(HttpServletRequest request,String parentid) {
		JSONArray ja = parameterService.selectByLocation(parentid);
		return ja.toString();
	}
	
	/**
	 * 删除字典记录
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/deleteParameter", method = RequestMethod.POST)
	@ResponseBody
	public Result deleteParameter(HttpServletRequest request, ParameterModel parameterModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(parameterModel);
		params.put("userId", user.getUserId());
		Result result = parameterManager.deleteParameter(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	
	/**
	 * 新增字典
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/createParameter", method = RequestMethod.POST)
	@ResponseBody
	public Result createParameter(HttpServletRequest request,  ParameterModel parameterModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(parameterModel);
		params.put("userId", user.getUserId());
		Result result = parameterManager.createParameter(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	
	/**
	 * 编辑字典
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/updateParameter", method = RequestMethod.POST)
	@ResponseBody
	public Result updateParameter(HttpServletRequest request,  ParameterModel parameterModel) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(parameterModel);
		params.put("userId", user.getUserId());
		Result result = parameterManager.updateParameter(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	
	/**
	 * 检查编码是否重复
	 * @author panjing
	 * @return
	 */
	@RequestMapping(value = "/checkParaCode", method = { RequestMethod.POST })
	@ResponseBody
	public String checkParaCode(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = parameterManager.checkParaCode(id, name);
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	/**
	 * 树节点所在位置层级是否还有子节点
	* @author mayh
	* @return String
	* @version 1.0
	* 2016年6月20日
	 */
	@RequestMapping(value = "/hasChild", method = { RequestMethod.GET })
	@ResponseBody
	public String hasChild(HttpServletRequest request,String treeId) {
		JSONObject result = new JSONObject();
		boolean ok = parameterManager.hasChild(treeId);
		result.put("hasChild", ok);
		return result.toString();
	}
	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public String selectParent(HttpServletRequest request, String type) {
		JSONArray ja = parameterService.selectParent(type);
		return ja.toString();
	}
}
