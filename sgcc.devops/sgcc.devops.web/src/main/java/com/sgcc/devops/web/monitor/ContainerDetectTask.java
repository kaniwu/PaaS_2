package com.sgcc.devops.web.monitor;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.core.detector.Detector;
import com.sgcc.devops.core.detector.DockerDetector;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 容器健康状态任务
 * @author dmw
 *
 */
public class ContainerDetectTask implements Runnable {

	private TaskCache cache;
	private List<Container> containers;

	public ContainerDetectTask() {
		super();
	}

	public ContainerDetectTask(TaskCache cache, List<Container> containers) {
		super();
		this.cache = cache;
		this.containers = containers;
	}

	@Override
	public void run() {
		if (null == containers || containers.isEmpty()) {
			return;
		}
		ExecutorService executor = Executors.newCachedThreadPool();
		CompletionService<Result> detectService = new ExecutorCompletionService<>(executor);
		final HttpClient httpClient = new HttpClient(5000);
		for (final Container container : containers) {
			if (Status.CONTAINER.EXIT.ordinal() == container.getConStatus().intValue()
					|| Status.CONTAINER.DELETE.ordinal() == container.getConStatus().intValue()) {
				continue;
			}
			// 探测容器状态的任务改成多线程进行
			Callable<Result> task = new Callable<Result>() {
				@Override
				public Result call() throws Exception {
					Detector detector = new DockerDetector(container.getClusterIp(), container.getClusterPort(),
							container.getConUuid(), httpClient,3);
					if (detector.normal()) {
						return new Result(true, "success");
					}
					Integer times = cache.getContainerCache().get(container.getConId());
					if (null == times) {
						cache.getContainerCache().put(container.getConId(), 1);
					} else {
						cache.getContainerCache().put(container.getConId(), times + 1);
					}
					return new Result(true, "success");
				}
			};
			detectService.submit(task);
		}
		executor.shutdown();
	}

}
