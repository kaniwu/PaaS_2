package com.sgcc.devops.web.daemon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.core.detector.Detector;
import com.sgcc.devops.core.detector.DockerDaemonDetector;
import com.sgcc.devops.core.detector.SwarmDetector;
import com.sgcc.devops.core.intf.HostCore;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.intf.HostMapper;
import com.sgcc.devops.service.ClusterService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.image.Encrypt;

/**
 * 集群守护线程，每隔5秒钟探测一次swarm是否正常， 在swarm所在主机不可达时每隔30秒重试一次。
 * 当swarm不可达的次数超过3次时，执行swarm重启操作
 * 
 * @author dmw
 *
 */
public class SwarmDaemon extends Thread {

	private static Logger logger = Logger.getLogger(SwarmDaemon.class);
	private static HttpClient client = new HttpClient(5000);
	private static int failedNum = 0;// 获取swarm失败的次数
	private static int localDaemonFaild = 0;// swarm本机守护次数
	private boolean stop = false;

	private String ip;// 集群IP
	private String port;// 集群端口
	private String config;// 配置文件全路径
	private String username;// 主机用户名
	private String password;// 主机密码
	private String hostport;//主机SSH登录端口
	private Detector dockerDetector;
	private Detector detector;// 集群健康探测器

	private String clusterId;
	private String keyStore;
	private HostMapper hostMapper;
	private HostService hostService;
	private HostCore hostCore;
	private ClusterService clusterService;
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getConfig() {
		return config;
	}

	public void setConfig(String config) {
		this.config = config;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getHostport() {
		return hostport;
	}

	public void setHostport(String hostport) {
		this.hostport = hostport;
	}

	private void init() {
		detector = new SwarmDetector(ip, port, client);
	}

	public void stopDeamon() {
		this.stop = true;
	}

	private String startCommand(String ip, String port, String config) {
		return "nohup swarm manage -H tcp://" + ip + ":" + port + " file://" + config + ">>" + config + ".log 2>&1&";
	}

	public SwarmDaemon(String ip, String port, String config, String username, String password, String hostport,String clusterId,
			String keyStore, HostMapper hostMapper, HostService hostService, HostCore hostCore,
			ClusterService clusterService) {
		super();
		this.ip = ip;
		this.port = port;
		this.config = config;
		this.username = username;
		this.password = password;
		this.hostport = hostport;
		this.clusterId = clusterId;
		this.keyStore = keyStore;
		this.hostMapper = hostMapper;
		this.hostService = hostService;
		this.hostCore = hostCore;
		this.clusterService = clusterService;
	}

	private Result failover() {
		Cluster cluster = clusterService.getCluster(clusterId);
		if (null == cluster) {// 集群不存在直接返回
			logger.error("The Cluster has been remove by other person!");
			this.stop = true;
			return new Result(false, "Cluster is not exist!");
		}
		if (detector.normal()) {
			new Result(true, "Cluster【"+cluster.getClusterName()+"】 restart success!");
		}
		// 获取集群下的所有节点
		List<Host> slavers = hostService.listHostByClusterId(clusterId);
		if (null == slavers) {
			slavers = new ArrayList<Host>();
		}
		Iterator<Host> slaverIterator = slavers.iterator();
		Host host = null;
		Host masterHost = hostService.getHost(cluster.getMasteHostId());
		String masterHostId = null == masterHost ? "" : masterHost.getHostId();
		// 探测集群下所有节点的Docker Daemon情况，剔除异常Docker宿主机和管理节点主机，
		while (slaverIterator.hasNext()) {
			host = slaverIterator.next();
			if (host.getHostId().equals(masterHostId)) {
				slaverIterator.remove();
				continue;
			}
			dockerDetector = new DockerDaemonDetector(host.getHostIp(), "2375");
			if (dockerDetector.normal()) {
				continue;
			} else {
				slaverIterator.remove();
				host.setDockerStatus((byte) Status.DOCKER.ABNORMAL.ordinal());
				hostService.update(host);
			}
		}
		StringBuffer clusterConfig = new StringBuffer();
		// 生成集群配置信息，消除最后一个换行\n
		for (Host slaver : slavers) {
			clusterConfig.append(slaver.getHostIp()).append(":2375").append("\n");
		}
		if (StringUtils.hasText(clusterConfig.toString())) {
			clusterConfig = clusterConfig.delete(clusterConfig.length() - 1, clusterConfig.length());
		}
		// 选定一个新的集群管理节点主机
		Host newHost = null;
		List<Host> freeHosts = hostMapper.freeHostList(new Host());
		if (null == freeHosts || freeHosts.isEmpty()) {
			if (null == masterHost) {// 原有集群的管理主机不存在同时没有其他的可用空闲主机，直接返回
				
				//失败状态设置为异常
				cluster.setClusterStatus((byte)Status.CLUSTER.ABNORMAL.ordinal());
				clusterService.update(cluster);
				logger.error("Master Host is Null And No Free Host to Failover");
				this.stop = true;
				return new Result(false, "Master Host is Null And No Free Host to Failover");
			} else {// 无可用空闲主机，选择本机再次尝试。
				newHost = masterHost;
			}
		} else {// 有可用空闲主机，选择第一台作为新的管理节点主机
			newHost = freeHosts.get(0);
			masterHost.setClusterId(null);
			masterHost.setHostType((byte)Type.HOST.UNUSED.ordinal());
			hostService.update(masterHost);
		}
		// 在新的管理节点主机上更新swarm集群配置
		String newPassword = Encrypt.decrypt(newHost.getHostPwd(), keyStore);
		boolean success = this.hostCore.updateCluster(newHost.getHostIp(), newHost.getHostUser(), newPassword,newHost.getHostPort(),
				clusterConfig.toString(), cluster.getManagePath());
		if (success) {
			//设置为集群主机
			newHost.setClusterId(clusterId);
			newHost.setHostType((byte) Type.HOST.SWARM.ordinal());
			hostService.update(newHost);
			cluster.setMasteHostId(newHost.getHostId());
			cluster.setClusterStatus((byte)Status.CLUSTER.NORMAL.ordinal());
			clusterService.update(cluster);
			logger.warn("Failover success!!");
			this.ip = newHost.getHostIp();
			this.username = newHost.getHostUser();
			this.password = newPassword;
			detector = new SwarmDetector(newHost.getHostIp(), cluster.getClusterPort(), client);
			logger.warn("Cluster ["+ip+"]Failover success!! new Master Host Ip is :" + newHost.getHostIp());
			return new Result(true, "Failover success!! new Master Host Ip is :" + newHost.getHostIp());
		} else {
			//失败状态设置为异常
			cluster.setClusterStatus((byte)Status.CLUSTER.ABNORMAL.ordinal());
			clusterService.update(cluster);
			this.stop = true;
			logger.error("Cluster ["+ip+"]Failover Failed!!!Migrate Cluster to newHost failed");
			return new Result(false, "Failover Failed");
		}
	}

	private void startSwarm(SSH ssh) {
		try {
			ssh.execute(startCommand(ip, port, config), 1000 * 5);
		} catch (Exception e) {
			logger.error("Restart swarm exception ：", e);
		} finally {
			ssh.close();
		}
	}

	@Override
	public void run() {
		init();
		while (!stop) {
			try {
				Thread.sleep(1000 * 5);
			} catch (InterruptedException e) {
				logger.error("Swarm【" + this.ip + "】 daemon is　intercepted");
			}
			if (localDaemonFaild >= 3) {
				logger.error("The Swarm【" + this.ip + "】 has self daemon failed over 3 times!!!FailOver it to Other host");
				Result result = this.failover();
				if (result.isSuccess()) {
					localDaemonFaild = 0;
					failedNum = 0;
					continue;
				}
			}
			if (detector.normal()) {
				Cluster cluster = clusterService.getCluster(clusterId);
				if((byte) Status.CLUSTER.ABNORMAL.ordinal()==cluster.getClusterStatus()){
					cluster.setClusterStatus((byte)Status.CLUSTER.NORMAL.ordinal());
					clusterService.update(cluster);
				}
				continue;
			} else {
				failedNum++;
				logger.error("Swarm is unreachable 【" + failedNum + "】 times");
			}
			if (failedNum < 3) {
				continue;
			}
			logger.warn("Swarm is unreachable over 3 times !!!!! restart it !");
			SSH ssh = new SSH(ip, username, password);
			if (ssh.connect()) {
				startSwarm(ssh);
			}
			localDaemonFaild++;
			failedNum = 0;
		}
	}

	@Override
	public String toString() {
		return "SwarmDaemon [stop=" + stop + ", ip=" + ip + ", port=" + port + ", config=" + config + ", username="
				+ username + ", password=" + password + ", detector=" + detector + "]";
	}

}
