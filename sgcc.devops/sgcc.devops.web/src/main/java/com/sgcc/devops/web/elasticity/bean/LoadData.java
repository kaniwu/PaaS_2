package com.sgcc.devops.web.elasticity.bean;

/**
 * 负载数据类
 * 
 * @author dmw
 *
 */
public class LoadData {

	private double cpu;// cpu使用率
	private double mem;// 内存使用量
	private double net;// 网卡入口速率
	private int link;// 端口连接数

	public double getCpu() {
		return cpu;
	}

	public void setCpu(double cpu) {
		this.cpu = cpu;
	}

	public double getMem() {
		return mem;
	}

	public void setMem(double mem) {
		this.mem = mem;
	}

	public double getNet() {
		return net;
	}

	public void setNet(double net) {
		this.net = net;
	}

	public int getLink() {
		return link;
	}

	public void setLink(int link) {
		this.link = link;
	}

	public LoadData(double cpu, double mem, double net, int link) {
		super();
		this.cpu = cpu;
		this.mem = mem;
		this.net = net;
		this.link = link;
	}

	public LoadData() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "LoadData [cpu=" + cpu + ", mem=" + mem + ", net=" + net + ", link=" + link + "]";
	}

}
