package com.sgcc.devops.web.controller.action;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.ClusterModel;
import com.sgcc.devops.common.model.HostModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ClusterWithIPAndUser;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.ClusterManager;
import com.sgcc.devops.web.manager.HostManager;
import com.sgcc.devops.web.query.QueryList;
/**  
 * date：2016年2月4日 下午1:46:38
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ClusterAction.java
 * description：  集群操作访问控制类
 */
@Controller
@RequestMapping("cluster")
public class ClusterAction {

	@Resource
	private QueryList queryList;
	@Resource
	private ClusterManager clusterManager;
	@Resource
	private HostManager hostManager;

	private static Logger logger = Logger.getLogger(ClusterAction.class);
	
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request, ClusterModel clusterModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(clusterModel);
		params.put("clusterName", clusterModel.getClusterName());
		params.put("clusterPort", clusterModel.getClusterPort());
		params.put("masterHost", clusterModel.getMasteHostId());
		params.put("standByHostId", clusterModel.getStandByHostId());
		params.put("clusterDesc", clusterModel.getClusterDesc());
		params.put("hostDocker", clusterModel.getHostDocker());
		params.put("registryLbId", clusterModel.getRegistryLbId());
		params.put("swarmIns", clusterModel.getSwarmIns());
		params.put("clusterEnv", clusterModel.getClusterEnv());
		params.put("userId", user.getUserId());
		params.put("dockerParam", clusterModel.getDockerParam());
		Result result = clusterManager.createCluster(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	public Result delete(HttpServletRequest request, ClusterModel clusterModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(clusterModel);
		params.put("clusterId", clusterModel.getClusterId());
		params.put("userId", user.getUserId());
		Result result = clusterManager.deleteCluster(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/deletes", method = { RequestMethod.POST })
	@ResponseBody
	public void deletes(HttpServletRequest request, @RequestParam(value = "ids", required = true) String ids) {

		if (ids != null) {
			User user = (User) request.getSession().getAttribute("user");
			JSONObject params = new JSONObject();
			params.put("array", ids);
			params.put("userId", user.getUserId());
			clusterManager.deleteCluster(params);
		}

	}

	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request, ClusterModel clusterModel) {

		User user = (User) request.getSession().getAttribute("user");
		Result result = clusterManager.updateCluster(user.getUserId(), clusterModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;

	}

	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean clusterList(HttpServletRequest request,PagerModel pagerModel, ClusterWithIPAndUser ClusterWithIPAndUser) {

		User user = (User) request.getSession().getAttribute("user");
		try {
			if(null!=ClusterWithIPAndUser&&null!=ClusterWithIPAndUser.getClusterName()){
				String clusterName = URLDecoder.decode(ClusterWithIPAndUser.getClusterName(), "UTF-8");
				ClusterWithIPAndUser.setClusterName(clusterName);
			}
			
		} catch (UnsupportedEncodingException e) {
			logger.error("clusterName encode error: "+e.getMessage());
		}
		return queryList.queryClusterList(user, pagerModel, ClusterWithIPAndUser);

	}

//	@RequestMapping(value = "/clusterMasterList", method = { RequestMethod.GET })
//	@ResponseBody
//	public String clusterMasterList(HttpServletRequest request, ClusterModel clusterModel) {
//
//		JSONArray ja = queryList.queryClusterMasterList();
//		return ja.toString();
//
//	}

	@RequestMapping(value = "/all", method = { RequestMethod.GET })
	@ResponseBody
	public String allList(HttpServletRequest request, ClusterWithIPAndUser clusterWithIPAndUser) {

		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = queryList.queryAllClusterList(user, clusterWithIPAndUser);
		return ja.toString();
	}

	@RequestMapping(value = "/cluster", method = { RequestMethod.GET })
	@ResponseBody
	public Cluster cluster(HttpServletRequest request, HostModel hostModel) {

		Host host = new Host();
		host.setHostId(hostModel.getHostId());
		Cluster cluster = queryList.queryCluster(host);
		if (cluster != null) {
			return cluster;
		} else {
			return null;
		}
	}

	@RequestMapping(value = "/addHost", method = { RequestMethod.GET })
	@ResponseBody
	public Result addHost(HttpServletRequest request, @RequestParam String[] hostIds,String dockerParam) {

		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.putToCluster(hostIds,dockerParam, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/removeHost", method = { RequestMethod.GET })
	@ResponseBody
	public Result removeHost(HttpServletRequest request, @RequestParam String[] hostIds) {

		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.clusterRemoveHost(hostIds, user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/healthCheck", method = { RequestMethod.GET })
	@ResponseBody
	public Result healthCheck(HttpServletRequest request, int clusterId) {

		User user = (User) request.getSession().getAttribute("user");
		JSONArray ja = new JSONArray();
		JSONObject params = new JSONObject();
		params.put("clusterId", clusterId);
		params.put("userId", user.getUserId());
		ja.add(0, params);
		Result result = clusterManager.clusterHealthCheck(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	@RequestMapping(value = "/dockerList", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean dockerList(HttpServletRequest request, @RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, String clusterId) {

		User user = (User) request.getSession().getAttribute("user");
		return clusterManager.dockerList(user.getUserId(), page, rows, clusterId);
	}

	/**
	 * @author zhangxin
	 * @time 2015年9月23日
	 * @description
	 */
	@RequestMapping("/clusterHost")
	public String clusterHost(Model model, Cluster cluster) {

		model.addAttribute("clusterId", cluster.getClusterId());
		cluster = clusterManager.getCluster(cluster);
		model.addAttribute("clusterName", cluster.getClusterName());
		return "cluster/docker";
	}

	/**
	 * @author zs
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = clusterManager.checkName(id, name);
		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	
	/**
	 * @author weiwei
	 * @time 2015年12月16
	 * @description 集群隔离
	 */
	@RequestMapping(value = "/isolate", method = { RequestMethod.POST })
	@ResponseBody
	public Result isolate(HttpServletRequest request,@RequestParam(value = "clusterId", required = true) String clusterId) {
		Result result = clusterManager.isolateCluster(clusterId);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	 * @author weiwei
	 * @time 2015年12月16
	 * @description 集群恢复
	 */
	@RequestMapping(value = "/recovery", method = { RequestMethod.POST })
	@ResponseBody
	public Result recovery(HttpServletRequest request,@RequestParam(value = "clusterId", required = true) String clusterId) {
		Result result = clusterManager.recoveryCluster(clusterId);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/**
	 * @author weiwei
	 * @time 2015年12月16
	 * @description 集群迁移
	 */
	@RequestMapping(value = "/migrate", method = { RequestMethod.POST })
	@ResponseBody
	public Result migrate(HttpServletRequest request,@RequestParam(value = "clusterId", required = true) String clusterId,
				@RequestParam(value = "hostId", required = true) String hostId) {
		Result result = clusterManager.migrateCluster(clusterId, hostId);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	/**
	* swarm服务安装、启动、停止
	* @author mayh
	* @return Result
	* @version 1.0
	* 2016年2月23日
	 */
	@RequestMapping(value = "/swarmDaemon", method = { RequestMethod.GET })
	@ResponseBody
	public Result swarmDaemon(HttpServletRequest request,
			@RequestParam(value = "clusterId", required = true) String clusterId,
			@RequestParam(value = "opration", required = true) String opration) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = hostManager.swarmDaemon(clusterId,opration,user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
}
