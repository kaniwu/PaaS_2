package com.sgcc.devops.web.image.builder;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.util.SSH;
import com.sgcc.devops.web.image.ConfigFile;
import com.sgcc.devops.web.image.DataSource;
import com.sgcc.devops.web.image.Encrypt;
import com.sgcc.devops.web.image.ImageBuilder;
import com.sgcc.devops.web.image.WeblogicContstants;
import com.sgcc.devops.web.image.material.ConfigFileMaterial;
import com.sgcc.devops.web.image.material.DockerFileMaterial;
import com.sgcc.devops.web.image.material.ImageMaterial;
import com.sgcc.devops.web.image.material.JdbcFileMaterial;
import com.sgcc.devops.web.task.TaskCache;
import com.sgcc.devops.web.task.TaskMessage;

/**
 * java应用镜像生成器 j2se 
 * 
 * @author lyd
 *
 */
@Repository("javaImageBuilder")
public class JavaImageBuilder extends Builder implements ImageBuilder {

	private static Logger logger = Logger.getLogger(JavaImageBuilder.class);
	@Autowired
	private TmpHostConfig tmpHostConfig;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private DockerfileBuilder dockerfileBuilder;
	@Autowired
	private TaskCache taskCache;
	@Override
	public Result build(ImageMaterial material) {
		logger.info("开始制作java应用镜像");
		if (null == material) {
			logger.error("镜像制作材料缺失！！！");
			return new Result(false, "镜像制作材料缺失！！！");
		}
		StringBuffer taskMessage = material.getTaskMessage();
		//转存主机
		String ip = tmpHostConfig.getTmpHostIp();
		String user = tmpHostConfig.getTmpHostUser();
		String pwd = Encrypt.decrypt(tmpHostConfig.getTmpHostPwd(), localConfig.getSecurityPath());
		SSH ssh = new SSH(ip,user,pwd);
		//新上传配置文件地址
		String localPath = localConfig.getLocalDeployPath()+material.getSystemId()+"/"+ material.getTimestamp() + "/";
		
		String remoteDir = material.getPath();
		//是否需要中转
		if(material.isTransferNeeded()){
			if (ssh.connect()) {
				StringBuffer mkdirCommand = new StringBuffer("mkdir -p "+remoteDir+"; ");
				try {
					String result = ssh.executeWithResult(mkdirCommand.toString());
					ssh.close();
					if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
						logger.error("文件中转时创建目录失败！"+result);
						return new Result(false, "文件中转时创建目录失败！"+result);
					}
				}catch (Exception e) {
					e.printStackTrace();
					logger.error("文件中转时创建目录失败！"+e);
					return new Result(false, "文件中转时创建目录失败！"+e);
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
			if (ssh.connect()) {
				boolean pushSuccess = ssh.SCPDirectory(localPath, remoteDir);
				ssh.close();
				if (!pushSuccess) {// 将dockerfile等文件成功上传到仓库所在的主机上
					logger.error("转储配置文件失败！");
					return new Result(false, "转储配置文件失败！");
				}
			} else {
				logger.error("转储配置文件时仓库主机连接失败");
				return new Result(false, "转储配置文件时仓库主机连接失败");
			}
		}else{
			remoteDir=localPath;
		}
		Map<String, String> configFileMap = new HashMap<String, String>();// 存储驱动文件路径和目标路径
		for (ConfigFile configFile : material.getConfigFiles()) {
			configFileMap.put(configFile.getFileName(), configFile.getFilePath());
		}
		// 生成dockerfile文件
		taskMessage.append("<br/>");
		taskMessage.append("生成Dockerfile...");
		TaskMessage Dockerfile = new TaskMessage(taskMessage.toString(), 25, material.getUserId(),material.getSystemId());
		taskCache.updateTask(Dockerfile);
		
		String exposePorts = "";
		if (StringUtils.hasLength(material.getBaseImage().getImagePort())) {
//			exposePorts = "EXPOSE " + material.getBaseImage().getImagePort();
		}
		String baseImage = material.getBaseImage().getImageName() + ":" + material.getBaseImage().getImageTag();
		DockerFileMaterial dockerFileMaterial = new DockerFileMaterial(material.getAppTag(), baseImage, "beyondcent",
				material.getAppFileName(),"/", null, exposePorts,configFileMap);
		dockerFileMaterial.setDirectory(localPath);
		dockerFileMaterial.setRunCommands(material.getRunCommands());
		dockerFileMaterial.setDockerfilePart(material.getDockerfile());
		dockerfileBuilder.init(dockerFileMaterial, localConfig);
		File dockerFile = dockerfileBuilder.build();
		if (null == dockerFile) {
			logger.error("制作Dockerfile文件失败！");
			return new Result(false, "制作Dockerfile文件失败！");
		}
		//是否需要中转
		if(material.isTransferNeeded()){
			if (ssh.connect()) {
				boolean pushSuccess = ssh.SCPDirectory(localPath + "/", remoteDir);
//						deleteFile(localPath);
				ssh.close();
				if (!pushSuccess) {// 将dockerfile等文件成功上传到仓库所在的主机上
					logger.error("转储配置文件失败！");
					return new Result(false, "转储配置文件失败！");
				}
			} else {
				logger.error("转储配置文件时仓库主机连接失败");
				return new Result(false, "转储配置文件时仓库主机连接失败");
			}
		}
		
		// 制作镜像
		String tempImageName = material.getImageName();
		String buildCommand = "sudo docker build -t " + tempImageName + " " + material.getPath();
		taskMessage.append("<br/>");
		taskMessage.append("执行制作镜像脚本："+buildCommand);
		TaskMessage dockerBuild = new TaskMessage(taskMessage.toString(), 30, material.getUserId(),material.getSystemId());
		taskCache.updateTask(dockerBuild);
		String uuid = null;
		try {
			if(ssh.connect()){
				String result = ssh.executeWithResult(buildCommand);
				logger.info("Make image result:" + result);
				Result makeResult = parseMakeResult(result);
				if (!makeResult.isSuccess()) {
					logger.error("镜像制作失败:"+makeResult.getMessage()+";result="+result);
					return new Result(false, "制作镜像失败：脚本执行异常:"+result);
				} else {
					logger.info("镜像制作成功！");
					uuid = makeResult.getMessage();
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
		} catch (Exception e) {
			logger.info("Make image fail: "+e);
			return new Result(false, "制作镜像失败：脚本执行异常！"+e);
		} finally {
			ssh.close();
		}
		// 上传镜像
		String pushCommand = "sudo docker push " + tempImageName;
		taskMessage.append("<br/>");
		taskMessage.append("执行上传镜像到仓库脚本："+pushCommand);
		TaskMessage pushTask = new TaskMessage(taskMessage.toString(), 40, material.getUserId(),material.getSystemId());
		taskCache.updateTask(pushTask);
		
		material.setTaskMessage(taskMessage);
		
		ssh.connect();
		try {
			if(ssh.connect()){
				String result = ssh.executeWithResult(pushCommand);
				logger.info("Push image result:" + result);
				ssh.close();
				ssh.connect();
				ssh.execute("docker rmi " + uuid);
				if (result.contains("error") || result.contains("failed") || result.contains("EOF")) {
					logger.error("镜像推送脚本执行失败！"+result);
					return new Result(false, "镜像发布脚本执行失败！"+result);
				} else {
					// logger.info("镜像推送成功！");
					return new Result(true, uuid);
				}
			}else{
				logger.error("转存文件主机连接失败！！");
				return new Result(false, "转存文件主机连接失败！");
			}
			
		} catch (Exception e) {
			logger.error("Push image fail" + e.getMessage());
			return new Result(false, "发布镜像失败：发布镜像脚本执行异常！");
		} finally {
			ssh.close();
		}
	}
}
