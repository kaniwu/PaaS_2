package com.sgcc.devops.web.image;

/**
 * Nginx配置项目类
 * @author dmw
 *
 */
public class SystemServer {

	private String ip;
	private String port;

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public SystemServer(String ip, String port) {
		super();
		this.ip = ip;
		this.port = port;
	}

	public SystemServer() {
		super();
		// TODO Auto-generated constructor stub
	}

}
