package com.sgcc.devops.web.elasticity.cache;

import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * 弹性伸缩任务处理队列
 * 
 * @author dmw
 *
 */
@Component("elasticCache")
public class ElasticCache {

	private static Logger logger = Logger.getLogger(ElasticCache.class);
	private ConcurrentHashMap<String, String> taskCache = null;

	/**
	 * 对象初始化
	 */
	@PostConstruct
	public void init() {
		if (null == taskCache) {
			taskCache = new ConcurrentHashMap<>(10, 0.9f);
		}
	}

	/**
	 * 队列销毁
	 */
	@PreDestroy
	public void destroy() {
		if (null != taskCache) {
			taskCache.clear();
		}
	}

	/**
	 * 物理系统的弹性伸缩任务是否存在
	 * 
	 * @param systemId
	 *            物理系统主键
	 * @return
	 */
	public boolean taskExist(String systemId) {
		synchronized (this) {
			return taskCache.containsKey(systemId);
		}
	}

	/**
	 * 增加弹性伸缩任务
	 * 
	 * @param systemId
	 *            物理系统主键
	 * @return
	 */
	public boolean addTask(String systemId) {
		synchronized (this) {
			if (null == taskCache) {
				taskCache = new ConcurrentHashMap<>(10, 0.9f);
				return false;
			}
			if (this.taskExist(systemId)) {
				logger.warn("The system 【" + systemId + "】 task is already in cache!");
				return false;
			}
			taskCache.put(systemId, systemId);
			return true;
		}
	}

	/**
	 * 移除弹性伸缩任务
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean removeTask(String systemId) {
		synchronized (this) {
			if (null == taskCache) {
				taskCache = new ConcurrentHashMap<>(10, 0.9f);
				return false;
			}
			if (taskCache.containsKey(systemId)) {
				taskCache.remove(systemId);
			} else {
				logger.warn("The system 【" + systemId + "】 task is not in cache!");
			}
			return true;
		}
	}

}
