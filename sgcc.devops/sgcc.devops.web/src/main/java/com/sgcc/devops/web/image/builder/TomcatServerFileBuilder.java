package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.material.TomcatServerMaterial;

/**
 * tomcat的server.xml文件生成器
 * 
 * @author dmw
 *
 */
@Repository("tomcatServerFileBuilder")
public class TomcatServerFileBuilder implements FileBuilder {

	private static Logger logger = Logger.getLogger(TomcatServerFileBuilder.class);
	private static String FILE_NAME = "server.vm";

	private TomcatServerMaterial material;

	private LocalConfig localConfig;

	private enum ParamPosition {
		JDBC_CONFIG;
	}

	public void init(TomcatServerMaterial material, LocalConfig localConfig) {
		this.material = material;
		this.localConfig = localConfig;
	}

	public TomcatServerMaterial getMaterial() {
		return material;
	}

	public void setMaterial(TomcatServerMaterial material) {
		this.material = material;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}

	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	@Override
	public File build() {
		if (null == material || material.getDataSources().isEmpty()) {
			logger.error("JDBC material is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, localConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();
		// 组合配置信息
		context.put(ParamPosition.JDBC_CONFIG.name(), material.getDataSources());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get server template infos error: " + e.getMessage());
			return null;
		}
		// 生成新的配置文件
		String localPath = material.getDirectory();
		localPath = FileUploadUtil.check(localPath);
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = localPath.replace("\\", "/");
		File file = new File(localPath + "/server.xml");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build server.xml exception: " + e.getMessage());
				return null;
			}
		}
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			writer.close();
			return file;
		} catch (IOException e) {
			logger.error("fild file exception: "+e.getMessage());
			return null;
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
