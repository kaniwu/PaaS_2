package com.sgcc.devops.web.controller.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.LocationModel;
import com.sgcc.devops.web.manager.LocationManager;


@Controller
@RequestMapping("location")
public class LocationAction {
	@Resource
	private LocationManager locationManager;

	/**
	 * 获取所有机房机架并拼装成json
	 * @return
	 */
	@RequestMapping(value = "/tree", method = { RequestMethod.GET })
	@ResponseBody
	public String treeList(HttpServletRequest request) {
		JSONObject result = new JSONObject();
		result.put("success", true);
		JSONArray data = locationManager.treeList();
		result.put("data", data);
		return result.toString();
	}
	
	
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result create(HttpServletRequest request,LocationModel locationModel) {
		Result result = locationManager.create(locationModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/update", method = { RequestMethod.POST })
	@ResponseBody
	public Result update(HttpServletRequest request,LocationModel locationModel) {
		Result result = locationManager.update(locationModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/delete", method = { RequestMethod.POST })
	@ResponseBody
	public Result delete(HttpServletRequest request,LocationModel locationModel) {
		Result result = locationManager.delete(locationModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/select", method = { RequestMethod.GET })
	@ResponseBody
	public String select(HttpServletRequest request,LocationModel locationModel) {
		JSONObject jo = locationManager.select(locationModel);
		return jo.toString();
	}
}
