package com.sgcc.devops.web.image;

/**
 * 
 * Tomcat常用路径
 * 
 * @author dmw
 *
 */
public class TomcatConstants {

	/**
	 * 基本路径
	 */
	public static final String BASE_PATH = "/usr/local/tomcat/";

	/**
	 * war包存放路径
	 */
	public static final String APP_PATH = BASE_PATH + "webapps/";

	/**
	 * 配置文件路径
	 */
	public static final String CONTEXT_PAHT = BASE_PATH + "conf/";

	/**
	 * LIB包存放路径
	 */
	public static final String LIB_PAHT = BASE_PATH + "lib/";

	/**
	 * 启动命令
	 */
	public static final String START_CMD = "sh " + BASE_PATH + "bin/startup.sh";
	/**
	 * 日志文件存放位置位置
	 */
	public static final String LOGS_PATH = BASE_PATH + "logs/";
	
	public static final String BIN_PATH = BASE_PATH + "bin/";
}
