package com.sgcc.devops.web.controller.action;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.model.ContainerModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Host;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.service.HostService;
import com.sgcc.devops.web.manager.ContainerManager;
import com.sgcc.devops.web.manager.LogManager;
import com.sgcc.devops.web.query.QueryList;

/**  
 * date：2016年2月4日 下午1:47:10
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ContainerAction.java
 * description：  容器操作访问控制类
 */
@RequestMapping("/container")
@Controller
public class ContainerAction {

	@Autowired
	private QueryList query;
	@Autowired
	private ContainerManager containerManager;
	@Autowired
	private LogManager logManager;
	@Autowired
	private LocalConfig localConfig;
	@Autowired
	private ContainerService containerService;
	@Autowired
	private HostService hostService;
	/**
	 * @author langzi
	 * @param requst
	 * @return
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/list", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean listContainerByPage(HttpServletRequest request,PagerModel pagerModel, Container model) {
		User user = (User) request.getSession().getAttribute("user");
		return query.listOnePageContainer(user,pagerModel, model);
	}

	/**
	 * @author langzi
	 * @param request
	 * @return
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/listAll", method = { RequestMethod.GET })
	@ResponseBody
	public String listContainers(HttpServletRequest request) {
		return query.listContainers().toString();
	}

	/**
	 * @author langzi
	 * @param request
	 * @param conId
	 * @return
	 * @version 1.0 2015年9月16日
	 */
	@RequestMapping(value = "/listPort", method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray listConports(HttpServletRequest request, String conId) {
		return query.listConPortByConId(conId);
	}

	/**
	 * @author langzi
	 * @param request
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/create", method = { RequestMethod.POST })
	@ResponseBody
	public Result createContainer(HttpServletRequest request, ContainerModel model) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("model", JSONUtil.parseObjectToJsonObject(model));
		params.put("userId", user.getUserId());
		Result result = containerManager.addContainer(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;

	}

	@RequestMapping(value = "/start", method = { RequestMethod.GET })
	@ResponseBody
	public Result startContainer(HttpServletRequest request, @RequestParam String[] containerids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("containerId", containerids);
		params.put("userId", user.getUserId());
		Result result = containerManager.startContainer(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	/**
	 * @author langzi
	 * @param request
	 * @param containerId
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/stop", method = { RequestMethod.GET })
	@ResponseBody
	public Result stopContainer(HttpServletRequest request, @RequestParam String[] containerids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("containerId", containerids);
		params.put("userId", user.getUserId());
		Result result = containerManager.stopContainer(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	/**
	 * @author langzi
	 * @param request
	 * @param containerIds
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/trash", method = { RequestMethod.GET })
	@ResponseBody
	public Result trashContainer(HttpServletRequest request, @RequestParam String[] containerids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("containerId", containerids);
		params.put("userId", user.getUserId());
		Result result = containerManager.removeContainer(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	/**
	 * @author langzi
	 * @param request
	 * @param containerIds
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/sync", method = { RequestMethod.GET })
	@ResponseBody
	public void syncContainer(HttpServletRequest request) {
		/*
		 * User user = (User)request.getSession().getAttribute("user");
		 * JSONObject jo = new JSONObject(); jo.put("handler",
		 * "syncContainerHandler"); JSONArray ja = new JSONArray(); JSONObject
		 * params = new JSONObject(); params.put("userId", user.getUserId());
		 * ja.add(0, params); jo.put("Params", ja); dispatch.distributor(jo);
		 */
	}

	/**
	 * @author zhangxin
	 * @param request
	 * @param containerIds
	 * @version 1.0 2015年8月20日
	 */
	@RequestMapping(value = "/monitor", method = { RequestMethod.GET })
	@ResponseBody
	public JSONArray monitorContainer(HttpServletRequest request, @RequestParam String[] containerId) {
		// User user = (User)request.getSession().getAttribute("user");
		// JSONObject params = new JSONObject();
		// params.put("containerId", containerids);
		// params.put("userId", user.getUserId());
		return containerManager.monitorContainer(containerId);
	}

	@RequestMapping(value = "/listByimageId", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean listByimageId(HttpServletRequest request, @RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, String imageId) {
		User user = (User) request.getSession().getAttribute("user");
		return query.conlistByImage(user.getUserId(), page, rows, imageId);
	}

	/**
	 * TODO
	 * @author mayh
	 * @return String
	 * @version 1.0 2015年10月22日
	 */
	@RequestMapping(value = "/listContainersByHostId", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean listContainersByHostId(HttpServletRequest request, @RequestParam String hostId) {
		return query.listContainersByHostId(hostId);
	}

	/**
	 * 批量停止、删除容器 TODO
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/stopAndRemoveCon", method = { RequestMethod.GET })
	@ResponseBody
	public Result stopAndRemoveCon(HttpServletRequest request, @RequestParam String systemId,
			@RequestParam String[] containerids) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("systemId", systemId);
		params.put("containerId", containerids);
		params.put("userId", user.getUserId());
		Result result = containerManager.stopAndRemoveCon(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	/**
	 * 依赖镜像批量启动容器 TODO
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/createConBySystemId", method = { RequestMethod.GET })
	@ResponseBody
	public Result createConBySystemId(HttpServletRequest request, @RequestParam int systemId, int conCount) {
		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = new JSONObject();
		params.put("systemId", systemId);
		params.put("conCount", conCount);
		params.put("userId", user.getUserId());
		Result result = containerManager.createConBySysId(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}

	/**
	 * @author zs
	 * @time 2015年11月26
	 * @description
	 */
	@RequestMapping(value = "/checkName", method = { RequestMethod.POST })
	@ResponseBody
	public String checkName(HttpServletRequest request, @RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "name", required = false) String name) {
		JSONObject resultObject = new JSONObject();
		// 调用接口
		boolean isOk = true;
		isOk = containerManager.checkName(id, name);

		// 调用接口
		resultObject.put("success", isOk);
		return resultObject.toString();
	}
	
	/**
	 * 清除主机容器
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/clear", method = { RequestMethod.POST })
	@ResponseBody
	public Result clear(HttpServletRequest request, @RequestParam String hostId) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = containerManager.clear(hostId,user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	/**
	 * 日誌下載
	* TODO
	* @author mayh
	* @return String
	* @version 1.0
	* 2015年12月22日
	 */
	@RequestMapping("/download")
    public void download(String conId,String systemCode,String logName, HttpServletRequest request,
            HttpServletResponse response) {
		User user = (User) request.getSession().getAttribute("user");
		containerManager.download(conId,systemCode,logName,request,response,user);
	}
	/**
	 * 日志列表
	* TODO
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年1月14日
	 */
	@RequestMapping(value = "/loadLogs", method = { RequestMethod.GET })
	@ResponseBody
	public String loadLogs(HttpServletRequest request, @RequestParam String conId,@RequestParam String systemCode) {
		JSONArray ja = containerManager.loadLogs(conId,systemCode);
		 return ja.toString();
	}
	/**
	 * scp到平台服务器
	* TODO
	* @author mayh
	* @return GridBean
	* @version 1.0
	* 2016年1月14日
	 */
	@RequestMapping(value = "/scpLogs", method = { RequestMethod.GET })
	@ResponseBody
	public Result scpLogs(HttpServletRequest request, @RequestParam String conId,@RequestParam String systemCode,@RequestParam String remotePath) {
		Result result = containerManager.scpLogs(conId,systemCode,remotePath);
		
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		 return result;
	}
	
	
	//获取实时的日志信息
	@RequestMapping(value = "/getRealTimeLog", method = { RequestMethod.GET })
	@ResponseBody
	public Result getRealTimeLog(HttpServletRequest request, @RequestParam String conId ,@RequestParam String systemCode,@RequestParam String conTempName,@RequestParam long lastFileSize,@RequestParam String logName) throws IOException {
		
		//获取容器的IP地址
		Container container = new Container();
		container.setConId(conId);
		container = containerService.getContainer(container);
		Host host = hostService.getHost(container.getHostId());
		String hostip = host.getHostIp();
		//目录格式为/home/container/paas/SGCC-TEMP/timelog/10.134.161.151/b0e4700d372c46a9807cff30aee30198_testPaaS
		String localTimeLog = localConfig.getLocalTimeLogPath()+hostip+"/"+conId+"_"+systemCode+"/"+logName;
		String logs = "";
		JSONArray logja = new JSONArray();
		if (lastFileSize ==0){
			logja = logManager.getLast10LineLog(localTimeLog);
		}else{
			logja = logManager.getLatestLog(localTimeLog,lastFileSize);
		}
		Result res  = new Result(true,logja.toString());
		return res;
	}
	
	//挂载实时的日志的目录
		@RequestMapping(value = "/mountTimeLog", method = { RequestMethod.GET })
		@ResponseBody
		public Result mountTimeLog(HttpServletRequest request, @RequestParam String conId,@RequestParam String systemCode,@RequestParam String conTempName,@RequestParam long lastFileSize) throws IOException {
			
			Result aaa = logManager.mountLogDir(conId, systemCode);
			Result res  = new Result(true,"挂载成功");
			return aaa;
		}
	
	/*导出容器镜像 docker commit */
	@RequestMapping(value = "/commitContainer", method = { RequestMethod.GET })
	@ResponseBody
	public String commitContainer(HttpServletRequest request,   @RequestParam(value = "conId", required = true) String conId,
			@RequestParam(value = "conUuid", required = true) String conUuid, 
			@RequestParam(value = "imageName", required = true) String imageName,
			@RequestParam(value = "imageTag", required = true) String imageTag) {
		JSONObject jo = containerManager.commitContainer(conId,conUuid,imageName,imageTag);
		 return jo.toString();
	}
	
	/*导出镜像 scp到平台服务器*/
	@RequestMapping(value = "/scpConImage", method = { RequestMethod.GET })
	@ResponseBody
	public Result scpConImage(HttpServletRequest request,   @RequestParam(value = "conId", required = true) String conId,
			@RequestParam(value = "remotePath",required = true) String remotePath,
			@RequestParam(value = "imageName",required = true) String imageName,
			@RequestParam(value = "imageTag",required = true) String imageTag) {
        Result result = containerManager.scpConImage(conId,remotePath,imageName,imageTag);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	/*导出镜像 download到本地*/
	@RequestMapping("/downloadConImage")
    public void downloadConImage(String conName, HttpServletRequest request,
            HttpServletResponse response) {
		User user = (User) request.getSession().getAttribute("user");
		containerManager.downloadConImage(conName,request,response,user);
	}
	/**
	 * 检查主机容器，将未入库的容器入库
	 * 
	 * @author mayh
	 * @return Result
	 * @version 1.0 2015年11月18日
	 */
	@RequestMapping(value = "/checkHostContaniners", method = { RequestMethod.GET })
	@ResponseBody
	public Result checkHostContaniners(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("user");
		Result result = containerManager.checkHostContaniners(user);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
}
