package com.sgcc.devops.web.manager;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.dao.entity.ElasticItem;
import com.sgcc.devops.dao.entity.ElasticStrategy;
import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.service.ElasticItemService;
import com.sgcc.devops.service.ElasticStrategyService;
import com.sgcc.devops.service.SystemService;

/**  
 * date：2016年2月4日 下午1:56:41
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：ElasticStrategyManager.java
 * description：  弹性伸缩业务流程处理类
 */
@Component
public class ElasticStrategyManager {

	@Autowired
	private ElasticItemService elasticItemService;
	@Autowired
	private ElasticStrategyService elasticStrategyService;
	@Autowired
	private SystemService systemService;

	private static Logger logger = Logger.getLogger(ElasticStrategyManager.class);

	// 创建弹性伸缩策略
	public Result create(ElasticStrategy elasticStrategy, List<ElasticItem> items, String elasticStatus) {
		Result result = null;
		logger.info("Begin add ElasticStrategy");
		String systemId = elasticStrategy.getSystemId();
		System system = systemService.getSystemById(systemId);
		system.setSystemElsasticityStatus(Byte.valueOf(elasticStatus));
		int resultInt = systemService.updateSystem(system);
		if (0 == resultInt) {
			logger.error("Update System fail");
			return new Result(false, "更新物理系统表失败");
		}
		// 判断是否已经设置了弹性伸缩，如果设置，更新。否则新增
		ElasticStrategy elasticStrategy2 = elasticStrategyService.loadBySystem(systemId);
		if (null != elasticStrategy2) {
			elasticStrategy.setId(elasticStrategy2.getId());
			result = elasticStrategyService.update(elasticStrategy);
			if (result.isSuccess()) {
				List<ElasticItem> elasticItems = elasticItemService.loadByStrategy(elasticStrategy2.getId());
				for (ElasticItem elasticItem : elasticItems) {
					for (ElasticItem item : items) {
						if (elasticItem.getItemName().equals(item.getItemName())) {
							item.setId(elasticItem.getId());
							result = elasticItemService.update(item);
							if (!result.isSuccess()) {
								logger.error("Update elasticItem fail");
								return new Result(false, "更新策略条目表失败");
							}
						}
					}

				}
			} else {
				logger.error("Update ElasticStrategy fail");
				return new Result(false, "更新策略表失败");
			}

		} else {
			// 添加弹性策略
			TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
			elasticStrategy.setGmtCreate(new Date());
			result = elasticStrategyService.insert(elasticStrategy);

			if (result.isSuccess()) {
				// 创建弹性策略条目
				for (ElasticItem item : items) {
					item.setStrategyId(elasticStrategy.getId());
					item.setGmtCreate(new Date());
					Result itemResult = elasticItemService.insert(item);
					if (itemResult.isSuccess()) {
					} else {
						return itemResult;
					}
				}
			} else {
				logger.error("Add ElasticStrategy fail");
				return result;
			}
		}
		return new Result(true, "操作成功");
	}

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return JSONArray
	 * @version 1.0 2015年12月7日
	 */
	public JSONObject load(String systemId) {
		JSONObject jo = new JSONObject();
		System system = systemService.getSystemById(systemId);
		byte elsasticityStatus = system.getSystemElsasticityStatus();// 是否启用
		ElasticStrategy elasticStrategy = elasticStrategyService.loadBySystem(systemId);
		jo.put("elsasticityStatus", elsasticityStatus);
		if (null != elasticStrategy) {
			jo.put("maxNode", elasticStrategy.getMaxNode());
			jo.put("minNode", elasticStrategy.getMinNode());
			jo.put("stepSize", elasticStrategy.getStepSize());
			jo.put("conflictStrategy", elasticStrategy.getConflictStrategy());
			jo.put("elasticStrategy", elasticStrategy.getElasticStrategy());
			jo.put("extendDuration", elasticStrategy.getExtendDuration());
			jo.put("extendIntervals", elasticStrategy.getExtendIntervals());
			jo.put("contractionDuration", elasticStrategy.getContractionDuration());
			jo.put("contractionIntervals", elasticStrategy.getContractionIntervals());


			List<ElasticItem> elasticItems = elasticStrategy.getItems();
			JSONArray jsonArray = new JSONArray();
			for (ElasticItem elasticItem : elasticItems) {
				JSONObject json = new JSONObject();
				json.put("itemName", elasticItem.getItemName());
				json.put("expandThreshold", elasticItem.getExpandThreshold());
				json.put("shrinkThreshold", elasticItem.getShrinkThreshold());
				json.put("priority", elasticItem.getPriority());
				jsonArray.add(json);
			}
			jo.put("items", jsonArray);
		}
		return jo;
	}
}
