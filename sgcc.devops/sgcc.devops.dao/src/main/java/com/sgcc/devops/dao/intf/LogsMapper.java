package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Logs;

/**
 * 运行日志信息数据接口
 */
public interface LogsMapper {
	/**
	 * 查询所有的运行日志
	 * @author langzi
	 * @return List<Logs>
	 * @version 1.0 2015年8月19日
	 */
	public List<Logs> selectAll(Logs logs);

}
