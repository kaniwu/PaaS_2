package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ConPort;

/**
 * 容器端口关联数据接口
 */
public interface ConPortMapper {

	/**
	 * 查询容器的所有端口关联
	 * @author langzi
	 * @param conPort
	 * @return List<ConPort>
	 * @version 1.0 2015年8月31日
	 */
	public List<ConPort> selectConport(String containerId) throws Exception;

	/**
	 * 查询应用所对应容器的所有端口关联
	 * @author langzi
	 * @param appId
	 * @return  List<ConPort>
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<ConPort> selectConportByAppId(String appId) throws Exception;

	/**
	 * 新增容器端口关联
	 * @author langzi
	 * @param record
	 * @return insert conport
	 * @version 1.0 2015年8月31日
	 */
	public int insertConport(ConPort record) throws Exception;

	/**
	 * 删除容器端口关联
	 * @author langzi
	 * @param id
	 * @return delete conport
	 * @version 1.0 2015年8月31日
	 */
	public int deleteConport(String[] containerIds) throws Exception;

	/**
	 * 更新容器端口关联
	 * @author langzi
	 * @param record
	 * @return update conports
	 * @version 1.0 2015年8月31日
	 */
	public int updateConport(ConPort record) throws Exception;

}