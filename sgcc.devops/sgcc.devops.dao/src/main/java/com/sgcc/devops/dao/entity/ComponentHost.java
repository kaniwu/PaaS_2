package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 组件关联主机信息类
 */
public class ComponentHost {
	private String hostId;

	private String hostUuid;

	private String hostName;

	private String hostUser;

	private String hostPwd;
	
	private String hostPort;

	private Byte hostType;

	private String hostIp;

	private Integer hostCpu;

	private Integer hostMem;

	private Byte hostStatus;

	private String hostDesc;

	private String hostKernelVersion;

	private String clusterId;

	private Byte dockerStatus;
	@JsonSerialize(using = DateSerializer.class)
	private Date hostCreatetime;

	private String hostCreator;
	
	private String componentHostId;
	
	private String dopComponentId;
	
	private Byte nginxStatus;
	
	private Integer nginxPort;
	
	private String delFlag;

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostUuid() {
		return hostUuid;
	}

	public void setHostUuid(String hostUuid) {
		this.hostUuid = hostUuid;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostUser() {
		return hostUser;
	}

	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}

	public String getHostPwd() {
		return hostPwd;
	}

	public void setHostPwd(String hostPwd) {
		this.hostPwd = hostPwd;
	}
	
	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public Byte getHostType() {
		return hostType;
	}

	public void setHostType(Byte hostType) {
		this.hostType = hostType;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

	public Integer getHostCpu() {
		return hostCpu;
	}

	public void setHostCpu(Integer hostCpu) {
		this.hostCpu = hostCpu;
	}

	public Integer getHostMem() {
		return hostMem;
	}

	public void setHostMem(Integer hostMem) {
		this.hostMem = hostMem;
	}

	public Byte getHostStatus() {
		return hostStatus;
	}

	public void setHostStatus(Byte hostStatus) {
		this.hostStatus = hostStatus;
	}

	public String getHostDesc() {
		return hostDesc;
	}

	public void setHostDesc(String hostDesc) {
		this.hostDesc = hostDesc;
	}

	public String getHostKernelVersion() {
		return hostKernelVersion;
	}

	public void setHostKernelVersion(String hostKernelVersion) {
		this.hostKernelVersion = hostKernelVersion;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public Byte getDockerStatus() {
		return dockerStatus;
	}

	public void setDockerStatus(Byte dockerStatus) {
		this.dockerStatus = dockerStatus;
	}

	public String getComponentHostId() {
		return componentHostId;
	}

	public void setComponentHostId(String componentHostId) {
		this.componentHostId = componentHostId;
	}

	public String getDopComponentId() {
		return dopComponentId;
	}

	public void setDopComponentId(String dopComponentId) {
		this.dopComponentId = dopComponentId;
	}

	public Byte getNginxStatus() {
		return nginxStatus;
	}

	public void setNginxStatus(Byte nginxStatus) {
		this.nginxStatus = nginxStatus;
	}

	public Integer getNginxPort() {
		return nginxPort;
	}

	public void setNginxPort(Integer nginxPort) {
		this.nginxPort = nginxPort;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public Date getHostCreatetime() {
		return hostCreatetime;
	}

	public void setHostCreatetime(Date hostCreatetime) {
		this.hostCreatetime = hostCreatetime;
	}

	public String getHostCreator() {
		return hostCreator;
	}

	public void setHostCreator(String hostCreator) {
		this.hostCreator = hostCreator;
	}
}
