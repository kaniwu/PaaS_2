package com.sgcc.devops.dao.intf;

import java.util.List;
import java.util.Map;

import com.sgcc.devops.dao.entity.Authority;


public interface AuthorityMapper {
	
	public List<Authority> selectAll(Authority authority) throws Exception;
	
	public List<Authority> selectListByActionParentId(String pid) throws Exception;
	
	public Authority getActionById(String id) throws Exception;
	
	public Authority getActionParentId(String id) throws Exception;
	
	public int childrenCount(String id) throws Exception;
	
    public int deleteByPrimaryKey(String actionId) throws Exception;
    
    public int deleteByRoleId(String roleId) throws Exception;

    public int insert(Authority record) throws Exception;

    public int insertSelective(Authority record) throws Exception;

    public Authority selectByPrimaryKey(String actionId) throws Exception;

    public int updateByPrimaryKeySelective(Authority record) throws Exception;

    public int updateByPrimaryKey(Authority record) throws Exception;

	public List<Authority> selectAllByUserId(String userId) throws Exception;
	
	public List<Authority> getAuthListByRoleId(Map<String,String> map) throws Exception;
    
}