package com.sgcc.devops.dao.entity;

/**
 * 仓库镜像关联类
 */
public class RegImage {
	private String id;

	private String registryId;

	private String imageId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public RegImage(String id, String registryId, String imageId) {
		super();
		this.id = id;
		this.registryId = registryId;
		this.imageId = imageId;
	}

	public RegImage() {
		super();
		// TODO Auto-generated constructor stub
	}

}