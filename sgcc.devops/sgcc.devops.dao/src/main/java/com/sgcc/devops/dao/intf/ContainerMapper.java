package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.expand.ContainerExpand;

/**
 * @author langzi
 * 容器信息数据接口
 */
public interface ContainerMapper {

	/**
	 * 查询所有容器
	 * @author langzi
	 * @return List<Container>
	 * @version 1.0 2015年8月21日
	 */
	public List<Container> selectAll() throws Exception;

	/**
	 * 查询应用所对应的容器
	 * @author langzi
	 * @param appId
	 * @return List<Container>
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<Container> selectContainerByAppId(String appId) throws Exception;

	/**
	 * 查询主机所对应的容器
	 * @author langzi
	 * @param appId
	 * @return List<Container>
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<Container> selectContainerByHostId(String HostId) throws Exception;

	/**
	 * Host下所有未删除的container
	 * 
	 * @author zhangxin
	 * @param hostId
	 * @return List<Container>
	 * @throws Exception
	 * @version 1.0 2015年9月15日
	 */
	public List<Container> selectAllContainerOfHost(String HostId) throws Exception;

	/**
	 * 查询容器信息
	 * @author langzi
	 * @param container
	 * @return Container
	 * @version 1.0 2015年8月21日
	 */
	public Container selectContainer(Container container) throws Exception;

	/**
	 * 查询容器列表(运行状态除删除外)
	 * @author langzi
	 * @param ja
	 * @return List<Container>
	 * @throws Exception
	 * @version 1.0 2015年8月26日
	 */
	public List<Container> selectContainerUuid(String[] containerIds) throws Exception;

	/**
	 * 新增容器
	 * @author langzi
	 * @param container
	 * @return insert container
	 * @version 1.0 2015年8月21日
	 */
	public int insertContainer(Container container) throws Exception;

	/**
	 * 更新容器
	 * @author langzi
	 * @param conId
	 * @return update container
	 * @version 1.0 2015年8月21日
	 */
	public int updateContainer(Container container) throws Exception;

	/**
	 * 更新容器状态
	 * @author langzi
	 * @param power
	 * @param ja
	 * @return update container
	 * @version 1.0 2015年8月26日
	 */
	public int updateContainerPower(ContainerExpand containerExpand) throws Exception;

	/**
	 * 更新容器运行状态
	 * @author langzi
	 * @param status
	 * @param ja
	 * @return update container
	 * @version 1.0 2015年8月26日
	 */
	public int updateContainerStatus(ContainerExpand containerExpand) throws Exception;

	/**
	 * 删除容器
	 * @author langzi
	 * @param conId
	 * @return delete container
	 * @version 1.0 2015年8月21日
	 */
	public int deleteContainer(String[] containerIds) throws Exception;

	/**
	 * 查询容器列表
	 * @author zhangxin
	 * @param containerIds
	 * @return List<Container>
	 * @throws Exception
	 * @version 1.0 2015年10月10日
	 */
	public List<Container> selectContainerId(String[] containerIds) throws Exception;

	/**
	 * TODO
	 * 查询镜像所对应的容器列表
	 * @author mayh
	 * @return List<Container>
	 * @version 1.0 2015年10月16日
	 */
	public List<Container> selectContainersByImageId(String conImgid);

	/**
	 * TODO
	 * 通过容器类别查询列表
	 * @author mayh
	 * @return List<Container>
	 * @version 1.0 2015年10月16日
	 */
	public List<Container> selectContainersByType(Container container);

	/**
	 * TODO
	 * 通过容器状态查询列表
	 * @author zhangxin
	 * @return List<Container>
	 * @version 1.0 2015年11月4日
	 */
	public List<Container> selectContainersByPower(Container container);

	/** 所有容器数量 */
	public int selectContainerCount(Container container);

	/**
	 * TODO
	 * 通过系统查询容器列表
	 * @author mayh
	 * @return List<Container>
	 * @version 1.0 2015年11月25日
	 */
	public List<Container> selectContainerBySysId(String sysId);
	
	public List<Container> listWaitContainersBySysId(String systemId);

}