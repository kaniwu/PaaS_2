package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

public class RegistryLbWithIP {
	private String id;

	private String registryName;

	private String registryDesc;
	
	private Byte loadBalanceType;
	
	private String domainName;
	
	private Integer loadBalancePort;
	
	private String f5Ip;
	
	private String lbHostId;
	
	private String lbHostIp;
	
	private Byte status;
	
	@JsonSerialize(using = DateSerializer.class)
	private Date registryCreatetime;
	
	private String creatorName;
	
	private String registryCreator;
	
	private String hostIp;//查询条件：hostip

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegistryName() {
		return registryName;
	}

	public void setRegistryName(String registryName) {
		this.registryName = registryName;
	}

	public String getRegistryDesc() {
		return registryDesc;
	}

	public void setRegistryDesc(String registryDesc) {
		this.registryDesc = registryDesc;
	}

	public Byte getLoadBalanceType() {
		return loadBalanceType;
	}

	public void setLoadBalanceType(Byte loadBalanceType) {
		this.loadBalanceType = loadBalanceType;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public Integer getLoadBalancePort() {
		return loadBalancePort;
	}

	public void setLoadBalancePort(Integer loadBalancePort) {
		this.loadBalancePort = loadBalancePort;
	}

	public String getF5Ip() {
		return f5Ip;
	}

	public void setF5Ip(String f5Ip) {
		this.f5Ip = f5Ip;
	}

	public String getLbHostId() {
		return lbHostId;
	}

	public void setLbHostId(String lbHostId) {
		this.lbHostId = lbHostId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Date getRegistryCreatetime() {
		return registryCreatetime;
	}

	public void setRegistryCreatetime(Date registryCreatetime) {
		this.registryCreatetime = registryCreatetime;
	}

	public String getRegistryCreator() {
		return registryCreator;
	}

	public void setRegistryCreator(String registryCreator) {
		this.registryCreator = registryCreator;
	}

	public String getLbHostIp() {
		return lbHostIp;
	}

	public void setLbHostIp(String lbHostIp) {
		this.lbHostIp = lbHostIp;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}

}
