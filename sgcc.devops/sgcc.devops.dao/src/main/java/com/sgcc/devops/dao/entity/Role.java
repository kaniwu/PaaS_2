package com.sgcc.devops.dao.entity;

public class Role {
    private String roleId;

    private String roleName;

    private String roleDesc;

    private Byte roleRemarks;

    private Byte roleStatus;
    
    private Byte isUsed;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc == null ? null : roleDesc.trim();
    }

    public Byte getRoleRemarks() {
        return roleRemarks;
    }

    public void setRoleRemarks(Byte roleRemarks) {
        this.roleRemarks = roleRemarks;
    }

    public Byte getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(Byte roleStatus) {
        this.roleStatus = roleStatus;
    }

	public Byte getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Byte isUsed) {
		this.isUsed = isUsed;
	}
}