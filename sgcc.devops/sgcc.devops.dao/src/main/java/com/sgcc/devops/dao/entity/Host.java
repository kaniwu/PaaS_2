package com.sgcc.devops.dao.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 主机信息类
 */
public class Host {
	private String hostId;

	private String hostUuid;

	private String hostName;

	private String hostUser;

	private String hostPwd;

	private Byte hostType;

	private String hostIp;
	
	private String hostPort;

	private Integer hostCpu;

	private Integer hostMem;

	private Byte hostStatus;

	private String hostDesc;

	private String hostEnv;
	
	private List<String> locationIdList;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String getHostEnv() {
		return hostEnv;
	}

	public void setHostEnv(String hostEnv) {
		this.hostEnv = hostEnv;
	}

	private String hostKernelVersion;

	private String clusterId;

	private Byte dockerStatus;
	@JsonSerialize(using = DateSerializer.class)
	private Date hostCreatetime;

	private String hostCreator;
	
	private String locationId;//位置ID
	
	private String regId;//主机指定仓库
	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostUuid() {
		return hostUuid;
	}

	public void setHostUuid(String hostUuid) {
		this.hostUuid = hostUuid == null ? null : hostUuid.trim();
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName == null ? null : hostName.trim();
	}

	public String getHostUser() {
		return hostUser;
	}

	public void setHostUser(String hostUser) {
		this.hostUser = hostUser == null ? null : hostUser.trim();
	}

	public String getHostPwd() {
		return hostPwd;
	}

	public void setHostPwd(String hostPwd) {
		this.hostPwd = hostPwd == null ? null : hostPwd.trim();
	}

	public Byte getHostType() {
		return hostType;
	}

	public void setHostType(Byte hostType) {
		this.hostType = hostType;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp == null ? null : hostIp.trim();
	}
	
	
	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public Integer getHostCpu() {
		return hostCpu;
	}

	public void setHostCpu(Integer hostCpu) {
		this.hostCpu = hostCpu;
	}

	public Integer getHostMem() {
		return hostMem;
	}

	public void setHostMem(Integer hostMem) {
		this.hostMem = hostMem;
	}

	public Byte getHostStatus() {
		return hostStatus;
	}

	public void setHostStatus(Byte hostStatus) {
		this.hostStatus = hostStatus;
	}

	public String getHostDesc() {
		return hostDesc;
	}

	public void setHostDesc(String hostDesc) {
		this.hostDesc = hostDesc == null ? null : hostDesc.trim();
	}

	public String getHostKernelVersion() {
		return hostKernelVersion;
	}

	public void setHostKernelVersion(String hostKernelVersion) {
		this.hostKernelVersion = hostKernelVersion == null ? null : hostKernelVersion.trim();
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public Date getHostCreatetime() {
		return hostCreatetime;
	}

	public void setHostCreatetime(Date hostCreatetime) {
		this.hostCreatetime = hostCreatetime;
	}

	public String getHostCreator() {
		return hostCreator;
	}

	public void setHostCreator(String hostCreator) {
		this.hostCreator = hostCreator;
	}

	public Byte getDockerStatus() {
		return dockerStatus;
	}

	public void setDockerStatus(Byte dockerStatus) {
		this.dockerStatus = dockerStatus;
	}


	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public List<String> getLocationIdList() {
		return locationIdList;
	}

	public void setLocationIdList(List<String> locationIdList) {
		this.locationIdList = locationIdList;
	}

}