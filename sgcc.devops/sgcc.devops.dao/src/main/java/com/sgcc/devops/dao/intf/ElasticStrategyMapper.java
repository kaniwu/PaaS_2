package com.sgcc.devops.dao.intf;

import com.sgcc.devops.dao.entity.ElasticStrategy;

/**
 * 弹性伸缩策略信息数据接口
 */
public interface ElasticStrategyMapper {

	/**
	 * TODO
	 * 新增弹性伸缩策略信息
	 * @return int
	 * @version 1.0
	 */
	public int insert(ElasticStrategy record);

	/**
	 * TODO
	 * 删除弹性伸缩策略信息
	 * @return int
	 * @version 1.0
	 */
	public int delete(String id);

	/**
	 * TODO
	 * 更新弹性伸缩策略信息
	 * @return int
	 * @version 1.0
	 */
	public int update(ElasticStrategy record);

	/**
	 * TODO
	 * 查询弹性伸缩策略信息
	 * @return ElasticStrategy
	 * @version 1.0
	 */
	public ElasticStrategy load(String id);

	/**
	 * TODO
	 * 查询弹性伸缩策略信息列表
	 * @return List<ElasticStrategy>
	 * @version 1.0
	 */
	public ElasticStrategy loadBySystem(String systemId);

}