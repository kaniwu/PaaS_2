package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.RegIdImageType;
import com.sgcc.devops.dao.entity.RegistryLb;

public interface RegistryLbMapper {
	
	public int deleteRegistryLb(String registryLbId);

	public int insertRegistryLb(RegistryLb record);

	public int updateRegistryLb(RegistryLb record);

	public List<RegistryLb> selectAll();
	
	public RegistryLb selectRegistryLb(RegistryLb record);
	
	public int changeStatus(List<String> list);
	
	public List<RegistryLb>  selectAllWithIP(RegistryLb record);

	/**
	* TODO
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年5月5日
	*/
	public int selectCountImageInRegistryLb(RegIdImageType image);
}