package com.sgcc.devops.dao.intf;

import com.sgcc.devops.dao.entity.Action;


public interface ActionMapper {
    int deleteByPrimaryKey(String actionId) throws Exception;

    int insert(Action record) throws Exception;

    int insertSelective(Action record) throws Exception;

    Action selectByPrimaryKey(String actionId) throws Exception;

    int updateByPrimaryKeySelective(Action record) throws Exception;

    int updateByPrimaryKey(Action record) throws Exception;
}