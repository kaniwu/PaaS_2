package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ElasticItem;

/**
 * 弹性伸缩策略信息项数据接口
 */
public interface ElasticItemMapper {
	
	/**
	 * TODO
	 * 新增弹性伸缩策略信息项
	 * @return int
	 * @version 1.0
	 */
	public int insert(ElasticItem record);

	/**
	 * TODO
	 * 删除弹性伸缩策略信息项
	 * @return int
	 * @version 1.0
	 */
	public int delete(String elasticItemId);

	/**
	 * TODO
	 * 更新弹性伸缩策略信息项
	 * @return int
	 * @version 1.0
	 */
	public int update(ElasticItem record);

	/**
	 * TODO
	 * 查询弹性伸缩策略信息项
	 * @return ElasticItem
	 * @version 1.0
	 */
	public ElasticItem load(String id);

	/**
	 * TODO
	 * 查询弹性伸缩策略信息项列表
	 * @return List<ElasticItem>
	 * @version 1.0
	 */
	public List<ElasticItem> loadByStrategy(String strategyId);

}