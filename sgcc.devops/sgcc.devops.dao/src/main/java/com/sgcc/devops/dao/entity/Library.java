package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 库文件类
 * @author liyp
 *
 */
public class Library {

	private String libraryId;
	private String libraryName;
	private String libraryDescription;
	private String libraryFilePath;
	private String userName;
	private int status;
	private String libraryLocalPath;
	private String libraryCreator;

	@JsonSerialize(using = DateSerializer.class)
	private Date libraryCreatetime;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
		
	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getLibraryDescription() {
		return libraryDescription;
	}

	public void setLibraryDescription(String libraryDescription) {
		this.libraryDescription = libraryDescription;
	}


	public String getLibraryCreator() {
		return libraryCreator;
	}

	public void setLibraryCreator(String libraryCreator) {
		this.libraryCreator = libraryCreator;
	}

	public Date getLibraryCreatetime() {
		return libraryCreatetime;
	}

	public void setLibraryCreatetime(Date libraryCreatetime) {
		this.libraryCreatetime = libraryCreatetime;
	}

	public String getLibraryFilePath() {
		return libraryFilePath;
	}

	public void setLibraryFilePath(String libraryFilePath) {
		this.libraryFilePath = libraryFilePath;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getLibraryLocalPath() {
		return libraryLocalPath;
	}

	public void setLibraryLocalPath(String libraryLocalPath) {
		this.libraryLocalPath = libraryLocalPath;
	}
	
}