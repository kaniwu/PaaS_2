package com.sgcc.devops.dao.entity;

import java.sql.Timestamp;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 监控信息类
 */
public class Monitor {

	private String monitorId;

	private Byte targetType;
	
	@JsonSerialize(using = DateSerializer.class)
	private Date monitorTime;

	private String targetId;

	private String cpu;

	private String mem;

	private String netin;

	private String netout;

	private String diskSpace;

	public String getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	public Byte getTargetType() {
		return targetType;
	}

	public void setTargetType(Byte targetType) {
		this.targetType = targetType;
	}

	public Date getMonitorTime() {
		return monitorTime;
	}

	public void setMonitorTime(Date monitorTime) {
		this.monitorTime = monitorTime;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getMem() {
		return mem;
	}

	public void setMem(String mem) {
		this.mem = mem;
	}

	public String getNetin() {
		return netin;
	}

	public void setNetin(String netin) {
		this.netin = netin;
	}

	public String getNetout() {
		return netout;
	}

	public void setNetout(String netout) {
		this.netout = netout;
	}

	public String getDiskSpace() {
		return diskSpace;
	}

	public void setDiskSpace(String diskSpace) {
		this.diskSpace = diskSpace;
	}

	public Monitor() {
		super();
	}

	public Monitor(Byte targetType, Timestamp monitorTime, String targetId, String cpu, String mem, String netin,
			String netout, String diskSpace) {
		super();
		this.targetType = targetType;
		this.monitorTime = monitorTime;
		this.targetId = targetId;
		this.cpu = cpu;
		this.mem = mem;
		this.netin = netin;
		this.netout = netout;
		this.diskSpace = diskSpace;
	}

	@Override
	public String toString() {
		return "Monitor [monitorId=" + monitorId + ", targetType=" + targetType + ", monitorTime=" + monitorTime
				+ ", targetId=" + targetId + ", cpu=" + cpu + ", mem=" + mem + ", netin=" + netin + ", netout=" + netout
				+ ", diskSpace=" + diskSpace + "]";
	}

}
