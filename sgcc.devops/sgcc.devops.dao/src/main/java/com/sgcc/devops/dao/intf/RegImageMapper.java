package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.RegImage;

/**
 * 仓库镜像关联信息数据接口
 */
public interface RegImageMapper {
	/**
	 * TODO
	 * 删除仓库镜像关联信息
	 * @return int
	 * @version 1.0
	 */
	int deleteByPrimaryKey(String id);

	/**
	 * TODO
	 * 通过image id删除仓库镜像关联信息
	 * @return int
	 * @version 1.0
	 */
	int deleteByImage(String id);

	/**
	 * TODO
	 * 新增仓库镜像关联信息
	 * @return int
	 * @version 1.0
	 */
	int insert(RegImage record);

	/**
	 * TODO
	 * 查询仓库镜像关联信息
	 * @return RegImage
	 * @version 1.0
	 */
	RegImage selectByPrimaryKey(String id);

	/**
	 * TODO
	 * 更新仓库镜像关联信息
	 * @return int
	 * @version 1.0
	 */
	int updateByPrimaryKeySelective(RegImage record);

	/**
	 * TODO
	 * 更新仓库镜像关联信息
	 * @return int
	 * @version 1.0
	 */
	int updateByPrimaryKey(RegImage record);

	/**
	 * TODO
	 * 通过镜像id查询仓库镜像关联信息
	 * @return RegImage
	 * @version 1.0
	 */
	RegImage selectByImageId(String imageId);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<RegImageMapper>
	 * @version 1.0 2015年8月31日
	 */
	public List<RegImage> selectAll(RegImage record);
}