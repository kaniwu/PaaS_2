package com.sgcc.devops.dao.intf;


import java.util.List;

import com.sgcc.devops.dao.entity.User;

public interface UserMapper {
	
	/**
	 * @author langzi
	 * @param user
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public int insertUser(User user) throws Exception;
	
    /**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public int deleteUser(String userId) throws Exception;
	
	/**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public int activeUser(String userId) throws Exception;
	
	/**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年11月10日
	 */
//	public int userLogin(Integer userId);
	
	/**
	 * @author langzi
	 * @param userId
	 * @return
	 * @version 1.0
	 * 2015年11月10日
	 */
//	public int userLogout(Integer userId);
	
	
    /**
	 * @author langzi
	 * @param user
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public int updateUser(User user) throws Exception;
	
	/**
	 * @author langzi
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public List<User> selectAll(User user) throws Exception;

   	/**
	 * @author langzi
	 * @param user
	 * @return
	 * @version 1.0
	 * 2015年8月14日
	 */
	public User selectUser(User user) throws Exception;
	
	/**
	 * @author langzi
	 * @param hostName
	 * @return
	 * @version 1.0 2015年10月21日
	 */
	public User selectUserByName(String userName) throws Exception;
	
	/**
	 * 初始化用户登录状态，在系统启动时使用
	 * @throws Exception
	 */
//	public int initUser()throws Exception;
	
	/**
	 * 查询当前物理系统管理员
	 * @author wangjx
	 * @return int
	 * @version 1.0 2016年3月10日
	 */
	public List<User> selectSystemAdmins(String systemId);
	
	/**
	 * 查询所有物理系统管理员
	 * @author wangjx
	 * @return int
	 * @version 1.0 2016年3月10日
	 */
	public List<User> selectAllAdmin();

}