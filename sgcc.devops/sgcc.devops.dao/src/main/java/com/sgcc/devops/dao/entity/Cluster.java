package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 集群基本信息类
 */
public class Cluster {
	private String clusterId;

	private String clusterUuid;

	private String clusterName;

	private Byte clusterType;

	private Byte clusterStatus;

	private String clusterPort;

	private String managePath;

	private String clusterDesc;
	private String clusterEnv;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String getClusterEnv() {
		return clusterEnv;
	}

	public void setClusterEnv(String clusterEnv) {
		this.clusterEnv = clusterEnv;
	}

	private String masteHostId;
	@JsonSerialize(using = DateSerializer.class)
	private Date clusterCreatetime;

	private String clusterCreator;
	
	private String standByHostId;

	private String registryLbId;
	public String getClusterId() {
		return clusterId;
	}

	public String getRegistryLbId() {
		return registryLbId;
	}

	public void setRegistryLbId(String registryLbId) {
		this.registryLbId = registryLbId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getClusterUuid() {
		return clusterUuid;
	}

	public void setClusterUuid(String clusterUuid) {
		this.clusterUuid = clusterUuid == null ? null : clusterUuid.trim();
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName == null ? null : clusterName.trim();
	}

	public Byte getClusterType() {
		return clusterType;
	}

	public void setClusterType(Byte clusterType) {
		this.clusterType = clusterType;
	}

	public Byte getClusterStatus() {
		return clusterStatus;
	}

	public void setClusterStatus(Byte clusterStatus) {
		this.clusterStatus = clusterStatus;
	}

	public String getClusterPort() {
		return clusterPort;
	}

	public void setClusterPort(String clusterPort) {
		this.clusterPort = clusterPort == null ? null : clusterPort.trim();
	}

	public String getManagePath() {
		return managePath;
	}

	public void setManagePath(String managePath) {
		this.managePath = managePath == null ? null : managePath.trim();
	}

	public String getClusterDesc() {
		return clusterDesc;
	}

	public void setClusterDesc(String clusterDesc) {
		this.clusterDesc = clusterDesc == null ? null : clusterDesc.trim();
	}

	public String getMasteHostId() {
		return masteHostId;
	}

	public void setMasteHostId(String masteHostId) {
		this.masteHostId = masteHostId;
	}

	public Date getClusterCreatetime() {
		return clusterCreatetime;
	}

	public void setClusterCreatetime(Date clusterCreatetime) {
		this.clusterCreatetime = clusterCreatetime;
	}

	public String getClusterCreator() {
		return clusterCreator;
	}

	public void setClusterCreator(String clusterCreator) {
		this.clusterCreator = clusterCreator;
	}

	public String getStandByHostId() {
		return standByHostId;
	}

	public void setStandByHostId(String standByHostId) {
		this.standByHostId = standByHostId;
	}
	
	
}