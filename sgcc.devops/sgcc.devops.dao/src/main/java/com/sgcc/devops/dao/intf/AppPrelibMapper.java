package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.AppPrelib;

public interface AppPrelibMapper {
	public List<AppPrelib> selectByAppId(String appId);

	/**
	* TODO
	* @author mayh
	* @return void
	* @version 1.0
	* 2016年4月22日
	*/
	public void createAppPrelib(AppPrelib appPrelib);
	
	public int selectPrelibCount(String id);
}
