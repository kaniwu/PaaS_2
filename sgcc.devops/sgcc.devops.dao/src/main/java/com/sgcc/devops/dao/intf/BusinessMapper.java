package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Business;

/**
 * 业务系统数据接口
 */
public interface BusinessMapper {

	/**
	 * TODO
	 * 根据业务id查询业务信息
	 * @author mayh
	 * @return Business
	 * @version 1.0 2015年10月26日
	 */
	Business selectByPrimaryKey(String businessId);

	/**
	 * TODO
	 * 列出所有的业务信息
	 * @author mayh
	 * @return List<Business>
	 * @version 1.0 2015年10月26日
	 */
	List<Business> listAllOfBusiness(Business business);

	/**
	 * TODO
	 * 查询业务数量
	 * @author zhangxin
	 * @return int 业务系统数量
	 * @version 1.0 2015年10月26日
	 */
	public int selectBusinessCount();

	/**
	* TODO
	* 更新业务信息
	* @author mayh
	* @return void
	* @version 1.0
	* 2015年12月30日
	*/
	int update(Business business);

	/**
	* TODO
	* @author mayh
	* @return Integer
	* @version 1.0
	* 2016年3月8日
	*/
	Integer countDeployed();
}