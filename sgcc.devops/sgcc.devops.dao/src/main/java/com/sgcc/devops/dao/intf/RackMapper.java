package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Rack;


public interface RackMapper {
    int deleteByPrimaryKey(String id) throws Exception;

    int insert(Rack record) throws Exception;

    Rack selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(Rack record) throws Exception;

    int updateByPrimaryKey(Rack record) throws Exception;

	/**
	* TODO
	* @author mayh
	* @return List<Rack>
	* @version 1.0
	* 2016年3月16日
	*/
	List<Rack> rackListByServerRoom(String serverRoomId);

	int countRackName(Rack rack);
}