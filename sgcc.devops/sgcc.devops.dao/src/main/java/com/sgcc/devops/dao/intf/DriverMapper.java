/**
 * 
 */
package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Driver;

/**
 * date：2015年11月25日 上午10:31:25 project name：sgcc.devops.dao
 * 驱动信息数据接口
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：DriverMapper.java description：
 */
public interface DriverMapper {
	public List<Driver> selectByType(int DBType);

	/**
	 * TODO
	 * 查询驱动信息
	 * @author mayh
	 * @return Driver
	 * @version 1.0 2015年11月30日
	 */
	public Driver selectByPrimaryId(String id);
	
	
}
