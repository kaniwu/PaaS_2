package com.sgcc.devops.dao.entity;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 弹性伸缩策略项类
 */
public class ElasticItem {

	private String id;
	private String strategyId;
	private String itemName;
	private Integer priority;
	private BigDecimal expandThreshold;
	private BigDecimal shrinkThreshold;

	@JsonSerialize(using = DateSerializer.class)
	private Date gmtCreate;
	@JsonSerialize(using = DateSerializer.class)
	private Date gmtModify;

	private Integer creator;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStrategyId() {
		return strategyId;
	}

	public void setStrategyId(String strategyId) {
		this.strategyId = strategyId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public BigDecimal getExpandThreshold() {
		return expandThreshold;
	}

	public void setExpandThreshold(BigDecimal expandThreshold) {
		this.expandThreshold = expandThreshold;
	}

	public BigDecimal getShrinkThreshold() {
		return shrinkThreshold;
	}

	public void setShrinkThreshold(BigDecimal shrinkThreshold) {
		this.shrinkThreshold = shrinkThreshold;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModify() {
		return gmtModify;
	}

	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}

	public Integer getCreator() {
		return creator;
	}

	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	public ElasticItem(String strategyId, String itemName, Integer priority, BigDecimal expandThreshold,
			BigDecimal shrinkThreshold) {
		super();
		this.strategyId = strategyId;
		this.itemName = itemName;
		this.priority = priority;
		this.expandThreshold = expandThreshold;
		this.shrinkThreshold = shrinkThreshold;
	}

	public ElasticItem() {
		super();
		// TODO Auto-generated constructor stub
	}

}
