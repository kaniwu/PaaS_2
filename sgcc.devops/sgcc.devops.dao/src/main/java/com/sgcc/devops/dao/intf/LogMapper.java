package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Log;

/**
 * 日志信息数据接口
 */
public interface LogMapper {

	/**
	 * 新增日志
	 * @author langzi
	 * @param record
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int insertLog(Log record);

	/**
	 * 删除日志
	 * @author langzi
	 * @param logId
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int deleteLog(String logId);

	/**
	 * 更新日志
	 * @author langzi
	 * @param record
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int updateLog(Log record);

	/**
	 * 查询符合条件的日志列表
	 * @author langzi
	 * @return List<Log>
	 * @version 1.0 2015年8月19日
	 */
	public List<Log> selectAll(Log log);

	/**
	 * 查询日志
	 * @author langzi
	 * @return Log
	 * @version 1.0 2015年8月19日
	 */
	public Log selectLog();

}