package com.sgcc.devops.dao.entity;

/**
 * 容器端口关联类
 */
public class ConPort {
	private String id;

	private String containerId;

	private String conIp;

	private String pubPort;

	private String priPort;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getConIp() {
		return conIp;
	}

	public void setConIp(String conIp) {
		this.conIp = conIp;
	}

	public String getPubPort() {
		return pubPort;
	}

	public void setPubPort(String pubPort) {
		this.pubPort = pubPort == null ? null : pubPort.trim();
	}

	public String getPriPort() {
		return priPort;
	}

	public void setPriPort(String priPort) {
		this.priPort = priPort == null ? null : priPort.trim();
	}
}