package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

public class UserWithRole {
	
    private String userId;

    private String userName;

    private String userPass;

    private String userMail;

    private String userPhone;

    private String userCompany;

    private Integer userLevel;

    private Byte userStatus;

    private String userRoleid;
    @JsonSerialize(using = DateSerializer.class)
    private Date userCreatedate;

    private String userCreator;
    
    private String createUserName; 
    
    private String roleName;
    
    public UserWithRole(){
    	
    }

    public UserWithRole(String userId, String userName, String userPass,
			String userMail, String userPhone, String userCompany,
			Integer userLevel, Byte userStatus, String userRoleid,
			Date userCreatedate, String userCreator) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userPass = userPass;
		this.userMail = userMail;
		this.userPhone = userPhone;
		this.userCompany = userCompany;
		this.userLevel = userLevel;
		this.userStatus = userStatus;
		this.userRoleid = userRoleid;
		this.userCreatedate = userCreatedate;
		this.userCreator = userCreator;
	}

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass == null ? null : userPass.trim();
    }

    public String getUserMail() {
        return userMail;
    }

    public void setUserMail(String userMail) {
        this.userMail = userMail == null ? null : userMail.trim();
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone == null ? null : userPhone.trim();
    }

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany == null ? null : userCompany.trim();
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(Integer userLevel) {
        this.userLevel = userLevel;
    }

    public Byte getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Byte userStatus) {
        this.userStatus = userStatus;
    }

    public String getUserRoleid() {
        return userRoleid;
    }

    public void setUserRoleid(String userRoleid) {
        this.userRoleid = userRoleid;
    }

    public Date getUserCreatedate() {
        return userCreatedate;
    }

    public void setUserCreatedate(Date userCreatedate) {
        this.userCreatedate = userCreatedate;
    }

    public String getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(String userCreator) {
        this.userCreator = userCreator;
    }

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
}