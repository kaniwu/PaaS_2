package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ServerRoom;


public interface ServerRoomMapper {
    int deleteByPrimaryKey(String id) throws Exception;

    int insert(ServerRoom record) throws Exception;

    int insertSelective(ServerRoom record) throws Exception;

    ServerRoom selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(ServerRoom record) throws Exception;

    int updateByPrimaryKey(ServerRoom record) throws Exception;

	/**
	* TODO
	* @author mayh
	* @return List<ServerRoom>
	* @version 1.0
	* 2016年3月16日
	*/
	List<ServerRoom> selectAll();
	
	/**
	* TODO
	* @author panjing
	* @return int
	* @version 1.0
	* 2016年3月24日
	*/
	int countServerRoomName(String serverRoomName);
}