package com.sgcc.devops.dao.intf;

import java.util.List;
import java.util.Map;

import com.sgcc.devops.dao.entity.System;
import com.sgcc.devops.dao.entity.SystemApp;
import com.sgcc.devops.dao.entity.User;

/**
 * 系统信息数据接口
 */
public interface SystemMapper {

	/**
	 * 删除系统信息
	 * @author langzi
	 * @return int
	 * @version 1.0 2015年8月14日
	 */
	int deleteByPrimaryKey(String systemId);
	
	/**
	 * 查询系统信息
	 * @author langzi
	 * @return System
	 * @version 1.0 2015年8月14日
	 */
	public System selectByPrimaryKey(String actionId);

	/**
	 * 查询符合条件的系统信息列表
	 * @author langzi
	 * @return List<System>
	 * @version 1.0 2015年8月14日
	 */
	public List<System> selectAll(System record);
	/**
	 * 新增系统信息
	 * @author langzi
	 * @return int
	 * @version 1.0 2015年8月14日
	 */
	public int insertSystem(System record);

	/**
	 * 更新系统信息
	 * @author langzi
	 * @return int
	 * @version 1.0 2015年8月14日
	 */
	public int updateSystem(System record);

	/**
	 * 查询系统数量
	 * @author langzi
	 * @return int
	 * @version 1.0 2015年8月14日
	 */
	public int selectSystemCount(System record);

	/**
	 * 查询系统信息
	 * @author langzi
	 * @return System
	 * @version 1.0 2015年8月14日
	 */
	public System selectSystem(System system);

	/**
	 * TODO
	 * 通过容器id查询
	 * @author mayh
	 * @return String
	 * @version 1.0 2015年10月23日
	 */
	String getSystemNameByContainerId(String conId);
	/**
	 * 
	 * @param conId
	 * @return
	 */
	String getSystemCodeByContainerId(String conId);
	/**
	 * TODO
	 * 查询系统部署的系统信息列表
	 * @author mayh
	 * @return List<System>
	 * @version 1.0 2015年11月18日
	 * @param i
	 */
	List<System> getAllSystemDeployed(Integer systemDeployStatus);

	/**
	* TODO
	* @author mayh
	* @return List<String>
	* @version 1.0
	* 2016年2月23日
	*/
	List<System> getSystemNames(String hostId);
	
	/**
	* 根据systemID获得 应用包信息
	* @author liyp
	* @return List<String>
	* @version 1.0
	* 2016年3月9日
	*/
	List<SystemApp> getAppPackageBySysId(String systemId);
	
	/**
	 * 新增系统应用包信息
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月10日
	 */
	public int insertSystemApp(SystemApp systemApp);

	/**
	 * 新增当前物理系统管理员
	 * @author wangjx
	 * @return int
	 * 
	 */
	public int addSystemAdmins(List<Map<String,Object>> list);
	
	/**
	 * 删除当前物理系统管理员
	 * @author wangjx
	 * @param list
	 * @return
	 */
	public int delSystemAdmins(List<Map<String,Object>> list);

	
	/**
	 * 修改系统应用包信息
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月10日
	 */
	public int updateSystemApp(SystemApp systemApp);
	
	
	public SystemApp getAppPackageById(String systemAppId) ;

}