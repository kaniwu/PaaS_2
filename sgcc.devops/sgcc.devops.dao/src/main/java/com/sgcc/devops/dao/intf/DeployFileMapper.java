package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.DeployFile;

/**
 * 部署文件信息数据接口
 */
public interface DeployFileMapper {
	/**
	 * select by delployId
	* TODO
	* @author mayh
	* @return Business
	* @version 1.0
	* 2016年1月26日
	 */
	List<DeployFile> selectByDeployId(String deployId);
	/**
	 * save deploy file
	* TODO
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年1月26日
	 */
	int insert(DeployFile deployFile);
	/**
	 * delete deploy file
	* TODO
	* @author mayh
	* @return int
	* @version 1.0
	* 2016年1月27日
	 */
	int deleteByDeployId(String deployId);
}