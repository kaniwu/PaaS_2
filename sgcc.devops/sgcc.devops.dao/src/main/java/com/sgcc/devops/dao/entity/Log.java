package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 日志信息类
 */
public class Log {
	private String logId;

	private String logObject;

	private String logAction;

	private String logResult;

	private String userId;

	private String userName;

	private String userIp;
	@JsonSerialize(using = DateSerializer.class)
	private Date logCreatetime;

	@JsonSerialize(using = DateSerializer.class)
	private Date beginTime;
	@JsonSerialize(using = DateSerializer.class)
	private Date endTime;
	private String logDetail;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public Log() {
	}

	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public String getLogObject() {
		return logObject;
	}

	public void setLogObject(String logObject) {
		this.logObject = logObject;
	}

	public String getLogAction() {
		return logAction;
	}

	public void setLogAction(String logAction) {
		this.logAction = logAction;
	}

	public String getLogResult() {
		return logResult;
	}

	public void setLogResult(String logResult) {
		this.logResult = logResult;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserIp() {
		return userIp;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public Date getLogCreatetime() {
		return logCreatetime;
	}

	public void setLogCreatetime(Date logCreatetime) {
		this.logCreatetime = logCreatetime;
	}

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getLogDetail() {
		return logDetail;
	}

	public void setLogDetail(String logDetail) {
		this.logDetail = logDetail;
	}

	public Log(String logId, String logObject, String logAction, String logResult, String userId, String userName,
			String userIp, Date logCreatetime, Date beginTime, Date endTime, String logDetail) {
		super();
		this.logId = logId;
		this.logObject = logObject;
		this.logAction = logAction;
		this.logResult = logResult;
		this.userId = userId;
		this.userName = userName;
		this.userIp = userIp;
		this.logCreatetime = logCreatetime;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.logDetail = logDetail;
	}

	public Log(String logId, String logObject, String logAction, String logResult, String userId, String userName,
			String userIp, Date logCreatetime, String logDetail) {
		super();
		this.logId = logId;
		this.logObject = logObject;
		this.logAction = logAction;
		this.logResult = logResult;
		this.userId = userId;
		this.userName = userName;
		this.userIp = userIp;
		this.logCreatetime = logCreatetime;
		this.logDetail = logDetail;
	}

	@Override
	public String toString() {
		return "Log [logId=" + logId + ", logObject=" + logObject + ", logAction=" + logAction + ", logResult="
				+ logResult + ", userId=" + userId + ", userName=" + userName + ", userIp=" + userIp
				+ ", logCreatetime=" + logCreatetime + ", beginTime=" + beginTime + ", endTime=" + endTime
				+ ", logDetail=" + logDetail + "]";
	}

}