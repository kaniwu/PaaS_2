package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 容器信息类
 */
public class Container {
	private String conId;

	private String conUuid;

	private String conImgid;

	private String conCreator;

	private String conName;

	private Byte conPower;

	private Byte conStatus;

	private String conStartCommand;

	private String conStartParam;

	private Integer conCpu;

	private Integer conMem;

	private String conDesc;
	
	private String conEnv;
	
	private String conCommand;
	private String conPorts;

	public String getConEnv() {
		return conEnv;
	}

	public void setConEnv(String conEnv) {
		this.conEnv = conEnv;
	}

	private String sysId;

	private String clusterIp;

	private String clusterPort;

	private String hostId;

	private String conTempName;// 别名

	private Integer conType;
	@JsonSerialize(using = DateSerializer.class)
	private Date conCreatetime;

	public String getConId() {
		return conId;
	}

	public void setConId(String conId) {
		this.conId = conId;
	}

	public String getConUuid() {
		return conUuid;
	}

	public void setConUuid(String conUuid) {
		this.conUuid = conUuid == null ? null : conUuid.trim();
	}

	public String getConImgid() {
		return conImgid;
	}

	public void setConImgid(String conImgid) {
		this.conImgid = conImgid;
	}

	public String getConCreator() {
		return conCreator;
	}

	public void setConCreator(String conCreator) {
		this.conCreator = conCreator;
	}

	public String getConName() {
		return conName;
	}

	public void setConName(String conName) {
		this.conName = conName == null ? null : conName.trim();
	}

	public Byte getConPower() {
		return conPower;
	}

	public void setConPower(Byte conPower) {
		this.conPower = conPower;
	}

	public Byte getConStatus() {
		return conStatus;
	}

	public void setConStatus(Byte conStatus) {
		this.conStatus = conStatus;
	}

	public String getConStartCommand() {
		return conStartCommand;
	}

	public void setConStartCommand(String conStartCommand) {
		this.conStartCommand = conStartCommand == null ? null : conStartCommand.trim();
	}

	public String getConStartParam() {
		return conStartParam;
	}

	public void setConStartParam(String conStartParam) {
		this.conStartParam = conStartParam == null ? null : conStartParam.trim();
	}

	public Integer getConCpu() {
		return conCpu;
	}

	public void setConCpu(Integer conCpu) {
		this.conCpu = conCpu;
	}

	public Integer getConMem() {
		return conMem;
	}

	public void setConMem(Integer conMem) {
		this.conMem = conMem;
	}

	public String getConDesc() {
		return conDesc;
	}

	public void setConDesc(String conDesc) {
		this.conDesc = conDesc == null ? null : conDesc.trim();
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getClusterIp() {
		return clusterIp;
	}

	public void setClusterIp(String clusterIp) {
		this.clusterIp = clusterIp;
	}

	public String getClusterPort() {
		return clusterPort;
	}

	public void setClusterPort(String clusterPort) {
		this.clusterPort = clusterPort;
	}

	public Date getConCreatetime() {
		return conCreatetime;
	}

	public void setConCreatetime(Date conCreatetime) {
		this.conCreatetime = conCreatetime;
	}

	public String getConTempName() {
		return conTempName;
	}

	public void setConTempName(String conTempName) {
		this.conTempName = conTempName;
	}

	public Integer getConType() {
		return conType;
	}

	public void setConType(Integer conType) {
		this.conType = conType;
	}

	public String getConCommand() {
		return conCommand;
	}

	public void setConCommand(String conCommand) {
		this.conCommand = conCommand;
	}

	public String getConPorts() {
		return conPorts;
	}

	public void setConPorts(String conPorts) {
		this.conPorts = conPorts;
	}

}