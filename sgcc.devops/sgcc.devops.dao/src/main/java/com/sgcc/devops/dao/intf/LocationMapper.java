package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Location;

public interface LocationMapper {
    int deleteByPrimaryKey(String id) throws Exception;

    int insertSelective(Location record) throws Exception;

    Location selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(Location record) throws Exception;

	/**
	* TODO
	* @author mayh
	* @return List<ServerRoom>
	* @version 1.0
	* 2016年3月16日
	*/
	List<Location> selectAll();

	/**
	* TODO
	* @author mayh
	* @return List<Location>
	* @version 1.0
	* 2016年4月6日
	*/
	List<Location> selectByParent(String parentId);

	/**
	* TODO
	* @author mayh
	* @return List<Location>
	* @version 1.0
	* 2016年6月16日
	*/
	List<Location> selectByParamCode(String locationParamCode);
}