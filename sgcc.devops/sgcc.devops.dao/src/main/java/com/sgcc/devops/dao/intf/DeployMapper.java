package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Deploy;

/**
 * 部署信息数据接口
 */
public interface DeployMapper {

	/**
	 * TODO
	 * 删除部署信息
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年10月14日
	 */
	int deleteByPrimaryKey(String deployId);

	/**
	 * TODO
	 * 查询部署信息
	 * @author mayh
	 * @return Deploy
	 * @version 1.0 2015年10月14日
	 */
	Deploy selectByPrimaryKey(String deployId);

	/**
	 * TODO
	 * 查询部署信息列表
	 * @author mayh
	 * @return List<Deploy>
	 * @version 1.0 2015年10月14日
	 */
	List<Deploy> selectAll(String userId, int page, int rows);

	/**
	 * TODO
	 * 新增部署信息
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年10月14日
	 */
	int insertSystem(Deploy record);

	/**
	 * TODO
	 * 更新部署信息
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年10月14日
	 */
	int updateByPrimaryKey(Deploy record);

	/**
	 * TODO
	 * 查询系统的部署信息列表
	 * @author mayh
	 * @return Deploy
	 * @version 1.0 2015年10月14日
	 */
	List<Deploy> selectLastBySystemId(String systemId);

	/**
	 * TODO
	 * 查询系统的部署信息列表
	 * @author mayh
	 * @return List<Deploy>
	 * @version 1.0 2015年10月15日
	 */
	List<Deploy> selectAllBySystemId(String systemId);

	/**
	 * TODO
	 * 查询集群的部署信息列表
	 * @author mayh
	 * @return List<Deploy>
	 * @version 1.0 2015年10月22日
	 */
	List<Deploy> getDeployByClusterId(String clusterId);

	/**
	* TODO
	* 查看符合条件的所有部署信息列表
	* @author mayh
	* @return Deploy
	* @version 1.0
	* 2015年12月29日
	*/
	List<Deploy> select(Deploy deploy);

	
	/**
	* 查看符合条件的所有library 引用数量
	* @author liyp
	* @return Deploy
	* @version 1.0
	* 2015年12月29日
	*/
	int selectDeployPrelibCount(String libraryId);

	/**
	* TODO
	* @author mayh
	* @return List<Deploy>
	* @version 1.0
	* 2016年5月26日
	*/
	List<Deploy> getUndeployedByTimerTask(String dialect) throws Exception;

}
