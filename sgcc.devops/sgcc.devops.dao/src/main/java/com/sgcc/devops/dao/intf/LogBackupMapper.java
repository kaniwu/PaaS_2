package com.sgcc.devops.dao.intf;

import com.sgcc.devops.dao.entity.LogBackup;

/**
 * 日志备份信息数据接口
 */
public interface LogBackupMapper {

	/**
	 * 新增日志备份
	 * @author langzi
	 * @param record
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int insertLog(LogBackup record);

	/**
	 * 删除日志
	 * @author langzi
	 * @param logId
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int deleteLog(String logId);

	/**
	 * 更新日志
	 * @author langzi
	 * @param record
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int updateLog(LogBackup record);


	/**
	 * 查询日志备份
	 * @author langzi
	 * @return LogBackup
	 * @version 1.0 2015年8月19日
	 */
	public LogBackup selectLog();

}