package com.sgcc.devops.dao.entity;

/**
 * 系统应用包信息类
 */
public class SystemApp {
	private String id;
	
	private String systemId;
	
	private String appRoute;

	private String appVersion;

	private int status ;
	
	private String appName;
	
	private String appType;
	
	private String appCategory;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getAppRoute() {
		return appRoute;
	}

	public void setAppRoute(String appRoute) {
		this.appRoute = appRoute;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppCategory() {
		return appCategory;
	}

	public void setAppCategory(String appCategory) {
		this.appCategory = appCategory;
	}

	 

}