package com.sgcc.devops.dao.entity;

/**
 * 业务物理系统关联类
 */
public class BusinessSys {
	
	private String Id;
	
	private String businessId;

	private String sysId;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public BusinessSys() {
		super();
	}

	public BusinessSys(String businessId, String sysId) {
		super();
		this.businessId = businessId;
		this.sysId = sysId;
	}
	public BusinessSys(String businessId) {
		super();
		this.businessId = businessId;
	}
}