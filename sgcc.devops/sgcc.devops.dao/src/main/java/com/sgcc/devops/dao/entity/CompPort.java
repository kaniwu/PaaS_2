package com.sgcc.devops.dao.entity;

public class CompPort {
	private String id;
	private String compId;//组件Id,nginx、f5...
	private String portId;//端口Id
	private String usedId;//使用对象Id,部署应用还是仓库负载
	private String usedType;//使用对象类型,部署应用还是仓库负载
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPortId() {
		return portId;
	}
	public void setPortId(String portId) {
		this.portId = portId;
	}
	public String getCompId() {
		return compId;
	}
	public void setCompId(String compId) {
		this.compId = compId;
	}
	public String getUsedId() {
		return usedId;
	}
	public void setUsedId(String usedId) {
		this.usedId = usedId;
	}
	public String getUsedType() {
		return usedType;
	}
	public void setUsedType(String usedType) {
		this.usedType = usedType;
	}
	
	public CompPort() {
		super();
	}
	public CompPort(String compId, String portId, String usedId, String usedType) {
		super();
		this.compId = compId;
		this.portId = portId;
		this.usedId = usedId;
		this.usedType = usedType;
	}
	
}
