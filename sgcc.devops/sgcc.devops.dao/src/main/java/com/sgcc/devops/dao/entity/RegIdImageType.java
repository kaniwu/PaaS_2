package com.sgcc.devops.dao.entity;

/**
 * 仓库镜像信息类
 */
public class RegIdImageType {

	private String registryId;

	private Byte imageStatus;

	private String imageName;
	
	private Byte imageType;
	
	private String tempName;
	
	private String envId;
	
	private String dialect;
	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public Byte getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(Byte imageStatus) {
		this.imageStatus = imageStatus;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public Byte getImageType() {
		return imageType;
	}

	public void setImageType(Byte imageType) {
		this.imageType = imageType;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getEnvId() {
		return envId;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	
}
