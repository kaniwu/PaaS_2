package com.sgcc.devops.dao.entity;

/**
 * 组件扩展类
 */
public class ComponentExpand {
	private String compId;
	
	private String f5CompId;
	
	private Byte autoConfigF5;
	
	private String bigipVersion;
	
	private String f5ExternalIp;
	
	private Integer f5ExternalPort;
	
	private String driverId;
	
	public String getCompId() {
		return compId;
	}

	public void setCompId(String compId) {
		this.compId = compId;
	}

	public String getF5CompId() {
		return f5CompId;
	}

	public void setF5CompId(String f5CompId) {
		this.f5CompId = f5CompId;
	}

	public Byte getAutoConfigF5() {
		return autoConfigF5;
	}

	public void setAutoConfigF5(Byte autoConfigF5) {
		this.autoConfigF5 = autoConfigF5;
	}

	public String getBigipVersion() {
		return bigipVersion;
	}

	public void setBigipVersion(String bigipVersion) {
		this.bigipVersion = bigipVersion;
	}

	public String getF5ExternalIp() {
		return f5ExternalIp;
	}

	public void setF5ExternalIp(String f5ExternalIp) {
		this.f5ExternalIp = f5ExternalIp;
	}

	public Integer getF5ExternalPort() {
		return f5ExternalPort;
	}

	public void setF5ExternalPort(Integer f5ExternalPort) {
		this.f5ExternalPort = f5ExternalPort;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
}
