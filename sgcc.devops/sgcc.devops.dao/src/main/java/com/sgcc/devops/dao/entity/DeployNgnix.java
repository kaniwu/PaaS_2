package com.sgcc.devops.dao.entity;

/**
 * 部署Nginx类
 * 
 * (未使用)
 */
public class DeployNgnix{
	private String id;

	private String deployId;

	private String nginxId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

	public String getNginxId() {
		return nginxId;
	}

	public void setNginxId(String nginxId) {
		this.nginxId = nginxId;
	}

}