package com.sgcc.devops.dao.entity.expand;

import com.sgcc.devops.dao.entity.Container;

/**
 * date：2015年8月27日 上午11:21:03 project name：sgcc-devops-dao
 * 容器扩展类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerExpand.java description：
 */
public class ContainerExpand extends Container {
	private String[] containerArray;

	public String[] getContainerArray() {
		return containerArray;
	}

	public void setContainerArray(String[] containerArray) {
		this.containerArray = containerArray;
	}

}
