package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.HostInstall;


public interface HostComponentMapper {

	List<HostComponent> selectAll();
	
	List<HostComponent> selectComByCon(HostComponent hostComponent);
	
	HostComponent selectByPrimaryKey(String id) throws Exception;
	
	int deleteByPrimaryKey(String id) throws Exception;
	
	HostComponent selectById(String id);
	
	int countName(HostComponent hostComponent);
	
	int insert(HostComponent hostComponent) throws Exception;
	 
	int updateByPrimaryKeySelective(HostComponent hostComponent);
	
	List<HostInstall> selectHostInstallByHostId(String hostId);

	int countHostComp();
}