package com.sgcc.devops.dao.entity;

/**
 * 服务组件类
 */
public class Component {
	/** 组件id */
	private String componentId;
	/** 组件名称 */
	private String componentName;
	/** 组件类型 */
	private Byte componentType;

	/** 组件IP */
	private String componentIp;

	private String componentUserName;

	private String componentPwd;
	/** 组件状态 */
	private Byte componentStatus;
	/** 组件端口 */
	private Integer componentPort;
	
	private Integer componentHostPort;
	/** 组件版本 */
	private String componentVersion;
	/** 说明 */
	private String componentDescription;
	/** 驱动 */
	private String componentDriver;
	/** JdbcUrl */
	private String componentJdbcurl;
	/** 配置文件目录 */
	private String componentConfigDir;
	/** 数据库类型 */
	private Byte componentDBType;
	/**
	 * DNS的zone值
	 */
	private String dnsZone;
	/**
	 * dns的key值
	 */
	private String key;
	/**
	 * 密钥
	 */
	private String secret;
	
	/**
	 * nginx 备份主机ip
	 */
	private String backComponentIp;
	
	/**
	 * 访问虚拟keepliveIp
	 */
	private String keepaliveIp;	
	
	private Byte nginxCategory;
	
	/** 扩展属性：f5组件id*/
	private String f5CompId;
	/** 扩展属性：是否自动配置f5*/
	private Byte autoConfigF5;
	/** 扩展属性：BIGIP版本*/
	private String bigipVersion;
	/** 扩展属性：f5对外ip*/
	private String f5ExternalIp;
	/** 扩展属性：f5对外端口*/
	private Integer f5ExternalPort;
	/** 扩展属性：数据库驱动id*/
	private String driverId;
	/** 扩展属性：初始化sql*/
	private String initSql;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Byte getComponentType() {
		return componentType;
	}

	public void setComponentType(Byte componentType) {
		this.componentType = componentType;
	}

	public String getComponentIp() {
		return componentIp;
	}

	public void setComponentIp(String componentIp) {
		this.componentIp = componentIp;
	}

	public Byte getComponentStatus() {
		return componentStatus;
	}

	public void setComponentStatus(Byte componentStatus) {
		this.componentStatus = componentStatus;
	}

	public String getComponentVersion() {
		return componentVersion;
	}

	public void setComponentVersion(String componentVersion) {
		this.componentVersion = componentVersion == null ? null : componentVersion.trim();
	}

	public String getComponentDescription() {
		return componentDescription;
	}

	public void setComponentDescription(String componentDescription) {
		this.componentDescription = componentDescription;
	}

	public Integer getComponentPort() {
		return componentPort;
	}

	public void setComponentPort(Integer componentPort) {
		this.componentPort = componentPort;
	}

	public String getComponentUserName() {
		return componentUserName;
	}

	public void setComponentUserName(String componentUserName) {
		this.componentUserName = componentUserName;
	}

	public String getComponentPwd() {
		return componentPwd;
	}

	public void setComponentPwd(String componentPwd) {
		this.componentPwd = componentPwd;
	}
	/*
	 * public void setHostKernelVersion(String hostKernelVersion) {
	 * this.hostKernelVersion = hostKernelVersion == null ? null :
	 * hostKernelVersion.trim(); }
	 */

	public String getComponentDriver() {
		return componentDriver;
	}

	public void setComponentDriver(String componentDriver) {
		this.componentDriver = componentDriver;
	}

	public String getComponentJdbcurl() {
		return componentJdbcurl;
	}

	public void setComponentJdbcurl(String componentJdbcurl) {
		this.componentJdbcurl = componentJdbcurl;
	}

	public String getComponentConfigDir() {
		return componentConfigDir;
	}

	public void setComponentConfigDir(String componentConfigDir) {
		this.componentConfigDir = componentConfigDir;
	}

	public Byte getComponentDBType() {
		return componentDBType;
	}

	public void setComponentDBType(Byte componentDBType) {
		this.componentDBType = componentDBType;
	}

	public String getF5CompId() {
		return f5CompId;
	}

	public void setF5CompId(String f5CompId) {
		this.f5CompId = f5CompId;
	}

	public Byte getAutoConfigF5() {
		return autoConfigF5;
	}

	public void setAutoConfigF5(Byte autoConfigF5) {
		this.autoConfigF5 = autoConfigF5;
	}

	public String getBigipVersion() {
		return bigipVersion;
	}

	public void setBigipVersion(String bigipVersion) {
		this.bigipVersion = bigipVersion;
	}

	public String getF5ExternalIp() {
		return f5ExternalIp;
	}

	public void setF5ExternalIp(String f5ExternalIp) {
		this.f5ExternalIp = f5ExternalIp;
	}

	public Integer getF5ExternalPort() {
		return f5ExternalPort;
	}

	public void setF5ExternalPort(Integer f5ExternalPort) {
		this.f5ExternalPort = f5ExternalPort;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public String getInitSql() {
		return initSql;
	}

	public void setInitSql(String initSql) {
		this.initSql = initSql;
	}

	public String getDnsZone() {
		return dnsZone;
	}

	public void setDnsZone(String dnsZone) {
		this.dnsZone = dnsZone;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getBackComponentIp() {
		return backComponentIp;
	}

	public void setBackComponentIp(String backComponentIp) {
		this.backComponentIp = backComponentIp;
	}

	public String getKeepaliveIp() {
		return keepaliveIp;
	}

	public void setKeepaliveIp(String keepaliveIp) {
		this.keepaliveIp = keepaliveIp;
	}

	public Byte getNginxCategory() {
		return nginxCategory;
	}

	public void setNginxCategory(Byte nginxCategory) {
		this.nginxCategory = nginxCategory;
	}

	public Integer getComponentHostPort() {
		return componentHostPort;
	}

	public void setComponentHostPort(Integer componentHostPort) {
		this.componentHostPort = componentHostPort;
	}

}