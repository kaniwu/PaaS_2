package com.sgcc.devops.dao.entity;

/**
 * 服务组件类型类
 */
public class ComponentType {

	/** 组件类型id */
	private String componentTypeId;
	/** 组件类型名称 */
	private String componentTypeName;

	public String getComponentTypeId() {
		return componentTypeId;
	}

	public void setComponentTypeId(String componentTypeId) {
		this.componentTypeId = componentTypeId;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

}
