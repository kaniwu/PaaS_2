package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Environment;



public interface EnvironmentMapper {
	
    //查询环境管理表
	List<Environment> selectEnvironment(Environment record);

	int insert(Environment record);

	int deleteByPrimaryKey(String id);

	int updateByPrimaryKeySelective(Environment environment);

	List<Environment> selectEnvironments(Environment environment);

	/**
	* TODO
	* @author mayh
	* @return Environment
	* @version 1.0
	* 2016年8月2日
	*/
	Environment selectByPirmaryKey(String eId);

}