package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.HostInstall;


public interface HostIntallMapper {
	
	public int insert(HostInstall hostInstall);
	public List<HostInstall> selectAll(HostInstall hostInstall);
	public int delete(String id);
}