/**
 * 
 */
package com.sgcc.devops.dao.entity;

/**  
 * date：2016年4月14日 下午6:33:28
 * project name：sgcc.devops.dao
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：HostInstall.java
 * description：  
 */
public class HostInstall {
	private String id;
	private String hostId;
	private String hostComId;
	private String type;
	private String version;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getHostId() {
		return hostId;
	}
	public void setHostId(String hostId) {
		this.hostId = hostId;
	}
	public String getHostComId() {
		return hostComId;
	}
	public void setHostComId(String hostComId) {
		this.hostComId = hostComId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
