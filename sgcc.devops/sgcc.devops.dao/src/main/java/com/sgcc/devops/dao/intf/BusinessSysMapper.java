/**
 * 
 */
package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.BusinessSys;

/**  
 * date：2015年12月30日 上午11:38:33
 * project name：sgcc.devops.dao
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：BusinessSysMapper.java
 * description：  
 */
public interface BusinessSysMapper {
	/**
	 * TODO
	 * 查询业务物理关心信息
	 * 根据业务id查询业务信息
	 * @return List<BusinessSys>
	 * @version 1.0
	 */
	public List<BusinessSys> load(BusinessSys businessSys);
}
