package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Image;

/**
 * @author luogan 2015年8月29日 下午4:04:25
 * 镜像信息数据接口
 */
public interface ImageMapper {

	/**
	 * 新增镜像信息
	 * @author luogan
	 * @param image
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int insertImage(Image record);

	/**
	 * 删除镜像信息
	 * @author luogan
	 * @param image
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int deleteImage(String imageId);

	/**
	 * 更新镜像信息
	 * @author luogan
	 * @param image
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int updateImage(Image record);

	/**
	 * 查询镜像信息
	 * @author luogan
	 * @param image
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public Image selectByPrimaryKey(String imageId);

	/**
	 * 查询符合条件的镜像列表
	 * @author luogan
	 * @param image
	 * @return List<Image>
	 * @version 1.0 2015年8月28日
	 */
	public List<Image> selectAllImage(Image image);

	/**
	 * 查询部署的镜像类别
	 * @author luogan
	 * @param image
	 * @return List<Image>
	 * @version 1.0 2015年8月28日
	 */
	public List<Image> selectImagesBydeployId(String appId);

	/**
	 * TODO
	 * 查询镜像数量
	 * @author zhangxin
	 * @return int
	 * @version 1.0 2015年11月20日
	 */
	public int selectImageCount(Image image);

	/**
	 * TODO
	 * 查询镜像信息
	 * @author zhangxin
	 * @return Image
	 * @version 1.0 2015年11月27日
	 */
	public Image selectImage(Image image);
}