package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ComponentHost;
import com.sgcc.devops.dao.entity.Host;

/**
 * @author luogan 2015年8月29日 下午4:03:44
 * 主机信息数据接口
 */
public interface HostMapper {

	/**
	 * 新增主机
	 * @author luogan
	 * @param host
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int insertHost(Host record);

	/**
	 * 删除主机
	 * @author luogan
	 * @param hostId
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int deleteHost(String hostId);

	/**
	 * 批量删除主机
	 * @author luogan
	 * @param list 主机id集合
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int deleteHosts(List<String> hostIds);

	/**
	 * 更新主机
	 * @author luogan
	 * @param host
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int updateHost(Host record);

	/**
	 * @author luogan
	 * @param host
	 * @return select host
	 * @version 1.0 2015年8月28日
	 */
	public Host selectHost(Host record);

	/**
	 * 根据主机ID获取主机信息
	 * 
	 * @param hostId
	 *            主机ID
	 * @return Host
	 */
	public Host loadHost(String hostId);

	/**
	 * 通过IP查询主机
	 * @author langzi
	 * @param hostIp
	 * @return Host
	 * @version 1.0 2015年9月11日
	 */
	public Host selectHostByIp(String hostIp);

	/**
	 * 查询符合条件的主机列表
	 * @author luogan
	 * @param host
	 * @return List<Host>
	 * @version 1.0 2015年8月28日
	 */
	public List<Host> selectAllHost(Host host);

	/**
	 * 查询主机通过类型
	 * @author luogan
	 * @param host
	 * @return List<Host>
	 * @version 1.0 2015年8月28日
	 */
	public List<Host> selectHostList(int type);

	/**
	 * 通过集群类型查询列表
	 * @author luogan
	 * @param host
	 * @return List<Host>
	 * @version 1.0 2015年8月28日
	 */
	public List<Host> selectHostListByCluster(int type);

	/**
	 * 集群查询主机列表
	 * @author luogan
	 * @param host
	 * @return List<Host>
	 * @version 1.0 2015年8月28日
	 */
	public List<Host> selectHostListByClusterId(String clusterId);

	/**
	 * TODO
	 * 自由的主机列表
	 * @author mayh
	 * @return List<Host>
	 * @version 1.0 2015年10月12日
	 */
	public List<Host> freeHostList(Host host);

	/**
	 * TODO
	 * 查询符合条件的主机列表
	 * @author zhangxin
	 * @return List<Host>
	 * @version 1.0 2015年10月19日
	 */
	public List<Host> selectHostListByHost(Host host);

	/**
	 * TODO
	 * 查询主机的数量
	 * @author zhangxin
	 * @return int
	 * @version 1.0 2015年11月20日
	 */
	public int selectHostCount(Host host);

	/**
	* TODO所有在线主机，排除掉删除和离线
	* @author mayh
	* @return List<Host>
	* @version 1.0
	* 2016年3月8日
	*/
	public List<Host> allOnlineHost();
	
	/**
	* 查询某组件关联的主机信息列表
	* @author yueyong
	* @return List<ComponentHost>
	* @version 1.0
	* 2016年3月8日
	*/
	public List<ComponentHost> selectComponentHost(String componentId);
	
	/**
	 * 删除组件主机信息
	 * @author yueyong
	 * @param hostId
	 * @return int
	 * @version 1.0 2015年8月28日
	 */
	public int deleteCompHost(String id);
	
	/**
	 * 新增组件主机信息
	 * @author yueyong
	 * @time 2016年3月10日下午9:31:52
	 * @description
	 */
	public int insertComponentHost(ComponentHost componentHost);
	
	/**
	 * 更新组件主机信息
	 * @author yueyong
	 * @time 2016年3月10日下午9:31:52
	 * @description
	 */
	public int updateComponentHost(ComponentHost componentHost);

	/**
	* TODO
	* @author mayh
	* @return List<Host>
	* @version 1.0
	* 2016年3月16日
	*/
	public List<Host> hostListByRack(Host host);
}