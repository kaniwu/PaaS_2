package com.sgcc.devops.dao.entity;

public class ServerRoom {
    private String id;

    private String serverRoomName;

    private String serverRoomRemark;

    private String creator;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getServerRoomName() {
		return serverRoomName;
	}

	public void setServerRoomName(String serverRoomName) {
		this.serverRoomName = serverRoomName;
	}

	public String getServerRoomRemark() {
		return serverRoomRemark;
	}

	public void setServerRoomRemark(String serverRoomRemark) {
		this.serverRoomRemark = serverRoomRemark;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

}