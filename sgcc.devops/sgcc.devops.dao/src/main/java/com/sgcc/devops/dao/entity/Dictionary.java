/**
 * 
 */
package com.sgcc.devops.dao.entity;

/**
 * date：2015年11月13日 上午11:17:16 project name：sgcc.devops.dao
 * 数据字典类
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：Dictionary.java description：
 */
public class Dictionary {
	private String id;
	private String dKey;
	private String dValue;
	private Byte type;
	private Byte status;
	private String desc;

	public Dictionary() {
	}

	public Dictionary(String id, String dKey, String dValue, Byte type, Byte status, String desc) {
		super();
		this.id = id;
		this.dKey = dKey;
		this.dValue = dValue;
		this.type = type;
		this.status = status;
		this.desc = desc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getdKey() {
		return dKey;
	}

	public void setdKey(String dKey) {
		this.dKey = dKey;
	}

	public String getdValue() {
		return dValue;
	}

	public void setdValue(String dValue) {
		this.dValue = dValue;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
