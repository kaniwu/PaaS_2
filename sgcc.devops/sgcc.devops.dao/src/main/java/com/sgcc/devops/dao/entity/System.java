package com.sgcc.devops.dao.entity;

/**
 * 系统信息类
 */
public class System {
	private String systemId;
	
	private String businessId;
	
	private String systemName;
	
	private String SystemCode;

	private String systemVersion;

	private Byte systemDeployStatus;

	private Byte systemElsasticityStatus;

	private String deployId;

	private String systemDescribe;

	private Byte operation;
	
	// 三种状态 1:导入, 2:手动创建, 3:隔离
	private Integer systemType;
	
	private Boolean isAllAuth;//查询条件：是否所有系统权限
	
	private String userId;//查询条件 用户id

	private String envId;//所属环境ID
	private String dialect;
	
	private String supporter;//负责人
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public Integer getSystemType() {
		return systemType;
	}

	public void setSystemType(Integer systemType) {
		this.systemType = systemType;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	public Byte getSystemDeployStatus() {
		return systemDeployStatus;
	}

	public void setSystemDeployStatus(Byte systemDeployStatus) {
		this.systemDeployStatus = systemDeployStatus;
	}

	public Byte getSystemElsasticityStatus() {
		return systemElsasticityStatus;
	}

	public void setSystemElsasticityStatus(Byte systemElsasticityStatus) {
		this.systemElsasticityStatus = systemElsasticityStatus;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

	public String getSystemDescribe() {
		return systemDescribe;
	}

	public void setSystemDescribe(String systemDescribe) {
		this.systemDescribe = systemDescribe;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public Byte getOperation() {
		return operation;
	}

	public void setOperation(Byte operation) {
		this.operation = operation;
	}

	public Boolean getIsAllAuth() {
		return isAllAuth;
	}

	public void setIsAllAuth(Boolean isAllAuth) {
		this.isAllAuth = isAllAuth;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEnvId() {
		return envId;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getSupporter() {
		return supporter;
	}

	public void setSupporter(String supporter) {
		this.supporter = supporter;
	}

	public String getSystemCode() {
		return SystemCode;
	}

	public void setSystemCode(String systemCode) {
		SystemCode = systemCode;
	}

}