package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Library;

/**
 * 库文件数据接口
 */
public interface LibraryMapper {

	/**
	 * 查询符合条件的库文件信息列表
	 * @author liyp
	 * @return List<Library>
	 * @version 1.0 2016年3月14日
	 */
	public List<Library> getLibraryList(Library record);

	/**
	 * 新增库文件信息
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月14日
	 */
	public int insertLibrary(Library record);
	
	/**
	 * 更新库文件信息
	 * @author liyp
	 * @return int
	 * @version 1.0 2016年3月14日
	 */
	public int updateLibrary(Library record);
	
	/**
	 * 删除库文件信息
	 * @author panjing
	 * @return int
	 
	 */
	public int deleteLibrary(String id);

}