package com.sgcc.devops.dao.intf;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.sgcc.devops.dao.entity.Monitor;

/**
 * 监控信息数据接口
 */
public interface MonitorMapper {

	/**
	 * 新增监控
	 * @return int
	 * @version 1.0
	 */
	public int insert(Monitor monitor);

	/**
	 * 删除监控
	 * @return int
	 * @version 1.0
	 */
	public int deleteByPrimaryKey(String monitorId);

	/**
	 * 通过类型删除监控
	 * @return int
	 * @version 1.0
	 */
	public int deleteByType(byte targetType);
	
	/**
	 * 通过类型删除监控
	 * @return int
	 * @version 1.0
	 */
	public int deleteBytargetId(String targetId);

	/**
	 * 查询监控
	 * @return Monitor
	 * @version 1.0
	 */
	public Monitor selectByPrimaryKey(String monitorId);

	/**
	 * 通过类型查询列表
	 * @return List<Monitor>
	 * @version 1.0
	 */
	public List<Monitor> selectByType(byte targetType);

	/**
	 * 通过target id查询列表
	 * @return List<Monitor>
	 * @version 1.0
	 */
	public List<Monitor> selectBytargetId(String targetId);

	/**
	 * 通过时间查询列表
	 * @return List<Monitor>
	 * @version 1.0
	 */
	public List<Monitor> selectByTime(Map<String, Timestamp> time);

	/**
	 * 查询列表
	 * @return List<Monitor>
	 * @version 1.0
	 */
	public List<Monitor> select(Monitor monitor);

	/** 查询时间最近的数据，type targetId time */
	public List<Monitor> selectByTypeAndId(Monitor monitor);
	
	/**
	 * 监控信息备份
	 * @return int
	 * @version 1.0
	 */
	public int dataTransfer(Date monitorTime);

	/**
	* TODO
	* @author mayh
	* @return List<Monitor>
	* @version 1.0
	* 2016年6月20日
	*/
	public Monitor getAvgData(Monitor monitor);
}
