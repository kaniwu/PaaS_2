package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

public class RegistryLb {
	private String id;

	private String registryName;

	private String registryDesc;
	
	private String componentId;
	
	private Byte LbType;
	
	private String domainName;
	
	private Integer LbPort;
	
	private String LbIp;
	
	private String LbHostId;
	
	private Byte status;
	
	@JsonSerialize(using = DateSerializer.class)
	private Date registryCreatetime;
	
	private String registryCreator;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRegistryName() {
		return registryName;
	}

	public void setRegistryName(String registryName) {
		this.registryName = registryName;
	}

	public String getRegistryDesc() {
		return registryDesc;
	}

	public void setRegistryDesc(String registryDesc) {
		this.registryDesc = registryDesc;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public Byte getLbType() {
		return LbType;
	}

	public void setLbType(Byte lbType) {
		LbType = lbType;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	public Integer getLbPort() {
		return LbPort;
	}

	public void setLbPort(Integer lbPort) {
		LbPort = lbPort;
	}

	public String getLbIp() {
		return LbIp;
	}

	public void setLbIp(String lbIp) {
		LbIp = lbIp;
	}

	public String getLbHostId() {
		return LbHostId;
	}

	public void setLbHostId(String lbHostId) {
		LbHostId = lbHostId;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Date getRegistryCreatetime() {
		return registryCreatetime;
	}

	public void setRegistryCreatetime(Date registryCreatetime) {
		this.registryCreatetime = registryCreatetime;
	}

	public String getRegistryCreator() {
		return registryCreator;
	}

	public void setRegistryCreator(String registryCreator) {
		this.registryCreator = registryCreator;
	}
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	
}
