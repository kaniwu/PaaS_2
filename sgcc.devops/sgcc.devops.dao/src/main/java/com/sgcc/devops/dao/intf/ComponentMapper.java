package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Component;

/**
 * 服务组件信息数据接口
 */
public interface ComponentMapper {

	/**
	 * 新增组件
	 * @param component
	 * @return insert component
	 * @version 1.0
	 */
	public int insertComponent(Component record);

	/**
	 * 删除组件
	 * @param componentId
	 * @return delete component
	 * @version 1.0
	 */
	public int deleteComponent(String componentId);

	/**
	 * 更新组件
	 * @param component
	 * @return update component
	 * @version 1.0
	 */
	public int updateComponent(Component record);

	/**
	 * 查询符合条件的所有组件
	 * @param component
	 * @return List<Component>
	 * @version 1.0
	 */
	public List<Component> selectAllComponent(Component component);

	/**
	 * 查询某个类型的所有组件
	 * @param type
	 * @return List<Component>
	 * @version 1.0
	 */
	public List<Component> selectComponentList(int type);

	/**
	 * 查询组件的数量
	 * @param type
	 * @return Integer
	 * @version 1.0
	 */
	public Integer selectComponentCount(Component record);

	/**
	 * 查询组件信息
	 * @param type
	 * @return Component
	 * @version 1.0
	 */
	public Component selectComponent(Component component);
}