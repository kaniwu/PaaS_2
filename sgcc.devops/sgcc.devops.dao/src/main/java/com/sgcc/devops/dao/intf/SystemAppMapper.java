package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.SystemApp;

/**
 * SystemApp查询接口
 */
public interface SystemAppMapper {

	
	public List<SystemApp> selectSystemApps(String systemId);
	
	public SystemApp selectByPrimaryKey(String id);

}