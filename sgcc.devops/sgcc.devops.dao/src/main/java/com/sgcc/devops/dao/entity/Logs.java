package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 运行日志类
 */
public class Logs {
	private String nodeId;

	private String Logger;

	@JsonSerialize(using = DateSerializer.class)
	private Date dated;

	private String level;

	private String message;

	@JsonSerialize(using = DateSerializer.class)
	private Date beginTime;
	@JsonSerialize(using = DateSerializer.class)
	private Date endTime;

	public Date getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getNodeId() {
		return nodeId;
	}

	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	public String getLogger() {
		return Logger;
	}

	public void setLogger(String logger) {
		Logger = logger;
	}

	public Date getDated() {
		return dated;
	}

	public void setDated(Date dated) {
		this.dated = dated;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
