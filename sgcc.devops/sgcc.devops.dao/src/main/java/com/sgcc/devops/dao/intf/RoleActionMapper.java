package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.RoleAction;


public interface RoleActionMapper {
	
	public List<RoleAction> selectAll(RoleAction roleAction) throws Exception;
	
	public int deleteByPrimaryKey(String id) throws Exception;

	public int deleteByRoleId(String roleId) throws Exception;
	
	public int insert(RoleAction record) throws Exception;

	public int insertSelective(RoleAction record) throws Exception;

	public RoleAction selectByPrimaryKey(String id) throws Exception;

	public int updateByPrimaryKeySelective(RoleAction record) throws Exception;

	public int updateByPrimaryKey(RoleAction record) throws Exception;
}