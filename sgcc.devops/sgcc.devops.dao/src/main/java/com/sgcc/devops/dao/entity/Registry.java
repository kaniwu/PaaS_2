package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 仓库信息类
 */
public class Registry {
	private String registryId;

	private String registryName;

	private Integer registryPort;

	private Byte registryStatus;

	private String hostId;

	private String registryDesc;

	@JsonSerialize(using = DateSerializer.class)
	private Date registryCreatetime;

	private String registryCreator;
	
	private String lbId;
	
	private String registryDns;

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public String getRegistryName() {
		return registryName;
	}

	public void setRegistryName(String registryName) {
		this.registryName = registryName == null ? null : registryName.trim();
	}

	public Integer getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(Integer registryPort) {
		this.registryPort = registryPort;
	}

	public Byte getRegistryStatus() {
		return registryStatus;
	}

	public void setRegistryStatus(Byte registryStatus) {
		this.registryStatus = registryStatus;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getRegistryDesc() {
		return registryDesc;
	}

	public void setRegistryDesc(String registryDesc) {
		this.registryDesc = registryDesc == null ? null : registryDesc.trim();
	}

	public Date getRegistryCreatetime() {
		return registryCreatetime;
	}

	public void setRegistryCreatetime(Date registryCreatetime) {
		this.registryCreatetime = registryCreatetime;
	}

	public String getRegistryCreator() {
		return registryCreator;
	}

	public void setRegistryCreator(String registryCreator) {
		this.registryCreator = registryCreator;
	}

	public String getLbId() {
		return lbId;
	}

	public void setLbId(String lbId) {
		this.lbId = lbId;
	}

	public String getRegistryDns() {
		return registryDns;
	}

	public void setRegistryDns(String registryDns) {
		this.registryDns = registryDns;
	}
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
}