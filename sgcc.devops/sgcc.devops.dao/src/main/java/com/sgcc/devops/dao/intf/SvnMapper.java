package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Svn;

public interface SvnMapper {

	/**
	 * 新增SVN
	 * @param Svn
	 * @return insert insertSvn
	 * @version 1.0
	 */
	public int insertSvn(Svn svn);
	/**
	 * 更新SVN
	 * @param Svn
	 * @return  updateSvn
	 * @version 1.0
	 */
	public int updateSvn(Svn svn);
	public int deleteSvn(String id);
	
	public List<Svn> getSvnList();
}