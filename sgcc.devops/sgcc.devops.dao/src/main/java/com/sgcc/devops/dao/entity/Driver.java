/**
 * 
 */
package com.sgcc.devops.dao.entity;

/**
 * date：2015年11月13日 上午11:17:16 project name：sgcc.devops.dao
 * 驱动类
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：Dictionary.java description：
 */
public class Driver {
	private String id;
	private String driverName;
	private String driverVersion;
	private String driverPath;
	private Byte DBType;
	private String driverDesc;

	public Driver() {
	}

	public Driver(String id, String driverName, String driverVersion, String driverPath, Byte dBType,
			String driverDesc) {
		super();
		this.id = id;
		this.driverName = driverName;
		this.driverVersion = driverVersion;
		this.driverPath = driverPath;
		DBType = dBType;
		this.driverDesc = driverDesc;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getDriverVersion() {
		return driverVersion;
	}

	public void setDriverVersion(String driverVersion) {
		this.driverVersion = driverVersion;
	}

	public String getDriverPath() {
		return driverPath;
	}

	public void setDriverPath(String driverPath) {
		this.driverPath = driverPath;
	}

	public Byte getDBType() {
		return DBType;
	}

	public void setDBType(Byte dBType) {
		DBType = dBType;
	}

	public String getDriverDesc() {
		return driverDesc;
	}

	public void setDriverDesc(String driverDesc) {
		this.driverDesc = driverDesc;
	}

}
