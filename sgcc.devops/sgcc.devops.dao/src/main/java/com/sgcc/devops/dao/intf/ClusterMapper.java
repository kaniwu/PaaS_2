package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Cluster;
import com.sgcc.devops.dao.entity.ClusterWithIPAndUser;

/**
 * @author luogan 2015年8月29日 下午4:03:22
 * 集群信息数据接口
 */
public interface ClusterMapper {

	/**
	 * 新增集群
	 * @author luogan
	 * @param cluster
	 * @return create cluster
	 * @version 1.0 2015年8月28日
	 */
	public int insertCluster(Cluster record);

	/**
	 * 删除集群
	 * @author luogan
	 * @param cluster
	 * @return delete cluster
	 * @version 1.0 2015年8月28日
	 */
	public int deleteCluster(String clusterId);

	/**
	 * 批量删除集群
	 * @author luogan
	 * @param cluster
	 * @return delete cluster
	 * @version 1.0 2015年8月28日
	 */
	public int deleteClusters(List<String> clusterIds);

	/**
	 * 更新集群
	 * @author luogan
	 * @param cluster
	 * @return update cluster
	 * @version 1.0 2015年8月28日
	 */
	public int updateCluster(Cluster record);

	/**
	 * 查询集群
	 * @author luogan
	 * @param cluster
	 * @return select cluster
	 * @version 1.0 2015年8月28日
	 */
	public Cluster selectCluster(String clusterId);

	/**
	 * 查询集群列表
	 * @author luogan
	 * @param cluster
	 * @return create clusterList
	 * @version 1.0 2015年8月28日
	 */
	public List<ClusterWithIPAndUser> selectAllClusterWithIPAndUser(ClusterWithIPAndUser record);

	/**
	 * 查询符合条件的集群
	 * @author luogan
	 * @param cluster
	 * @return create clusterList
	 * @version 1.0 2015年8月28日
	 */
	public List<Cluster> selectAllCluster(Cluster record);

	/**
	 * 通过hostID查询集群
	 * @author luogan
	 * @param cluster
	 * @return create cluster
	 * @version 1.0 2015年8月28日
	 */
	public Cluster selectClusterByHost(String hostId);

	/**
	 * 列出当前主机下的所有集群
	 * @author luogan
	 * @param cluster
	 * @return create clusterList
	 * @version 1.0 2015年8月28日
	 */
	public List<Cluster> selectClustersByhostId(String hostId);

	/**
	 * 查询集群
	 * @author zhangxin
	 * @param cluster
	 * @return Cluster
	 * @version 1.0 2015年8月28日
	 */
	public Cluster selectClusterByName(Cluster record);

	/**
	 * 查询集群数量
	 * @author zhangxin
	 * @param cluster
	 * @return 集群数量
	 * @version 1.0 2015年8月28日
	 */
	public int selectClusterCount();

}