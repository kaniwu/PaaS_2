package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Parameter;


public interface ParameterMapper {
	
    int deleteByPrimaryKey(String id) throws Exception;

    int insert(Parameter record) throws Exception;

    Parameter selectByPrimaryKey(String id) throws Exception;

    int updateByPrimaryKeySelective(Parameter record) throws Exception;

    int updateByPrimaryKey(Parameter record) throws Exception;

    List<Parameter> selectAll();
    
    /**
	 * 查询符合条件的字典列表
	 * @author panjing
	 * @param record
	 * @return List<Parameter>
	 */
    List<Parameter> selectParameter(Parameter record);
    
    /**
	 * 查询字典
	 * @author panjing
	 */
	Parameter selectParameterByCode(Parameter record);

	List<Parameter> selectParent(Parameter parameter);

}