package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ComponentExpand;

/**
 * @author yueyong 2016年3月8日 上午10:03:44
 * 组件扩展数据接口
 */
public interface ComponentExpandMapper {
	
	/**
	 * 新增组件F5(是否自动配置F5)
	 * @param componentExpand
	 * @return insert componentExpand
	 * @version 1.0
	 */
	public int insertComponentExpand(ComponentExpand componentExpand);
	
	
	/**
	 * 更新组件F5(是否自动配置F5)
	 * @param componentExpand
	 * @return update componentExpand
	 * @version 1.0
	 */
	public int updateComponentExpand(ComponentExpand componentExpand);
	
	/**
	 * 删除某组件的扩展信息
	 * @param componentId
	 * @return delete componentExpand
	 * @version 1.0
	 */
	public int deleteComponentExpand(String componentId);
	/**
	 * 根据componentid查询对应的expand表
	 * @param CompId  componentid
	 * @return
	 */
	public ComponentExpand selectByCompId(String CompId);
	/**
	 * 批量查询
	* TODO
	* @author mayh
	* @return List<ComponentExpand>
	* @version 1.0
	* 2016年8月4日
	 */
	public List<ComponentExpand> select(ComponentExpand componentExpand);
}
