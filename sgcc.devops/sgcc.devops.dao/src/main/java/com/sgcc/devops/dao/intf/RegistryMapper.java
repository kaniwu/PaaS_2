package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.RegIdImageType;
import com.sgcc.devops.dao.entity.Registry;
import com.sgcc.devops.dao.entity.RegistrySlaveImage;
import com.sgcc.devops.dao.entity.RegistryWithIP;

/**
 * date：2015年8月19日 下午4:43:06 project name：sgcc-devops-dao
 * 仓库信息数据接口
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegistryMapper.java description：
 */
public interface RegistryMapper {
	/**
	 * TODO
	 * 删除仓库
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int deleteRegistry(String registryId);

	/**
	 * TODO
	 * 新增仓库
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int insertRegistry(Registry record);

	/**
	 * TODO
	 * 查询仓库列表
	 * @author mayh
	 * @return List<Registry>
	 * @version 1.0 2015年8月19日
	 */
	public List<Registry> selectAll(Registry record);

	/**
	 * TODO
	 * 通过主机查询仓库列表
	 * @author mayh
	 * @return List<Registry>
	 * @version 1.0 2015年8月19日
	 */
	public List<Registry> selectRegistryByHostId(String hostId);

	/**
	 * TODO
	 * 更新仓库
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月19日
	 */
	public int updateRegistry(Registry record);

	/**
	 * TODO
	 * 查询仓库
	 * @author mayh
	 * @return Registry
	 * @version 1.0 2015年8月27日
	 */

	public Registry selectRegistry(Registry record);
	/**
	 * TODO
	 * 修改主机状态
	 * @author mayh
	 * @return int
	 * @version 1.0 2015年8月27日
	 */
	public int changeStatus(List<String> list);

	/**
	 * TODO
	 * 按仓库ID和镜像的类型查询仓库下所有的镜像列表
	 * @author yangqinglin
	 * @return List<Image>
	 * @description  2015年8月27日
	 */
	public List<RegistrySlaveImage> selectAllImageInRegistry(RegIdImageType regIdImageType);
	/**
	 * TODO
	 * 仓库镜像数量
	 * @author yangqinglin
	 * @return int
	 * @description  2015年8月27日
	 */
	public int selectCountImageInRegistry(RegIdImageType regIdImageType);

	/**
	 * TODO
	 * 仓库数量
	 * @author yangqinglin
	 * @return int
	 * @description  2015年8月27日
	 */
	public int registryCount();

	/**
	* TODO
	* @author mayh
	* @return List<Registry>
	* @version 1.0
	* 2016年4月13日
	*/
	public List<Registry> selectAll();

	public List<Registry> selectRegistryByLbId(Registry record);
}