package com.sgcc.devops.dao.entity;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/** 
 * 弹性伸缩策略类
 */
public class ElasticStrategy {

	private String id;
	private String systemId;
	private Integer maxNode;
	private Integer minNode;
	@JsonSerialize(using = DateSerializer.class)
	private Date gmtCreate;
	@JsonSerialize(using = DateSerializer.class)
	private Date gmtModify;
	private String creator;
	private String elasticStrategy;
	private Integer stepSize;
	private String conflictStrategy;
	private String extendDuration;
	private String extendIntervals;
	private String contractionDuration;
	private String contractionIntervals;
	
	private List<ElasticItem> items;

	public List<ElasticItem> getItems() {
		return items;
	}

	public void setItems(List<ElasticItem> items) {
		this.items = items;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public Integer getMaxNode() {
		return maxNode;
	}

	public void setMaxNode(Integer maxNode) {
		this.maxNode = maxNode;
	}

	public Integer getMinNode() {
		return minNode;
	}

	public void setMinNode(Integer minNode) {
		this.minNode = minNode;
	}

	public Date getGmtCreate() {
		return gmtCreate;
	}

	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}

	public Date getGmtModify() {
		return gmtModify;
	}

	public void setGmtModify(Date gmtModify) {
		this.gmtModify = gmtModify;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getElasticStrategy() {
		return elasticStrategy;
	}

	public void setElasticStrategy(String elasticStrategy) {
		this.elasticStrategy = elasticStrategy;
	}

	public Integer getStepSize() {
		return stepSize;
	}

	public void setStepSize(Integer stepSize) {
		this.stepSize = stepSize;
	}

	public String getConflictStrategy() {
		return conflictStrategy;
	}

	public void setConflictStrategy(String conflictStrategy) {
		this.conflictStrategy = conflictStrategy;
	}

	public String getExtendDuration() {
		return extendDuration;
	}

	public void setExtendDuration(String extendDuration) {
		this.extendDuration = extendDuration;
	}

	public String getExtendIntervals() {
		return extendIntervals;
	}

	public void setExtendIntervals(String extendIntervals) {
		this.extendIntervals = extendIntervals;
	}

	public String getContractionDuration() {
		return contractionDuration;
	}

	public void setContractionDuration(String contractionDuration) {
		this.contractionDuration = contractionDuration;
	}

	public String getContractionIntervals() {
		return contractionIntervals;
	}

	public void setContractionIntervals(String contractionIntervals) {
		this.contractionIntervals = contractionIntervals;
	}

}
