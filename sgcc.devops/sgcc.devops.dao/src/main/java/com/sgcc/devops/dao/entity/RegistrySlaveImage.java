package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 镜像系统部署类
 */
public class RegistrySlaveImage {

	private String imageId;

	private String imageUuid;

	private Byte imageStatus;

	private String imageName;

	private String imageTag;

	private Integer imageSize;

	private String imageDesc;

	private String appId;

	private String deployId;

	private String systemName;

	private String appName;

	private String imagePort;
	@JsonSerialize(using = DateSerializer.class)
	private Date imageCreatetime;

	private String imageCreator;

	private String tempName;// image别名

	private Byte imageType;
	
	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageUuid() {
		return imageUuid;
	}

	public void setImageUuid(String imageUuid) {
		this.imageUuid = imageUuid == null ? null : imageUuid.trim();
	}

	public Byte getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(Byte imageStatus) {
		this.imageStatus = imageStatus;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName == null ? null : imageName.trim();
	}

	public String getImageTag() {
		return imageTag;
	}

	public void setImageTag(String imageTag) {
		this.imageTag = imageTag == null ? null : imageTag.trim();
	}

	public Integer getImageSize() {
		return imageSize;
	}

	public void setImageSize(Integer imageSize) {
		this.imageSize = imageSize;
	}

	public String getImageDesc() {
		return imageDesc;
	}

	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc == null ? null : imageDesc.trim();
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getImagePort() {
		return imagePort;
	}

	public void setImagePort(String imagePort) {
		this.imagePort = imagePort == null ? null : imagePort.trim();
	}

	public Date getImageCreatetime() {
		return imageCreatetime;
	}

	public void setImageCreatetime(Date imageCreatetime) {
		this.imageCreatetime = imageCreatetime;
	}

	public String getImageCreator() {
		return imageCreator;
	}

	public void setImageCreator(String imageCreator) {
		this.imageCreator = imageCreator;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public Byte getImageType() {
		return imageType;
	}

	public void setImageType(Byte imageType) {
		this.imageType = imageType;
	}

}
