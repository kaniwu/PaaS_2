/**
 * 
 */
package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.Dictionary;

/**
 * date：2015年11月13日 上午11:22:30 project name：sgcc.devops.dao
 * 数据字典信息数据接口
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：DictionaryMapper.java description：
 */
public interface DictionaryMapper {
	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Dictionary>
	 * @version 1.0 2015年11月13日
	 */
	public List<Dictionary> selectAll();

	public Dictionary selectbyKey(String dKey);

	public int deleteDictionaryById(String id);

	public int insert(Dictionary dictionary);

	public int update(Dictionary dictionary);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Dictionary>
	 * @version 1.0 2015年11月13日
	 */
	public List<Dictionary> selectAllIndex(int type);

	/**
	 * TODO
	 * 
	 * @author mayh
	 * @return List<Dictionary>
	 * @version 1.0 2015年11月20日
	 */
	public List<Dictionary> selectAllbyKey(String string);
}
