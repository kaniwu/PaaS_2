package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.ComponentType;

/**
 * 服务组件类型数据接口
 */
public interface ComponentTypeMapper {

	/**
	 * 新增组件类型
	 * @param componentType
	 * @return insert componentType
	 * @version 1.0
	 */
	public int insertComponentType(ComponentType record);

	/**
	 * 删除组件类型
	 * @param componentTypeId
	 * @return delete componentType
	 * @version 1.0
	 */
	public int deleteComponentType(String componentTypeId);

	/**
	 * 更新组件类型
	 * @param componentType
	 * @return update componentType
	 * @version 1.0
	 */
	public int updateComponentType(ComponentType record);

	/**
	 * 查询组件类型信息
	 * @param componentTypeId
	 * @return ComponentType
	 * @version 1.0
	 */
	public ComponentType selectComponentType(String componentTypeId);

	/**
	 * 查询所有组件类型信息
	 * @param userId,page,rows,componentType
	 * @return List<ComponentType>
	 * @version 1.0
	 */
	public List<ComponentType> selectAllComponentType(String userId, int page, int rows, ComponentType componentType);

	// public List<CompRelation> selectCompRelationList(int type);
}
