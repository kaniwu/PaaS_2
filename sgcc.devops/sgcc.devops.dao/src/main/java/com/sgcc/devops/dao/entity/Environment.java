package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

public class Environment {
	
	private String eId;
	
	private String eName;
	
	private String eDesc;
	
	private String eCreator;
	
	private String userName;
	
	@JsonSerialize(using = DateSerializer.class)
	private Date eTime;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String geteId() {
		return eId;
	}
	public void seteId(String eId) {
		this.eId = eId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	public String geteDesc() {
		return eDesc;
	}
	public void seteDesc(String eDesc) {
		this.eDesc = eDesc;
	}
	public String geteCreator() {
		return eCreator;
	}
	public void seteCreator(String eCreator) {
		this.eCreator = eCreator;
	}
	public Date geteTime() {
		return eTime;
	}
	public void seteTime(Date eTime) {
		this.eTime = eTime;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}