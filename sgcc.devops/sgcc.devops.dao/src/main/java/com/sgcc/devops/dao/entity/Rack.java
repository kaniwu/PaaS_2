package com.sgcc.devops.dao.entity;

public class Rack {
    private String id;

    private String rackName;

    private String rackRemark;

    private String creator;

    private String serverRoomId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRackName() {
		return rackName;
	}

	public void setRackName(String rackName) {
		this.rackName = rackName;
	}

	public String getRackRemark() {
		return rackRemark;
	}

	public void setRackRemark(String rackRemark) {
		this.rackRemark = rackRemark;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getServerRoomId() {
		return serverRoomId;
	}

	public void setServerRoomId(String serverRoomId) {
		this.serverRoomId = serverRoomId;
	}

	/*public Byte getServerRoomId() {
		return serverRoomId;
	}

	public void setServerRoomId(Byte serverRoomId) {
		this.serverRoomId = serverRoomId;
	}*/

	
}