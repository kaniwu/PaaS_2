package com.sgcc.devops.dao.entity;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 日志信息备份类
 */
public class LogBackup {
	private String logId;

	private Integer logObject;

	private Integer logAction;

	private Integer logResult;

	private Integer userId;

	@JsonSerialize(using = DateSerializer.class)
	private Date logCreatetime;

	private String logDetail;
	private String dialect;
	
	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}
	public String getLogId() {
		return logId;
	}

	public void setLogId(String logId) {
		this.logId = logId;
	}

	public Integer getLogObject() {
		return logObject;
	}

	public void setLogObject(Integer logObject) {
		this.logObject = logObject;
	}

	public Integer getLogAction() {
		return logAction;
	}

	public void setLogAction(Integer logAction) {
		this.logAction = logAction;
	}

	public Integer getLogResult() {
		return logResult;
	}

	public void setLogResult(Integer logResult) {
		this.logResult = logResult;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Date getLogCreatetime() {
		return logCreatetime;
	}

	public void setLogCreatetime(Date logCreatetime) {
		this.logCreatetime = logCreatetime;
	}

	public String getLogDetail() {
		return logDetail;
	}

	public void setLogDetail(String logDetail) {
		this.logDetail = logDetail == null ? null : logDetail.trim();
	}
}