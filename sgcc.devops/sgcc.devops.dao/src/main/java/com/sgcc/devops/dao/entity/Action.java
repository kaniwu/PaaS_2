package com.sgcc.devops.dao.entity;

public class Action {
    private String actionId;

    private String actionName;

    private String actionDesc;

    private String actionRelativeUrl;

    private Byte actionType;

    private String actionRemarks;

    private String actionParentId;

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName == null ? null : actionName.trim();
    }

    public String getActionDesc() {
        return actionDesc;
    }

    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc == null ? null : actionDesc.trim();
    }

    public String getActionRelativeUrl() {
        return actionRelativeUrl;
    }

    public void setActionRelativeUrl(String actionRelativeUrl) {
        this.actionRelativeUrl = actionRelativeUrl == null ? null : actionRelativeUrl.trim();
    }

    public Byte getActionType() {
        return actionType;
    }

    public void setActionType(Byte actionType) {
        this.actionType = actionType;
    }

    public String getActionRemarks() {
        return actionRemarks;
    }

    public void setActionRemarks(String actionRemarks) {
        this.actionRemarks = actionRemarks == null ? null : actionRemarks.trim();
    }

    public String getActionParentId() {
        return actionParentId;
    }

    public void setActionParentId(String actionParentId) {
        this.actionParentId = actionParentId;
    }
}