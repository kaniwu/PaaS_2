package com.sgcc.devops.dao.intf;

import java.util.List;

import com.sgcc.devops.dao.entity.UserRole;


public interface UserRoleMapper {
	
	public List<UserRole> selectAll(UserRole UserRole) throws Exception;
	
	public int deleteByPrimaryKey(String id) throws Exception;

	public int deleteByRoleId(String roleId) throws Exception;
	
	public int insert(UserRole record) throws Exception;

	public int insertSelective(UserRole record) throws Exception;

	public UserRole selectByPrimaryKey(String id) throws Exception;

	public int updateByPrimaryKeySelective(UserRole record) throws Exception;

	public int updateByPrimaryKey(UserRole record) throws Exception;
	
	public int deleteByUserId(String userId) throws Exception;
}