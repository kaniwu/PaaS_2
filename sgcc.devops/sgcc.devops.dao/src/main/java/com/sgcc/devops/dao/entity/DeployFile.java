package com.sgcc.devops.dao.entity;

/**
 * 部署文件类
 */
public class DeployFile {
	private String id;

	private String deployId;
	
	private String fileName;

	private String filePath;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public DeployFile(String id, String deployId, String fileName,
			String filePath) {
		super();
		this.id = id;
		this.deployId = deployId;
		this.fileName = fileName;
		this.filePath = filePath;
	}


}