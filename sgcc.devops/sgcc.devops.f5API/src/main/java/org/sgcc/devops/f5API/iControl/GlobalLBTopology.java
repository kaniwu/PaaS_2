/**
 * GlobalLBTopology.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBTopology extends javax.xml.rpc.Service {

/**
 * The Topology interface enables you to work with topology attributes.
 * For example, you can create and 
 *  delete a topology.  You can also use the Topology interface to add
 * virtual server entries to, 
 *  or remove virtual server entries from, a topology.
 */
    public java.lang.String getGlobalLBTopologyPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBTopologyPortType getGlobalLBTopologyPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBTopologyPortType getGlobalLBTopologyPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
