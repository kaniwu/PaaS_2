/**
 * NetworkingSTPGlobals.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingSTPGlobals extends javax.xml.rpc.Service {

/**
 * The STPGlobals interface enables you to work with global attributes
 * used to configure STP (Spanning Tree Protocol).
 */
    public java.lang.String getNetworkingSTPGlobalsPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingSTPGlobalsPortType getNetworkingSTPGlobalsPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingSTPGlobalsPortType getNetworkingSTPGlobalsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
