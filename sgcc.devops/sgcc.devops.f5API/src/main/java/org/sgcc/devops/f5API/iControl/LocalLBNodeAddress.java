/**
 * LocalLBNodeAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBNodeAddress extends javax.xml.rpc.Service {

/**
 * The NodeAddress interface enables you to work with the states,
 * statistics, limits, availability, ratios, application response deltas,
 * and monitors of a local load balancer's node address.
 */
    public java.lang.String getLocalLBNodeAddressPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBNodeAddressPortType getLocalLBNodeAddressPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBNodeAddressPortType getLocalLBNodeAddressPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
