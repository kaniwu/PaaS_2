/**
 * NetworkingVLANGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingVLANGroup extends javax.xml.rpc.Service {

/**
 * The VLANGroup interface enables you to work with the definitions
 * and attributes contained in a device's VLAN group.
 */
    public java.lang.String getNetworkingVLANGroupPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingVLANGroupPortType getNetworkingVLANGroupPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingVLANGroupPortType getNetworkingVLANGroupPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
