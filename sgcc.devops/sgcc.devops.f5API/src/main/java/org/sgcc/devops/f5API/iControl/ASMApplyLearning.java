/**
 * ASMApplyLearning.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class ASMApplyLearning  implements java.io.Serializable {
    private org.sgcc.devops.f5API.iControl.ASMApplyLearningType type;
    private java.lang.String policy_name;

    public ASMApplyLearning() {
    }

    public ASMApplyLearning(
           org.sgcc.devops.f5API.iControl.ASMApplyLearningType type,
           java.lang.String policy_name) {
           this.type = type;
           this.policy_name = policy_name;
    }


    /**
     * Gets the type value for this ASMApplyLearning.
     * 
     * @return type
     */
    public org.sgcc.devops.f5API.iControl.ASMApplyLearningType getType() {
        return type;
    }


    /**
     * Sets the type value for this ASMApplyLearning.
     * 
     * @param type
     */
    public void setType(org.sgcc.devops.f5API.iControl.ASMApplyLearningType type) {
        this.type = type;
    }


    /**
     * Gets the policy_name value for this ASMApplyLearning.
     * 
     * @return policy_name
     */
    public java.lang.String getPolicy_name() {
        return policy_name;
    }


    /**
     * Sets the policy_name value for this ASMApplyLearning.
     * 
     * @param policy_name
     */
    public void setPolicy_name(java.lang.String policy_name) {
        this.policy_name = policy_name;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ASMApplyLearning)) return false;
        ASMApplyLearning other = (ASMApplyLearning) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.policy_name==null && other.getPolicy_name()==null) || 
             (this.policy_name!=null &&
              this.policy_name.equals(other.getPolicy_name())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getPolicy_name() != null) {
            _hashCode += getPolicy_name().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ASMApplyLearning.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "ASM.ApplyLearning"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "ASM.ApplyLearningType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policy_name");
        elemField.setXmlName(new javax.xml.namespace.QName("", "policy_name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
