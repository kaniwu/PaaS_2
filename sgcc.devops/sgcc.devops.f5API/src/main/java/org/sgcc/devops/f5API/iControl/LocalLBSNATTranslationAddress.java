/**
 * LocalLBSNATTranslationAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBSNATTranslationAddress extends javax.xml.rpc.Service {

/**
 * The SNATTranslationAddress interface enables you to work with the
 * definitions contained in a local load balancer's SNAT translation
 * address. 
 *  A translation address defines an address to which a client address
 * may be translated on the server side.
 */
    public java.lang.String getLocalLBSNATTranslationAddressPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressPortType getLocalLBSNATTranslationAddressPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressPortType getLocalLBSNATTranslationAddressPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
