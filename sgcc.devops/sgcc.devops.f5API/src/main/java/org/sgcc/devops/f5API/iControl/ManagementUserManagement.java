/**
 * ManagementUserManagement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementUserManagement extends javax.xml.rpc.Service {

/**
 * The UserManagement interface deals with adding/deleting and 
 *  modifying users and user permission.
 */
    public java.lang.String getManagementUserManagementPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementUserManagementPortType getManagementUserManagementPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementUserManagementPortType getManagementUserManagementPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
