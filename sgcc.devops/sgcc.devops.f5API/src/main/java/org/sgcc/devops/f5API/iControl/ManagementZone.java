/**
 * ManagementZone.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementZone extends javax.xml.rpc.Service {

/**
 * The Zone interface enables the user to perform "zone" operations
 * on a dns database
 */
    public java.lang.String getManagementZonePortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementZonePortType getManagementZonePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementZonePortType getManagementZonePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
