/**
 * CommonStringSequenceHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl.holders;

public final class CommonStringSequenceHolder implements javax.xml.rpc.holders.Holder {
    public java.lang.String[] value;

    public CommonStringSequenceHolder() {
    }

    public CommonStringSequenceHolder(java.lang.String[] value) {
        this.value = value;
    }

}
