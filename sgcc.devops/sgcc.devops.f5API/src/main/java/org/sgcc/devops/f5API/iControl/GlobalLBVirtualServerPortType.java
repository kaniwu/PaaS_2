/**
 * GlobalLBVirtualServerPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBVirtualServerPortType extends java.rmi.Remote {

    /**
     * Adds the virtual servers to the dependency list that the specified
     * virtual servers depend on.
     */
    public void add_dependency(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[][] dependencies) throws java.rmi.RemoteException;

    /**
     * Creates the specified virtual server.
     */
    public void create(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, java.lang.String[] servers) throws java.rmi.RemoteException;

    /**
     * Deletes all virtual servers.
     */
    public void delete_all_virtual_servers() throws java.rmi.RemoteException;

    /**
     * Deletes the specified virtual servers.
     */
    public void delete_virtual_server(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for all the virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerVirtualServerStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets the list of virtual servers that the specified virtual
     * servers depend on.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[][] get_dependency(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the enabled states for the specified virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_enabled_state(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets all metrics limits for a sequence of virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerVirtualServerMetricLimit[] get_limit(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets a list of virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the monitor associations for the specified virtual servers,
     * i.e. the monitor rules used by the virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerMonitorAssociation[] get_monitor_association(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the statuses of the specified virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.CommonObjectStatus[] get_object_status(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets a parent links the specified virtual servers belong to.
     */
    public java.lang.String[] get_parent_link(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets a list of servers the specified virtual servers belong
     * to.
     */
    public java.lang.String[] get_server(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for the specified virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerVirtualServerStatistics get_statistics(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the translation IP addresses and ports for the specified
     * virtual servers.
     */
    public org.sgcc.devops.f5API.iControl.CommonIPPortDefinition[] get_translation(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Removes any and all dependencies of the specified virtual servers.
     */
    public void remove_all_dependencies(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Removes the virtual servers from the dependency list that the
     * specified virtual servers depend on.
     */
    public void remove_dependency(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[][] dependencies) throws java.rmi.RemoteException;

    /**
     * Removes the monitor associations for the specified virtual
     * servers.  This basically deletes the monitor
     *  associations between a virtual server and a monitor rule, i.e. the
     * specified virtual servers will no longer
     *  be monitored.
     */
    public void remove_monitor_association(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Resets the statistics for the specified virtual servers.
     */
    public void reset_statistics(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers) throws java.rmi.RemoteException;

    /**
     * Sets the enabled states for the specified virtual servers.
     */
    public void set_enabled_state(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the limits for virtual server metrics.
     */
    public void set_limit(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerVirtualServerMetricLimit[] limits) throws java.rmi.RemoteException;

    /**
     * Sets/creates the monitor associations for the specified virtual
     * servers. This basically creates the monitor associations
     *  between a virtual server and a monitor rule.
     */
    public void set_monitor_association(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerMonitorAssociation[] monitor_associations) throws java.rmi.RemoteException;

    /**
     * Sets the servers the specified virtual servers belong to.
     */
    public void set_server(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, java.lang.String[] servers) throws java.rmi.RemoteException;

    /**
     * Sets the translation IP addresses and ports for the specified
     * virtual servers.
     */
    public void set_translation(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] virtual_servers, org.sgcc.devops.f5API.iControl.CommonIPPortDefinition[] translations) throws java.rmi.RemoteException;
}
