/**
 * ManagementRADIUSServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementRADIUSServer extends javax.xml.rpc.Service {

/**
 * The RADIUSServer interface enables you to manage RADIUS Server
 * configuration.
 */
    public java.lang.String getManagementRADIUSServerPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementRADIUSServerPortType getManagementRADIUSServerPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementRADIUSServerPortType getManagementRADIUSServerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
