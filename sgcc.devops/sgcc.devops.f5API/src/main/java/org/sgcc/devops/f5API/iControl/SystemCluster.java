/**
 * SystemCluster.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemCluster extends javax.xml.rpc.Service {

/**
 * The Cluster interface enables you to work with the definitions
 * and attributes in a clustered device.
 */
    public java.lang.String getSystemClusterPortAddress();

    public org.sgcc.devops.f5API.iControl.SystemClusterPortType getSystemClusterPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.SystemClusterPortType getSystemClusterPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
