/**
 * LocalLBPoolMember.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBPoolMember extends javax.xml.rpc.Service {

/**
 * The PoolMember interface enables you to work with the pool members
 * and their settings, and statistics.
 */
    public java.lang.String getLocalLBPoolMemberPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBPoolMemberPortType getLocalLBPoolMemberPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBPoolMemberPortType getLocalLBPoolMemberPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
