/**
 * LocalLBProfileStream.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileStream extends javax.xml.rpc.Service {

/**
 * The ProfileStream interface enables you to manipulate a local load
 * balancer's stream profile.
 */
    public java.lang.String getLocalLBProfileStreamPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileStreamPortType getLocalLBProfileStreamPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileStreamPortType getLocalLBProfileStreamPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
