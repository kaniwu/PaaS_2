/**
 * GlobalLBApplication.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBApplication extends javax.xml.rpc.Service {

/**
 * The Application interface enables you to work with applications
 * running within Wide IPs.
 */
    public java.lang.String getGlobalLBApplicationPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBApplicationPortType getGlobalLBApplicationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBApplicationPortType getGlobalLBApplicationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
