/**
 * CommonEnabledStateHolder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl.holders;

public final class CommonEnabledStateHolder implements javax.xml.rpc.holders.Holder {
    public org.sgcc.devops.f5API.iControl.CommonEnabledState value;

    public CommonEnabledStateHolder() {
    }

    public CommonEnabledStateHolder(org.sgcc.devops.f5API.iControl.CommonEnabledState value) {
        this.value = value;
    }

}
