/**
 * ASMPolicy.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ASMPolicy extends javax.xml.rpc.Service {

/**
 * The Policy interface enables you to manipulate a policy.
 */
    public java.lang.String getASMPolicyPortAddress();

    public org.sgcc.devops.f5API.iControl.ASMPolicyPortType getASMPolicyPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ASMPolicyPortType getASMPolicyPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
