package org.sgcc.devops.f5API.bean;
/*
 * The contents of this file are subject to the "END USER LICENSE AGREEMENT FOR F5
 * Software Development Kit for iControl"; you may not use this file except in
 * compliance with the License. The License is included in the iControl
 * Software Development Kit.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is iControl Code and related documentation
 * distributed by F5.
 *
 * The Initial Developer of the Original Code is F5 Networks,
 * Inc. Seattle, WA, USA. Portions created by F5 are Copyright (C) 1996-2004 F5
 * Inc. All Rights Reserved.  iControl (TM) is a registered trademark of F5 Netw
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */


public class LocalLBNodeAddress extends Object
{
	//--------------------------------------------------------------------------
	// Member Variables
	//--------------------------------------------------------------------------
	public String m_endpoint;
	public org.sgcc.devops.f5API.iControl.LocalLBNodeAddressBindingStub m_nodeAddress;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public LocalLBNodeAddress()
	{
		System.setProperty("javax.net.ssl.trustStore", System.getProperty("user.home") + "/.keystore");
		XTrustProvider.install();
	}

	//--------------------------------------------------------------------------
	// parseArgs
	//--------------------------------------------------------------------------
	public boolean parseArgs(String[] args) throws Exception
	{
		boolean bSuccess = false;

		if ( args.length < 4 )
		{
			usage();
		}
		else
		{
			// build parameters

			m_endpoint = "https://" + args[2] + ":" + args[3] + "@" + args[0] + ":" + args[1] + "/iControl/iControlPortal.cgi";

			m_nodeAddress = (org.sgcc.devops.f5API.iControl.LocalLBNodeAddressBindingStub)
				new org.sgcc.devops.f5API.iControl.LocalLBNodeAddressLocator().getLocalLBNodeAddressPort(new java.net.URL(m_endpoint));

			m_nodeAddress.setTimeout(60000);

			bSuccess = true;

			if ( args.length >= 5 )
			{
				getNodeInfo(new String[] {args[4]});
			}
			else
			{
				getAllNodeInfo();
			}
		}

		return bSuccess;
	}

	//-------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	public void usage()
	{
		System.out.println("Usage: LocalLBNodeAddress hostname port username password [node_address]\n");
	}

	//--------------------------------------------------------------------------
	// getAllNodeInfo
	//--------------------------------------------------------------------------
	public void getAllNodeInfo() throws Exception
	{
		getNodeInfo(m_nodeAddress.get_list());
	}

	//--------------------------------------------------------------------------
	// getNodeInfo
	//--------------------------------------------------------------------------
	public void getNodeInfo(String[] node_list) throws Exception
	{
		org.sgcc.devops.f5API.iControl.LocalLBSessionStatus[] session_status_list =
			m_nodeAddress.get_session_status(node_list);

		org.sgcc.devops.f5API.iControl.CommonEnabledState [] enabled_state_list =
			m_nodeAddress.get_session_enabled_state(node_list);

		org.sgcc.devops.f5API.iControl.LocalLBObjectStatus [] object_status_list =
			m_nodeAddress.get_object_status(node_list);

		org.sgcc.devops.f5API.iControl.LocalLBMonitorStatus [] monitor_status_list =
			m_nodeAddress.get_monitor_status(node_list);

		org.sgcc.devops.f5API.iControl.CommonULong64 [] connection_limit_list =
			m_nodeAddress.get_connection_limit(node_list);

		long [] ratio_list =
			m_nodeAddress.get_ratio(node_list);

		org.sgcc.devops.f5API.iControl.LocalLBNodeAddressNodeAddressStatistics na_stats =
			m_nodeAddress.get_statistics(node_list);

		for(int i=0; i<node_list.length; i++)
		{
			System.out.println("NODE " + node_list[i]);
			System.out.println("    Session Status   : " + session_status_list[i]);
			System.out.println("    Enabled State    : " + enabled_state_list[i]);
			System.out.println("    Monitor Status   : " + monitor_status_list[i]);
			System.out.println("    Availability     : " + object_status_list[i].getAvailability_status());
			System.out.println("    Connection Limit : " + connection_limit_list[i].getLow());
			System.out.println("    Ratio            : " + ratio_list[i]);
			System.out.println("    Statistics       : ");

			org.sgcc.devops.f5API.iControl.CommonStatistic [] statistic_list = na_stats.getStatistics()[i].getStatistics();
			for(int j=0; j<statistic_list.length; j++)
			{
				System.out.println("                       " +
					statistic_list[j].getType() + " : " + statistic_list[j].getValue().getLow());
			}

		}
	}

	//--------------------------------------------------------------------------
	// Main
	//--------------------------------------------------------------------------
	public static void main(String[] args)
	{
		try
		{
			LocalLBNodeAddress localLBNodeAddress = new LocalLBNodeAddress();
			localLBNodeAddress.parseArgs(args);
		}
		catch(Exception ex)
		{
			ex.printStackTrace(System.out);
		}
	}
};
