/**
 * LocalLBRule.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBRule extends javax.xml.rpc.Service {

/**
 * The Rule interface enables you to manipulate a local load balancer's
 * rules.  For example, use the Rule interface to get a list of all rules,
 * create rules, delete rules, modify rules, and query rules.
 */
    public java.lang.String getLocalLBRulePortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBRulePortType getLocalLBRulePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBRulePortType getLocalLBRulePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
