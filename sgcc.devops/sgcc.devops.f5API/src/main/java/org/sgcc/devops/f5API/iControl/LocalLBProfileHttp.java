/**
 * LocalLBProfileHttp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileHttp extends javax.xml.rpc.Service {

/**
 * The ProfileHttp interface enables you to manipulate a local load
 * balancer's HTTP profile.
 */
    public java.lang.String getLocalLBProfileHttpPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileHttpPortType getLocalLBProfileHttpPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileHttpPortType getLocalLBProfileHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
