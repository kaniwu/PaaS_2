/**
 * ManagementEventSubscriptionUserCredential.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class ManagementEventSubscriptionUserCredential  implements java.io.Serializable {
    private org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionAuthenticationMode auth_mode;
    private java.lang.String username;
    private java.lang.String password;

    public ManagementEventSubscriptionUserCredential() {
    }

    public ManagementEventSubscriptionUserCredential(
           org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionAuthenticationMode auth_mode,
           java.lang.String username,
           java.lang.String password) {
           this.auth_mode = auth_mode;
           this.username = username;
           this.password = password;
    }


    /**
     * Gets the auth_mode value for this ManagementEventSubscriptionUserCredential.
     * 
     * @return auth_mode
     */
    public org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionAuthenticationMode getAuth_mode() {
        return auth_mode;
    }


    /**
     * Sets the auth_mode value for this ManagementEventSubscriptionUserCredential.
     * 
     * @param auth_mode
     */
    public void setAuth_mode(org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionAuthenticationMode auth_mode) {
        this.auth_mode = auth_mode;
    }


    /**
     * Gets the username value for this ManagementEventSubscriptionUserCredential.
     * 
     * @return username
     */
    public java.lang.String getUsername() {
        return username;
    }


    /**
     * Sets the username value for this ManagementEventSubscriptionUserCredential.
     * 
     * @param username
     */
    public void setUsername(java.lang.String username) {
        this.username = username;
    }


    /**
     * Gets the password value for this ManagementEventSubscriptionUserCredential.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this ManagementEventSubscriptionUserCredential.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ManagementEventSubscriptionUserCredential)) return false;
        ManagementEventSubscriptionUserCredential other = (ManagementEventSubscriptionUserCredential) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.auth_mode==null && other.getAuth_mode()==null) || 
             (this.auth_mode!=null &&
              this.auth_mode.equals(other.getAuth_mode()))) &&
            ((this.username==null && other.getUsername()==null) || 
             (this.username!=null &&
              this.username.equals(other.getUsername()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAuth_mode() != null) {
            _hashCode += getAuth_mode().hashCode();
        }
        if (getUsername() != null) {
            _hashCode += getUsername().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ManagementEventSubscriptionUserCredential.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Management.EventSubscription.UserCredential"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("auth_mode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "auth_mode"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Management.EventSubscription.AuthenticationMode"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("username");
        elemField.setXmlName(new javax.xml.namespace.QName("", "username"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
