/**
 * LocalLBRateClassPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBRateClassPortType extends java.rmi.Remote {

    /**
     * Creates the specified rate classes.
     */
    public void create(java.lang.String[] rate_classes, org.sgcc.devops.f5API.iControl.LocalLBRateClassRateUnit[] base_rates) throws java.rmi.RemoteException;

    /**
     * Deletes all rate classes.
     */
    public void delete_all_rate_classes() throws java.rmi.RemoteException;

    /**
     * Deletes the specified rate classes.
     */
    public void delete_rate_class(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for all the rate classes.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassRateClassStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets the base rates for the specified rate classes.  The base
     * rate specifies the maximum throughput to allot to traffic 
     *  handled by the Rate Class. Packets in excess of the posted rate will
     * be dropped.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassRateUnit[] get_base_rate(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the burst sizes for the specified rate classes. The burst
     * size specifies maximum number of 
     *  bytes that traffic is allowed to burst beyond the base rate.
     */
    public long[] get_burst_size(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the ceiling rates for the specified rate classes.  The
     * ceiling rate specifies how far beyond the base rate the traffic 
     *  is allowed to flow when bursting. The ceiling rate is an absolute
     * limit - it is impossible for traffic to flow at a higher 
     *  rate than the ceiling rate, even when bursting. If the ceiling rate
     * is omitted or is equal to the base rate, the traffic 
     *  may not exceed the base rate. It is illegal for the ceiling rate
     * to be less than the base rate.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassRateUnit[] get_ceiling_rate(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the direction types being used by the specified rate classes.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassDirectionType[] get_direction(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets a list of all rate classes.
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the name of the parent rate class. Any child Rate Class
     * has the ability to borrow bandwidth from its parent. 
     *  If the parent rate class is not specified in the definition of a
     * rate class, the Rate Class is not a child, but 
     *  may be a parent if another Rate Class refers to it as such.
     */
    public java.lang.String[] get_parent(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the queue types being used by the specified rate classes.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassQueueType[] get_queue_type(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for the specified rate classes.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassRateClassStatistics get_statistics(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Resets the statistics for the specified rate classes.
     */
    public void reset_statistics(java.lang.String[] rate_classes) throws java.rmi.RemoteException;

    /**
     * Sets the base rates for the specified rate classes.  The base
     * rate specifies the maximum throughput to allot to traffic 
     *  handled by the Rate Class. Packets in excess of the posted rate will
     * be dropped.
     */
    public void set_base_rate(java.lang.String[] rate_classes, org.sgcc.devops.f5API.iControl.LocalLBRateClassRateUnit[] rates) throws java.rmi.RemoteException;

    /**
     * Sets the burst sizes for the specified rate classes. The burst
     * size specifies maximum number of 
     *  bytes that traffic is allowed to burst beyond the base rate.
     */
    public void set_burst_size(java.lang.String[] rate_classes, long[] burst_sizes) throws java.rmi.RemoteException;

    /**
     * Sets the ceiling rates for the specified rate classes.  The
     * ceiling rate specifies how far beyond the base rate the traffic 
     *  is allowed to flow when bursting. The ceiling rate is an absolute
     * limit - it is impossible for traffic to flow at a higher 
     *  rate than the ceiling rate, even when bursting. If the ceiling rate
     * is omitted or is equal to the base rate, the traffic 
     *  may not exceed the base rate. It is illegal for the ceiling rate
     * to be less than the base rate.
     */
    public void set_ceiling_rate(java.lang.String[] rate_classes, org.sgcc.devops.f5API.iControl.LocalLBRateClassRateUnit[] rates) throws java.rmi.RemoteException;

    /**
     * Sets the direction types to be used by the specified rate classes.
     */
    public void set_direction(java.lang.String[] rate_classes, org.sgcc.devops.f5API.iControl.LocalLBRateClassDirectionType[] direction_types) throws java.rmi.RemoteException;

    /**
     * Sets the name of the parent rate class. Any child Rate Class
     * has the ability to borrow bandwidth from its parent. 
     *  If the parent rate class is not specified in the definition of a
     * rate class, the Rate Class is not a child, but 
     *  may be a parent if another Rate Class refers to it as such.
     */
    public void set_parent(java.lang.String[] rate_classes, java.lang.String[] parents) throws java.rmi.RemoteException;

    /**
     * Sets the queue types to be used by the specified rate classes.
     */
    public void set_queue_type(java.lang.String[] rate_classes, org.sgcc.devops.f5API.iControl.LocalLBRateClassQueueType[] queue_types) throws java.rmi.RemoteException;
}
