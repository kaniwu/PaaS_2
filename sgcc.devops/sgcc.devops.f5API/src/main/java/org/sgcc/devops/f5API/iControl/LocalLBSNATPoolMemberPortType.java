/**
 * LocalLBSNATPoolMemberPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBSNATPoolMemberPortType extends java.rmi.Remote {

    /**
     * Gets the statistics for all SNATPool members in the specified
     * SNAT pools.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolMemberSNATPoolMemberStatistics[] get_all_statistics(java.lang.String[] snat_pools) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for a list of SNATPool members in the specified
     * SNAT pools.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolMemberSNATPoolMemberStatistics[] get_statistics(java.lang.String[] snat_pools, java.lang.String[][] members) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Resets the statistics for a list of SNATPool members in the
     * specified SNAT pools.
     */
    public void reset_statistics(java.lang.String[] snat_pools, java.lang.String[][] members) throws java.rmi.RemoteException;
}
