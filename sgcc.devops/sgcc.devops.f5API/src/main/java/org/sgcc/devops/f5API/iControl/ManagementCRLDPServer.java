/**
 * ManagementCRLDPServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementCRLDPServer extends javax.xml.rpc.Service {

/**
 * The CRLDPServer interface enables you to manage CRLDP Server configuration.
 */
    public java.lang.String getManagementCRLDPServerPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementCRLDPServerPortType getManagementCRLDPServerPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementCRLDPServerPortType getManagementCRLDPServerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
