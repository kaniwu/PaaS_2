/**
 * ManagementKeyCertificatePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementKeyCertificatePortType extends java.rmi.Remote {

    /**
     * Adds certificates identified by "certificate_files" to the
     * certificate bundles, which are 
     *  presumed to exist already. Each of the original certificate bundle
     * can theoretically be a 
     *  normal certificate, i.e. a certificate bundle of one. After the add
     * operation, the bundles 
     *  will contain more than one certificate.
     */
    public void certificate_add_file_to_bundle(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] certificate_files) throws java.rmi.RemoteException;

    /**
     * Adds certificates identified by "pem_data" to the certificate
     * bundles, which are presumed
     *  to exist already. Each of the original certificate bundle can theoretically
     * be a normal
     *  certificate, i.e. a certificate bundle of one. After the add operation,
     * the bundles will
     *  contain more than one certificate.
     */
    public void certificate_add_pem_to_bundle(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] pem_data) throws java.rmi.RemoteException;

    /**
     * Binds/associates the specified keys and certificates.
     */
    public void certificate_bind(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] key_ids) throws java.rmi.RemoteException;

    /**
     * Gets the validity of the specified certificates.
     */
    public org.sgcc.devops.f5API.iControl.ManagementKeyCertificateValidityType[] certificate_check_validity(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, long[] watermark_days) throws java.rmi.RemoteException;

    /**
     * Deletes/uninstalls the specified certificates.
     */
    public void certificate_delete(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids) throws java.rmi.RemoteException;

    /**
     * Deletes certificates, identified by their subject's X509 data,
     * from the certificate bundles.
     *  If the last certificate has been deleted from the bundle, the certificate
     * file will 
     *  automatically be deleted.
     */
    public void certificate_delete_from_bundle(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateX509Data[] x509_data) throws java.rmi.RemoteException;

    /**
     * Exports the specified certificates to the given files.
     */
    public void certificate_export_to_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Exports the specified certificates to PEM-formatted data.
     */
    public java.lang.String[] certificate_export_to_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids) throws java.rmi.RemoteException;

    /**
     * Generates the specified certificates.  This assumes that each
     * of the associated keys,
     *  having the same identification as each certificate, has already been
     * created.
     */
    public void certificate_generate(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateCertificate[] certs, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateX509Data[] x509_data, long[] lifetime_days, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified certificates from the given
     * files.
     */
    public void certificate_import_from_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified certificates from the given
     * PEM-formatted data.
     */
    public void certificate_import_from_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] cert_ids, java.lang.String[] pem_data, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Deletes the specified CSRs.
     */
    public void certificate_request_delete(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] csr_ids) throws java.rmi.RemoteException;

    /**
     * Exports the specified certificate requests to the given files.
     */
    public void certificate_request_export_to_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] csr_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Exports the specified certificate requests to PEM-formatted
     * data.
     */
    public java.lang.String[] certificate_request_export_to_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] csr_ids) throws java.rmi.RemoteException;

    /**
     * Generates the specified certificate signing requests.  This
     * assumes that each of the 
     *  associated keys, having the same identification as each certificate
     * request, has 
     *  already been created.
     */
    public void certificate_request_generate(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateCertificateRequest[] csrs, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateX509Data[] x509_data, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified certificate requests from the
     * given files.
     */
    public void certificate_request_import_from_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] csr_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified certificate requests from the
     * given PEM-formatted data.
     */
    public void certificate_request_import_from_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] csr_ids, java.lang.String[] pem_data, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Exports all currently installed keys and certificates into
     * the specified archive file.
     *  The archive file is a .tgz file that will contain all keys and certificates.
     */
    public void export_all_to_archive_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String archive_location, java.lang.String archive_name) throws java.rmi.RemoteException;

    /**
     * Exports all currently installed keys and certificates into
     * the returned archive stream.
     *  The returned archive stream is basically the contents of a .tgz file
     * that contains 
     *  all keys and certificates.
     */
    public byte[] export_all_to_archive_stream(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode) throws java.rmi.RemoteException;

    /**
     * Exports the specified keys and certificates into the specified
     * archive file.
     *  The archive file is a .tgz file that will contain only the specified
     * keys and 
     *  certificates that have been exported.
     */
    public void export_to_archive_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String archive_location, java.lang.String archive_name, java.lang.String[] keys, java.lang.String[] certs) throws java.rmi.RemoteException;

    /**
     * Exports the specified keys and certificates into the returned
     * archive stream.
     *  The returned archive stream is basically the contents of a .tgz file
     * that contains 
     *  the exported keys and certificates.
     */
    public byte[] export_to_archive_stream(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] keys, java.lang.String[] certs) throws java.rmi.RemoteException;

    /**
     * Gets the list of all certificates bundled in the certificate
     * files as 
     *  specified by the file_names.  Each file_name will contain multiple
     * certficates.
     *  Note: only call this method when the results of get_certificate_list
     * indicate
     *  that there are multiple certificated bundled in a particular file.
     */
    public org.sgcc.devops.f5API.iControl.ManagementKeyCertificateCertificateDetail[][] get_certificate_bundle(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] file_names) throws java.rmi.RemoteException;

    /**
     * Gets the list of all installed certificates and their information.
     * If there's 
     *  a certificate bundle, only the first certificate in the bundle is
     * returned, and
     *  and is_bundle flag will be set to true for the correponding bundle
     * file name.
     */
    public org.sgcc.devops.f5API.iControl.ManagementKeyCertificateCertificateInformation[] get_certificate_list(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode) throws java.rmi.RemoteException;

    /**
     * Gets the list of all CSRs and their information.
     */
    public org.sgcc.devops.f5API.iControl.ManagementKeyCertificateCertificateRequestInformation[] get_certificate_request_list(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode) throws java.rmi.RemoteException;

    /**
     * Gets the list of all installed keys and their information.
     */
    public org.sgcc.devops.f5API.iControl.ManagementKeyCertificateKeyInformation[] get_key_list(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Imports/installs all keys and certificates from the specified
     * archive file.
     *  The archive file should be a .tgz file that contains all keys and
     * certificates.
     */
    public void import_all_from_archive_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String archive_location, java.lang.String archive_name) throws java.rmi.RemoteException;

    /**
     * Imports/installs all keys and certificates from the incoming
     * archive stream.
     *  The archive stream should be the contents of a .tgz file that contains
     * all 
     *  keys and certificates.
     */
    public void import_all_from_archive_stream(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, byte[] archive_stream) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified keys and certificates from the
     * specified archive file.
     *  The archive file should be a .tgz file that may contain more keys
     * and certificates 
     *  than what will be imported/installed.
     */
    public void import_from_archive_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String archive_location, java.lang.String archive_name, java.lang.String[] keys, java.lang.String[] certs) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified keys and certificates from the
     * incoming archive stream.
     *  The archive stream should be the contents of a .tgz file that may
     * contain more keys 
     *  and certificates than what will be imported/installed.
     */
    public void import_from_archive_stream(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, byte[] archive_stream, java.lang.String[] keys, java.lang.String[] certs) throws java.rmi.RemoteException;

    /**
     * Checks to see if the device supports FIPS security.
     */
    public boolean is_fips_available(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode) throws java.rmi.RemoteException;

    /**
     * Deletes/uninstalls the specified keys.
     */
    public void key_delete(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids) throws java.rmi.RemoteException;

    /**
     * Exports the specified keys to the given files.
     */
    public void key_export_to_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Exports the specified keys to PEM-formatted data.
     */
    public java.lang.String[] key_export_to_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids) throws java.rmi.RemoteException;

    /**
     * Generates the specified keys.
     */
    public void key_generate(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateKey[] keys, org.sgcc.devops.f5API.iControl.ManagementKeyCertificateX509Data[] x509_data, boolean create_optional_cert_csr, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified keys from the given files.
     */
    public void key_import_from_file(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids, java.lang.String[] file_names, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Imports/installs the specified keys from the given PEM-formatted
     * data.
     */
    public void key_import_from_pem(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids, java.lang.String[] pem_data, boolean overwrite) throws java.rmi.RemoteException;

    /**
     * Converts the specified keys to FIPS-enabled keys.
     */
    public void key_to_fips(org.sgcc.devops.f5API.iControl.ManagementKeyCertificateManagementModeType mode, java.lang.String[] key_ids) throws java.rmi.RemoteException;
}
