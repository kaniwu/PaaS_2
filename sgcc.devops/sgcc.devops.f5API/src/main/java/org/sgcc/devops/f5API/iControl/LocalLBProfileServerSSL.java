/**
 * LocalLBProfileServerSSL.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileServerSSL extends javax.xml.rpc.Service {

/**
 * The ProfileServerSSL interface enables you to manipulate a local
 * load balancer's server SSL profile.
 */
    public java.lang.String getLocalLBProfileServerSSLPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLPortType getLocalLBProfileServerSSLPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLPortType getLocalLBProfileServerSSLPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
