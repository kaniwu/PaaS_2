/**
 * ManagementEventSubscription.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementEventSubscription extends javax.xml.rpc.Service {

/**
 * The EventSubscription interface is to be used to register for system
 * configuration change events.  Events are sent using the EventNotification
 * interface.
 */
    public java.lang.String getManagementEventSubscriptionPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionPortType getManagementEventSubscriptionPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementEventSubscriptionPortType getManagementEventSubscriptionPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
