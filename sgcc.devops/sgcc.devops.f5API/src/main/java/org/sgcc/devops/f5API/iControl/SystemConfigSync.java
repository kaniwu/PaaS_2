/**
 * SystemConfigSync.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemConfigSync extends javax.xml.rpc.Service {

/**
 * The ConfigSync interface enables you to work with configuration
 * files.  For example, use the 
 *  ConfigSync interface to retrieve a configuration file list, roll
 * up and save a specified configuration, 
 *  install a configuration on a device, synchronize configuration setups,
 * roll back a configuration, and 
 *  upload or download a configuration.
 */
    public java.lang.String getSystemConfigSyncPortAddress();

    public org.sgcc.devops.f5API.iControl.SystemConfigSyncPortType getSystemConfigSyncPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.SystemConfigSyncPortType getSystemConfigSyncPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
