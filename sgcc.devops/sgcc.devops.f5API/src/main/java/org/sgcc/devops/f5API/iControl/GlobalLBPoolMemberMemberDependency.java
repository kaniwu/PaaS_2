/**
 * GlobalLBPoolMemberMemberDependency.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class GlobalLBPoolMemberMemberDependency  implements java.io.Serializable {
    private org.sgcc.devops.f5API.iControl.CommonIPPortDefinition member;
    private org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] dependencies;

    public GlobalLBPoolMemberMemberDependency() {
    }

    public GlobalLBPoolMemberMemberDependency(
           org.sgcc.devops.f5API.iControl.CommonIPPortDefinition member,
           org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] dependencies) {
           this.member = member;
           this.dependencies = dependencies;
    }


    /**
     * Gets the member value for this GlobalLBPoolMemberMemberDependency.
     * 
     * @return member
     */
    public org.sgcc.devops.f5API.iControl.CommonIPPortDefinition getMember() {
        return member;
    }


    /**
     * Sets the member value for this GlobalLBPoolMemberMemberDependency.
     * 
     * @param member
     */
    public void setMember(org.sgcc.devops.f5API.iControl.CommonIPPortDefinition member) {
        this.member = member;
    }


    /**
     * Gets the dependencies value for this GlobalLBPoolMemberMemberDependency.
     * 
     * @return dependencies
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] getDependencies() {
        return dependencies;
    }


    /**
     * Sets the dependencies value for this GlobalLBPoolMemberMemberDependency.
     * 
     * @param dependencies
     */
    public void setDependencies(org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerDefinition[] dependencies) {
        this.dependencies = dependencies;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GlobalLBPoolMemberMemberDependency)) return false;
        GlobalLBPoolMemberMemberDependency other = (GlobalLBPoolMemberMemberDependency) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.member==null && other.getMember()==null) || 
             (this.member!=null &&
              this.member.equals(other.getMember()))) &&
            ((this.dependencies==null && other.getDependencies()==null) || 
             (this.dependencies!=null &&
              java.util.Arrays.equals(this.dependencies, other.getDependencies())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMember() != null) {
            _hashCode += getMember().hashCode();
        }
        if (getDependencies() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDependencies());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDependencies(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GlobalLBPoolMemberMemberDependency.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "GlobalLB.PoolMember.MemberDependency"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("member");
        elemField.setXmlName(new javax.xml.namespace.QName("", "member"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Common.IPPortDefinition"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dependencies");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dependencies"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "GlobalLB.VirtualServerDefinition"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
