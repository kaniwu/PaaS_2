/**
 * LocalLBProfileServerSSLProfileServerSSLStatistics.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBProfileServerSSLProfileServerSSLStatistics  implements java.io.Serializable {
    private org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLProfileServerSSLStatisticEntry[] statistics;
    private org.sgcc.devops.f5API.iControl.CommonTimeStamp time_stamp;

    public LocalLBProfileServerSSLProfileServerSSLStatistics() {
    }

    public LocalLBProfileServerSSLProfileServerSSLStatistics(
           org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLProfileServerSSLStatisticEntry[] statistics,
           org.sgcc.devops.f5API.iControl.CommonTimeStamp time_stamp) {
           this.statistics = statistics;
           this.time_stamp = time_stamp;
    }


    /**
     * Gets the statistics value for this LocalLBProfileServerSSLProfileServerSSLStatistics.
     * 
     * @return statistics
     */
    public org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLProfileServerSSLStatisticEntry[] getStatistics() {
        return statistics;
    }


    /**
     * Sets the statistics value for this LocalLBProfileServerSSLProfileServerSSLStatistics.
     * 
     * @param statistics
     */
    public void setStatistics(org.sgcc.devops.f5API.iControl.LocalLBProfileServerSSLProfileServerSSLStatisticEntry[] statistics) {
        this.statistics = statistics;
    }


    /**
     * Gets the time_stamp value for this LocalLBProfileServerSSLProfileServerSSLStatistics.
     * 
     * @return time_stamp
     */
    public org.sgcc.devops.f5API.iControl.CommonTimeStamp getTime_stamp() {
        return time_stamp;
    }


    /**
     * Sets the time_stamp value for this LocalLBProfileServerSSLProfileServerSSLStatistics.
     * 
     * @param time_stamp
     */
    public void setTime_stamp(org.sgcc.devops.f5API.iControl.CommonTimeStamp time_stamp) {
        this.time_stamp = time_stamp;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocalLBProfileServerSSLProfileServerSSLStatistics)) return false;
        LocalLBProfileServerSSLProfileServerSSLStatistics other = (LocalLBProfileServerSSLProfileServerSSLStatistics) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statistics==null && other.getStatistics()==null) || 
             (this.statistics!=null &&
              java.util.Arrays.equals(this.statistics, other.getStatistics()))) &&
            ((this.time_stamp==null && other.getTime_stamp()==null) || 
             (this.time_stamp!=null &&
              this.time_stamp.equals(other.getTime_stamp())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatistics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatistics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatistics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTime_stamp() != null) {
            _hashCode += getTime_stamp().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocalLBProfileServerSSLProfileServerSSLStatistics.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.ProfileServerSSL.ProfileServerSSLStatistics"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statistics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statistics"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.ProfileServerSSL.ProfileServerSSLStatisticEntry"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("time_stamp");
        elemField.setXmlName(new javax.xml.namespace.QName("", "time_stamp"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Common.TimeStamp"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
