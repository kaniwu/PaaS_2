/**
 * LocalLBProfileOneConnect.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileOneConnect extends javax.xml.rpc.Service {

/**
 * The ProfileOneConnect interface enables you to manipulate a local
 * load balancer's OneConnect profile.
 */
    public java.lang.String getLocalLBProfileOneConnectPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileOneConnectPortType getLocalLBProfileOneConnectPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileOneConnectPortType getLocalLBProfileOneConnectPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
