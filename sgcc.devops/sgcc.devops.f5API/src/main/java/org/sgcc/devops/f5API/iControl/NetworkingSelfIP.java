/**
 * NetworkingSelfIP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingSelfIP extends javax.xml.rpc.Service {

/**
 * The SelfIP interface enables you to work with the definitions and
 * attributes contained in a device's Self IP.
 */
    public java.lang.String getNetworkingSelfIPPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingSelfIPPortType getNetworkingSelfIPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingSelfIPPortType getNetworkingSelfIPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
