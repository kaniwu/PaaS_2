/**
 * LocalLBRateClassRateClassStatisticEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBRateClassRateClassStatisticEntry  implements java.io.Serializable {
    private java.lang.String rate_class;
    private org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics;

    public LocalLBRateClassRateClassStatisticEntry() {
    }

    public LocalLBRateClassRateClassStatisticEntry(
           java.lang.String rate_class,
           org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
           this.rate_class = rate_class;
           this.statistics = statistics;
    }


    /**
     * Gets the rate_class value for this LocalLBRateClassRateClassStatisticEntry.
     * 
     * @return rate_class
     */
    public java.lang.String getRate_class() {
        return rate_class;
    }


    /**
     * Sets the rate_class value for this LocalLBRateClassRateClassStatisticEntry.
     * 
     * @param rate_class
     */
    public void setRate_class(java.lang.String rate_class) {
        this.rate_class = rate_class;
    }


    /**
     * Gets the statistics value for this LocalLBRateClassRateClassStatisticEntry.
     * 
     * @return statistics
     */
    public org.sgcc.devops.f5API.iControl.CommonStatistic[] getStatistics() {
        return statistics;
    }


    /**
     * Sets the statistics value for this LocalLBRateClassRateClassStatisticEntry.
     * 
     * @param statistics
     */
    public void setStatistics(org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
        this.statistics = statistics;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocalLBRateClassRateClassStatisticEntry)) return false;
        LocalLBRateClassRateClassStatisticEntry other = (LocalLBRateClassRateClassStatisticEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.rate_class==null && other.getRate_class()==null) || 
             (this.rate_class!=null &&
              this.rate_class.equals(other.getRate_class()))) &&
            ((this.statistics==null && other.getStatistics()==null) || 
             (this.statistics!=null &&
              java.util.Arrays.equals(this.statistics, other.getStatistics())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRate_class() != null) {
            _hashCode += getRate_class().hashCode();
        }
        if (getStatistics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatistics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatistics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocalLBRateClassRateClassStatisticEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.RateClass.RateClassStatisticEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rate_class");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rate_class"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statistics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statistics"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Common.Statistic"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
