/**
 * LocalLBProfileDNS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileDNS extends javax.xml.rpc.Service {

/**
 * The ProfileDNS interface enables you to manipulate a local load
 * balancer's DNS profile.
 */
    public java.lang.String getLocalLBProfileDNSPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileDNSPortType getLocalLBProfileDNSPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileDNSPortType getLocalLBProfileDNSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
