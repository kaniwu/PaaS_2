/**
 * ManagementLicenseAdministrationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementLicenseAdministrationPortType extends java.rmi.Remote {

    /**
     * Checks to see if the specified registration keys will pass
     * the CRC validation.
     */
    public boolean[] check_registration_key_crc(java.lang.String[] registration_keys) throws java.rmi.RemoteException;

    /**
     * Retrieves the copyright file.
     */
    public byte[] get_copyright_file() throws java.rmi.RemoteException;

    /**
     * Retrieves the EULA agreement file.
     */
    public byte[] get_eula_file() throws java.rmi.RemoteException;

    /**
     * Gets information on when the evaluation license will expire.
     */
    public org.sgcc.devops.f5API.iControl.ManagementLicenseAdministrationEvaluationExpiration get_evaluation_license_expiration() throws java.rmi.RemoteException;

    /**
     * Checks to see whether the device has been licensed.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState get_license_activation_status() throws java.rmi.RemoteException;

    /**
     * Retrieves the active license file.
     */
    public byte[] get_license_file() throws java.rmi.RemoteException;

    /**
     * Gets the list of registration keys used to license the device.
     */
    public java.lang.String[] get_registration_keys() throws java.rmi.RemoteException;

    /**
     * Gets the kernel dossier based on the specified registration
     * keys.
     */
    public java.lang.String get_system_dossier(java.lang.String[] registration_keys) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Installs and authorizes the device using the specified stream
     * of license file data.
     */
    public void install_license(byte[] license_file_data) throws java.rmi.RemoteException;

    /**
     * Installs and authorizes the device using the specified license
     * file somewhere on the device.
     */
    public void install_license_from_file(java.lang.String license_file) throws java.rmi.RemoteException;

    /**
     * Checks to see whether the device is running with an evaluation
     * license.
     */
    public boolean is_evaluation_license() throws java.rmi.RemoteException;
}
