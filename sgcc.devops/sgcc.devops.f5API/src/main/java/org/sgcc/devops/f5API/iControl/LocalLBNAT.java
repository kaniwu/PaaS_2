/**
 * LocalLBNAT.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBNAT extends javax.xml.rpc.Service {

/**
 * The NAT interface enables you to work with the statistics and definitions
 * contained in a local load balancer's network address translations
 * (NAT).  For example, use the NAT interface to both get and set the
 * NAT statistics and attributes of a local load balancer.
 */
    public java.lang.String getLocalLBNATPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBNATPortType getLocalLBNATPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBNATPortType getLocalLBNATPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
