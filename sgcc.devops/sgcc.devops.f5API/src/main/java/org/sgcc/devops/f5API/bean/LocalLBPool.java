package org.sgcc.devops.f5API.bean;
/*
 * The contents of this file are subject to the "END USER LICENSE AGREEMENT FOR F5
 * Software Development Kit for iControl"; you may not use this file except in
 * compliance with the License. The License is included in the iControl
 * Software Development Kit.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is iControl Code and related documentation
 * distributed by F5.
 *
 * The Initial Developer of the Original Code is F5 Networks,
 * Inc. Seattle, WA, USA. Portions created by F5 are Copyright (C) 1996-2002 F5
 * Inc. All Rights Reserved.  iControl (TM) is a registered trademark of F5 Netw
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

import org.sgcc.devops.f5API.iControl.CommonIPPortDefinition;

public class LocalLBPool extends Object
{
	//--------------------------------------------------------------------------
	// Member Variables
	//--------------------------------------------------------------------------
	public String m_endpoint;
	public org.sgcc.devops.f5API.iControl.LocalLBPoolBindingStub m_pool;
	public org.sgcc.devops.f5API.iControl.LocalLBPoolMemberBindingStub m_poolMember;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public LocalLBPool()
	{
		System.setProperty("javax.net.ssl.trustStore", System.getProperty("user.home") + "/.keystore");
		XTrustProvider.install();
	}

	//--------------------------------------------------------------------------
	// parseArgs
	//--------------------------------------------------------------------------
	public boolean parseArgs(String[] args) throws Exception
	{
		boolean bSuccess = false;

		if ( args.length < 4 )
		{
			usage();
		}
		else
		{
			// build parameters

			m_endpoint = "https://" + args[2] + ":" + args[3] + "@" + args[0] + ":" + args[1] + "/iControl/iControlPortal.cgi";

			m_pool = (org.sgcc.devops.f5API.iControl.LocalLBPoolBindingStub)
				new org.sgcc.devops.f5API.iControl.LocalLBPoolLocator().getLocalLBPoolPort(new java.net.URL(m_endpoint));
			m_poolMember = (org.sgcc.devops.f5API.iControl.LocalLBPoolMemberBindingStub)
				new org.sgcc.devops.f5API.iControl.LocalLBPoolMemberLocator().getLocalLBPoolMemberPort(new java.net.URL(m_endpoint));

			m_pool.setTimeout(60000);
			m_poolMember.setTimeout(60000);

			bSuccess = true;
		}

		return bSuccess;
	}

	//--------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	public void usage()
	{
		System.out.println("Usage: LocalLBPool hostname port username password\n");
	}

	//--------------------------------------------------------------------------
	// 
	//--------------------------------------------------------------------------
	public void getInfo() throws Exception
	{
		int i = 0;
		int j = 0;
		int k = 0;

		// Get list of pools
		String[] poolList = m_pool.get_list();
		String[] aa = new String[]{"1"};
		String[][] bb = new String[][]{new String[]{"2"}};
		
		
		//给POOL增加member
		CommonIPPortDefinition[][] cid = new CommonIPPortDefinition[][]{new CommonIPPortDefinition[]{new CommonIPPortDefinition("10.134.160.8",80)}};
		//删除member
		m_pool.remove_member(new String[]{"QLC_TEST_POOL"}, cid);
		//添加member
		//m_pool.add_member(new String[]{"QLC_TEST_POOL"}, cid);
		// Get LBMethod
		org.sgcc.devops.f5API.iControl.LocalLBLBMethod[] lbMethodList = null;
		lbMethodList = m_pool.get_lb_method(poolList);
		
		// Get Min/cur active members
		long[] minMemberList = null;
		minMemberList = m_pool.get_minimum_active_member(poolList);

		long[] curMemberList = null;
		curMemberList = m_pool.get_active_member_count(poolList);

		// Get Statistics
		org.sgcc.devops.f5API.iControl.LocalLBPoolPoolStatistics poolStats = null;
		poolStats = m_pool.get_statistics(poolList);

		// Get member statistics
		org.sgcc.devops.f5API.iControl.LocalLBPoolMemberMemberStatistics[] memberStatList = null;
		memberStatList = m_poolMember.get_all_statistics(poolList);

		// print out summary
		for(i=0; i<poolList.length; i++)
		{
			System.out.println("POOL " + poolList[i] +
					"  LB METHOD " + lbMethodList[i] + 
					"  MIN/CUR ACTIVE MEMBERS: " + minMemberList[i] + "/" + curMemberList[i]);
			for(j=0; j<poolStats.getStatistics()[i].getStatistics().length; j++)
			{
				org.sgcc.devops.f5API.iControl.CommonStatisticType type = poolStats.getStatistics()[i].getStatistics()[j].getType();
				org.sgcc.devops.f5API.iControl.CommonULong64 value = poolStats.getStatistics()[i].getStatistics()[j].getValue();

			//	System.out.println("|    " + type + " : " + value.getLow());
			}

			// loop over members

			org.sgcc.devops.f5API.iControl.LocalLBPoolMemberMemberStatisticEntry[] memberEntryList = memberStatList[i].getStatistics();
			for(j=0; j<memberEntryList.length; j++)
			{
				org.sgcc.devops.f5API.iControl.CommonIPPortDefinition member = memberEntryList[j].getMember();
				org.sgcc.devops.f5API.iControl.CommonStatistic[] memberStats = memberEntryList[j].getStatistics();
			//	System.out.println("+-> MEMBER " + member.getAddress() + ":" + member.getPort());

				for(k=0; k<memberStats.length; k++)
				{
					org.sgcc.devops.f5API.iControl.CommonStatisticType type = memberStats[k].getType();
					org.sgcc.devops.f5API.iControl.CommonULong64 value = memberStats[k].getValue();
				//	System.out.println("    |    " + type + " : " + value.getLow());

				}
			}
		}

	}


	//--------------------------------------------------------------------------
	// Main
	//--------------------------------------------------------------------------
	public static void main(String[] args)
	{
		try
		{
			//访问地址用户名密码
			args = new String[]{"172.16.85.201","443","qlc","jsepc0!"};
			LocalLBPool localLBPool = new LocalLBPool();
			if ( localLBPool.parseArgs(args) )
			{
				localLBPool.getInfo();
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace(System.out);
		}
	}
};
