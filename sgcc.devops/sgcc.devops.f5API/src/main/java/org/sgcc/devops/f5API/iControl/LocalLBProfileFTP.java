/**
 * LocalLBProfileFTP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileFTP extends javax.xml.rpc.Service {

/**
 * The ProfileFTP interface enables you to manipulate a local load
 * balancer's FTP profile.
 */
    public java.lang.String getLocalLBProfileFTPPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFTPPortType getLocalLBProfileFTPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFTPPortType getLocalLBProfileFTPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
