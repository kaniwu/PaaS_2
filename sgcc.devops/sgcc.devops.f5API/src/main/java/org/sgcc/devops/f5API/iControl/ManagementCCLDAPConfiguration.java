/**
 * ManagementCCLDAPConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementCCLDAPConfiguration extends javax.xml.rpc.Service {

/**
 * The CCLDAPConfiguration interface enables you to manage SSL Client
 * Certificate LDAP PAM configuration.
 */
    public java.lang.String getManagementCCLDAPConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementCCLDAPConfigurationPortType getManagementCCLDAPConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementCCLDAPConfigurationPortType getManagementCCLDAPConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
