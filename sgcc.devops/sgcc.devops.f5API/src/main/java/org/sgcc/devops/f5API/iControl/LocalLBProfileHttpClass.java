/**
 * LocalLBProfileHttpClass.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileHttpClass extends javax.xml.rpc.Service {

/**
 * The ProfileHttpClass interface enables you to manipulate a local
 * load balancer's
 *  ability to classify HTTP traffic.
 */
    public java.lang.String getLocalLBProfileHttpClassPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileHttpClassPortType getLocalLBProfileHttpClassPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileHttpClassPortType getLocalLBProfileHttpClassPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
