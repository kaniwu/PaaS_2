/**
 * LocalLBPool.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBPool extends javax.xml.rpc.Service {

/**
 * The Pool interface enables you to work with attributes, and statistics
 * for pools.  
 *  You can also use this interface to create pools, add members to a
 * pool, delete members from a pool, 
 *  find out the load balancing mode for a pool, and set the load balancing
 * mode for a pool.
 */
    public java.lang.String getLocalLBPoolPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBPoolPortType getLocalLBPoolPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBPoolPortType getLocalLBPoolPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
