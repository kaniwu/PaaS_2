/**
 * NetworkingSTPInstance.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingSTPInstance extends javax.xml.rpc.Service {

/**
 * The STPInstance interface enables you to work with the definitions
 * and attributes associated with an STP instance.
 */
    public java.lang.String getNetworkingSTPInstancePortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingSTPInstancePortType getNetworkingSTPInstancePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingSTPInstancePortType getNetworkingSTPInstancePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
