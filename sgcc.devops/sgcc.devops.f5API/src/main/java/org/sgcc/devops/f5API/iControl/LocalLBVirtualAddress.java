/**
 * LocalLBVirtualAddress.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBVirtualAddress extends javax.xml.rpc.Service {

/**
 * The VirtualAddress interface enables you to work with the states,
 * statistics, limits, availability, 
 *  and settings of a local load balancer's virtual address.
 */
    public java.lang.String getLocalLBVirtualAddressPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBVirtualAddressPortType getLocalLBVirtualAddressPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBVirtualAddressPortType getLocalLBVirtualAddressPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
