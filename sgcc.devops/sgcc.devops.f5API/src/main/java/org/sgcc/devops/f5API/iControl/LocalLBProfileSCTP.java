/**
 * LocalLBProfileSCTP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileSCTP extends javax.xml.rpc.Service {

/**
 * The ProfileSCTP interface enables you to manipulate a local load
 * balancer's SCTP profile.
 */
    public java.lang.String getLocalLBProfileSCTPPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileSCTPPortType getLocalLBProfileSCTPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileSCTPPortType getLocalLBProfileSCTPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
