/**
 * ManagementOCSPConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementOCSPConfiguration extends javax.xml.rpc.Service {

/**
 * The OCSPConfiguration interface enables you to manage OCSP PAM
 * configuration.
 */
    public java.lang.String getManagementOCSPConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementOCSPConfigurationPortType getManagementOCSPConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementOCSPConfigurationPortType getManagementOCSPConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
