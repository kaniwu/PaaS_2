/**
 * ManagementEventNotification.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementEventNotification extends javax.xml.rpc.Service {

/**
 * The EventNotification interface is used for system configuration
 * change events that are configured with the EventSubscriptions interface.
 */
    public java.lang.String getManagementEventNotificationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementEventNotificationPortType getManagementEventNotificationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementEventNotificationPortType getManagementEventNotificationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
