/**
 * LocalLBRateClassRateUnit.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBRateClassRateUnit  implements java.io.Serializable {
    private long rate;
    private org.sgcc.devops.f5API.iControl.LocalLBRateClassUnitType unit;

    public LocalLBRateClassRateUnit() {
    }

    public LocalLBRateClassRateUnit(
           long rate,
           org.sgcc.devops.f5API.iControl.LocalLBRateClassUnitType unit) {
           this.rate = rate;
           this.unit = unit;
    }


    /**
     * Gets the rate value for this LocalLBRateClassRateUnit.
     * 
     * @return rate
     */
    public long getRate() {
        return rate;
    }


    /**
     * Sets the rate value for this LocalLBRateClassRateUnit.
     * 
     * @param rate
     */
    public void setRate(long rate) {
        this.rate = rate;
    }


    /**
     * Gets the unit value for this LocalLBRateClassRateUnit.
     * 
     * @return unit
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRateClassUnitType getUnit() {
        return unit;
    }


    /**
     * Sets the unit value for this LocalLBRateClassRateUnit.
     * 
     * @param unit
     */
    public void setUnit(org.sgcc.devops.f5API.iControl.LocalLBRateClassUnitType unit) {
        this.unit = unit;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocalLBRateClassRateUnit)) return false;
        LocalLBRateClassRateUnit other = (LocalLBRateClassRateUnit) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.rate == other.getRate() &&
            ((this.unit==null && other.getUnit()==null) || 
             (this.unit!=null &&
              this.unit.equals(other.getUnit())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Long(getRate()).hashCode();
        if (getUnit() != null) {
            _hashCode += getUnit().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocalLBRateClassRateUnit.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.RateClass.RateUnit"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "rate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unit");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unit"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.RateClass.UnitType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
