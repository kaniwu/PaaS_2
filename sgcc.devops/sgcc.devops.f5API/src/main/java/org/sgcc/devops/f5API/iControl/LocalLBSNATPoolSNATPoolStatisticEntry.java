/**
 * LocalLBSNATPoolSNATPoolStatisticEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBSNATPoolSNATPoolStatisticEntry  implements java.io.Serializable {
    private java.lang.String snat_pool;
    private org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics;

    public LocalLBSNATPoolSNATPoolStatisticEntry() {
    }

    public LocalLBSNATPoolSNATPoolStatisticEntry(
           java.lang.String snat_pool,
           org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
           this.snat_pool = snat_pool;
           this.statistics = statistics;
    }


    /**
     * Gets the snat_pool value for this LocalLBSNATPoolSNATPoolStatisticEntry.
     * 
     * @return snat_pool
     */
    public java.lang.String getSnat_pool() {
        return snat_pool;
    }


    /**
     * Sets the snat_pool value for this LocalLBSNATPoolSNATPoolStatisticEntry.
     * 
     * @param snat_pool
     */
    public void setSnat_pool(java.lang.String snat_pool) {
        this.snat_pool = snat_pool;
    }


    /**
     * Gets the statistics value for this LocalLBSNATPoolSNATPoolStatisticEntry.
     * 
     * @return statistics
     */
    public org.sgcc.devops.f5API.iControl.CommonStatistic[] getStatistics() {
        return statistics;
    }


    /**
     * Sets the statistics value for this LocalLBSNATPoolSNATPoolStatisticEntry.
     * 
     * @param statistics
     */
    public void setStatistics(org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
        this.statistics = statistics;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof LocalLBSNATPoolSNATPoolStatisticEntry)) return false;
        LocalLBSNATPoolSNATPoolStatisticEntry other = (LocalLBSNATPoolSNATPoolStatisticEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.snat_pool==null && other.getSnat_pool()==null) || 
             (this.snat_pool!=null &&
              this.snat_pool.equals(other.getSnat_pool()))) &&
            ((this.statistics==null && other.getStatistics()==null) || 
             (this.statistics!=null &&
              java.util.Arrays.equals(this.statistics, other.getStatistics())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSnat_pool() != null) {
            _hashCode += getSnat_pool().hashCode();
        }
        if (getStatistics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatistics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatistics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocalLBSNATPoolSNATPoolStatisticEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.SNATPool.SNATPoolStatisticEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snat_pool");
        elemField.setXmlName(new javax.xml.namespace.QName("", "snat_pool"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statistics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statistics"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Common.Statistic"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
