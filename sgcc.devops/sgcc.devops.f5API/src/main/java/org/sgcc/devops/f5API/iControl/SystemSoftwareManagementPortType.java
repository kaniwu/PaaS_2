/**
 * SystemSoftwareManagementPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemSoftwareManagementPortType extends java.rmi.Remote {

    /**
     * Cleanup the ramfs mounted at HF_MNT_LOCATION
     */
    public void cleanup_ramfs() throws java.rmi.RemoteException;

    /**
     * Create a RAM disk and return its path to the caller.
     */
    public java.lang.String create_ramfs() throws java.rmi.RemoteException;

    /**
     * Gets the file content that consists boot image information,
     * including what's installed in each of 
     *  the boot locations in the device.
     */
    public byte[] get_boot_image_information(boolean save_active_config) throws java.rmi.RemoteException;

    /**
     * Gets the configured default boot location, which will be the
     * location that boots after the system reboots.
     */
    public java.lang.String get_boot_location() throws java.rmi.RemoteException;

    /**
     * Gets information on any hotfixes applied to the system.  There
     * may not be
     *  any hotfix installed, in which case the returned sequence is empty.
     */
    public org.sgcc.devops.f5API.iControl.SystemSoftwareManagementHotFixInformation[] get_hotfix_information() throws java.rmi.RemoteException;

    /**
     * Gets the percent complete of the current live install processes.
     */
    public java.lang.String[] get_live_install_completion(org.sgcc.devops.f5API.iControl.SystemSoftwareManagementInstallationID[] installation_ids) throws java.rmi.RemoteException;

    /**
     * Gets the RPM packages installed on the device.
     */
    public java.lang.String[] get_rpm_package_information() throws java.rmi.RemoteException;

    /**
     * Gets a list of software images available in the repository.
     */
    public java.lang.String[] get_software_repository_list() throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Installs the specified hotfixes.
     */
    public void install_hotfix(java.lang.String[] hotfix_files, boolean reboot_system) throws java.rmi.RemoteException;

    /**
     * Calls bigstart stop and installs the specified hotfixes.  Upon
     * completion, if reboot_system
     *  is set to false, bigstart start is called to bring daemons back up.
     */
    public void install_hotfix_no_daemons(java.lang.String[] hotfix_files, boolean reboot_system) throws java.rmi.RemoteException;

    /**
     * Initiates an install of SW images on all blades installed on
     * one chassis.
     */
    public void install_software_image(java.lang.String install_volume, java.lang.String product, java.lang.String version, java.lang.String build) throws java.rmi.RemoteException;

    /**
     * Sets the boot image information and automatically initiates
     * installation based on the 
     *  boot image information.
     */
    public void set_boot_image_information(byte[] boot_image_information) throws java.rmi.RemoteException;

    /**
     * Sets the default boot location, which will be the boot location
     * that boots after the system reboots.
     */
    public void set_boot_location(java.lang.String location) throws java.rmi.RemoteException;
}
