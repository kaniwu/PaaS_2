/**
 * LocalLBSNATTranslationAddressLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBSNATTranslationAddressLocator extends org.apache.axis.client.Service implements org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddress {

/**
 * The SNATTranslationAddress interface enables you to work with the
 * definitions contained in a local load balancer's SNAT translation
 * address. 
 *  A translation address defines an address to which a client address
 * may be translated on the server side.
 */

    public LocalLBSNATTranslationAddressLocator() {
    }


    public LocalLBSNATTranslationAddressLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LocalLBSNATTranslationAddressLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LocalLBSNATTranslationAddressPort
    private java.lang.String LocalLBSNATTranslationAddressPort_address = "https://url_to_service";

    public java.lang.String getLocalLBSNATTranslationAddressPortAddress() {
        return LocalLBSNATTranslationAddressPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LocalLBSNATTranslationAddressPortWSDDServiceName = "LocalLB.SNATTranslationAddressPort";

    public java.lang.String getLocalLBSNATTranslationAddressPortWSDDServiceName() {
        return LocalLBSNATTranslationAddressPortWSDDServiceName;
    }

    public void setLocalLBSNATTranslationAddressPortWSDDServiceName(java.lang.String name) {
        LocalLBSNATTranslationAddressPortWSDDServiceName = name;
    }

    public org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressPortType getLocalLBSNATTranslationAddressPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LocalLBSNATTranslationAddressPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLocalLBSNATTranslationAddressPort(endpoint);
    }

    public org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressPortType getLocalLBSNATTranslationAddressPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressBindingStub _stub = new org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressBindingStub(portAddress, this);
            _stub.setPortName(getLocalLBSNATTranslationAddressPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLocalLBSNATTranslationAddressPortEndpointAddress(java.lang.String address) {
        LocalLBSNATTranslationAddressPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressBindingStub _stub = new org.sgcc.devops.f5API.iControl.LocalLBSNATTranslationAddressBindingStub(new java.net.URL(LocalLBSNATTranslationAddressPort_address), this);
                _stub.setPortName(getLocalLBSNATTranslationAddressPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LocalLB.SNATTranslationAddressPort".equals(inputPortName)) {
            return getLocalLBSNATTranslationAddressPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:iControl", "LocalLB.SNATTranslationAddress");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:iControl", "LocalLB.SNATTranslationAddressPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LocalLBSNATTranslationAddressPort".equals(portName)) {
            setLocalLBSNATTranslationAddressPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
