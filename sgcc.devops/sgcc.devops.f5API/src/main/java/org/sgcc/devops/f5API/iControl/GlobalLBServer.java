/**
 * GlobalLBServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBServer extends javax.xml.rpc.Service {

/**
 * The Server interface enables you to work with servers within a
 * data center.
 */
    public java.lang.String getGlobalLBServerPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBServerPortType getGlobalLBServerPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBServerPortType getGlobalLBServerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
