/**
 * LocalLBNATPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBNATPortType extends java.rmi.Remote {

    /**
     * Creates the specified NATs.
     */
    public void create(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions, long[] unit_ids, org.sgcc.devops.f5API.iControl.CommonVLANFilterList[] vlans) throws java.rmi.RemoteException;

    /**
     * Deletes all NATs.
     */
    public void delete_all_nats() throws java.rmi.RemoteException;

    /**
     * Deletes the specified NATs.
     */
    public void delete_nat(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Gets statistics for all NATs.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBNATNATStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets the ARP states for the specified NATs.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_arp_state(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Gets the states of the specified NAT addresses.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_enabled_state(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Gets a sequence of all NAT definitions.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets statistics for a sequence of NATs.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBNATNATStatistics get_statistics(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Gets the controller to which the specified NATs apply in an
     * active-active, redundant 
     *  load balancer configuration.
     */
    public long[] get_unit_id(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Gets the lists of VLANs on which access to the specified NATs
     * is disabled.
     */
    public org.sgcc.devops.f5API.iControl.CommonVLANFilterList[] get_vlan(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Resets statistics for a NAT.
     */
    public void reset_statistics(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions) throws java.rmi.RemoteException;

    /**
     * Sets the ARP states for the specified NATs.
     */
    public void set_arp_state(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the state for a list of NAT addresses.
     */
    public void set_enabled_state(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the Unit IDs for the specified NATs.
     */
    public void set_unit_id(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions, long[] unit_ids) throws java.rmi.RemoteException;

    /**
     * Adds VLANSs to the list of VLANs on which access to the specified
     * NATs is disabled.
     */
    public void set_vlan(org.sgcc.devops.f5API.iControl.LocalLBNATNATDefinition[] nat_definitions, org.sgcc.devops.f5API.iControl.CommonVLANFilterList[] vlans) throws java.rmi.RemoteException;
}
