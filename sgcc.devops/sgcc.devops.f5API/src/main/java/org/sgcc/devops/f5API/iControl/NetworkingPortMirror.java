/**
 * NetworkingPortMirror.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingPortMirror extends javax.xml.rpc.Service {

/**
 * The PortMirror interface enables you to work with the definitions
 * and attributes of port mirroring.
 */
    public java.lang.String getNetworkingPortMirrorPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingPortMirrorPortType getNetworkingPortMirrorPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingPortMirrorPortType getNetworkingPortMirrorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
