/**
 * SystemSoftwareManagement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemSoftwareManagement extends javax.xml.rpc.Service {

/**
 * The SoftwareManagement interface enables you to manage the software
 * installed on the system.
 */
    public java.lang.String getSystemSoftwareManagementPortAddress();

    public org.sgcc.devops.f5API.iControl.SystemSoftwareManagementPortType getSystemSoftwareManagementPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.SystemSoftwareManagementPortType getSystemSoftwareManagementPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
