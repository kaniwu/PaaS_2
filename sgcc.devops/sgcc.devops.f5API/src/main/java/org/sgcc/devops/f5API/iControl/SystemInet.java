/**
 * SystemInet.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemInet extends javax.xml.rpc.Service {

/**
 * The Inet interface exposes the internal API functionality that
 * you can use to manipulate the rc.conf and resolv.conf files.  The
 * ntp.conf file includes the functionality that you can use to set and
 * get the following settings:  host name, IP address (get only), router,
 * NTP server and DNS server.
 */
    public java.lang.String getSystemInetPortAddress();

    public org.sgcc.devops.f5API.iControl.SystemInetPortType getSystemInetPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.SystemInetPortType getSystemInetPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
