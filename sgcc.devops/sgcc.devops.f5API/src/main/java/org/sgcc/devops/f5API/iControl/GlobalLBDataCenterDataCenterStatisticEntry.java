/**
 * GlobalLBDataCenterDataCenterStatisticEntry.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class GlobalLBDataCenterDataCenterStatisticEntry  implements java.io.Serializable {
    private java.lang.String data_center;
    private org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics;

    public GlobalLBDataCenterDataCenterStatisticEntry() {
    }

    public GlobalLBDataCenterDataCenterStatisticEntry(
           java.lang.String data_center,
           org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
           this.data_center = data_center;
           this.statistics = statistics;
    }


    /**
     * Gets the data_center value for this GlobalLBDataCenterDataCenterStatisticEntry.
     * 
     * @return data_center
     */
    public java.lang.String getData_center() {
        return data_center;
    }


    /**
     * Sets the data_center value for this GlobalLBDataCenterDataCenterStatisticEntry.
     * 
     * @param data_center
     */
    public void setData_center(java.lang.String data_center) {
        this.data_center = data_center;
    }


    /**
     * Gets the statistics value for this GlobalLBDataCenterDataCenterStatisticEntry.
     * 
     * @return statistics
     */
    public org.sgcc.devops.f5API.iControl.CommonStatistic[] getStatistics() {
        return statistics;
    }


    /**
     * Sets the statistics value for this GlobalLBDataCenterDataCenterStatisticEntry.
     * 
     * @param statistics
     */
    public void setStatistics(org.sgcc.devops.f5API.iControl.CommonStatistic[] statistics) {
        this.statistics = statistics;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GlobalLBDataCenterDataCenterStatisticEntry)) return false;
        GlobalLBDataCenterDataCenterStatisticEntry other = (GlobalLBDataCenterDataCenterStatisticEntry) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.data_center==null && other.getData_center()==null) || 
             (this.data_center!=null &&
              this.data_center.equals(other.getData_center()))) &&
            ((this.statistics==null && other.getStatistics()==null) || 
             (this.statistics!=null &&
              java.util.Arrays.equals(this.statistics, other.getStatistics())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getData_center() != null) {
            _hashCode += getData_center().hashCode();
        }
        if (getStatistics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getStatistics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getStatistics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GlobalLBDataCenterDataCenterStatisticEntry.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "GlobalLB.DataCenter.DataCenterStatisticEntry"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data_center");
        elemField.setXmlName(new javax.xml.namespace.QName("", "data_center"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statistics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statistics"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:iControl", "Common.Statistic"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
