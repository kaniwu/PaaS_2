/**
 * GlobalLBVirtualServer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBVirtualServer extends javax.xml.rpc.Service {

/**
 * The VirtualServer interface enables you to work with virtual servers
 * associated with a server.
 */
    public java.lang.String getGlobalLBVirtualServerPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerPortType getGlobalLBVirtualServerPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBVirtualServerPortType getGlobalLBVirtualServerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
