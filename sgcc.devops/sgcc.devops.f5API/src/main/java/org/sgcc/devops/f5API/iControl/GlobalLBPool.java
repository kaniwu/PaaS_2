/**
 * GlobalLBPool.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBPool extends javax.xml.rpc.Service {

/**
 * The Pool interface enables you to work with pools and their attributes.
 */
    public java.lang.String getGlobalLBPoolPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBPoolPortType getGlobalLBPoolPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBPoolPortType getGlobalLBPoolPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
