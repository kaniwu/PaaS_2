/**
 * LocalLBProfileXML.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileXML extends javax.xml.rpc.Service {

/**
 * The ProfileXML interface enables you to manipulate a local load
 * balancer's XML profile.
 */
    public java.lang.String getLocalLBProfileXMLPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileXMLPortType getLocalLBProfileXMLPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileXMLPortType getLocalLBProfileXMLPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
