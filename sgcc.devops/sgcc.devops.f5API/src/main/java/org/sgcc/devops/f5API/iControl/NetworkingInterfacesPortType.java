/**
 * NetworkingInterfacesPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingInterfacesPortType extends java.rmi.Remote {

    /**
     * Gets the active media types of the specified interface names.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesMediaType[] get_active_media(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the actual/effective flow control types for the specified
     * interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingFlowControlType[] get_actual_flow_control(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the statistics of all interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesInterfaceStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether the interface is a dual
     * media
     *  port supporting both fixed copper and SFP.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_dual_media_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states of the specified interface names.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_enabled_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the configured learning modes of the specified interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingLearningMode[] get_learning_mode(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets a list of all interfaces on this device.
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the MAC addresses of the specified interface names.
     */
    public java.lang.String[] get_mac_address(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the configured media types of the specified interface
     * names.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesMediaType[] get_media(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the media options for the specified interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesInterfaceMediaOption[] get_media_option(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the SFP media options for the specified interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesInterfaceMediaOption[] get_media_option_sfp(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the configured SFP media types of the specified interface
     * names.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesMediaType[] get_media_sfp(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the media speeds of the specified interface names. Unit
     * = Mbps.
     */
    public long[] get_media_speed(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the media status of the specified interface names.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingMediaStatus[] get_media_status(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the MTUs of the specified interface names.
     */
    public long[] get_mtu(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the configured SFP media types of the specified interface
     * names.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingPhyMasterSlaveMode[] get_phy_master_slave_mode(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating that SFP is the preferred media.
     * Only used for dual media ports.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_prefer_sfp_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the configured/requested flow control types for the specified
     * interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingFlowControlType[] get_requested_flow_control(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether the interface supports SFP
     * media.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_sfp_media_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the statistics of the specified interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesInterfaceStatistics get_statistics(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the active states indicating whether the specified interfaces
     * are edge ports or not. The 
     *  spanning tree algorithms include important optimizations that can
     * only be used on so-called 
     *  edge ports, i.e. interfaces that connect to end stations rather than
     * to other bridges. 
     *  Note: This active state is reported by STP daemon.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_stp_active_edge_port_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether automatic detection of edge
     * port status for the
     *  specified interfaces.  When automatic edge port detection is enabled
     * on an interface, 
     *  the BIG-IP monitors the interface for incoming STP, RSTP, or MSTP
     * packets.  If no such 
     *  packets are received for a sufficient period of time (about 3 seconds),
     * the interface 
     *  is automatically given edge port status.  When automatic edge port
     * detection is disabled 
     *  on an interface, the BIG-IP never gives the interface edge port status
     * automatically.
     *  By default, automatic edge port detection is enabled on all interfaces.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_stp_auto_edge_port_detection_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether the specified interfaces
     * are edge ports or not. The 
     *  spanning tree algorithms include important optimizations that can
     * only be used on so-called 
     *  edge ports, i.e. interfaces that connect to end stations rather than
     * to other bridges.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_stp_edge_port_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether the specified interfaces
     * will participate in the
     *  spanning tree protocol.  Disabling spanning tree protocol on an interface
     * enables 
     *  learning and forwarding on it.  The spanning tree algorithm then
     * behaves as if the 
     *  interface did not exist.  No STP, RSTP, or MSTP packets are transmitted
     * or received
     *  on the interface, and the spanning tree algorithm exerts no control
     * over forwarding 
     *  or learning on the port.
     * 
     *  NOTE: Disabling spanning tree protocol on an interface which is a
     * configured member of 
     *  a trunk disables spanning tree protocol on the trunk as a whole.
     * For a trunk to 
     *  participate in spanning tree protocol, the protocol must be enabled
     * on all of its 
     *  configured member interfaces.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_stp_enabled_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the STP link types for the specified interfaces.
     */
    public org.sgcc.devops.f5API.iControl.NetworkingSTPLinkType[] get_stp_link_type(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the states indicating whether RSTP or MSTP BPDUs (depending
     * on the current STP
     *  mode) to be sent on the specified interfaces, until such time  as
     * a legacy STP bridge
     *  is detected again on those interfaces.
     * 
     *  Note: This method is only applicable when the current STP mode is
     * RSTP or MSTP.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_stp_protocol_detection_reset_state(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Resets the statistics of the specified interfaces.
     */
    public void reset_statistics(java.lang.String[] interfaces) throws java.rmi.RemoteException;

    /**
     * Sets the states of the specified interface names.
     */
    public void set_enabled_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the learning modes for the specified interfaces.
     */
    public void set_learning_mode(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingLearningMode[] modes) throws java.rmi.RemoteException;

    /**
     * Sets the media types of the specified interface names.
     */
    public void set_media(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingInterfacesMediaType[] media_types) throws java.rmi.RemoteException;

    /**
     * Sets the SFP media types of the specified interface names.
     */
    public void set_media_sfp(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingInterfacesMediaType[] media_types) throws java.rmi.RemoteException;

    /**
     * Sets the SFP media types of the specified interface names.
     */
    public void set_phy_master_slave_mode(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingPhyMasterSlaveMode[] modes) throws java.rmi.RemoteException;

    /**
     * Sets the states indicating that SFP is the preferred media.
     * Only used for dual media ports.
     */
    public void set_prefer_sfp_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the configured/requested flow control types for the specified
     * interfaces.
     */
    public void set_requested_flow_control(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingFlowControlType[] flow_controls) throws java.rmi.RemoteException;

    /**
     * Sets the states indicating whether automatic detection of edge
     * port status for the
     *  specified interfaces.  When automatic edge port detection is enabled
     * on an interface, 
     *  the BIG-IP monitors the interface for incoming STP, RSTP, or MSTP
     * packets.  If no such 
     *  packets are received for a sufficient period of time (about 3 seconds),
     * the interface 
     *  is automatically given edge port status.  When automatic edge port
     * detection is disabled 
     *  on an interface, the BIG-IP never gives the interface edge port status
     * automatically.
     *  By default, automatic edge port detection is enabled on all interfaces.
     */
    public void set_stp_auto_edge_port_detection_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the states indicating whether the specified interfaces
     * are edge ports or not. The 
     *  spanning tree algorithms include important optimizations that can
     * only be used on so-called 
     *  edge ports, i.e. interfaces that connect to end stations rather than
     * to other bridges.
     */
    public void set_stp_edge_port_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the states indicating whether the specified interfaces
     * will participate in the
     *  spanning tree protocol.  Disabling spanning tree protocol on an interface
     * enables 
     *  learning and forwarding on it.  The spanning tree algorithm then
     * behaves as if the 
     *  interface did not exist.  No STP, RSTP, or MSTP packets are transmitted
     * or received
     *  on the interface, and the spanning tree algorithm exerts no control
     * over forwarding 
     *  or learning on the port.
     * 
     *  NOTE: Disabling spanning tree protocol on an interface which is a
     * configured member of 
     *  a trunk disables spanning tree protocol on the trunk as a whole.
     * For a trunk to 
     *  participate in spanning tree protocol, the protocol must be enabled
     * on all of its 
     *  configured member interfaces.
     */
    public void set_stp_enabled_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the STP link types for the specified interfaces.
     */
    public void set_stp_link_type(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.NetworkingSTPLinkType[] link_types) throws java.rmi.RemoteException;

    /**
     * Sets the states indicating whether RSTP or MSTP BPDUs (depending
     * on the current STP
     *  mode) to be sent on the specified interfaces, until such time  as
     * a legacy STP bridge
     *  is detected again on those interfaces.
     * 
     *  Note: This method is only applicable when the current STP mode is
     * RSTP or MSTP.
     */
    public void set_stp_protocol_detection_reset_state(java.lang.String[] interfaces, org.sgcc.devops.f5API.iControl.CommonEnabledState[] states) throws java.rmi.RemoteException;
}
