/**
 * LocalLBProfileXMLPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileXMLPortType extends java.rmi.Remote {

    /**
     * Creates the specified XML profiles.
     */
    public void create(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Deletes all XML profiles.
     */
    public void delete_all_profiles() throws java.rmi.RemoteException;

    /**
     * Deletes the specified XML profiles.
     */
    public void delete_profile(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Gets the state that if true, abort the connection on errors.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBProfileEnabledState[] get_abort_on_error_state(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for all the XML profiles.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBProfileXMLProfileXMLStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets the names of the default profiles from which the specified
     * profiles will derive
     *  default values for its attributes.
     */
    public java.lang.String[] get_default_profile(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Gets a list of all XML profiles.
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the values that specify the maximum buffer size for the
     * specified profiles.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBProfileULong[] get_maximum_buffer_size(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for the specified XML profiles.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBProfileXMLProfileXMLStatistics get_statistics(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Determines whether the specified profiles are base/pre-configured
     * profiles, or user-defined profiles.
     */
    public boolean[] is_base_profile(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Resets the statistics for the specified XML profiles.
     */
    public void reset_statistics(java.lang.String[] profile_names) throws java.rmi.RemoteException;

    /**
     * Sets the state that if true, abort the connection on errors.
     */
    public void set_abort_on_error_state(java.lang.String[] profile_names, org.sgcc.devops.f5API.iControl.LocalLBProfileEnabledState[] states) throws java.rmi.RemoteException;

    /**
     * Sets the names of the default profiles from which the specified
     * profiles will derive
     *  default values for its attributes.
     */
    public void set_default_profile(java.lang.String[] profile_names, java.lang.String[] defaults) throws java.rmi.RemoteException;

    /**
     * Sets the values that specify the maximum buffer size for the
     * specified profiles.
     */
    public void set_maximum_buffer_size(java.lang.String[] profile_names, org.sgcc.devops.f5API.iControl.LocalLBProfileULong[] sizes) throws java.rmi.RemoteException;
}
