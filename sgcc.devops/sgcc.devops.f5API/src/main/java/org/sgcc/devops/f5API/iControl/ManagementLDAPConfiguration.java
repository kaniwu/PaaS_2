/**
 * ManagementLDAPConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementLDAPConfiguration extends javax.xml.rpc.Service {

/**
 * The LDAPConfiguration interface enables you to manage LDAP PAM
 * configuration.
 */
    public java.lang.String getManagementLDAPConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementLDAPConfigurationPortType getManagementLDAPConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementLDAPConfigurationPortType getManagementLDAPConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
