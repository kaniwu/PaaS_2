/**
 * ManagementDBVariable.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementDBVariable extends javax.xml.rpc.Service {

/**
 * The DBVariable interface exposes methods that enable you to work
 * directly with our internal 
 *  database that contains configuration variables using name/value pairs.
 */
    public java.lang.String getManagementDBVariablePortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementDBVariablePortType getManagementDBVariablePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementDBVariablePortType getManagementDBVariablePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
