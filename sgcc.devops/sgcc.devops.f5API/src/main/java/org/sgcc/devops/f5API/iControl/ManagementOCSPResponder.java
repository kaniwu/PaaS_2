/**
 * ManagementOCSPResponder.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementOCSPResponder extends javax.xml.rpc.Service {

/**
 * The OCSPResponder interface enables you to manage OCSP responder
 * configuration.
 */
    public java.lang.String getManagementOCSPResponderPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementOCSPResponderPortType getManagementOCSPResponderPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementOCSPResponderPortType getManagementOCSPResponderPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
