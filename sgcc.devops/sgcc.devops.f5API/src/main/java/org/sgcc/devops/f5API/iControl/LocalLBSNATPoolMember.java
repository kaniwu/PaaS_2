/**
 * LocalLBSNATPoolMember.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBSNATPoolMember extends javax.xml.rpc.Service {

/**
 * The SNATPoolMember interface enables you to work with the SNATPool
 * members and their settings, and statistics.
 */
    public java.lang.String getLocalLBSNATPoolMemberPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolMemberPortType getLocalLBSNATPoolMemberPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolMemberPortType getLocalLBSNATPoolMemberPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
