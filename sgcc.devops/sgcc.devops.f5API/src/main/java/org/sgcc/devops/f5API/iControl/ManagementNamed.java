/**
 * ManagementNamed.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementNamed extends javax.xml.rpc.Service {

/**
 * The Named interface provides the calls to manipulate the named.conf
 * and the named server
 */
    public java.lang.String getManagementNamedPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementNamedPortType getManagementNamedPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementNamedPortType getManagementNamedPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
