/**
 * NetworkingSelfIPPortLockdown.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingSelfIPPortLockdown extends javax.xml.rpc.Service {

/**
 * The SelfIPPortLockdown interface enables you to lock down protocols
 * and ports on self IP addresses.
 */
    public java.lang.String getNetworkingSelfIPPortLockdownPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingSelfIPPortLockdownPortType getNetworkingSelfIPPortLockdownPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingSelfIPPortLockdownPortType getNetworkingSelfIPPortLockdownPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
