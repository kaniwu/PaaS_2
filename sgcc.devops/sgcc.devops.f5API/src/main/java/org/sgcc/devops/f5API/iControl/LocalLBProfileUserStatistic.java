/**
 * LocalLBProfileUserStatistic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileUserStatistic extends javax.xml.rpc.Service {

/**
 * The ProfileUserStatistic interface enables you to manage user-defined
 * statistics in a profile.
 *  The user can define up to 32 user-defined statistics in each profile.
 */
    public java.lang.String getLocalLBProfileUserStatisticPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileUserStatisticPortType getLocalLBProfileUserStatisticPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileUserStatisticPortType getLocalLBProfileUserStatisticPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
