/**
 * ManagementView.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementView extends javax.xml.rpc.Service {

/**
 * The View interface contains all calls necessary to manipulate views
 */
    public java.lang.String getManagementViewPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementViewPortType getManagementViewPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementViewPortType getManagementViewPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
