/**
 * LocalLBProfilePersistence.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfilePersistence extends javax.xml.rpc.Service {

/**
 * The ProfilePersistence interface enables you to manipulate a local
 * load balancer's Persistence profile.
 */
    public java.lang.String getLocalLBProfilePersistencePortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfilePersistencePortType getLocalLBProfilePersistencePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfilePersistencePortType getLocalLBProfilePersistencePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
