/**
 * ASMWebApplicationGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ASMWebApplicationGroup extends javax.xml.rpc.Service {

/**
 * The WebApplicationGroup interface enables you to manipulate a 
 *  group of ASM Web Applications.
 */
    public java.lang.String getASMWebApplicationGroupPortAddress();

    public org.sgcc.devops.f5API.iControl.ASMWebApplicationGroupPortType getASMWebApplicationGroupPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ASMWebApplicationGroupPortType getASMWebApplicationGroupPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
