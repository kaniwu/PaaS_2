/**
 * ASMPolicyPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ASMPolicyPortType extends java.rmi.Remote {

    /**
     * Create policies for the specified web applications.
     */
    public void create(java.lang.String[] webapp_names, java.lang.String[][] policy_names) throws java.rmi.RemoteException;

    /**
     * Deletes a policy.
     */
    public void delete_policy(java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Downloads the policy from the device.
     */
    public org.sgcc.devops.f5API.iControl.ASMFileTransferContext download_policy(java.lang.String policy_name, long chunk_size, javax.xml.rpc.holders.LongHolder file_offset) throws java.rmi.RemoteException;

    /**
     * Export Policy
     */
    public void export_policy(java.lang.String policy_name, java.lang.String filename) throws java.rmi.RemoteException;

    /**
     * Get the max cookie length.
     */
    public long[] get_cookie_length(java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Get blocking flag.
     */
    public boolean[] get_disable_blocking_flag(java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Get max http header length.
     */
    public long[] get_http_header_length(java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Gets violation blocking flag.
     */
    public org.sgcc.devops.f5API.iControl.ASMViolation[][] get_violation_flags(java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Import Policy
     */
    public void import_policy(java.lang.String webapp_name, java.lang.String filename) throws java.rmi.RemoteException;

    /**
     * Updates max cookie length.
     */
    public void set_cookie_length(java.lang.String[] policy_names, long[] cookie_lengths) throws java.rmi.RemoteException;

    /**
     * Updates blocking flag.
     */
    public void set_disable_blocking_flag(java.lang.String[] policy_names, boolean[] blocking_flags) throws java.rmi.RemoteException;

    /**
     * Updates max http header length.
     */
    public void set_http_header_length(java.lang.String[] policy_names, long[] http_header_lengths) throws java.rmi.RemoteException;

    /**
     * Updates violation blocking flag.
     */
    public void set_violation_flags(java.lang.String[] policy_names, org.sgcc.devops.f5API.iControl.ASMViolation[][] violations) throws java.rmi.RemoteException;

    /**
     * Uploads the policy to the device.
     */
    public void upload_policy(java.lang.String policy_name, org.sgcc.devops.f5API.iControl.ASMFileTransferContext file_context) throws java.rmi.RemoteException;
}
