/**
 * NetworkingARP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingARP extends javax.xml.rpc.Service {

/**
 * The ARP interface enables you to work with the ARP table and entries.
 */
    public java.lang.String getNetworkingARPPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingARPPortType getNetworkingARPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingARPPortType getNetworkingARPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
