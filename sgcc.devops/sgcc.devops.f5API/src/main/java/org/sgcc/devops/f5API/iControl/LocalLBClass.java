/**
 * LocalLBClass.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBClass extends javax.xml.rpc.Service {

/**
 * The Class interface enables you to manipulate a local load balancer's
 * Class attributes. 
 *  There are 3 different Class types: Address, String, and Value.
 */
    public java.lang.String getLocalLBClassPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBClassPortType getLocalLBClassPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBClassPortType getLocalLBClassPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
