/**
 * LTConfigField.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LTConfigField extends javax.xml.rpc.Service {

/**
 * The Field interface enables you to manage fields and field
 *  instances in the Loosely-Typed Configuration system.
 */
    public java.lang.String getLTConfigFieldPortAddress();

    public org.sgcc.devops.f5API.iControl.LTConfigFieldPortType getLTConfigFieldPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LTConfigFieldPortType getLTConfigFieldPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
