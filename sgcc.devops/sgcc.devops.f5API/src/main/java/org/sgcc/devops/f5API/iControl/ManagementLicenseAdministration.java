/**
 * ManagementLicenseAdministration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementLicenseAdministration extends javax.xml.rpc.Service {

/**
 * The LicenseAdministration interface exposes methods that enable
 * you to authorize the system,
 *  either manually or in an automated fashion.  This interface allows
 * you to generate license
 *  files, install previously generated licenses, and view other licensing
 * characteristics.
 */
    public java.lang.String getManagementLicenseAdministrationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementLicenseAdministrationPortType getManagementLicenseAdministrationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementLicenseAdministrationPortType getManagementLicenseAdministrationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
