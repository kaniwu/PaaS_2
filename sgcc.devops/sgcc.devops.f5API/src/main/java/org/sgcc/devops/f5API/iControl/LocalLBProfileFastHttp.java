/**
 * LocalLBProfileFastHttp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileFastHttp extends javax.xml.rpc.Service {

/**
 * The ProfileFastHttp interface enables you to manipulate a local
 * load balancer's Fast HTTP profile.
 */
    public java.lang.String getLocalLBProfileFastHttpPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFastHttpPortType getLocalLBProfileFastHttpPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFastHttpPortType getLocalLBProfileFastHttpPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
