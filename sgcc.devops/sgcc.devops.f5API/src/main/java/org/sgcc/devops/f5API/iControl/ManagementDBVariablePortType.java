/**
 * ManagementDBVariablePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementDBVariablePortType extends java.rmi.Remote {

    /**
     * Creates the specified variables in the database.
     */
    public void create(org.sgcc.devops.f5API.iControl.ManagementDBVariableVariableNameValue[] variables) throws java.rmi.RemoteException;

    /**
     * Deletes the variables referenced from the database.
     */
    public void delete_variable(java.lang.String[] variables) throws java.rmi.RemoteException;

    /**
     * Retrieves the values of all variables defined in the database.
     * This list can potentially be huge.
     */
    public org.sgcc.devops.f5API.iControl.ManagementDBVariableVariableNameValue[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Verifies the existence of the specified variables in the database.
     */
    public boolean[] is_variable_available(java.lang.String[] variables) throws java.rmi.RemoteException;

    /**
     * Modifies the specified variables in the database.
     */
    public void modify(org.sgcc.devops.f5API.iControl.ManagementDBVariableVariableNameValue[] variables) throws java.rmi.RemoteException;

    /**
     * Queries the values of the specified variables.
     */
    public org.sgcc.devops.f5API.iControl.ManagementDBVariableVariableNameValue[] query(java.lang.String[] variables) throws java.rmi.RemoteException;
}
