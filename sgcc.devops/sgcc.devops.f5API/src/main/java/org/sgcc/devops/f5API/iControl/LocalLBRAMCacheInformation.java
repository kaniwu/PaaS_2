/**
 * LocalLBRAMCacheInformation.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBRAMCacheInformation extends javax.xml.rpc.Service {

/**
 * The RAMCacheInformation interface enables you to query for RAM
 * cache entries/statistics, 
 *  as well as evicting RAM cache entries.
 */
    public java.lang.String getLocalLBRAMCacheInformationPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBRAMCacheInformationPortType getLocalLBRAMCacheInformationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBRAMCacheInformationPortType getLocalLBRAMCacheInformationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
