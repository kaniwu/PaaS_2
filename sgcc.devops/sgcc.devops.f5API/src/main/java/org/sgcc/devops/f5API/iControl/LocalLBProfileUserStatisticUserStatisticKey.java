/**
 * LocalLBProfileUserStatisticUserStatisticKey.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public class LocalLBProfileUserStatisticUserStatisticKey implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected LocalLBProfileUserStatisticUserStatisticKey(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _USER_STATISTIC_UNDEFINED = "USER_STATISTIC_UNDEFINED";
    public static final java.lang.String _USER_STATISTIC_1 = "USER_STATISTIC_1";
    public static final java.lang.String _USER_STATISTIC_2 = "USER_STATISTIC_2";
    public static final java.lang.String _USER_STATISTIC_3 = "USER_STATISTIC_3";
    public static final java.lang.String _USER_STATISTIC_4 = "USER_STATISTIC_4";
    public static final java.lang.String _USER_STATISTIC_5 = "USER_STATISTIC_5";
    public static final java.lang.String _USER_STATISTIC_6 = "USER_STATISTIC_6";
    public static final java.lang.String _USER_STATISTIC_7 = "USER_STATISTIC_7";
    public static final java.lang.String _USER_STATISTIC_8 = "USER_STATISTIC_8";
    public static final java.lang.String _USER_STATISTIC_9 = "USER_STATISTIC_9";
    public static final java.lang.String _USER_STATISTIC_10 = "USER_STATISTIC_10";
    public static final java.lang.String _USER_STATISTIC_11 = "USER_STATISTIC_11";
    public static final java.lang.String _USER_STATISTIC_12 = "USER_STATISTIC_12";
    public static final java.lang.String _USER_STATISTIC_13 = "USER_STATISTIC_13";
    public static final java.lang.String _USER_STATISTIC_14 = "USER_STATISTIC_14";
    public static final java.lang.String _USER_STATISTIC_15 = "USER_STATISTIC_15";
    public static final java.lang.String _USER_STATISTIC_16 = "USER_STATISTIC_16";
    public static final java.lang.String _USER_STATISTIC_17 = "USER_STATISTIC_17";
    public static final java.lang.String _USER_STATISTIC_18 = "USER_STATISTIC_18";
    public static final java.lang.String _USER_STATISTIC_19 = "USER_STATISTIC_19";
    public static final java.lang.String _USER_STATISTIC_20 = "USER_STATISTIC_20";
    public static final java.lang.String _USER_STATISTIC_21 = "USER_STATISTIC_21";
    public static final java.lang.String _USER_STATISTIC_22 = "USER_STATISTIC_22";
    public static final java.lang.String _USER_STATISTIC_23 = "USER_STATISTIC_23";
    public static final java.lang.String _USER_STATISTIC_24 = "USER_STATISTIC_24";
    public static final java.lang.String _USER_STATISTIC_25 = "USER_STATISTIC_25";
    public static final java.lang.String _USER_STATISTIC_26 = "USER_STATISTIC_26";
    public static final java.lang.String _USER_STATISTIC_27 = "USER_STATISTIC_27";
    public static final java.lang.String _USER_STATISTIC_28 = "USER_STATISTIC_28";
    public static final java.lang.String _USER_STATISTIC_29 = "USER_STATISTIC_29";
    public static final java.lang.String _USER_STATISTIC_30 = "USER_STATISTIC_30";
    public static final java.lang.String _USER_STATISTIC_31 = "USER_STATISTIC_31";
    public static final java.lang.String _USER_STATISTIC_32 = "USER_STATISTIC_32";
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_UNDEFINED = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_UNDEFINED);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_1 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_1);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_2 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_2);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_3 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_3);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_4 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_4);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_5 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_5);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_6 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_6);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_7 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_7);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_8 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_8);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_9 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_9);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_10 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_10);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_11 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_11);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_12 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_12);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_13 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_13);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_14 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_14);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_15 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_15);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_16 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_16);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_17 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_17);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_18 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_18);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_19 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_19);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_20 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_20);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_21 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_21);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_22 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_22);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_23 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_23);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_24 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_24);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_25 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_25);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_26 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_26);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_27 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_27);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_28 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_28);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_29 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_29);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_30 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_30);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_31 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_31);
    public static final LocalLBProfileUserStatisticUserStatisticKey USER_STATISTIC_32 = new LocalLBProfileUserStatisticUserStatisticKey(_USER_STATISTIC_32);
    public java.lang.String getValue() { return _value_;}
    public static LocalLBProfileUserStatisticUserStatisticKey fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        LocalLBProfileUserStatisticUserStatisticKey enumeration = (LocalLBProfileUserStatisticUserStatisticKey)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static LocalLBProfileUserStatisticUserStatisticKey fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(LocalLBProfileUserStatisticUserStatisticKey.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:iControl", "LocalLB.ProfileUserStatistic.UserStatisticKey"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
