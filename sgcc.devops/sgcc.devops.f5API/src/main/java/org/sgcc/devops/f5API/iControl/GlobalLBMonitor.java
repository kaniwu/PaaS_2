/**
 * GlobalLBMonitor.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBMonitor extends javax.xml.rpc.Service {

/**
 * The Monitor interface enables you to manipulate a load balancer's
 * monitor templates and instances.  
 *  For example, use the Monitor interface to enable or disable a monitor
 * instance, or to create a monitor template,
 *  or to get and set different attributes of a monitor template.
 */
    public java.lang.String getGlobalLBMonitorPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBMonitorPortType getGlobalLBMonitorPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBMonitorPortType getGlobalLBMonitorPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
