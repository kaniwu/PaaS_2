/**
 * LocalLBSNATPool.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBSNATPool extends javax.xml.rpc.Service {

/**
 * The SNATPool interface enables you to work with the definitions
 * contained in a local load balancer's SNAT pool,
 *  which is a set of translation addresses.  Either a virtual server,
 * an iRule or a top-level SNAT object can 
 *  reference a SNAT pool.
 */
    public java.lang.String getLocalLBSNATPoolPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolPortType getLocalLBSNATPoolPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBSNATPoolPortType getLocalLBSNATPoolPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
