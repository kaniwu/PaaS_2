/**
 * GlobalLBDataCenter.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBDataCenter extends javax.xml.rpc.Service {

/**
 * The DataCenter interface enables you to manipulate the data center
 * attributes for a Global TM.  
 *  For example, use the DataCenter interface to add or remove a data
 * center, transfer server assignments 
 *  from one data center to another, get and set data center attributes,
 * remove a server from a data center, 
 *  and so on.
 */
    public java.lang.String getGlobalLBDataCenterPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBDataCenterPortType getGlobalLBDataCenterPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBDataCenterPortType getGlobalLBDataCenterPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
