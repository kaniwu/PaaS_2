/**
 * GlobalLBRegion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBRegion extends javax.xml.rpc.Service {

/**
 * The Region interface enables you to work with user-defined region
 * definitions.
 */
    public java.lang.String getGlobalLBRegionPortAddress();

    public org.sgcc.devops.f5API.iControl.GlobalLBRegionPortType getGlobalLBRegionPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.GlobalLBRegionPortType getGlobalLBRegionPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
