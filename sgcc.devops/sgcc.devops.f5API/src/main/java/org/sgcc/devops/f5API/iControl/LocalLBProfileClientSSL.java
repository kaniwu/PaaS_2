/**
 * LocalLBProfileClientSSL.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileClientSSL extends javax.xml.rpc.Service {

/**
 * The ProfileClientSSL interface enables you to manipulate a local
 * load balancer's client SSL profile.
 */
    public java.lang.String getLocalLBProfileClientSSLPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileClientSSLPortType getLocalLBProfileClientSSLPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileClientSSLPortType getLocalLBProfileClientSSLPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
