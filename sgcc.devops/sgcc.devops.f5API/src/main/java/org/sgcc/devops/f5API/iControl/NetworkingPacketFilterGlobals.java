/**
 * NetworkingPacketFilterGlobals.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingPacketFilterGlobals extends javax.xml.rpc.Service {

/**
 * The PacketFilterGlobals interface enables you to work with the
 * global lists of trusted source addresses and ingress VLANs used
 *  in packet filtering.
 */
    public java.lang.String getNetworkingPacketFilterGlobalsPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingPacketFilterGlobalsPortType getNetworkingPacketFilterGlobalsPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingPacketFilterGlobalsPortType getNetworkingPacketFilterGlobalsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
