/**
 * SystemClusterPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemClusterPortType extends java.rmi.Remote {

    /**
     * Gets the cluster enabled states.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[] get_cluster_enabled_state(java.lang.String[] cluster_names) throws java.rmi.RemoteException;

    /**
     * Gets the primary slot numbers
     */
    public long[] get_current_primary_slot(java.lang.String[] cluster_names) throws java.rmi.RemoteException;

    /**
     * Gets a list of the cluster names
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the cluster member enabled state.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[][] get_member_enabled_state(java.lang.String[] cluster_names, long[][] slot_ids) throws java.rmi.RemoteException;

    /**
     * Gets the cluster member licensed state.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[][] get_member_licensed_state(java.lang.String[] cluster_names, long[][] slot_ids) throws java.rmi.RemoteException;

    /**
     * Gets the cluster member priming state.
     */
    public org.sgcc.devops.f5API.iControl.CommonEnabledState[][] get_member_priming_state(java.lang.String[] cluster_names, long[][] slot_ids) throws java.rmi.RemoteException;

    /**
     * Get a list of list of slot ids (the size of each list is the
     * number of slots).
     */
    public long[][] get_slot_id(java.lang.String[] cluster_names) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Sets the cluster enabled states.
     */
    public void set_cluster_enabled_state(java.lang.String[] cluster_names, org.sgcc.devops.f5API.iControl.CommonEnabledState[] cluster_states) throws java.rmi.RemoteException;

    /**
     * Sets the primary slot numbers
     */
    public void set_current_primary_slot(java.lang.String[] cluster_names, long[] primary_slots) throws java.rmi.RemoteException;

    /**
     * Sets the cluster member enabled state.
     */
    public void set_member_enabled_state(java.lang.String[] cluster_names, long[][] slot_ids, org.sgcc.devops.f5API.iControl.CommonEnabledState[][] member_states) throws java.rmi.RemoteException;

    /**
     * Sets the cluster member priming states.
     */
    public void set_member_priming_state(java.lang.String[] cluster_names, long[][] slot_ids, org.sgcc.devops.f5API.iControl.CommonEnabledState[][] priming_states) throws java.rmi.RemoteException;
}
