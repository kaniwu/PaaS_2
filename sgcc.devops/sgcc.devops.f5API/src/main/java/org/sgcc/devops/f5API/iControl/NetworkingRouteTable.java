/**
 * NetworkingRouteTable.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingRouteTable extends javax.xml.rpc.Service {

/**
 * The RouteTable interface enables you to work with the Route table
 * and entries.
 */
    public java.lang.String getNetworkingRouteTablePortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingRouteTablePortType getNetworkingRouteTablePort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingRouteTablePortType getNetworkingRouteTablePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
