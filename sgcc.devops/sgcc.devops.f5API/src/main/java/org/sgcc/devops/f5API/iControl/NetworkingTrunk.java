/**
 * NetworkingTrunk.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingTrunk extends javax.xml.rpc.Service {

/**
 * The Trunk interface enables you to work with the definitions and
 * attributes contained in a device's trunk.
 */
    public java.lang.String getNetworkingTrunkPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingTrunkPortType getNetworkingTrunkPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingTrunkPortType getNetworkingTrunkPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
