/**
 * ManagementTACACSConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementTACACSConfiguration extends javax.xml.rpc.Service {

/**
 * The TACACSConfiguration interface enables you to manage TACACS
 * PAM configuration.
 */
    public java.lang.String getManagementTACACSConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementTACACSConfigurationPortType getManagementTACACSConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementTACACSConfigurationPortType getManagementTACACSConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
