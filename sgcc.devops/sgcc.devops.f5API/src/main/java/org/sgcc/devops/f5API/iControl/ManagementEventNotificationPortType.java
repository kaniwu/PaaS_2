/**
 * ManagementEventNotificationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementEventNotificationPortType extends java.rmi.Remote {

    /**
     * The notification mechanism used to send event notification
     * messages.
     * Clients must implement this interface to act as an endpoint of event
     * notifications.
     */
    public void events_occurred(org.sgcc.devops.f5API.iControl.ManagementEventNotificationEventSource event_source, java.lang.String subscription_id, org.sgcc.devops.f5API.iControl.ManagementEventNotificationEventData[] event_data_list, org.sgcc.devops.f5API.iControl.CommonTimeStamp time_stamp) throws java.rmi.RemoteException;
}
