/**
 * LTConfigClass.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LTConfigClass extends javax.xml.rpc.Service {

/**
 * The Class interface enables you to manage classes and class
 *  instances in the Loosely-Typed Configuration system.
 */
    public java.lang.String getLTConfigClassPortAddress();

    public org.sgcc.devops.f5API.iControl.LTConfigClassPortType getLTConfigClassPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LTConfigClassPortType getLTConfigClassPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
