/**
 * LocalLBProfileFastL4.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileFastL4 extends javax.xml.rpc.Service {

/**
 * The ProfileFastL4 interface enables you to manipulate a local load
 * balancer's L4 profile.
 */
    public java.lang.String getLocalLBProfileFastL4PortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFastL4PortType getLocalLBProfileFastL4Port() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileFastL4PortType getLocalLBProfileFastL4Port(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
