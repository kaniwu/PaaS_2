/**
 * SystemSystemInfoPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemSystemInfoPortType extends java.rmi.Remote {

    /**
     * Attempts to acquire lock with specified name for specified
     * number of seconds. 
     *  These locks can be used to synchronize work of multiple clients working
     * with this
     *  device. This call returns immediately.
     */
    public boolean acquire_lock(java.lang.String lock_name, long duration_sec, java.lang.String comment) throws java.rmi.RemoteException;

    /**
     * Gets the CPU usage extended information for this system by
     * host ID for all hosts.
     */
    public org.sgcc.devops.f5API.iControl.SystemCPUUsageExtendedInformation get_all_cpu_usage_extended_information() throws java.rmi.RemoteException;

    /**
     * Gets the CPU metrics for the CPU(s) on the platform.
     */
    public org.sgcc.devops.f5API.iControl.SystemPlatformCPUs get_cpu_metrics() throws java.rmi.RemoteException;

    /**
     * Gets the CPU usage extended information for this system by
     * host ID.
     */
    public org.sgcc.devops.f5API.iControl.SystemCPUUsageExtendedInformation get_cpu_usage_extended_information(java.lang.String[] host_ids) throws java.rmi.RemoteException;

    /**
     * This method has been deprecated; use get_cpu_usage_extended_information
     * and
     *  related methods instead.
     * 
     *  Gets the CPU usage information for this system.
     */
    public org.sgcc.devops.f5API.iControl.SystemCPUUsageInformation get_cpu_usage_information() throws java.rmi.RemoteException;

    /**
     * Gets the disk usage information for this system.
     */
    public org.sgcc.devops.f5API.iControl.SystemDiskUsageInformation get_disk_usage_information() throws java.rmi.RemoteException;

    /**
     * Gets the Fan metrics for the Fan(s) on the platform.
     */
    public org.sgcc.devops.f5API.iControl.SystemPlatformFans get_fan_metrics() throws java.rmi.RemoteException;

    /**
     * Gets a globally unique identifier for the system.
     */
    public java.lang.String get_globally_unique_identifier() throws java.rmi.RemoteException;

    /**
     * Gets the unique identifier for the configsync or sync group
     * of which this device is a member.
     */
    public java.lang.String get_group_id() throws java.rmi.RemoteException;

    /**
     * Lists all names of currently acquired locks.
     */
    public java.lang.String[] get_lock_list() throws java.rmi.RemoteException;

    /**
     * Gets lock statuses of specified locks
     */
    public org.sgcc.devops.f5API.iControl.SystemLockStatus[] get_lock_status(java.lang.String[] lock_names) throws java.rmi.RemoteException;

    /**
     * This method has been deprecated; use get_host_statistics and
     * related methods instead.
     * 
     *  Gets the memory information for this system.
     */
    public org.sgcc.devops.f5API.iControl.SystemMemoryUsageInformation get_memory_usage_information() throws java.rmi.RemoteException;

    /**
     * Gets the Power Supply metrics for the Power Supplies on the
     * platform.
     */
    public org.sgcc.devops.f5API.iControl.SystemPlatformPowerSupplies get_power_supply_metrics() throws java.rmi.RemoteException;

    /**
     * Gets a list of attributes of installed product.
     */
    public org.sgcc.devops.f5API.iControl.SystemProductInformation get_product_information() throws java.rmi.RemoteException;

    /**
     * Gets the unique identifier for the system.
     */
    public java.lang.String get_system_id() throws java.rmi.RemoteException;

    /**
     * Gets the system-identifying attributes of the operating system.
     */
    public org.sgcc.devops.f5API.iControl.SystemSystemInformation get_system_information() throws java.rmi.RemoteException;

    /**
     * Gets the current chassis temperatures.
     */
    public org.sgcc.devops.f5API.iControl.SystemPlatformTemperatures get_temperature_metrics() throws java.rmi.RemoteException;

    /**
     * Gets the system time in UTC.
     */
    public org.sgcc.devops.f5API.iControl.CommonTimeStamp get_time() throws java.rmi.RemoteException;

    /**
     * Gets the local time zone information.
     */
    public org.sgcc.devops.f5API.iControl.CommonTimeZone get_time_zone() throws java.rmi.RemoteException;

    /**
     * Gets the number of seconds the device has been running.
     */
    public long get_uptime() throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Attempts to release lock with specified name. These locks can
     * be used to synchronize
     *  work of multiple clients working with this device. This call returns
     * immediately.
     *  Nothing will happen if specified lock doesn't exist.
     */
    public void release_lock(java.lang.String lock_name) throws java.rmi.RemoteException;

    /**
     * Sets the unique identifier for the configsync or sync group
     * of which this device is a member.
     */
    public void set_group_id(java.lang.String group_id) throws java.rmi.RemoteException;
}
