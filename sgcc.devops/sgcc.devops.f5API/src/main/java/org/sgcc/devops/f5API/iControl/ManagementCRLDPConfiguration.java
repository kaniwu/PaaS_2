/**
 * ManagementCRLDPConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementCRLDPConfiguration extends javax.xml.rpc.Service {

/**
 * The CRLDPConfiguration interface enables you to manage CRLDP PAM
 * configuration.
 */
    public java.lang.String getManagementCRLDPConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementCRLDPConfigurationPortType getManagementCRLDPConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementCRLDPConfigurationPortType getManagementCRLDPConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
