/**
 * ManagementResourceRecordPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementResourceRecordPortType extends java.rmi.Remote {

    /**
     * Adds DNS "A" Records.
     */
    public void add_a(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementARecord[][] a_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Adds DNS "AAAA" Records.
     */
    public void add_aaaa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementAAAARecord[][] aaaa_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Adds DNS "CNAME" Records.
     */
    public void add_cname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementCNAMERecord[][] cname_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "DNAME" Records.
     */
    public void add_dname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementDNAMERecord[][] dname_records) throws java.rmi.RemoteException;

    /**
     * Adds  DNS "HINFO" Records.
     */
    public void add_hinfo(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementHINFORecord[][] hinfo_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "MX" Records.
     */
    public void add_mx(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementMXRecord[][] mx_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "NS" Records.
     */
    public void add_ns(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementNSRecord[][] ns_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "PTR" Records.
     */
    public void add_ptr(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementPTRRecord[][] ptr_records) throws java.rmi.RemoteException;

    /**
     * Adds a list of RRS to a view/zone
     */
    public void add_rrs(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementRRList[] rr_lists, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Adds DNS "SOA" Records.
     */
    public void add_soa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSOARecord[][] soa_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "SRV" Records.
     */
    public void add_srv(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSRVRecord[][] srv_records) throws java.rmi.RemoteException;

    /**
     * Adds DNS "TXT" Records.
     */
    public void add_txt(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementTXTRecord[][] txt_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "A" Records.
     */
    public void delete_a(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementARecord[][] a_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * deletes DNS "A6" Records.
     */
    public void delete_a6(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementA6Record[][] a6_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "AAAA" Records.
     */
    public void delete_aaaa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementAAAARecord[][] aaaa_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "CNAME" Records.
     */
    public void delete_cname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementCNAMERecord[][] cname_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "DNAME" Records.
     */
    public void delete_dname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementDNAMERecord[][] dname_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "HINFO" Records.
     */
    public void delete_hinfo(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementHINFORecord[][] hinfo_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "KEY" Records.
     */
    public void delete_key(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementKEYRecord[][] key_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "MX" Records.
     */
    public void delete_mx(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementMXRecord[][] mx_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "NS" Records.
     */
    public void delete_ns(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementNSRecord[][] ns_records) throws java.rmi.RemoteException;

    /**
     * deletes DNS "NXT" Records.
     */
    public void delete_nxt(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementNXTRecord[][] nxt_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "PTR" Records.
     */
    public void delete_ptr(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementPTRRecord[][] ptr_records) throws java.rmi.RemoteException;

    /**
     * deletes DNS "SIG" Records.
     */
    public void delete_sig(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSIGRecord[][] sig_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "SOA" Records.
     */
    public void delete_soa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSOARecord[][] soa_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "SRV" Records.
     */
    public void delete_srv(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSRVRecord[][] srv_records) throws java.rmi.RemoteException;

    /**
     * Deletes DNS "TXT" Records.
     */
    public void delete_txt(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementTXTRecord[][] txt_records) throws java.rmi.RemoteException;

    /**
     * Gets a list of resource records for a specified view/zone
     */
    public java.lang.String[][] get_rrs(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones) throws java.rmi.RemoteException;

    /**
     * Gets a list of resource records for the specified view/zone
     * information.
     */
    public org.sgcc.devops.f5API.iControl.ManagementRRList[] get_rrs_detailed(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "A" Records.
     */
    public void update_a(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementARecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementARecord[][] new_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "AAAA" Records.
     */
    public void update_aaaa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementAAAARecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementAAAARecord[][] new_records, boolean[] sync_ptrs) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "CNAME" Records.
     */
    public void update_cname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementCNAMERecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementCNAMERecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "DNAME" Records.
     */
    public void update_dname(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementDNAMERecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementDNAMERecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "HINFO" Records.
     */
    public void update_hinfo(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementHINFORecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementHINFORecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "MX" Records.
     */
    public void update_mx(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementMXRecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementMXRecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "NS" Records.
     */
    public void update_ns(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementNSRecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementNSRecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "PTR" Records.
     */
    public void update_ptr(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementPTRRecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementPTRRecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "SOA" Records.
     */
    public void update_soa(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSOARecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementSOARecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "SRV" Records.
     */
    public void update_srv(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementSRVRecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementSRVRecord[][] new_records) throws java.rmi.RemoteException;

    /**
     * Updates or changes DNS "TXT" Records.
     */
    public void update_txt(org.sgcc.devops.f5API.iControl.ManagementViewZone[] view_zones, org.sgcc.devops.f5API.iControl.ManagementTXTRecord[][] old_records, org.sgcc.devops.f5API.iControl.ManagementTXTRecord[][] new_records) throws java.rmi.RemoteException;
}
