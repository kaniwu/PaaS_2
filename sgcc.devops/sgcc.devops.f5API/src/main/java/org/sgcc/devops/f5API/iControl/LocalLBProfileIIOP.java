/**
 * LocalLBProfileIIOP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileIIOP extends javax.xml.rpc.Service {

/**
 * The ProfileIIOP interface enables you to manipulate a local load
 * balancer's IIOP profile.
 */
    public java.lang.String getLocalLBProfileIIOPPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileIIOPPortType getLocalLBProfileIIOPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileIIOPPortType getLocalLBProfileIIOPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
