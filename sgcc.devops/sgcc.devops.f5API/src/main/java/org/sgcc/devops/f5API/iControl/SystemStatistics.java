/**
 * SystemStatistics.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface SystemStatistics extends javax.xml.rpc.Service {

/**
 * The Statistics interface enables you to get information on various
 * system statistics.
 */
    public java.lang.String getSystemStatisticsPortAddress();

    public org.sgcc.devops.f5API.iControl.SystemStatisticsPortType getSystemStatisticsPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.SystemStatisticsPortType getSystemStatisticsPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
