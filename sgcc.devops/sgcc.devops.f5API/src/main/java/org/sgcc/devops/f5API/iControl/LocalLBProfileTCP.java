/**
 * LocalLBProfileTCP.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBProfileTCP extends javax.xml.rpc.Service {

/**
 * The ProfileTCP interface enables you to manipulate a local load
 * balancer's TCP profile.
 */
    public java.lang.String getLocalLBProfileTCPPortAddress();

    public org.sgcc.devops.f5API.iControl.LocalLBProfileTCPPortType getLocalLBProfileTCPPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.LocalLBProfileTCPPortType getLocalLBProfileTCPPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
