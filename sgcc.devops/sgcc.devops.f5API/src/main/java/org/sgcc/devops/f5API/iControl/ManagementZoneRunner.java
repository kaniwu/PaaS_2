/**
 * ManagementZoneRunner.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementZoneRunner extends javax.xml.rpc.Service {

/**
 * The ZoneRunner interface handles all the top level calls
 */
    public java.lang.String getManagementZoneRunnerPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementZoneRunnerPortType getManagementZoneRunnerPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementZoneRunnerPortType getManagementZoneRunnerPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
