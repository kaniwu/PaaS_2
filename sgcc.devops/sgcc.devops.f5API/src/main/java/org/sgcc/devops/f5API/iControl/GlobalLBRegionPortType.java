/**
 * GlobalLBRegionPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface GlobalLBRegionPortType extends java.rmi.Remote {

    /**
     * Adds the specified region items to the specified region definitions.
     */
    public void add_region_item(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions, org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionItem[][] items) throws java.rmi.RemoteException;

    /**
     * Creates the specified region definitions with the specified
     * region items.
     */
    public void create(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions, org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionItem[][] items) throws java.rmi.RemoteException;

    /**
     * Deletes all region definitions.
     */
    public void delete_all_region_definitions() throws java.rmi.RemoteException;

    /**
     * Deletes the specified region definitions.
     */
    public void delete_region_definition(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions) throws java.rmi.RemoteException;

    /**
     * Gets a list of of region definitions.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the list of region items that define the specified regions.
     */
    public org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionItem[][] get_region_item(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Removes any and all region items in the specified regions.
     */
    public void remove_all_region_items(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions) throws java.rmi.RemoteException;

    /**
     * Removes the specified region items from the specified region
     * definitions.
     */
    public void remove_region_item(org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionDefinition[] regions, org.sgcc.devops.f5API.iControl.GlobalLBRegionRegionItem[][] items) throws java.rmi.RemoteException;
}
