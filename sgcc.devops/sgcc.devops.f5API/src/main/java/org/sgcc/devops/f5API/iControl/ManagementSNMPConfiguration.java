/**
 * ManagementSNMPConfiguration.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ManagementSNMPConfiguration extends javax.xml.rpc.Service {

/**
 * The SNMPConfiguration interface allows users to manage the full
 * configurations 
 *  for UCD SNMP agent.
 *  Please read the manual pages for snmpd.conf for further information.
 * 
 * 
 * TODO:
 * Missing directives:
 * - override
 */
    public java.lang.String getManagementSNMPConfigurationPortAddress();

    public org.sgcc.devops.f5API.iControl.ManagementSNMPConfigurationPortType getManagementSNMPConfigurationPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.ManagementSNMPConfigurationPortType getManagementSNMPConfigurationPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
