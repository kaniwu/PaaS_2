/**
 * ASMWebApplicationPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface ASMWebApplicationPortType extends java.rmi.Remote {

    /**
     * Deletes all disabled web applications.
     */
    public void delete_all_disabled_webapps() throws java.rmi.RemoteException;

    /**
     * Deletes all non-actice policies for web applications. The active
     * policy will not be deleted.
     */
    public void delete_all_non_active_policies(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Deletes the specified web applications.  Only disabled web
     * applications can be deleted.
     */
    public void delete_webapp(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Get the active policy for the specified web applications.
     */
    public java.lang.String[] get_active_policy(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Get the "apply learning" for the specified web applications.
     */
    public org.sgcc.devops.f5API.iControl.ASMApplyLearning[] get_apply_learning(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Get the "dynamic sessions in URL" for the specified web applications.
     */
    public org.sgcc.devops.f5API.iControl.ASMDynamicSessionsInUrl[] get_dynamic_sessions_in_url(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Returns the enabled flag of specified web application.
     */
    public boolean[] get_enabled_state(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Gets the language for the specified web applications.
     */
    public org.sgcc.devops.f5API.iControl.ASMWebApplicationLanguage[] get_language(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Gets a list of all web applications.
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Returns a list of the policies associated with the specified
     * web application.
     */
    public java.lang.String[][] get_policy_list(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Reconfigures the specified web applications.  This resets all
     * of the web application properties.
     */
    public void reconfigure(java.lang.String[] webapp_names) throws java.rmi.RemoteException;

    /**
     * Set the active policy for the specified web applications.
     *  This applies the specified policy to the enforcer.
     */
    public void set_active_policy(java.lang.String[] webapp_names, java.lang.String[] policy_names) throws java.rmi.RemoteException;

    /**
     * Set the "apply learning" for the specified web applications.
     */
    public void set_apply_learning(java.lang.String[] webapp_names, org.sgcc.devops.f5API.iControl.ASMApplyLearning[] settings) throws java.rmi.RemoteException;

    /**
     * Set the "dynamic sessions in URL" for the specified web applications.
     */
    public void set_dynamic_sessions_in_url(java.lang.String[] webapp_names, org.sgcc.devops.f5API.iControl.ASMDynamicSessionsInUrl[] settings) throws java.rmi.RemoteException;

    /**
     * Set the language for the specified web applications.
     *  Note that the language can only be set once for a web application.
     */
    public void set_language(java.lang.String[] webapp_names, org.sgcc.devops.f5API.iControl.ASMWebApplicationLanguage[] languages) throws java.rmi.RemoteException;
}
