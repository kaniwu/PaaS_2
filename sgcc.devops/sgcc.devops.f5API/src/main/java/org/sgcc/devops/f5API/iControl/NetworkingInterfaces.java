/**
 * NetworkingInterfaces.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface NetworkingInterfaces extends javax.xml.rpc.Service {

/**
 * The Interface interface enables you to work with the definitions
 * and attributes contained in a device's interface.
 */
    public java.lang.String getNetworkingInterfacesPortAddress();

    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesPortType getNetworkingInterfacesPort() throws javax.xml.rpc.ServiceException;

    public org.sgcc.devops.f5API.iControl.NetworkingInterfacesPortType getNetworkingInterfacesPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
