/**
 * LocalLBRulePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.2.1 Jun 14, 2005 (09:15:57 EDT) WSDL2Java emitter.
 */

package org.sgcc.devops.f5API.iControl;

public interface LocalLBRulePortType extends java.rmi.Remote {

    /**
     * Creates the specified rules.
     */
    public void create(org.sgcc.devops.f5API.iControl.LocalLBRuleRuleDefinition[] rules) throws java.rmi.RemoteException;

    /**
     * Deletes all user-defined rules.
     */
    public void delete_all_rules() throws java.rmi.RemoteException;

    /**
     * Deletes the specified rules.  An exception will be raised when
     * attempting to delete
     *  a default system rule.
     */
    public void delete_rule(java.lang.String[] rule_names) throws java.rmi.RemoteException;

    /**
     * Gets the statistics for all the rules.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRuleRuleStatistics get_all_statistics() throws java.rmi.RemoteException;

    /**
     * Gets a list of all rules.
     */
    public java.lang.String[] get_list() throws java.rmi.RemoteException;

    /**
     * Gets the statistics for the specified rules.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRuleRuleStatistics get_statistics(java.lang.String[] rule_names) throws java.rmi.RemoteException;

    /**
     * Gets the version information for this interface.
     */
    public java.lang.String get_version() throws java.rmi.RemoteException;

    /**
     * Modifies the specified rules.
     */
    public void modify_rule(org.sgcc.devops.f5API.iControl.LocalLBRuleRuleDefinition[] rules) throws java.rmi.RemoteException;

    /**
     * Queries all rules.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRuleRuleDefinition[] query_all_rules() throws java.rmi.RemoteException;

    /**
     * Queries the specified rules.
     */
    public org.sgcc.devops.f5API.iControl.LocalLBRuleRuleDefinition[] query_rule(java.lang.String[] rule_names) throws java.rmi.RemoteException;

    /**
     * Resets the statistics for the specified rules.
     */
    public void reset_statistics(java.lang.String[] rule_names) throws java.rmi.RemoteException;
}
