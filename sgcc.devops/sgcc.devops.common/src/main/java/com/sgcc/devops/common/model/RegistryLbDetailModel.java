package com.sgcc.devops.common.model;

public class RegistryLbDetailModel {
	/**
	 *              reg_name: reg_name,        仓库名称
            		reg_loadtype: reg_loadtype,仓库负载类型
            		f5_Id: f5_Id,    f5id
            		nginx_Id: nginx_Id,  选择nginx的id
            		nginx_port:nginx_port, nginx的端口
            		load_domain:load_domain,负载域名
            		load_ip:load_ip,负载主机ip
            		load_port:load_port,负载端口
            		host_Id:host_Id,主机
            		host_port:host_port,主机端口
            		standHost_Id:standHost_Id,备用主机ip
            		standHost_port:standHost_port,备用主机端口
            		reg_desc:reg_desc 仓库备注
	 */
     private String id ;
     private String reg_name ;
     private String reg_loadtype ;
     private String f5_Id ;
     private String nginx_Id ;
     private String nginx_port ;
     private String load_domain ;
     private String load_ip ;
     private String load_port ;
     private String host_Id ;
     private String host_port ;
     private String standHost_Id ;
     private String standHost_port ;
     private String reg_desc ;
     private String registryIns ;
     private String dockerParam;
     public String getDockerIns() {
		return dockerIns;
	}
	public void setDockerIns(String dockerIns) {
		this.dockerIns = dockerIns;
	}
	private String dockerIns ;
	public String getRegistryIns() {
		return registryIns;
	}
	public void setRegistryIns(String registryIns) {
		this.registryIns = registryIns;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getReg_name() {
		return reg_name;
	}
	public void setReg_name(String reg_name) {
		this.reg_name = reg_name;
	}
	public String getReg_loadtype() {
		return reg_loadtype;
	}
	public void setReg_loadtype(String reg_loadtype) {
		this.reg_loadtype = reg_loadtype;
	}
	public String getF5_Id() {
		return f5_Id;
	}
	public void setF5_Id(String f5_Id) {
		this.f5_Id = f5_Id;
	}
	public String getNginx_Id() {
		return nginx_Id;
	}
	public void setNginx_Id(String nginx_Id) {
		this.nginx_Id = nginx_Id;
	}
	public String getNginx_port() {
		return nginx_port;
	}
	public void setNginx_port(String nginx_port) {
		this.nginx_port = nginx_port;
	}
	public String getLoad_domain() {
		return load_domain;
	}
	public void setLoad_domain(String load_domain) {
		this.load_domain = load_domain;
	}
	public String getLoad_ip() {
		return load_ip;
	}
	public void setLoad_ip(String load_ip) {
		this.load_ip = load_ip;
	}
	public String getLoad_port() {
		return load_port;
	}
	public void setLoad_port(String load_port) {
		this.load_port = load_port;
	}
	public String getHost_Id() {
		return host_Id;
	}
	public void setHost_Id(String host_Id) {
		this.host_Id = host_Id;
	}
	public String getHost_port() {
		return host_port;
	}
	public void setHost_port(String host_port) {
		this.host_port = host_port;
	}
	public String getStandHost_Id() {
		return standHost_Id;
	}
	public void setStandHost_Id(String standHost_Id) {
		this.standHost_Id = standHost_Id;
	}
	public String getStandHost_port() {
		return standHost_port;
	}
	public void setStandHost_port(String standHost_port) {
		this.standHost_port = standHost_port;
	}
	public String getReg_desc() {
		return reg_desc;
	}
	public void setReg_desc(String reg_desc) {
		this.reg_desc = reg_desc;
	}
	public String getDockerParam() {
		return dockerParam;
	}
	public void setDockerParam(String dockerParam) {
		this.dockerParam = dockerParam;
	}
	
}	
	