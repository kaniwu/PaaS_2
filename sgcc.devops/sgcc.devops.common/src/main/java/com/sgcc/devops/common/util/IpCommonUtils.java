package com.sgcc.devops.common.util;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class IpCommonUtils {
	/**
	 * 获得本机IP
	 * @return
	 */
	public final static String getIpAddress(){
		return getNetworkIps()[0];
	}
	
	public static String[] getNetworkIps(){
		List<String> ipList=new ArrayList<String>();
		Enumeration<NetworkInterface> netInts=null;
		try{
			netInts=NetworkInterface.getNetworkInterfaces();
		}catch(SocketException e){
		}
		if (netInts!=null){
			while (netInts.hasMoreElements()){
				NetworkInterface netInt=netInts.nextElement();
				for(InterfaceAddress address:netInt.getInterfaceAddresses()){
					if(address.getAddress() instanceof Inet4Address){
						Inet4Address inet4Address = (Inet4Address)address.getAddress();
						String ipStr=inet4Address.getHostAddress();
						if (!"127.0.0.1".equals(ipStr)) ipList.add(ipStr);
					}
				}
			}
		}
		if (ipList.size()==0){
			try {
				String ipStr=InetAddress.getLocalHost().getHostAddress();
				if (ipStr.equals("127.0.0.1")){
					ipStr=InetAddress.getLocalHost().getHostName();
				}
				ipList.add(ipStr);
			} catch (UnknownHostException e1) {
				throw new RuntimeException(e1);
			}
		}
		if (ipList.size()==0) ipList.add("127.0.0.1");
		return ipList.toArray(new String[ipList.size()]);
	}
}
