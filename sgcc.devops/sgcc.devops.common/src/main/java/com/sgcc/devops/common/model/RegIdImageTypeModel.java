package com.sgcc.devops.common.model;

/**
 * 仓库镜像关系模板类
 * @author dmw
 *
 */
public class RegIdImageTypeModel {
	private String registryId;
	private Byte imageStatus;

	private int page;
	private int limit;
	private String search;

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public Byte getImageStatus() {
		return imageStatus;
	}

	public void setImageStatus(Byte imageStatus) {
		this.imageStatus = imageStatus;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

}
