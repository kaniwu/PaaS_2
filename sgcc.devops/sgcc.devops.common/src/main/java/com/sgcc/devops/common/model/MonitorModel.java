package com.sgcc.devops.common.model;

/**
 * 监控数据模板类
 * @author dmw
 *
 */
public class MonitorModel implements Comparable<MonitorModel> {

	private String monitorId;

	private Byte targetType;

	private String targetId;

	private Double value;
	
	private Double cpu;
	 
	private Double mem;
	 
	private Double diskSpace;

	public String getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(String monitorId) {
		this.monitorId = monitorId;
	}

	public Byte getTargetType() {
		return targetType;
	}

	public void setTargetType(Byte targetType) {
		this.targetType = targetType;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	
	public Double getCpu() {
		return cpu;
	}

	public void setCpu(Double cpu) {
		this.cpu = cpu;
	}

	public Double getMem() {
		return mem;
	}

	public void setMem(Double mem) {
		this.mem = mem;
	}

	public Double getDiskSpace() {
		return diskSpace;
	}

	public void setDiskSpace(Double diskSpace) {
		this.diskSpace = diskSpace;
	}

	@Override
	public int compareTo(MonitorModel o) {
		if (value > o.value) {
			return -1;
		} else if (value < o.value) {
			return 1;
		}
		return 0;
	}

	public MonitorModel(String monitorId, Byte targetType, String targetId, Double value,
			Double cpu,Double mem,Double diskSpace) {
		super();
		this.monitorId = monitorId;
		this.targetType = targetType;
		this.targetId = targetId;
		this.value = value;
		this.cpu = cpu;
		this.mem = mem;
		this.diskSpace = diskSpace;
	}

	public MonitorModel() {
		super();
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}
	public MonitorModel(String monitorId, Byte targetType, String targetId, Double value) {
		super();
		this.monitorId = monitorId;
		this.targetType = targetType;
		this.targetId = targetId;
		this.value = value;
	}
}
