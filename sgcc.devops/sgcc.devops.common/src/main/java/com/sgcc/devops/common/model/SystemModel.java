/**
 * 
 */
package com.sgcc.devops.common.model;

/**
 * date：2015年9月21日 上午8:54:06 project name：sgcc-devops-common
 * 物理系统模板类
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：SystemModel.java description：
 */
public class SystemModel {
	private String systemId;

	private String physicalId;

	private String systemName;
	
	private String systemCode;

	private String businessId;

	private String systemVersion;

	private String runningVersion;

	private String newestVersion;

	private Byte systemDeployStatus;

	private String businessName;

	private Integer systemType;

	private String deployId;

	private String url;

	private Byte operation;
	
	private Byte systemElsasticityStatus;

	private String supporter;//负责人
	
	private Integer instanceCount;//实际实例个数
	
	private Integer initCount;//初始化实例个数
	
	public String getPhysicalId() {

		return physicalId;
	}

	public void setPhysicalId(String physicalId) {
		this.physicalId = physicalId;
	}

	public Integer getSystemType() {
		return systemType;
	}

	public void setSystemType(Integer systemType) {
		this.systemType = systemType;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getSystemVersion() {
		return systemVersion;
	}

	public void setSystemVersion(String systemVersion) {
		this.systemVersion = systemVersion;
	}

	public String getRunningVersion() {
		return runningVersion;
	}

	public void setRunningVersion(String runningVersion) {
		this.runningVersion = runningVersion;
	}

	public String getNewestVersion() {
		return newestVersion;
	}

	public void setNewestVersion(String newestVersion) {
		this.newestVersion = newestVersion;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public Byte getSystemDeployStatus() {
		return systemDeployStatus;
	}

	public void setSystemDeployStatus(Byte systemDeployStatus) {
		this.systemDeployStatus = systemDeployStatus;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getDeployId() {
		return deployId;
	}

	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Byte getSystemElsasticityStatus() {
		return systemElsasticityStatus;
	}

	public void setSystemElsasticityStatus(Byte systemElsasticityStatus) {
		this.systemElsasticityStatus = systemElsasticityStatus;
	}

	public Byte getOperation() {
		return operation;
	}

	public void setOperation(Byte operation) {
		this.operation = operation;
	}

	public String getSupporter() {
		return supporter;
	}

	public void setSupporter(String supporter) {
		this.supporter = supporter;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public Integer getInstanceCount() {
		return instanceCount;
	}

	public void setInstanceCount(Integer instanceCount) {
		this.instanceCount = instanceCount;
	}

	public Integer getInitCount() {
		return initCount;
	}

	public void setInitCount(Integer initCount) {
		this.initCount = initCount;
	}

}
