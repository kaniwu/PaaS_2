package com.sgcc.devops.common.constant;

import java.util.concurrent.ConcurrentHashMap;

/**
 * HttpClient常量类
 * @author dmw
 *
 */
public final class HttpClientConstant {
	/* 从Docker Http Api返回的结果的key值 */
	public final static String RESULT_KEY = "json_node";
	/* 基于HTTP的docker的基础协议 */
	public final static String BASE_TCP_PROTOCOL = "http://";
	/* 查询的端口的版本号 */
	public final static String QUERY_VERSION = "/v2/";
	/* 查询全部仓库的命令 */
	public final static String QUERY_SEARCH = "search";
	/* 查询仓库下的镜像ID命令 */
	public final static String QUERY_REPOSITORIES = "repositories/";
	/* 查询镜像的命令 */
	public final static String QUERY_IMAGES = "/images";
	/* 查询镜像的标签命令 */
	public final static String QUERY_TAGS = "/tags";

	/* 查询镜像的标签命令 */
	public final static String QUERY_CATALOG = "/_catalog";
	
	public final static ConcurrentHashMap<String, String> constant_hashmap = new ConcurrentHashMap<String, String>();

	static {
		/* 从Docker Http Api返回的结果的key值 */
		constant_hashmap.put("RESULT_KEY", "json_node");
		/* 基于HTTP的docker的基础协议 */
		constant_hashmap.put("BASE_TCP_PROTOCOL", "http://");
		/* 查询的端口的版本号 */
		constant_hashmap.put("QUERY_VERSION", "/v2");
		/* 查询全部仓库的命令 */
		constant_hashmap.put("QUERY_SEARCH", "search");
		/* 查询仓库下的镜像ID命令 */
		constant_hashmap.put("QUERY_REPOSITORIES", "repositories/");
		/* 查询镜像的命令 */
		constant_hashmap.put("QUERY_IMAGES", "/images");
		/* 查询镜像的标签命令 */
		constant_hashmap.put("QUERY_TAGS", "/tags");
		/* 查询V2 */
		constant_hashmap.put("QUERY_CATALOG", "/_catalog");
	}

	/**
	 * @author youngtsinglin
	 * @description 获取完整查询仓库列表的路径
	 * @date 2015年9月15日
	 */
	public static String GET_QUERY_REGISTRY(String IP, String Port) {
		return constant_hashmap.get("BASE_TCP_PROTOCOL") + IP + ":" + Port + constant_hashmap.get("QUERY_VERSION")
				+ constant_hashmap.get("QUERY_CATALOG");
	}

	/**
	 * @author youngtsinglin
	 * @description 获取某个仓库下所有的镜像ID信息
	 * @date 2015年9月15日
	 */
	public static String GET_REGISTRY_IMAGES(String IP, String Port, String image_name) {
		return constant_hashmap.get("BASE_TCP_PROTOCOL") + IP + ":" + Port + constant_hashmap.get("QUERY_VERSION")
				+ constant_hashmap.get("QUERY_REPOSITORIES") + image_name + constant_hashmap.get("QUERY_IMAGES");
	}

	/**
	 * @author youngtsinglin
	 * @description 获取某个仓库下所有的镜像ID信息
	 * @date 2015年9月15日
	 */
	public static String GET_REGISTRY_IMAGE_TAGS(String IP, String Port, String image_name) {
		return constant_hashmap.get("BASE_TCP_PROTOCOL") + IP + ":" + Port + constant_hashmap.get("QUERY_VERSION")
				+ constant_hashmap.get("QUERY_REPOSITORIES") + image_name + constant_hashmap.get("QUERY_TAGS");
	}

	public static void main(String[] args) {
		System.out.println(GET_REGISTRY_IMAGES("192.168.1.117", "5000", "sgcc"));
	}
}
