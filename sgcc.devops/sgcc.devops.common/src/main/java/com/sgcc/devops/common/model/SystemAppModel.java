/**
 * 
 */
package com.sgcc.devops.common.model;


/**
 * date：2016年3月10日11:30:04 project name：cmbc-devops-common
 * 物理系统应用包模板类
 * @author liyp
 * @version 1.0
 * @since JDK 1.7.0_21 file SystemAppModel.java description：
 */
public class SystemAppModel {
	
	private String id;
	
	private String systemId;
	
	private String appRoute;

	private String appVersion;
	
	private String systemName;
	
	private int status;
	
	private String appName;
	
	private String appType;
	
	private String appCategory;
	
	private String prelibInfo;
	
	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getAppRoute() {
		return appRoute;
	}

	public void setAppRoute(String appRoute) {
		this.appRoute = appRoute;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPrelibInfo() {
		return prelibInfo;
	}

	public void setPrelibInfo(String prelibInfo) {
		this.prelibInfo = prelibInfo;
	}

	public String getAppType() {
		return appType;
	}

	public void setAppType(String appType) {
		this.appType = appType;
	}

	public String getAppCategory() {
		return appCategory;
	}

	public void setAppCategory(String appCategory) {
		this.appCategory = appCategory;
	}

	 
	
	
}
