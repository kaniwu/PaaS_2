package com.sgcc.devops.common.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.constant.SshCommand;

import ch.ethz.ssh2.ChannelCondition;
import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.SFTPv3Client;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

/**
 * SSH对象
 * @author langzi
 *
 */
public class SSH {

	private final static Logger m_logger = Logger.getLogger(SSH.class);

	private final static int DEFAULT_PORT = 22;

	/**
	 * The host name of the agent, in form of IP address
	 */
	private String hostname;

	/**
	 * The user name of the agent
	 */
	private String username;

	/**
	 * The password of the agent, corresponds to the user-name
	 * 主机的密码或者私钥设置口令
	 */
	private String password;

	/**
	 * The port
	 */
	private int port;
	/**
	 * SSH connection between the console and agent
	 */
	private Connection conn;

	private Session session;
	
	private File publicKeyFile;
	
	/**
	 * Constructor
	 * 
	 * @author liangzi
	 * @param hostname
	 * @param username
	 * @param password
	 * @version 1.0 2015年8月17日
	 */
	public SSH(String hostname, String username, String password) {
		this(hostname, username, password, DEFAULT_PORT);
	}
	public SSH(String hostname, String username, String password, int port) {
		this(hostname, username, null, password, port);
	}
	public SSH(String hostname, String username,File publicKeyFile, String password) {
		this(hostname, username, publicKeyFile, password, DEFAULT_PORT);
	}

	/**
	 * Constructor
	 * 
	 * @param hostname
	 * @param username
	 * @param publicKeyFile 远程主机公钥
	 * @param password
	 * @param port
	 * @version 1.0 2015年8月17日
	 */
	public SSH(String hostname, String username,File publicKeyFile, String password, int port) {
		this.hostname = hostname;
		this.username = username;
		this.publicKeyFile = publicKeyFile;
		this.password = password;
		this.port = port;
	}
	
	public SSH() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @author langzi
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean connect() {
		boolean isAuthenticated = false;
		try {
			conn = new Connection(hostname, port);
			conn.connect();
			//add by lianyd rsa证书
			Properties prop = new Properties();
			URL url= Thread.currentThread().getContextClassLoader().getResource("config/system.properties");
			InputStream is = url.openStream();
			prop.load(is);
			String rsaPath = prop.getProperty("local.app.rsa.path");
			File f = new File(rsaPath,"/id_rsa_"+hostname);
			if(f.exists()){
				this.publicKeyFile = f;
			}
			if(publicKeyFile!=null){
				isAuthenticated = conn.authenticateWithPublicKey(username, publicKeyFile, password);
//				m_logger.warn("测试rsa connect is "+isAuthenticated);
			}
			if(!isAuthenticated){
				isAuthenticated = conn.authenticateWithPassword(username, password);
			}
			session = conn.openSession();
		} catch (Exception e) {
			e.printStackTrace();
			m_logger.error(e);
		}
		return isAuthenticated;
	}
	public boolean testConnect() {
		boolean isAuthenticated = false;
		try {
			conn = new Connection(hostname, port);
			conn.connect();
			if(publicKeyFile!=null){
				isAuthenticated = conn.authenticateWithPublicKey(username, publicKeyFile, password);
				m_logger.warn("测试rsa connect is "+isAuthenticated);
			}
			if(!isAuthenticated){
				isAuthenticated = conn.authenticateWithPassword(username, password);
			}
			session = conn.openSession();
		} catch (Exception e) {
			e.printStackTrace();
			m_logger.error(e);
		}
		return isAuthenticated;
	}
	/**
	 * @author langzi
	 * @version 1.0 2015年8月17日
	 */
	public void close() {

		if (session != null) {
			session.close();
		}

		if (conn != null) {
			conn.close();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return hostname + File.pathSeparator + username + File.pathSeparator + password;
	}

	/********************************************************************************************
	 * 
	 * Other Commands
	 * 
	 ********************************************************************************************/

	/**
	 * 直到指令全部执行完，方能返回结果。 如果执行大文件拷贝指令，则需要等待很久
	 * 
	 * @param commandLine
	 * @return
	 * @throws Exception
	 */
	public boolean execute(String commandLine) throws Exception {
		return execute(commandLine, 0);
	}

	/**
	 * 如果timeout，则返回false
	 * 
	 * 
	 * @param commandLine
	 * @param timeout
	 * @return
	 * @throws Exception
	 */
	public boolean execute(String commandLine, long timeout) throws Exception {
		if (ObjectUtils.isNull(session) || ObjectUtils.isNull(commandLine)) {
			return false;
		}

		session.execCommand(commandLine);
		session.waitForCondition(ChannelCondition.EXIT_STATUS, timeout);
		// 当超时发生时，session.getExitStatus()为null
		return (ObjectUtils.isNull(session.getExitStatus())) ? false : ((session.getExitStatus() == 0) ? true : false);
	}

	/**
	 * 直到指令全部执行完，方能返回结果。 如果执行大文件拷贝指令，则需要等待很久
	 * 
	 * @param commandLine
	 * @return
	 * @throws Exception
	 */
	public String executeWithResult(String commandLine) throws Exception {
		return executeWithResult(commandLine, 0);
	}

	/**
	 * 如果timeout，则返回""
	 * 
	 * @param commandLine
	 * @param timeout
	 * @return
	 * @throws Exception
	 */
	public String executeWithResult(String commandLine, long timeout) throws Exception {
		StringBuffer result = new StringBuffer();
		if (ObjectUtils.isNull(session) || ObjectUtils.isNull(commandLine)) {
			return null;
		}
		session.execCommand(commandLine);
		int condition = session.waitForCondition(ChannelCondition.EXIT_STATUS, timeout);
		if (timeout(condition)) {
			m_logger.error("Command [" + commandLine + "] is timeout");
		} else {
			StreamGobbler is = new StreamGobbler(session.getStderr());
			result.append(IOUtils.toString(new StreamGobbler(session.getStdout()))).append(IOUtils.toString(is));
			IOUtils.close(is);
		}
		return result.toString();
	}

	/**
	 * @author langzi
	 * @param condition
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	private boolean timeout(int condition) {
		return ((condition & ChannelCondition.TIMEOUT) == 1) ? true : false;
	}

	/********************************************************************************************
	 * 
	 * Other Commands
	 * 
	 ********************************************************************************************/

	/**
	 * @author langzi
	 * @param localFile
	 * @param remoteDir
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean SCPFile(String localFile, String remoteDir) {
		SCPClient cp = new SCPClient(conn);
		try {
			cp.put(localFile, remoteDir);
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * 远程拷贝目录
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public boolean SCPDirectory(String source, String target) {
		SCPClient cp = new SCPClient(conn);
		try {
			source = FileUploadUtil.check(source);
			File directory = new File(source);
			if (!directory.isDirectory()) {
				m_logger.error("source is not a directpory");
				return false;
			}
			File[] subFiles = directory.listFiles();
			if (null == subFiles) {
				m_logger.error("Source has no files");
				return false;
			}
			for (File file : subFiles) {
				if(file.isDirectory()){
					this.SCPDirectory(file.getAbsolutePath(), target+"/"+file.getName());
				}else{
					cp.put(file.getAbsolutePath(), target);
				}
				
			}
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 文件夹拷贝
	 * 
	 * @param source
	 *            源文件夹路径
	 * @param target
	 *            目标文件夹
	 * @param directory
	 *            子文件夹名称
	 * 
	 * @return
	 */
	public boolean CopyDirectory(String source, String target, String directory) {
		try {
			SFTPv3Client client = new SFTPv3Client(conn);
			client.mkdir(target + directory, 0777);
			client.close();
		} catch (Exception e) {
			m_logger.error(e);
			if (!e.getMessage().contains("exists")) {
				return false;
			}
		}
		return this.SCPDirectory(source, target + directory);
	}

	public static void main(String[] args) {
		SSH ssh = new SSH("10.134.161.111", "root", "docker123!");
		long a = System.currentTimeMillis();
		if (ssh.testConnect()) {
//			System.out.println(ssh.CopyDirectory("d://vm/", "/home/sgcc/", "test28"));
			System.out.println(System.currentTimeMillis()-a);
		}
	
//		String hostname = "10.134.161.151";
//		File f = new File("D:/SGCC-TEMP/rsa/id_rsa"+hostname);
//		System.out.println(f.exists());
//		URL url= Thread.currentThread().getContextClassLoader().getResource("config/system.properties");
//		String path = url.getPath().
//		System.out.println(url.getPath());
	}

	/**
	 * @author langzi
	 * @param toDelFileName
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean rmFile(String toDelFileName) {
		try {
			SFTPv3Client sftpClient = new SFTPv3Client(conn);
			sftpClient.rm(toDelFileName);
			return true;
		} catch (IOException e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * @author langzi
	 * @param remoteFile
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean GetFile(String remoteFile, String localDir) {
		SCPClient sc = new SCPClient(conn);
		try {
			sc.get(remoteFile, localDir);
			return true;
		} catch (IOException e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * @author langzi
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean mountAll() {
		try {
			this.execute(SshCommand.COMMAND_MOUNTALL);
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * @author langzi
	 * @param dir
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean forceMakeDir(String dir) {
		try {
			this.execute(SshCommand.COMMAND_MAKEDIR + dir);
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * @author langzi
	 * @param src
	 * @param target
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean forceMoveFolderTo(String src, String target) {
		try {
			this.execute(SshCommand.COMMAND_MOVE + src + " " + target);
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			return false;
		}
	}

	/**
	 * @author langzi
	 * @param src
	 * @param target
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public boolean forceCopyFolderTo(String src, String target) {
		try {
			execute(SshCommand.COMMAND_COPY + src + " " + target);
			return true;
		} catch (Exception e) {
			m_logger.error(e);
			return false;
		}
	}
	 
}
