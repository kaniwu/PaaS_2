package com.sgcc.devops.common.constant;

/**
 * 
 * date：2015年8月17日 下午3:19:55 project name：sgcc.devops
 * 常用ssh命令
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：SshCommand.java description：
 */
public class SshCommand {

	public final static String COMMAND_MOUNTALL = "/bin/mount -a";

	public final static String COMMAND_MAKEDIR = "mkdir -p ";

	public final static String COMMAND_MOVE = "mv -f ";

	public final static String COMMAND_COPY = "\\cp -r ";
	
	public final static String COMMAND_DF = "df -h ";
}
