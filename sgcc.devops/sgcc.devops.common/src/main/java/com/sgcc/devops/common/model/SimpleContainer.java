package com.sgcc.devops.common.model;

/**
 * date：2015年9月10日 上午9:56:35 project name：cmbc-devops-dao
 * 简单容器模板类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：SimpleContainer.java description：
 */
public class SimpleContainer {

	private String containerId;
	private String containerUuid;
	private String clusterIp;
	private String clusterPort;

	public SimpleContainer() {
	}

	public SimpleContainer(String containerUuid, String clusterIp, String clusterPort) {
		super();
		this.containerUuid = containerUuid;
		this.clusterIp = clusterIp;
		this.clusterPort = clusterPort;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getContainerUuid() {
		return containerUuid;
	}

	public void setContainerUuid(String containerUuid) {
		this.containerUuid = containerUuid;
	}

	public String getClusterIp() {
		return clusterIp;
	}

	public void setClusterIp(String clusterIp) {
		this.clusterIp = clusterIp;
	}

	public String getClusterPort() {
		return clusterPort;
	}

	public void setClusterPort(String clusterPort) {
		this.clusterPort = clusterPort;
	}
}
