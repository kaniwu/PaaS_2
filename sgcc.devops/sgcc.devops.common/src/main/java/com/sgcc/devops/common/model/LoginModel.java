package com.sgcc.devops.common.model;

/**
 * date：2015年8月17日 上午9:45:00 project name：cmbc-devops-common
 * 登录数据模板类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：LoginModel.java description：
 */
public class LoginModel {

	private String userName;
	private char[] pwd;
	private String vercode;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public char[] getPwd() {
		return pwd;
	}

	public void setPwd(char[] pwd) {
		this.pwd = pwd;
	}

	public String getVercode() {
		return vercode;
	}

	public void setVercode(String vercode) {
		this.vercode = vercode;
	}

}
