/**
 * 
 */
package com.sgcc.devops.common.constant;

/**  
 * date：2016年6月21日 上午9:10:41
 * project name：sgcc.devops.common
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：Weights.java
 * description：  
 */
public class Weights {
	public static int CPU_WEIGHT=2;
	public static int MEM_WEIGHT=2;
	public static int NETIN_WEIGHT=1;
	public static int NETOUT_WEIGHT=1;
	public static int DISK_WEIGHT=2;
}
