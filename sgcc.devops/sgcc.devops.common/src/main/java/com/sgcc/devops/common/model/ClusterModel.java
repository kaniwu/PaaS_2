package com.sgcc.devops.common.model;

/**
 * 集群模板类
 * @author dmw
 *
 */
public class ClusterModel {

	private String clusterId;
	private String clusterUuid;
	private String clusterName;
	private Byte clusterType;
	private Byte clusterStatus;
	private String clusterPort;
	private String dockerParam;
	public String getSwarmIns() {
		return swarmIns;
	}

	public void setSwarmIns(String swarmIns) {
		this.swarmIns = swarmIns;
	}

	private String managePath;
	private String clusterDesc;
	private String clusterEnv;
	
	public String getClusterEnv() {
		return clusterEnv;
	}

	public void setClusterEnv(String clusterEnv) {
		this.clusterEnv = clusterEnv;
	}

	private String masteHostId;
	private String hostDocker;
	private String registryLbId;
	private String swarmIns;
	public String getRegistryLbId() {
		return registryLbId;
	}
	
	public void setRegistryLbId(String registryLbId) {
		this.registryLbId = registryLbId;
	}
	public String getHostDocker() {
		return hostDocker;
	}

	public void setHostDocker(String hostDocker) {
		this.hostDocker = hostDocker;
	}

	private String standByHostId;
	private int page;
	private int limit;
	private String search;

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getClusterUuid() {
		return clusterUuid;
	}

	public void setClusterUuid(String clusterUuid) {
		this.clusterUuid = clusterUuid;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public Byte getClusterType() {
		return clusterType;
	}

	public void setClusterType(Byte clusterType) {
		this.clusterType = clusterType;
	}

	public Byte getClusterStatus() {
		return clusterStatus;
	}

	public void setClusterStatus(Byte clusterStatus) {
		this.clusterStatus = clusterStatus;
	}

	public String getClusterPort() {
		return clusterPort;
	}

	public void setClusterPort(String clusterPort) {
		this.clusterPort = clusterPort;
	}

	public String getManagePath() {
		return managePath;
	}

	public void setManagePath(String managePath) {
		this.managePath = managePath;
	}

	public String getClusterDesc() {
		return clusterDesc;
	}

	public void setClusterDesc(String clusterDesc) {
		this.clusterDesc = clusterDesc;
	}

	public String getMasteHostId() {
		return masteHostId;
	}

	public void setMasteHostId(String masteHostId) {
		this.masteHostId = masteHostId;
	}

	
	public String getStandByHostId() {
		return standByHostId;
	}

	public void setStandByHostId(String standByHostId) {
		this.standByHostId = standByHostId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getDockerParam() {
		return dockerParam;
	}

	public void setDockerParam(String dockerParam) {
		this.dockerParam = dockerParam;
	}

}