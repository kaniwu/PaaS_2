package com.sgcc.devops.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 部署配置类
 * @author dmw
 *
 */
@Component("tmpHostConfig")
public class TmpHostConfig{
	
	@Value("${transfer.needed}")
	private boolean transferNeeded;
	
	@Value("${tmp.host.ip}")
	private String tmpHostIp;//临时主机，用于开发环境下paas平台运行在windows时把应用包、基础镜像等需要在docker环境下完成的工作放到临时主机上做
	
	@Value("${tmp.host.user}")
	private String tmpHostUser;
	
	@Value("${tmp.host.pwd}")
	private String tmpHostPwd;
	
	@Value("${tmp.host.port}")
	private String tmpHostPort;
	
	@Value("${tmp.baseImage.path}")
	private String baseImagePath;//基础镜像包上传时临时存放目录
	
	@Value("${tmp.library.path}")
	private String libraryPath;//数据库预加载包上传
	
	@Value("${tmp.app.path}")
	private String appPath;//应用包管理上传包
	
	@Value("${tmp.deploy.path}")
	private String deployPath;//应用部署时产生的存放配置文件的路径,jdbc.xml Dockerfile conf.xml
	
//	@Value("${tmp.db.driver.path}")
//	private String dbDriverPath;//数据库驱动
	
	@Value("${tmp.host.install.path}")
	private String hostInstallPath;//主机组件安装包上传

	public String getTmpHostIp() {
		return tmpHostIp;
	}

	public void setTmpHostIp(String tmpHostIp) {
		this.tmpHostIp = tmpHostIp;
	}

	public String getTmpHostUser() {
		return tmpHostUser;
	}

	public void setTmpHostUser(String tmpHostUser) {
		this.tmpHostUser = tmpHostUser;
	}

	public String getTmpHostPwd() {
		return tmpHostPwd;
	}

	public void setTmpHostPwd(String TmpHostPwd) {
		this.tmpHostPort = TmpHostPwd;
	}

	public String getTmpHostPort() {
		return tmpHostPort;
	}

	public void setTmpHostPort(String tmpHostPwd) {
		this.tmpHostPwd = tmpHostPwd;
	}
	
	public String getBaseImagePath() {
		return baseImagePath;
	}

	public void setBaseImagePath(String baseImagePath) {
		this.baseImagePath = baseImagePath;
	}

	public String getLibraryPath() {
		return libraryPath;
	}

	public void setLibraryPath(String libraryPath) {
		this.libraryPath = libraryPath;
	}

	public String getAppPath() {
		return appPath;
	}

	public void setAppPath(String appPath) {
		this.appPath = appPath;
	}

	public String getDeployPath() {
		return deployPath;
	}

	public void setDeployPath(String deployPath) {
		this.deployPath = deployPath;
	}

//	public String getDbDriverPath() {
//		return dbDriverPath;
//	}
//
//	public void setDbDriverPath(String dbDriverPath) {
//		this.dbDriverPath = dbDriverPath;
//	}

	public String getHostInstallPath() {
		return hostInstallPath;
	}

	public void setHostInstallPath(String hostInstallPath) {
		this.hostInstallPath = hostInstallPath;
	}

	public boolean isTransferNeeded() {
		return transferNeeded;
	}

	public void setTransferNeeded(boolean transferNeeded) {
		this.transferNeeded = transferNeeded;
	}
	
}
