package com.sgcc.devops.common.model;

import java.util.ArrayList;

/**
 * date：2015年8月17日 上午10:18:29 project name：sgcc-devops-common
 * 注册模板类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegisterModel.java description：
 */
public class RegisterModel {

	private Object object;
	private String methodName;
	private ArrayList<Class<?>> paramsList;

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public ArrayList<Class<?>> getParamsList() {
		return paramsList;
	}

	public void setParamsList(ArrayList<Class<?>> paramsList) {
		this.paramsList = paramsList;
	}
}
