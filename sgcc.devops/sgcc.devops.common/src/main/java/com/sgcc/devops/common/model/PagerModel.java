/**
 * 
 */
package com.sgcc.devops.common.model;

/**  
 * date：2016年2月24日 上午11:11:04
 * project name：sgcc.devops.common
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：PagerModel.java
 * description：  
 */
public class PagerModel {
	private int page;//页码
	private int rows;//每页显示行数
	private String sidx;//排序对象，对应的表字段名称
	private String sord;//排序方式，asc/desc
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getRows() {
		return rows;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	public String getSidx() {
		return sidx;
	}
	public void setSidx(String sidx) {
		this.sidx = sidx;
	}
	public String getSord() {
		return sord;
	}
	public void setSord(String sord) {
		this.sord = sord;
	}
}
