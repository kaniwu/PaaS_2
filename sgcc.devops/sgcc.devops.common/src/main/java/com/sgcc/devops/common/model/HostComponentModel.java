package com.sgcc.devops.common.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;


public class HostComponentModel {

	//字典表（dop_parameter）主键
    private String id;
//组件类型
    private String type;
//组件版本
    private String version;
//文件名称
    private String filename;
//查看状态脚本
    private String statusCommand;
//安装脚本    
    private String installCommand;
//查看已安装的版本    
    private String versionCommand;
//  升级脚本  
    private String upgradeCommand;
// 安装或者升级时是否转移容器：1是，0否   
    private Byte transfer;
// 创建人   
    private String creator;
// 创建时间
    @JsonSerialize(using = DateSerializer.class)
	private Date createTime;
//本地地址
    private String localPath;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getStatusCommand() {
		return statusCommand;
	}
	public void setStatusCommand(String statusCommand) {
		this.statusCommand = statusCommand;
	}
	public String getInstallCommand() {
		return installCommand;
	}
	public void setInstallCommand(String installCommand) {
		this.installCommand = installCommand;
	}
	public String getVersionCommand() {
		return versionCommand;
	}
	public void setVersionCommand(String versionCommand) {
		this.versionCommand = versionCommand;
	}
	public String getUpgradeCommand() {
		return upgradeCommand;
	}
	public void setUpgradeCommand(String upgradeCommand) {
		this.upgradeCommand = upgradeCommand;
	}
	public Byte getTransfer() {
		return transfer;
	}
	public void setTransfer(Byte transfer) {
		this.transfer = transfer;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getLocalPath() {
		return localPath;
	}
	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}


}
