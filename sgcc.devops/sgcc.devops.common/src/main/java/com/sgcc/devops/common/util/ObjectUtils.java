
package com.sgcc.devops.common.util;

/**
 * 对象工具类
 * @author dmw
 *
 */
public final class ObjectUtils {

	public static boolean isNull(Object obj) {
		return obj == null;
	}

	public static String[] toStringArray(Object[] objects) {
		String[] strs = new String[objects.length];
		for (int i = 0; i < objects.length; i++) {
			strs[i] = objects[i].toString();
		}
		return strs;
	}
}
