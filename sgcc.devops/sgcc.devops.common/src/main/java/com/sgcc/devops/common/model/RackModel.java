package com.sgcc.devops.common.model;

/**
 * 机架模板类
 * @author panjing
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegisterModel.java description：
 */
public class RackModel {

	private String rackId;
	private String rackName;
	private String rackRemark;
	private String creator;
	private String serverRoomId;
	
	public String getRackId() {
		return rackId;
	}
	public void setRackId(String rackId) {
		this.rackId = rackId;
	}
	public String getRackName() {
		return rackName;
	}
	public void setRackName(String rackName) {
		this.rackName = rackName;
	}
	public String getRackRemark() {
		return rackRemark;
	}
	public void setRackRemark(String rackRemark) {
		this.rackRemark = rackRemark;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getServerRoomId() {
		return serverRoomId;
	}
	public void setServerRoomId(String serverRoomId) {
		this.serverRoomId = serverRoomId;
	}


}
