package com.sgcc.devops.common.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 环境管理模板类
 * @author panjing
 * @version 1.0
 */
public class EnvironmentModel {

	private String eId;
	private String eName;
	private String eDesc;
	private String eCreator;
	private String eCreatorName;
	@JsonSerialize(using = DateSerializer.class)
	private Date eTime;
	public String geteId() {
		return eId;
	}
	public void seteId(String eId) {
		this.eId = eId;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	public String geteDesc() {
		return eDesc;
	}
	public void seteDesc(String eDesc) {
		this.eDesc = eDesc;
	}
	public String geteCreator() {
		return eCreator;
	}
	public void seteCreator(String eCreator) {
		this.eCreator = eCreator;
	}
	public Date geteTime() {
		return eTime;
	}
	public void seteTime(Date eTime) {
		this.eTime = eTime;
	}
	public String geteCreatorName() {
		return eCreatorName;
	}
	public void seteCreatorName(String eCreatorName) {
		this.eCreatorName = eCreatorName;
	}
	
	

}
