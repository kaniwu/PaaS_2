package com.sgcc.devops.common.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 主机模板类
 * @author dmw
 *
 */
public class HostModel {

	private String hostId;
	private String hostUuid;
	private String hostName;
	private String hostUser;
	private String hostPwd;
	private Byte hostType;
	private String hostIp;
	private String hostPort;
	private Integer hostCpu;
	private Integer hostMem;
	private Byte hostStatus;
	private String hostDesc;
	private String hostEnv;
	public String getHostEnv() {
		return hostEnv;
	}

	public void setHostEnv(String hostEnv) {
		this.hostEnv = hostEnv;
	}

	private String hostKernelVersion;
	private String clusterId;
	private int page;
	private int limit;
	private String search;
	private Byte dockerStatus;
	private String clusterName;
	private String locationId;
	private String beginIp;
	private String endIp;
	@JsonSerialize(using = DateSerializer.class)
	private Date hostCreatetime;

	public Date getHostCreatetime() {
		return hostCreatetime;
	}

	public void setHostCreatetime(Date hostCreatetime) {
		this.hostCreatetime = hostCreatetime;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostUuid() {
		return hostUuid;
	}

	public void setHostUuid(String hostUuid) {
		this.hostUuid = hostUuid;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getHostUser() {
		return hostUser;
	}

	public void setHostUser(String hostUser) {
		this.hostUser = hostUser;
	}

	public String getHostPwd() {
		return hostPwd;
	}

	public void setHostPwd(String hostPwd) {
		this.hostPwd = hostPwd;
	}

	public Byte getHostType() {
		return hostType;
	}

	public void setHostType(Byte hostType) {
		this.hostType = hostType;
	}

	public String getHostIp() {
		return hostIp;
	}

	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}
	
	public String getHostPort() {
		return hostPort;
	}

	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}

	public Integer getHostCpu() {
		return hostCpu;
	}

	public void setHostCpu(Integer hostCpu) {
		this.hostCpu = hostCpu;
	}

	public Integer getHostMem() {
		return hostMem;
	}

	public void setHostMem(Integer hostMem) {
		this.hostMem = hostMem;
	}

	public Byte getHostStatus() {
		return hostStatus;
	}

	public void setHostStatus(Byte hostStatus) {
		this.hostStatus = hostStatus;
	}

	public String getHostDesc() {
		return hostDesc;
	}

	public void setHostDesc(String hostDesc) {
		this.hostDesc = hostDesc;
	}

	public String getHostKernelVersion() {
		return hostKernelVersion;
	}

	public void setHostKernelVersion(String hostKernelVersion) {
		this.hostKernelVersion = hostKernelVersion;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	public Byte getDockerStatus() {
		return dockerStatus;
	}

	public void setDockerStatus(Byte dockerStatus) {
		this.dockerStatus = dockerStatus;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getBeginIp() {
		return beginIp;
	}

	public void setBeginIp(String beginIp) {
		this.beginIp = beginIp;
	}

	public String getEndIp() {
		return endIp;
	}

	public void setEndIp(String endIp) {
		this.endIp = endIp;
	}

}