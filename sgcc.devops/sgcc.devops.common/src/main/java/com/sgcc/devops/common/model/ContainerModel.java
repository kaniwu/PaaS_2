package com.sgcc.devops.common.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * date：2015年8月20日 下午4:56:10 project name：sgcc-devops-common
 * 容器模板类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：ContainerModel.java description：
 */
public class ContainerModel {

	private int page;
	private int limit;
	private String search;
	private String imageId;
	private String imageName;
	private String clusterId;
	private String createModel;//0,create,1,run
	private String conNumber;
	private String createParams;
	private String systemName;
	private String systemCode;
	private Integer conType;
	private String conId;

	private String conUuid;

	private String conImgid;

	private String conCreator;

	private String conName;

	private Byte conPower;

	private Byte conStatus;

	private String conDesc;
	private String conEnv;
	
	private String conCommand;
	private String conPorts;

	public String getConEnv() {
		return conEnv;
	}

	public void setConEnv(String conEnv) {
		this.conEnv = conEnv;
	}

	private String sysId;

	private String conTempName;// 别名
	private String hostId;
	private String hostName;
	private Integer cpu;
	private Integer memery;
	private String logPath;
	@JsonSerialize(using = DateSerializer.class)
	private Date conCreatetime;
	private String parameter;
	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public String getCreateModel() {
		return createModel;
	}

	public void setCreateModel(String createModel) {
		this.createModel = createModel;
	}

	public String getConNumber() {
		return conNumber;
	}

	public void setConNumber(String conNumber) {
		this.conNumber = conNumber;
	}

	public String getConName() {
		return conName;
	}

	public void setConName(String conName) {
		this.conName = conName;
	}

	public String getCreateParams() {
		return createParams;
	}

	public void setCreateParams(String createParams) {
		this.createParams = createParams;
	}

	@Override
	public String toString() {
		return "ContainerModel [page=" + page + ", limit=" + limit + ", search=" + search + ", imageName=" + imageName
				+ ", clusterId=" + clusterId + ", conNumber=" + conNumber + ", conName=" + conName + ", createParams="
				+ createParams + "]";
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getConId() {
		return conId;
	}

	public void setConId(String conId) {
		this.conId = conId;
	}

	public String getConUuid() {
		return conUuid;
	}

	public void setConUuid(String conUuid) {
		this.conUuid = conUuid;
	}

	public String getConImgid() {
		return conImgid;
	}

	public void setConImgid(String conImgid) {
		this.conImgid = conImgid;
	}

	public String getConCreator() {
		return conCreator;
	}

	public void setConCreator(String conCreator) {
		this.conCreator = conCreator;
	}

	public Byte getConPower() {
		return conPower;
	}

	public void setConPower(Byte conPower) {
		this.conPower = conPower;
	}

	public Byte getConStatus() {
		return conStatus;
	}

	public void setConStatus(Byte conStatus) {
		this.conStatus = conStatus;
	}

	public String getConDesc() {
		return conDesc;
	}

	public void setConDesc(String conDesc) {
		this.conDesc = conDesc;
	}

	public String getSysId() {
		return sysId;
	}

	public void setSysId(String sysId) {
		this.sysId = sysId;
	}

	public String getConTempName() {
		return conTempName;
	}

	public void setConTempName(String conTempName) {
		this.conTempName = conTempName;
	}

	public Date getConCreatetime() {
		return conCreatetime;
	}

	public void setConCreatetime(Date conCreatetime) {
		this.conCreatetime = conCreatetime;
	}

	public Integer getConType() {
		return conType;
	}

	public void setConType(Integer conType) {
		this.conType = conType;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Integer getCpu() {
		return cpu;
	}

	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}

	public Integer getMemery() {
		return memery;
	}

	public void setMemery(Integer memery) {
		this.memery = memery;
	}

	public String getLogPath() {
		return logPath;
	}

	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getSystemCode() {
		return systemCode;
	}

	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}

	public String getConCommand() {
		return conCommand;
	}

	public void setConCommand(String conCommand) {
		this.conCommand = conCommand;
	}

	public String getConPorts() {
		return conPorts;
	}

	public void setConPorts(String conPorts) {
		this.conPorts = conPorts;
	}

	
}
