package com.sgcc.devops.common.model;

/**
 * 组件模板类
 * @author dmw
 *
 */
public class ComponentModel {
	/** 组件ID */
	private String componentId;
	/** 组件名称 */
	private String componentName;
	/** 组件类型 */
	private Byte componentType;
	/** 组件IP */
	private String componentIp;
	/** 状态 */
	private Byte componentStatus;
	/** 说明 */
	private String componentDescription;
	/** 组件版本 */
	private String componentVersion;
	/** 组件端口 */
	private Integer componentPort;
	
	private Integer componentHostPort;
	
	private String componentUserName;

	private String componentPwd;
	/** 驱动 */
	private String componentDriver;
	/** JdbcUrl */
	private String componentJdbcurl;
	/** 配置文件目录 */
	private String componentConfigDir;
	/** 数据库类型 */
	private Byte componentDBType;
	
	/** 是否自动配置F5 */
	private Byte autoConfigF5;
	/**BIGIP版本*/
	private String bigipVersion;
	
	private ComponentExpandModel componentExpand;
	
	/** 组件 主机列表信息拼接字符串 */
	private String componentHostInfo;
	
	private String nginxIns;
	
	private String dnsZone;
	
	private String key;
	
	private String secret;
	
	private String backComponentIp;
	
	private String keepaliveIp;
	
	private Byte nginxCategory;
	
	public String getNginxIns() {
		return nginxIns;
	}

	public void setNginxIns(String nginxIns) {
		this.nginxIns = nginxIns;
	}

	public Byte getAutoConfigF5() {
		return autoConfigF5;
	}

	public void setAutoConfigF5(Byte autoConfigF5) {
		this.autoConfigF5 = autoConfigF5;
	}

	public String getBigipVersion() {
		return bigipVersion;
	}

	public void setBigipVersion(String bigipVersion) {
		this.bigipVersion = bigipVersion;
	}

	public String getComponentDriver() {
		return componentDriver;
	}

	public void setComponentDriver(String componentDriver) {
		this.componentDriver = componentDriver;
	}

	public String getComponentJdbcurl() {
		return componentJdbcurl;
	}

	public void setComponentJdbcurl(String componentJdbcurl) {
		this.componentJdbcurl = componentJdbcurl;
	}

	public String getComponentConfigDir() {
		return componentConfigDir;
	}

	public void setComponentConfigDir(String componentConfigDir) {
		this.componentConfigDir = componentConfigDir;
	}

	private int page;
	private int limit;
	private String search;

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public Byte getComponentType() {
		return componentType;
	}

	public void setComponentType(Byte componentType) {
		this.componentType = componentType;
	}

	public String getComponentIp() {
		return componentIp;
	}

	public void setComponentIp(String componentIp) {
		this.componentIp = componentIp;
	}

	public Byte getComponentStatus() {
		return componentStatus;
	}

	public void setComponentStatus(Byte componentStatus) {
		this.componentStatus = componentStatus;
	}

	public String getComponentDescription() {
		return componentDescription;
	}

	public void setComponentDescription(String componentDescription) {
		this.componentDescription = componentDescription;
	}

	public String getComponentVersion() {
		return componentVersion;
	}

	public void setComponentVersion(String componentVersion) {
		this.componentVersion = componentVersion;
	}

	public Integer getComponentPort() {
		return componentPort;
	}

	public void setComponentPort(Integer componentPort) {
		this.componentPort = componentPort;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getComponentUserName() {
		return componentUserName;
	}

	public void setComponentUserName(String componentUserName) {
		this.componentUserName = componentUserName;
	}

	public String getComponentPwd() {
		return componentPwd;
	}

	public void setComponentPwd(String componentPwd) {
		this.componentPwd = componentPwd;
	}

	public Byte getComponentDBType() {
		return componentDBType;
	}

	public void setComponentDBType(Byte componentDBType) {
		this.componentDBType = componentDBType;
	}

	public ComponentExpandModel getComponentExpand() {
		return componentExpand;
	}

	public void setComponentExpand(ComponentExpandModel componentExpand) {
		this.componentExpand = componentExpand;
	}

	public String getComponentHostInfo() {
		return componentHostInfo;
	}

	public void setComponentHostInfo(String componentHostInfo) {
		this.componentHostInfo = componentHostInfo;
	}

	public String getDnsZone() {
		return dnsZone;
	}

	public void setDnsZone(String dnsZone) {
		this.dnsZone = dnsZone;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getBackComponentIp() {
		return backComponentIp;
	}

	public void setBackComponentIp(String backComponentIp) {
		this.backComponentIp = backComponentIp;
	}

	public String getKeepaliveIp() {
		return keepaliveIp;
	}

	public void setKeepaliveIp(String keepaliveIp) {
		this.keepaliveIp = keepaliveIp;
	}

	public Byte getNginxCategory() {
		return nginxCategory;
	}

	public void setNginxCategory(Byte nginxCategory) {
		this.nginxCategory = nginxCategory;
	}

	public Integer getComponentHostPort() {
		return componentHostPort;
	}

	public void setComponentHostPort(Integer componentHostPort) {
		this.componentHostPort = componentHostPort;
	}

	

}