package com.sgcc.devops.common.util;

import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.log4j.Logger;

/**
 * date：2015年8月14日 下午3:24:34 project name：sgcc-devops-common
 * JSON工具类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：JSONUtil.java description：
 */
public class JSONUtil {
	private static Logger logger = Logger.getLogger(JSONUtil.class);
	/**
	 * 实体类型的对象
	 */
	public static final int OBJECT_TYPE_BEAN = 1;

	/**
	 * 集合类型对象
	 */
	public static final int OBJECT_TYPE_LIST = 2;

	/**
	 * 内容摘要：将任何一个实体类转变为JSONObject对象
	 * 
	 * @author langzi
	 * @param obj
	 * @return
	 * @version 1.0 2015年8月14日
	 */
	public static JSONObject parseObjectToJsonObject(Object obj) {
		try {
			return JSONObject.fromObject(obj);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 内容摘要：将任何一个实体类转变为JSONArray对象
	 * 
	 * @author langzi
	 * @param obj
	 * @return
	 * @version 1.0 2015年8月14日
	 */
	public static JSONArray parseObjectToJsonArray(Object obj) {
		try {
			return JSONArray.fromObject(obj);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * 内容摘要：将任何一个实体类转变为JSONArray对象
	 * 
	 * @author langzi
	 * @param obj
	 * @return
	 * @version 1.0 2015年8月14日
	 */
	public static JSONArray parseObjectToJsonArray(Object obj,JsonConfig jsonConfig) {
		try {
			return JSONArray.fromObject(obj,jsonConfig);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 内容摘要：将一个JSON对象转换成指定类型的Obj
	 * 
	 * @author langzi
	 * @param json
	 * @param clazz
	 * @return
	 * @version 1.0 2015年8月14日
	 */
	public static <T> Object parseJsonObjectToObject(Object json, Class<T> clazz) {
		Object obj = null;
		try {
			JSONObject jsonObject = JSONObject.fromObject(json);
			obj = JSONObject.toBean(jsonObject, clazz);
		} catch (Exception e) {
			logger.error(e);
		}
		return obj;
	}

	/**
	 * 方法名称：parseJsonArrayToBean 内容摘要：将一个JSON对象转换成指定类型的Bean集合
	 * 
	 * @param <T>
	 * @param json
	 *            任意实体，包括Json格式字符串
	 * @param clazz
	 *            需要转换的bean的Class
	 * @return
	 */
	public static <T> List<Object> parseJsonArrayToObject(Object json, Class<T> clazz) {
		List<Object> list = new LinkedList<Object>();
		try {
			JSONArray jsonArray = JSONArray.fromObject(json);
			Object[] objs = jsonArray.toArray();
			Object obj = null;
			for (int i = 0; i < objs.length; i++) {
				obj = parseJsonObjectToObject(objs[i], clazz);
				list.add(obj);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return list;
	}

	/**
	 * 方法名称：getJsonString 内容摘要：将对象转换为JSON字符串
	 * 
	 * @param object
	 * @param objType
	 * @return
	 */
	public static String getJsonString(Object object, int objType) {
		JSON json = null;
		try {
			json = null;
			if (objType == OBJECT_TYPE_BEAN) {
				json = JSONObject.fromObject(object);
			} else if (objType == OBJECT_TYPE_LIST) {
				json = JSONArray.fromObject(object);
			} else {
				return "待写入实体的对象类型不正确";
			}
		} catch (Exception e) {
			return "转换JSON字符串出错";
		}
		return json.toString();
	}
}
