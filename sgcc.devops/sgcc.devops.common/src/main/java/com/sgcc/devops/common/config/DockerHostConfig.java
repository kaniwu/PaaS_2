package com.sgcc.devops.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 部署配置类
 * @author dmw
 *
 */
@Component("dockerHostConfig")
public class DockerHostConfig {
	@Value("${host.install.path}")
	private String hostInstallPath;//主机安装组件包上传到主机目录
	
	@Value("${host.app.log.path}")
	private String hostAppLogPath;//docker应用日志-v到本地的目录

	@Value("${host.load.image.path}")
	private String loadImagePath;//导出镜像时，镜像存储目录，此目录不需要手动创建

	public String getHostInstallPath() {
		return hostInstallPath;
	}

	public void setHostInstallPath(String hostInstallPath) {
		this.hostInstallPath = hostInstallPath;
	}

	public String getHostAppLogPath() {
		return hostAppLogPath;
	}

	public void setHostAppLogPath(String hostAppLogPath) {
		this.hostAppLogPath = hostAppLogPath;
	}

	public String getLoadImagePath() {
		return loadImagePath;
	}

	public void setLoadImagePath(String loadImagePath) {
		this.loadImagePath = loadImagePath;
	}
}
