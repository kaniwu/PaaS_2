package com.sgcc.devops.common.model;

import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 库文件模板类
 * @author liyp
 *
 */
public class LibraryModel {

	private String libraryId;
	private String libraryName;
	private String libraryDescription;
	private String libraryFilePath;
	private String libraryCreator;
	private String libraryCreatorName;
	private int status;
	
	@JsonSerialize(using = DateSerializer.class)
	private Date libraryCreatetime;

		
	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}

	public String getLibraryName() {
		return libraryName;
	}

	public void setLibraryName(String libraryName) {
		this.libraryName = libraryName;
	}

	public String getLibraryDescription() {
		return libraryDescription;
	}

	public void setLibraryDescription(String libraryDescription) {
		this.libraryDescription = libraryDescription;
	}

	public String getLibraryFilePath() {
		return libraryFilePath;
	}

	public void setLibraryFilePath(String libraryFilePath) {
		this.libraryFilePath = libraryFilePath;
	}

	public String getLibraryCreator() {
		return libraryCreator;
	}

	public void setLibraryCreator(String libraryCreator) {
		this.libraryCreator = libraryCreator;
	}

	public Date getLibraryCreatetime() {
		return libraryCreatetime;
	}

	public void setLibraryCreatetime(Date libraryCreatetime) {
		this.libraryCreatetime = libraryCreatetime;
	}

	public String getLibraryCreatorName() {
		return libraryCreatorName;
	}

	public void setLibraryCreatorName(String libraryCreatorName) {
		this.libraryCreatorName = libraryCreatorName;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
}