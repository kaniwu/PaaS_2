package com.sgcc.devops.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 部署配置类
 * @author dmw
 *
 */
@Component("jdbcConfig")
public class JdbcConfig {
	@Value("${db.dialect}")
	private String dialect;// 数据库方言：mysql/oracle

	public String getDialect() {
		return dialect;
	}

	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

}
