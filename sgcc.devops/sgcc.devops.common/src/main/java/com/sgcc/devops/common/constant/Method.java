package com.sgcc.devops.common.constant;

/**
 * Http请求方法枚举
 * @author dmw
 *
 */
public enum Method {
	POST, GET;
}
