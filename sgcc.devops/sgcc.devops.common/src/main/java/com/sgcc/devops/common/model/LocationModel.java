package com.sgcc.devops.common.model;

public class LocationModel {
    private String id;
    //位置名称
    private String locationName;
    //位置编码
    private String locationCode;
    //位置层级，是否是叶子节点
    private Byte locationLevel;
    //父节点ID
    private String locationParent;
    //父节点Name
    private String locationParentName;
    //对应参数表中层级code值
    private String locationParamCode;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public String getLocationCode() {
		return locationCode;
	}
	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}
	public Byte getLocationLevel() {
		return locationLevel;
	}
	public void setLocationLevel(Byte locationLevel) {
		this.locationLevel = locationLevel;
	}
	public String getLocationParent() {
		return locationParent;
	}
	public void setLocationParent(String locationParent) {
		this.locationParent = locationParent;
	}
	public String getLocationParamCode() {
		return locationParamCode;
	}
	public void setLocationParamCode(String locationParamCode) {
		this.locationParamCode = locationParamCode;
	}
	public String getLocationParentName() {
		return locationParentName;
	}
	public void setLocationParentName(String locationParentName) {
		this.locationParentName = locationParentName;
	}
}