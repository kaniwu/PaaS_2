package com.sgcc.devops.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 部署配置类
 * @author dmw
 *
 */
@Component("localConfig")
public class LocalConfig {
	@Value("${local.template.path}")
	private String templatePath;// 配置模板存放位置，这个在平台主机上创建的目录，可以『tomcat.dir』/weapps/ROOT/WEB-INF/classes/vm

	@Value("${local.security.path}")
	private String securityPath;// weblogic密钥文件存放位置『tomcat.dir』/weapps/ROOT/WEB-INF

	@Value("${local.baseImage.path}")
	private String localBaseImagePath;//基础镜像包上传时临时存放目录
	
	@Value("${local.host.install.path}")
	private String localHostInstallPath;//主机组件安装包上传
	
	@Value("${local.library.path}")
	private String localLibraryPath;//预加载包在平台服务器上的存储路径，这个路径要在paas平台所在的主机上创建
	
	@Value("${local.app.path}")
	private String localAppPath;//应用程序包在平台服务器上的存储路径，这个路径要在paas平台所在的主机上创建
	
	@Value("${local.deploy.path}")
	private String localDeployPath;//应用部署时产生的存放配置文件的路径,jdbc.xml Dockerfile conf.xml

	@Value("${local.db.driver.path}")
	private String localDBDriverPath;// 数据库驱动存放路径，

	@Value("${local.nginx.conf.path}")
	private String localNginxConfPath;// nginx.conf存储路径,这个随便配置，一般不用，后续会删除

	@Value("${local.app.log.path}")
	private String localLogPath;//容器日志在平台服务器上的临时路径，这个路径要在paas平台所在的主机上新建
	
	@Value("${local.app.image.path}")
	private String localImagePath;//docker save保存的镜像tar包在平台服务器上的临时路径，这个路径要在paas平台所在的主机上新建
	
	@Value("${local.app.timelog.path}")
	private String localTimeLogPath;//paas主机挂载远程docker主机的日志目录
	
	@Value("${host.app.log.path}")
	private String hostLogPath;//paas主机挂载远程docker主机的日志目录
	
	public String getHostLogPath(){
		return this.hostLogPath;
	}
	
	public void setHostTimeLogPath(String path){
		this.hostLogPath=path;
	}
	
	public String getLocalTimeLogPath(){
		return this.localTimeLogPath;
	}
	
	public void setLocalTimeLogPath(String path){
		this.localTimeLogPath=path;
	}
	public String getTemplatePath() {
		return templatePath;
	}

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}

	public String getSecurityPath() {
		return securityPath;
	}

	public void setSecurityPath(String securityPath) {
		this.securityPath = securityPath;
	}

	public String getLocalBaseImagePath() {
		return localBaseImagePath;
	}

	public void setLocalBaseImagePath(String localBaseImagePath) {
		this.localBaseImagePath = localBaseImagePath;
	}

	public String getLocalHostInstallPath() {
		return localHostInstallPath;
	}

	public void setLocalHostInstallPath(String localHostInstallPath) {
		this.localHostInstallPath = localHostInstallPath;
	}

	public String getLocalLibraryPath() {
		return localLibraryPath;
	}

	public void setLocalLibraryPath(String localLibraryPath) {
		this.localLibraryPath = localLibraryPath;
	}

	public String getLocalAppPath() {
		return localAppPath;
	}

	public void setLocalAppPath(String localAppPath) {
		this.localAppPath = localAppPath;
	}

	public String getLocalDeployPath() {
		return localDeployPath;
	}

	public void setLocalDeployPath(String localDeployPath) {
		this.localDeployPath = localDeployPath;
	}

	public String getLocalDBDriverPath() {
		return localDBDriverPath;
	}

	public void setLocalDBDriverPath(String localDBDriverPath) {
		this.localDBDriverPath = localDBDriverPath;
	}

	public String getLocalNginxConfPath() {
		return localNginxConfPath;
	}

	public void setLocalNginxConfPath(String localNginxConfPath) {
		this.localNginxConfPath = localNginxConfPath;
	}

	public String getLocalLogPath() {
		return localLogPath;
	}

	public void setLocalLogPath(String localLogPath) {
		this.localLogPath = localLogPath;
	}

	public String getLocalImagePath() {
		return localImagePath;
	}

	public void setLocalImagePath(String localImagePath) {
		this.localImagePath = localImagePath;
	}

	public void setHostLogPath(String hostLogPath) {
		this.hostLogPath = hostLogPath;
	}
	
}
