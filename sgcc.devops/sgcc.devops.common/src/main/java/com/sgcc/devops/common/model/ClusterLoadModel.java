package com.sgcc.devops.common.model;

/**
 * 集群负载模板类
 * @author dmw
 *
 */
public class ClusterLoadModel implements Comparable<ClusterLoadModel> {

	private String clusterId;
	private String name;
	/** 负载 */
	private Double value;
	
	/**cpu*/
	private Double cpus;
	
	/**内存*/
	private Double mems;
	
	/**磁盘*/
	private Double diskSpaces;
	
	
	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public Double getCpus() {
		return cpus;
	}

	public void setCpus(Double cpus) {
		this.cpus = cpus;
	}

	public Double getMems() {
		return mems;
	}

	public void setMems(Double mems) {
		this.mems = mems;
	}

	public Double getDiskSpaces() {
		return diskSpaces;
	}

	public void setDiskSpaces(Double diskSpaces) {
		this.diskSpaces = diskSpaces;
	}

	@Override
	public int compareTo(ClusterLoadModel o) {
		if (value > o.value) {
			return -1;
		} else if (value < o.value) {
			return 1;
		}
		return 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
