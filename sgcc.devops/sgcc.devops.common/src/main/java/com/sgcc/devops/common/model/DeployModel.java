package com.sgcc.devops.common.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.sgcc.devops.common.util.DateSerializer;

/**
 * 部署模板类
 * @author dmw
 *
 */
public class DeployModel {
	private String timestamp;//时间戳，用于创建唯一文件夹目录
	private String deployVersion;//部署版本，如果版本不变，无需重新生成镜像
	private byte autoDeploy;//版本切换
	private String systemAppId;//选择应用程序包
	private String locationLeavel;//位置层级设置
	private String locationStragegy;//分布策略，随机或者资源最优
	private String clusterId;//选择集群
	private String baseImageId;//基础镜像
	private String contentPath;//应用上下文名称
	private Integer instanceCount;//实例数量
	private String deployFiles;//上传配置文件及上传路径：文件名=文件上传路径
	private Integer cpu;//容器cpu限制
	private Integer memery;//容器内存限制
	private Integer Xmxmax;//jvm最大使用内存
	private Integer Xmxmin;//jvm最小使用内存
	private byte regularlyPublish;//是否定时发布
	@JsonSerialize(using = DateSerializer.class)
	private String publishTime;//发布时间
	private String databaseInfo;//数据库
	private boolean dnsEnable;//启用DNS
	private boolean syncDnsEnable;//是否启用同步DNS
	private String sonZone;
	private String dnsComponentId;
	private byte loadBalanceType;//负载类型
	private String loadBalanceId;//f5
	private String f5ExternalPort;//f5对外端口
	private String f5ExternalIp;//f5对外IP
	private String nginxPort;//nginx端口
	private String nginxPortId;//nginx端口Id
	private String http;//http类型
	private String domainName;//域名
	private String appName;//应用名称
	private String port;//端口
	private String url;//校验连接
	private String deployId;//当前已部署版本ID
	private String systemId;//当前物理系统
	private String appRoute;
	private String configRoute;
	private String appImageId;
	private String status;
//	@JsonSerialize(using = DateSerializer.class)
	private String createTime;
	private String deployConfiFile;//上一版本配置文件与应用包变更相关
	private String Dockerfile;//Dockerfile段
	private String systemName;
	private String systemCode;
	private String imageName;
	private String creator;//创建人ID
	private Integer initCount;
	private String parameter;//启动参数
	private String appCategory;
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getDeployVersion() {
		return deployVersion;
	}
	public void setDeployVersion(String deployVersion) {
		this.deployVersion = deployVersion;
	}
	public String getSystemAppId() {
		return systemAppId;
	}
	public void setSystemAppId(String systemAppId) {
		this.systemAppId = systemAppId;
	}
	public String getLocationLeavel() {
		return locationLeavel;
	}
	public void setLocationLeavel(String locationLeavel) {
		this.locationLeavel = locationLeavel;
	}
	
	public String getLocationStragegy() {
		return locationStragegy;
	}
	public void setLocationStragegy(String locationStragegy) {
		this.locationStragegy = locationStragegy;
	}
	public String getClusterId() {
		return clusterId;
	}
	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}
	public String getBaseImageId() {
		return baseImageId;
	}
	public void setBaseImageId(String baseImageId) {
		this.baseImageId = baseImageId;
	}
	public Integer getInstanceCount() {
		return instanceCount;
	}
	public void setInstanceCount(Integer instanceCount) {
		this.instanceCount = instanceCount;
	}
	public String getDeployFiles() {
		return deployFiles;
	}
	public void setDeployFiles(String deployFiles) {
		this.deployFiles = deployFiles;
	}
	public Integer getCpu() {
		return cpu;
	}
	public void setCpu(Integer cpu) {
		this.cpu = cpu;
	}
	public Integer getMemery() {
		return memery;
	}
	public void setMemery(Integer memery) {
		this.memery = memery;
	}
	public byte getRegularlyPublish() {
		return regularlyPublish;
	}
	public void setRegularlyPublish(byte regularlyPublish) {
		this.regularlyPublish = regularlyPublish;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getDatabaseInfo() {
		return databaseInfo;
	}
	public void setDatabaseInfo(String databaseInfo) {
		this.databaseInfo = databaseInfo;
	}
	public String getF5ExternalPort() {
		return f5ExternalPort;
	}
	public void setF5ExternalPort(String f5ExternalPort) {
		this.f5ExternalPort = f5ExternalPort;
	}
	public String getF5ExternalIp() {
		return f5ExternalIp;
	}
	public void setF5ExternalIp(String f5ExternalIp) {
		this.f5ExternalIp = f5ExternalIp;
	}
	public String getNginxPort() {
		return nginxPort;
	}
	public void setNginxPort(String nginxPort) {
		this.nginxPort = nginxPort;
	}
	public String getHttp() {
		return http;
	}
	public void setHttp(String http) {
		this.http = http;
	}
	public String getDomainName() {
		return domainName;
	}
	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDeployId() {
		return deployId;
	}
	public void setDeployId(String deployId) {
		this.deployId = deployId;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getAppRoute() {
		return appRoute;
	}
	public void setAppRoute(String appRoute) {
		this.appRoute = appRoute;
	}
	public String getConfigRoute() {
		return configRoute;
	}
	public void setConfigRoute(String configRoute) {
		this.configRoute = configRoute;
	}
	public String getAppImageId() {
		return appImageId;
	}
	public void setAppImageId(String appImageId) {
		this.appImageId = appImageId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getDeployConfiFile() {
		return deployConfiFile;
	}
	public void setDeployConfiFile(String deployConfiFile) {
		this.deployConfiFile = deployConfiFile;
	}
	public String getDockerfile() {
		return Dockerfile;
	}
	public void setDockerfile(String dockerfile) {
		Dockerfile = dockerfile;
	}
	public String getSystemName() {
		return systemName;
	}
	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public byte getLoadBalanceType() {
		return loadBalanceType;
	}
	public void setLoadBalanceType(byte loadBalanceType) {
		this.loadBalanceType = loadBalanceType;
	}
	public String getLoadBalanceId() {
		return loadBalanceId;
	}
	public void setLoadBalanceId(String loadBalanceId) {
		this.loadBalanceId = loadBalanceId;
	}
	public Integer getXmxmax() {
		return Xmxmax;
	}
	public void setXmxmax(Integer xmxmax) {
		Xmxmax = xmxmax;
	}
	public Integer getXmxmin() {
		return Xmxmin;
	}
	public void setXmxmin(Integer xmxmin) {
		Xmxmin = xmxmin;
	}
	public boolean isDnsEnable() {
		return dnsEnable;
	}
	public void setDnsEnable(boolean dnsEnable) {
		this.dnsEnable = dnsEnable;
	}
	public String getContentPath() {
		return contentPath;
	}
	public void setContentPath(String contentPath) {
		this.contentPath = contentPath;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getNginxPortId() {
		return nginxPortId;
	}
	public void setNginxPortId(String nginxPortId) {
		this.nginxPortId = nginxPortId;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public Integer getInitCount() {
		return initCount;
	}
	public void setInitCount(Integer initCount) {
		this.initCount = initCount;
	}
	public byte getAutoDeploy() {
		return autoDeploy;
	}
	public void setAutoDeploy(byte autoDeploy) {
		this.autoDeploy = autoDeploy;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public boolean isSyncDnsEnable() {
		return syncDnsEnable;
	}
	public void setSyncDnsEnable(boolean syncDnsEnable) {
		this.syncDnsEnable = syncDnsEnable;
	}
	public String getSonZone() {
		return sonZone;
	}
	public void setSonZone(String sonZone) {
		this.sonZone = sonZone;
	}
	public String getDnsComponentId() {
		return dnsComponentId;
	}
	public void setDnsComponentId(String dnsComponentId) {
		this.dnsComponentId = dnsComponentId;
	}
	public String getSystemCode() {
		return systemCode;
	}
	public void setSystemCode(String systemCode) {
		this.systemCode = systemCode;
	}
	public String getAppCategory() {
		return appCategory;
	}
	public void setAppCategory(String appCategory) {
		this.appCategory = appCategory;
	}

}