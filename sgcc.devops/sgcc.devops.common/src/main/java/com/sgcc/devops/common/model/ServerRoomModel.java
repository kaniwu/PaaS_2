package com.sgcc.devops.common.model;


/**
 * 机房模板类
 * @author panjing
 *
 */
public class ServerRoomModel {

	private String serverRoomId;
	private String serverRoomName;
	private String serverRoomRemark;
	private String creator;
	
	
	public String getServerRoomId() {
		return serverRoomId;
	}
	public void setServerRoomId(String serverRoomId) {
		this.serverRoomId = serverRoomId;
	}
	public String getServerRoomName() {
		return serverRoomName;
	}
	public void setServerRoomName(String serverRoomName) {
		this.serverRoomName = serverRoomName;
	}
	public String getServerRoomRemark() {
		return serverRoomRemark;
	}
	public void setServerRoomRemark(String serverRoomRemark) {
		this.serverRoomRemark = serverRoomRemark;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	

}