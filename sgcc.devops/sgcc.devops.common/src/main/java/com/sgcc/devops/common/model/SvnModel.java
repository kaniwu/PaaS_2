package com.sgcc.devops.common.model;

/**
 * svn类
 *
 */
public class SvnModel {

	public SvnModel(String id, String svnUsername, String svnPassword, String svnUrl) {
		super();
		Id = id;
		this.svnUsername = svnUsername;
		this.svnPassword = svnPassword;
		this.svnUrl = svnUrl;
	}
	public SvnModel() {
		super();
	}
	private String Id;
	private String svnUsername;
	private String svnPassword;
	private String svnUrl;
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getSvnUsername() {
		return svnUsername;
	}
	public void setSvnUsername(String svnUsername) {
		this.svnUsername = svnUsername;
	}
	public String getSvnPassword() {
		return svnPassword;
	}
	public void setSvnPassword(String svnPassword) {
		this.svnPassword = svnPassword;
	}
	public String getSvnUrl() {
		return svnUrl;
	}
	public void setSvnUrl(String svnUrl) {
		this.svnUrl = svnUrl;
	}
	
}