package com.sgcc.devops.common.model;

/**
 * 组件类型模板类
 * @author dmw
 *
 */
public class ComponentTypeModel {
	/** 组件类型id */
	private String componentTypeId;
	/** 组件类型名称 */
	private String componentTypeName;

	public String getComponentTypeId() {
		return componentTypeId;
	}

	public void setComponentTypeId(String componentTypeId) {
		this.componentTypeId = componentTypeId;
	}

	public String getComponentTypeName() {
		return componentTypeName;
	}

	public void setComponentTypeName(String componentTypeName) {
		this.componentTypeName = componentTypeName;
	}

	private int page;
	private int limit;
	private String search;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

}