package com.sgcc.devops.common.constant;

/**
 * date：2015年8月12日 下午3:45:56 project name：sgcc-devops-common
 * 类型枚举类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：Type.java description：
 */
public final class Type {

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum USER {
		ADMIN, USER, NOTTYPE
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum HOST {
		SWARM, DOCKER, REGISTRY, NGINX, UNUSED
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum CLUSTER {
		DOCKER, REGISTRY
	}

	/**
	 * @author langzi
	 * @version 1.0
	 *
	 */
	public static enum ACTION {
		USER, DETAIL, MODAL
	}

	/**
	 * @author yangqinglin
	 * @version 1.0
	 *
	 */
	public static enum APPLICATION {
		EXTERNAL, MIDDLEWARE
	}

	/**
	 * @author yangqinglin
	 * @version 1.0
	 *
	 */
	public static enum IMAGE_OPER {
		INSERT, DELETE, UPDATE
	}

	public static enum IMAGE_TYPE {
		BASIC, APP, NGINX
	}
	/**
	 * 中间件、app、nginx、基础镜像
	 * date：2016年2月2日 下午2:58:03
	 * project name：sgcc.devops.common
	 * @author  mayh
	 * @version 1.0   
	 * @since JDK 1.7.0_21  
	 * file name：Type.java
	 * description：
	 */
	public static enum CON_TYPE {
		MID, APP,NGINX,BASIC,OTHER,WAITDEL
	}

	public static enum TARGET_TYPE {
		HOST, CONTAINER
	}

	public static enum DB_TYPE {
		ORACLE, MYSQL, DB2, ISOLATION
	}

	public static enum DIC_TYPE {
		LOG, DB
	}
	public static enum OPERATION {
		UNUSED,ADD, DEL,RESTART
	}
	public static enum AUTHORITY {
		PARENT, CHILD
	}
	public static enum COMPPORT_TYPE {
		DEPLOY,REGISTRY
	}
}
