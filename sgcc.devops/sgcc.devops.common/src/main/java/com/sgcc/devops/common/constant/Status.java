package com.sgcc.devops.common.constant;

/**
 * date：2015年8月12日 下午3:45:56 project name：sgcc-devops-common
 * 状态枚举类
 * @author langzi
 * @version 1.0
 * @since JDK 1.7.0_21 file name：Status.java description：
 */
public final class Status {

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum USER {
		DELETE, NORMAL
	}

	/**删除状态，正常，监控异常，离线状态
	 * @author langzi
	 * @version 1.0
	 */
	public static enum HOST {
		DELETE, NORMAL, ABNORMAL,OFFLINE
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum CLUSTER {
		DELETE, NORMAL, ABNORMAL,ISOLATED,MIGRATING
	}

	/**删除，正常，异常，停止
	 * @author langzi
	 * @version 1.0
	 */
	public static enum REGISTRY {
		DELETE, NORMAL, ABNORMAL,STOP
	}
	
	/**删除状态、未安装、正常、停止
	 * @author yueyong
	 * @version 1.0
	 */
	public static enum REGISTRYHOST {
		DELETE, UNINSTAILL, NORMAL, ABNORMAL
	}

	/**
	 * date：2015年8月31日 下午4:50:31 project name：sgcc-devops-common
	 * 
	 * @author mayh
	 * @version 1.0
	 * @since JDK 1.7.0_21 file name：Status.java description：
	 */
	public static enum IMAGE {
		DELETED, NORMAL, MAKED, ABNORMAL
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum APPLICATION {
		DELETE, NORMAL, ABNORMAL
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum CONTAINER {
		DELETE, EXIT, UP, HALT
	}

	public static enum POWER {
		OFF, UP
	}

	/**
	 * @author langzi
	 * @version 1.0
	 */
	public static enum PARAMETER {
		DELETE, NORMAL, ABNORMAL
	}

	public static enum LOADBALANCE {
		DELETE, NORMAL, ABNORMAL
	}

	public static enum COMPONENT {
		DELETE, NORMAL, ABNORMAL
	}

	public static enum SYSTEM {
		DELETE, DEPLOY, UNDEPLOY
	}
	
	public static enum ElSASTICITYSTATUS{
		UNUSED,USED
	}
	/**删除状态，正常，监控异常，离线状态
	 * @author langzi
	 * @version 1.0
	 */
	public static enum DOCKER {
		UNINSTAILL, NORMAL, ABNORMAL
	}
	public static enum AUTHTYPE {
		PAGE, BUTTON
	}
	public static enum ROLE {
		DELETE, NORMAL
	}
	
	/**
	 *删除状态、正常(物理系统 程序包)
	 * @author yueyong
	 */
	public static enum SYSTEMAPP {
		DELETE, NORMAL
	}
	
	/**
	 * 删除状态、正常(预加载包)
	 * @author yueyong
	 */
	public static enum LIBRARY {
		DELETE, NORMAL
	}
	
	public static enum YESNO{
		NO,YES
	}
}
