package com.sgcc.devops.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.log4j.Logger;

/**
 * @author langzi
 *	IO工具类
 */
public class IOUtils {

	private final static Logger m_logger = Logger.getLogger(IOUtils.class);

	/**
	 * @author langzi
	 * @param is
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public static String toString(InputStream is) {
		BufferedReader br = toBufferedReader(is);
		return toStringBuffer(br).toString();
	}

	/**
	 * @author langzi
	 * @param br
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public static StringBuffer toStringBuffer(BufferedReader br) {
		StringBuffer sb = new StringBuffer();

		try {
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (Exception e) {
			m_logger.error(e);
		}

		return sb;
	}

	/**
	 * @author langzi
	 * @param is
	 * @return
	 * @version 1.0 2015年8月17日
	 */
	public static BufferedReader toBufferedReader(InputStream is) {
		try {
			return new BufferedReader(new InputStreamReader(is));
		} catch (Exception e) {
			m_logger.error(e);
			return null;
		}
	}

	/**
	 * @author langzi
	 * @param reader
	 * @version 1.0 2015年8月17日
	 */
	public static void close(Reader reader) {
		if (!ObjectUtils.isNull(reader)) {
			try {
				reader.close();
			} catch (IOException e) {
				m_logger.warn(e);
			}
		}
	}

	/**
	 * @author langzi
	 * @param is
	 * @version 1.0 2015年8月17日
	 */
	public static void close(InputStream is) {
		if (!ObjectUtils.isNull(is)) {
			try {
				is.close();
			} catch (IOException e) {
				m_logger.warn(e);
			}
		}
	}
}
