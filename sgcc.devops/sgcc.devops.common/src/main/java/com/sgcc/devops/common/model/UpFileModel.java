package com.sgcc.devops.common.model;

/**
 * 上传文件模板类
 * @author dmw
 *
 */
public class UpFileModel {

	private String fileStartName;
	private String fileEndName;
	private String hostList;
	private String fileSrc;
	private String componentId;
	private String appId;
	private String webcontainer;
	private String timestamp;
	
	private String appFileStartName;
	private String deployConfigStartName;
	private String systemId;
	
	private String libraryFileStartName;
	
	private String hostCompFileStartName;
	
	private String uuid;

	public String getHostList() {
		return hostList;
	}

	public void setHostList(String hostList) {
		this.hostList = hostList;
	}

	public String getFileStartName() {
		return fileStartName;
	}

	public void setFileStartName(String fileStartName) {
		this.fileStartName = fileStartName;
	}

	public String getFileEndName() {
		return fileEndName;
	}

	public void setFileEndName(String fileEndName) {
		this.fileEndName = fileEndName;
	}

	public String getFileSrc() {
		return fileSrc;
	}

	public void setFileSrc(String fileSrc) {
		this.fileSrc = fileSrc;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getWebcontainer() {
		return webcontainer;
	}

	public void setWebcontainer(String webcontainer) {
		this.webcontainer = webcontainer;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getAppFileStartName() {
		return appFileStartName;
	}

	public void setAppFileStartName(String appFileStartName) {
		this.appFileStartName = appFileStartName;
	}

	public String getLibraryFileStartName() {
		return libraryFileStartName;
	}

	public void setLibraryFileStartName(String libraryFileStartName) {
		this.libraryFileStartName = libraryFileStartName;
	}

	public String getHostCompFileStartName() {
		return hostCompFileStartName;
	}

	public void setHostCompFileStartName(String hostCompFileStartName) {
		this.hostCompFileStartName = hostCompFileStartName;
	}

	public String getDeployConfigStartName() {
		return deployConfigStartName;
	}

	public void setDeployConfigStartName(String deployConfigStartName) {
		this.deployConfigStartName = deployConfigStartName;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
