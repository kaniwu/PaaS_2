/**
 * 
 */
package com.sgcc.devops.common.model;


/**  
 * date：2016年7月27日 下午2:10:56
 * project name：sgcc.devops.common
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：F5Model.java
 * description：  
 */
public class F5Model {
	private String monitorName;
	private long monitorInterval;//检测时间间隔，建议5s
	private long monitorTimeout;//超时时间，建议16s，3*interval+1
	private String monitorTemplateName;//monitor模板名称，默认可使用http
	private String monitorUri;// 检测的链接，使用相对路径即可，eg： /nesp/app/heartbeat
	private String monitorReceiveString;// 检测页面返回的特殊字符串，不建议使用html等页面常见字符串
	
	private String poolName;//
	private String members;// ip端口冒号隔开，多个member用分号隔开 eg: 172.16.0.80:80;172.16.0.4:8080
	
	private String vsName;
	private String vsIp;
	private long vsPort;
	
	private String hostIp;
	private String user;
	private String pwd;
	private String port;
	
	public String getMonitorName() {
		return monitorName;
	}
	public void setMonitorName(String monitorName) {
		this.monitorName = monitorName;
	}
	public long getMonitorInterval() {
		return monitorInterval;
	}
	public void setMonitorInterval(long monitorInterval) {
		this.monitorInterval = monitorInterval;
	}
	public long getMonitorTimeout() {
		return monitorTimeout;
	}
	public void setMonitorTimeout(long monitorTimeout) {
		this.monitorTimeout = monitorTimeout;
	}
	public String getMonitorTemplateName() {
		return monitorTemplateName;
	}
	public void setMonitorTemplateName(String monitorTemplateName) {
		this.monitorTemplateName = monitorTemplateName;
	}
	public String getMonitorUri() {
		return monitorUri;
	}
	public void setMonitorUri(String monitorUri) {
		this.monitorUri = monitorUri;
	}
	public String getMonitorReceiveString() {
		return monitorReceiveString;
	}
	public void setMonitorReceiveString(String monitorReceiveString) {
		this.monitorReceiveString = monitorReceiveString;
	}
	public String getPoolName() {
		return poolName;
	}
	public void setPoolName(String poolName) {
		this.poolName = poolName;
	}
	public String getMembers() {
		return members;
	}
	public void setMembers(String members) {
		this.members = members;
	}
	public String getVsName() {
		return vsName;
	}
	public void setVsName(String vsName) {
		this.vsName = vsName;
	}
	public String getVsIp() {
		return vsIp;
	}
	public void setVsIp(String vsIp) {
		this.vsIp = vsIp;
	}
	public long getVsPort() {
		return vsPort;
	}
	public void setVsPort(long vsPort) {
		this.vsPort = vsPort;
	}
	public String getHostIp() {
		return hostIp;
	}
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	
}
