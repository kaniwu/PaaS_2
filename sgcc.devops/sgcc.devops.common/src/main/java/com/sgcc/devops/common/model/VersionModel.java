package com.sgcc.devops.common.model;


public class VersionModel {
	private String hostID;
	private String hostIp;
	private String hostName;
	private String hostPassword;
	private String hostPort;
	private String type;
	private String dockerVersion;
	private String swarmVersion;
	private String registryVersion;
	private String nginxVersion;
	public String getHostIp() {
		return hostIp;
	}
	public String getHostID() {
		return hostID;
	}
	public void setHostID(String hostID) {
		this.hostID = hostID;
	}
	public void setHostIp(String hostIp) {
		this.hostIp = hostIp;
	}
	public String getHostName() {
		return hostName;
	}
	public void setHostName(String hostName) {
		this.hostName = hostName;
	}
	public String getHostPassword() {
		return hostPassword;
	}
	public void setHostPassword(String hostPassword) {
		this.hostPassword = hostPassword;
	}
	public String getHostPort() {
		return hostPort;
	}
	public void setHostPort(String hostPort) {
		this.hostPort = hostPort;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDockerVersion() {
		return dockerVersion;
	}
	public void setDockerVersion(String dockerVersion) {
		this.dockerVersion = dockerVersion;
	}
	public String getSwarmVersion() {
		return swarmVersion;
	}
	public void setSwarmVersion(String swarmVersion) {
		this.swarmVersion = swarmVersion;
	}
	public String getRegistryVersion() {
		return registryVersion;
	}
	public void setRegistryVersion(String registryVersion) {
		this.registryVersion = registryVersion;
	}
	public String getNginxVersion() {
		return nginxVersion;
	}
	public void setNginxVersion(String nginxVersion) {
		this.nginxVersion = nginxVersion;
	}

}