package com.sgcc.devops.common.util;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.List;

import org.xbill.DNS.ARecord;
import org.xbill.DNS.DClass;
import org.xbill.DNS.Flags;
import org.xbill.DNS.Lookup;
import org.xbill.DNS.Message;
import org.xbill.DNS.Name;
import org.xbill.DNS.Record;
import org.xbill.DNS.Resolver;
import org.xbill.DNS.Section;
import org.xbill.DNS.SimpleResolver;
import org.xbill.DNS.TSIG;
import org.xbill.DNS.Type;
import org.xbill.DNS.Update;
import org.xbill.DNS.ZoneTransferIn;

/**
 * DNS类
 * @author lianyadong
 *
 */
public class DnsZone {
	
	private String ip;
	private int port = 53;
	private String key;
	private String secret;
	private String zone;
	
	public DnsZone(String ip,int port,String key,String secret,String zone){
		this.ip = ip;
		this.port = port;
		this.key = key;
		this.secret = secret;
		this.zone = zone;
	}
	
	public DnsZone(String ip,String key,String secret,String zone){
		this.ip = ip;
		this.key = key;
		this.secret = secret;
		this.zone = zone;
	}
	
	/**
	 * 根据dns查询ip
	 * @param dns
	 * @return
	 * @throws IOException
	 */
	public String queryHostIpByDns(String dns) throws IOException{
		Resolver resolver = new SimpleResolver(this.ip);  
        resolver.setPort(this.port); 
        Lookup lookup = new Lookup(dns, Type.A);  
        lookup.setResolver(resolver);  
        lookup.run();  
        String hostIp = null;
        if (lookup.getResult() == Lookup.SUCCESSFUL) {  
        	hostIp = lookup.getAnswers()[0].rdataToString();  
        }  
        return hostIp;
	}
	
	/**
	 * 获取dns服务器指定zone的配置
	 * @param zone eg "dev.js.sgcc.com.cn."
	 * @return
	 * @throws Exception
	 */
    public Message transferZone() throws Exception {  
        ZoneTransferIn xfr = ZoneTransferIn.newAXFR(new Name(zone), this.ip, null);  
		List<Record> records = xfr.run();  
        Message response = new Message();  
        response.getHeader().setFlag(Flags.AA);  
        response.getHeader().setFlag(Flags.QR);  
        // response.addRecord(query.getQuestion(),Section.QUESTION);  
        Iterator<Record> it = records.iterator();  
        while (it.hasNext()) {  
            response.addRecord(it.next(), Section.ANSWER);  
        }  
        return response;
    } 
	
    
 	/**
 	 * 在zone配置文件中添加域名 ip的映射
 	 * @param code
 	 * @param hostIp
 	 * @return
 	 * @throws Exception
 	 */
    public Message addHostIpAndDNSToZone(String code,String hostIp) throws Exception {  
        Name name = Name.fromString(zone);  
        Name host = Name.fromString(code, name);  
        Update update = new Update(name, DClass.IN);  
        Record record = new ARecord(host, DClass.IN, 3600, InetAddress.getByName(hostIp));  
        update.add(record);  
        Resolver resolver = new SimpleResolver(this.ip);  
        resolver.setPort(this.port);  
        TSIG tsig = new TSIG(this.key, this.secret);  
        resolver.setTSIGKey(tsig);  
        resolver.setTCP(true);  
        Message response = resolver.send(update);  
        return response;
    }  
	
    /**
     * 在zone配置文件中删除域名 ip的映射
     * @param code
     * @param hostIp
     * @return
     * @throws Exception
     */
    public Message deleteHostIpAndDNSToZone(String code,String hostIp) throws Exception {  
        Name name = Name.fromString(zone);  
        Name host = Name.fromString(code, name);  
        Update update = new Update(name, DClass.IN);  
        Record record = new ARecord(host, DClass.IN, 3600, InetAddress.getByName(hostIp));  
        update.delete(record);
        Resolver resolver = new SimpleResolver(this.ip);  
        resolver.setPort(this.port);  
        TSIG tsig = new TSIG(this.key, this.secret);  
        resolver.setTSIGKey(tsig);  
        resolver.setTCP(true);  
        Message response = resolver.send(update);  
        return response;
    }  
	
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
	
	
	public static void main(String[] args) throws Exception {  
		String ip = "192.168.121.146";
		String key = "devsgcc";
		String secret = "xdvSc7sRXvMUA9FGurj8NZDlEmqg6LbDeTQ99OZe5rtkFOwJR9GKnayDa8AJiUtGLBHzAWovje1yG6M3hK9rzw==";
		String zone = "paas.com.cn.";
		DnsZone dnsZone = new DnsZone(ip, key, secret, zone);
		
//		String hostIp = dnsZone.queryHostIpByDns("lyd.paas.com.cn");
//		System.out.println(hostIp);
		
	    Message message1 = dnsZone.transferZone();
	    System.out.println(message1.getRcode());
		
	    Message messageAdd = dnsZone.addHostIpAndDNSToZone("mytest16", "192.168.121.111");
//		System.out.println(messageAdd);
		
//		Message messageDelete = dnsZone.deleteHostIpAndDNSToZone("mytest5", "192.168.121.112");
//		System.out.println(messageDelete);
  }  
}
