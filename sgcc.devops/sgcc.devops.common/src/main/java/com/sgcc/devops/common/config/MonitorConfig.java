package com.sgcc.devops.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 监控相关配置类
 * @author dmw
 *
 */
@Component("monitorConfig")
public class MonitorConfig {

	@Value("${monitor.filedate}")
	private String monitorFileDate;

	@Value("${monitor.containerdate}")
	private String monitorContainerDate;

	@Value("${monitor.hostdate}")
	private String monitorHostDate;
	//主机cpu使用上限
	@Value("${monitor.cpu}")
	private String monitorCpu;
	//主机内存使用上限
	@Value("${monitor.mem}")
	private String monitorMem;
	@Value("${host.monitor.store.path}")
	private String hostMonitorStorePath;//主机监控目录
	
	@Value("${host.monitor.store.used}")
	private String hostMonitorStoreUsed;//主机监控目录上限
	@Value("${http.read.times}")
	private int httpReadTimes;//容器监控超时时轮询次数
	
	public String getMonitorFileDate() {
		return monitorFileDate;
	}

	public void setMonitorFileDate(String monitorFileDate) {
		this.monitorFileDate = monitorFileDate;
	}

	public String getMonitorContainerDate() {
		return monitorContainerDate;
	}

	public void setMonitorContainerDate(String monitorContainerDate) {
		this.monitorContainerDate = monitorContainerDate;
	}

	public String getMonitorHostDate() {
		return monitorHostDate;
	}

	public void setMonitorHostDate(String monitorHostDate) {
		this.monitorHostDate = monitorHostDate;
	}

	public String getMonitorCpu() {
		return monitorCpu;
	}

	public void setMonitorCpu(String monitorCpu) {
		this.monitorCpu = monitorCpu;
	}

	public String getMonitorMem() {
		return monitorMem;
	}

	public void setMonitorMem(String monitorMem) {
		this.monitorMem = monitorMem;
	}

	public String getHostMonitorStorePath() {
		return hostMonitorStorePath;
	}

	public void setHostMonitorStorePath(String hostMonitorStorePath) {
		this.hostMonitorStorePath = hostMonitorStorePath;
	}

	public String getHostMonitorStoreUsed() {
		return hostMonitorStoreUsed;
	}

	public void setHostMonitorStoreUsed(String hostMonitorStoreUsed) {
		this.hostMonitorStoreUsed = hostMonitorStoreUsed;
	}

	public int getHttpReadTimes() {
		return httpReadTimes;
	}

	public void setHttpReadTimes(int httpReadTimes) {
		this.httpReadTimes = httpReadTimes;
	}
}
