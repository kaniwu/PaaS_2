/**
 * 
 */
package com.sgcc.devops.common.model;

/**
 * date：2015年8月24日 上午12:22:54 project name：sgcc-devops-common
 * 仓库模板类
 * @author mayh
 * @version 1.0
 * @since JDK 1.7.0_21 file name：RegistryModel.java description：
 */
public class RegistryModel {

	private String registryId;

	private String registryName;

	private int registryPort;

	private String hostId;

	private String registryDesc;
	
	private String lbId;
	
	private String registryDns;

	private int page;
	private int limit;
	private String search;

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getRegistryId() {
		return registryId;
	}

	public void setRegistryId(String registryId) {
		this.registryId = registryId;
	}

	public String getRegistryName() {
		return registryName;
	}

	public void setRegistryName(String registryName) {
		this.registryName = registryName;
	}

	public int getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(int registryPort) {
		this.registryPort = registryPort;
	}

	public String getHostId() {
		return hostId;
	}

	public void setHostId(String hostId) {
		this.hostId = hostId;
	}

	public String getRegistryDesc() {
		return registryDesc;
	}

	public void setRegistryDesc(String registryDesc) {
		this.registryDesc = registryDesc;
	}

	public String getLbId() {
		return lbId;
	}

	public void setLbId(String lbId) {
		this.lbId = lbId;
	}

	public String getRegistryDns() {
		return registryDns;
	}

	public void setRegistryDns(String registryDns) {
		this.registryDns = registryDns;
	}

}
