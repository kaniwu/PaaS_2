package com.sgcc.devops.web.controller.action;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sgcc.devops.common.constant.Type;
import com.sgcc.devops.web.manager.MonitorManager;
/**  
 * date：2016年2月4日 下午1:49:08
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：MonitorController.java
 * description：  主机、容器监控控制类
 */
@Controller
@RequestMapping("/monitor")
public class MonitorController {
	@Autowired
	private MonitorManager monitorManager;

	/**
	 * 跳转到容器监控界面
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/container/{id}.html")
	private ModelAndView containerPage(@PathVariable String id) {
		ModelAndView mav = new ModelAndView("monitor/container");
		mav.addObject("containerName", monitorManager.getContainerName(id));
		mav.addObject("container", id);
		return mav;
	}

	/**
	 * 跳转到宿主机监控界面
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/host/{id}.html")
	private ModelAndView hostPage(@PathVariable String id) {
		ModelAndView mav = new ModelAndView("monitor/host");
		mav.addObject("hostName", monitorManager.getHost(id));
		mav.addObject("host", id);
		return mav;
	}

	/**
	 * 获取容器监控数据
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/container/{id}")
	@ResponseBody
	public Map<String, Object> container(@PathVariable String id) {
		return monitorManager.container(id);
	}

	/**
	 * 获取宿主机监控数据
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("/host/{id}")
	@ResponseBody
	public Map<String, Object> host(@PathVariable String id) {
		return monitorManager.host(id);
	}

	@RequestMapping("/host/getData")
	@ResponseBody
	public Map<String, Object> host(String hostId, int model) {

		return monitorManager.getStaticData(hostId, model,Type.TARGET_TYPE.HOST.ordinal());
	}
	
	/**
	 * 获取历史监控记录
	 */
	@RequestMapping("/container/getData")
	@ResponseBody
	public Map<String, Object> container(String containerId, int model) {
		return monitorManager.getStaticData(containerId, model,Type.TARGET_TYPE.CONTAINER.ordinal());
	}
	
	/**
	 * 获取初始化监控记录
	 */
	@RequestMapping("/getMonitorData")
	@ResponseBody
	public Map<String, Object> getContainer(String id,int type) {
		return monitorManager.getMonitorData(id,type);
	}
	
}
