package com.sgcc.devops.service.Impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.config.JdbcConfig;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.config.TmpHostConfig;
import com.sgcc.devops.common.model.LibraryModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.Library;
import com.sgcc.devops.dao.intf.AppPrelibMapper;
import com.sgcc.devops.dao.intf.DeployMapper;
import com.sgcc.devops.dao.intf.LibraryMapper;
import com.sgcc.devops.service.LibraryService;

/**  
 * date：2016年3月14日
 * project name：sgcc.devops.service
 * @author  liyp
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LibraryServiceImpl.java
 * description：库文件数据维护接口实现类
 */
@Component
public class LibraryServiceImpl implements LibraryService {

	private static Logger logger = Logger.getLogger(LibraryServiceImpl.class);
	@Resource
	private LibraryMapper libraryMapper;
	@Resource
	private DeployMapper deployMapper;
	@Resource
	private TmpHostConfig tmpHostConfig;
	@Resource
	private JdbcConfig jdbcConfig;
	@Resource
	private LocalConfig localConfig;
	@Resource 
	private AppPrelibMapper appPrelibMapper;
	/**
	* @author liyp
	* @param pagerModel
	* @param Library
	* @return GridBean
	* @version 1.0
	* get a GridBean for Library List
	* 2016年3月14日
	*/
	@Override
	public GridBean listLibrary(PagerModel pagerModel,Library library) {
		int page = pagerModel.getPage();
		int pageSize = pagerModel.getRows();
		String sidx = pagerModel.getSidx();
		String sord = pagerModel.getSord();
		if(!StringUtils.isEmpty(sidx)&&!StringUtils.isEmpty(sord)){
			if(sidx.equals("libraryName")){
				PageHelper.startPage(page, pageSize,"LIBRARY_NAME "+sord);
			}
			if(sidx.equals("libraryCreatorName")){
				PageHelper.startPage(page, pageSize,"LIBRARY_CREATOR "+sord);
			}
			if(sidx.equals("libraryCreatetime")){
				PageHelper.startPage(page, pageSize,"LIBRARY_CREATETIME "+sord);
			}
		}else{
			PageHelper.startPage(page, pageSize);
		}
		PageHelper.startPage(page, pageSize);
		
		library.setDialect(jdbcConfig.getDialect());
		List<Library> librarys= libraryMapper.getLibraryList(library);
		int totalpage = ((Page<?>) librarys).getPages();
		Long totalNum = ((Page<?>) librarys).getTotal();
		List<LibraryModel> libraryModels = new ArrayList<LibraryModel>();
		for (Library templibrary : librarys) {
			LibraryModel libraryModel = new LibraryModel();
			libraryModel.setLibraryId(templibrary.getLibraryId());
			libraryModel.setLibraryName(templibrary.getLibraryName());
			libraryModel.setLibraryDescription(templibrary.getLibraryDescription());
			libraryModel.setLibraryFilePath(templibrary.getLibraryFilePath());
			libraryModel.setLibraryCreator(templibrary.getLibraryCreator());
			libraryModel.setLibraryCreatetime(templibrary.getLibraryCreatetime());
			libraryModel.setLibraryCreatorName(templibrary.getUserName());
			libraryModel.setStatus(templibrary.getStatus());
			libraryModels.add(libraryModel);
		}
		GridBean gridBean = new GridBean(page, totalpage, totalNum.intValue(), libraryModels);
		return gridBean;
	}

	/**
	* @author liyp
	* @param LibraryModel
	* @return int
	* @version 1.0
	* create a Library record
	* 2016年3月14日
	*/
	@Override
	public int createLibrary(LibraryModel libraryModel) {
		Library library = new Library();
		library.setLibraryId(KeyGenerator.uuid());
		library.setLibraryName(libraryModel.getLibraryName());
		library.setLibraryDescription(libraryModel.getLibraryDescription());
		library.setLibraryFilePath(tmpHostConfig.getLibraryPath()+libraryModel.getLibraryFilePath());
		library.setLibraryLocalPath(localConfig.getLocalLibraryPath()+libraryModel.getLibraryFilePath());
		library.setLibraryCreator(libraryModel.getLibraryCreator());
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		library.setLibraryCreatetime(new Date());
		library.setStatus(1);
		int i = libraryMapper.insertLibrary(library);
		return i;
	}
	/**
	* @author liyp
	* @param LibraryModel
	* @return int
	* @version 1.0
	* update a Library record
	* 2016年3月14日
	*/
	@Override
	public int updateLibrary(LibraryModel libraryModel) {
		Library library = new Library();
		library.setLibraryId(libraryModel.getLibraryId());
		library.setLibraryName(libraryModel.getLibraryName());
		library.setLibraryDescription(libraryModel.getLibraryDescription());
		library.setStatus(libraryModel.getStatus());
		int i = libraryMapper.updateLibrary(library);
		return i;
	}

	/**
	* @author liyp
	* @param LibraryModel
	* @return int
	* @version 1.0
	* check Library  record Used
	* 2016年3月16日
	*/
	@Override
	public boolean checkLibraryUsed(String libraryId) {
//		int usedCount= deployMapper.selectDeployPrelibCount(libraryId);
		int usedCount = appPrelibMapper.selectPrelibCount(libraryId);
		if (usedCount>0) {
			return true;
		}else{
			return false;
		}
	}
	/**
	* @author liyp
	* @param LibraryModel
	* @return int
	* @version 1.0
	* delete a Library record
	* 2016年3月14日
	*/
	@Override
	public int deleteLibrary(LibraryModel libraryModel) {
		Library library = new Library();
		library.setLibraryId(libraryModel.getLibraryId());
		library.setStatus(0);
		int i = libraryMapper.deleteLibrary(libraryModel.getLibraryId());
		File file = null;
		try {
			String source = FileUploadUtil.check(library.getLibraryLocalPath());
			file = new File(source);
			if (file.exists())
				file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}
	
}
