<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.sgcc.devops.dao.intf.MonitorMapper">
	<resultMap id="BaseResultMap" type="com.sgcc.devops.dao.entity.Monitor">
		<id column="MONITOR_ID" property="monitorId" jdbcType="VARCHAR" />
		<result column="MONITOR_ITEM" property="monitorItem" jdbcType="TINYINT" />
		<result column="TARGET_TYPE" property="targetType" jdbcType="TINYINT" />
		<result column="MONITOR_TIME" property="monitorTime" jdbcType="TIMESTAMP" />
		<result column="TARGET_ID" property="targetId" jdbcType="VARCHAR" />
		<result column="CPU" property="cpu" jdbcType="VARCHAR" />
		<result column="MEM" property="mem" jdbcType="VARCHAR" />
		<result column="NETIN" property="netin" jdbcType="VARCHAR" />
		<result column="NETOUT" property="netout" jdbcType="VARCHAR" />
		<result column="DISK_SPACE" property="diskSpace" jdbcType="VARCHAR" />
	</resultMap>
	<sql id="Base_Column_List">
		MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE
	</sql>

	<insert id="insert" parameterType="com.sgcc.devops.dao.entity.Monitor">
		insert into dop_monitor
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="monitorId != null and monitorId != ''">
				MONITOR_ID,
			</if>
			<if test="targetType != null ">
				TARGET_TYPE,
			</if>
			<if test="monitorTime != null'">
				MONITOR_TIME,
			</if>
			<if test="targetId != null and targetId !=''">
				TARGET_ID,
			</if>
			<if test="cpu != null and cpu != ''">
				CPU,
			</if>
			<if test="mem != null and mem != ''">
				MEM,
			</if>
			<if test="netin != null and netin != ''">
				NETIN,
			</if>
			<if test="netout != null and netout != ''">
				NETOUT,
			</if>
			<if test="diskSpace != null and diskSpace != ''">
				DISK_SPACE,
			</if>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
			<if test="monitorId != null ">
				#{monitorId,jdbcType=VARCHAR},
			</if>
			<if test="targetType != null ">
				#{targetType,jdbcType=TINYINT},
			</if>
			<if test="monitorTime != null">
				#{monitorTime,jdbcType=TIMESTAMP},
			</if>
			<if test="targetId != null and targetId != ''">
				#{targetId,jdbcType=VARCHAR},
			</if>
			<if test="cpu != null and cpu != ''">
				#{cpu,jdbcType=VARCHAR},
			</if>
			<if test="mem != null and mem != ''">
				#{mem,jdbcType=VARCHAR},
			</if>
			<if test="netin != null and netin != ''">
				#{netin,jdbcType=VARCHAR},
			</if>
			<if test="netout != null and netout != ''">
				#{netout,jdbcType=VARCHAR},
			</if>
			<if test="diskSpace != null and diskSpace != ''">
				#{diskSpace,jdbcType=VARCHAR},
			</if>
		</trim>
	</insert>

	<select id="select" resultMap="BaseResultMap"
		parameterType="com.sgcc.devops.dao.entity.Monitor">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where 1=1
		<if test="targetType != null or targetType==0 ">
			AND TARGET_TYPE=#{targetType,jdbcType=TINYINT}
		</if>
		<if test="targetId != null and targetId != ''">
			AND TARGET_ID=#{targetId,jdbcType=VARCHAR}
		</if>
		<if test="monitorTime != null">
			AND MONITOR_TIME &gt; #{monitorTime,jdbcType=TIMESTAMP}
		</if>
		order by MONITOR_TIME asc
	</select>
	<select id="getAvgData" resultMap="BaseResultMap"
		parameterType="com.sgcc.devops.dao.entity.Monitor">
		select
		avg(cpu) as CPU,avg(mem) as MEM,avg(NETIN) as NETIN,avg(NETOUT) as NETOUT,avg(DISK_SPACE) as DISK_SPACE
		from dop_monitor
		where TARGET_TYPE = 0
		<if test="targetId != null and targetId != ''">
			AND TARGET_ID=#{targetId,jdbcType=VARCHAR}
		</if>
		<if test="monitorTime != null">
			AND MONITOR_TIME &gt; #{monitorTime,jdbcType=TIMESTAMP}
		</if>
	</select>
	<select id="selectByTypeAndId" resultMap="BaseResultMap"
		parameterType="com.sgcc.devops.dao.entity.Monitor">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where 1=1
		<if test="targetType != null ">
			AND TARGET_TYPE=#{targetType,jdbcType=TINYINT}
		</if>
		<if test="targetId != null and targetId != ''">
			AND TARGET_ID=#{targetId,jdbcType=VARCHAR}
		</if>
		ORDER BY MONITOR_TIME DESC 
	</select>

	<select id="selectByTime" resultMap="BaseResultMap"
		parameterType="java.util.Map">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where 1=1
		<if test="startTime != null">  
                <![CDATA[ AND MONITOR_TIME >= "${startTime}" ]]>
		</if>
		<if test="endTime != null">  
                <![CDATA[ AND MONITOR_TIME <=  "${endTime}" ]]>
		</if>
	</select>

	<select id="selectByPrimaryKey" resultMap="BaseResultMap"
		parameterType="java.lang.String">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where MONITOR_ID = #{monitorId,jdbcType=VARCHAR}
	</select>

	<select id="selectByType" resultMap="BaseResultMap"
		parameterType="java.lang.Integer">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where TARGET_TYPE = #{targetType,jdbcType=TINYINT}
	</select>

	<select id="selectBytargetId" resultMap="BaseResultMap"
		parameterType="java.lang.String">
		select
		<include refid="Base_Column_List" />
		from dop_monitor
		where TARGET_ID = #{targetId,jdbcType=VARCHAR}
	</select>


	<delete id="deleteByPrimaryKey" parameterType="java.lang.String">
		delete from
		dop_monitor
		where MONITOR_ID = #{monitorId,jdbcType=VARCHAR}
	</delete>

	<delete id="deleteByType" parameterType="java.lang.Integer">
		delete from dop_monitor
		where TARGET_TYPE = #{targetType,jdbcType=TINYINT}
	</delete>

	<delete id="deleteBytargetId" parameterType="java.lang.String">
		delete from
		dop_monitor
		where TARGET_ID = #{targetId,jdbcType=VARCHAR}
	</delete>
	<insert id="dataTransfer" parameterType="java.util.Date">
		Insert into dop_monitor_file(
		MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE) 
		select MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE 
		from dop_monitor 
		WHERE dop_monitor.MONITOR_TIME &gt; #{monitorTime,jdbcType=TIMESTAMP}
	</insert>
</mapper>