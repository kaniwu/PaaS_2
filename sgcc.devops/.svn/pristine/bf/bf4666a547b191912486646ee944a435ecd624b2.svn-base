package org.sgcc.devops.f5API.bean;
/*
 * The contents of this file are subject to the "END USER LICENSE AGREEMENT FOR F5
 * Software Development Kit for iControl"; you may not use this file except in
 * compliance with the License. The License is included in the iControl
 * Software Development Kit.
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See
 * the License for the specific language governing rights and limitations
 * under the License.
 *
 * The Original Code is iControl Code and related documentation
 * distributed by F5.
 *
 * The Initial Developer of the Original Code is F5 Networks,
 * Inc. Seattle, WA, USA. Portions created by F5 are Copyright (C) 1996-2004 F5
 * Inc. All Rights Reserved.  iControl (TM) is a registered trademark of F5 Netw
 *
 * Alternatively, the contents of this file may be used under the terms
 * of the GNU General Public License (the "GPL"), in which case the
 * provisions of GPL are applicable instead of those above.  If you wish
 * to allow use of your version of this file only under the terms of the
 * GPL and not to allow others to use your version of this file under the
 * License, indicate your decision by deleting the provisions above and
 * replace them with the notice and other provisions required by the GPL.
 * If you do not delete the provisions above, a recipient may use your
 * version of this file under either the License or the GPL.
 */

import java.rmi.RemoteException;

import org.sgcc.devops.f5API.iControl.CommonIPPortDefinition;
import org.sgcc.devops.f5API.iControl.CommonProtocolType;
import org.sgcc.devops.f5API.iControl.CommonVirtualServerDefinition;
import org.sgcc.devops.f5API.iControl.LocalLBAddressType;
import org.sgcc.devops.f5API.iControl.LocalLBLBMethod;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorCommonAttributes;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorIPPort;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorMonitorTemplate;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorRule;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorRuleType;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorStrPropertyType;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorStringValue;
import org.sgcc.devops.f5API.iControl.LocalLBMonitorTemplateType;
import org.sgcc.devops.f5API.iControl.LocalLBPoolMonitorAssociation;
import org.sgcc.devops.f5API.iControl.LocalLBProfileContextType;
import org.sgcc.devops.f5API.iControl.LocalLBProfileType;
import org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerPersistence;
import org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerProfile;
import org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerProfileAttribute;
import org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerResource;
import org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerType;
import org.sgcc.devops.f5API.iControl.SystemConfigSyncSyncMode;

public class LocalLBBean extends Object
{
	//--------------------------------------------------------------------------
	// Member Variables
	//--------------------------------------------------------------------------
	public String m_endpoint;
	public org.sgcc.devops.f5API.iControl.LocalLBVirtualServerBindingStub m_virtualServer;
	public org.sgcc.devops.f5API.iControl.LocalLBPoolBindingStub m_pool;
	public org.sgcc.devops.f5API.iControl.LocalLBPoolMemberBindingStub m_poolMember;	
	public org.sgcc.devops.f5API.iControl.LocalLBMonitorBindingStub m_monitor;
	public org.sgcc.devops.f5API.iControl.SystemConfigSyncBindingStub m_systemConfSync;

	//--------------------------------------------------------------------------
	// Constructor
	//--------------------------------------------------------------------------
	public LocalLBBean()
	{
		System.setProperty("javax.net.ssl.trustStore", System.getProperty("user.home") + "/.keystore");
		XTrustProvider.install();
	}

	//--------------------------------------------------------------------------
	// parseArgs
	//--------------------------------------------------------------------------
	public boolean login(String username,String password,String ipaddress,long port) throws Exception
	{
		boolean bSuccess = false;

		m_endpoint = "https://" + username + ":" + password + "@" + ipaddress + ":" + port + "/iControl/iControlPortal.cgi";

		m_virtualServer = (org.sgcc.devops.f5API.iControl.LocalLBVirtualServerBindingStub)
			new org.sgcc.devops.f5API.iControl.LocalLBVirtualServerLocator().getLocalLBVirtualServerPort(new java.net.URL(m_endpoint));

		m_virtualServer.setTimeout(60000);

		m_pool = (org.sgcc.devops.f5API.iControl.LocalLBPoolBindingStub)
				new org.sgcc.devops.f5API.iControl.LocalLBPoolLocator().getLocalLBPoolPort(new java.net.URL(m_endpoint));
		m_poolMember = (org.sgcc.devops.f5API.iControl.LocalLBPoolMemberBindingStub)
			new org.sgcc.devops.f5API.iControl.LocalLBPoolMemberLocator().getLocalLBPoolMemberPort(new java.net.URL(m_endpoint));

		m_pool.setTimeout(60000);
		m_poolMember.setTimeout(60000);

		m_monitor = (org.sgcc.devops.f5API.iControl.LocalLBMonitorBindingStub) new org.sgcc.devops.f5API.iControl.LocalLBMonitorLocator().getLocalLBMonitorPort(new java.net.URL(m_endpoint));
		m_monitor.setTimeout(60000);
		
		m_systemConfSync = (org.sgcc.devops.f5API.iControl.SystemConfigSyncBindingStub) new org.sgcc.devops.f5API.iControl.SystemConfigSyncLocator().getSystemConfigSyncPort(new java.net.URL(m_endpoint));
		m_systemConfSync.setTimeout(60000);
		
		bSuccess = true;

//		if ( args.length >= 5 )
//		{
//			getVSInfo(new String[] {args[4]});
//		}
//		else
//		{
//			getAllVSInfo();
//		}

		return bSuccess;
	}

	//-------------------------------------------------------------------------
	//
	//--------------------------------------------------------------------------
	public void createVs(String name,String vip,long port,String pool_name) throws Exception
	{
		
		String[] vss = m_virtualServer.get_list();
		for(int i=0;i<vss.length;i++)
		{
			if(vss[i].equalsIgnoreCase(name))
			{
				throw new Exception("Virtual Server Exists!");
			}
		}	
		//检测VIP port,避免已使用，避免使用f5的IP及端口
		CommonIPPortDefinition[] cipds = m_virtualServer.get_destination(vss);
		for(int i=0;i<cipds.length;i++)
		{
			CommonIPPortDefinition cipd = cipds[i];
			if(cipd.getAddress().equals(vip) && cipd.getPort()==port)
				throw new Exception("VIP and PORT already in use!");
		}
		
		CommonVirtualServerDefinition definitions = new CommonVirtualServerDefinition(name,vip,port,CommonProtocolType.fromString(CommonProtocolType._PROTOCOL_TCP));
		
		LocalLBVirtualServerVirtualServerResource  resources = new LocalLBVirtualServerVirtualServerResource(LocalLBVirtualServerVirtualServerType.RESOURCE_TYPE_POOL,pool_name);
		LocalLBVirtualServerVirtualServerProfile profiles = new LocalLBVirtualServerVirtualServerProfile(LocalLBProfileContextType.PROFILE_CONTEXT_TYPE_ALL,"tcp");
		m_virtualServer.create(new CommonVirtualServerDefinition[] {definitions}, new String[]{"255.255.255.255"}, new LocalLBVirtualServerVirtualServerResource[] {resources}, new LocalLBVirtualServerVirtualServerProfile[][] {new LocalLBVirtualServerVirtualServerProfile[]{profiles}});
		//set AutoMap for SNAT
		m_virtualServer.set_snat_automap(new String[] {name});
		
		m_virtualServer.add_profile(new String[] {name}, new LocalLBVirtualServerVirtualServerProfileAttribute[][]{new LocalLBVirtualServerVirtualServerProfileAttribute[]{new LocalLBVirtualServerVirtualServerProfileAttribute(LocalLBProfileType.PROFILE_TYPE_HTTP,LocalLBProfileContextType.PROFILE_CONTEXT_TYPE_ALL,"http")}});
		
		//persistence cookie
		m_virtualServer.add_persistence_profile(new String[]{name}, new LocalLBVirtualServerVirtualServerPersistence[][]{new LocalLBVirtualServerVirtualServerPersistence[]{new LocalLBVirtualServerVirtualServerPersistence("cookie",true)}});
		m_virtualServer.set_fallback_persistence_profile(new String[]{name}, new String[]{"source_addr"});

		
		//	System.out.println("Usage: LocalLBVSStats hostname port username password [vs_address]\n");
	}

	/**
	 * 创建Pool
	 * @param name 
	 * @param members ip端口冒号隔开，多个member用分号隔开 eg: 172.16.0.80:80;172.16.0.4:8080
	 * @param monitorName monitor name 如不指定可传入null或空，默认使用tcp
	 * @throws Exception
	 */
	public void createPool(String name,String members,String monitorName) throws Exception
	{
		name = name.toUpperCase().trim();
		// Get list of pools
		String[] poolList = m_pool.get_list();
		for(int i=0;i<poolList.length;i++)
		{
			if(poolList[i].equalsIgnoreCase(name.trim()))
			{
				throw new Exception("pool name exists!");
			}
		}
		if(monitorName==null || monitorName.trim().equals(""))
			monitorName="tcp";
		//获取monitor列表，监测monitor是否存在
//		LocalLBMonitorMonitorTemplate[] monitorTemplateList = m_monitor.get_template_list();
//		boolean existsMonitor = false;
//		for(int i=0;i<monitorTemplateList.length;i++)
//		{
//			LocalLBMonitorMonitorTemplate monitorTemplate = monitorTemplateList[i];
//
//			if(monitorTemplate.getTemplate_name().equalsIgnoreCase(monitorName))
//			{
//				existsMonitor = true;
//			}			
//		}
//		if(existsMonitor==false)
//			throw new Exception("monitor not exists!");		
		
		String[] ipports = members.trim().split(";");
		CommonIPPortDefinition[] cid = new CommonIPPortDefinition[ipports.length];
		for (int i=0;i<ipports.length;i++)
		{
			String[] ipport = ipports[i].split(":");
			String ip = ipport[0];
			long port = Long.parseLong(ipport[1]);
			cid[i] = new CommonIPPortDefinition(ip,port);
		}
		//给POOL增加member
		CommonIPPortDefinition[][] cids = new CommonIPPortDefinition[][]{cid};
		
		m_pool.create(new String[]{name}, new LocalLBLBMethod[]{LocalLBLBMethod.LB_METHOD_ROUND_ROBIN}, cids);
		
		//set monitor
		m_pool.set_monitor_association(new LocalLBPoolMonitorAssociation[]{new LocalLBPoolMonitorAssociation(name,new LocalLBMonitorRule(LocalLBMonitorRuleType.MONITOR_RULE_TYPE_SINGLE,1,new String[]{monitorName}))});
	}
	/**
	 * 给pool增加membere
	 * @param poolName
	 * @param members
	 * @throws Exception
	 */
	public void addMember(String poolName,String members) throws Exception
	{
		poolName = poolName.toUpperCase().trim();
		// Get list of pools
//		String[] poolList = m_pool.get_list();
//		boolean hasPool = false;
//		for(int i=0;i<poolList.length;i++)
//		{
//			if(poolList[i].equalsIgnoreCase(poolName.trim()))
//			{
//				hasPool = true;
//			}
//		}
//		if(hasPool == false)
//			throw new Exception("pool not exists!");
		
		String[] ipports = members.trim().split(";");
		CommonIPPortDefinition[] cid = new CommonIPPortDefinition[ipports.length];
		for (int i=0;i<ipports.length;i++)
		{
			String[] ipport = ipports[i].split(":");
			String ip = ipport[0];
			long port = Long.parseLong(ipport[1]);
			cid[i] = new CommonIPPortDefinition(ip,port);
		}
		CommonIPPortDefinition[][] cids = new CommonIPPortDefinition[][]{cid};
		
		//添加member
		m_pool.add_member(new String[]{poolName}, cids);
	}

	/**
	 * 从pool中移除membere
	 * @param poolName
	 * @param members
	 * @throws Exception
	 */
	public void removeMember(String poolName,String members) throws Exception
	{
		poolName = poolName.toUpperCase();
		// Get list of pools
		String[] poolList = m_pool.get_list();
		boolean hasPool = false;
		for(int i=0;i<poolList.length;i++)
		{
			if(poolList[i].equalsIgnoreCase(poolName.trim()))
			{
				hasPool = true;
			}
		}
		if(hasPool == false)
			throw new Exception("pool not exists!");
		
		String[] ipports = members.split(";");
		CommonIPPortDefinition[] cid = new CommonIPPortDefinition[ipports.length];
		for (int i=0;i<ipports.length;i++)
		{
			String[] ipport = ipports[i].split(":");
			String ip = ipport[0];
			long port = Long.parseLong(ipport[1]);
			cid[i] = new CommonIPPortDefinition(ip,port);
		}
		CommonIPPortDefinition[][] cids = new CommonIPPortDefinition[][]{cid};
		
		//移除member
		m_pool.remove_member(new String[]{poolName}, cids);
	}
	
	/**
	 * 创建monitor，只支持http类型
	 * @param name monitor name
	 * @param interval 检测时间间隔，建议5s
	 * @param timeout 超时时间，建议16s，3*interval+1
	 * @param templateName monitor模板名称，默认可使用http
	 * @param uri 检测的链接，使用相对路径即可，eg： /nesp/app/heartbeat
	 * @param receiveString 检测页面返回的特殊字符串，不建议使用html等页面常见字符串
	 * @throws Exception
	 */
	public void createMonitor(String name,long interval,long timeout,String templateName,String uri,String receiveString) throws Exception
	{
		name = name.toLowerCase().trim();
		if(templateName==null || templateName.trim().equals(""))
			templateName="http";
		if(uri==null||uri.trim().equals(""))
			uri = "/";
		uri = "GET "+uri+" HTTP/1.0\\r\\n";
		if(receiveString==null||receiveString.trim().equals(""))
			receiveString = "html";
		
//		LocalLBMonitorMonitorTemplate[] monitorTemplateList = m_monitor.get_template_list();
//		boolean existsTemplate = false;
//		for(int i=0;i<monitorTemplateList.length;i++)
//		{
//			LocalLBMonitorMonitorTemplate monitorTemplate = monitorTemplateList[i];
//			System.out.println(monitorTemplate.getTemplate_name()+monitorTemplate.getTemplate_type());
//			if(!monitorTemplate.getTemplate_type().equals(LocalLBMonitorTemplateType.TTYPE_HTTP)) continue;
//			
//			LocalLBMonitorIPPort[] lbips = m_monitor.get_template_destination(new String[]{monitorTemplate.getTemplate_name()});
//			for(int k=0;k<lbips.length;k++)
//			{
//				LocalLBMonitorIPPort lbip = lbips[k];
//				System.err.println("type:"+lbip.getAddress_type().getValue());
//				System.err.println(lbip.getIpport().getAddress()+"port::"+lbip.getIpport().getPort());
//			}
//			LocalLBMonitorStringValue[] lms = m_monitor.get_template_string_property(new String[]{monitorTemplate.getTemplate_name()}, new LocalLBMonitorStrPropertyType[]{LocalLBMonitorStrPropertyType.STYPE_SEND});
//			for(int j=0;j<lms.length;j++)
//			{
//				LocalLBMonitorStringValue lm = lms[j];
//				System.err.println(lm.getType()+"::"+lm.getValue());
//			}
//			if(monitorTemplate.getTemplate_name().equalsIgnoreCase(name))
//			{
//				throw new Exception("monitor name exists!");
//			}
//			
//			if(monitorTemplate.getTemplate_name().equalsIgnoreCase(templateName))
//			{
//				existsTemplate = true;
//			}			
//		}
		
//		if(existsTemplate==false)
//			throw new Exception("monitorTemplate not exists!");
		LocalLBMonitorMonitorTemplate monitorTemplate = new LocalLBMonitorMonitorTemplate(name,LocalLBMonitorTemplateType.TTYPE_HTTP);
		LocalLBMonitorCommonAttributes mca = new LocalLBMonitorCommonAttributes();
		mca.setInterval(interval);
		mca.setTimeout(timeout);
		mca.setParent_template(templateName);
		LocalLBMonitorIPPort lip = new LocalLBMonitorIPPort();
		lip.setAddress_type(LocalLBAddressType.ATYPE_STAR_ADDRESS_STAR_PORT);
		lip.setIpport(new CommonIPPortDefinition("0.0.0.0",0));
		mca.setDest_ipport(lip);
		m_monitor.create_template(new LocalLBMonitorMonitorTemplate[]{monitorTemplate}, new LocalLBMonitorCommonAttributes[]{mca});
		m_monitor.set_template_string_property(new String[]{name}, new LocalLBMonitorStringValue[]{new LocalLBMonitorStringValue(LocalLBMonitorStrPropertyType.STYPE_RECEIVE,receiveString)});
		m_monitor.set_template_string_property(new String[]{name}, new LocalLBMonitorStringValue[]{new LocalLBMonitorStringValue(LocalLBMonitorStrPropertyType.STYPE_SEND,uri)});
		
	}
	/**
	 * 删除Virtual Server
	 * @param name VS名称
	 * @throws Exception
	 */
	public void deleteVs(String name) throws Exception
	{
		name = name.trim().toUpperCase();
		String[] vss = m_virtualServer.get_list();
		boolean existsVs = false;
		for(int i=0;i<vss.length;i++)
		{
			if(vss[i].equalsIgnoreCase(name))
			{
				existsVs = true;
				break;
			}
		}
		if(existsVs==false) throw new Exception("Virtual Server not Exists!");
		m_virtualServer.delete_virtual_server(new String[]{name});

	}

	/**
	 * 删除Pool,删除前应确保不被VS使用
	 * @param name pool名称 
	 * @throws Exception
	 */
	public void deletePool(String name) throws Exception
	{
		name = name.toUpperCase().trim();
		// Get list of pools
		String[] poolList = m_pool.get_list();
		boolean existsPool = false;
		for(int i=0;i<poolList.length;i++)
		{
			if(poolList[i].equalsIgnoreCase(name.trim()))
			{
				existsPool = true;
				break;
			}
		}
		if(existsPool == false ) throw new Exception("pool not exists!");
		m_pool.delete_pool(new String[]{name});
	}
	/**
	 * 删除monitor，删除前应确保不被POOL使用
	 * @param name
	 * @throws Exception
	 */
	public void deleteMonitor(String name) throws Exception
	{
		name = name.toLowerCase().trim();
		LocalLBMonitorMonitorTemplate[] monitorTemplateList = m_monitor.get_template_list();
		boolean existsTemplate = false;
		for(int i=0;i<monitorTemplateList.length;i++)
		{
			LocalLBMonitorMonitorTemplate monitorTemplate = monitorTemplateList[i];
			
			if(monitorTemplate.getTemplate_name().equalsIgnoreCase(name))
			{
				existsTemplate = true;
			}			
		}
		
		if(existsTemplate==false)
			throw new Exception("monitorTemplate not exists!");
		m_monitor.delete_template(new String[]{name});
		
	}	
	
	/**
	 * （待测试）同步配置，使用BASIC模式，同步bigip.conf等基本配置。此处同步的参数与已知操作不一致，实际操作时同步到对端或从对端同步到本机
	 * @throws Exception
	 * 
	 */
	public void systemConfSync() throws Exception
	{
		try {
			m_systemConfSync.synchronize_configuration(SystemConfigSyncSyncMode.CONFIGSYNC_BASIC);
		} catch (RemoteException e) {
			e.printStackTrace();
			throw new Exception("Configuration Sync failed!");
		}
	}
	//--------------------------------------------------------------------------
	// getAllVSInfo
	//--------------------------------------------------------------------------
	public void getAllVSInfo() throws Exception
	{
		getVSInfo(m_virtualServer.get_list());
	}

	//--------------------------------------------------------------------------
	// getVSInfo
	//--------------------------------------------------------------------------
	public void getVSInfo(String[] vs_list) throws Exception
	{
		org.sgcc.devops.f5API.iControl.LocalLBVirtualServerVirtualServerStatistics vs_stats =
			m_virtualServer.get_statistics(vs_list);

		for(int i=0; i<vs_stats.getStatistics().length; i++)
		{
			org.sgcc.devops.f5API.iControl.CommonVirtualServerDefinition vs_def = 
				vs_stats.getStatistics()[i].getVirtual_server();


			System.out.println("Virtual Server : " + vs_def.getName()+" ----"+vs_def.getAddress()+":"+vs_def.getPort()+" Protocol:"+vs_def.getProtocol());
			
			
//			iControl.CommonStatistic [] stats_list =
//				vs_stats.getStatistics()[i].getStatistics();
//			for(int j=0; j<stats_list.length; j++)
//			{
//				System.out.println("--> " + 
//					stats_list[j].getType() +
//					" : " +
//					stats_list[j].getValue().getLow());
//			}
		}
	}
	
	//--------------------------------------------------------------------------
	// Main
	//--------------------------------------------------------------------------
	public static void main(String[] args)
	{
		try
		{
			LocalLBBean localLBBean = new LocalLBBean();
			localLBBean.login("admin","jsepc01!","qilincheng.oicp.net",443);
			//删除顺序VS POOL Monitor
//			localLBBean.deleteVs("QLC_TEST_VS3");
//			localLBBean.deletePool("QLC_TEST_POOL3");
//			localLBBean.deleteMonitor("qlc_monitor");
			//创建顺序monitor、POOL、VS
//			localLBBean.createMonitor("qlc_monitor",5, 16, "http", "/", "js.sgcc.com.cn");
//			localLBBean.createPool("QLC_TEST_POOL3", "10.134.88.217:80;10.134.90.116:80","qlc_monitor");
//			localLBBean.createVs("QLC_TEST_VS3", "172.16.85.201", 8092, "QLC_TEST_POOL3");
			//增加Member
			localLBBean.addMember("QLC_TEST_POOL3", "10.134.90.111:80");

		}
		catch(Exception ex)
		{
			ex.printStackTrace(System.out);
		}
	}
};
