package com.sgcc.devops.web.monitor;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ParameterizedPreparedStatementSetter;
import org.springframework.stereotype.Component;

import com.sgcc.devops.common.constant.Status;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.core.monitor.ContainerMonitorCluster;
import com.sgcc.devops.core.monitor.ContainerMonitorParam;
import com.sgcc.devops.dao.entity.Container;
import com.sgcc.devops.dao.entity.Monitor;
import com.sgcc.devops.service.ContainerService;
import com.sgcc.devops.web.task.TaskCache;

/**
 * 容器监控任务
 * @author dmw
 *
 */
@Component
public class ContainerMonitorTask {
	private static Logger logger = Logger.getLogger(ContainerMonitorTask.class);
	@Autowired
	private ContainerService containerService;
	@Autowired
	private ContainerMonitorCluster containerMonitorCluster;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private TaskCache cache;

	public void containerMonitor() {
		logger.debug("start monitor container...");
		Container container = new Container();
		container.setConPower((byte) Status.POWER.UP.ordinal());
		List<Container> containers = new ArrayList<>();
		try {
			containers = containerService.listContainerByPower(container);
		} catch (Exception e) {
			logger.error("get live container exception:"+ e);
		}
		if (containers == null || containers.isEmpty()) {
			logger.info("monitor container over!");
			return;
		}
		new Thread(new ContainerDetectTask(cache, containers)).start();// 启动探测容器健康任务
		List<Monitor> monitors = new ArrayList<>();
		try {
			monitors = containerMonitorCluster.batchMoniter(getContainerMonitorParam(containers));
		} catch (Exception e) {
			logger.error("get container load data exception:"+ e);
		}
		if (monitors == null || monitors.isEmpty()) {
			logger.info("monitor container over!");
			return;
		}
		try {
			String sql = "insert into dop_monitor(MONITOR_ID,TARGET_TYPE,MONITOR_TIME,TARGET_ID,CPU,MEM,NETIN,NETOUT,DISK_SPACE) values(?,?,?,?,?,?,?,?,?)";
			jdbcTemplate.batchUpdate(
					sql.toString(),
			        monitors,
			        monitors.size(),
			        new ParameterizedPreparedStatementSetter<Monitor>() {
			          public void setValues(PreparedStatement ps, Monitor argument) throws SQLException {
			        	  ps.setString(1, KeyGenerator.uuid());
			        	  ps.setByte(2, argument.getTargetType());
			        	  TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
			        	  ps.setTimestamp(3,new Timestamp(new Date().getTime()));
			        	  ps.setString(4, argument.getTargetId());
			        	  ps.setString(5, argument.getCpu());
			        	  ps.setString(6, argument.getMem());
			        	  ps.setString(7, argument.getNetin());
			        	  ps.setString(8, argument.getNetout());
			        	  ps.setString(9, argument.getDiskSpace());
			          }
			        });
			
		} catch (Exception e) {
			logger.error("save container data exception:"+ e);
		}
		logger.debug("monitor container over!");
	}

	// 将Container转换为ContainerMonitorParam
	public List<ContainerMonitorParam> getContainerMonitorParam(List<Container> containers) {
		List<ContainerMonitorParam> containerMonitorParams = new ArrayList<>();
		for (Container container : containers) {
			ContainerMonitorParam containerMonitorParam = new ContainerMonitorParam(container.getConId(),
					container.getClusterIp(), container.getClusterPort(), container.getConUuid());
			containerMonitorParams.add(containerMonitorParam);
		}
		return containerMonitorParams;
	}

}
