package com.sgcc.devops.web.controller.action;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.model.HostComponentModel;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.JSONUtil;
import com.sgcc.devops.common.util.KeyGenerator;
import com.sgcc.devops.dao.entity.HostComponent;
import com.sgcc.devops.dao.entity.HostInstall;
import com.sgcc.devops.dao.entity.User;
import com.sgcc.devops.web.manager.HostComponentManager;
import com.sgcc.devops.web.manager.ServerRoomManager;
import com.sgcc.devops.web.query.QueryList;


@Controller
@RequestMapping("hostComponent")
public class HostComponentAction {
	Logger logger = Logger.getLogger(HostComponentAction.class);
	
	@Resource
	private QueryList queryList;
	@Resource
	private ServerRoomManager serverRoomManager;
	@Resource
	private HostComponentManager hostComponentManager;

	@RequestMapping(value = "/hostComponentListAll", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean hostComponentList(HttpServletRequest request,PagerModel pagerModel) {
		return queryList.hostComponentList(pagerModel);
	}
	

	@RequestMapping(value = "/hostComponentList", method = { RequestMethod.GET })
	@ResponseBody
	public GridBean hostComponentList(HttpServletRequest request,PagerModel pagerModel, HostComponentModel hostComponentModel) {
		return queryList.hostComponentList(pagerModel,hostComponentModel);
	}

	@RequestMapping(value = "/deleteHostComponent", method = RequestMethod.POST)
	@ResponseBody
	public Result delete(HttpServletRequest request, HostComponentModel hostComponentModel) {

		User user = (User) request.getSession().getAttribute("user");
		JSONObject params = JSONUtil.parseObjectToJsonObject(hostComponentModel);
		params.put("userId", user.getUserId());
		Result result = hostComponentManager.deleteHostComponent(params);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	
	@RequestMapping(value = "/selectById", method = RequestMethod.POST)
	@ResponseBody
	public HostComponent selectById(HttpServletRequest request, HostComponentModel hostComponentModel) {
		HostComponent hostComponent =hostComponentManager.selectById(hostComponentModel.getId());
		return hostComponent;
	}
	
	@RequestMapping(value = "/createHostComponent", method = RequestMethod.POST)
	@ResponseBody
	public Result createHostComponent(HttpServletRequest request, HostComponentModel hostComponentModel) {
		User user = (User) request.getSession().getAttribute("user");
		hostComponentModel.setCreator(user.getUserId());
		hostComponentModel.setCreateTime(new Date());
		hostComponentModel.setId(KeyGenerator.uuid());
		Result result = hostComponentManager.createHostComponent(hostComponentModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	

	@RequestMapping(value = "/updateHostComponent", method = RequestMethod.POST)
	@ResponseBody
	public Result updateHostComponent(HttpServletRequest request,HostComponentModel hostComponentModel) {
		Result result = hostComponentManager.updateHostComponent(hostComponentModel);
		request.setAttribute("logDetail", result.getMessage());
		request.setAttribute("success", result.isSuccess()?"Success":"Fail");
		return result;
	}
	@RequestMapping(value = "/autoHostComponent", method = RequestMethod.POST)
	@ResponseBody
	public Result autoHostComponent(HttpServletRequest request,HostComponentModel hostComponentModel) {
		Result result = new Result();
		result.setMessage("自动升级成功");
		result.setSuccess(true);
		return result;
	}
	@RequestMapping(value = "/selectHostInstallByHostId", method = RequestMethod.GET)
	@ResponseBody
	public List<HostInstall> selectHostInstallByHostId(HttpServletRequest request,@RequestParam(value = "hostId", required = true)String hostId) {
		List<HostInstall> hostInstalls = hostComponentManager.selectHostInstallByHostId(hostId);
		return hostInstalls;
	}
}
