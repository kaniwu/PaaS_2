<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="com.sgcc.devops.dao.intf.HostMapper">
	<resultMap id="BaseResultMap" type="com.sgcc.devops.dao.entity.Host">
		<id column="HOST_ID" property="hostId" jdbcType="VARCHAR" />
		<result column="HOST_UUID" property="hostUuid" jdbcType="VARCHAR" />
		<result column="HOST_NAME" property="hostName" jdbcType="VARCHAR" />
		<result column="HOST_USER" property="hostUser" jdbcType="VARCHAR" />
		<result column="HOST_PWD" property="hostPwd" jdbcType="VARCHAR" />
		<result column="HOST_TYPE" property="hostType" jdbcType="TINYINT" />
		<result column="HOST_IP" property="hostIp" jdbcType="VARCHAR" />
		<result column="HOST_CPU" property="hostCpu" jdbcType="INTEGER" />
		<result column="HOST_MEM" property="hostMem" jdbcType="INTEGER" />
		<result column="HOST_STATUS" property="hostStatus" jdbcType="TINYINT" />
		<result column="HOST_DESC" property="hostDesc" jdbcType="VARCHAR" />
		<result column="HOST_KERNEL_VERSION" property="hostKernelVersion"
			jdbcType="VARCHAR" />
		<result column="CLUSTER_ID" property="clusterId" jdbcType="VARCHAR" />
		<result column="HOST_CREATETIME" property="hostCreatetime"
			jdbcType="TIMESTAMP" />
		<result column="HOST_CREATOR" property="hostCreator" jdbcType="VARCHAR" />
		<result column="DOCKER_STATUS" property="dockerStatus" jdbcType="TINYINT" />
		<result column="LOCATION_ID" property="locationId" jdbcType="VARCHAR" />
		<result column="REG_ID" property="regId" jdbcType="VARCHAR" />
		<result column="ENVIRONMENT_ID" property="hostEnv" jdbcType="VARCHAR" />
	</resultMap>
	<resultMap id="BaseResultMapForComponentHost" type="com.sgcc.devops.dao.entity.ComponentHost">
		<id column="HOST_ID" property="hostId" jdbcType="VARCHAR" />
		<result column="HOST_UUID" property="hostUuid" jdbcType="VARCHAR" />
		<result column="HOST_NAME" property="hostName" jdbcType="VARCHAR" />
		<result column="HOST_USER" property="hostUser" jdbcType="VARCHAR" />
		<result column="HOST_PWD" property="hostPwd" jdbcType="VARCHAR" />
		<result column="HOST_TYPE" property="hostType" jdbcType="TINYINT" />
		<result column="HOST_IP" property="hostIp" jdbcType="VARCHAR" />
		<result column="HOST_CPU" property="hostCpu" jdbcType="INTEGER" />
		<result column="HOST_MEM" property="hostMem" jdbcType="INTEGER" />
		<result column="HOST_STATUS" property="hostStatus" jdbcType="TINYINT" />
		<result column="HOST_DESC" property="hostDesc" jdbcType="VARCHAR" />
		<result column="ENVIRONMENT_ID" property="hostEnv" jdbcType="VARCHAR" />
		<result column="HOST_KERNEL_VERSION" property="hostKernelVersion"
			jdbcType="VARCHAR" />
		<result column="CLUSTER_ID" property="clusterId" jdbcType="VARCHAR" />
		<result column="HOST_CREATETIME" property="hostCreatetime"
			jdbcType="TIMESTAMP" />
		<result column="HOST_CREATOR" property="hostCreator" jdbcType="VARCHAR" />
		<result column="DOCKER_STATUS" property="dockerStatus" jdbcType="TINYINT" />
		
		<result column="COMPONENT_HOST_ID" property="componentHostId" jdbcType="VARCHAR" />
		<result column="DOP_COMPONENT_ID" property="dopComponentId" jdbcType="VARCHAR" />
		<result column="NGINX_STATUS" property="nginxStatus" jdbcType="TINYINT" />
		<result column="NGINX_PORT" property="nginxPort" jdbcType="INTEGER" />
	</resultMap>

	<sql id="Base_Column_List">
		HOST_ID, HOST_UUID, HOST_NAME, HOST_USER, HOST_PWD,
		HOST_TYPE, HOST_IP,
		HOST_CPU,
		HOST_MEM, HOST_STATUS,HOST_DESC,
		HOST_KERNEL_VERSION, CLUSTER_ID,
		HOST_CREATETIME,
		HOST_CREATOR,
		DOCKER_STATUS,
		LOCATION_ID,
		REG_ID, ENVIRONMENT_ID
	</sql>

	<!-- Select all host -->
	<select id="selectAllHost" resultMap="BaseResultMap" parameterType="com.sgcc.devops.dao.entity.Host">
		select
		<include refid="Base_Column_List" />
		from dop_host where HOST_STATUS != 0
		
		<if test="hostIp != null and hostIp !='' ">
		AND HOST_IP like '%${hostIp}%'
		</if>
		<if test="clusterId != null and clusterId != ''">
		AND CLUSTER_ID = #{clusterId,jdbcType=VARCHAR}
		</if>
		<if test="hostType != null or hostType==0">
		AND HOST_TYPE = #{hostType,jdbcType=TINYINT}
		</if>
		<if test="hostEnv != null  and hostEnv != ''">
		AND ENVIRONMENT_ID = #{hostEnv,jdbcType=VARCHAR}
		</if>
<!-- 		<if test="locationId != null and locationId !=''"> -->
<!-- 		AND LOCATION_ID = #{locationId,jdbcType=VARCHAR} -->
<!-- 		</if> -->
		<if test="locationIdList != null">
			AND LOCATION_ID in
			<foreach collection="locationIdList" index="index" item="item" open="("
				separator="," close=")">
				#{item}
			</foreach>
		</if>
	</select>
	
	<select id="hostListByRack" resultMap="BaseResultMap" parameterType="com.sgcc.devops.dao.entity.Host">
		select
		<include refid="Base_Column_List" />
		from dop_host where HOST_STATUS != 0
		AND LOCATION_ID = #{locationId,jdbcType=VARCHAR}
		<if test="clusterId != null">
			AND CLUSTER_ID = #{clusterId,jdbcType=VARCHAR}
		</if>
	</select>
	<!-- Select all host -->
	<select id="allOnlineHost" resultMap="BaseResultMap">
		select
		<include refid="Base_Column_List" />
		from dop_host where HOST_STATUS in (1,2)
	</select>
	<!-- Select hostList -->
<!-- 	<select id="selectHostList" resultMap="BaseResultMap"> -->
<!-- 		select -->
<!-- 		<include refid="Base_Column_List" /> -->
<!-- 		from dop_host -->
<!-- 		where HOST_TYPE = #{hostType,jdbcType=TINYINT} -->
<!-- 		AND -->
<!-- 		HOST_STATUS != 0 -->
<!-- 	</select> -->
	<!-- Select freeHostList -->
	<select id="freeHostList" resultMap="BaseResultMap" parameterType="com.sgcc.devops.dao.entity.Host">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where HOST_STATUS != 0 and HOST_STATUS != 3 and HOST_TYPE =4
		<if test="hostEnv != null  and hostEnv != ''">
		AND ENVIRONMENT_ID = #{hostEnv,jdbcType=VARCHAR}
		</if>
	</select>

	<!-- Select hostList -->
	<select id="selectHostListByCluster" resultMap="BaseResultMap">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where HOST_TYPE = #{hostType,jdbcType=TINYINT} AND
		HOST_STATUS != 0 AND CLUSTER_ID IS NULL OR CLUSTER_ID = 0
	</select>

	<!-- Select hostList -->
	<select id="selectHostListByClusterId" resultMap="BaseResultMap">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where CLUSTER_ID = #{clusterId,jdbcType=TINYINT} AND
		HOST_STATUS != 0
	</select>

	<!-- Select hostList -->
	<select id="selectHostListByHost" resultMap="BaseResultMap">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where CLUSTER_ID = #{clusterId,jdbcType=TINYINT}
		<if test="hostType != null">
			AND HOST_TYPE = #{hostType,jdbcType=TINYINT}
		</if>
		AND HOST_STATUS != 0
	</select>


	<select id="selectHost" resultMap="BaseResultMap"
		parameterType="com.sgcc.devops.dao.entity.Host">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where HOST_STATUS != 0
		<if test="hostId != null and hostId !=''">
			AND HOST_ID = #{hostId,jdbcType=VARCHAR}
		</if>
		<if test="hostName != null and hostName != ''">
			AND HOST_NAME = #{hostName,jdbcType=VARCHAR}
		</if>
		<if test="hostIp != null and hostIp != ''">
			AND HOST_IP = #{hostIp,jdbcType=VARCHAR}
		</if>
		<if test="hostPwd != null and hostPwd != ''">
			AND HOST_PWD = #{hostPwd,jdbcType=VARCHAR}
		</if>
	</select>

	<select id="loadHost" resultMap="BaseResultMap">
		select
		<include refid="Base_Column_List" />
		from dop_host
		WHERE HOST_ID = #{hostId,jdbcType=VARCHAR}
	</select>
	<select id="selectHostCount" resultType="java.lang.Integer"
		parameterType="com.sgcc.devops.dao.entity.Host">
		select count(*) from dop_host
		where HOST_STATUS != 0
		<if test="hostType != null and hostType !='' or hostType==0">
			AND HOST_TYPE = #{hostType,jdbcType=TINYINT}
		</if>
	</select>

	<select id="selectHostByIp" resultMap="BaseResultMap"
		parameterType="java.lang.String">
		select
		<include refid="Base_Column_List" />
		from dop_host
		where HOST_IP = #{hostIp,jdbcType=VARCHAR} AND
		HOST_STATUS != 0
	</select>
	
	<select id="selectComponentHost" resultMap="BaseResultMapForComponentHost"
		parameterType="java.lang.String">
		select
		a.HOST_ID, a.HOST_UUID, a.HOST_NAME, a.HOST_USER, a.HOST_PWD,
		a.HOST_TYPE, a.HOST_IP,
		a.HOST_CPU,
		a.HOST_MEM, a.HOST_STATUS, a.HOST_DESC,
		a.HOST_KERNEL_VERSION, a.CLUSTER_ID,
		a.HOST_CREATETIME,
		a.HOST_CREATOR,
		a.DOCKER_STATUS,
		b.ID AS COMPONENT_HOST_ID,b.DOP_COMPONENT_ID,b.NGINX_STATUS,b.NGINX_PORT 
		from dop_host a,dop_comp_host b 
		where a.HOST_ID = b.HOST_ID and b.DOP_COMPONENT_ID = #{componentId,jdbcType=VARCHAR}
	</select>

	<update id="deleteHost" parameterType="java.lang.Integer">
		update dop_host
		<set>
			HOST_STATUS = 0
		</set>
		where HOST_ID = #{hostId,jdbcType=VARCHAR}
	</update>

	<update id="deleteHosts" parameterType="java.util.List">
		update dop_host
		<set>
			HOST_STATUS = 0
		</set>
		where HOST_ID in
		<foreach collection="list" index="index" item="item" open="("
			separator="," close=")">
			#{item}
		</foreach>
	</update>

	<insert id="insertHost" parameterType="com.sgcc.devops.dao.entity.Host">
		insert into dop_host
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="hostId != null and hostId != ''">
				HOST_ID,
			</if>
			<if test="hostUuid != null and hostUuid != ''">
				HOST_UUID,
			</if>
			<if test="hostName != null and hostName != ''">
				HOST_NAME,
			</if>
			<if test="hostUser != null and hostUser != ''">
				HOST_USER,
			</if>
			<if test="hostPwd != null and hostPwd != ''">
				HOST_PWD,
			</if>
			<if test="hostType != null ">
				HOST_TYPE,
			</if>
			<if test="hostIp != null and hostIp != ''">
				HOST_IP,
			</if>
			<if test="hostCpu != null ">
				HOST_CPU,
			</if>
			<if test="hostMem != null ">
				HOST_MEM,
			</if>
			<if test="hostStatus != null ">
				HOST_STATUS,
			</if>
			<if test="hostDesc != null ">
				HOST_DESC,
			</if>
			<if test="hostKernelVersion != null and hostKernelVersion != ''">
				HOST_KERNEL_VERSION,
			</if>
			<if test="clusterId != null ">
				CLUSTER_ID,
			</if>
			<if test="hostCreatetime != null ">
				HOST_CREATETIME,
			</if>
			<if test="hostCreator != null ">
				HOST_CREATOR,
			</if>
			<if test="dockerStatus != null ">
				DOCKER_STATUS,
			</if>
			<if test="locationId != null ">
				LOCATION_ID,
			</if>
			<if test="regId != null ">
				REG_ID,
			</if>
			<if test="hostEnv != null ">
				ENVIRONMENT_ID,
			</if>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
			<if test="hostId != null and hostId != ''">
				#{hostId,jdbcType=VARCHAR},
			</if>
			<if test="hostUuid != null and hostUuid != ''">
				#{hostUuid,jdbcType=VARCHAR},
			</if>
			<if test="hostName != null and hostName != ''">
				#{hostName,jdbcType=VARCHAR},
			</if>
			<if test="hostUser != null and hostUser != ''">
				#{hostUser,jdbcType=VARCHAR},
			</if>
			<if test="hostPwd != null and hostPwd != ''">
				#{hostPwd,jdbcType=VARCHAR},
			</if>
			<if test="hostType != null ">
				#{hostType,jdbcType=TINYINT},
			</if>
			<if test="hostIp != null and hostIp != ''">
				#{hostIp,jdbcType=VARCHAR},
			</if>
			<if test="hostCpu != null ">
				#{hostCpu,jdbcType=INTEGER},
			</if>
			<if test="hostMem != null ">
				#{hostMem,jdbcType=INTEGER},
			</if>
			<if test="hostStatus != null ">
				#{hostStatus,jdbcType=TINYINT},
			</if>
			<if test="hostDesc != null ">
				#{hostDesc,jdbcType=VARCHAR},
			</if>
			<if test="hostKernelVersion != null and hostKernelVersion != ''">
				#{hostKernelVersion,jdbcType=VARCHAR},
			</if>
			<if test="clusterId != null ">
				#{clusterId,jdbcType=VARCHAR},
			</if>
			<if test="hostCreatetime != null ">
				#{hostCreatetime,jdbcType=TIMESTAMP},
			</if>
			<if test="hostCreator != null ">
				#{hostCreator,jdbcType=VARCHAR},
			</if>
			<if test="dockerStatus != null ">
				#{dockerStatus,jdbcType=TINYINT},
			</if>
			<if test="locationId != null ">
				#{locationId,jdbcType=VARCHAR},
			</if>
			<if test="regId != null ">
				#{regId,jdbcType=VARCHAR},
			</if>
			<if test="hostEnv != null ">
				#{hostEnv,jdbcType=VARCHAR},
			</if>
		</trim>
	</insert>

	<update id="updateHost" parameterType="com.sgcc.devops.dao.entity.Host">
		update dop_host
		<set>
			<if test="hostUuid != null and hostUuid != ''">
				HOST_UUID = #{hostUuid,jdbcType=VARCHAR},
			</if>
			<if test="hostName != null and hostName != ''">
				HOST_NAME = #{hostName,jdbcType=VARCHAR},
			</if>
			<if test="hostUser != null and hostUser != ''">
				HOST_USER = #{hostUser,jdbcType=VARCHAR},
			</if>
			<if test="hostPwd != null and hostPwd != ''">
				HOST_PWD = #{hostPwd,jdbcType=VARCHAR},
			</if>
			<if test="hostType != null ">
				HOST_TYPE = #{hostType,jdbcType=TINYINT},
			</if>
			<if test="hostIp != null and hostIp != ''">
				HOST_IP = #{hostIp,jdbcType=VARCHAR},
			</if>
			<if test="hostCpu != null ">
				HOST_CPU = #{hostCpu,jdbcType=INTEGER},
			</if>
			<if test="hostMem != null ">
				HOST_MEM = #{hostMem,jdbcType=INTEGER},
			</if>
			<if test="hostStatus != null ">
				HOST_STATUS = #{hostStatus,jdbcType=TINYINT},
			</if>
			<if test="hostDesc != null ">
				HOST_DESC = #{hostDesc,jdbcType=VARCHAR},
			</if>
			<if test="hostKernelVersion != null and hostKernelVersion != ''">
				HOST_KERNEL_VERSION =
				#{hostKernelVersion,jdbcType=VARCHAR},
			</if>
			<if test="clusterId != null ">
				CLUSTER_ID = #{clusterId,jdbcType=VARCHAR},
			</if>
			<if test="hostCreatetime != null ">
				HOST_CREATETIME = #{hostCreatetime,jdbcType=TIMESTAMP},
			</if>
			<if test="hostCreator != null ">
				HOST_CREATOR = #{hostCreator,jdbcType=VARCHAR},
			</if>
			<if test="dockerStatus != null ">
				DOCKER_STATUS = #{dockerStatus,jdbcType=TINYINT},
			</if>
			<if test="locationId != null ">
				LOCATION_ID = #{locationId,jdbcType=VARCHAR},
			</if>
			<if test="regId != null ">
				REG_ID = #{regId,jdbcType=VARCHAR},
			</if>
			<if test="hostEnv != null ">
				ENVIRONMENT_ID = #{hostEnv,jdbcType=VARCHAR},
			</if>
		</set>
		where HOST_ID = #{hostId,jdbcType=VARCHAR}
	</update>
	
	<update id="deleteCompHost" parameterType="java.lang.String">
		delete from dop_comp_host 
		where ID = #{id,jdbcType=VARCHAR}
	</update>
	<!-- 新增组件 主机信息 -->
	<insert id="insertComponentHost" parameterType="com.sgcc.devops.dao.entity.ComponentHost">
		insert into dop_comp_host
		<trim prefix="(" suffix=")" suffixOverrides=",">
			<if test="componentHostId != null and componentHostId != ''">
				ID,
			</if>
			<if test="dopComponentId != null and dopComponentId != ''">
				DOP_COMPONENT_ID,
			</if>
			<if test="hostId != null and hostId != ''">
				HOST_ID,
			</if>
			<if test="nginxStatus != null and nginxStatus != ''">
				NGINX_STATUS,
			</if>
			<if test="nginxPort != null and nginxPort != ''">
				NGINX_PORT,
			</if>
		</trim>
		<trim prefix="values (" suffix=")" suffixOverrides=",">
			<if test="componentHostId != null and componentHostId != ''">
				#{componentHostId,jdbcType=VARCHAR},
			</if>
			<if test="dopComponentId != null and dopComponentId != ''">
				#{dopComponentId,jdbcType=VARCHAR},
			</if>
			<if test="hostId != null and hostId != ''">
				#{hostId,jdbcType=VARCHAR},
			</if>
			<if test="nginxStatus != null and nginxStatus != ''">
				#{nginxStatus,jdbcType=TINYINT},
			</if>
			<if test="nginxPort != null and nginxPort != ''">
				#{nginxPort,jdbcType=INTEGER},
			</if>
		</trim>
	</insert>
	
	<update id="updateComponentHost" parameterType="com.sgcc.devops.dao.entity.ComponentHost">
		update dop_comp_host
		<set>
			<if test="dopComponentId != null and dopComponentId != ''">
				DOP_COMPONENT_ID = #{dopComponentId,jdbcType=VARCHAR},
			</if>
			<if test="hostId != null and hostId != ''">
				HOST_ID = #{hostId,jdbcType=VARCHAR},
			</if>
			<if test="nginxStatus != null and nginxStatus != ''">
				NGINX_STATUS = #{nginxStatus,jdbcType=TINYINT},
			</if>
			<if test="nginxPort != null and nginxPort != ''">
				NGINX_PORT = #{nginxPort,jdbcType=INTEGER},
			</if>
		</set>
		where ID = #{componentHostId,jdbcType=VARCHAR}
	</update>
</mapper>