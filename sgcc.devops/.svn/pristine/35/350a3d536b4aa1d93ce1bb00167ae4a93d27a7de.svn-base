package com.sgcc.devops.core.monitor;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sgcc.devops.common.constant.Type.TARGET_TYPE;
import com.sgcc.devops.core.util.HttpClient;
import com.sgcc.devops.dao.entity.Monitor;

/**
 * 容器监控客户端
 * 
 * @author dmw
 *
 */
@Repository("containerMonitorClient")
public class ContainerMonitorClient {

	@Autowired
	private HttpClient httpClient;

	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}

	/**
	 * 获取容器的监控数据
	 * 
	 * @param clusterIp
	 *            容器所在集群的IP
	 * @param clusterPort
	 *            容器所在集群的服务端口
	 * @param containerId
	 *            容器自身的UUID
	 * @return 容器当前的监控数据
	 */
	public Monitor monitor(ContainerMonitorParam param) {
		String response = httpClient.get(null, assembleUrl(param.getCluter(), param.getPort(), param.getContainer()));
		if (StringUtils.isEmpty(response)) {
			return null;
		}
		JSONObject jsonObject = JSON.parseObject(response);
		NetIO firstNet = parseNetIO(jsonObject);
		Cpu firstCpu = parseCpu(jsonObject.getJSONObject("cpu_stats"));
		long begin = System.currentTimeMillis();
		response = httpClient.get(null, assembleUrl(param.getCluter(), param.getPort(), param.getContainer()));
		long end = System.currentTimeMillis();
		if (StringUtils.isEmpty(response)) {
			return null;
		}
		BigDecimal gap = new BigDecimal(end - begin).divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP);
		jsonObject = JSON.parseObject(response);
		NetIO secondNet = parseNetIO(jsonObject);
		Cpu secondCpu = parseCpu(jsonObject.getJSONObject("cpu_stats"));
		BigDecimal ninSpeed = secondNet.getNetin().subtract(firstNet.getNetin()).divide(gap, 3, RoundingMode.HALF_UP);
		BigDecimal nouSpeed = secondNet.getNetou().subtract(firstNet.getNetou()).divide(gap, 3, RoundingMode.HALF_UP);
		BigDecimal mem = jsonObject.getJSONObject("memory_stats").getBigDecimal("usage")
				.divide(new BigDecimal(1024 * 1024), 2, RoundingMode.HALF_UP);
		BigDecimal all = new BigDecimal(secondCpu.getAll() - firstCpu.getAll());
		BigDecimal cpu = new BigDecimal(secondCpu.getUse() - firstCpu.getUse());
		BigDecimal cpuUsage = (cpu.multiply(new BigDecimal("100"))).divide(all, 2, RoundingMode.HALF_UP);
		TimeZone.setDefault(TimeZone.getTimeZone("Etc/GMT-8"));
		Monitor monitor = new Monitor((byte) TARGET_TYPE.CONTAINER.ordinal(), new Timestamp(new Date().getTime()),
				param.getId(), cpuUsage.doubleValue() + "", mem.toString(), ninSpeed.toString(), nouSpeed.toString(),
				null);
		return monitor;
	}

	private String assembleUrl(String clusterIp, String clusterPort, String containerId) {
		return "http://" + clusterIp + ":" + clusterPort + "/containers/" + containerId + "/stats?stream=false";
	}

	private NetIO parseNetIO(JSONObject json) {
		BigDecimal nin = new BigDecimal(0);
		BigDecimal nou = new BigDecimal(0);
		if (null != json.getJSONObject("network")) {
			nin = new BigDecimal(json.getJSONObject("network").getLong("rx_bytes"));
			nou = new BigDecimal(json.getJSONObject("network").getLong("tx_bytes"));
		} else if (null != json.getJSONObject("networks")) {
			nin = new BigDecimal(json.getJSONObject("networks").getJSONObject("eth0").getLong("rx_bytes"));
			nou = new BigDecimal(json.getJSONObject("networks").getJSONObject("eth0").getLong("tx_bytes"));
		}
		return new NetIO(nin.divide(new BigDecimal(1024)), nou.divide(new BigDecimal(1024)));
	}

	/**
	 * 解析CPU状态
	 * 
	 * @param json
	 * @return
	 */
	private Cpu parseCpu(JSONObject json) {
		int core = json.getJSONObject("cpu_usage").getJSONArray("percpu_usage").size();
		BigDecimal all = json.getBigDecimal("system_cpu_usage");
		BigDecimal use = json.getJSONObject("cpu_usage").getBigDecimal("total_usage");
		return new Cpu(use.longValue(), all.longValue(), core);
	}

	/**
	 * 网络监控对象
	 * 
	 * @author dmw
	 *
	 */
	class NetIO {
		private BigDecimal netin;
		private BigDecimal netou;

		public NetIO(BigDecimal netin, BigDecimal netou) {
			super();
			this.netin = netin;
			this.netou = netou;
		}

		public BigDecimal getNetin() {
			return netin;
		}

		public void setNetin(BigDecimal netin) {
			this.netin = netin;
		}

		public BigDecimal getNetou() {
			return netou;
		}

		public void setNetou(BigDecimal netou) {
			this.netou = netou;
		}
	}

	/**
	 * CPU监控数据
	 * 
	 * @author dmw
	 *
	 */
	class Cpu {
		private int core;
		private long use;
		private long all;

		public long getUse() {
			return use;
		}

		public void setUse(long use) {
			this.use = use;
		}

		public long getAll() {
			return all;
		}

		public void setAll(long all) {
			this.all = all;
		}

		public int getCore() {
			return core;
		}

		public void setCore(int core) {
			this.core = core;
		}

		public Cpu(long use, long all, int core) {
			super();
			this.use = use;
			this.all = all;
			this.core = core;
		}
	}
}
