package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.DeployConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.material.ConfigFileMaterial;

/**
 * config.xml文件生成器
 * 
 * @author dmw
 *
 */
@Repository("configFileBuilder")
public class ConfigFileBuilder implements FileBuilder {

	private static Logger logger = Logger.getLogger(DockerfileBuilder.class);
	private static final String FILE_NAME = "config.vm";
	private DeployConfig deployConfig;
	private ConfigFileMaterial material;

	private enum ParamPosition {
		APP_NAME, // 应用名称
		APP_FILE, // 应用包名
		SESSION_SHARE, // 是否session共享
		JDBC_CONFIG// 数据源配置map
		;
	}

	public void init(ConfigFileMaterial material, DeployConfig deployConfig) {
		this.material = material;
		this.deployConfig = deployConfig;
	}

	public DeployConfig getDeployConfig() {
		return deployConfig;
	}

	public void setDeployConfig(DeployConfig deployConfig) {
		this.deployConfig = deployConfig;
	}

	public ConfigFileMaterial getMaterial() {
		return material;
	}

	public void setMaterial(ConfigFileMaterial material) {
		this.material = material;
	}

	@Override
	public File build() {
		logger.info("开始制作weblogic的config.xml文件");
		if (null == material) {
			logger.error("Config material is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, deployConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();
		// 组合配置信息
		context.put(ParamPosition.APP_NAME.name(), material.getAppName());
		context.put(ParamPosition.APP_FILE.name(), material.getAppFile());
		context.put(ParamPosition.SESSION_SHARE.name(), material.isSessionShare());
		context.put(ParamPosition.JDBC_CONFIG.name(), material.getJdbcConfig());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get dockerfile template infos error", e);
			return null;
		}
		// 生成新的配置文件
		String localPath = material.getDirectory();
		localPath = localPath.replace("\\", "/");
		localPath = FileUploadUtil.check(localPath);
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = FileUploadUtil.check(localPath);
		File file = new File(localPath + "/config.xml");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build configfile exception:", e);
				return null;
			}
		}
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			logger.info("制作config.xml完成");
			return file;
		} catch (IOException e) {
			logger.error("fild file exception:", e);
			return null;
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
