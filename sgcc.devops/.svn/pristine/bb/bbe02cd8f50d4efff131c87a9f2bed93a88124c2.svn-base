package com.sgcc.devops.web.image.builder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.stereotype.Repository;

import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.util.FileUploadUtil;
import com.sgcc.devops.web.image.FileBuilder;
import com.sgcc.devops.web.image.material.JdbcFileMaterial;

/**
 * jdbc数据源文件生成器
 * 
 * @author dmw
 *
 */
@Repository("jdbcFileBuilder")
public class JdbcFileBuilder implements FileBuilder {

	private static Logger logger = Logger.getLogger(DockerfileBuilder.class);
	private static final String FILE_NAME = "jdbc.vm";
	private LocalConfig localConfig;
	private JdbcFileMaterial material;

	private enum ParamPosition {
		SOURCE_NAME, // 数据源名称
		JDBC_URL, // 数据源字符串
		JDBC_DRIVER, // 驱动
		JDBC_USER, // 用户名
		PASSWORD, // 密码
		JNDI_NAME,// jndi名称
		INIT_SQL;//初始化脚本
	}

	public void init(JdbcFileMaterial material,LocalConfig localConfig) {
		this.material = material;
		this.localConfig = localConfig;
	}

	public LocalConfig getLocalConfig() {
		return localConfig;
	}
	public void setLocalConfig(LocalConfig localConfig) {
		this.localConfig = localConfig;
	}

	public JdbcFileMaterial getMaterial() {
		return material;
	}

	public void setMaterial(JdbcFileMaterial material) {
		this.material = material;
	}

	@Override
	public File build() {
		if (null == material) {
			logger.error("JDBC material is Null!");
			return null;
		}
		Properties properties = new Properties();
		properties.setProperty(VelocityEngine.FILE_RESOURCE_LOADER_PATH, localConfig.getTemplatePath());
		Velocity.init(properties);
		VelocityContext context = new VelocityContext();
		// 组合配置信息
		context.put(ParamPosition.SOURCE_NAME.name(), material.getSourceName());
		context.put(ParamPosition.JDBC_URL.name(), material.getDataSource().getJdbcUrl());
		context.put(ParamPosition.JDBC_DRIVER.name(), material.getDataSource().getJdbcDriver());
		context.put(ParamPosition.JDBC_USER.name(), material.getDataSource().getJdbcUser());
		context.put(ParamPosition.PASSWORD.name(), material.getDataSource().getJdbcPassword());
		context.put(ParamPosition.JNDI_NAME.name(), material.getDataSource().getJndiName());
		context.put(ParamPosition.INIT_SQL.name(), material.getDataSource().getInitsql()==null?"":material.getDataSource().getInitsql());
		// 加载配置文件
		Template template = null;
		try {
			template = Velocity.getTemplate(FILE_NAME);
		} catch (Exception e) {
			logger.error("Get dockerfile template infos error", e);
			return null;
		}
		// 生成新的配置文件
		String localPath = material.getDirectory();
		localPath = FileUploadUtil.check(localPath);
		File direction = new File(localPath);
		if (!direction.exists()) {
			direction.mkdirs();
		}
		localPath = localPath.replace("\\", "/");
		File file = new File(localPath + "/" + material.getSourceName() + "-jdbc.xml");
		if (!file.exists()) {
			// 不存在则创建新文件
			try {
				file.createNewFile();
			} catch (IOException e) {
				logger.error("build jdbcfile exception:", e);
				return null;
			}
		}
		FileWriter fw = null;
		BufferedWriter writer = null;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			// 替换文件内容
			writer = new BufferedWriter(fw);
			template.merge(context, writer);
			writer.flush();
			return file;
		} catch (IOException e) {
			logger.error("fild file exception:", e);
			return null;
		}finally{
			if(writer!=null){
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fw!=null){
				try {
					fw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
