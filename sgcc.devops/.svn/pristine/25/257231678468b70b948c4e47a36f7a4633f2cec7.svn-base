package com.sgcc.devops.web.controller.action;

import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sgcc.devops.common.bean.GridBean;
import com.sgcc.devops.common.bean.Result;
import com.sgcc.devops.common.config.LocalConfig;
import com.sgcc.devops.common.model.PagerModel;
import com.sgcc.devops.common.util.TimeUtils;
import com.sgcc.devops.dao.entity.Log;
import com.sgcc.devops.dao.entity.Logs;
import com.sgcc.devops.service.LogService;
import com.sgcc.devops.service.LogsService;
import com.sgcc.devops.web.image.Encrypt;

/**  
 * date：2016年2月4日 下午1:48:35
 * project name：sgcc.devops.web
 * @author  mayh
 * @version 1.0   
 * @since JDK 1.7.0_21  
 * file name：LogAction.java
 * description：  日志操作访问控制类
 */
@Controller
@RequestMapping("log")
public class LogAction {

	private static Logger logger = Logger.getLogger(LogAction.class);
	@Autowired
	private LogService logService;
	@Autowired
	private LogsService logsService;
	@Resource
	private LocalConfig localConfig;
	@RequestMapping(value = "/list")
	@ResponseBody
	public GridBean list(PagerModel pagerModel, String from, String to, String target,
			String userName, String userIp, String logResult) {
		Log log = new Log();
		if (null != from && !from.isEmpty()) {
			log.setBeginTime(TimeUtils.stringToDateTime(from));
		}
		if (null != to && !to.isEmpty()) {
			log.setEndTime(TimeUtils.stringToDateTime(to));
		}
		if (null != target && !target.isEmpty()) {
			/*try {
				target = new String(target.getBytes("ISO-8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("target encoding error: " + e.getMessage());
			}*/
			log.setLogObject(target);
		}
		if (null != userName && !userName.isEmpty()) {
			log.setUserName(userName);
		}
		if (null != userIp && !userIp.isEmpty()) {
			log.setUserIp(userIp);
		}
		if (null != logResult && !logResult.isEmpty()) {
			log.setLogResult(logResult);
		}

		return logService.list(log,pagerModel);
	}

	@RequestMapping("/encry/{password}")
	@ResponseBody
	public Result encry(@PathVariable String password) {
		return new Result(true, Encrypt.encrypt(password, localConfig.getSecurityPath()));
	}

	@RequestMapping(value = "/runLogslist")
	@ResponseBody
	public GridBean runLogslist(@RequestParam(value = "page", required = true) int page,
			@RequestParam(value = "rows", required = true) int rows, String from, String to, String level) {
		Logs logs = new Logs();
		if (null != from && !from.isEmpty()) {
			logs.setBeginTime(TimeUtils.stringToDateTime(from));
		}
		if (null != to && !to.isEmpty()) {
			logs.setEndTime(TimeUtils.stringToDateTime(to));
		}
		if (null != level && !level.isEmpty()) {
			try {
				level = new String(level.getBytes("ISO-8859-1"), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				logger.error("target encoding error " + e.getMessage());
			}
			logs.setLevel(level);
		}
		return logsService.list(logs, page, rows);
	}

}
